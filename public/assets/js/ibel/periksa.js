// advanced search
$("#advancedSearch").submit(function (e) {
    e.preventDefault();

    var query = $(this)
        .serializeArray()
        .filter(function (i) {
            return i.value;
        });

    window.location.href =
        $(this).attr("action") + (query ? "?" + $.param(query) : "");
});

// button setuju permohonan
$(".btnSetujuPermohonan").click(function () {
    let id = $(this).data("id");
    let token = $("meta[name='csrf-token']").attr("content");
    let url = app_url + "/ibel/periksa/permohonan/" + id + "/setuju";

    Swal.fire({
        title: "Yakin data akan disetujui?",
        text: "Data akan masuk ke tahap berikutnya!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Setuju!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            window.location.href =
                app_url + "/ibel/periksa/permohonan/" + id + "/setuju";
        }
    });
});

// button tolak permohonan
$(".btnTolakPermohonan").click(function () {
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        input: "textarea",
        inputLabel: "Alasan Tolak",
        inputPlaceholder: "Masukan Alasan Penolakan",
        inputAttributes: {
            "aria-label": "Masukan Alasan Penolakan",
        },
        title: "Yakin akan menolak permohonan?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Tolak!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/ibel/periksa/permohonan/" + id + "/tolak",
                data: {
                    _token: token,
                    keterangan: result.value,
                },
                success: function (response) {
                    if (response.status == 1) {
                        swalSuccess("Permohonan berhasil di tolak");
                        setTimeout(function () {
                            window.location.href = app_url + "/ibel/periksa/";
                        }, 1500);
                    } else {
                        swalError(response.message);
                    }
                },
            });
        }
    });
});
