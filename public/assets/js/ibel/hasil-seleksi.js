// assets
$(".datepick").flatpickr();

$(".tahun_format").datepicker({
    format: "yyyy",
    weekStart: 1,
    orientation: "bottom",
    // keyboardNavigation: false,
    viewMode: "years",
    minViewMode: "years",
});

$(".bulan_format").datepicker({
    format: "mm",
    weekStart: 1,
    orientation: "bottom",
    // keyboardNavigation: false,
    viewMode: "years",
    minViewMode: "years",
});

// masking tahun
Inputmask({
    mask: "9",
    repeat: 4,
    greedy: false,
}).mask(".tahun_mask");

Inputmask({
    mask: "9",
    repeat: 2,
    greedy: false,
}).mask(".bulan_mask");

// advanced search
$("#advancedSearch").submit(function (e) {
    e.preventDefault();

    var query = $(this)
        .serializeArray()
        .filter(function (i) {
            return i.value;
        });

    window.location.href =
        $(this).attr("action") + (query ? "?" + $.param(query) : "");
});

// show hide keputusan
$('input[name="keputusan"]').on("click", function () {
    let opsi = $('input[name="keputusan"]:checked').val();

    // open card
    if (opsi == 1) {
        $("#jangka_waktu").attr("hidden", true);
        $("#alasan_penolakan").attr("hidden", true);

        $("#jangka_waktu").attr("hidden", false);
        $("#alasan_penolakan").attr("hidden", true);
        $("#card_tim_seleksi").attr("hidden", false);
        $("#buttonAction").attr("hidden", false);
    } else if (opsi == 0) {
        $("#jangka_waktu").attr("hidden", true);
        $("#alasan_penolakan").attr("hidden", true);

        $("#jangka_waktu").attr("hidden", true);
        $("#alasan_penolakan").attr("hidden", false);
        $("#card_tim_seleksi").attr("hidden", false);
        $("#buttonAction").attr("hidden", false);
    }
});

// get tim seleksi ibel
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    // get jenjang (add)
    $("#tim_seleksi").select2({
        minimumInputLength: 3,
        ajax: {
            url: app_url + "/ibel/hasil-seleksi/get-tim-seleksi",
            dataType: "json",
            type: "post",
            delay: 300,
            data: function (params) {
                return {
                    search: params.term,
                };
            },
            processResults: function (response) {
                return {
                    results: response,
                };
            },
        },
    });
});
