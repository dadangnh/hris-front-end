$("#btnKelola2").click(function () {
    var $this = $(this);
    $this.attr("cek", $this.html());
    $this.prop("disabled", true);
    $this.html('Loading... <i class="spinner-border spinner-border-sm"></i>');
    setTimeout(function () {
        $this.prop("disabled", false);
        $this.html($this.attr("cek"));
    }, 60000);
});

// button hapus ibel
$(".btnHapusIbel").click(function () {
    var id = $(this).data("id_ref_ibel_hapus");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        text: "Data tidak dapat dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/ibel/administrasi/hapus-ibel",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: id,
                },
                success: function (response) {
                    $("#" + id).remove();

                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: response.message,
                        showConfirmButton: false,
                        title: "Berhasil",
                        text: "data berhasil dihapus",
                        timer: 2000,
                    });
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button kirim izin belajar
$(".btnKirimIbel").click(function () {
    var id = $(this).data("id");
    var outputid = $(this).data("outputid");
    var output = $(this).data("output");
    var token = $("meta[name='csrf-token']").attr("content");
    var url = $(this).data("url");

    Swal.fire({
        title: "Yakin akan mengirim laporan?",
        text: "Data akan masuk ke tahap berikutnya!",
        icon: "info",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Kirim!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: url,
                data: {
                    _token: token,
                    id: id,
                    outputid: outputid,
                    output: output,
                },
                success: function (response) {
                    console.log(response);
                    if (response.status == 1) {
                        location.reload();
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "Laporan berhasil dikirim",
                            showConfirmButton: false,
                            timer: 1800,
                        });
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1800,
                        });
                    }
                },
            });
        }
    });
});

// button lihat surat ibel
$(".btnLihatKepIbel").click(function () {
    // document.getElementById("data_st_stan").style.display = "none !important";

    $("#lihat_surat_ibel").modal("show");

    $("#nomor_surat_ibel").val(
        $(this).data("nomor_surat_ibel")
    );
    $("#tanggal_surat_ibel").val(
        $(this).data("tanggal_surat_ibel")
    );
    $("#dok_surat_ibel").attr(
        "href",
        app_url + "/files/" + $(this).data("dok_surat_ibel")
    );
    $("#nomor_kep_ibel").val(
        $(this).data("nomor_kep_ibel")
    );
    $("#tanggal_kep_ibel").val(
        $(this).data("tanggal_kep_ibel")
    );
    $("#dok_kep_ibel").attr(
        "href",
        app_url + "/files/" + $(this).data("dok_kep_ibel")
    );

});
// button lihat surat penolakan
$(".btnLihatSuratTolak").click(function () {
    // document.getElementById("data_st_stan").style.display = "none !important";

    $("#lihat_surat_penolakan").modal("show");

    $("#nomor_surat_penolakan").val(
        $(this).data("nomor_surat_penolakan")
    );
    $("#tanggal_surat_penolakan").val(
        $(this).data("tanggal_surat_penolakan")
    );
    $("#dok_surat_penolakan").attr(
        "href",
        app_url + "/files/" + $(this).data("dok_surat_penolakan")
    );
    $("#nomor_surat_pemeberitahuan").val(
        $(this).data("nomor_surat_pemeberitahuan")
    );
    $("#tanggal_surat_pemberitahuan").val(
        $(this).data("tanggal_surat_pemberitahuan")
    );
    $("#dok_surat_pemberitahuan").attr(
        "href",
        app_url + "/files/" + $(this).data("dok_surat_pemberitahuan")
    );

});
