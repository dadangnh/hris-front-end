// advanced search
$("#advancedSearch").submit(function (e) {
    e.preventDefault();

    var query = $(this)
        .serializeArray()
        .filter(function (i) {
            return i.value;
        });

    window.location.href =
        $(this).attr("action") + (query ? "?" + $.param(query) : "");
});

// button setuju permohonan
$(".btnDsPermohonan").click(function () {
    // get element check
    var inputElements = document.getElementsByClassName("dataCheck");

    // looping data, get data check
    var id = [];
    for (var i = 0; inputElements[i]; i++) {
        if (inputElements[i].checked) {
            id.push(inputElements[i].value);
        }
    }

    // validasi
    if (id == "") {
        swalError("Tidak ada data yg dipilih")
    } else {
        // show modal
        $("#modal_ds").modal("show");

        // set to modal
        $("#id_permohonan_ibel").val(id);
    }
});
