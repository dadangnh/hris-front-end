// assets
$(".datepick").flatpickr();

// advanced search
$("#advancedSearch").submit(function (e) {
    e.preventDefault();

    var query = $(this)
        .serializeArray()
        .filter(function (i) {
            return i.value;
        });

    window.location.href =
        $(this).attr("action") + (query ? "?" + $.param(query) : "");
});

// button lanjutkan prosesan
$(".btnRekamTindakLanjut").on("click", function () {
    let token = $("meta[name='csrf-token']").attr("content");
    let id_permohonan = $(this).data("id_permohonan");

    $("#modal_perekaman_tindak_lanjut").modal("show");
    $("input[name='id_permohonan']").val(id_permohonan);

    $("input[name='nomor_surat_penolakan']").val($(this).data('no_surat_permohonan'));
    $("input[name='tgl_surat_penolakan']").val($(this).data('tgl_surat_permohonan'));

    $("input[name='nomor_nd']").val($(this).data('no_nd_pemberitahuan'));
    $("input[name='tgl_nd']").val($(this).data('tgl_nd_pemberitahuan'));
});

// button lanjutkan prosesan
$(".btnLanjutkanProses").on("click", function () {
    let token = $("meta[name='csrf-token']").attr("content");
    let id_permohonan = $(this).data("id_permohonan");

    Swal.fire({
        title: "Yakin akan melanjutkan proses?",
        text: "Data tidak dapat dikembalikan!",
        icon: "info",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Kirim!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            window.location.href =
                app_url + "/ibel/tindak-lanjut/proses/" + id_permohonan;
        }
    });
});
