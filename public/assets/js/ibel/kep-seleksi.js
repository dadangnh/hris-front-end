// assets
$(".datepick").flatpickr();

// advanced search
$("#advancedSearch").submit(function (e) {
    e.preventDefault();

    var query = $(this)
        .serializeArray()
        .filter(function (i) {
            return i.value;
        });

    window.location.href =
        $(this).attr("action") + (query ? "?" + $.param(query) : "");
});

// show hide keputusan
$('input[name="keputusan"]').on("click", function () {
    let opsi = $('input[name="keputusan"]:checked').val();

    // open card
    if (opsi == 1) {
        $("#jangka_waktu").attr("hidden", true);
        $("#alasan_penolakan").attr("hidden", true);

        $("#jangka_waktu").attr("hidden", false);
        $("#alasan_penolakan").attr("hidden", true);
        $("#buttonAction").attr("hidden", false);
    } else if (opsi == 0) {
        $("#jangka_waktu").attr("hidden", true);
        $("#alasan_penolakan").attr("hidden", true);

        $("#jangka_waktu").attr("hidden", true);
        $("#alasan_penolakan").attr("hidden", false);
        $("#buttonAction").attr("hidden", false);
    }
});

// button lanjutkan prosesan
$(".btnRekamPermohonan").on("click", function () {
    let token = $("meta[name='csrf-token']").attr("content");
    let id_permohonan = $(this).data("id_permohonan");

    $("#modal_rekam_keputusan_hasil_seleksi").modal("show");
    $("input[name='id_permohonan']").val(id_permohonan);

    $.ajax({
        type: "get",
        url: app_url + "/ibel/kep-seleksi/" + id_permohonan + "/kep_seleksi",
        data: {
            _token: token,
        },
        success: function (response) {
            $("input[name='nomor_kep']").val(response.nomorKep);
            $("input[name='tgl_kep']").val(response.tglKep);
        },
    });
});

// button lanjutkan prosesan
$(".btnLanjutkanProses").on("click", function () {
    let token = $("meta[name='csrf-token']").attr("content");
    let id_permohonan = $(this).data("id_permohonan");

    Swal.fire({
        title: "Yakin akan melanjutkan proses?",
        text: "Data tidak dapat dikembalikan!",
        icon: "info",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Kirim!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            window.location.href =
                app_url + "/ibel/kep-seleksi/proses/" + id_permohonan;
        }
    });
});
