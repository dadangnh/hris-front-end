// button hapus ibel
$(".btnHapusIbel").click(function () {
    var id = $(this).data("id_ref_ibel_hapus");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        text: "Data tidak dapat dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/ibel/administrasi/hapus-ibel",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: id,
                },
                success: function (response) {
                    location.reload();
                    $("#" + id).remove();

                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: response.message,
                        showConfirmButton: false,
                        title: "Berhasil",
                        text: "data berhasil dihapus",
                        timer: 2000,
                    });
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button kirim izin belajar
$(".btnKirimIbel").click(function () {
    var id = $(this).data("id");
    var outputid = $(this).data("outputid");
    var output = $(this).data("output");
    var token = $("meta[name='csrf-token']").attr("content");
    var url = $(this).data("url");

    Swal.fire({
        title: "Yakin akan mengirim laporan?",
        text: "Data akan masuk ke tahap berikutnya!",
        icon: "info",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Kirim!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: url,
                data: {
                    _token: token,
                    id: id,
                    outputid: outputid,
                    output: output,
                },
                success: function (response) {
                    console.log(response);
                    if (response.status == 1) {
                        location.reload();
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "Laporan berhasil dikirim",
                            showConfirmButton: false,
                            timer: 1800,
                        });
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1800,
                        });
                    }
                },
            });
        }
    });
});

// spinner / loading button
function BtnLoading(elem) {
    $(elem).attr("data-original-text", $(elem).html());
    $(elem).prop("disabled", true);
    $(elem).html('<i class="spinner-border spinner-border-sm"></i>');
    setTimeout(function () {
        $(elem).prop("disabled", false);
        $(elem).html($(elem).attr("data-original-text"));
    }, 20000);
}

// spinner off / reset button
function BtnReset(elem) {
    $(elem).prop("disabled", false);
    $(elem).html($(elem).attr("data-original-text"));
}

//upload file_tidak_menuntut_upkp
$(document).on("click", "#btn_file_tidak_menuntut_upkp", function () {
    var $this = $(this);

    //Call Button Loading Function
    BtnLoading($this);
    var file_tidak_menuntut_upkp_lama = $(
        "#myinputfile_tidak_menuntut_upkp"
    ).val();
    var idIbel = $("#idIbel").val();
    var token = $("meta[name='csrf-token']").attr("content");

    //cek file kosong
    if ($("#file_tidak_menuntut_upkp").prop("files")[0] == undefined) {
        swalError("Tidak ada file yg diupload!");
        BtnReset($this);
    } else {
        //upload file
        var file_data = $("#file_tidak_menuntut_upkp").prop("files")[0];
        var form_data = new FormData();

        form_data.append("file_tidak_menuntut_upkp", file_data);
        form_data.append(
            "pegawaiId",
            "{{ session()->get('user')['pegawaiId'] }}"
        );

        $.ajax({
            url: app_url + "/ibel/administrasi/upload-dokumen-persyaratan/upload_file",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: "post",
            success: function (response) {
                var data = JSON.parse(JSON.stringify(response));

                // jika sukses upload input nama file ke db
                if (data.status == 1) {
                    var nmfile_tidak_menuntut_upkp_baru = data.file;
                    // alert(file_lama);
                    $.ajax({
                        type: "patch",
                        url:
                            app_url +
                            "/ibel/administrasi/upload-dokumen-persyaratan/inputNameFile",
                        data: {
                            _token: token,
                            file_tidak_menuntut_upkp_baru:
                                nmfile_tidak_menuntut_upkp_baru,
                            file_tidak_menuntut_upkp_lama:
                                file_tidak_menuntut_upkp_lama,
                            idIbel: idIbel,
                        },
                        success: (response) => {
                            // jika sukses munculkan icon file pdf dan link donwload
                            if (response.status == 1) {
                                Swal.fire({
                                    position: "center",
                                    icon: "success",
                                    title: "Berhasil",
                                    text: "File berhasil diupload",
                                    showConfirmButton: false,
                                    timer: 2000,
                                }),
                                    (document.getElementById(
                                        "iconfile"
                                    ).style.display = "");
                                var nmfile_tidak_menuntut_upkp =
                                    app_url + "/files/" + data.file;
                                $("#myinputfile_tidak_menuntut_upkp")
                                    .val(data.file)
                                    .change();
                                document.getElementById(
                                    "urlfile_tidak_menuntut_upkp"
                                ).href = nmfile_tidak_menuntut_upkp;
                            } else {
                                alert(response);
                                Swal.fire({
                                    position: "center",
                                    icon: "error",
                                    title: "Gagal",
                                    text: data.message,
                                    showConfirmButton: false,
                                    timer: 2000,
                                });
                            }
                        },
                    });
                } else {
                    Swal.fire({
                        position: "center",
                        icon: "error",
                        title: "Gagal",
                        text: data.message,
                        showConfirmButton: false,
                        timer: 2000,
                    });
                }
                // reset button
                BtnReset($this);
            },
        });
    }
});
//upload file_prodi_sesuai_kebutuhan
$(document).on("click", "#btn_file_prodi_sesuai_kebutuhan", function () {
    var $this = $(this);
    //Call Button Loading Function
    BtnLoading($this);
    // console.log($("#file_jarak_lokasi").prop("files")[0])
    var file_prodi_sesuai_kebutuhan_lama = $(
        "#myinputfile_prodi_sesuai_kebutuhan"
    ).val();
    var idIbel = $("#idIbel").val();
    var token = $("meta[name='csrf-token']").attr("content");

    //cek file kosong
    if ($("#file_prodi_sesuai_kebutuhan").prop("files")[0] == undefined) {
        swalError("Tidak ada file yg diupload!");
        BtnReset($this);
    } else {
        //upload file
        var file_data = $("#file_prodi_sesuai_kebutuhan").prop("files")[0];
        var form_data = new FormData();

        form_data.append("file_prodi_sesuai_kebutuhan", file_data);
        form_data.append(
            "pegawaiId",
            "{{ session()->get('user')['pegawaiId'] }}"
        );

        $.ajax({
            url: app_url + "/ibel/administrasi/upload-dokumen-persyaratan/upload_file",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: "post",
            success: function (response) {
                var data1 = JSON.parse(JSON.stringify(response));

                // jika sukses upload input nama file ke db
                if (data1.status == 1) {
                    var nmfile_prodi_sesuai_kebutuhan_baru = data1.file;
                    // alert(file_lama);
                    $.ajax({
                        type: "patch",
                        url:
                            app_url +
                            "/ibel/administrasi/upload-dokumen-persyaratan/inputNameFile",
                        data: {
                            _token: token,
                            file_prodi_sesuai_kebutuhan_baru:
                                nmfile_prodi_sesuai_kebutuhan_baru,
                            file_prodi_sesuai_kebutuhan_lama:
                                file_prodi_sesuai_kebutuhan_lama,
                            idIbel: idIbel,
                        },
                        success: (response) => {
                            // jika sukses munculkan icon file pdf dan link donwload
                            if (response.status == 1) {
                                Swal.fire({
                                    position: "center",
                                    icon: "success",
                                    title: "Berhasil",
                                    text: "File berhasil diupload",
                                    showConfirmButton: false,
                                    timer: 2000,
                                }),
                                    (document.getElementById(
                                        "iconfile2"
                                    ).style.display = "");
                                var nmfile_prodi_sesuai_kebutuhan =
                                    app_url + "/files/" + data1.file;
                                $("#myinputfile_prodi_sesuai_kebutuhan")
                                    .val(data1.file)
                                    .change();
                                document.getElementById(
                                    "urlfile_prodi_sesuai_kebutuhan"
                                ).href = nmfile_prodi_sesuai_kebutuhan;
                            } else {
                                alert(response);
                                Swal.fire({
                                    position: "center",
                                    icon: "error",
                                    title: "Gagal",
                                    text: data1.message,
                                    showConfirmButton: false,
                                    timer: 2000,
                                });
                            }
                        },
                    });
                } else {
                    Swal.fire({
                        position: "center",
                        icon: "error",
                        title: "Gagal",
                        text: data1.message,
                        showConfirmButton: false,
                        timer: 2000,
                    });
                }
                // reset button
                BtnReset($this);
            },
        });
    }
});
//upload file_tidak_skorsing
$(document).on("click", "#btn_file_tidak_skorsing", function () {
    var $this = $(this);
    //Call Button Loading Function
    BtnLoading($this);
    // console.log($("#file_jarak_lokasi").prop("files")[0])
    var file_tidak_skorsing_lama = $("#myinputfile_tidak_skorsing").val();
    var idIbel = $("#idIbel").val();
    var token = $("meta[name='csrf-token']").attr("content");

    //cek file kosong
    if ($("#file_tidak_skorsing").prop("files")[0] == undefined) {
        swalError("Tidak ada file yg diupload!");
        BtnReset($this);
    } else {
        //upload file
        var file_data = $("#file_tidak_skorsing").prop("files")[0];
        var form_data = new FormData();

        form_data.append("file_tidak_skorsing", file_data);
        form_data.append(
            "pegawaiId",
            "{{ session()->get('user')['pegawaiId'] }}"
        );

        $.ajax({
            url: app_url + "/ibel/administrasi/upload-dokumen-persyaratan/upload_file",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: "post",
            success: function (response) {
                var data2 = JSON.parse(JSON.stringify(response));

                // jika sukses upload input nama file ke db
                if (data2.status == 1) {
                    var nmfile_tidak_skorsing_baru = data2.file;
                    // alert(file_lama);
                    $.ajax({
                        type: "patch",
                        url:
                            app_url +
                            "/ibel/administrasi/upload-dokumen-persyaratan/inputNameFile",
                        data: {
                            _token: token,
                            file_tidak_skorsing_baru:
                                nmfile_tidak_skorsing_baru,
                            file_tidak_skorsing_lama: file_tidak_skorsing_lama,
                            idIbel: idIbel,
                        },
                        success: (response) => {
                            // jika sukses munculkan icon file pdf dan link donwload
                            if (response.status == 1) {
                                Swal.fire({
                                    position: "center",
                                    icon: "success",
                                    title: "Berhasil",
                                    text: "File berhasil diupload",
                                    showConfirmButton: false,
                                    timer: 2000,
                                }),
                                    (document.getElementById(
                                        "iconfile3"
                                    ).style.display = "");
                                var nmfile_tidak_skorsing =
                                    app_url + "/files/" + data2.file;
                                $("#myinputfile_tidak_skorsing")
                                    .val(data2.file)
                                    .change();
                                document.getElementById(
                                    "urlfile_tidak_skorsing"
                                ).href = nmfile_tidak_skorsing;
                            } else {
                                alert(response);
                                Swal.fire({
                                    position: "center",
                                    icon: "error",
                                    title: "Gagal",
                                    text: data2.message,
                                    showConfirmButton: false,
                                    timer: 2000,
                                });
                            }
                        },
                    });
                } else {
                    Swal.fire({
                        position: "center",
                        icon: "error",
                        title: "Gagal",
                        text: data2.message,
                        showConfirmButton: false,
                        timer: 2000,
                    });
                }
                // reset button
                BtnReset($this);
            },
        });
    }
});
//upload file_tidak_hukdis
$(document).on("click", "#btn_file_tidak_hukdis", function () {
    var $this = $(this);
    //Call Button Loading Function
    BtnLoading($this);
    // console.log($("#file_jarak_lokasi").prop("files")[0])
    var file_tidak_hukdis_lama = $("#myinputfile_tidak_hukdis").val();
    var idIbel = $("#idIbel").val();
    var token = $("meta[name='csrf-token']").attr("content");

    //cek file kosong
    if ($("#file_tidak_hukdis").prop("files")[0] == undefined) {
        swalError("Tidak ada file yg diupload!");
        BtnReset($this);
    } else {
        //upload file
        var file_data = $("#file_tidak_hukdis").prop("files")[0];
        var form_data = new FormData();

        form_data.append("file_tidak_hukdis", file_data);
        form_data.append(
            "pegawaiId",
            "{{ session()->get('user')['pegawaiId'] }}"
        );

        $.ajax({
            url: app_url + "/ibel/administrasi/upload-dokumen-persyaratan/upload_file",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: "post",
            success: function (response) {
                var data3 = JSON.parse(JSON.stringify(response));

                // jika sukses upload input nama file ke db
                if (data3.status == 1) {
                    var nmfile_tidak_hukdis_baru = data3.file;
                    // alert(file_lama);
                    $.ajax({
                        type: "patch",
                        url:
                            app_url +
                            "/ibel/administrasi/upload-dokumen-persyaratan/inputNameFile",
                        data: {
                            _token: token,
                            file_tidak_hukdis_baru: nmfile_tidak_hukdis_baru,
                            file_tidak_hukdis_lama: file_tidak_hukdis_lama,
                            idIbel: idIbel,
                        },
                        success: (response) => {
                            // jika sukses munculkan icon file pdf dan link donwload
                            if (response.status == 1) {
                                Swal.fire({
                                    position: "center",
                                    icon: "success",
                                    title: "Berhasil",
                                    text: "File berhasil diupload",
                                    showConfirmButton: false,
                                    timer: 2000,
                                }),
                                    (document.getElementById(
                                        "iconfile4"
                                    ).style.display = "");
                                var nmfile_tidak_hukdis =
                                    app_url + "/files/" + data3.file;
                                $("#myinputfile_tidak_hukdis")
                                    .val(data3.file)
                                    .change();
                                document.getElementById(
                                    "urlfile_tidak_hukdis"
                                ).href = nmfile_tidak_hukdis;
                            } else {
                                alert(response);
                                Swal.fire({
                                    position: "center",
                                    icon: "error",
                                    title: "Gagal",
                                    text: data3.message,
                                    showConfirmButton: false,
                                    timer: 2000,
                                });
                            }
                        },
                    });
                } else {
                    Swal.fire({
                        position: "center",
                        icon: "error",
                        title: "Gagal",
                        text: data3.message,
                        showConfirmButton: false,
                        timer: 2000,
                    });
                }
                // reset button
                BtnReset($this);
            },
        });
    }
});
//upload file_jarak_lokasi
$(document).on("click", "#btn_file_jarak_lokasi", function () {
    var $this = $(this);
    //Call Button Loading Function
    BtnLoading($this);
    // console.log($("#file_jarak_lokasi").prop("files")[0])
    var file_jarak_lokasi_lama = $("#myinputfile_jarak_lokasi").val();
    var idIbel = $("#idIbel").val();
    var token = $("meta[name='csrf-token']").attr("content");

    //cek file kosong
    if ($("#file_jarak_lokasi").prop("files")[0] == undefined) {
        swalError("Tidak ada file yg diupload!");
        BtnReset($this);
    } else {
        //upload file
        var file_data = $("#file_jarak_lokasi").prop("files")[0];
        var form_data = new FormData();

        form_data.append("file_jarak_lokasi", file_data);
        form_data.append(
            "pegawaiId",
            "{{ session()->get('user')['pegawaiId'] }}"
        );

        $.ajax({
            url: app_url + "/ibel/administrasi/upload-dokumen-persyaratan/upload_file",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: "post",
            success: function (response) {
                var data4 = JSON.parse(JSON.stringify(response));

                // jika sukses upload input nama file ke db
                if (data4.status == 1) {
                    var nmfile_jarak_lokasi_baru = data4.file;
                    // alert(file_lama);
                    $.ajax({
                        type: "patch",
                        url:
                            app_url +
                            "/ibel/administrasi/upload-dokumen-persyaratan/inputNameFile",
                        data: {
                            _token: token,
                            file_jarak_lokasi_baru: nmfile_jarak_lokasi_baru,
                            file_jarak_lokasi_lama: file_jarak_lokasi_lama,
                            idIbel: idIbel,
                        },
                        success: (response) => {
                            // jika sukses munculkan icon file pdf dan link donwload
                            if (response.status == 1) {
                                Swal.fire({
                                    position: "center",
                                    icon: "success",
                                    title: "Berhasil",
                                    text: "File berhasil diupload",
                                    showConfirmButton: false,
                                    timer: 2000,
                                }),
                                    (document.getElementById(
                                        "iconfile5"
                                    ).style.display = "");
                                var nmfile_jarak_lokasi =
                                    app_url + "/files/" + data4.file;
                                $("#myinputfile_jarak_lokasi")
                                    .val(data4.file)
                                    .change();
                                document.getElementById(
                                    "urlfile_jarak_lokasi"
                                ).href = nmfile_jarak_lokasi;
                            } else {
                                alert(response);
                                Swal.fire({
                                    position: "center",
                                    icon: "error",
                                    title: "Gagal",
                                    text: data4.message,
                                    showConfirmButton: false,
                                    timer: 2000,
                                });
                            }
                        },
                    });
                } else {
                    Swal.fire({
                        position: "center",
                        icon: "error",
                        title: "Gagal",
                        text: data4.message,
                        showConfirmButton: false,
                        timer: 2000,
                    });
                }
                // reset button
                BtnReset($this);
            },
        });
    }
});
//upload file_luar_jam_kerja
$(document).on("click", "#btn_file_luar_jam_kerja", function () {
    var $this = $(this);
    //Call Button Loading Function
    BtnLoading($this);
    // console.log($("#file_luar_jam_kerja").prop("files")[0])
    var file_luar_jam_kerja_lama = $("#myinputfile_luar_jam_kerja").val();
    var idIbel = $("#idIbel").val();
    var token = $("meta[name='csrf-token']").attr("content");

    //cek file kosong
    if ($("#file_luar_jam_kerja").prop("files")[0] == undefined) {
        swalError("Tidak ada file yg diupload!");
        BtnReset($this);
    } else {
        //upload file
        var file_data = $("#file_luar_jam_kerja").prop("files")[0];
        var form_data = new FormData();

        form_data.append("file_luar_jam_kerja", file_data);
        form_data.append(
            "pegawaiId",
            "{{ session()->get('user')['pegawaiId'] }}"
        );

        $.ajax({
            url: app_url + "/ibel/administrasi/upload-dokumen-persyaratan/upload_file",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: "post",
            success: function (response) {
                var data5 = JSON.parse(JSON.stringify(response));

                // jika sukses upload input nama file ke db
                if (data5.status == 1) {
                    var nmfile_luar_jam_kerja_baru = data5.file;
                    // alert(file_lama);
                    $.ajax({
                        type: "patch",
                        url:
                            app_url +
                            "/ibel/administrasi/upload-dokumen-persyaratan/inputNameFile",
                        data: {
                            _token: token,
                            file_luar_jam_kerja_baru:
                                nmfile_luar_jam_kerja_baru,
                            file_luar_jam_kerja_lama: file_luar_jam_kerja_lama,
                            idIbel: idIbel,
                        },
                        success: (response) => {
                            // jika sukses munculkan icon file pdf dan link donwload
                            if (response.status == 1) {
                                Swal.fire({
                                    position: "center",
                                    icon: "success",
                                    title: "Berhasil",
                                    text: "File berhasil diupload",
                                    showConfirmButton: false,
                                    timer: 2000,
                                }),
                                    (document.getElementById(
                                        "iconfile6"
                                    ).style.display = "");
                                var nmfile_luar_jam_kerja =
                                    app_url + "/files/" + data5.file;
                                $("#myinputfile_luar_jam_kerja")
                                    .val(data5.file)
                                    .change();
                                document.getElementById(
                                    "urlfile_luar_jam_kerja"
                                ).href = nmfile_luar_jam_kerja;
                            } else {
                                alert(response);
                                Swal.fire({
                                    position: "center",
                                    icon: "error",
                                    title: "Gagal",
                                    text: data5.message,
                                    showConfirmButton: false,
                                    timer: 2000,
                                });
                            }
                        },
                    });
                } else {
                    Swal.fire({
                        position: "center",
                        icon: "error",
                        title: "Gagal",
                        text: data5.message,
                        showConfirmButton: false,
                        timer: 2000,
                    });
                }
                // reset button
                BtnReset($this);
            },
        });
    }
});
//upload file_brosur_pmb
$(document).on("click", "#btn_file_brosur_pmb", function () {
    var $this = $(this);
    //Call Button Loading Function
    BtnLoading($this);
    // console.log($("#file_brosur_pmb").prop("files")[0])
    var file_brosur_pmb_lama = $("#myinputfile_brosur_pmb").val();
    var idIbel = $("#idIbel").val();
    var token = $("meta[name='csrf-token']").attr("content");

    //cek file kosong
    if ($("#file_brosur_pmb").prop("files")[0] == undefined) {
        swalError("Tidak ada file yg diupload!");
        BtnReset($this);
    } else {
        //upload file
        var file_data = $("#file_brosur_pmb").prop("files")[0];
        var form_data = new FormData();

        form_data.append("file_brosur_pmb", file_data);
        form_data.append(
            "pegawaiId",
            "{{ session()->get('user')['pegawaiId'] }}"
        );

        $.ajax({
            url: app_url + "/ibel/administrasi/upload-dokumen-persyaratan/upload_file",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: "post",
            success: function (response) {
                var data6 = JSON.parse(JSON.stringify(response));

                // jika sukses upload input nama file ke db
                if (data6.status == 1) {
                    var nmfile_brosur_pmb_baru = data6.file;
                    // alert(file_lama);
                    $.ajax({
                        type: "patch",
                        url:
                            app_url +
                            "/ibel/administrasi/upload-dokumen-persyaratan/inputNameFile",
                        data: {
                            _token: token,
                            file_brosur_pmb_baru: nmfile_brosur_pmb_baru,
                            file_brosur_pmb_lama: file_brosur_pmb_lama,
                            idIbel: idIbel,
                        },
                        success: (response) => {
                            // jika sukses munculkan icon file pdf dan link donwload
                            if (response.status == 1) {
                                Swal.fire({
                                    position: "center",
                                    icon: "success",
                                    title: "Berhasil",
                                    text: "File berhasil diupload",
                                    showConfirmButton: false,
                                    timer: 2000,
                                }),
                                    (document.getElementById(
                                        "iconfile7"
                                    ).style.display = "");
                                var nmfile_brosur_pmb =
                                    app_url + "/files/" + data6.file;
                                $("#myinputfile_brosur_pmb")
                                    .val(data6.file)
                                    .change();
                                document.getElementById(
                                    "urlfile_brosur_pmb"
                                ).href = nmfile_brosur_pmb;
                            } else {
                                alert(response);
                                Swal.fire({
                                    position: "center",
                                    icon: "error",
                                    title: "Gagal",
                                    text: data6.message,
                                    showConfirmButton: false,
                                    timer: 2000,
                                });
                            }
                        },
                    });
                } else {
                    Swal.fire({
                        position: "center",
                        icon: "error",
                        title: "Gagal",
                        text: data6.message,
                        showConfirmButton: false,
                        timer: 2000,
                    });
                }
                // reset button
                BtnReset($this);
            },
        });
    }
});
//upload file_sertifikat_prodi
$(document).on("click", "#btn_file_sertifikat_prodi", function () {
    var $this = $(this);
    //Call Button Loading Function
    BtnLoading($this);
    // console.log($("#file_jarak_lokasi").prop("files")[0])
    var file_lama_sert_akreditasi = $("#myinputfile_sertifikat_prodi").val();
    var idIbel = $("#idIbel").val();
    var token = $("meta[name='csrf-token']").attr("content");

    //cek file kosong
    if ($("#file_sertifikat_prodi").prop("files")[0] == undefined) {
        swalError("Tidak ada file yg diupload!");
        BtnReset($this);
    } else {
        //upload file
        var file_data = $("#file_sertifikat_prodi").prop("files")[0];
        var form_data = new FormData();

        form_data.append("file_sertifikat_prodi", file_data);
        form_data.append(
            "pegawaiId",
            "{{ session()->get('user')['pegawaiId'] }}"
        );

        $.ajax({
            url: app_url + "/ibel/administrasi/upload-dokumen-persyaratan/upload_file",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: "post",
            success: function (response) {
                var data7 = JSON.parse(JSON.stringify(response));

                // jika sukses upload input nama file ke db
                if (data7.status == 1) {
                    var nmFileProdi = data7.file;
                    // alert(file_lama);
                    $.ajax({
                        type: "patch",
                        url:
                            app_url +
                            "/ibel/administrasi/upload-dokumen-persyaratan/inputNameFile",
                        data: {
                            _token: token,
                            file_baru_sert_akreditasi: nmFileProdi,
                            file_lama_sert_akreditasi:
                                file_lama_sert_akreditasi,
                            idIbel: idIbel,
                        },
                        success: (response) => {
                            // jika sukses munculkan icon file pdf
                            if (response.status == 1) {
                                Swal.fire({
                                    position: "center",
                                    icon: "success",
                                    title: "Berhasil",
                                    text: "File berhasil diupload",
                                    showConfirmButton: false,
                                    timer: 2000,
                                }),
                                    (document.getElementById(
                                        "iconfile8"
                                    ).style.display = "");
                                var nmfile_sertifikat_prodi =
                                    app_url + "/files/" + data7.file;
                                $("#myinputfile_sertifikat_prodi")
                                    .val(data7.file)
                                    .change();
                                document.getElementById(
                                    "urlfile_sertifikat_prodi"
                                ).href = nmfile_sertifikat_prodi;
                            } else {
                                alert(response);
                                Swal.fire({
                                    position: "center",
                                    icon: "error",
                                    title: "Gagal",
                                    text: data7.message,
                                    showConfirmButton: false,
                                    timer: 2000,
                                });
                            }
                        },
                    });
                } else {
                    Swal.fire({
                        position: "center",
                        icon: "error",
                        title: "Gagal",
                        text: data7.message,
                        showConfirmButton: false,
                        timer: 2000,
                    });
                }
                // reset button
                BtnReset($this);
            },
        });
    }
});
//upload file_ijazah_kedinasan
$(document).on("click", "#btn_ijazah_kedinasan", function () {
    var $this = $(this);
    //Call Button Loading Function
    BtnLoading($this);
    // console.log($("#file_jarak_lokasi").prop("files")[0])
    var file_lama_ijazah_kedinasan = $("#myinput_ijazah_kedinasan").val();
    var idIbel = $("#idIbel").val();
    var token = $("meta[name='csrf-token']").attr("content");

    //cek file kosong
    if ($("#ijazah_kedinasan").prop("files")[0] == undefined) {
        swalError("Tidak ada file yg diupload!");
        BtnReset($this);
    } else {
        //upload file
        var file_data = $("#ijazah_kedinasan").prop("files")[0];
        var form_data = new FormData();

        form_data.append("ijazah_kedinasan", file_data);
        form_data.append(
            "pegawaiId",
            "{{ session()->get('user')['pegawaiId'] }}"
        );

        $.ajax({
            url: app_url + "/ibel/administrasi/upload-dokumen-persyaratan/upload_file",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: "post",
            success: function (response) {
                var data8 = JSON.parse(JSON.stringify(response));

                // jika sukses upload input nama file ke db
                if (data8.status == 1) {
                    var nmijazah_kedinasan = data8.file;
                    // alert(file_lama_ijazah_kedinasan);
                    $.ajax({
                        type: "patch",
                        url:
                            app_url +
                            "/ibel/administrasi/upload-dokumen-persyaratan/inputNameFile",
                        data: {
                            _token: token,
                            file_baru_ijazah_kedinasan: nmijazah_kedinasan,
                            file_lama_ijazah_kedinasan:
                                file_lama_ijazah_kedinasan,
                            idIbel: idIbel,
                        },
                        success: (response) => {
                            // jika sukses munculkan icon file pdf
                            if (response.status == 1) {
                                Swal.fire({
                                    position: "center",
                                    icon: "success",
                                    title: "Berhasil",
                                    text: "File berhasil diupload",
                                    showConfirmButton: false,
                                    timer: 2000,
                                }),
                                    (document.getElementById(
                                        "iconfile9"
                                    ).style.display = "");
                                var nmfile_ijazah_kedinasan =
                                    app_url + "/files/" + data8.file;
                                $("#myinput_ijazah_kedinasan")
                                    .val(data8.file)
                                    .change();
                                document.getElementById(
                                    "url_ijazah_kedinasan"
                                ).href = nmfile_ijazah_kedinasan;
                            } else {
                                alert(response);
                                Swal.fire({
                                    position: "center",
                                    icon: "error",
                                    title: "Gagal",
                                    text: data8.message,
                                    showConfirmButton: false,
                                    timer: 2000,
                                });
                            }
                        },
                    });
                } else {
                    Swal.fire({
                        position: "center",
                        icon: "error",
                        title: "Gagal",
                        text: data7.message,
                        showConfirmButton: false,
                        timer: 2000,
                    });
                }
                // reset button
                BtnReset($this);
            },
        });
    }
});
//upload file_transkrip_nilai
$(document).on("click", "#btn_file_transkrip_nilai", function () {
    var $this = $(this);
    //Call Button Loading Function
    BtnLoading($this);
    // console.log($("#file_transkrip_nilai").prop("files")[0])
    var file_transkrip_nilai_lama = $("#myinputfile_transkrip_nilai").val();
    var idIbel = $("#idIbel").val();
    var token = $("meta[name='csrf-token']").attr("content");

    //cek file kosong
    if ($("#file_transkrip_nilai").prop("files")[0] == undefined) {
        swalError("Tidak ada file yg diupload!");
        BtnReset($this);
    } else {
        //upload file
        var file_data = $("#file_transkrip_nilai").prop("files")[0];
        var form_data = new FormData();

        form_data.append("file_transkrip_nilai", file_data);
        form_data.append(
            "pegawaiId",
            "{{ session()->get('user')['pegawaiId'] }}"
        );

        $.ajax({
            url: app_url + "/ibel/administrasi/upload-dokumen-persyaratan/upload_file",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: "post",
            success: function (response) {
                var data9 = JSON.parse(JSON.stringify(response));

                // jika sukses upload input nama file ke db
                if (data9.status == 1) {
                    var nmfile_transkrip_nilai_baru = data9.file;
                    // alert(file_lama);
                    $.ajax({
                        type: "patch",
                        url:
                            app_url +
                            "/ibel/administrasi/upload-dokumen-persyaratan/inputNameFile",
                        data: {
                            _token: token,
                            file_transkrip_nilai_baru:
                                nmfile_transkrip_nilai_baru,
                            file_transkrip_nilai_lama:
                                file_transkrip_nilai_lama,
                            idIbel: idIbel,
                        },
                        success: (response) => {
                            // jika sukses munculkan icon file pdf dan link donwload
                            if (response.status == 1) {
                                Swal.fire({
                                    position: "center",
                                    icon: "success",
                                    title: "Berhasil",
                                    text: "File berhasil diupload",
                                    showConfirmButton: false,
                                    timer: 2000,
                                }),
                                    (document.getElementById(
                                        "iconfile10"
                                    ).style.display = "");
                                var nmfile_transkrip_nilai =
                                    app_url + "/files/" + data9.file;
                                $("#myinputfile_transkrip_nilai")
                                    .val(data9.file)
                                    .change();
                                document.getElementById(
                                    "urlfile_transkrip_nilai"
                                ).href = nmfile_transkrip_nilai;
                            } else {
                                alert(response);
                                Swal.fire({
                                    position: "center",
                                    icon: "error",
                                    title: "Gagal",
                                    text: data9.message,
                                    showConfirmButton: false,
                                    timer: 2000,
                                });
                            }
                        },
                    });
                } else {
                    Swal.fire({
                        position: "center",
                        icon: "error",
                        title: "Gagal",
                        text: data9.message,
                        showConfirmButton: false,
                        timer: 2000,
                    });
                }
                // reset button
                BtnReset($this);
            },
        });
    }
});
//upload file_sket_sehat
$(document).on("click", "#btn_file_sket_sehat", function () {
    var $this = $(this);
    //Call Button Loading Function
    BtnLoading($this);
    // console.log($("#file_sket_sehat").prop("files")[0])
    var file_sket_sehat_lama = $("#myinputfile_sket_sehat").val();
    var idIbel = $("#idIbel").val();
    var token = $("meta[name='csrf-token']").attr("content");

    //cek file kosong
    if ($("#file_sket_sehat").prop("files")[0] == undefined) {
        swalError("Tidak ada file yg diupload!");
        BtnReset($this);
    } else {
        //upload file
        var file_data = $("#file_sket_sehat").prop("files")[0];
        var form_data = new FormData();

        form_data.append("file_sket_sehat", file_data);
        form_data.append(
            "pegawaiId",
            "{{ session()->get('user')['pegawaiId'] }}"
        );

        $.ajax({
            url: app_url + "/ibel/administrasi/upload-dokumen-persyaratan/upload_file",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: "post",
            success: function (response) {
                var data10 = JSON.parse(JSON.stringify(response));

                // jika sukses upload input nama file ke db
                if (data10.status == 1) {
                    var nmfile_sket_sehat_baru = data10.file;
                    // alert(file_lama);
                    $.ajax({
                        type: "patch",
                        url:
                            app_url +
                            "/ibel/administrasi/upload-dokumen-persyaratan/inputNameFile",
                        data: {
                            _token: token,
                            file_sket_sehat_baru: nmfile_sket_sehat_baru,
                            file_sket_sehat_lama: file_sket_sehat_lama,
                            idIbel: idIbel,
                        },
                        success: (response) => {
                            // jika sukses munculkan icon file pdf dan link donwload
                            if (response.status == 1) {
                                Swal.fire({
                                    position: "center",
                                    icon: "success",
                                    title: "Berhasil",
                                    text: "File berhasil diupload",
                                    showConfirmButton: false,
                                    timer: 2000,
                                }),
                                    (document.getElementById(
                                        "iconfile11"
                                    ).style.display = "");
                                var nmfile_sket_sehat =
                                    app_url + "/files/" + data10.file;
                                $("#myinputfile_sket_sehat")
                                    .val(data10.file)
                                    .change();
                                document.getElementById(
                                    "urlfile_sket_sehat"
                                ).href = nmfile_sket_sehat;
                            } else {
                                alert(response);
                                Swal.fire({
                                    position: "center",
                                    icon: "error",
                                    title: "Gagal",
                                    text: data10.message,
                                    showConfirmButton: false,
                                    timer: 2000,
                                });
                            }
                        },
                    });
                } else {
                    Swal.fire({
                        position: "center",
                        icon: "error",
                        title: "Gagal",
                        text: data10.message,
                        showConfirmButton: false,
                        timer: 2000,
                    });
                }
                // reset button
                BtnReset($this);
            },
        });
    }
});
//upload file_sk_cpns
$(document).on("click", "#btn_file_sk_cpns", function () {
    var $this = $(this);
    //Call Button Loading Function
    BtnLoading($this);
    // console.log($("#file_sk_cpns").prop("files")[0])
    var file_sk_cpns_lama = $("#myinputfile_sk_cpns").val();
    var idIbel = $("#idIbel").val();
    var token = $("meta[name='csrf-token']").attr("content");

    //cek file kosong
    if ($("#file_sk_cpns").prop("files")[0] == undefined) {
        swalError("Tidak ada file yg diupload!");
        BtnReset($this);
    } else {
        //upload file
        var file_data = $("#file_sk_cpns").prop("files")[0];
        var form_data = new FormData();

        form_data.append("file_sk_cpns", file_data);
        form_data.append(
            "pegawaiId",
            "{{ session()->get('user')['pegawaiId'] }}"
        );

        $.ajax({
            url: app_url + "/ibel/administrasi/upload-dokumen-persyaratan/upload_file",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: "post",
            success: function (response) {
                var data11 = JSON.parse(JSON.stringify(response));

                // jika sukses upload input nama file ke db
                if (data11.status == 1) {
                    var nmfile_sk_cpns_baru = data11.file;
                    // alert(file_lama);
                    $.ajax({
                        type: "patch",
                        url:
                            app_url +
                            "/ibel/administrasi/upload-dokumen-persyaratan/inputNameFile",
                        data: {
                            _token: token,
                            file_sk_cpns_baru: nmfile_sk_cpns_baru,
                            file_sk_cpns_lama: file_sk_cpns_lama,
                            idIbel: idIbel,
                        },
                        success: (response) => {
                            // jika sukses munculkan icon file pdf dan link donwload
                            if (response.status == 1) {
                                Swal.fire({
                                    position: "center",
                                    icon: "success",
                                    title: "Berhasil",
                                    text: "File berhasil diupload",
                                    showConfirmButton: false,
                                    timer: 2000,
                                }),
                                    (document.getElementById(
                                        "iconfile12"
                                    ).style.display = "");
                                var nmfile_sk_cpns =
                                    app_url + "/files/" + data11.file;
                                $("#myinputfile_sk_cpns")
                                    .val(data11.file)
                                    .change();
                                document.getElementById(
                                    "urlfile_sk_cpns"
                                ).href = nmfile_sk_cpns;
                            } else {
                                alert(response);
                                Swal.fire({
                                    position: "center",
                                    icon: "error",
                                    title: "Gagal",
                                    text: data11.message,
                                    showConfirmButton: false,
                                    timer: 2000,
                                });
                            }
                        },
                    });
                } else {
                    Swal.fire({
                        position: "center",
                        icon: "error",
                        title: "Gagal",
                        text: data11.message,
                        showConfirmButton: false,
                        timer: 2000,
                    });
                }
                // reset button
                BtnReset($this);
            },
        });
    }
});
//upload file_sk_pns
$(document).on("click", "#btn_file_sk_pns", function () {
    var $this = $(this);
    //Call Button Loading Function
    BtnLoading($this);
    // console.log($("#file_sk_pns").prop("files")[0])
    var file_sk_pns_lama = $("#myinputfile_sk_pns").val();
    var idIbel = $("#idIbel").val();
    var token = $("meta[name='csrf-token']").attr("content");

    //cek file kosong
    if ($("#file_sk_pns").prop("files")[0] == undefined) {
        swalError("Tidak ada file yg diupload!");
        BtnReset($this);
    } else {
        //upload file
        var file_data = $("#file_sk_pns").prop("files")[0];
        var form_data = new FormData();

        form_data.append("file_sk_pns", file_data);
        form_data.append(
            "pegawaiId",
            "{{ session()->get('user')['pegawaiId'] }}"
        );

        $.ajax({
            url: app_url + "/ibel/administrasi/upload-dokumen-persyaratan/upload_file",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: "post",
            success: function (response) {
                var data12 = JSON.parse(JSON.stringify(response));

                // jika sukses upload input nama file ke db
                if (data12.status == 1) {
                    var nmfile_sk_pns_baru = data12.file;
                    // alert(file_lama);
                    $.ajax({
                        type: "patch",
                        url:
                            app_url +
                            "/ibel/administrasi/upload-dokumen-persyaratan/inputNameFile",
                        data: {
                            _token: token,
                            file_sk_pns_baru: nmfile_sk_pns_baru,
                            file_sk_pns_lama: file_sk_pns_lama,
                            idIbel: idIbel,
                        },
                        success: (response) => {
                            // jika sukses munculkan icon file pdf dan link donwload
                            if (response.status == 1) {
                                Swal.fire({
                                    position: "center",
                                    icon: "success",
                                    title: "Berhasil",
                                    text: "File berhasil diupload",
                                    showConfirmButton: false,
                                    timer: 2000,
                                }),
                                    (document.getElementById(
                                        "iconfile13"
                                    ).style.display = "");
                                var nmfile_sk_pns =
                                    app_url + "/files/" + data12.file;
                                $("#myinputfile_sk_pns")
                                    .val(data12.file)
                                    .change();
                                document.getElementById("urlfile_sk_pns").href =
                                    nmfile_sk_pns;
                            } else {
                                alert(response);
                                Swal.fire({
                                    position: "center",
                                    icon: "error",
                                    title: "Gagal",
                                    text: data12.message,
                                    showConfirmButton: false,
                                    timer: 2000,
                                });
                            }
                        },
                    });
                } else {
                    Swal.fire({
                        position: "center",
                        icon: "error",
                        title: "Gagal",
                        text: data12.message,
                        showConfirmButton: false,
                        timer: 2000,
                    });
                }
                // reset button
                BtnReset($this);
            },
        });
    }
});
//upload file_sk_pangkat
$(document).on("click", "#btn_file_sk_pangkat", function () {
    var $this = $(this);
    //Call Button Loading Function
    BtnLoading($this);
    // console.log($("#file_sk_pangkat").prop("files")[0])
    var file_sk_pangkat_lama = $("#myinputfile_sk_pangkat").val();
    var idIbel = $("#idIbel").val();
    var token = $("meta[name='csrf-token']").attr("content");

    //cek file kosong
    if ($("#file_sk_pangkat").prop("files")[0] == undefined) {
        swalError("Tidak ada file yg diupload!");
        BtnReset($this);
    } else {
        //upload file
        var file_data = $("#file_sk_pangkat").prop("files")[0];
        var form_data = new FormData();

        form_data.append("file_sk_pangkat", file_data);
        form_data.append(
            "pegawaiId",
            "{{ session()->get('user')['pegawaiId'] }}"
        );

        $.ajax({
            url: app_url + "/ibel/administrasi/upload-dokumen-persyaratan/upload_file",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: "post",
            success: function (response) {
                var data13 = JSON.parse(JSON.stringify(response));

                // jika sukses upload input nama file ke db
                if (data13.status == 1) {
                    var nmfile_sk_pangkat_baru = data13.file;
                    // alert(file_lama);
                    $.ajax({
                        type: "patch",
                        url:
                            app_url +
                            "/ibel/administrasi/upload-dokumen-persyaratan/inputNameFile",
                        data: {
                            _token: token,
                            file_sk_pangkat_baru: nmfile_sk_pangkat_baru,
                            file_sk_pangkat_lama: file_sk_pangkat_lama,
                            idIbel: idIbel,
                        },
                        success: (response) => {
                            // jika sukses munculkan icon file pdf dan link donwload
                            if (response.status == 1) {
                                Swal.fire({
                                    position: "center",
                                    icon: "success",
                                    title: "Berhasil",
                                    text: "File berhasil diupload",
                                    showConfirmButton: false,
                                    timer: 2000,
                                }),
                                    (document.getElementById(
                                        "iconfile14"
                                    ).style.display = "");
                                var nmfile_sk_pangkat =
                                    app_url + "/files/" + data13.file;
                                $("#myinputfile_sk_pangkat")
                                    .val(data13.file)
                                    .change();
                                document.getElementById(
                                    "urlfile_sk_pangkat"
                                ).href = nmfile_sk_pangkat;
                            } else {
                                alert(response);
                                Swal.fire({
                                    position: "center",
                                    icon: "error",
                                    title: "Gagal",
                                    text: data13.message,
                                    showConfirmButton: false,
                                    timer: 2000,
                                });
                            }
                        },
                    });
                } else {
                    Swal.fire({
                        position: "center",
                        icon: "error",
                        title: "Gagal",
                        text: data13.message,
                        showConfirmButton: false,
                        timer: 2000,
                    });
                }
                // reset button
                BtnReset($this);
            },
        });
    }
});
//upload file_ppkpns_satu
$(document).on("click", "#btn_file_ppkpns_satu", function () {
    var $this = $(this);
    //Call Button Loading Function
    BtnLoading($this);
    // console.log($("#file_ppkpns_satu").prop("files")[0])
    var file_ppkpns_satu_lama = $("#myinputfile_ppkpns_satu").val();
    var idIbel = $("#idIbel").val();
    var token = $("meta[name='csrf-token']").attr("content");

    //cek file kosong
    if ($("#file_ppkpns_satu").prop("files")[0] == undefined) {
        swalError("Tidak ada file yg diupload!");
        BtnReset($this);
    } else {
        //upload file
        var file_data = $("#file_ppkpns_satu").prop("files")[0];
        var form_data = new FormData();

        form_data.append("file_ppkpns_satu", file_data);
        form_data.append(
            "pegawaiId",
            "{{ session()->get('user')['pegawaiId'] }}"
        );

        $.ajax({
            url: app_url + "/ibel/administrasi/upload-dokumen-persyaratan/upload_file",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: "post",
            success: function (response) {
                var data14 = JSON.parse(JSON.stringify(response));

                // jika sukses upload input nama file ke db
                if (data14.status == 1) {
                    var nmfile_ppkpns_satu_baru = data14.file;
                    // alert(file_lama);
                    $.ajax({
                        type: "patch",
                        url:
                            app_url +
                            "/ibel/administrasi/upload-dokumen-persyaratan/inputNameFile",
                        data: {
                            _token: token,
                            file_ppkpns_satu_baru: nmfile_ppkpns_satu_baru,
                            file_ppkpns_satu_lama: file_ppkpns_satu_lama,
                            idIbel: idIbel,
                        },
                        success: (response) => {
                            // jika sukses munculkan icon file pdf dan link donwload
                            if (response.status == 1) {
                                Swal.fire({
                                    position: "center",
                                    icon: "success",
                                    title: "Berhasil",
                                    text: "File berhasil diupload",
                                    showConfirmButton: false,
                                    timer: 2000,
                                }),
                                    (document.getElementById(
                                        "iconfile15"
                                    ).style.display = "");
                                var nmfile_ppkpns_satu =
                                    app_url + "/files/" + data14.file;
                                $("#myinputfile_ppkpns_satu")
                                    .val(data14.file)
                                    .change();
                                document.getElementById(
                                    "urlfile_ppkpns_satu"
                                ).href = nmfile_ppkpns_satu;
                            } else {
                                alert(response);
                                Swal.fire({
                                    position: "center",
                                    icon: "error",
                                    title: "Gagal",
                                    text: data14.message,
                                    showConfirmButton: false,
                                    timer: 2000,
                                });
                            }
                        },
                    });
                } else {
                    Swal.fire({
                        position: "center",
                        icon: "error",
                        title: "Gagal",
                        text: data14.message,
                        showConfirmButton: false,
                        timer: 2000,
                    });
                }
                // reset button
                BtnReset($this);
            },
        });
    }
});
//upload file_ppkpns_dua
$(document).on("click", "#btn_file_ppkpns_dua", function () {
    var $this = $(this);
    //Call Button Loading Function
    BtnLoading($this);
    // console.log($("#file_ppkpns_dua").prop("files")[0])
    var file_ppkpns_dua_lama = $("#myinputfile_ppkpns_dua").val();
    var idIbel = $("#idIbel").val();
    var token = $("meta[name='csrf-token']").attr("content");

    //cek file kosong
    if ($("#file_ppkpns_dua").prop("files")[0] == undefined) {
        swalError("Tidak ada file yg diupload!");
        BtnReset($this);
    } else {
        //upload file
        var file_data = $("#file_ppkpns_dua").prop("files")[0];
        var form_data = new FormData();

        form_data.append("file_ppkpns_dua", file_data);
        form_data.append(
            "pegawaiId",
            "{{ session()->get('user')['pegawaiId'] }}"
        );

        $.ajax({
            url: app_url + "/ibel/administrasi/upload-dokumen-persyaratan/upload_file",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: "post",
            success: function (response) {
                var data15 = JSON.parse(JSON.stringify(response));

                // jika sukses upload input nama file ke db
                if (data15.status == 1) {
                    var nmfile_ppkpns_dua_baru = data15.file;
                    // alert(file_lama);
                    $.ajax({
                        type: "patch",
                        url:
                            app_url +
                            "/ibel/administrasi/upload-dokumen-persyaratan/inputNameFile",
                        data: {
                            _token: token,
                            file_ppkpns_dua_baru: nmfile_ppkpns_dua_baru,
                            file_ppkpns_dua_lama: file_ppkpns_dua_lama,
                            idIbel: idIbel,
                        },
                        success: (response) => {
                            // jika sukses munculkan icon file pdf dan link donwload
                            if (response.status == 1) {
                                Swal.fire({
                                    position: "center",
                                    icon: "success",
                                    title: "Berhasil",
                                    text: "File berhasil diupload",
                                    showConfirmButton: false,
                                    timer: 2000,
                                }),
                                    (document.getElementById(
                                        "iconfile16"
                                    ).style.display = "");
                                var nmfile_ppkpns_dua =
                                    app_url + "/files/" + data15.file;
                                $("#myinputfile_ppkpns_dua")
                                    .val(data15.file)
                                    .change();
                                document.getElementById(
                                    "urlfile_ppkpns_dua"
                                ).href = nmfile_ppkpns_dua;
                            } else {
                                alert(response);
                                Swal.fire({
                                    position: "center",
                                    icon: "error",
                                    title: "Gagal",
                                    text: data15.message,
                                    showConfirmButton: false,
                                    timer: 2000,
                                });
                            }
                        },
                    });
                } else {
                    Swal.fire({
                        position: "center",
                        icon: "error",
                        title: "Gagal",
                        text: data15.message,
                        showConfirmButton: false,
                        timer: 2000,
                    });
                }
                // reset button
                BtnReset($this);
            },
        });
    }
});
