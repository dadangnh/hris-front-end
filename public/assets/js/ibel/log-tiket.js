$(".btnTiket").click(function(){
    $("#tiket-spinner").show();
    $(".log-table-induk").hide();

    var token = $("meta[name='csrf-token']").attr("content");
    var nama = $(this).data("nama");
    var tiket = $(this).data("tiket");

    $("#labelTiket").text(tiket).change()
    $("#labelNama").text(nama).change()

    $("#logStatusModal").modal('show');

    $.ajax({
        url: app_url + '/ibel/tiket/' + tiket,
        method: "GET",
        contentType: "application/json",
        processing: true,
    }).done(function (data) {

        $(".log-table-induk").show();
        $("#tiket-spinner").hide();

        var data = (data);
        var eTable = '<thead class="fw-bolder bg-secondary fs-6">';
            eTable += '<th class="text-center ps-2 col-md-1">No</th>';
            eTable += '<th class="text-center col-md-2">Tanggal</th>';
            eTable += '<th class="text-center col-md-3">Status</th>';
            eTable += '<th class="text-center col-md">Keterangan</th>';
            eTable += '</tr>';
            eTable += '</thead>';
            eTable += '<tbody>';
        $.each(data, function (i, item) {
            eTable += '<td class="text-center ps-2 col-md-1">' + (i + 1) + '</td>';
            eTable += '<td class="text-center col-md-3">' + item.createdDate + '</td>';
            eTable += '<td class="text-center col-md-2">' + item.output + '</td>';
            eTable += '<td class="text-center">' + ((item.keterangan == null) ? '-' : item.keterangan) + '</td>';
            eTable += '</tr>';
        });
            eTable += '</tbody>';
            eTable += '</thead>';
        $(".log-table").html(eTable);
    });
});
