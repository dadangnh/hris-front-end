
// show hide keputusan
$('input[name="keputusan"]').on("click", function () {
    let opsi = $('input[name="keputusan"]:checked').val();

    // open card
    if (opsi == 1) {
        $("#jangka_waktu").attr("hidden", true);
        $("#alasan_penolakan").attr("hidden", true);

        $("#jangka_waktu").attr("hidden", false);
        $("#alasan_penolakan").attr("hidden", true);
        $("#buttonAction").attr("hidden", false);
    } else if (opsi == 0) {
        $("#jangka_waktu").attr("hidden", true);
        $("#alasan_penolakan").attr("hidden", true);

        $("#jangka_waktu").attr("hidden", true);
        $("#alasan_penolakan").attr("hidden", false);
        $("#buttonAction").attr("hidden", false);
    }
});

// button tolak permohonan
$(".btnTimPersetujuan").click(function () {
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        title: "Yakin akan mengirim data?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            window.location.href = app_url + "/ibel/persetujuan-hasil-seleksi/persetujuan/" + id + "/timSetuju"
        }
    });
});

