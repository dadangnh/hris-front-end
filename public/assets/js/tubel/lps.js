$(".btnTeliti").click(function () {
    $("#periksa_lps").modal("show");

    var smt = $(this).data("smtganjilgenap");
    if (smt == 1) {
        smt = "Ganjil";
    } else {
        smt = "Genap";
    }

    $("#id_lps").val($(this).data("id"));

    $("#perguruantinggi_teliti").val($(this).data("pt"));
    $("#semesterke_teliti").val($(this).data("smt"));
    $("#semester_teliti").val(smt);
    $("#tahun_teliti").val($(this).data("tahun"));
    $("#ipk_teliti").val($(this).data("ipk"));
    $("#jumlah_sks_teliti").val($(this).data("sks"));

    $("#get_nama").text(": " + $(this).data("nama"));
    $("#get_nip").text(": " + $(this).data("nip"));
    $("#get_pangkat").text(": " + $(this).data("pangkat"));
    $("#get_jabatan").text(": " + $(this).data("jabatan"));
    $("#get_jenjang").text(": " + $(this).data("jenjang"));
    $("#get_program_beasiswa").text(": " + $(this).data("program_beasiswa"));
    $("#get_lokasi").text(": " + $(this).data("lokasi"));
    $("#get_pt").text(": " + $(this).data("pt"));
    $("#get_prodi").text(": " + $(this).data("prodi"));
    $("#get_st_tubel").text(": " + $(this).data("st_tubel"));
    $("#get_tgl_st_tubel").text(": " + $(this).data("tgl_st_tubel"));
    $("#get_kep_pembebasan").text(": " + $(this).data("kep_pembebasan"));
    $("#get_tgl_kep_pembebasan").text(
        ": " + $(this).data("tgl_kep_pembebasan")
    );
    $("#get_tmt_kep_pembebasan").text(
        ": " + $(this).data("tmt_kep_pembebasan")
    );
    $("#get_email").text(": " + $(this).data("email"));
    $("#get_hp").text(": " + $(this).data("hp"));
    $("#get_alamat").text(": " + $(this).data("alamat"));
    $("#get_tgl_mulai_selesai").text(
        ": " +
            $(this).data("tgl_mulai") +
            " s.d. " +
            $(this).data("tgl_selesai")
    );
    $("#file_lps_teliti").attr(
        "href",
        app_url + "/files/" + $(this).data("file_lps")
    );
    $("#file_khs_teliti").attr(
        "href",
        app_url + "/files/" + $(this).data("file_khs")
    );
    $.fn.modal.Constructor.prototype._enforceFocus = function () {};
});

// button setuju lps
$(".btnSetuju").click(function () {
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        title: "Yakin akan menyetujui data?",
        text: "Data akan masuk ke tahap berikutnya!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        reverseButtons: true,
        confirmButtonText: "Setuju!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/lps/updateStatusLps",
                data: {
                    _token: token,
                    id: $("#id_lps").val(),
                    caseOutputId: "ffb7abb9-922b-455d-94cb-8290686a8f14",
                    keterangan: null,
                },
                success: function (response) {
                    if (response["status"] == 0) {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600,
                        });
                    } else {
                        location.reload();
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil di setujui",
                            showConfirmButton: false,
                            timer: 2000,
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button tolak lps
$(".btnTolak").click(function () {
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        input: "textarea",
        inputLabel: "Alasan Tolak",
        inputPlaceholder: "Masukan Alasan Penolakan",
        inputAttributes: {
            "aria-label": "Masukan Alasan Penolakan",
        },
        title: "Yakin akan menolak data?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Tolak!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/lps/updateStatusLps",
                data: {
                    _token: token,
                    caseOutputId: "27741cda-2ddf-4b98-8cfb-f9ba4ce3c9bd",
                    id: $("#id_lps").val(),
                    keterangan: result.value,
                },
                success: function (response) {
                    // $("#periksa_lps").modal('hide');
                    location.reload();

                    if (response["status"] == 0) {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600,
                        });
                    } else {
                        // $('#' + $('#id_lps').val()).remove();

                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil di tolak",
                            showConfirmButton: false,
                            timer: 1600,
                        });
                    }
                },
            });
        }
    });
});

// placeholder advance search
$(document).ready(function () {
    // jenjang
    $selectElement = $("#search_jenjang").select2({
        placeholder: "jenjang pendidikan",
        allowClear: false,
        minimumResultsForSearch: -1,
    });

    // lokasi
    $selectElement = $("#search_lokasi").select2({
        placeholder: "lokasi pendidikan",
        allowClear: false,
        minimumResultsForSearch: -1,
    });
});

// format tahun
$("#search_tahun").datepicker({
    format: "yyyy",
    weekStart: 1,
    orientation: "bottom",
    keyboardNavigation: false,
    viewMode: "years",
    minViewMode: "years",
});
