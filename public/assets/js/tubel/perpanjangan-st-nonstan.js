$(".datepick").flatpickr();

// button periksa perpanjangan st
$(".btnPeriksaPerpanjanganSt").click(function () {
    $("#periksa_perpanjangan_st").modal("show");

    $("#id_permohonan_aktif").val($(this).data("id_permohonan_aktif"));

    $("#get_nama_setujui").text(": " + $(this).data("nama"));
    $("#get_nip_setujui").text(": " + $(this).data("nip"));
    $("#get_pangkat_setujui").text(": " + $(this).data("pangkat"));
    $("#get_jabatan_setujui").text(": " + $(this).data("jabatan"));
    $("#get_jenjang_setujui").text(": " + $(this).data("jenjang"));
    $("#get_program_beasiswa_setujui").text(
        ": " + $(this).data("program_beasiswa")
    );
    $("#get_lokasi_setujui").text(": " + $(this).data("lokasi"));
    // $('#get_pt_setujui').text(': ' + $(this).data("pt")
    // $('#get_prodi_setujui').text(': ' + $(this).data("prodi")
    $("#get_st_tubel_setujui").text(": " + $(this).data("st_tubel"));
    $("#get_tgl_st_tubel_setujui").text(": " + $(this).data("tgl_st_tubel"));
    $("#get_tgl_mulai_selesai_setujui").text(
        ": " + $(this).data("tgl_mulai") + " - " + $(this).data("tgl_selesai")
    );
    $("#get_kep_pembebasan_setujui").text(
        ": " + $(this).data("kep_pembebasan")
    );
    $("#get_tgl_kep_pembebasan_setujui").text(
        ": " + $(this).data("tgl_kep_pembebasan")
    );
    $("#get_tmt_kep_pembebasan_setujui").text(
        ": " + $(this).data("tmt_kep_pembebasan")
    );
    $("#get_email_setujui").text(": " + $(this).data("email"));
    $("#get_hp_setujui").text(": " + $(this).data("hp"));
    $("#get_alamat_setujui").text(": " + $(this).data("alamat"));

    $("#alasan_perpanjangan_st_periksa").val(
        $(this).data("alasan_perpanjangan_st_periksa")
    );
    $("#tahapan_studi_periksa").val($(this).data("tahapan_studi_periksa"));
    $("#tgl_selesai_perpanjangan_periksa").val(
        $(this).data("tgl_selesai_perpanjangan_periksa")
    );
    $("#st_awal_periksa").val($(this).data("st_awal_periksa"));
    $("#file_st_awal_periksa").attr(
        "href",
        app_url + "/files/" + $(this).data("file_st_awal_periksa")
    );
    $("#surat_persetujuan_kampus_periksa").val(
        $(this).data("surat_persetujuan_kampus_periksa")
    );
    $("#tgl_surat_persetujuan_kampus_periksa").val(
        $(this).data("tgl_surat_persetujuan_kampus_periksa")
    );
    $("#file_surat_persetujuan_kampus_periksa").attr(
        "href",
        app_url +
            "/files/" +
            $(this).data("file_surat_persetujuan_kampus_periksa")
    );
    $("#surat_persetujuan_sponsor_periksa").val(
        $(this).data("surat_persetujuan_sponsor_periksa")
    );
    $("#tgl_surat_persetujuan_sponsor_periksa").val(
        $(this).data("tgl_surat_persetujuan_sponsor_periksa")
    );
    $("#file_surat_persetujuan_sponsor_periksa").attr(
        "href",
        app_url +
            "/files/" +
            $(this).data("file_surat_persetujuan_sponsor_periksa")
    );
    $.fn.modal.Constructor.prototype._enforceFocus = function () {};
});

// button setuju periksa perpanjangan st
$(".btnSetujuPerpanjanganSt").click(function () {
    var id = $("#id_permohonan_aktif").val();
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        title: "Yakin akan menyetujui data?",
        text: "Data akan masuk ke tahap berikutnya!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Setuju!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/perpanjangan-st-nonstan/setuju",
                data: {
                    _token: token,
                    id: id,
                },
                success: function (response) {
                    // console.log(response);
                    location.reload();

                    if (response["status"] === 0) {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600,
                        });
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil di setujui",
                            showConfirmButton: false,
                            timer: 1600,
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button kembalikan perpanjangan st
$(".btnKembalikanPerpanjanganSt").click(function () {
    var id = $("#id_permohonan_aktif").val();
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        input: "textarea",
        inputLabel: "Alasan Kembalikan",
        inputPlaceholder: "Masukan Alasan Kembalikan",
        inputAttributes: {
            "aria-label": "Masukan Alasan Kembalikan",
        },
        title: "Yakin akan kembalikan data?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#FACA3E",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Kembalikan!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/perpanjangan-st-nonstan/kembalikan",
                data: {
                    _token: token,
                    id: id,
                    keterangan: result.value,
                },
                success: function (response) {
                    if (response["status"] == 0) {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600,
                        });
                    } else {
                        location.reload();
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil di tolak",
                            showConfirmButton: false,
                            timer: 1600,
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button tolak perpanjangan st
$(".btnTolakPerpanjanganSt").click(function () {
    var id = $("#id_permohonan_aktif").val();

    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        input: "textarea",
        inputLabel: "Alasan Tolak",
        inputPlaceholder: "Masukan Alasan Penolakan",
        inputAttributes: {
            "aria-label": "Masukan Alasan Penolakan",
        },
        title: "Yakin akan menolak data?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Tolak!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/perpanjangan-st-nonstan/tolak",
                data: {
                    _token: token,
                    id: id,
                    keterangan: result.value,
                },
                success: function (response) {
                    if (response["status"] == 0) {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600,
                        });
                    } else {
                        location.reload();
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil di tolak",
                            showConfirmButton: false,
                            timer: 1600,
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button lihat perpanjangan st
$(".btnLihatPerpanjanganSt").click(function () {
    $("#lihat_perpanjangan_st").modal("show");

    $("#id_permohonan_aktif").val($(this).data("id_permohonan_aktif"));

    $("#nama_lihat").text(": " + $(this).data("nama"));
    $("#nip_lihat").text(": " + $(this).data("nip"));
    $("#pangkat_lihat").text(": " + $(this).data("pangkat"));
    $("#jabatan_lihat").text(": " + $(this).data("jabatan"));
    $("#jenjang_lihat").text(": " + $(this).data("jenjang"));
    $("#program_beasiswa_lihat").text(": " + $(this).data("program_beasiswa"));
    $("#lokasi_lihat").text(": " + $(this).data("lokasi"));
    // $('#pt_lihat').text(': ' + $(this).data("pt")
    // $('#prodi_lihat').text(': ' + $(this).data("prodi")
    $("#st_tubel_lihat").text(": " + $(this).data("st_tubel"));
    $("#tgl_st_tubel_lihat").text(": " + $(this).data("tgl_st_tubel"));
    $("#tgl_mulai_selesai_lihat").text(
        ": " + $(this).data("tgl_mulai") + " - " + $(this).data("tgl_selesai")
    );
    $("#kep_pembebasan_lihat").text(": " + $(this).data("kep_pembebasan"));
    $("#tgl_kep_pembebasan_lihat").text(
        ": " + $(this).data("tgl_kep_pembebasan")
    );
    $("#tmt_kep_pembebasan_lihat").text(
        ": " + $(this).data("tmt_kep_pembebasan")
    );
    $("#email_lihat").text(": " + $(this).data("email"));
    $("#hp_lihat").text(": " + $(this).data("hp"));
    $("#alamat_lihat").text(": " + $(this).data("alamat"));

    $("#alasan_perpanjangan_st_lihat").val(
        $(this).data("alasan_perpanjangan_st_lihat")
    );
    $("#tahapan_studi_lihat").val($(this).data("tahapan_studi_lihat"));
    $("#tgl_selesai_perpanjangan_lihat").val(
        $(this).data("tgl_selesai_perpanjangan_lihat")
    );
    $("#st_awal_lihat").val($(this).data("st_awal_lihat"));
    $("#file_st_awal_lihat").attr(
        "href",
        app_url + "/files/" + $(this).data("file_st_awal_lihat")
    );
    $("#surat_persetujuan_kampus_lihat").val(
        $(this).data("surat_persetujuan_kampus_lihat")
    );
    $("#tgl_surat_persetujuan_kampus_lihat").val(
        $(this).data("tgl_surat_persetujuan_kampus_lihat")
    );
    $("#file_surat_persetujuan_kampus_lihat").attr(
        "href",
        app_url +
            "/files/" +
            $(this).data("file_surat_persetujuan_kampus_lihat")
    );
    $("#surat_persetujuan_sponsor_lihat").val(
        $(this).data("surat_persetujuan_sponsor_lihat")
    );
    $("#tgl_surat_persetujuan_sponsor_lihat").val(
        $(this).data("tgl_surat_persetujuan_sponsor_lihat")
    );
    $("#file_surat_persetujuan_sponsor_lihat").attr(
        "href",
        app_url +
            "/files/" +
            $(this).data("file_surat_persetujuan_sponsor_lihat")
    );

    $("#nomor_st_perpanjangan_lihat").val(
        $(this).data("nomor_st_perpanjangan_lihat")
    );
    $("#tgl_st_perpanjangan_lihat").val(
        $(this).data("tgl_st_perpanjangan_lihat")
    );
    $("#tgl_mulai_st_perpanjangan_lihat").val(
        $(this).data("tgl_mulai_st_perpanjangan_lihat")
    );
    $("#tgl_selesai_st_perpanjangan_lihat").val(
        $(this).data("tgl_selesai_st_perpanjangan_lihat")
    );
    $("#file_selesai_st_perpanjangan_lihat").attr(
        "href",
        app_url +
            "/files/" +
            $(this).data("file_surat_persetujuan_sponsor_lihat")
    );
    $.fn.modal.Constructor.prototype._enforceFocus = function () {};
});

// button lihat perpanjangan st
$(".btnUploadStPerpanjanganSt").click(function () {
    $("#upload_st_perpanjangan_st").modal("show");

    $("#id_permohonan_aktif_upload_st").val(
        $(this).data("id_permohonan_aktif")
    );

    $("#nama_upload_st").text(": " + $(this).data("nama"));
    $("#nip_upload_st").text(": " + $(this).data("nip"));
    $("#pangkat_upload_st").text(": " + $(this).data("pangkat"));
    $("#jabatan_upload_st").text(": " + $(this).data("jabatan"));
    $("#jenjang_upload_st").text(": " + $(this).data("jenjang"));
    $("#program_beasiswa_upload_st").text(
        ": " + $(this).data("program_beasiswa")
    );
    $("#lokasi_upload_st").text(": " + $(this).data("lokasi"));
    // $('#pt_upload_st').text(': ' + $(this).data("pt")
    // $('#prodi_upload_st').text(': ' + $(this).data("prodi")
    $("#st_tubel_upload_st").text(": " + $(this).data("st_tubel"));
    $("#tgl_st_tubel_upload_st").text(": " + $(this).data("tgl_st_tubel"));
    $("#tgl_mulai_selesai_upload_st").text(
        ": " + $(this).data("tgl_mulai") + " - " + $(this).data("tgl_selesai")
    );
    $("#kep_pembebasan_upload_st").text(": " + $(this).data("kep_pembebasan"));
    $("#tgl_kep_pembebasan_upload_st").text(
        ": " + $(this).data("tgl_kep_pembebasan")
    );
    $("#tmt_kep_pembebasan_upload_st").text(
        ": " + $(this).data("tmt_kep_pembebasan")
    );
    $("#email_upload_st").text(": " + $(this).data("email"));
    $("#hp_upload_st").text(": " + $(this).data("hp"));
    $("#alamat_upload_st").text(": " + $(this).data("alamat"));

    $("#alasan_perpanjangan_st_periksa").val(
        $(this).data("alasan_perpanjangan_st_periksa")
    );
    $("#tahapan_studi_periksa").val($(this).data("tahapan_studi_periksa"));
    $("#tgl_selesai_perpanjangan_periksa").val(
        $(this).data("tgl_selesai_perpanjangan_periksa")
    );
    $("#st_awal_periksa").val($(this).data("st_awal_periksa"));
    $("#file_st_awal_periksa").attr(
        "href",
        app_url + "/files/" + $(this).data("file_st_awal_periksa")
    );
    $("#surat_persetujuan_kampus_periksa").val(
        $(this).data("surat_persetujuan_kampus_periksa")
    );
    $("#tgl_surat_persetujuan_kampus_periksa").val(
        $(this).data("tgl_surat_persetujuan_kampus_periksa")
    );
    $("#file_surat_persetujuan_kampus_periksa").attr(
        "href",
        app_url +
            "/files/" +
            $(this).data("file_surat_persetujuan_kampus_periksa")
    );
    $("#surat_persetujuan_sponsor_periksa").val(
        $(this).data("surat_persetujuan_sponsor_periksa")
    );
    $("#tgl_surat_persetujuan_sponsor_periksa").val(
        $(this).data("tgl_surat_persetujuan_sponsor_periksa")
    );
    $("#file_surat_persetujuan_sponsor_periksa").attr(
        "href",
        app_url +
            "/files/" +
            $(this).data("file_surat_persetujuan_sponsor_periksa")
    );
    $.fn.modal.Constructor.prototype._enforceFocus = function () {};
});

// placeholder advance search
$(window).on("load", function () {
    // jenjang
    $selectElement = $("#search_jenjang").select2({
        placeholder: "jenjang pendidikan",
        allowClear: true,
        minimumResultsForSearch: -1,
    });

    // lokasi
    $selectElement = $("#search_lokasi").select2({
        placeholder: "lokasi pendidikan",
        allowClear: true,
        minimumResultsForSearch: -1,
    });
});

// date format
$(".date-search").flatpickr({
    dateFormat: "d-m-Y",
});
