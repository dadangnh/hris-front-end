// asset
$(".datepick").flatpickr();

// placeholder advance search
$(document).ready(function () {
    // jenjang
    $selectElement = $("#search_jenjang").select2({
        placeholder: "jenjang pendidikan",
        allowClear: false,
        minimumResultsForSearch: -1,
    });

    // lokasi
    $selectElement = $("#search_lokasi").select2({
        placeholder: "lokasi pendidikan",
        allowClear: false,
        minimumResultsForSearch: -1,
    });

    $selectElement = $("#topik").select2({
        placeholder: "Pilih topik riset",
        allowClear: false,
        minimumResultsForSearch: -1,
    });

    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    // get option topik riset
    $("#topik_search").select2({
        placeholder: "Pilih topik riset",
        minimumInputLength: 3,
        ajax: {
            url: app_url + "/tubel/riset/get-topik-riset",
            dataType: "json",
            type: "post",
            delay: 300,
            data: function (params) {
                return {
                    search: params.term,
                };
            },
            processResults: function (response) {
                return {
                    results: response,
                };
            },
        },
    });
});

// format tahun
$("#search_tahun").datepicker({
    format: "yyyy",
    weekStart: 1,
    orientation: "bottom",
    keyboardNavigation: false,
    viewMode: "years",
    minViewMode: "years",
});

// button hapus lampiran (all)
$(".btnDelete").click(function () {
    var token = $("meta[name='csrf-token']").attr("content");

    // get element check
    var inputElements = document.getElementsByClassName("dataCheck");

    // looping data, get data check
    var id_usulan_topik = [];
    for (var i = 0; inputElements[i]; i++) {
        if (inputElements[i].checked) {
            id_usulan_topik.push(inputElements[i].value);
        }
    }

    if (id_usulan_topik != "") {
        Swal.fire({
            title: "Yakin akan menghapus data?",
            // text: "Data tidak dapat dikembalikan!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#C5584B",
            cancelButtonColor: "#B2B2B2",
            confirmButtonText: "Hapus!",
            cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
            reverseButtons: true,
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "delete",
                    url: app_url + "/tubel/riset/kelola-referensi",
                    data: {
                        _token: token,
                        id_usulan_topik: id_usulan_topik,
                    },
                    success: function (response) {
                        // delete row
                        for (var i = 0; i < id_usulan_topik.length; i++) {
                            $("#topikRiset-" + id_usulan_topik[i]).remove();
                        }

                        // swal
                        if (response.status == 1) {
                            Swal.fire({
                                position: "center",
                                icon: "success",
                                title: "Berhasil",
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 1800,
                            });
                        } else {
                            Swal.fire({
                                position: "center",
                                icon: "error",
                                title: "Error!",
                                text: response.data.message,
                                showConfirmButton: false,
                                timer: 1800,
                            });
                        }
                    },
                    error: function (err) {
                        console.log(err);
                    },
                });
            }
        });
    } else {
        Swal.fire({
            position: "center",
            icon: "error",
            title: "Error!",
            text: "Tidak ada data yang dipilih",
            showConfirmButton: false,
            timer: 1600,
        });
    }
});

// button preview file csv
$(".btnLoadCsv").click(function () {
    $("#load_csv_modal").modal("show");
    $(".btnAction").hide();
    $(".referensiRisetTableInduk").hide();
    $(".btnImportCsv").hide();
});

// button upload file csv
$(document).ready(function () {
    $("#btnPreviewCsv").click(function (e) {
        // prevent default
        e.preventDefault();

        // loading button spinner
        BtnLoading($(".btnPreviewCsv"));

        // loading spinner
        $("#tiket-spinner").show();

        // set token
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });

        const file_csv = $("#file_csv").prop("files")[0];

        if (file_csv != undefined) {
            let formData = new FormData();
            formData.append("file_csv", file_csv);

            $.ajax({
                type: "post",
                url: app_url + "/tubel/riset/kelola-referensi/preview",
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                success: function (response) {
                    if (response.status == 0) {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Error!",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 2000,
                        });
                    } else {
                        var data = response.data;
                        var eTable =
                            '<table class="table table-striped align-middle">';
                        eTable = '<thead class="fw-bolder bg-secondary fs-6">';
                        eTable +=
                            '<th class="text-center ps-2 col-lg">Topik</th>';
                        eTable +=
                            '<th class="text-center col-lg-3">Nomor Pengumuman</th>';
                        eTable +=
                            '<th class="text-center col-lg-2 pe-2">Tgl Pengumuman</th>';
                        eTable += "</tr>";
                        eTable += "</thead>";
                        eTable += "<tbody>";
                        $.each(data, function (i, item) {
                            eTable +=
                                '<td class="ps-6">' + item.topik + "</td>";
                            eTable +=
                                '<td class="text-center">' +
                                item.nomor_pengumuman +
                                "</td>";
                            eTable +=
                                '<td class="text-center">' +
                                item.tanggal_pengumuman +
                                "</td>";
                            eTable += "</tr>";
                        });
                        eTable += "</tbody>";
                        eTable += "</thead>";
                        eTable += "</table>";
                        $(".referensiRisetTable").html(eTable);

                        $(".labelPreview").show();
                        $(".referensiRisetTableInduk").show();
                        $(".btnAction").show();
                    }

                    $("#tiket-spinner").hide();
                    BtnReset($(".btnPreviewCsv"));
                },
            });
        } else {
            Swal.fire({
                position: "center",
                icon: "error",
                title: "Error!",
                text: "Tidak ada file yg diupload",
                showConfirmButton: false,
                timer: 2000,
            });
            BtnReset($(".btnPreviewCsv"));
        }
    });
});

// button periksa usulan topik riset
$(".btnPeriksaUsulanRiset").click(function () {
    $("#periksa_usulan_topik_riset_modal").modal("show");

    $("#id_usulan_riset").val($(this).data("id_usulan_riset_periksa"));
    $("#nama_periksa").text(": " + $(this).data("nama"));
    $("#nip_periksa").text(": " + $(this).data("nip"));
    $("#pangkat_periksa").text(": " + $(this).data("pangkat"));
    $("#jabatan_periksa").text(": " + $(this).data("jabatan"));
    $("#jenjang_periksa").text(": " + $(this).data("jenjang"));
    $("#program_beasiswa_periksa").text(
        ": " + $(this).data("program_beasiswa")
    );
    $("#lokasi_periksa").text(": " + $(this).data("lokasi"));
    $("#pt_periksa").text(": " + $(this).data("pt"));
    $("#prodi_periksa").text(": " + $(this).data("prodi"));
    $("#st_tubel_periksa").text(": " + $(this).data("st_tubel"));
    $("#tgl_mulai_selesai_periksa").text(
        ": " +
            $(this).data("tgl_mulai") +
            " s.d. " +
            $(this).data("tgl_selesai")
    );
    $("#tgl_st_tubel_periksa").text(": " + $(this).data("tgl_st_tubel"));
    $("#kep_pembebasan_periksa").text(": " + $(this).data("kep_pembebasan"));
    $("#tgl_kep_pembebasan_periksa").text(
        ": " + $(this).data("tgl_kep_pembebasan")
    );
    $("#tmt_kep_pembebasan_periksa").text(
        ": " + $(this).data("tmt_kep_pembebasan")
    );
    $("#email_periksa").text(": " + $(this).data("email"));
    $("#hp_periksa").text(": " + $(this).data("hp"));
    $("#alamat_periksa").text(": " + $(this).data("alamat"));

    $("#id_usulan_riset_periksa").val($(this).data("id_usulan_riset_periksa"));
    $("#topik_riset_periksa").val($(this).data("topik_riset_periksa"));
    $("#judul_riset_periksa").val($(this).data("judul_riset_periksa"));
    $("#dokumen_proposal_periksa").attr(
        "href",
        app_url + "/files/" + $(this).data("file_proposal_periksa")
    );
    $("#tgl_persetujuan_pt_periksa").val(
        $(this).data("tgl_persetujuan_pt_periksa")
    );
    $("#dokumen_persetujuan_periksa").attr(
        "href",
        app_url + "/files/" + $(this).data("dokumen_persetujuan_periksa")
    );
    $.fn.modal.Constructor.prototype._enforceFocus = function () {};
});

// button lihat usulan topik riset
$(".btnLihatUsulanRiset").click(function () {
    $("#lihat_usulan_topik_riset_modal").modal("show");

    $("#nama_lihat").text(": " + $(this).data("nama"));
    $("#nip_lihat").text(": " + $(this).data("nip"));
    $("#pangkat_lihat").text(": " + $(this).data("pangkat"));
    $("#jabatan_lihat").text(": " + $(this).data("jabatan"));
    $("#jenjang_lihat").text(": " + $(this).data("jenjang"));
    $("#program_beasiswa_lihat").text(": " + $(this).data("program_beasiswa"));
    $("#lokasi_lihat").text(": " + $(this).data("lokasi"));
    $("#pt_lihat").text(": " + $(this).data("pt"));
    $("#prodi_lihat").text(": " + $(this).data("prodi"));
    $("#st_tubel_lihat").text(": " + $(this).data("st_tubel"));
    $("#tgl_mulai_selesai_lihat").text(
        ": " +
            $(this).data("tgl_mulai") +
            " s.d. " +
            $(this).data("tgl_selesai")
    );
    $("#tgl_st_tubel_lihat").text(": " + $(this).data("tgl_st_tubel"));
    $("#kep_pembebasan_lihat").text(": " + $(this).data("kep_pembebasan"));
    $("#tgl_kep_pembebasan_lihat").text(
        ": " + $(this).data("tgl_kep_pembebasan")
    );
    $("#tmt_kep_pembebasan_lihat").text(
        ": " + $(this).data("tmt_kep_pembebasan")
    );
    $("#email_lihat").text(": " + $(this).data("email"));
    $("#hp_lihat").text(": " + $(this).data("hp"));
    $("#alamat_lihat").text(": " + $(this).data("alamat"));

    $("#id_usulan_riset_lihat").val($(this).data("id_usulan_riset"));
    $("#topik_riset_lihat").val($(this).data("topik_riset"));
    $("#judul_riset_lihat").val($(this).data("judul_riset"));
    $("#dokumen_proposal_lihat").attr(
        "href",
        app_url + "/files/" + $(this).data("file_proposal")
    );

    $("#tgl_persetujuan_pt_lihat").val($(this).data("tgl_persetujuan_pt"));
    $("#dokumen_persetujuan_pt_lihat").attr(
        "href",
        app_url + "/files/" + $(this).data("file_persetujuan_pt")
    );
    $("#nomor_nd_persetujuan_lihat").val($(this).data("nomor_nd_persetujuan"));
    $("#tgl_nd_persetujuan_lihat").val($(this).data("tgl_nd_persetujuan"));
    $("#dokumen_persetujuan_nd_lihat").attr(
        "href",
        app_url + "/files/" + $(this).data("dokumen_persetujuan_nd")
    );
    $.fn.modal.Constructor.prototype._enforceFocus = function () {};
});

// button tolak usulan topik riset
$(".btnTolakUsulanTopikRiset").click(function () {
    var id_usulan_topik_riset = $("#id_usulan_riset").val();
    var token = $("meta[name='csrf-token']").attr("content");
    var act = "tolak";

    Swal.fire({
        input: "textarea",
        inputLabel: "Alasan Tolak",
        inputPlaceholder: "Masukan Alasan Penolakan",
        inputAttributes: {
            "aria-label": "Masukan Alasan Penolakan",
        },
        title: "Yakin akan menolak data?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Tolak!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/riset/usulan-topik-riset",
                data: {
                    _token: token,
                    id: id_usulan_topik_riset,
                    action: act,
                    keterangan: result.value,
                },
                success: function (response) {
                    if (response.status == 1) {
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil dihapus",
                            showConfirmButton: false,
                            timer: 1800,
                        });
                        $("#topikRiset-" + id_usulan_topik_riset).remove();
                        $("#periksa_usulan_topik_riset_modal").modal("hide");
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Error!",
                            text: "data gagal dihapus",
                            showConfirmButton: false,
                            timer: 1800,
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button kembalikan usulan topik riset
$(".btnKembalikanUsulanTopikRiset").click(function () {
    var id_usulan_topik_riset = $("#id_usulan_riset").val();
    var token = $("meta[name='csrf-token']").attr("content");
    var act = "kembalikan";

    Swal.fire({
        input: "textarea",
        inputLabel: "Alasan Kembalikan",
        inputPlaceholder: "Masukan Alasan Kembalikan",
        inputAttributes: {
            "aria-label": "Masukan Alasan Kembalikan",
        },
        title: "Yakin akan kembalikan data?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#FACA3E",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Kembalikan!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/riset/usulan-topik-riset/",
                data: {
                    _token: token,
                    id: id_usulan_topik_riset,
                    action: act,
                    keterangan: result.value,
                },
                success: function (response) {
                    if (response.status == 1) {
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil dikembalikan",
                            showConfirmButton: false,
                            timer: 1800,
                        });
                        $("#topikRiset-" + id_usulan_topik_riset).remove();
                        $("#periksa_usulan_topik_riset_modal").modal("hide");
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Error!",
                            text: "data gagal dikembalikan",
                            showConfirmButton: false,
                            timer: 1800,
                        });
                    }
                },
            });
        }
    });
});

// button setuju usulan topik riset
$(".btnsSetujuiUsulanTopikRiset").click(function () {
    var id_usulan_topik_riset = $("#id_usulan_riset").val();
    var token = $("meta[name='csrf-token']").attr("content");
    var act = "setuju";

    Swal.fire({
        title: "Yakin akan menyetujui data?",
        text: "Data akan masuk ke tahap berikutnya!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Setuju!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/riset/usulan-topik-riset",
                data: {
                    _token: token,
                    id: id_usulan_topik_riset,
                    action: act,
                    keterangan: null,
                },
                success: function (response) {
                    console.log(response);
                    if (response.status == 1) {
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil disetujui",
                            showConfirmButton: false,
                            timer: 1800,
                        });
                        $("#topikRiset-" + id_usulan_topik_riset).remove();
                        $("#periksa_usulan_topik_riset_modal").modal("hide");
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Error!",
                            text: "data gagal disetujui",
                            showConfirmButton: false,
                            timer: 1800,
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button sahkan usulan topik riset
$(".btnSahkanUsulanTopikRiset").click(function () {
    var id_usulan_topik_riset = $("#id_usulan_riset").val();
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        title: "Yakin akan mengesahkan data?",
        text: "Data akan masuk ke tahap berikutnya!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Sahkan!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/riset/usulan-topik-riset/sahkan",
                data: {
                    _token: token,
                    id: id_usulan_topik_riset,
                    keterangan: null,
                },
                success: function (response) {
                    if (response.status == 1) {
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil disahkan",
                            showConfirmButton: false,
                            timer: 1800,
                        });
                        $("#topikRiset-" + id_usulan_topik_riset).remove();
                        $("#periksa_usulan_topik_riset_modal").modal("hide");
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Error!",
                            text: "data gagal disahkan",
                            showConfirmButton: false,
                            timer: 1800,
                        });
                    }
                },
            });
        }
    });
});

// button import csv
$("#btnImportCsv").submit(function (e) {
    e.preventDefault();
    BtnLoading($(".btnPreviewCsv"));

    $("#tiket-spinner").show();

    var formData = new FormData(this);

    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    $.ajax({
        type: "post",
        url: app_url + "/tubel/riset/kelola-referensi-preview",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: (response) => {
            if (response.status == 0) {
                Swal.fire({
                    position: "center",
                    icon: "error",
                    title: "Error!",
                    text: response.message,
                    showConfirmButton: false,
                    timer: 2000,
                });
            } else {
                var data = response.data;
                var eTable = '<table class="table table-striped align-middle">';
                eTable = '<thead class="fw-bolder bg-secondary fs-6">';
                eTable += '<th class="text-center ps-2 col-lg">Topik</th>';
                eTable +=
                    '<th class="text-center col-lg-3">Nomor Pengumuman</th>';
                eTable +=
                    '<th class="text-center col-lg-2">Tanggal Pengumuman</th>';
                eTable += "</tr>";
                eTable += "</thead>";
                eTable += "<tbody>";
                $.each(data, function (i, item) {
                    eTable +=
                        '<td class="text-center ps-2 col-md-1">' +
                        item.topik +
                        "</td>";
                    eTable +=
                        '<td class="text-center col-md-3">' +
                        item.no_pengumuman +
                        "</td>";
                    eTable +=
                        '<td class="text-center col-md-2">' +
                        item.tgl_pengumuman +
                        "</td>";
                    eTable += "</tr>";
                });
                eTable += "</tbody>";
                eTable += "</thead>";
                eTable += "</table>";
                $(".referensiRisetTable").html(eTable);

                $(".referensiRisetTableInduk").show();
                $(".btnAction").show();

                $(".btnImportCsv").show();
            }

            $("#tiket-spinner").hide();
            $(".btnPreviewCsv").hide();
            BtnReset($(".btnPreviewCsv"));
        },
        error: function (data) {
            BtnReset($(".btnPreviewCsv"));
        },
    });
});

// spinner / loading button
function BtnLoading(elem) {
    $(elem).attr("data-original-text", $(elem).html());
    $(elem).prop("disabled", true);
    $(elem).html('<i class="spinner-border spinner-border-sm"></i>  Loading');
}

// spinner off / reset button
function BtnReset(elem) {
    $(elem).prop("disabled", false);
    $(elem).html($(elem).attr("data-original-text"));
}
