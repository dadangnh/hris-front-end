$(".datepick").flatpickr();
$(".date-search").flatpickr({
    dateFormat: "d-m-Y",
});

// button periksa
$(".btnPeriksa").click(function () {
    $("#periksa_lapor_diri").modal("show");
    $(".form_opsi").attr("hidden", true);

    $("#id_lapor_diri").val($(this).data("id")).change();
    $(".btnTolak").prop("data-id", $(this).data("id"));
    $(".btnSetuju").prop("data-id", $(this).data("id"));

    // switch
    switch ($(this).data("status")) {
        case "Lulus":
            $(".form_hasil_studi_lulus").attr("hidden", false);
            $("#id_hasil_studi").val($(this).data("id"));
            let pt = $(this).data("count_pt");

            switch (pt) {
                case 1:
                    $(".data_pt_1").attr("hidden", false);
                    break;

                case 2:
                    $(".data_pt_1").attr("hidden", false);
                    $(".data_pt_2").attr("hidden", false);
                    break;

                case 3:
                    $(".data_pt_1").attr("hidden", false);
                    $(".data_pt_2").attr("hidden", false);
                    $(".data_pt_3").attr("hidden", false);
                    break;
            }

            $("#tgl_kelulusan_l_periksa").val($(this).data("tgl_kelulusan"));
            $("#opsi_dokumen_penelitian_l_periksa").val(
                $(this).data("tipe_dokumen_penelitian")
            );
            $("#file_penelitian_l_periksa").attr(
                "href",
                app_url +
                    "/files/" +
                    $(this).data("file_draft_dokumen_penelitian")
            );

            $("#file_lps_l_periksa").attr(
                "href",
                app_url + "/files/" + $(this).data("file_lps")
            );

            // loop data perguruan tinggi
            for (let i = 1; i <= pt; i++) {
                $("#nama_pt_" + i)
                    .text($(this).data("nama_pt_" + i))
                    .change();

                $("#ipk_periksa_ijazah_" + i)
                    .val($(this).data("ipk_pt_" + i))
                    .change();

                $("#nomor_skl_periksa_ijazah_" + i)
                    .val($(this).data("no_skl_pt_" + i))
                    .change();

                $("#tgl_skl_periksa_ijazah_" + i)
                    .val($(this).data("tgl_skl_pt_" + i))
                    .change();

                if ($(this).data("file_skl_pt_" + i) == "-") {
                    $(".file_skl_" + i).attr("hidden", true);
                    $(".msg_file_skl_" + i).attr("hidden", false);
                } else {
                    $("#file_skl_" + i).attr(
                        "href",
                        app_url + "/files/" + $(this).data("file_skl_pt_" + i)
                    );
                }

                $("#nomor_ijazah_periksa_ijazah_" + i)
                    .val($(this).data("no_ijazah_pt_" + i))
                    .change();

                $("#tgl_ijazah_periksa_ijazah_" + i)
                    .val($(this).data("tgl_ijazah_pt_" + i))
                    .change();

                if ($(this).data("file_skl_pt_" + i) == "-") {
                    $(".file_ijazah_" + i).attr("hidden", true);
                    $(".msg_file_ijazah_" + i).attr("hidden", false);
                } else {
                    $("#file_ijazah_" + i).attr(
                        "href",
                        app_url +
                            "/files/" +
                            $(this).data("file_ijazah_pt_" + i)
                    );
                }

                $("#nomor_transkrip_periksa_ijazah_" + i)
                    .val($(this).data("no_transkrip_pt_" + i))
                    .change();

                $("#tgl_transkrip_periksa_ijazah_" + i)
                    .val($(this).data("tgl_transkrip_pt_" + i))
                    .change();

                if ($(this).data("file_skl_pt_" + i) == "-") {
                    $(".file_transkrip_" + i).attr("hidden", true);
                    $(".msg_file_transkrip_" + i).attr("hidden", false);
                } else {
                    $("#file_transkrip_" + i).attr(
                        "href",
                        app_url +
                            "/files/" +
                            $(this).data("file_transkrip_pt_" + i)
                    );
                }
            }
            break;

        case "Belum Lulus":
            $(".form_hasil_studi_belum_lulus").attr("hidden", false);

            // set data
            $("#tahapan_studi_bl_periksa")
                .val($(this).data("tahapan_studi"))
                .change();
            $("#file_timeline_bl_periksa").attr(
                "href",
                app_url + "/files/" + $(this).data("file_timeline")
            );
            $("#tipe_dokumen_penelitian_periksa")
                .val($(this).data("tipe_dokumen_penelitian"))
                .change();
            $("#file_draft_dokumen_penelitian_bl_periksa").attr(
                "href",
                app_url +
                    "/files/" +
                    $(this).data("file_draft_dokumen_penelitian")
            );
            break;

        case "Mengundurkan Diri":
            $(".form_hasil_studi_mengundurkan_diri").attr("hidden", false);

            // set data
            $("#alasan_pengunduran_diri_md_periksa")
                .val($(this).data("alasan_pengunduran_diri"))
                .change();
            $("#mundur_semester_md_periksa")
                .val($(this).data("mundur_semester"))
                .change();
            $("#tahun_akademik_md_periksa")
                .val($(this).data("tahun_akademik"))
                .change();
            $("#file_permohonan_md_periksa").attr(
                "href",
                app_url + "/files/" + $(this).data("file_permohonan")
            );
            $("#no_surat_persetujuan_md_periksa")
                .val($(this).data("no_surat_persetujuan"))
                .change();
            $("#tgl_surat_persetujuan_md_periksa")
                .val($(this).data("tgl_surat_persetujuan"))
                .change();
            $("#file_dokumen_surat_persetujuan_md_periksa").attr(
                "href",
                app_url +
                    "/files/" +
                    $(this).data("file_dokumen_surat_persetujuan")
            );

            break;

        case "Drop Out":
            $(".form_hasil_studi_drop_out").attr("hidden", false);

            // set data
            $("#semester_do_periksa").val($(this).data("semester_do")).change();
            $("#tahun_do_periksa").val($(this).data("tahun_do")).change();
            $("#no_surat_do_periksa").val($(this).data("no_surat_do")).change();
            $("#tgl_surat_keterangan_do_periksa")
                .val($(this).data("tgl_surat_keterangan_do"))
                .change();
            $("#file_surat_keterangan_do_periksa").attr(
                "href",
                app_url + "/files/" + $(this).data("file_surat_keterangan_do")
            );
            break;
    }
});

// button setuju lapor diri
$(".btnSetuju").click(function () {
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        title: "Yakin akan menyetujui data?",
        text: "Data akan masuk ke tahap berikutnya!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        reverseButtons: true,
        confirmButtonText: "Setuju!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/pelaporan/lapor-diri/setuju",
                data: {
                    _token: token,
                    id: $("#id_lapor_diri").val(),
                    keterangan: result.value,
                },
                success: function (response) {
                    $("#periksa_lapor_diri").modal("hide");

                    if (response.status == 1) {
                        swalSuccess("data berhasil di setujui");
                        $("#ld-" + $("#id_lapor_diri").val()).remove();
                    } else {
                        swalError(response.message);
                    }
                },
            });
        }
    });
});

// button tolak lapor diri
$(".btnTolak").click(function () {
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        input: "textarea",
        inputLabel: "Alasan Tolak",
        inputPlaceholder: "Masukan Alasan Penolakan",
        inputAttributes: {
            "aria-label": "Masukan Alasan Penolakan",
        },
        title: "Yakin akan menolak data?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Tolak!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/pelaporan/lapor-diri/tolak",
                data: {
                    _token: token,
                    id: $("#id_lapor_diri").val(),
                    keterangan: result.value,
                },
                success: function (response) {
                    $("#periksa_lapor_diri").modal("hide");

                    if (response.status == 1) {
                        swalSuccess("data berhasil ditolak");
                        $("#ld-" + $("#id_lapor_diri").val()).remove();
                    } else {
                        swalError(response.message);
                    }
                },
            });
        }
    });
});

// button periksa hasil akhir
$(".btnPeriksaHasilAkhir").click(function () {
    $("#periksa_hasil_akhir").modal("show");
    $(".form_opsi").attr("hidden", true);

    $("#id_hasil_akhir").val($(this).data("id")).change();
    $(".btnTolak").prop("data-id", $(this).data("id"));
    $(".btnSetuju").prop("data-id", $(this).data("id"));

    // switch
    switch ($(this).data("status")) {
        case "Lulus":
            $(".form_hasil_studi_lulus").attr("hidden", false);
            let pt = $(this).data("count_pt");

            switch (pt) {
                case 1:
                    $(".data_pt_1").attr("hidden", false);
                    break;

                case 2:
                    $(".data_pt_1").attr("hidden", false);
                    $(".data_pt_2").attr("hidden", false);
                    break;

                case 3:
                    $(".data_pt_1").attr("hidden", false);
                    $(".data_pt_2").attr("hidden", false);
                    $(".data_pt_3").attr("hidden", false);
                    break;
            }

            $(".get_nama").text(": " + $(this).data("nama"));
            $(".get_nip").text(": " + $(this).data("nip"));
            $(".get_pangkat").text(": " + $(this).data("pangkat"));
            $(".get_jabatan").text(": " + $(this).data("jabatan"));
            $(".get_jenjang").text(": " + $(this).data("jenjang"));
            $(".get_program_beasiswa").text(
                ": " + $(this).data("program_beasiswa")
            );
            $(".get_lokasi").text(": " + $(this).data("lokasi"));
            $(".get_pt").text(": " + $(this).data("pt"));
            $(".get_prodi").text(": " + $(this).data("prodi"));
            $(".get_st_tubel").text(": " + $(this).data("st_tubel"));
            $(".get_tgl_mulai_selesai").text(
                ": " +
                    $(this).data("tgl_mulai") +
                    " s.d. " +
                    $(this).data("tgl_selesai")
            );
            $(".get_tgl_st_tubel").text(": " + $(this).data("tgl_st_tubel"));
            $(".get_kep_pembebasan").text(
                ": " + $(this).data("kep_pembebasan")
            );
            $(".tgl_kep_pembebasan_lihat").text(
                ": " + $(this).data("tgl_kep_pembebasan")
            );
            $(".get_tmt_kep_pembebasan").text(
                ": " + $(this).data("tmt_kep_pembebasan")
            );
            $(".get_email").text(": " + $(this).data("email"));
            $(".get_hp").text(": " + $(this).data("hp"));
            $(".get_alamat").text(": " + $(this).data("alamat"));

            $(".get_id_usulan_riset").val($(this).data("id_usulan_riset"));
            $(".get_topik_riset").val($(this).data("topik_riset"));
            $(".get_judul_riset").val($(this).data("judul_riset"));
            $(".get_dokumen_proposal").attr(
                "href",
                app_url + "/files/" + $(this).data("file_proposal")
            );

            $("#tgl_kelulusan_mp_periksa").val($(this).data("tgl_kelulusan"));
            $("#opsi_dokumen_penelitian_mp_periksa").val(
                $(this).data("tipe_dokumen_penelitian")
            );
            $("#file_penelitian_mp_periksa").attr(
                "href",
                app_url + "/files/" + $(this).data("file_dokumen_penelitian")
            );
            $("#file_lps_mp_periksa").attr(
                "href",
                app_url + "/files/" + $(this).data("file_lps")
            );

            // loop data perguruan tinggi
            for (let i = 1; i <= pt; i++) {
                $("#nama_pt_" + i)
                    .text($(this).data("nama_pt_" + i))
                    .change();

                $("#ipk_periksa_ijazah_" + i)
                    .val($(this).data("ipk_pt_" + i))
                    .change();

                $("#nomor_skl_periksa_ijazah_" + i)
                    .val($(this).data("no_skl_pt_" + i))
                    .change();

                $("#tgl_skl_periksa_ijazah_" + i)
                    .val($(this).data("tgl_skl_pt_" + i))
                    .change();

                $("#file_skl_" + i).attr(
                    "href",
                    app_url + "/files/" + $(this).data("file_skl_pt_" + i)
                );

                $("#nomor_ijazah_periksa_ijazah_" + i)
                    .val($(this).data("no_ijazah_pt_" + i))
                    .change();

                $("#tgl_ijazah_periksa_ijazah_" + i)
                    .val($(this).data("tgl_ijazah_pt_" + i))
                    .change();

                $("#file_ijazah_" + i).attr(
                    "href",
                    app_url + "/files/" + $(this).data("file_ijazah_pt_" + i)
                );

                $("#nomor_transkrip_periksa_ijazah_" + i)
                    .val($(this).data("no_transkrip_pt_" + i))
                    .change();

                $("#tgl_transkrip_periksa_ijazah_" + i)
                    .val($(this).data("tgl_transkrip_pt_" + i))
                    .change();

                $("#file_transkrip_" + i).attr(
                    "href",
                    app_url + "/files/" + $(this).data("file_transkrip_pt_" + i)
                );
            }

            break;

        case "Belum Lulus":
            $(".form_hasil_studi_belum_lulus").attr("hidden", false);

            // set data
            $("#tahapan_studi_bl_periksa")
                .val($(this).data("tahapan_studi"))
                .change();
            $("#file_timeline_bl_periksa").attr(
                "href",
                app_url + "/files/" + $(this).data("file_timeline")
            );
            $("#tipe_dokumen_penelitian_periksa")
                .val($(this).data("tipe_dokumen_penelitian"))
                .change();
            $("#file_draft_dokumen_penelitian_bl_periksa").attr(
                "href",
                app_url +
                    "/files/" +
                    $(this).data("file_draft_dokumen_penelitian")
            );
            break;

        case "Mengundurkan Diri":
            $(".form_hasil_studi_mengundurkan_diri").attr("hidden", false);

            // set data
            $("#alasan_pengunduran_diri_md_periksa")
                .val($(this).data("alasan_pengunduran_diri"))
                .change();
            $("#mundur_semester_md_periksa")
                .val($(this).data("mundur_semester"))
                .change();
            $("#tahun_akademik_md_periksa")
                .val($(this).data("tahun_akademik"))
                .change();
            $("#file_permohonan_md_periksa").attr(
                "href",
                app_url + "/files/" + $(this).data("file_permohonan")
            );
            $("#no_surat_persetujuan_md_periksa")
                .val($(this).data("no_surat_persetujuan"))
                .change();
            $("#tgl_surat_persetujuan_md_periksa")
                .val($(this).data("tgl_surat_persetujuan"))
                .change();
            $("#file_dokumen_surat_persetujuan_md_periksa").attr(
                "href",
                app_url +
                    "/files/" +
                    $(this).data("file_dokumen_surat_persetujuan")
            );

            break;

        case "Drop Out":
            $(".form_hasil_studi_drop_out").attr("hidden", false);

            // set data
            $("#semester_do_periksa").val($(this).data("semester_do")).change();
            $("#tahun_do_periksa").val($(this).data("tahun_do")).change();
            $("#no_surat_do_periksa").val($(this).data("no_surat_do")).change();
            $("#tgl_surat_keterangan_do_periksa")
                .val($(this).data("tgl_surat_keterangan_do"))
                .change();
            $("#file_surat_keterangan_do_periksa").attr(
                "href",
                app_url + "/files/" + $(this).data("file_surat_keterangan_do")
            );
            break;
    }
});

// button setuju lapor diri
$(".btnSetujuHasilAkhir").click(function () {
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        title: "Yakin akan menyetujui data?",
        text: "Data akan masuk ke tahap berikutnya!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        reverseButtons: true,
        confirmButtonText: "Setuju!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/pelaporan/hasil-akhir/setuju",
                data: {
                    _token: token,
                    id: $("#id_hasil_akhir").val(),
                    keterangan: result.value,
                },
                success: function (response) {
                    $("#periksa_hasil_akhir").modal("hide");

                    if (response.status == 1) {
                        swalSuccess("data berhasil disetujui");
                        $("#ha-" + $("#id_hasil_akhir").val()).remove();
                    } else {
                        swalError(response.message);
                    }
                },
            });
        }
    });
});

// button tolak lapor diri
$(".btnTolakHasilAkhir").click(function () {
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        input: "textarea",
        inputLabel: "Alasan Tolak",
        inputPlaceholder: "Masukan Alasan Penolakan",
        inputAttributes: {
            "aria-label": "Masukan Alasan Penolakan",
        },
        title: "Yakin akan menolak data?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Tolak!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/pelaporan/hasil-akhir/tolak",
                data: {
                    _token: token,
                    id: $("#id_hasil_akhir").val(),
                    keterangan: result.value,
                },
                success: function (response) {
                    $("#periksa_hasil_akhir").modal("hide");

                    if (response.status == 1) {
                        swalSuccess("data berhasil ditolak");
                        $("#ha-" + $("#id_hasil_akhir").val()).remove();
                    } else {
                        swalError(response.message);
                    }
                },
            });
        }
    });
});

// opsi hasil studi
$("#opsi_hasil_studi").on("change", function () {
    if ($(this).val() == 1) {
        $(".form_hasil_studi_lulus").attr("hidden", false);
    }
});

// button periksa ijazah
$(".btnPeriksaIjazah").on("click", function () {
    $("#modal_periksa_ijazah").modal("show");
    $("#id_hasil_studi").val($(this).data("id"));
    let pt = $(this).data("count_pt");

    switch (pt) {
        case 1:
            $(".data_pt_1").attr("hidden", false);
            break;

        case 2:
            $(".data_pt_1").attr("hidden", false);
            $(".data_pt_2").attr("hidden", false);
            break;

        case 3:
            $(".data_pt_1").attr("hidden", false);
            $(".data_pt_2").attr("hidden", false);
            $(".data_pt_3").attr("hidden", false);
            break;
    }

    $(".get_nama").text(": " + $(this).data("nama"));
    $(".get_nip").text(": " + $(this).data("nip"));
    $(".get_pangkat").text(": " + $(this).data("pangkat"));
    $(".get_jabatan").text(": " + $(this).data("jabatan"));
    $(".get_jenjang").text(": " + $(this).data("jenjang"));
    $(".get_program_beasiswa").text(": " + $(this).data("program_beasiswa"));
    $(".get_lokasi").text(": " + $(this).data("lokasi"));
    $(".get_st_tubel").text(": " + $(this).data("st_tubel"));
    $(".get_tgl_st_tubel").text(": " + $(this).data("tgl_st_tubel"));
    $(".get_tgl_mulai_selesai").text(
        ": " +
            $(this).data("tgl_mulai") +
            " s.d. " +
            $(this).data("tgl_selesai")
    );
    $(".get_kep_pembebasan").text(": " + $(this).data("kep_pembebasan"));
    $(".get_tgl_kep_pembebasan").text(
        ": " + $(this).data("tgl_kep_pembebasan")
    );
    $(".get_tmt_kep_pembebasan").text(
        ": " + $(this).data("tmt_kep_pembebasan")
    );
    $(".get_email").text(": " + $(this).data("email"));
    $(".get_hp").text(": " + $(this).data("hp"));
    $(".get_alamat").text(": " + $(this).data("alamat"));

    // loop data perguruan tinggi
    for (let i = 1; i <= pt; i++) {
        $("#nama_pt_" + i)
            .text($(this).data("nama_pt_" + i))
            .change();

        $("#ipk_periksa_ijazah_" + i)
            .val($(this).data("ipk_pt_" + i))
            .change();

        $("#nomor_skl_periksa_ijazah_" + i)
            .val($(this).data("no_skl_pt_" + i))
            .change();

        $("#tgl_skl_periksa_ijazah_" + i)
            .val($(this).data("tgl_skl_pt_" + i))
            .change();

        if ($(this).data("file_ijazah_pt_" + i) == "") {
            $(".file_ijazah_ha_" + i).attr("hidden", true);
        } else {
            $(".file_ijazah_ha_" + i).attr("hidden", false);
            $("#file_ijazah_ha_" + i).attr(
                "href",
                app_url + "/files/" + $(this).data("file_ijazah_pt_" + i)
            );
            $("#file_skl_" + i).attr(
                "href",
                app_url + "/files/" + $(this).data("file_skl_pt_" + i)
            );
        }

        $("#nomor_ijazah_periksa_ijazah_" + i)
            .val($(this).data("no_ijazah_pt_" + i))
            .change();

        $("#tgl_ijazah_periksa_ijazah_" + i)
            .val($(this).data("tgl_ijazah_pt_" + i))
            .change();

        $("#file_ijazah_" + i).attr(
            "href",
            app_url + "/files/" + $(this).data("file_ijazah_pt_" + i)
        );

        $("#nomor_transkrip_periksa_ijazah_" + i)
            .val($(this).data("no_transkrip_pt_" + i))
            .change();

        $("#tgl_transkrip_periksa_ijazah_" + i)
            .val($(this).data("tgl_transkrip_pt_" + i))
            .change();

        $("#file_transkrip_" + i).attr(
            "href",
            app_url + "/files/" + $(this).data("file_transkrip_pt_" + i)
        );
        $.fn.modal.Constructor.prototype._enforceFocus = function () {};
    }
});

// button tolak verifikasi ijazah
$(".btnTolakverifikasiIjazah").click(function () {
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        input: "textarea",
        inputLabel: "Alasan Tolak",
        inputPlaceholder: "Masukan Alasan Penolakan",
        inputAttributes: {
            "aria-label": "Masukan Alasan Penolakan",
        },
        title: "Yakin akan menolak data?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Tolak!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/pelaporan/verifikasi-ijazah/tolak",
                data: {
                    _token: token,
                    id: $("#id_hasil_studi").val(),
                    keterangan: result.value,
                },
                success: function (response) {
                    $("#modal_periksa_ijazah").modal("hide");

                    if (response.status == 1) {
                        swalSuccess("data berhasil di tolak");
                        $("#ld-" + $("#id_hasil_studi").val()).remove();
                    } else {
                        swalError(response.message);
                    }
                },
            });
        }
    });
});

// button terima verifikasi ijazah
$(".btnSetujuverifikasiIjazah").click(function () {
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        title: "Yakin akan menyetujui data?",
        text: "Data akan masuk ke tahap berikutnya!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        reverseButtons: true,
        confirmButtonText: "Setuju!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/pelaporan/verifikasi-ijazah/setuju",
                data: {
                    _token: token,
                    id: $("#id_hasil_studi").val(),
                    keterangan: result.value,
                },
                success: function (response) {
                    $("#modal_periksa_ijazah").modal("hide");

                    if (response.status == 1) {
                        swalSuccess("data berhasil di setujui");
                        $("#ld-" + $("#id_hasil_studi").val()).remove();
                    } else {
                        swalError(response.message);
                    }
                },
            });
        }
    });
});

// placeholder advance search
$(document).ready(function () {
    // jenjang
    $selectElement = $("#search_jenjang").select2({
        placeholder: "jenjang pendidikan",
        allowClear: false,
        minimumResultsForSearch: -1,
    });

    // lokasi
    $selectElement = $("#search_lokasi").select2({
        placeholder: "lokasi pendidikan",
        allowClear: false,
        minimumResultsForSearch: -1,
    });
});
