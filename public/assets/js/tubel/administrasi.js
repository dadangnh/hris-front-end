$(document).ready(function () {
    $("#btnTarikData").click(function () {
        var $this = $(this);

        // Call Button Loading Function
        BtnLoading($this);

        let url = app_url + "/tubel/administrasi/get-tubel";

        $.ajax({
            type: "get",
            url: url,
            success: function (response) {
                if (response.status == 0) {
                    swalError(response.message);
                } else {
                    if (response.data.message == "Data sudah ada di database") {
                        swalSuccess(response.data.message);
                    } else {
                        location.reload();
                        swalSuccess(response.data.message);
                    }
                    $(this).prop("disabled", true);
                }

                // reset button
                BtnReset($this);
            },
        });
    });
});

// spinner / loading button
function BtnLoading(elem) {
    $(elem).attr("data-original-text", $(elem).html());
    $(elem).prop("disabled", true);
    $(elem).html('Loading... <i class="spinner-border spinner-border-sm"></i>');
}

// spinner off / reset button
function BtnReset(elem) {
    $(elem).prop("disabled", false);
    $(elem).html($(elem).attr("data-original-text"));
}
