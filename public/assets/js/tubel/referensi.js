
// button edit dasar referensi
$(".btnEdit").click(function () {
    $("#edit_dasar_ref").modal("show");

    $("#id_ref_edit").val(
        $(this).data("id_ref")
    );
    $("#nama_ref_edit").val(
        $(this).data("nama_ref")
    );
});

// button hapus referensi
$(".btnHapus").click(function () {
    var id = $(this).data("id_ref_negara_hapus");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        text: "Data tidak dapat dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/referensi/delete",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: id,
                },
                success: function (response) {
                    $("#" + id).remove();

                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: response.message,
                        showConfirmButton: false,
                        title: "Berhasil",
                        text: "data berhasil dihapus",
                        timer: 2000,
                    });
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button hapus perguruan tinggi
$(".btnHapusPT").click(function () {
    var id = $(this).data("id_ref_negara_hapus");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        text: "Data tidak dapat dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/referensi/pt/delete",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: id,
                },
                success: function (response) {
                    $("#" + id).remove();

                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: response.message,
                        showConfirmButton: false,
                        title: "Berhasil",
                        text: "data berhasil dihapus",
                        timer: 2000,
                    });
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button hapus program studi
$(".btnHapusProdi").click(function () {
    var id = $(this).data("id_ref_prodi_hapus");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        text: "Data tidak dapat dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/referensi/prodi/delete",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: id,
                },
                success: function (response) {
                    $("#" + id).remove();

                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: response.message,
                        showConfirmButton: false,
                        title: "Berhasil",
                        text: "data berhasil dihapus",
                        timer: 2000,
                    });
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button hapus jenjang pendidikan
$(".btnHapusJenjang").click(function () {
    var id = $(this).data("id_ref_jenjang_hapus");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        text: "Data tidak dapat dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/referensi/jenjang/delete",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: id,
                },
                success: function (response) {
                    $("#" + id).remove();

                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: response.message,
                        showConfirmButton: false,
                        title: "Berhasil",
                        text: "data berhasil dihapus",
                        timer: 2000,
                    });
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button hapus lokasi pendidikan
$(".btnHapusLokasi").click(function () {
    var id = $(this).data("id_ref_lokasi_hapus");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        text: "Data tidak dapat dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/referensi/lokasi/delete",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: id,
                },
                success: function (response) {
                    $("#" + id).remove();

                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: response.message,
                        showConfirmButton: false,
                        title: "Berhasil",
                        text: "data berhasil dihapus",
                        timer: 2000,
                    });
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});


