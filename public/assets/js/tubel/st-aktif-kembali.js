$(".datepick").flatpickr();

// button rekam permohonan aktif
$(".btnRekamStAktif").click(function () {
    $("#rekam_st_aktif_kembali").modal("show");

    $("#id_st_aktif_kembali").val($(this).data("id_st_aktif_kembali"));
    $("#nip_rekam").text(": " + $(this).data("nip"));
    $("#nama_rekam").text(": " + $(this).data("nama"));
    $("#jenjang_pendidikan_rekam").text(": " + $(this).data("jenjang"));
    $("#lokasi_pendidikan_rekam").text(": " + $(this).data("lokasi"));
    $("#tgl_aktif_kembali_rekam").text(": " + $(this).data("tglaktifkembali"));
});

// button lihat permohonan aktif
$(".btnLihatStAktif").click(function () {
    $("#lihat_st_aktif_kembali").modal("show");

    $("#nip_lihat").text(": " + $(this).data("nip"));
    $("#nama_lihat").text(": " + $(this).data("nama"));
    $("#jenjang_pendidikan_lihat").text(": " + $(this).data("jenjang"));
    $("#lokasi_pendidikan_lihat").text(": " + $(this).data("lokasi"));
    $("#tgl_aktif_kembali_lihat").text(": " + $(this).data("tglaktifkembali"));

    $("#nomor_surat_tugas_lihat").val($(this).data("nost"));
    $("#tgl_surat_tugas_lihat").val($(this).data("tglst"));
    // $("#tgl_aktif_kembali_lihat").val($(this).data('tglmulaist') + 's.d.' + $(this).data('tglselesaist'))
    $("#tgl_mulai_lihat").val($(this).data("tglmulaist"));
    $("#tgl_selesai_lihat").val($(this).data("tglselesaist"));
    $("#file_surat_tugas_lihat").attr(
        "href",
        app_url + "/files/" + $(this).data("file")
    );
});

// placeholder advance search
$(window).on("load", function () {
    // jenjang
    $selectElement = $("#search_jenjang").select2({
        placeholder: "jenjang pendidikan",
        allowClear: true,
        minimumResultsForSearch: -1,
    });

    // lokasi
    $selectElement = $("#search_lokasi").select2({
        placeholder: "lokasi pendidikan",
        allowClear: true,
        minimumResultsForSearch: -1,
    });
});
