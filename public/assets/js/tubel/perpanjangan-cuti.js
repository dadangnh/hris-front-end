// assets
$(".datepick").flatpickr();

// button periksa cuti
$(".btnPeriksa").click(function () {
    $("#periksa_perpanjangan_cuti").modal("show");

    $("#idCuti").val($(this).data("idcuti"));
    $("#alasan_cuti").val($(this).data("alasancuti"));
    $("#tglMulai").val($(this).data("tglmulai"));
    $("#tglSelesai").val($(this).data("tglselesai"));
    $("#tglSurat").val($(this).data("tglsurat"));
    $("#surat_izin_cuti_kampus_periksa").val($(this).data("suratkampus"));
    $("#surat_izin_cuti_sponsor").val($(this).data("suratsponsor"));
    $("#tglSuratSponsor").val($(this).data("tglsponsor"));
    $("#tglSuratKampus").val($(this).data("tglkampus"));
    $("#filesuratkampus").attr(
        "href",
        app_url + "/files/" + $(this).data("filesuratkampus")
    );
    $("#filesuratsponsor").attr(
        "href",
        app_url + "/files/" + $(this).data("filesuratsponsor")
    );

    $("#get_nama_setujui").text(": " + $(this).data("nama"));
    $("#get_nip_setujui").text(": " + $(this).data("nip"));
    $("#get_pangkat_setujui").text(": " + $(this).data("pangkat"));
    $("#get_jabatan_setujui").text(": " + $(this).data("jabatan"));
    $("#get_jenjang_setujui").text(": " + $(this).data("jenjang"));
    $("#get_program_beasiswa_setujui").text(
        ": " + $(this).data("program_beasiswa")
    );
    $("#get_lokasi_setujui").text(": " + $(this).data("lokasi"));
    // $('#get_pt_setujui').text(': ' + $(this).data("pt")
    // $('#get_prodi_setujui').text(': ' + $(this).data("prodi")
    $("#get_st_tubel_setujui").text(": " + $(this).data("st_tubel"));
    $("#get_tgl_st_tubel_setujui").text(": " + $(this).data("tgl_st_tubel"));
    $("#get_tgl_mulai_selesai_setujui").text(
        ": " +
            $(this).data("tgl_mulai") +
            " s.d. " +
            $(this).data("tgl_selesai")
    );
    $("#get_kep_pembebasan_setujui").text(
        ": " + $(this).data("kep_pembebasan")
    );
    $("#get_tgl_kep_pembebasan_setujui").text(
        ": " + $(this).data("tgl_kep_pembebasan")
    );
    $("#get_tmt_kep_pembebasan_setujui").text(
        ": " + $(this).data("tmt_kep_pembebasan")
    );
    $("#get_email_setujui").text(": " + $(this).data("email"));
    $("#get_hp_setujui").text(": " + $(this).data("hp"));
    $("#get_alamat_setujui").text(": " + $(this).data("alamat"));
    $.fn.modal.Constructor.prototype._enforceFocus = function () {};
});

// button lihat cuti
$(".btnLihat").click(function () {
    $("#lihat_perpanjangan_cuti").modal("show");

    // $("#alasan_cuti_lihat").attr("placeholder", $(this).data("alasancuti"));
    // $("#tgl_mulai_lihat").attr("placeholder", $(this).data("tglmulai"));
    // $("#tgl_selesai_lihat").attr("placeholder", $(this).data("tglselesai"));
    // $("#surat_izin_cuti_kampus").attr("placeholder", $(this).data("suratkampus"));
    // $("#surat_izin_cuti_sponsor_lihat").attr("placeholder", $(this).data("suratsponsor"));
    // $("#tgl_surat_sponsor_lihat").attr("placeholder", $(this).data("tglsponsor"));
    // $("#tgl_surat_kampus_lihat").attr("placeholder", $(this).data("tglkampus"));
    // $("#file_surat_kampus_lihat").attr("href", app_url + '/files/' + $(this).data("filesuratkampus"));
    // $("#file_surat_sponsor_lihat").attr("href", app_url + '/files/' + $(this).data("filesuratsponsor"));

    $("#alasan_cuti_lihat").val($(this).data("alasancuti"));
    $("#tgl_mulai_lihat").val($(this).data("tglmulai"));
    $("#tgl_selesai_lihat").val($(this).data("tglselesai"));
    $("#surat_izin_cuti_kampus").val($(this).data("suratkampus"));
    $("#surat_izin_cuti_sponsor_lihat").val($(this).data("suratsponsor"));
    $("#tgl_surat_sponsor_lihat").val($(this).data("tglsponsor"));
    $("#tgl_surat_kampus_lihat").val($(this).data("tglkampus"));
    $("#file_surat_kampus_lihat").attr(
        "href",
        app_url + "/files/" + $(this).data("filesuratkampus")
    );
    $("#file_surat_sponsor_lihat").attr(
        "href",
        app_url + "/files/" + $(this).data("filesuratsponsor")
    );

    $("#lihat_nama_setujui").text(": " + $(this).data("nama"));
    $("#lihat_nip_setujui").text(": " + $(this).data("nip"));
    $("#lihat_pangkat_setujui").text(": " + $(this).data("pangkat"));
    $("#lihat_jabatan_setujui").text(": " + $(this).data("jabatan"));
    $("#lihat_jenjang_setujui").text(": " + $(this).data("jenjang"));
    $("#lihat_program_beasiswa_setujui").text(
        ": " + $(this).data("program_beasiswa")
    );
    $("#lihat_lokasi_setujui").text(": " + $(this).data("lokasi"));
    // $('#lihat_pt_setujui').text(': ' + $(this).data("pt")
    // $('#lihat_prodi_setujui').text(': ' + $(this).data("prodi")
    $("#lihat_st_tubel_setujui").text(": " + $(this).data("st_tubel"));
    $("#lihat_tgl_st_tubel_setujui").text(": " + $(this).data("tgl_st_tubel"));
    $("#lihat_tgl_mulai_selesai_setujui").text(
        ": " +
            $(this).data("tgl_mulai") +
            " s.d. " +
            $(this).data("tgl_selesai")
    );
    $("#lihat_kep_pembebasan_setujui").text(
        ": " + $(this).data("kep_pembebasan")
    );
    $("#lihat_tgl_kep_pembebasan_setujui").text(
        ": " + $(this).data("tgl_kep_pembebasan")
    );
    $("#lihat_tmt_kep_pembebasan_setujui").text(
        ": " + $(this).data("tmt_kep_pembebasan")
    );
    $("#lihat_email_setujui").text(": " + $(this).data("email"));
    $("#lihat_hp_setujui").text(": " + $(this).data("hp"));
    $("#lihat_alamat_setujui").text(": " + $(this).data("alamat"));
    $.fn.modal.Constructor.prototype._enforceFocus = function () {};
});

// button edit perpanjangan cuti
$(".btnEdit").click(function () {
    $("#edit_perpanjangan_cuti").modal("show");

    $("#idTubelEdit").val($(this).data("idtubel"));
    $("#id_cuti").val($(this).data("id_cuti"));
    $("#alasan_cuti_perpanjangan_edit").val($(this).data("alasancuti"));
    $("#tgl_mulai_cuti_perpanjangan_edit").val($(this).data("tglmulai"));
    $("#tgl_selesai_cuti_perpanjangan_edit").val($(this).data("tglselesai"));
    $("#surat_izin_cuti_kampus_perpanjangan_edit").val(
        $(this).data("suratkampus")
    );
    $("#surat_izin_cuti_sponsor_perpanjangan_edit").val(
        $(this).data("suratsponsor")
    );
    $("#tgl_surat_sponsor_perpanjangan_edit").val($(this).data("tglsponsor"));
    $("#tgl_surat_kampus_perpanjangan_edit").val($(this).data("tglkampus"));
    $("#file_cuti_kampus_edit").val($(this).data("filesuratkampus"));
    $("#file_cuti_kampus").attr(
        "href",
        app_url + "/files/" + $(this).data("filesuratkampus")
    );
    $("#file_cuti_sponsor_edit").val($(this).data("filesuratsponsor"));
    $("#file_cuti_sponsor").attr(
        "href",
        app_url + "/files/" + $(this).data("filesuratsponsor")
    );

    $("#nama_edit").text(": " + $(this).data("nama"));
    $("#nip_edit").text(": " + $(this).data("nip"));
    $("#pangkat_edit").text(": " + $(this).data("pangkat"));
    $("#jabatan_edit").text(": " + $(this).data("jabatan"));
    $("#jenjang_edit").text(": " + $(this).data("jenjang"));
    $("#program_beasiswa_edit").text(": " + $(this).data("program_beasiswa"));
    $("#lokasi_edit").text(": " + $(this).data("lokasi"));
    // $('#pt_edit').text(': ' + $(this).data("pt")
    // $('#prodi_edit').text(': ' + $(this).data("prodi")
    $("#st_tubel_edit").text(": " + $(this).data("st_tubel"));
    $("#tgl_st_tubel_edit").text(": " + $(this).data("tgl_st_tubel"));
    $("#tgl_mulai_selesai_edit").text(
        ": " +
            $(this).data("tgl_mulai") +
            " s.d. " +
            $(this).data("tgl_selesai")
    );
    $("#kep_pembebasan_edit").text(": " + $(this).data("kep_pembebasan"));
    $("#tgl_kep_pembebasan_edit").text(
        ": " + $(this).data("tgl_kep_pembebasan")
    );
    $("#tmt_kep_pembebasan_edit").text(
        ": " + $(this).data("tmt_kep_pembebasan")
    );
    $("#email_edit").text(": " + $(this).data("email"));
    $("#hp_edit").text(": " + $(this).data("hp"));
    $("#alamat_edit").text(": " + $(this).data("alamat"));
    $.fn.modal.Constructor.prototype._enforceFocus = function () {};
});

// button setuju periksa cuti aksi
$("#btnSetujuCuti").click(function () {
    var act = $(this).data("act");
    var id = $("#idCuti").val();

    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        title: "Yakin akan menyetujui data?",
        text: "Data akan masuk ke tahap berikutnya!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Setuju!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            // alert(id);
            $.ajax({
                type: "post",
                url: app_url + "/tubel/cuti/updatestatus",
                data: {
                    _token: token,
                    caseOutputId: act,
                    id: id,
                    keterangan: null,
                },
                success: function (response) {
                    // console.log(response);
                    location.reload();

                    if (response["status"] === 0) {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response["message"],
                            showConfirmButton: false,
                            // width: 600,
                            // padding: '5em',
                            timer: 1600,
                        });
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil di setujui",
                            showConfirmButton: false,
                            timer: 1600,
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button tolak cuti aksi
$("#btnTolakCuti").click(function () {
    var act = $(this).data("act");
    var idCuti = $("#idCuti").val();

    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        input: "textarea",
        inputLabel: "Alasan Tolak",
        inputPlaceholder: "Masukan Alasan Penolakan",
        inputAttributes: {
            "aria-label": "Masukan Alasan Penolakan",
        },
        title: "Yakin akan menolak data?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Tolak!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/cuti/updatestatus",
                data: {
                    _token: token,
                    caseOutputId: act,
                    id: idCuti,
                    keterangan: result.value,
                },
                success: function (response) {
                    if (response["status"] == 0) {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600,
                        });
                    } else {
                        location.reload();
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil di tolak",
                            showConfirmButton: false,
                            timer: 1600,
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button rekam cuti > get pegawai
$("#btnGetCuti").click(function () {
    $("#formRekamCutiAdmin").hide();

    // validasi nomor tiket jika kosong
    if ($("#tiket").val().length == "") {
        Swal.fire({
            position: "center",
            icon: "error",
            title: "Gagal!",
            text: "Inputan nomor tiket!",
            showConfirmButton: false,
            timer: 1500,
        });
    } else {
        BtnLoading("#btnGetCuti");

        $.ajax({
            type: "get",
            url: app_url + "/tubel/perpanjangancuti/getcuti",
            data: {
                tiket: $("#tiket").val(),
            },
            success: function (response) {
                if (response.status == 0) {
                    Swal.fire({
                        position: "center",
                        icon: "error",
                        title: "Gagal!",
                        text: response.message,
                        showConfirmButton: false,
                        timer: 3000,
                    });
                } else {
                    $("#formRekamCutiAdmin").show();
                    $("#niprekam").prop("disabled", true);
                    // console.log(response.data.indukTubel)
                    $("#idTubel").val(response.data.indukTubel.id);

                    $("#get_nama").text(
                        ": " + response.data.indukTubel.namaPegawai
                    );
                    $("#get_nip").text(": " + response.data.indukTubel.nip18);
                    $("#get_pangkat").text(
                        ": " + response.data.indukTubel.namaPangkat
                    );
                    $("#get_jabatan").text(
                        ": " + response.data.indukTubel.namaJabatan
                    );
                    $("#get_jenjang").text(
                        ": " +
                            response.data.indukTubel.jenjangPendidikanId
                                .jenjangPendidikan
                    );
                    $("#get_program_beasiswa").text(
                        ": " + response.data.indukTubel.programBeasiswa
                    );
                    $("#get_lokasi").text(
                        ": " +
                            response.data.indukTubel.lokasiPendidikanId.lokasi
                    );
                    // $('#get_pt').text(': ' + response.data.indukTubel.)
                    // $('#get_prodi').text(': ' + response.data.indukTubel.);
                    $("#get_st_tubel").text(
                        ": " + response.data.indukTubel.tglSt
                    );
                    $("#get_tgl_st_tubel").text(
                        ": " + response.data.indukTubel.tglSt
                    );
                    $("#get_tgl_mulai_selesai").text(
                        ": " +
                            response.data.indukTubel.tglMulaiSt +
                            " s.d. " +
                            response.data.indukTubel.tglSelesaiSt
                    );
                    $("#get_kep_pembebasan").text(
                        ": " + response.data.indukTubel.nomorKepPembebasan
                    );
                    $("#get_tgl_kep_pembebasan").text(
                        ": " + response.data.indukTubel.tglKepPembebasan
                    );
                    $("#get_tmt_kep_pembebasan").text(
                        ": " + response.data.indukTubel.tmtKepPembebasan
                    );
                    $("#get_email").text(
                        ": " + response.data.indukTubel.emailNonPajak
                    );
                    $("#get_hp").text(
                        ": " + response.data.indukTubel.noHandphone
                    );
                    $("#get_alamat").text(
                        ": " + response.data.indukTubel.alamatTinggal
                    );
                }

                // reset button
                BtnReset($this);
            },
        });
    }
});

// button hapus cuti by admin
$(".btnHapus").click(function () {
    var id = $(this).data("id");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/administrasi/hapuscuti",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: $(this).data("id"),
                },
                success: function (response) {
                    if (response["status"] == 1) {
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "Data berhasil di hapus",
                            showConfirmButton: false,
                            timer: 1800,
                        });
                        location.reload();
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 2000,
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// spinner / loading button
function BtnLoading(elem) {
    $(elem).attr("data-original-text", $(elem).html());
    $(elem).prop("disabled", true);
    $(elem).html('Loading... <i class="spinner-border spinner-border-sm"></i>');
}

// spinner off / reset button
function BtnReset(elem) {
    $(elem).prop("disabled", false);
    $(elem).html($(elem).attr("data-original-text"));
}

// placeholder advance search
$(window).on("load", function () {
    // jenjang
    $selectElement = $("#search_jenjang").select2({
        placeholder: "jenjang pendidikan",
        allowClear: true,
        minimumResultsForSearch: -1,
    });

    // lokasi
    $selectElement = $("#search_lokasi").select2({
        placeholder: "lokasi pendidikan",
        allowClear: true,
        minimumResultsForSearch: -1,
    });
});

// date format
$(".date-search").flatpickr({
    dateFormat: "d-m-Y",
});
