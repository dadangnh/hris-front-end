$(".datepick").flatpickr();

// get kantor iam
$(document).ready(function () {
    // jenjang
    $selectElement = $("#search_jenjang").select2({
        placeholder: "jenjang pendidikan",
        allowClear: false,
        minimumResultsForSearch: -1,
    });

    // lokasi
    $selectElement = $("#search_lokasi").select2({
        placeholder: "lokasi pendidikan",
        allowClear: false,
        minimumResultsForSearch: -1,
    });

    $selectElement = $("#topik").select2({
        placeholder: "Pilih topik riset",
        allowClear: false,
        minimumResultsForSearch: -1,
    });

    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $("#lokasi_penempatan_upload").select2({
        placeholder: "Pilih lokasi penempatan",
        minimumInputLength: 3,
        ajax: {
            url: app_url + "/tubel/administrasi/get-kantor",
            dataType: "json",
            type: "post",
            delay: 300,
            data: function (params) {
                return {
                    search: params.term,
                };
            },
            processResults: function (response) {
                return {
                    results: response,
                };
            },
        },
    });
});

// button upload kep
$(".btnUpload").on("click", function () {
    $("#id_upload_kep").val($(this).data("id")).change();
});

// button lihat kep
$(".btnLihat").on("click", function () {
    $("#modal_lihat_kep").modal("show");

    $("#nomor_kep_lihat").val($(this).data("nomor_kep")).change();
    $("#tgl_kep_lihat").val($(this).data("tgl_kep")).change();
    $("#tmt_kep_lihat").val($(this).data("tmt_kep")).change();
    $("#lokasi_penempatan_lihat")
        .val($(this).data("lokasi_penempatan"))
        .change();
    $("#file_kep_lihat").attr(
        "href",
        app_url + "/files/" + $(this).data("file_kep")
    );
});
