// get url
function getUrl() {
    return window.location.protocol + "//" + window.location.host;
}

// assets
$("#tgl_mulai_pendidikan").flatpickr();
$("#tgl_mulai_pendidikan2").flatpickr();
$("#tglSurat").flatpickr();
$("#tglSuratSponsor").flatpickr();
$("#tglMulaiAktif").flatpickr();
$("#tglSuratPengaktifan").flatpickr();
$(".datepick").flatpickr();

// masking
Inputmask({
    mask: "9999",
}).mask(".tahun_mask");

Inputmask({
    mask: "9",
    repeat: 2,
    greedy: false,
}).mask(".semester_mask");

Inputmask({
    mask: "9.99",
}).mask(".ipk_mask");

// button tambah perguruan tinggi
$(".btnAddPt").click(function () {
    var programPt = $(this).data("program_pt");
    var jmlPt = $(this).data("jml_pt");

    if (
        (programPt == "Dalam Negeri" && jmlPt == 1) ||
        (programPt == "Luar Negeri" && jmlPt == 1) ||
        (programPt == "Linkage" && jmlPt == 2) ||
        (programPt == "Sandwich" && jmlPt == 3)
    ) {
        Swal.fire({
            position: "center",
            icon: "error",
            title: "Gagal",
            text: "tidak dapat menambah data",
            showConfirmButton: false,
            timer: 2000,
        });
    } else {
        $("#add_perguruan_tinggi").modal("show");
    }
});

// button edit perguruan tinggi
$(".btnEditPt").click(function () {
    $("#update_perguruan_tinggi").modal("show");

    var id = $(this).data("id");
    var idPt = $(this).data("id_perguruan_tinggi");
    var idProdi = $(this).data("id_program_studi");
    var idNegara = $(this).data("id_negara");
    var tglMulai = $(this).data("tgl_mulai_pendidikan");

    $("#idPerguruanTinggi").val(id).change();
    $("#edit_perguruan_tinggi").val(idPt).change();
    $("#edit_pt_prodi").val(idProdi).change();
    $("#edit_pt_negara").val(idNegara).change();
    $("#tgl_mulai_pendidikan").val(tglMulai);
});

// button hapus perguruan tinggi
$(".btnHapusPt").click(function () {
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
    var url = $(this).data("url") + id;

    Swal.fire({
        title: "Yakin akan menghapus data?",
        text: "Data tidak dapat dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "delete",
                url: url,
                data: {
                    _token: token,
                    id: id,
                },
                success: function (response) {
                    $("#pid" + id).remove();

                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: response.message,
                        showConfirmButton: false,
                        title: "Berhasil",
                        text: "data berhasil dihapus",
                        timer: 1200,
                    });
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button lihat lps
$(".btnLihatLps").click(function () {
    $("#lihat_laporan_perkembangan_studi").modal("show");
    $("#fileLpsKhs").hide();

    var id = $(this).data("id");

    $("#perguruantinggi").val($(this).data("pt")).change();
    $("#semesterke").val($(this).data("smtke")).change();
    if ($(this).data("smt") == 1) {
        $("#semester").val("Ganjil").change();
    } else {
        $("#semester").val("Genap").change();
    }
    $("#tahun").val($(this).data("th")).change();
    $("#lihatipk").val($(this).data("ipk")).change();
    $("#jumlah_sks").val($(this).data("sks")).change();

    if ($(this).data("filelps") !== "" && $(this).data("filekhs") !== "") {
        $("#fileLpsKhs").show();

        $("#file_lps_lihat").attr(
            "href",
            app_url + "/files/" + $(this).data("filelps")
        );
        $("#file_khs_lihat").attr(
            "href",
            app_url + "/files/" + $(this).data("filekhs")
        );
    }
});

// button edit lps
$(".btnEditLps").click(function () {
    $("#edit_laporan_perkembangan_studi").modal("show");

    var id = $(this).data("id");
    var idpt = $(this).data("uidpt");
    var pt = $(this).data("pt");

    if ($(this).data("smt") == 1) {
        document.getElementById("rGanjil").checked = true;
    } else if ($(this).data("smt") == 2) {
        document.getElementById("rGenap").checked = true;
    }

    $("#idLps").val(id).change();
    // $('#eperguruantinggi').val($(this).data('pt')).change();
    $("#eperguruantinggi").val(idpt).change(); //
    $("#esemesterke").val($(this).data("smtke")).change();
    $("#etahun").val($(this).data("th")).change();
    $("#eipk").val($(this).data("ipk")).change();
    $("#ejumlah_sks").val($(this).data("sks")).change();
});

// button hapus laporan perkembangan studi
$(".btnHapusLps").click(function () {
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
    var url = $(this).data("url");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "delete",
                url: url,
                data: {
                    _token: token,
                    id: id,
                },
                success: function (response) {
                    if (response.status == 1) {
                        $("#lid" + id).remove();
                        swalSuccess("data berhasil di hapus");
                    } else {
                        swalError(response.message);
                    }
                },
            });
        }
    });
});

// button kirim laporan perkembangan studi
$(".btnKirimLps").click(function () {

    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
    var url = $(this).data("url");

    Swal.fire({
        title: "Yakin akan mengirim laporan?",
        text: "Data akan masuk ke tahap berikutnya!",
        icon: "info",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Kirim!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            blockUI.block()

            $.ajax({
                type: "post",
                url: url,
                data: {
                    _token: token,
                    id: id,
                },
                success: function (response) {
                    if (response.status == 1) {
                        location.reload();
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "Laporan berhasil dikirim",
                            showConfirmButton: false,
                            timer: 1800,
                        });
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1800,
                        });
                        blockUI.release()
                    }

                },
            });
        }
    });
});

// button lihat cuti
$(".btnLihatCuti").click(function () {
    var id = $(this).data("id");
    var tglmulai = $(this).data("tglmulai");
    var tglselesai = $(this).data("tglselesai");
    var alasan = $(this).data("alasan");
    var nokampus = $(this).data("nokampus");
    var tglsuratkampus = $(this).data("tglsuratkampus");
    var nosponsor = $(this).data("nosponsor");
    var tglsuratsponsor = $(this).data("tglsuratsponsor");
    var filesuratkampus = $(this).data("filesuratkampus");
    var filesuratsponsor = $(this).data("filesuratsponsor");

    $("#filesuratkampuslihat").attr("href", filesuratkampus);
    $("#filesuratsponsorlihat").attr("href", filesuratsponsor);

    $("#tglMulaiLihat").val(tglmulai).change();
    $("#tglSelesaiLihat").val(tglselesai).change();
    $("#tglSuratKamLihat").val(tglsuratkampus).change();
    $("#tglSuratSponsorLihat").val(tglsuratsponsor).change();
    $("#noSuratSponsorLihat").val(nosponsor);
    $("#noSuratKamLihat").val(nokampus).change();
    $("#alasanCutiLihat").val(alasan).change();

    if ($(this).data("nomorkep") != "") {
        document.getElementById("kep_penempatan").style.display = "";

        $("#nomor_kep_lihat").val($(this).data("nomorkep"));
        $("#tgl_kep_lihat").val($(this).data("tglkep"));
        $("#tmt_penempatan_lihat").val($(this).data("tgltmt"));
        $("#lokasi_penempatan_lihat").val($(this).data("lokasipenempatan"));
        $("#file_kep_lihat").attr("href", $(this).data("file"));
    }

    $("#lihat_cuti").modal("show");
});

// button edit cuti
$(".btnEditCuti").click(function () {
    $("#edit_cuti").modal("show");

    $("#idCuti").val($(this).data("id")).change();
    $("#tglMulaiEdit").val($(this).data("tglmulai")).change();
    $("#tglSelesaiEdit").val($(this).data("tglselesai")).change();
    $("#tglSuratKamEdit").val($(this).data("tglsuratkampus")).change();
    $("#tglSuratSponsorEdit").val($(this).data("tglsuratsponsor")).change();
    $("#noSuratSponsorEdit").val($(this).data("nosponsor"));
    $("#noSuratKamEdit").val($(this).data("nokampus")).change();
    $("#alasanCutiEdit").val($(this).data("alasan")).change();
    $("#file_cuti").attr("href", $(this).data("pathsuratkampus"));
    $("#file_cuti_lama").val($(this).data("pathsuratkampus_lama")).change();
    $("#file_sponsors").attr("href", $(this).data("pathsuratsponsor")).change();
    $("#file_sponsors_lama")
        .val($(this).data("pathsuratsponsor_lama"))
        .change();
});

// button hapus cuti
$(".btnHapusCuti").click(function () {
    var id = $(this).data("id");
    var url = $(this).data("url");

    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "delete",
                url: url,
                data: {
                    _token: token,
                    id: id,
                },
                success: function (response) {
                    if (response["status"] == 1) {
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil di hapus",
                            showConfirmButton: false,
                            timer: 1800,
                        });
                        $("#cut" + id).remove();
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1800,
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button kirim cuti
$(".btnKirimCuti").click(function () {
    var act = $(this).data("act");
    var id = $(this).data("id");
    var url = $(this).data("url");
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        title: "Yakin akan mengajukan cuti?",
        text: "Data akan masuk ke tahap berikutnya!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Kirim!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: url,
                data: {
                    _token: token,
                    caseOutputId: act,
                    id: id,
                    keterangan: null,
                },
                success: function (response) {
                    if (response.status === 1) {
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil di kirim",
                            showConfirmButton: false,
                            timer: 1600,
                        });

                        location.reload();
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600,
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button attachment lps
$(".btnUploadLps").click(function () {
    $("#upload_file_lps").modal("show");

    // message upload
    $("#messageuploadkhs").hide();
    $("#messageuploadlps").hide();

    // message download file
    $("#messagefilekhs").hide();
    $("#messagefilelps").hide();

    var idlps = $(this).data("idlps");
    var urlkhs = $(this).data("urlkhs");
    var urllps = $(this).data("urllps");

    $("#uidlps").val(idlps).change();
    $("#unggahidlps").val(idlps).change();
    $("#filekhs_lama").val(urlkhs).change();
    $("#filelps_lama").val(urllps).change();

    $("#urlfilekhs").attr("href", app_url + "/files/" + urlkhs);
    $("#urlfilelps").attr("href", app_url + "/files/" + urllps);
    $("#template_lps").attr(
        "href",
        app_url + "/tubel/administrasi/generate-lps/" + idlps
    );

    // download file khs
    // if((urlkhs !== 'null') && (urlkhs = "")) {
    if (urlkhs !== "") {
        $("#messagefilekhs").show();
    }
    // download file lps
    // if((urllps !== 'null') || (urllps = "")) {
    if (urllps !== "") {
        $("#messagefilelps").show();
    }
});

// upload file khs dan lps
$(document).ready(function (e) {
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    // upload khs - selesai
    $("#uploadkhs").submit(function (e) {
        e.preventDefault();
        BtnLoading($("#btnUploadKhs"));

        var formData = new FormData(this);

        var uidlps = $("#uidlps").val();
        var file_lama = $("#filekhs_lama").val();

        $.ajax({
            type: "POST",
            url: app_url + "/tubel/administrasi/uploadkhs",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: (response) => {
                if (response.status == 0) {
                    Swal.fire({
                        position: "center",
                        icon: "error",
                        title: "Error!",
                        text: response.message,
                        showConfirmButton: false,
                        timer: 2000,
                    });
                } else {
                    var nmFileKhs = response.file;
                    $.ajax({
                        type: "patch",
                        url: app_url + "/tubel/administrasi/uploadkhs",
                        data: {
                            id: uidlps,
                            file_baru: nmFileKhs,
                            file_lama: file_lama,
                        },
                        success: function (response) {
                            if (response.status == 1) {
                                Swal.fire({
                                    position: "center",
                                    icon: "success",
                                    title: "Berhasil",
                                    text: "File berhasil diupload",
                                    showConfirmButton: false,
                                    timer: 2000,
                                }),
                                    (document.getElementById(
                                        "messageuploadkhs"
                                    ).style.display = "");
                                document.getElementById(
                                    "messagefilekhs"
                                ).style.display = "";

                                var nmFileKhsBaru =
                                    app_url + "/files/" + nmFileKhs;

                                document.getElementById("urlfilekhs").href =
                                    nmFileKhsBaru;
                            }
                        },
                    });
                }
                BtnReset($("#btnUploadKhs"));
            },
            error: function (data) {
                BtnReset($("#btnUploadKhs"));
            },
        });
    });

    // upload laporan - selesai
    $("#uploadlps").submit(function (e) {
        e.preventDefault();
        BtnLoading($("#btnUploadLps"));

        var formData = new FormData(this);
        var unggahidlps = $("#unggahidlps").val();
        var file_lps_lama = $("#filelps_lama").val();

        $.ajax({
            type: "POST",
            url: app_url + "/tubel/administrasi/uploadlps",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: (response) => {
                if (response.status == 0) {
                    this.reset();
                    Swal.fire({
                        position: "center",
                        icon: "error",
                        title: "Error!",
                        text: response.message,
                        showConfirmButton: false,
                        timer: 2000,
                    });
                } else {
                    var nmFileLps = response.file;
                    $.ajax({
                        type: "patch",
                        url: app_url + "/tubel/administrasi/uploadlps",
                        data: {
                            id: unggahidlps,
                            file_baru: nmFileLps,
                            file_lama: file_lps_lama,
                        },
                        success: function (response) {
                            if (response.status == 1) {
                                Swal.fire({
                                    position: "center",
                                    icon: "success",
                                    title: "Berhasil",
                                    text: "File berhasil diupload",
                                    showConfirmButton: false,
                                    timer: 2000,
                                }),
                                    (document.getElementById(
                                        "messageuploadlps"
                                    ).style.display = "");
                                document.getElementById(
                                    "messagefilelps"
                                ).style.display = "";

                                var nmFileLpsBaru =
                                    app_url + "/files/" + nmFileLps;

                                document.getElementById("urlfilelps").href =
                                    nmFileLpsBaru;
                            }
                        },
                    });
                }
                BtnReset($("#btnUploadLps"));
            },
            error: function (data) {
                console.log(data);
            },
        });
    });
});

// button lihat permohonan aktif kembali
$(".btnLihatPermohonanAktif").click(function () {
    $("#lihat_permohonan_aktif").modal("show");

    var tglmulaiaktif = $(this).data("tglmulaiaktif");
    var nosurat = $(this).data("nosurat");
    var tglsurat = $(this).data("tglsurat");

    $("#tglMulaiAktifLihat").val(tglmulaiaktif);
    $("#suratAktifKembaliLihat").val(nosurat);
    $("#tglSuratPengaktifanLihat").val(tglsurat);
    $("#filePengaktifanLihat").attr(
        "href",
        app_url + "/files/" + $(this).data("file")
    );

    if (
        $(this).data("status") == "Surat Tugas diterbitkan" ||
        $(this).data("status") == "Kep Pembebasan diterbitkan"
    ) {
        document.getElementById("st_aktif_kembali").style.display = "";

        $("#nomor_surat_tugas_lihat").val($(this).data("nost"));
        $("#tgl_surat_tugas_lihat").val($(this).data("tglst"));
        $("#tgl_mulai_lihat").val($(this).data("tglmulaist"));
        $("#tgl_selesai_lihat").val($(this).data("tglselesaist"));
        $("#file_surat_tugas_lihat").attr(
            "href",
            app_url + "/files/" + $(this).data("filest")
        );
    }

    if ($(this).data("status") == "Kep Pembebasan diterbitkan") {
        document.getElementById("kep_pembebasan").style.display = "";

        $("#nomor_kep_pembebasan_lihat").val($(this).data("nomorkep"));
        $("#tgl_kep_pembebasan_lihat").val($(this).data("tglkep"));
        $("#tmt_lihat").val($(this).data("tmt"));
        $("#file_kep").attr(
            "href",
            app_url + "/files/" + $(this).data("filekep")
        );
    }
});

// button edit permohonan aktif kembali
$(".btnEditPermohonanAktif").click(function () {
    $("#edit_permohonan_aktif").modal("show");

    $("#id_permohonan_aktif").val($(this).data("id")).change();
    $("#tglMulaiAktifEdit").val($(this).data("tglmulaiaktif")).change();
    $("#suratAktifKembaliEdit").val($(this).data("nosurat")).change();
    $("#tglSuratPengaktifanEdit").val($(this).data("tglsurat")).change();
    $("#file_pengaktifan_edit").attr(
        "href",
        app_url + "/files/" + $(this).data("file")
    );
    $("#file_pengaktifan_edit_lama").val($(this).data("file")).change();
});

// button kirim permohonan aktif kembali
$(".btnKirimPermohonanAktif").click(function () {
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        title: "Yakin akan mengajukan permohonan aktif kembali?",
        text: "Data akan masuk ke tahap berikutnya!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Kirim!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/administrasi/pengaktifan-kembali/kirim",
                data: {
                    _token: token,
                    id: id,
                },
                success: function (response) {
                    if (response.status == 1) {
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil di kirim",
                            showConfirmButton: false,
                            timer: 1600,
                        });
                        location.reload();
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600,
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button hapus permohonan aktif kembali
$(".btnHapusPermohonanAktif").click(function () {
    var id = $(this).data("id");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "delete",
                url: app_url + "/tubel/administrasi/pengaktifan-kembali",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: id,
                },
                success: function (response) {
                    if (response["status"] == 1) {
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil di hapus",
                            showConfirmButton: false,
                            timer: 1800,
                        });
                        $("#cut" + id).remove();
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1800,
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button edit perpanjangan st tubel
$(".btnEditPerpanjanganSt").click(function () {
    $("#edit_perpanjangan_st").modal("show");

    $("#id_perpanjangan_st").val($(this).data("id_perpanjangan_st"));
    $("#alasan_perpanjangan_st_edit").val(
        $(this).data("alasan_perpanjangan_st_edit")
    );
    $("#tahapan_studi_edit").val($(this).data("tahapan_studi_edit"));
    $("#tgl_selesai_perpanjangan_edit").val(
        $(this).data("tgl_selesai_perpanjangan_edit")
    );
    $("#st_awal_edit").val($(this).data("st_awal_edit"));
    $("#file_st_awal_edit").attr(
        "href",
        app_url + "/files/" + $(this).data("file_st_awal_edit")
    );
    $("#file_st_awal_lama").val($(this).data("file_st_awal_edit"));
    $("#surat_persetujuan_kampus_edit").val(
        $(this).data("surat_persetujuan_kampus_edit")
    );
    $("#tgl_surat_persetujuan_kampus_edit").val(
        $(this).data("tgl_surat_persetujuan_kampus_edit")
    );
    $("#file_surat_persetujuan_kampus_edit").attr(
        "href",
        app_url + "/files/" + $(this).data("file_surat_persetujuan_kampus_edit")
    );
    $("#file_surat_persetujuan_kampus_lama").val(
        $(this).data("file_surat_persetujuan_kampus_edit")
    );
    $("#surat_persetujuan_sponsor_edit").val(
        $(this).data("surat_persetujuan_sponsor_edit")
    );
    $("#tgl_surat_persetujuan_sponsor_edit").val(
        $(this).data("tgl_surat_persetujuan_sponsor_edit")
    );
    $("#file_surat_persetujuan_sponsor_edit").attr(
        "href",
        app_url +
            "/files/" +
            $(this).data("file_surat_persetujuan_sponsor_edit")
    );
    $("#file_surat_persetujuan_sponsor_lama").val(
        $(this).data("file_surat_persetujuan_sponsor_edit")
    );
});

// button hapus perpanjangan st tubel
$(".btnHapusPerpanjanganSt").click(function () {
    var id = $(this).data("id");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "delete",
                url: app_url + "/tubel/administrasi/perpanjangan-st-tubel",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: id,
                },
                success: function (response) {
                    if (response["status"] == 1) {
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil di hapus",
                            showConfirmButton: false,
                            timer: 1800,
                        });
                        $("#" + id).remove();
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1800,
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button lihat perpanjangan st tubel
$(".btnLihatPerpanjanganSt").click(function () {
    document.getElementById("data_st_stan").style.display = "none !important";

    $("#lihat_perpanjangan_st").modal("show");

    $("#alasan_perpanjangan_st_lihat").val(
        $(this).data("alasan_perpanjangan_st_lihat")
    );
    $("#tahapan_studi_lihat").val($(this).data("tahapan_studi_lihat"));
    $("#tgl_selesai_perpanjangan_lihat").val(
        $(this).data("tgl_selesai_perpanjangan_lihat")
    );
    $("#st_awal_lihat").val($(this).data("st_awal_lihat"));
    $("#file_st_awal_lihat").attr(
        "href",
        app_url + "/files/" + $(this).data("file_st_awal_lihat")
    );
    $("#surat_persetujuan_kampus_lihat").val(
        $(this).data("surat_persetujuan_kampus_lihat")
    );
    $("#tgl_surat_persetujuan_kampus_lihat").val(
        $(this).data("tgl_surat_persetujuan_kampus_lihat")
    );
    $("#file_surat_persetujuan_kampus_lihat").attr(
        "href",
        app_url +
            "/files/" +
            $(this).data("file_surat_persetujuan_kampus_lihat")
    );
    $("#surat_persetujuan_sponsor_lihat").val(
        $(this).data("surat_persetujuan_sponsor_lihat")
    );
    $("#tgl_surat_persetujuan_sponsor_lihat").val(
        $(this).data("tgl_surat_persetujuan_sponsor_lihat")
    );
    $("#file_surat_persetujuan_sponsor_lihat").attr(
        "href",
        app_url +
            "/files/" +
            $(this).data("file_surat_persetujuan_sponsor_lihat")
    );

    if ($(this).data("status") == "ST Perpanjangan Tubel diterbitkan") {
        document.getElementById("upload_st_perpanjangan").style.display = "";

        $("#nomor_st_lihat_st").val(
            $(this).data("nomor_st_perpanjangan_lihat")
        );
        $("#tgl_lihat_st").val($(this).data("tgl_st_perpanjangan_lihat"));
        $("#tgl_mulai_lihat_st").val(
            $(this).data("tgl_mulai_st_perpanjangan_lihat")
        );
        $("#tgl_selesai_lihat_st").val(
            $(this).data("tgl_selesai_st_perpanjangan_lihat")
        );
        $("#file_lihat_st").attr(
            "href",
            app_url + "/files/" + $(this).data("file_st_perpanjangan_lihat")
        );
    }
});

// button lihat perpanjangan st stan
$(".btnLihatPerpanjanganStStan").click(function () {
    document.getElementById("data_st_stan").style.display = "none !important";
    $("#lihat_perpanjangan_st_stan").modal("show");

    $("#alasan_perpanjangan_st_stan").val(
        $(this).data("alasan_perpanjangan_st_stan")
    );
    $("#tgl_selesai_perpanjangan_stan").val(
        $(this).data("tgl_selesai_perpanjangan_stan")
    );
    $("#st_awal_stan").val($(this).data("st_awal_stan"));
    $("#file_st_awal_stan").attr(
        "href",
        app_url + "/files/" + $(this).data("file_st_awal_stan")
    );
    $("#keterangan").val($(this).data("keterangan"));
    $("#dokumen_pendukung").attr(
        "href",
        app_url + "/files/" + $(this).data("dokumen_pendukung")
    );

    $.ajax({
        type: "post",
        url: app_url + "/tubel/administrasi/get-stan",
        data: {
            _token: $("meta[name='csrf-token']").attr("content"),
            id: $(this).data("id_stan"),
        },
        success: function (response) {
            document.getElementById("data_st_stan").style.display = "";
            var data = JSON.parse(response);

            document.getElementById("nomor_st_stan").value =
                data.nomorStPerpanjangan;
            document.getElementById("tgl_st_stan").value =
                data.tglStPerpanjangan;
            document.getElementById("tgl_mulai_st_stan").value =
                data.tglMulaiStPerpanjangan;
            document.getElementById("tgl_selesai_st_stan").value =
                data.tglSelesaiStPerpanjangan;
            document.getElementById("file_st_stan").href =
                data.pathStPerpanjangan;
        },
        error: function (err) {
            console.log(err);
        },
    });
});

// button buat perpanjangan st
$(".btnPerpanjanganSt").click(function () {
    var fg_stan = $(this).data("stan");

    if (fg_stan == 0) {
        $("#add_perpanjangan_st").modal("show");
    } else {
        Swal.fire({
            position: "center",
            icon: "error",
            title: "Gagal",
            text: "tidak dapat menambah data (status tubel STAN)",
            showConfirmButton: false,
            timer: 2000,
        });
    }
});

// button kirim permohonan aktif kembali
$(".btnKirimPerpanjanganSt").click(function () {
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        title: "Yakin akan mengajukan permohonan perpanjangan ST Tubel?",
        text: "Data akan masuk ke tahap berikutnya!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Kirim!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url:
                    app_url + "/tubel/administrasi/perpanjangan-st-tubel/kirim",
                data: {
                    _token: token,
                    id: id,
                },
                success: function (response) {
                    if (response["status"] == 1) {
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil di kirim",
                            showConfirmButton: false,
                            timer: 1600,
                        });
                        location.reload();
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600,
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button perpanjang cuti
$(".btnPerpanjangCuti").click(function () {
    $("#add_perpanjangan_cuti_belajar").modal("show");
    $("#id_cuti_perpanjang").val($(this).data("idcuti")).change();
});

// button lihat usulan topik riset
$(".btnLihatUsulanRiset").click(function () {
    $("#lihat_usulan_riset").modal("show");
    $(".topik_riset_disahkan").attr("hidden", true);
    $(".riset_disetujui").attr("hidden", true);

    $("#nama_lihat").text(": " + $(this).data("nama"));
    $("#nip_lihat").text(": " + $(this).data("nip"));
    $("#pangkat_lihat").text(": " + $(this).data("pangkat"));
    $("#jabatan_lihat").text(": " + $(this).data("jabatan"));
    $("#jenjang_lihat").text(": " + $(this).data("jenjang"));
    $("#program_beasiswa_lihat").text(": " + $(this).data("program_beasiswa"));
    $("#lokasi_lihat").text(": " + $(this).data("lokasi"));
    $("#pt_lihat").text(": " + $(this).data("pt"));
    $("#prodi_lihat").text(": " + $(this).data("prodi"));
    $("#st_tubel_lihat").text(": " + $(this).data("st_tubel"));
    $("#tgl_mulai_selesai_lihat").text(
        ": " +
            $(this).data("tgl_mulai") +
            " s.d. " +
            $(this).data("tgl_selesai")
    );
    $("#tgl_st_tubel_lihat").text(": " + $(this).data("tgl_st_tubel"));
    $("#kep_pembebasan_lihat").text(": " + $(this).data("kep_pembebasan"));
    $(".tgl_kep_pembebasan_lihat").text(
        ": " + $(this).data("tgl_kep_pembebasan")
    );
    $("#tmt_kep_pembebasan_lihat").text(
        ": " + $(this).data("tmt_kep_pembebasan")
    );
    $("#email_lihat").text(": " + $(this).data("email"));
    $("#hp_lihat").text(": " + $(this).data("hp"));
    $("#alamat_lihat").text(": " + $(this).data("alamat"));

    $("#id_usulan_riset_lihat").val($(this).data("id_usulan_riset"));
    $("#topik_riset_lihat").val($(this).data("topik_riset"));
    $("#judul_riset_lihat").val($(this).data("judul_riset"));
    $("#dokumen_proposal_lihat").attr(
        "href",
        app_url + "/files/" + $(this).data("file_proposal")
    );

    if (
        $(this).data("case") == "Perguruan Tinggi setuju topik" ||
        $(this).data("case") == "Persetujuan PT dikirim" ||
        $(this).data("case") == "Topik Riset disahkan" ||
        $(this).data("case") == "Surat Persetujuan Topik diunggah"
    ) {
        $(".topik_riset_disahkan").removeAttr("hidden");

        $("#tgl_persetujuan_lihat").val($(this).data("tgl_persetujuan_pt"));
        $("#dokumen_persetujuan_lihat").attr(
            "href",
            app_url + "/files/" + $(this).data("file_persetujuan_pt")
        );

        if ($(this).data("case") == "Surat Persetujuan Topik diunggah") {
            $(".riset_disetujui").removeAttr("hidden");

            $("#nomor_nd_persetujuan_lihat").val(
                $(this).data("nomor_nd_persetujuan")
            );
            $("#tgl_nd_persetujuan_lihat").val(
                $(this).data("tgl_nd_persetujuan")
            );
            $("#dokumen_persetujuan_nd_lihat").attr(
                "href",
                app_url + "/files/" + $(this).data("dokumen_persetujuan_nd")
            );
        }
    }
});

// button kirim usulan topik riset
$(".btnKirimUsulanRiset").click(function () {
    var id = $(this).data("id");
    var act = $(this).data("act");
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        // title: title,
        title: "Yakin akan mengirimkan topik riset?",
        text: "Data akan masuk ke tahap berikutnya!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Kirim!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/administrasi/usulan-topik-riset/kirim",
                data: {
                    _token: token,
                    id: id,
                    act: act,
                    keterangan: null,
                },
                success: function (response) {
                    if (response.status == 1) {
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "usulan riset berhasil di kirim",
                            showConfirmButton: false,
                            timer: 1600,
                        });

                        location.reload();
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600,
                        });
                    }
                },
            });
        }
    });
});

// button persetujuan usulan topik riset
$(".btnPersetujuaniUsulanRiset").click(function () {
    $("#persetujuan_usulan_riset").modal("show");
    $("#btnSetujuUsulanRisetClick").hide();
    $("#btnTolakUsulanRisetClick").hide();
    $("#btnActionUsulanRiset").hide();

    $("#id_usulan_topik_riset_setuju").val($(this).data("id")).change();
});

// button persetujuan usulan topik riset > setuju
$("#rdSetujuUsulanRiset").click(function () {
    $("#btnActionUsulanRiset").show();
    $("#btnSetujuUsulanRisetClick").show();
    $("#btnTolakUsulanRisetClick").hide();
});

// button persetujuan usulan topik riset > tolak
$("#rdTolakUsulanRiset").click(function () {
    $("#btnActionUsulanRiset").show();
    $("#btnTolakUsulanRisetClick").show();
    $("#btnSetujuUsulanRisetClick").hide();
});

// button edit usulan riset
$(".btnEditUsulanRiset").click(function () {
    $("#edit_usulan_riset").modal("show");

    var option_selected =
        '<option value="' +
        $(this).data("id_pilihan_topik_edit") +
        '">' +
        $(this).data("pilihan_topik_edit") +
        "</option>";
    $("#pilihan_topik_edit").html(option_selected);

    $("#id_usulan_riset_edit")
        .val($(this).data("id_usulan_riset_edit"))
        .change();
    $("#pilihan_topik_edit")
        .val($(this).data("id_pilihan_topik_edit"))
        .change();
    $("#judul_riset_edit").val($(this).data("judul_riset_edit")).change();
    $("#file_dokumen_proposal_lama")
        .val($(this).data("file_dokumen_proposal_edit"))
        .change();
    $("#file_dokumen_proposal").attr(
        "href",
        app_url + "/files/" + $(this).data("file_dokumen_proposal_edit")
    );
});

// button hapus permohonan aktif kembali
$(".btnHapusUsulanRiset").click(function () {
    var id = $(this).data("id");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "delete",
                url: app_url + "/tubel/administrasi/usulan-topik-riset",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: id,
                },
                success: function (response) {
                    if (response["status"] == 1) {
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil di hapus",
                            showConfirmButton: false,
                            timer: 1800,
                        });
                        $("#usulantopikriset-" + id).remove();
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1800,
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button opsi hasil studi
$(".opsi_hasil_studi").on("change", function () {
    let opsi = $(this).val();

    switch (opsi) {
        case "1":
            $(".form_opsi").attr("hidden", true);
            $(".form_hasil_studi_lulus").attr("hidden", false);
            break;
        case "2":
            $(".form_opsi").attr("hidden", true);
            $(".form_hasil_studi_belum_lulus").attr("hidden", false);
            break;
        case "3":
            $(".form_opsi").attr("hidden", true);
            $(".form_hasil_studi_mengundurkan_diri").attr("hidden", false);
            break;
        case "4":
            $(".form_opsi").attr("hidden", true);
            $(".form_hasil_studi_drop_out").attr("hidden", false);
            break;
        default:
            $(".form_opsi").attr("hidden", true);
            break;
    }
});

// button opsi hasil studi
$(".opsi_hasil_studi_md_edit").on("change", function () {
    let opsi = $(this).val();

    switch (opsi) {
        case "1":
            $(".form_opsi_md_edit").attr("hidden", true);
            $(".form_edit_hasil_studi_lulus").attr("hidden", false);
            break;
        case "2":
            $(".form_opsi_md_edit").attr("hidden", true);
            $(".form_edit_hasil_studi_belum_lulus").attr("hidden", false);
            break;
        case "3":
            $(".form_opsi_md_edit").attr("hidden", true);
            $(".form_edit_hasil_studi_mengundurkan_diri").attr("hidden", false);
            break;
        case "4":
            $(".form_opsi_md_edit").attr("hidden", true);
            $(".form_edit_hasil_studi_drop_out").attr("hidden", false);
            break;
        default:
            $(".form_opsi_md_edit").attr("hidden", true);
            break;
    }
});

$("#opsi_hasil_akhir_studi").on("change", function () {
    let opsi = $(this).val();

    switch (opsi) {
        case "1":
            $(".form_opsi").attr("hidden", true);
            $(".form_hasil_studi_lulus").attr("hidden", false);
            break;
        case "3":
            $(".form_opsi").attr("hidden", true);
            $(".form_hasil_studi_mengundurkan_diri").attr("hidden", false);
            break;
        case "4":
            $(".form_opsi").attr("hidden", true);
            $(".form_hasil_studi_drop_out").attr("hidden", false);
            break;
        default:
            $(".form_opsi").attr("hidden", true);
            break;
    }
});

// button edit hasil studi
$(".btnEditHasilStudi").click(function () {
    $("#modal_edit_laporan_hasil_studi").modal("show");
    $(".form_opsi_md_edit").attr("hidden", true);
    $(".form_modal").attr("hidden", true);
    let status = $(this).data("status");
    let pt = $(this).data("pt");

    switch (status) {
        case "Lulus":
            $(".form_opsi_md_edit").attr("hidden", true);
            $(".form_edit_hasil_studi_lulus").attr("hidden", false);

            // card
            $(".file_penelitian").attr("hidden", false);
            $(".file_lps").attr("hidden", false);
            $(".file_skl").attr("hidden", false);
            $(".file_ijazah").attr("hidden", false);
            $(".file_transkrip").attr("hidden", false);

            $("#laporan_akhir_l_edit").val($(this).data("id")).change();

            // get data kelulusan
            $("#tgl_kelulusan_edit")
                .val($(this).data("tgl_kelulusan"))
                .change();

            $("#opsi_dokumen_penelitian_edit")
                .val($(this).data("tipe_dokumen_penelitian"))
                .change();

            $("#file_penelitian_edit").attr(
                "href",
                app_url +
                    "/files/" +
                    $(this).data("file_draft_dokumen_penelitian")
            );

            $("#file_penelitian_edit_lama")
                .val($(this).data("file_draft_dokumen_penelitian"))
                .change();

            $("#file_lps_edit").attr(
                "href",
                app_url + "/files/" + $(this).data("file_lps")
            );

            $("#file_lps_edit_lama").val($(this).data("file_lps")).change();

            // loop data perguruan tinggi
            for (let i = 1; i <= pt; i++) {
                $("#ipk_edit_" + i)
                    .val($(this).data("ipk_pt_" + i))
                    .change();

                $("#nomor_skl_edit_" + i)
                    .val($(this).data("no_skl_pt_" + i))
                    .change();

                $("#tgl_skl_edit_" + i)
                    .val($(this).data("tgl_skl_pt_" + i))
                    .change();

                if ($(this).data("file_skl_pt_" + i) == "") {
                    $(".file_skl_" + i).attr("hidden", true);
                } else {
                    $(".file_skl_" + i).attr("hidden", false);
                    $("#file_skl_" + i).attr(
                        "href",
                        app_url + "/files/" + $(this).data("file_skl_pt_" + i)
                    );
                }

                $("#file_skl_lama_" + i)
                    .val($(this).data("file_skl_pt_" + i))
                    .change();

                $("#nomor_ijazah_edit_" + i)
                    .val($(this).data("no_ijazah_pt_" + i))
                    .change();

                $("#tgl_ijazah_edit_" + i)
                    .val($(this).data("tgl_ijazah_pt_" + i))
                    .change();

                if ($(this).data("file_ijazah_pt_" + i) == "") {
                    $(".file_ijazah_" + i).attr("hidden", true);
                } else {
                    $(".file_ijazah_" + i).attr("hidden", false);
                    $("#file_ijazah_" + i).attr(
                        "href",
                        app_url +
                            "/files/" +
                            $(this).data("file_ijazah_pt_" + i)
                    );
                }

                $("#file_ijazah_lama_" + i)
                    .val($(this).data("file_ijazah_pt_" + i))
                    .change();

                $("#nomor_transkrip_edit_" + i)
                    .val($(this).data("no_transkrip_pt_" + i))
                    .change();

                $("#tgl_transkrip_edit_" + i)
                    .val($(this).data("tgl_transkrip_pt_" + i))
                    .change();

                if ($(this).data("file_transkrip_pt_" + i) == "") {
                    $(".file_transkrip_" + i).attr("hidden", true);
                } else {
                    $(".file_transkrip_" + i).attr("hidden", false);
                    $("#file_transkrip_" + i).attr(
                        "href",
                        app_url +
                            "/files/" +
                            $(this).data("file_transkrip_pt_" + i)
                    );
                }

                $("#file_transkrip_lama_" + i)
                    .val($(this).data("file_transkrip_pt_" + i))
                    .change();
            }
            break;

        case "Belum Lulus":
            $(".form_opsi_md_edit").attr("hidden", true);
            $(".form_edit_hasil_studi_belum_lulus").attr("hidden", false);

            // card
            $(".file_draft_dokumen_penelitian").attr("hidden", false);
            $(".file_timeline_studi").attr("hidden", false);

            $("#opsi_tahapan_studi_bl_edit")
                .val($(this).data("tahapan_studi"))
                .change();

            $("#file_timeline_studi_lama")
                .val($(this).data("file_timeline"))
                .change();

            $("#file_timeline_studi").attr(
                "href",
                app_url + "/files/" + $(this).data("file_timeline")
            );

            $("#opsi_dokumen_penelitian_bl_edit")
                .val($(this).data("tipe_dokumen_penelitian"))
                .change();

            $("#file_draft_dokumen_penelitian_lama")
                .val($(this).data("file_draft_dokumen_penelitian"))
                .change();

            $("#file_draft_dokumen_penelitian").attr(
                "href",
                app_url +
                    "/files/" +
                    $(this).data("file_draft_dokumen_penelitian")
            );

            break;

        case "Drop Out":
            $(".form_opsi_md_edit").attr("hidden", true);
            $(".form_edit_hasil_studi_drop_out").attr("hidden", false);

            // card
            $(".file_surat_keterangan_do").attr("hidden", false);

            $("#semester_do_edit").val($(this).data("semester_do")).change();

            $("#tahun_do_edit").val($(this).data("tahun_do")).change();

            $("#no_surat_keterangan_do_edit")
                .val($(this).data("no_surat_do"))
                .change();

            $("#tgl_surat_keterangan_do_edit")
                .val($(this).data("tgl_surat_keterangan_do"))
                .change();

            $("#file_surat_keterangan_do_lama")
                .val($(this).data("file_surat_keterangan_do"))
                .change();

            $("#file_surat_keterangan_do").attr(
                "href",
                app_url + "/files/" + $(this).data("file_surat_keterangan_do")
            );
            break;

        case "Mengundurkan Diri":
            $(".form_opsi_md_edit").attr("hidden", true);
            $(".form_edit_hasil_studi_mengundurkan_diri").attr("hidden", false);

            // card
            $(".file_dokumen_permohonan").attr("hidden", false);
            $(".file_dokumen_surat_persetujuan").attr("hidden", false);

            $("#alasan_pengunduran_diri_md_edit")
                .val($(this).data("alasan_pengunduran_diri"))
                .change();

            $("#mundur_semester_md_edit")
                .val($(this).data("mundur_semester"))
                .change();

            $("#tahun_akademik_md_edit")
                .val($(this).data("tahun_akademik"))
                .change();

            $("#file_dokumen_permohonan_lama")
                .val($(this).data("file_permohonan"))
                .change();

            $("#file_dokumen_permohonan").attr(
                "href",
                app_url + "/files/" + $(this).data("file_permohonan")
            );

            $("#no_surat_persetujuan_md_edit")
                .val($(this).data("no_surat_persetujuan"))
                .change();

            $("#tgl_surat_persetujuan_md_edit")
                .val($(this).data("tgl_surat_persetujuan"))
                .change();

            $("#file_dokumen_surat_persetujuan_lama")
                .val($(this).data("file_dokumen_surat_persetujuan"))
                .change();

            $("#file_dokumen_surat_persetujuan").attr(
                "href",
                app_url +
                    "/files/" +
                    $(this).data("file_dokumen_surat_persetujuan")
            );
            break;
        default:
            $(".form_opsi_md_edit").attr("hidden", true);
            break;
    }
});

// button edit hasil studi
$(".btnEditHasilAkhirStudi").click(function () {
    $("#modal_edit_laporan_akhir_hasil_studi").modal("show");
    $(".form_opsi_md_edit").attr("hidden", true);
    $(".form_modal").attr("hidden", true);
    let status = $(this).data("status");
    let pt = $(this).data("pt");

    switch (status) {
        case "Lulus":
            $(".form_opsi_md_edit").attr("hidden", true);
            $(".form_edit_hasil_studi_lulus").attr("hidden", false);

            // card
            $(".file_penelitian").attr("hidden", false);
            $(".file_lps").attr("hidden", false);
            $(".file_skl").attr("hidden", false);
            $(".file_ijazah").attr("hidden", false);
            $(".file_transkrip").attr("hidden", false);

            $("#laporan_akhir_l_edit").val($(this).data("id")).change();

            // get data kelulusan
            $("#tgl_kelulusan").val($(this).data("tgl_kelulusan")).change();

            $("#opsi_dokumen_penelitian")
                .val($(this).data("tipe_dokumen_penelitian"))
                .change();

            $("#file_penelitian_l_edit").attr(
                "href",
                app_url +
                    "/files/" +
                    $(this).data("file_draft_dokumen_penelitian")
            );

            $("#file_penelitian_l_edit_lama")
                .val($(this).data("file_draft_dokumen_penelitian"))
                .change();

            $("#file_lps_l_edit").attr(
                "href",
                app_url + "/files/" + $(this).data("file_lps")
            );

            $("#file_lps_l_edit_lama").val($(this).data("file_lps")).change();

            // loop data perguruan tinggi
            for (let i = 1; i <= pt; i++) {
                $("#ipk_l_edit_" + i)
                    .val($(this).data("ipk_pt_" + i))
                    .change();

                $("#nomor_skl_l_edit_" + i)
                    .val($(this).data("no_skl_pt_" + i))
                    .change();

                $("#tgl_skl_l_edit_" + i)
                    .val($(this).data("tgl_skl_pt_" + i))
                    .change();

                if ($(this).data("file_skl_pt_" + i) == "-") {
                    $(".file_skl_" + i).attr("hidden", true);
                    $(".msg_file_skl_" + i).attr("hidden", false);
                } else {
                    $("#file_skl_" + i).attr(
                        "href",
                        app_url + "/files/" + $(this).data("file_skl_pt_" + i)
                    );
                }

                $("#file_skl_" + i).attr(
                    "href",
                    app_url + "/files/" + $(this).data("file_skl_pt_" + i)
                );

                $("#file_skl_lama_" + i)
                    .val($(this).data("file_skl_pt_" + i))
                    .change();

                $("#nomor_ijazah_l_edit_" + i)
                    .val($(this).data("no_ijazah_pt_" + i))
                    .change();

                $("#tgl_ijazah_l_edit_" + i)
                    .val($(this).data("tgl_ijazah_pt_" + i))
                    .change();

                $("#file_ijazah_" + i).attr(
                    "href",
                    app_url + "/files/" + $(this).data("file_ijazah_pt_" + i)
                );

                $("#file_ijazah_lama_" + i)
                    .val($(this).data("file_ijazah_pt_" + i))
                    .change();

                $("#nomor_transkrip_l_edit_" + i)
                    .val($(this).data("no_transkrip_pt_" + i))
                    .change();

                $("#tgl_transkrip_l_edit_" + i)
                    .val($(this).data("tgl_transkrip_pt_" + i))
                    .change();

                $("#file_transkrip_" + i).attr(
                    "href",
                    app_url + "/files/" + $(this).data("file_transkrip_pt_" + i)
                );

                $("#file_transkrip_lama_" + i)
                    .val($(this).data("file_transkrip_pt_" + i))
                    .change();
            }

            break;

        case "Drop Out":
            $(".form_opsi_md_edit").attr("hidden", true);
            $(".form_edit_hasil_studi_drop_out").attr("hidden", false);

            // card
            $(".file_surat_keterangan_do").attr("hidden", false);

            $("#semester_do_edit_akhir")
                .val($(this).data("semester_do"))
                .change();

            $("#tahun_do_edit_akhir").val($(this).data("tahun_do")).change();

            $("#no_surat_keterangan_do_edit_akhir")
                .val($(this).data("no_surat_do"))
                .change();

            $("#tgl_surat_keterangan_do_edit_akhir")
                .val($(this).data("tgl_surat_keterangan_do"))
                .change();

            $("#file_surat_keterangan_do_lama_akhir")
                .val($(this).data("file_surat_keterangan_do"))
                .change();

            $("#file_surat_keterangan_do_akhir").attr(
                "href",
                app_url + "/files/" + $(this).data("file_surat_keterangan_do")
            );
            break;

        case "Mengundurkan Diri":
            $(".form_opsi_md_edit").attr("hidden", true);
            $(".form_edit_hasil_studi_mengundurkan_diri").attr("hidden", false);

            // card
            $(".file_dokumen_permohonan").attr("hidden", false);
            $(".file_dokumen_surat_persetujuan").attr("hidden", false);

            $("#alasan_pengunduran_diri_md_edit_akhir")
                .val($(this).data("alasan_pengunduran_diri"))
                .change();

            $("#mundur_semester_md_edit_akhir")
                .val($(this).data("mundur_semester"))
                .change();

            $("#tahun_akademik_md_edit_akhir")
                .val($(this).data("tahun_akademik"))
                .change();

            $("#file_dokumen_permohonan_lama_akhir")
                .val($(this).data("file_permohonan"))
                .change();

            $("#file_dokumen_permohonan_akhir").attr(
                "href",
                app_url + "/files/" + $(this).data("file_permohonan")
            );

            $("#no_surat_persetujuan_md_edit_akhir")
                .val($(this).data("no_surat_persetujuan"))
                .change();

            $("#tgl_surat_persetujuan_md_edit_akhir")
                .val($(this).data("tgl_surat_persetujuan"))
                .change();

            $("#file_dokumen_surat_persetujuan_lama_akhir")
                .val($(this).data("file_dokumen_surat_persetujuan"))
                .change();

            $("#file_dokumen_surat_persetujuan_akhir").attr(
                "href",
                app_url +
                    "/files/" +
                    $(this).data("file_dokumen_surat_persetujuan")
            );
            break;
        default:
            $(".form_opsi_md_edit").attr("hidden", true);
            break;
    }
});

// button kirim hasil studi
$(".btnKirimLaporanStudi").click(function () {
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        title: "Yakin akan mengirim laporan studi?",
        text: "Data akan masuk ke tahap berikutnya!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Kirim!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/administrasi/selesai-studi/kirim",
                data: {
                    _token: token,
                    id: id,
                },
                success: function (response) {
                    if (response.status == 1) {
                        swalSuccess("data berhasil di kirim");
                        location.reload();
                    } else {
                        swalError(response.message);
                    }
                },
            });
        }
    });
});

// button kirim hasil akhir
$(".btnKirimHasilAkhir").click(function () {
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        title: "Yakin akan mengirim hasil akhir studi?",
        text: "Data akan masuk ke tahap berikutnya!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Kirim!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/administrasi/hasil-studi/kirim",
                data: {
                    _token: token,
                    id: id,
                },
                success: function (response) {
                    console.log(response);
                    // if (response.status == 1) {
                    //     swalSuccess("data berhasil di kirim");
                    //     location.reload();
                    // } else {
                    //     swalError(response['message']);
                    // }
                },
            });
        }
    });
});

// button kirim hasil akhir
$(".btnKirimIjazah").click(function () {
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        title: "Yakin akan mengirim ijazah?",
        text: "Data akan masuk ke tahap berikutnya!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Kirim!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/administrasi/hasil-studi/kirim-ijazah",
                data: {
                    _token: token,
                    id: id,
                },
                success: function (response) {
                    if (response.status == 1) {
                        swalSuccess("data berhasil di kirim");
                        location.reload();
                    } else {
                        swalError(response["message"]);
                    }
                },
            });
        }
    });
});

// button rekam ijazah
$(".btnRekamIjazah").click(function () {
    $("#id_hasil_studi_ijazah").val($(this).data("id"));
});

// button rekam ijazah
$(".btnEditIjazah").click(function () {
    $("#edit_laporan_akhir_hasil_studi_ijazah").modal("show");
    $("#id_laporan_akhir_ijazah_edit").val($(this).data("id"));
    let pt = $(this).data("pt");

    // loop data perguruan tinggi
    for (let i = 1; i <= pt; i++) {
        $("#nomor_ijazah_ha_edit_" + i)
            .val($(this).data("no_ijazah_pt_" + i))
            .change();

        $("#tgl_ijazah_ha_edit_" + i)
            .val($(this).data("tgl_ijazah_pt_" + i))
            .change();

        if ($(this).data("file_ijazah_pt_" + i) == "") {
            $(".file_ijazah_ha_" + i).attr("hidden", true);
        } else {
            $(".file_ijazah_ha_" + i).attr("hidden", false);
            $("#file_ijazah_ha_" + i).attr(
                "href",
                app_url + "/files/" + $(this).data("file_ijazah_pt_" + i)
            );
        }

        $("#file_ijazah_ha_lama_" + i)
            .val($(this).data("file_ijazah_pt_" + i))
            .change();

        $("#nomor_transkrip_ijazah_ha_edit_" + i)
            .val($(this).data("no_transkrip_pt_" + i))
            .change();

        $("#tgl_transkrip_ijazah_ha_edit_" + i)
            .val($(this).data("tgl_transkrip_pt_" + i))
            .change();

        $("#file_transkrip_ha_" + i).attr(
            "href",
            app_url + "/files/" + $(this).data("file_transkrip_pt_" + i)
        );

        $("#file_transkrip_ha_lama_" + i)
            .val($(this).data("file_transkrip_pt_" + i))
            .change();
    }
});

// tiket hasil akhir
$(document).ready(function () {
    let id_selesai_studi = $("#id_selesai_studi").val();

    if (id_selesai_studi != null) {
        $("#tiket_akhir").attr("hidden", true);

        let success = "badge-light-success";
        let danger = "badge-light-danger";

        $.ajax({
            type: "post",
            url:
                app_url +
                "/tubel/administrasi/selesai-studi/generate/hasil-akhir",
            data: {
                _token: $("meta[name='csrf-token']").attr("content"),
                id: id_selesai_studi,
            },
            success: function (response) {
                $("#tiket_akhir").attr("hidden", false);

                $("#status_hasil_akhir").text(response.logStatusProbis.output);
                $("#status_hasil_akhir").addClass(success);

                $("#tiket_hasil_akhir").text(
                    response.logStatusProbis.nomorTiket
                );
            },
        });
    }
});

// spinner / loading button
function BtnLoading(elem) {
    $(elem).attr("data-original-text", $(elem).html());
    $(elem).prop("disabled", true);
    $(elem).html('<i class="spinner-border spinner-border-sm"></i>  Loading');
}

// spinner off / reset button
function BtnReset(elem) {
    $(elem).prop("disabled", false);
    $(elem).html($(elem).attr("data-original-text"));
}

// modal form add laporan perkembangan studi
$("#formAddLps").validate({
    rules: {
        semesterke: {
            required: true,
            digits: true,
            maxlength: 2,
        },
        tahun: {
            required: true,
            digits: true,
            minlength: 4,
            maxlength: 4,
        },
        ipk: {
            required: true,
            number: true,
        },
        jumlah_sks: {
            required: true,
            digits: true,
            maxlength: 3,
        },
    },
});

// modal form edit laporan perkembangan studi
$("#formEditLps").validate({
    rules: {
        esemesterke: {
            required: true,
            digits: true,
            maxlength: 2,
        },
        etahun: {
            required: true,
            digits: true,
            minlength: 4,
            maxlength: 4,
        },
        eipk: {
            required: true,
            number: true,
        },
        ejumlah_sks: {
            required: true,
            digits: true,
            maxlength: 3,
        },
    },
});

// select2
$(document).ready(function () {

    $("#pilihan_topik_add").select2({
        placeholder: "Pilih topik riset",
        allowClear: true,
    });

    // opsi hasil studi
    $selectElement = $("#opsi_hasil_studi").select2({
        placeholder: "Pilih hasil studi",
        allowClear: false,
        minimumResultsForSearch: -1,
    });
});

$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    // get jenjang (add)
    $("#tambah_perguruan_tinggi").select2({
        placeholder: "Pilih perguruan tinggi",
        minimumInputLength: 3,
        ajax: {
            url: app_url + "/tubel/administrasi/get-pt",
            dataType: "json",
            type: "post",
            delay: 300,
            data: function (params) {
                return {
                    search: params.term,
                };
            },
            processResults: function (response) {
                return {
                    results: response,
                };
            },
        },
    });

    // get program studi (add)
    $("#tambah_pt_prodi").select2({
        placeholder: "Pilih program studi",
        minimumInputLength: 3,
        ajax: {
            url: app_url + "/tubel/administrasi/get-prodi",
            dataType: "json",
            type: "post",
            delay: 300,
            data: function (params) {
                return {
                    search: params.term,
                };
            },
            processResults: function (response) {
                return {
                    results: response,
                };
            },
        },
    });

    // get negara (add)
    $("#tambah_pt_negara").select2({
        placeholder: "Pilih negara",
        minimumInputLength: 3,
        ajax: {
            url: app_url + "/tubel/administrasi/get-negara",
            dataType: "json",
            type: "post",
            delay: 300,
            data: function (params) {
                return {
                    search: params.term,
                };
            },
            processResults: function (response) {
                return {
                    results: response,
                };
            },
        },
    });
});
