$(".datepick").flatpickr();
$(".date-search").flatpickr({
    dateFormat: "d-m-Y",
});

// button rekam kep
$(".btnRekamKep").click(function () {
    $("#rekam_kep_cuti").modal("show");

    $("#id_pengaktifan_kembali").val($(this).data("idcuti")).change();
    $("#namaRekam").text(": " + $(this).data("nama"));
    $("#nipRekam").text(": " + $(this).data("nip"));
    $("#tglMulai").text(": " + $(this).data("tglmulai"));
    $("#tglSelesai").text(": " + $(this).data("tglselesai"));
});

// button lihat kep
$(".btnLihatKep").click(function () {
    $("#lihat_kep_cuti").modal("show");

    $("#id_cuti").val($(this).data("idcuti")).change();
    $("#namaLihat").text(": " + $(this).data("nama"));
    $("#nipLihat").text(": " + $(this).data("nip"));
    $("#tglMulaiLihat").text(": " + $(this).data("tglmulai"));
    $("#tglSelesaiLihat").text(": " + $(this).data("tglselesai"));
});

// button lihat permohonan aktif
$(".btnLihatPermohonanAktif").click(function () {
    $("#lihat_pengaktifan_kembali").modal("show");

    $("#lihat_nama").text(": " + $(this).data("nama_lihat"));
    $("#lihat_nip").text(": " + $(this).data("nip_lihat"));
    $("#lihat_pangkat").text(": " + $(this).data("pangkat_lihat"));
    $("#lihat_jabatan").text(": " + $(this).data("jabatan_lihat"));
    $("#lihat_jenjang").text(": " + $(this).data("jenjang_lihat"));
    $("#lihat_program_beasiswa").text(
        ": " + $(this).data("program_beasiswa_lihat")
    );
    $("#lihat_lokasi").text(": " + $(this).data("lokasi_lihat"));
    // $('#lihat_pt').text(': ' + $(this).data('nama')
    // $('#lihat_prodi').text(': ' + $(this).data('nama')
    $("#lihat_st_tubel").text(": " + $(this).data("st_tubel_lihat"));
    $("#lihat_tgl_st_tubel").text(": " + $(this).data("tgl_st_tubel_lihat"));
    $("#lihat_tgl_mulai_selesai").text(
        ": " +
            $(this).data("tgl_mulai_lihat") +
            " - " +
            $(this).data("tgl_selesai_lihat")
    );
    $("#lihat_kep_pembebasan").text(
        ": " + $(this).data("kep_pembebasan_lihat")
    );
    $("#lihat_tgl_kep_pembebasan").text(
        ": " + $(this).data("tgl_kep_pembebasan_lihat")
    );
    $("#lihat_tmt_kep_pembebasan").text(
        ": " + $(this).data("tmt_kep_pembebasan_lihat")
    );
    $("#lihat_email").text(": " + $(this).data("email_lihat"));
    $("#lihat_hp").text(": " + $(this).data("hp_lihat"));
    $("#lihat_alamat").text(": " + $(this).data("alamat_lihat"));

    $("#tgl_mulai_aktif_lihat").val($(this).data("tglmulaiaktif_lihat"));
    $("#nomor_surat_aktif_kembali_lihat").val(
        $(this).data("nosuratkembali_lihat")
    );
    $("#tgl_surat_lihat").val($(this).data("tglsurat_lihat"));
    $("#file_dokumen_lihat").attr(
        "href",
        app_url + "/files/" + $(this).data("file_lihat")
    );
});

// button periksa pengaktifan kembali
$(".btnPeriksaPermohonanAktif").click(function () {
    $("#periksa_pengaktifan_kembali").modal("show");

    $("#id_pengaktifan_kembali").val($(this).data("id"));

    $("#get_nama").text(": " + $(this).data("nama"));
    $("#get_nip").text(": " + $(this).data("nip"));
    $("#get_pangkat").text(": " + $(this).data("pangkat"));
    $("#get_jabatan").text(": " + $(this).data("jabatan"));
    $("#get_jenjang").text(": " + $(this).data("jenjang"));
    $("#get_program_beasiswa").text(": " + $(this).data("program_beasiswa"));
    $("#get_lokasi").text(": " + $(this).data("lokasi"));
    // $('#get_pt').text(': ' + $(this).data('nama')
    // $('#get_prodi').text(': ' + $(this).data('nama')
    $("#get_st_tubel").text(": " + $(this).data("st_tubel"));
    $("#get_tgl_st_tubel").text(": " + $(this).data("tgl_st_tubel"));
    $("#get_tgl_mulai_selesai").text(
        ": " + $(this).data("tgl_mulai") + " - " + $(this).data("tgl_selesai")
    );
    $("#get_kep_pembebasan").text(": " + $(this).data("kep_pembebasan"));
    $("#get_tgl_kep_pembebasan").text(
        ": " + $(this).data("tgl_kep_pembebasan")
    );
    $("#get_tmt_kep_pembebasan").text(
        ": " + $(this).data("tmt_kep_pembebasan")
    );
    $("#get_email").text(": " + $(this).data("email"));
    $("#get_hp").text(": " + $(this).data("hp"));
    $("#get_alamat").text(": " + $(this).data("alamat"));

    $("#tgl_mulai_aktif_periksa").val($(this).data("tglmulaiaktif"));
    $("#nomor_surat_aktif_kembali_periksa").val($(this).data("nosuratkembali"));
    $("#tgl_surat_periksa").val($(this).data("tglsurat"));
    $("#file_dokumen_periksa").attr(
        "href",
        app_url + "/files/" + $(this).data("file")
    );
    $.fn.modal.Constructor.prototype._enforceFocus = function () {};
});

// button tolak permohonan aktif
$(".btnTolakPengaktifanKembali").click(function () {
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        input: "textarea",
        inputLabel: "Alasan Tolak",
        inputPlaceholder: "Masukan Alasan Penolakan",
        inputAttributes: {
            "aria-label": "Masukan Alasan Penolakan",
        },
        title: "Yakin akan menolak data?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        confirmButtonText: "Tolak!",
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/pengaktifan-kembali/tolak",
                data: {
                    _token: token,
                    id_pengaktifan_kembali: $("#id_pengaktifan_kembali").val(),
                    keterangan: result.value,
                },
                success: function (response) {
                    if (response.status == 0) {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600,
                        });
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil di tolak",
                            showConfirmButton: false,
                            timer: 1600,
                        });
                        location.reload();
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button setujui permohonan aktif
$(".btnSetujuPengaktifanKembali").click(function () {
    Swal.fire({
        title: "Yakin akan menyetujui data?",
        text: "Data akan masuk ke tahap berikutnya!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Setuju!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            // alert(id);
            $.ajax({
                type: "post",
                url: app_url + "/tubel/pengaktifan-kembali/setuju",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: $("#id_pengaktifan_kembali").val(),
                },
                success: function (response) {
                    // console.log(response);
                    location.reload();

                    if (response["status"] == 0) {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response["message"],
                            showConfirmButton: false,
                            // width: 600,
                            // padding: '5em',
                            timer: 1600,
                        });
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil di setujui",
                            showConfirmButton: false,
                            timer: 1600,
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button rekam permohonan aktif
$(".btnRekamPermohonanAktif").click(function () {
    $("#rekam_permohonan_aktif").modal("show");

    $("#id_permohonan_aktif_kembali").val(
        $(this).data("id_permohonan_aktif_kembali")
    );
    $("#nip_rekam").text(": " + $(this).data("nip"));
    $("#nama_rekam").text(": " + $(this).data("nama"));
    $("#jenjang_pendidikan_rekam").text(": " + $(this).data("jenjang"));
    $("#lokasi_pendidikan_rekam").text(": " + $(this).data("lokasi"));
    $("#tgl_aktif_kembali_rekam").text(": " + $(this).data("tgl_mulai_aktif"));
});

// button lihat kep
$(".btnLihatKepPembebasan").click(function () {
    $("#lihat_kep_pembebasan").modal("show");

    $("#nip_lihat").text(": " + $(this).data("nip"));
    $("#nama_lihat").text(": " + $(this).data("nama"));
    $("#tgl_aktif_kembali_lihat").text(": " + $(this).data("tglaktifkembali"));
    $("#nomor_st_lihat").text(": " + $(this).data("nomorst"));
    $("#tgl_st_lihat").text(": " + $(this).data("tglst"));
    // $("#tgl_mulai_selesai_lihat").text(": " + $(this).data('tglmulaist') + " s.d. " + $(this).data('tglselesaist'))
    $("#tgl_mulai_lihat").text(": " + $(this).data("tglmulaist"));
    $("#tgl_selesai_lihat").text(": " + $(this).data("tglselesaist"));

    $("#nomor_surat_tugas_lihat").val($(this).data("nomorkep"));
    $("#tgl_surat_tugas_lihat").val($(this).data("tglkep"));
    $("#tmt_lihat").val($(this).data("tmt"));
    $("#file_kep").attr("href", app_url + "/files/" + $(this).data("file"));
});

// placeholder advance search
$(window).on("load", function () {
    // jenjang
    $selectElement = $("#search_jenjang").select2({
        placeholder: "jenjang pendidikan",
        allowClear: true,
        minimumResultsForSearch: -1,
    });

    // lokasi
    $selectElement = $("#search_lokasi").select2({
        placeholder: "lokasi pendidikan",
        allowClear: true,
        minimumResultsForSearch: -1,
    });
});

// date format
$(".datepick").flatpickr({
    dateFormat: "d-m-Y",
});
