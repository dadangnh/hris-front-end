// button rekam cuti by admin
$(".btnRekamCuti").click(function () {
    $("#rekam_cuti").modal("show");

    $("#niprekam").val("");
    $("#formInput").hide();
});

// button periksa cuti
$(".btnPeriksa").click(function () {
    $("#periksa_cuti").modal("show");

    $("#idCuti").val($(this).data("idcuti"));
    $("#alasan_cuti").val($(this).data("alasancuti"));
    $("#tglMulai").val($(this).data("tglmulai"));
    $("#tglSelesai").val($(this).data("tglselesai"));
    $("#tglSurat").val($(this).data("tglsurat"));
    $("#surat_izin_cuti_kampus").val($(this).data("suratkampus"));
    $("#surat_izin_cuti_sponsor").val($(this).data("suratsponsor"));
    $("#tglSuratSponsor").val($(this).data("tglsponsor"));
    $("#tglSuratKampus").val($(this).data("tglkampus"));
    $("#filesuratkampus").attr(
        "href",
        app_url + "/files/" + $(this).data("filesuratkampus")
    );
    $("#filesuratsponsor").attr(
        "href",
        app_url + "/files/" + $(this).data("filesuratsponsor")
    );
    $.fn.modal.Constructor.prototype._enforceFocus = function () {};
});

// button lihat cuti
$(".btnLihat").click(function () {
    $("#lihat_cuti").modal("show");

    $("#tgl_mulai_st_lihat").val($(this).data("tglmulai"));
    $("#tgl_selesai_st_lihat").val($(this).data("tglselesai"));
    $("#alasan_cuti_lihat").val($(this).data("alasancuti"));
    $("#surat_izin_cuti_kampus_lihat").val($(this).data("suratkampus"));
    $("#tgl_surat_kampus_lihat").val($(this).data("tglkampus"));
    $("#surat_izin_cuti_sponsor_lihat").val($(this).data("suratsponsor"));
    $("#tgl_surat_sponsor_lihat").val($(this).data("tglsponsor"));
    $("#file_surat_kampus_lihat").attr(
        "href",
        app_url + "/files/" + $(this).data("filesuratkampus")
    );
    $("#file_surat_sponsor").attr(
        "href",
        app_url + "/files/" + $(this).data("filesuratsponsor")
    );

    if ($(this).data("tab") == "selesai" && $(this).data("nomorkep") != "") {
        document.getElementById("data_kep").style.display = "";

        $("#nomor_kep_lihat").val($(this).data("nomorkep"));
        $("#tgl_kep_lihat").val($(this).data("tglkep"));
        $("#tmt_penempatan_lihat").val($(this).data("tgltmt"));
        $("#lokasi_penempatan_lihat").val($(this).data("lokasipenempatan"));
        $("#file_kep_lihat").attr(
            "href",
            app_url + "/files/" + $(this).data("filekep")
        );
    }
});

// button edit cuti admin
$(".btnEdit").click(function () {
    $("#edit_cuti_admin").modal("show");

    $("#id_tubel_edit").val($(this).data("idtubel_edit"));
    $("#id_cuti_edit").val($(this).data("idcuti_edit"));
    $("#tgl_mulai_cuti_edit").val($(this).data("tglmulai_edit"));
    $("#tgl_selesai_cuti_edit").val($(this).data("tglselesai_edit"));
    $("#alasan_cuti_edit").val($(this).data("alasancuti_edit"));
    $("#surat_izin_cuti_kampus_edit").val($(this).data("suratkampus_edit"));
    $("#tgl_surat_kampus_edit").val($(this).data("tglkampus_edit"));
    $("#surat_izin_cuti_sponsor_edit").val($(this).data("suratsponsor_edit"));
    $("#tgl_surat_sponsor_edit").val($(this).data("tglsponsor_edit"));
    $("#file_cuti_edit_lama").val($(this).data("filesuratkampus_edit"));
    $("#file_cuti_edit").attr(
        "href",
        app_url + "/files/" + $(this).data("filesuratkampus_edit")
    );
    $("#file_sponsors_edit_lama").val($(this).data("filesuratsponsor_edit"));
    $("#file_sponsors_edit").attr(
        "href",
        app_url + "/files/" + $(this).data("filesuratsponsor_edit")
    );
});

// button setuju periksa cuti aksi
$("#btnSetujuCuti").click(function () {
    var act = $(this).data("act");
    var id = $("#idCuti").val();

    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        title: "Yakin akan menyetujui data?",
        text: "Data akan masuk ke tahap berikutnya!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#40BA50",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Setuju!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            // alert(id);
            $.ajax({
                type: "post",
                url: app_url + "/tubel/cuti/updatestatus",
                data: {
                    _token: token,
                    caseOutputId: act,
                    id: id,
                    keterangan: null,
                },
                success: function (response) {
                    // console.log(response);
                    location.reload();

                    if (response["status"] === 0) {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response["message"],
                            showConfirmButton: false,
                            // width: 600,
                            // padding: '5em',
                            timer: 1600,
                        });
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil di setujui",
                            showConfirmButton: false,
                            timer: 1600,
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button tolak cuti aksi
$("#btnTolakCuti").click(function () {
    var act = $(this).data("act");
    var idCuti = $("#idCuti").val();
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        input: "textarea",
        inputLabel: "Alasan Tolak",
        inputPlaceholder: "Masukan Alasan Penolakan",
        inputAttributes: {
            "aria-label": "Masukan Alasan Penolakan",
        },
        title: "Yakin akan menolak data?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Tolak!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/cuti/updatestatus",
                data: {
                    _token: token,
                    caseOutputId: act,
                    id: idCuti,
                    keterangan: result.value,
                },
                success: function (response) {
                    console.log(response);
                    if ((response.status = 0)) {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600,
                        });
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "data berhasil di tolak",
                            showConfirmButton: false,
                            timer: 1600,
                        });
                        location.reload();
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button rekam cuti > get pegawai
$("#btnGetPegawai").click(function () {
    $("#formInput").hide();

    if ($("#niprekam").val().length == 18) {
        BtnLoading("#btnGetPegawai");

        $.ajax({
            type: "get",
            url: app_url + "/tubel/cuti/getinduktubel",
            data: {
                nip: $("#niprekam").val(),
            },
            success: function (response) {
                // console.log(response)
                if (response.status == 0) {
                    Swal.fire({
                        position: "center",
                        icon: "error",
                        title: "Gagal!",
                        text: response.message,
                        showConfirmButton: false,
                        timer: 1500,
                    });
                } else {
                    $("#formInput").show();

                    // $('#niprekam').prop('disabled', true);

                    $("#idTubel").val(response.id);

                    $("#get_nama").text(": " + response.namaPegawai);
                    $("#get_nip").text(": " + response.nip18);
                    $("#get_pangkat").text(": " + response.namaPangkat);
                    $("#get_jabatan").text(": " + response.namaJabatan);
                    $("#get_jenjang").text(
                        ": " + response.jenjangPendidikanId.jenjangPendidikan
                    );
                    $("#get_program_beasiswa").text(
                        ": " + response.programBeasiswa
                    );
                    $("#get_lokasi").text(
                        ": " + response.lokasiPendidikanId.lokasi
                    );
                    // $('#get_pt').text(': ' + response.)
                    // $('#get_prodi').text(': ' + response.);
                    $("#get_st_tubel").text(": " + response.nomorSt);
                    $("#get_tgl_st_tubel").text(": " + response.tglSt);
                    $("#get_tgl_mulai_selesai").text(
                        ": " +
                            response.tglMulaiSt +
                            " s.d. " +
                            response.tglSelesaiSt
                    );
                    $("#get_kep_pembebasan").text(
                        ": " + response.nomorKepPembebasan
                    );
                    $("#get_tgl_kep_pembebasan").text(
                        ": " + response.tglKepPembebasan
                    );
                    $("#get_tmt_kep_pembebasan").text(
                        ": " + response.tmtKepPembebasan
                    );
                    $("#get_email").text(": " + response.emailNonPajak);
                    $("#get_hp").text(": " + response.noHandphone);
                    $("#get_alamat").text(": " + response.alamatTinggal);
                }

                // reset button
                BtnReset("#btnGetPegawai");
            },
        });
    } else {
        Swal.fire({
            position: "center",
            icon: "error",
            title: "Gagal!",
            text: "Jumlah karakter harus 18 digit",
            showConfirmButton: false,
            timer: 1500,
        });
    }
});

// button hapus cuti by admin
$(".btnHapus").click(function () {
    var id = $(this).data("id");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "delete",
                url: app_url + "/tubel/administrasi/cuti-belajar",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: $(this).data("id"),
                },
                success: function (response) {
                    if (response["status"] == 1) {
                        Swal.fire({
                            position: "center",
                            icon: "success",
                            title: "Berhasil",
                            text: "Data berhasil di hapus",
                            showConfirmButton: false,
                            timer: 1800,
                        });
                        location.reload();
                    } else {
                        Swal.fire({
                            position: "center",
                            icon: "error",
                            title: "Gagal",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 2000,
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// spinner / loading button
function BtnLoading(elem) {
    $(elem).attr("data-original-text", $(elem).html());
    $(elem).prop("disabled", true);
    $(elem).html('Loading... <i class="spinner-border spinner-border-sm"></i>');
}

// spinner off / reset button
function BtnReset(elem) {
    $(elem).prop("disabled", false);
    $(elem).html($(elem).attr("data-original-text"));
}

// placeholder advance search
$(window).on("load", function () {
    // jenjang
    $selectElement = $("#search_jenjang").select2({
        placeholder: "jenjang pendidikan",
        allowClear: true,
        minimumResultsForSearch: -1,
    });

    // lokasi
    $selectElement = $("#search_lokasi").select2({
        placeholder: "lokasi pendidikan",
        allowClear: true,
        minimumResultsForSearch: -1,
    });
});

// date search format
$(".date-search").flatpickr({
    dateFormat: "d-m-Y",
});
