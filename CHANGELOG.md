# HRIS Front-End Changelog

## Version 0.2.2

This version contains:
  * Updated assets
  * Updated template newsikka

## Version 0.2.1

This version contains:
  * Updated Laravel to 9.52.4
  * Updated components
  * Updated dependencies
  * Updated js dependencies
  * Updated PHP to v8.2
  * Fix dockerfile
  * Fix cuti

## Version 0.2.0

This version contains:
  * Updated Laravel to 9.48.0
  * Updated components
  * Updated dependencies
  * Updated js dependencies
  * update phpoffice/phpword to 1.0.0

## Version 0.1.9

This version contains:
  * Updated Laravel to 9.30
  * Updated components
  * Updated dependencies
  * Updated js dependencies

## Version 0.1.8

This version contains:
  * [#26](#26) Updated Laravel to 9.22
  * Updated components
  * Updated dependencies

## Version 0.1.7

This version contains:
  * [#21](#21) Fix bug on Cuti Melahirkan.
  * [#24](#24) Fix bug on Cuti Tambahan.
  * Beautifying the UI.
  * Updated components

## Version 0.1.6

This version contains:
  * [#19](#19) Fix code quality job.
  * [#20](#20) Beautify alert dialog.
  * [#22](#22) Make tabular menu on Cuti Tambahan.
  * [#23](#23) Fix Cuti Approval on UPK ROLE.

## Version 0.1.5

This version contains:
  * [#18](#18) Fix bug on templating.
  * [#17](#17) Update env variable example.

## Version 0.1.4

This version contains:
  * [#15](#15) Fix error on additional leave approval.
  * [#16](#16) Added release pipeline job.

## Version 0.1.3

This version contains:
  * [#13](#13) Fix error on leave approval.
  * [#14](#14) Fix error on holiday workflow.
  * Updated Laravel and Symfony Components

## Version 0.1.2

This version contains:
  * [#9](#9) Fix responsive page bug on small screen.
  * Fix modal close button
  * Fix null on kantor data

## Version 0.1.1

This version contains:
  * [#7](#7) Fix the bug on the jenis cuti page.
  * [#10](#10) Fix error kuota cuti tambahan.
  * [#11](#11) Add more data on db seeder.
  * Updated dependencies

## Version 0.1.0

Development version. Combine multiple branch.


## Version 0.0.0-dev

Development version.
