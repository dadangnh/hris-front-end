<meta name="description"
      content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 94,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue &amp; Laravel versions. Grab your copy now and get life-time updates for free."/>
<meta name="keywords"
      content="Metronic, bootstrap, bootstrap 5, Angular, VueJs, React, Laravel, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta charset="utf-8"/>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta property="og:locale" content="en_US"/>
<meta property="og:type" content="article"/>
<meta property="og:title"
      content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme"/>
<meta property="og:site_name" content="Keenthemes | Metronic"/>

<link rel="shortcut icon" href="{{ url('assets/dist') }}/assets/media/logos/favicon.ico"/>
<link rel="stylesheet" href="{{ url('assets/dist') }}/assets/plugins/global/fonts/fontgoogleapis.css"/>
<link href="{{ url('assets/dist') }}/assets/css/custom.css" rel="stylesheet" type="text/css"/>

<!--begin::Global Stylesheets Bundle(used by all pages)-->
<link href="{{ url('assets/dist') }}/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css"/>
<link href="{{ url('assets/dist') }}/assets/css/style.bundle.css" rel="stylesheet" type="text/css"/>
<link href="{{ url('assets/dist') }}/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet"
      type="text/css">

<!--bootstrap-->
<link href="{{ url('vendor') }}/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" rel="stylesheet"
      type="text/css"/>
<link href="{{ url('vendor') }}/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet"
      type="text/css"/>
<link href="{{ url('vendor') }}/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css"/>
