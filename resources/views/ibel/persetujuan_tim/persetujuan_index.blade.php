@extends('layout.master')

@section('content')

    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card border-warning">
                    <div class="card-header card-header-stretch" id="card-header">
                        <div class="card-toolbar">
                            <ul class="nav nav-tabs nav-line-tabs nav-stretch border-transparent fs-5 fw-bolder"
                                id="tabLps">
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'menunggu-persetujuan' ? 'active' : '' }}"
                                       href="{{ url('ibel/persetujuan-hasil-seleksi/menunggu-persetujuan') }}"> Menunggu
                                        Persetujuan </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'disetujui' ? 'active' : '' }}"
                                       href="{{ url('ibel/persetujuan-hasil-seleksi/disetujui') }}"> Telah
                                        Disetujui </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6 mb-4">
                                <div class="text-start" data-kt-customer-table-toolbar="base">
                                    <button type="button"
                                            class="btn btn-sm btn-ligth btn-active-primary border border-gray-400 fs-6"
                                            data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start"
                                            data-kt-menu-flip="top-end">
                                        <i class="fa fa-filter"></i> Advanced Search
                                    </button>
                                    <div class="menu menu-sub menu-sub-dropdown w-md-600px border-warning mt-1"
                                         data-kt-menu="true">
                                        <div class="px-7 py-5">
                                            <div class="fs-4 text-dark fw-bolder">Filter Pencarian</div>
                                        </div>
                                        <div class="separator border-gray-200"></div>
                                        <form id="advancedSearch" action="" method="get">
                                            <div class="px-7 py-5">
                                                <div class="row">
                                                    <div class="col-lg">
                                                        <div class="row mb-4">
                                                            <label class="form-label fw-bold">Pegawai:</label>
                                                            <div class="col-lg-6">
                                                                <input class="form-control form-select-solid"
                                                                       type="text"
                                                                       name="nama" placeholder="nama pegawai"
                                                                       value="{{ isset($_GET['nama']) ? $_GET['nama'] : '' }}">
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <input class="form-control form-select-solid"
                                                                       type="text"
                                                                       name="nip" placeholder="nip 18 digit"
                                                                       value="{{ isset($_GET['nip']) ? $_GET['nip'] : '' }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg">
                                                        <div class="col-lg-6">
                                                            <label class="form-label fs-5 fw-bold">Tiket</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" name="tiket"
                                                                       placeholder="tiket"
                                                                       value="{{ isset($_GET['tiket']) ? $_GET['tiket'] : '' }}"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-end">
                                                    <a href="{{ url()->current() }}"
                                                       class="btn btn-light btn-active-light-primary me-2">Reset</a>
                                                    <button type="submit" class="btn btn-sm btn-primary"
                                                            data-kt-menu-dismiss="true"
                                                            data-kt-customer-table-filter="filter">Search
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'menunggu-persetujuan' ? 'active show' : '' }}"
                                 id="menunggu-persetujuan" role="tabpanel">
                                @if ($tab == 'menunggu-persetujuan')
                                    @include('ibel.persetujuan_tim.persetujuan_tabel')
                                @endif
                            </div>
                            <div class="tab-pane fade {{ $tab == 'disetujui' ? 'active show' : '' }}" id="disetujui"
                                 role="tabpanel">
                                @if ($tab == 'disetujui')
                                    @include('ibel.persetujuan_tim.persetujuan_tabel')
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('ibel.modal_tiket')

@endsection

@section('js')

    <script src="{{ asset('assets/js/ibel') }}/log-tiket.js"></script>

@endsection
