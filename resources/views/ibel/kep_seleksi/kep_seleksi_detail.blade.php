@extends('layout.master')

@section('content')

    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="card border-warning">
                <div class="card-header" id="card-header">
                    <h3 class="card-title">
                        <span class="fw-bold fs-4 text-white">Pemeriksaan Permohonan</span>
                    </h3>
                </div>
                @include('ibel._component.detail_permohonan')
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('assets/js/ibel') }}/hasil-seleksi.js"></script>
@endsection
