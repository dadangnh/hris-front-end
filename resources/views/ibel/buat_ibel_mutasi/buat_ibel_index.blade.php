@extends('layout.master')

@section('content')
    <!--begin::Stepper-->
    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card mb-3 border-warning shadow-sm">
                    <div id="card-header" class="card-header">
                        <div class="card-title fw-bold fs-4 text-white">
                            Izin Pendidikan Anda
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="stepper stepper-links d-flex flex-column" id="kt_stepper_example_basic">
                            <div class="col"><a href="{{ url('ibel/administrasi') }}"
                                                class="btn btn-sm btn-secondary mb-5"><i class="fa fa-reply"></i>
                                    Kembali</a></div>
                            <!--begin::Form-->
                            @if ($tab == 'mutasi-2')
                                @include('ibel.buat_ibel_mutasi.form_ibel_dua')
                            @endif
                            @if ($tab == 'mutasi-3')
                                @include('ibel.buat_ibel_mutasi.form_ibel_tiga')
                            @endif
                            @if ($tab == 'mutasi-4')
                                @include(
                                    'ibel.buat_ibel_mutasi.form_ibel_empat'
                                )
                            @endif
                            <!--end::Form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end::Stepper-->
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
    </script>

    <script src="{{ asset('assets/js/ibel') }}/buat_ibel.js"></script>
@endsection
