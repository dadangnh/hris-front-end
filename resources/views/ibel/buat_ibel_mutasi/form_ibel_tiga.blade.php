<!--begin::Form-->
<h2 class="mb-10">Langkah 3 - Jadwal Perkuliahan</h2>
<div class="form w-lg-1000px mx-auto">
    <!--begin::Group-->
    <div class="mb-5">
        <!--begin::Step 2-->
        <div class="flex-column">
            <div class="table-responsive-lg bg-gray-400 rounded border border-gray-300">
                <table class="table table-striped align-middle" id="tabel_tubel">
                    <thead class="fw-bolder bg-secondary fs-6">
                    <tr class="text-center">
                        <th class="fs-2">Hari</th>
                        <th class="fs-2">Jam Mulai Perkuliahan</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="text-center">
                        <td class="text-dark mb-1 fs-4">Senin
                        </td>
                        <td class="text-dark mb-1 fs-6 text-center">
                            <div class="row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4 jamSenin">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input class="form-control jamSenin2" type="text"
                                                   placeholder="-" name="data[1][jam]" data-input
                                                   value="{{ $ibelJadwal['0']->jamMulai }}" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4"></div>
                            </div>
                        </td>
                    </tr>
                    <tr class="text-center">
                        <td class="text-dark  mb-1 fs-4">Selasa</td>
                        <td class="text-dark  mb-1 fs-6">
                            <div class="row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4 jamSelasa">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input class="form-control jamSelasa2" type="text"
                                                   placeholder="-" name="data[2][jam]" data-input
                                                   value="{{ $ibelJadwal['1']->jamMulai }}" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4"></div>
                            </div>
                        </td>
                    </tr>
                    <tr class="text-center">
                        <td class="text-dark  mb-1 fs-4">Rabu</td>
                        <td class="text-dark  mb-1 fs-6">
                            <div class="row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4 jamRabu">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input class="form-control jamRabu2" type="text"
                                                   placeholder="-" name="data[3][jam]" data-input
                                                   value="{{ $ibelJadwal['2']->jamMulai }}" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4"></div>
                            </div>
                        </td>
                    </tr>
                    <tr class="text-center">
                        <td class="text-dark  mb-1 fs-4">Kamis</td>
                        <td class="text-dark  mb-1 fs-6">
                            <div class="row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4 jamKamis">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input class="form-control jamKamis2" type="text"
                                                   placeholder="-" name="data[4][jam]" data-input
                                                   value="{{ $ibelJadwal['3']->jamMulai }}" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4"></div>
                            </div>
                        </td>
                    </tr>
                    <tr class="text-center">
                        <td class="text-dark  mb-1 fs-4">Jumat</td>
                        <td class="text-dark  mb-1 fs-6">
                            <div class="row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4 jamJumat">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input class="form-control jamJumat2" type="text"
                                                   placeholder="-" name="data[5][jam]" data-input
                                                   value="{{ $ibelJadwal['4']->jamMulai }}" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4"></div>
                            </div>
                        </td>
                    </tr>
                    <tr class="text-center">
                        <td class="text-dark  mb-1 fs-4">Sabtu</td>
                        <td class="text-dark  mb-1 fs-6">
                            <div class="row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4 jamSabtu">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input class="form-control jamSabtu2" type="text"
                                                   placeholder="-" name="data[6][jam]" data-input
                                                   value="{{ $ibelJadwal['5']->jamMulai }}" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4"></div>
                            </div>
                        </td>
                    </tr>
                    <tr class="text-center">
                        <td class="text-dark  mb-1 fs-4">Minggu</td>
                        <td class="text-dark  mb-1 fs-6">
                            <div class="row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4 jamMinggu">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input class="form-control jamMinggu2" type="text"
                                                   placeholder="-" name="data[7][jam]" data-input
                                                   value="{{ $ibelJadwal['6']->jamMulai }}" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4"></div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>

    </div>
</div>

<!--end::Group-->
<div class="text-center mb-8">
    <a href="{{ url('ibel/administrasi/langkah-dua-mutasi/' . $id) }}"
       class="btn btn-sm btn-secondary my-1"><i class="fas fa-chevron-left"></i> sebelumnya</a>
    <a href="{{ url('ibel/administrasi/langkah-empat-mutasi/' . $id) }}" class="btn btn-sm btn-success">Selanjutnya <i
            class="fas fa-chevron-right"></i></a>
</div>
<!--end::Form-->
