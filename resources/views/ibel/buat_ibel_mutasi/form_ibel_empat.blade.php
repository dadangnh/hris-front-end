<h2 class="mb-10">Langkah 4 - Unggah Dokumen Persyaratan</h2>
<form class="form w-lg-1000px mx-auto" enctype="multipart/form-data" action=" " method="post">
    @csrf
    <input type="hidden" id="idIbel" name="id" value="{{ $id }}">
    {{-- UPPKP --}}
    <div class="flex-column mb-8">
        <div class="row">
            <label class="col-lg-4 col-form-label fw-bold fs-6">Surat Pernyataan Tidak
                Menuntut UPKP/DFA</label>
            <div class="col-lg-6">
                <input id="file_tidak_menuntut_upkp" type="file" class="form-control">
            </div>
            <div class="col-lg-1" style="width: 55px;">
                <a id="btn_file_tidak_menuntut_upkp" class="btn btn-info btn-icon"><i class="fa fa-upload"></i></a>
            </div>
            @if ($file_lama['0']->pathTidakUpkp != null)
                <div class="col-lg-1">
                    <div id="iconfile" class="col-lg">
                        <a id="urlfile_tidak_menuntut_upkp" target="_blank"
                           href="{{ url('files/' . $file_lama['0']->pathTidakUpkp) }}"><i
                                class="fas fa-file-pdf fa-3x" style="color:red;"></i></a>
                        <input id="myinputfile_tidak_menuntut_upkp" type="hidden" name="path_file_tidak_menuntut_upkp1"
                               value="{{ $file_lama['0']->pathTidakUpkp }}" required/>
                    </div>
                </div>
            @else
                <div class="col-lg-1">
                    <div id="iconfile" class="col-lg" style="display: none">
                        <a id="urlfile_tidak_menuntut_upkp" target="_blank" href=""><i class="fas fa-file-pdf fa-3x"
                                                                                       style="color:red;"></i></a>
                        <input id="myinputfile_tidak_menuntut_upkp" type="hidden" name="path_file_tidak_menuntut_upkp1"
                               value="" required/>
                    </div>
                </div>
            @endif
        </div>
        <div class="row mb-3">
            <a class="fas fa-file-word fs-3"
               href="{{ url('ibel/administrasi/upload-dokumen-persyaratan/' . $id . '/file-tidak-menuntut') }}"
               style="color:rgb(24, 80, 201)"> Template Surat
                Pernyataan </a>
        </div>
    </div>
    {{-- Surat Pernyataan Prodi Sesuai Kebutuhan Organisasi --}}
    <div class="flex-column mb-8">
        <div class="row">
            <label class="col-lg-4 col-form-label fw-bold fs-6">Surat Pernyataan Prodi Sesuai Kebutuhan
                Organisasi</label>
            <div class="col-lg-6">
                <input id="file_prodi_sesuai_kebutuhan" type="file" class="form-control">
            </div>
            <div class="col-lg-1" style="width: 55px;">
                <a id="btn_file_prodi_sesuai_kebutuhan" class="btn btn-info btn-icon"><i class="fa fa-upload"></i></a>
            </div>
            @if ($file_lama['0']->pathKebutuhanProdi != null)
                <div class="col-lg-1">
                    <div id="iconfile2" class="col-lg">
                        <a id="urlfile_prodi_sesuai_kebutuhan" target="_blank"
                           href="{{ url('files/' . $file_lama['0']->pathKebutuhanProdi) }}"><i
                                class="fas fa-file-pdf fa-3x" style="color:red;"></i></a>
                        <input id="myinputfile_prodi_sesuai_kebutuhan" type="hidden"
                               name="path_file_prodi_sesuai_kebutuhan" value="{{ $file_lama['0']->pathKebutuhanProdi }}"
                               required/>
                    </div>
                </div>
            @else
                <div class="col-lg-1">
                    <div id="iconfile2" class="col-lg" style="display: none">
                        <a id="urlfile_prodi_sesuai_kebutuhan" target="_blank" href=""><i class="fas fa-file-pdf fa-3x"
                                                                                          style="color:red;"></i></a>
                        <input id="myinputfile_prodi_sesuai_kebutuhan" type="hidden"
                               name="path_file_prodi_sesuai_kebutuhan" value="" required/>
                    </div>
                </div>
            @endif
        </div>
        <div class="row mb-3">
            <a class="fas fa-file-word fs-3"
               href="{{ url('ibel/administrasi/upload-dokumen-persyaratan/' . $id . '/file-prodi-sesuai') }}"
               style="color:rgb(24, 80, 201)"> Template Surat
                Pernyataan </a>
        </div>
    </div>
    {{-- Surat Pernyataan Tidak Menjalani Skorsing --}}
    <div class="flex-column mb-8">
        <div class="row">
            <label class="col-lg-4 col-form-label fw-bold fs-6">Surat Pernyataan Tidak Menjalani Skorsing
            </label>
            <div class="col-lg-6">
                <input id="file_tidak_skorsing" type="file" class="form-control">
            </div>
            <div class="col-lg-1" style="width: 55px;">
                <a id="btn_file_tidak_skorsing" class="btn btn-info btn-icon"><i class="fa fa-upload"></i></a>
            </div>
            @if ($file_lama['0']->pathSkorsing != null)
                <div class="col-lg-1">
                    <div id="iconfile3" class="col-lg">
                        <a id="urlfile_tidak_skorsing" target="_blank"
                           href="{{ url('files/' . $file_lama['0']->pathSkorsing) }}"><i
                                class="fas fa-file-pdf fa-3x" style="color:red;"></i></a>
                        <input id="myinputfile_tidak_skorsing" type="hidden" name="path_file_tidak_skorsing"
                               value="{{ $file_lama['0']->pathSkorsing }}" required/>
                    </div>
                </div>
            @else
                <div class="col-lg-1">
                    <div id="iconfile3" class="col-lg" style="display: none">
                        <a id="urlfile_tidak_skorsing" target="_blank" href=""><i class="fas fa-file-pdf fa-3x"
                                                                                  style="color:red;"></i></a>
                        <input id="myinputfile_tidak_skorsing" type="hidden" name="path_file_tidak_skorsing" value=""
                               required/>
                    </div>
                </div>
            @endif
        </div>
    </div>
    {{-- Surat Keterangan tidak sedang menjalani Hukdis sedang/berat/sedang dalam proses pemeriksaan --}}
    <div class="flex-column mb-8">
        <div class="row">
            <label class="col-lg-4 col-form-label fw-bold fs-6">Surat Keterangan tidak sedang menjalani Hukdis
                sedang/berat/sedang dalam proses pemeriksaan
            </label>
            <div class="col-lg-6">
                <input id="file_tidak_hukdis" type="file" class="form-control">
            </div>
            <div class="col-lg-1" style="width: 55px;">
                <a id="btn_file_tidak_hukdis" class="btn btn-info btn-icon"><i class="fa fa-upload"></i></a>
            </div>
            @if ($file_lama['0']->pathHukdis != null)
                <div class="col-lg-1">
                    <div id="iconfile4" class="col-lg">
                        <a id="urlfile_tidak_hukdis" target="_blank"
                           href="{{ url('files/' . $file_lama['0']->pathHukdis) }}"><i class="fas fa-file-pdf fa-3x"
                                                                                       style="color:red;"></i></a>
                        <input id="myinputfile_tidak_hukdis" type="hidden" name="path_file_tidak_hukdis"
                               value="{{ $file_lama['0']->pathHukdis }}" required/>
                    </div>
                </div>
            @else
                <div class="col-lg-1">
                    <div id="iconfile4" class="col-lg" style="display: none">
                        <a id="urlfile_tidak_hukdis" target="_blank" href=""><i class="fas fa-file-pdf fa-3x"
                                                                                style="color:red;"></i></a>
                        <input id="myinputfile_tidak_hukdis" type="hidden" name="path_file_tidak_hukdis" value=""
                               required/>
                    </div>
                </div>
            @endif
        </div>
    </div>
    {{-- Surat Keterangan jarak lokasi perkuliahan dari tempat kerja (non pjj) --}}
    <div class="flex-column mb-8">
        <div class="row">
            <label class="col-lg-4 col-form-label fw-bold fs-6">Surat Keterangan jarak lokasi perkuliahan dari tempat
                kerja (non pjj)
            </label>
            <div class="col-lg-6">
                <input id="file_jarak_lokasi" type="file" class="form-control">
            </div>
            <div class="col-lg-1" style="width: 55px;">
                <a id="btn_file_jarak_lokasi" class="btn btn-info btn-icon"><i class="fa fa-upload"></i></a>
            </div>
            @if ($file_lama['0']->pathJarak != null)
                <div class="col-lg-1">
                    <div id="iconfile5" class="col-lg">
                        <a id="urlfile_jarak_lokasi" target="_blank"
                           href="{{ url('files/' . $file_lama['0']->pathJarak) }}"><i class="fas fa-file-pdf fa-3x"
                                                                                      style="color:red;"></i></a>
                        <input id="myinputfile_jarak_lokasi" type="hidden" name="path_file_jarak_lokasi"
                               value="{{ $file_lama['0']->pathJarak }}" required/>
                    </div>
                </div>
            @else
                <div class="col-lg-1">
                    <div id="iconfile5" class="col-lg" style="display: none">
                        <a id="urlfile_jarak_lokasi" target="_blank" href=""><i class="fas fa-file-pdf fa-3x"
                                                                                style="color:red;"></i></a>
                        <input id="myinputfile_jarak_lokasi" type="hidden" name="path_file_jarak_lokasi" value=""
                               required/>
                    </div>
                </div>
            @endif
        </div>
    </div>
    {{-- Surat Keterangan waktu pendidikan di luar jam kerja --}}
    <div class="flex-column mb-8">
        <div class="row">
            <label class="col-lg-4 col-form-label fw-bold fs-6">Surat Keterangan waktu pendidikan di luar jam kerja
            </label>
            <div class="col-lg-6">
                <input id="file_luar_jam_kerja" type="file" class="form-control">
            </div>
            <div class="col-lg-1" style="width: 55px;">
                <a id="btn_file_luar_jam_kerja" class="btn btn-info btn-icon"><i class="fa fa-upload"></i></a>
            </div>
            @if ($file_lama['0']->pathJadwal != null)
                <div class="col-lg-1">
                    <div id="iconfile6" class="col-lg">
                        <a id="urlfile_luar_jam_kerja" target="_blank"
                           href="{{ url('files/' . $file_lama['0']->pathJadwal) }}"><i class="fas fa-file-pdf fa-3x"
                                                                                       style="color:red;"></i></a>
                        <input id="myinputfile_luar_jam_kerja" type="hidden" name="path_file_luar_jam_kerja"
                               value="{{ $file_lama['0']->pathJadwal }}" required/>
                    </div>
                </div>
            @else
                <div class="col-lg-1">
                    <div id="iconfile6" class="col-lg" style="display: none">
                        <a id="urlfile_luar_jam_kerja" target="_blank" href=""><i class="fas fa-file-pdf fa-3x"
                                                                                  style="color:red;"></i></a>
                        <input id="myinputfile_luar_jam_kerja" type="hidden" name="path_file_luar_jam_kerja" value=""
                               required/>
                    </div>
                </div>
            @endif
        </div>
    </div>
    {{-- pembatas --}}
    {{-- <div class="separator separator-dashed border-gray-700 m-8"></div> --}}
    {{-- Brosur Penerimaan Mahasiswa Baru --}}
    <div class="flex-column mb-8">
        <div class="row">
            <label class="col-lg-4 col-form-label fw-bold fs-6">Brosur Penerimaan Mahasiswa Baru</label>
            <div class="col-lg-6">
                <input id="file_brosur_pmb" type="file" class="form-control">
            </div>
            <div class="col-lg-1" style="width: 55px;">
                <a id="btn_file_brosur_pmb" class="btn btn-info btn-icon"><i class="fa fa-upload"></i></a>
            </div>
            @if ($file_lama['0']->pathBrosur != null)
                <div class="col-lg-1">
                    <div id="iconfile7" class="col-lg">
                        <a id="urlfile_brosur_pmb" target="_blank"
                           href="{{ url('files/' . $file_lama['0']->pathBrosur) }}"><i
                                class="fas fa-file-pdf fa-3x" style="color:red;"></i></a>
                        <input id="myinputfile_brosur_pmb" type="hidden" name="path_file_brosur_pmb"
                               value="{{ $file_lama['0']->pathBrosur }}" required/>
                    </div>
                </div>
            @else
                <div class="col-lg-1">
                    <div id="iconfile7" class="col-lg" style="display: none">
                        <a id="urlfile_brosur_pmb" target="_blank" href=""><i class="fas fa-file-pdf fa-3x"
                                                                              style="color:red;"></i></a>
                        <input id="myinputfile_brosur_pmb" type="hidden" name="path_file_brosur_pmb" value=""
                               required/>
                    </div>
                </div>
            @endif
        </div>
    </div>
    {{-- Keputusan/Sertifikat Akreditasi Prodi BANPT --}}
    <div class="flex-column mb-8">
        <div class="row">
            <label class="col-lg-4 col-form-label fw-bold fs-6">Keputusan/Sertifikat Akreditasi Prodi BANPT
            </label>
            <div class="col-lg-6">
                <input id="file_sertifikat_prodi" type="file" class="form-control">
            </div>
            <div class="col-lg-1" style="width: 55px;">
                <a id="btn_file_sertifikat_prodi" class="btn btn-info btn-icon"><i class="fa fa-upload"></i></a>
            </div>
            @if ($file_lama['0']->pathAkreditasi != null)
                <div class="col-lg-1">
                    <div id="iconfile8" class="col-lg">
                        <a id="urlfile_sertifikat_prodi" target="_blank"
                           href="{{ url('files/' . $file_lama['0']->pathAkreditasi) }}"><i
                                class="fas fa-file-pdf fa-3x" style="color:red;"></i></a>
                        <input id="myinputfile_sertifikat_prodi" type="hidden" name="path_file_sertifikat_prodi"
                               value="{{ $file_lama['0']->pathAkreditasi }}" required/>
                    </div>
                </div>
            @else
                <div class="col-lg-1">
                    <div id="iconfile8" class="col-lg" style="display: none">
                        <a id="urlfile_sertifikat_prodi" target="_blank" href=""><i class="fas fa-file-pdf fa-3x"
                                                                                    style="color:red;"></i></a>
                        <input id="myinputfile_sertifikat_prodi" type="hidden" name="path_file_sertifikat_prodi"
                               value="" required/>
                    </div>
                </div>
            @endif
        </div>
    </div>
    {{-- Ijazah yang telah diakui secara kedinasan --}}
    <div class="flex-column mb-8">
        <div class="row">
            <label class="col-lg-4 col-form-label fw-bold fs-6">Ijazah yang telah diakui secara kedinasan</label>
            <div class="col-lg-6">
                <input id="ijazah_kedinasan" type="file" class="form-control">
            </div>
            <div class="col-lg-1" style="width: 55px;">
                <a id="btn_ijazah_kedinasan" class="btn btn-info btn-icon"><i class="fa fa-upload"></i></a>
            </div>
            @if ($file_lama['0']->pathIjazah != null)
                <div class="col-lg-1">
                    <div id="iconfile9" class="col-lg">
                        <a id="url_ijazah_kedinasan" target="_blank"
                           href="{{ url('files/' . $file_lama['0']->pathIjazah) }}"><i
                                class="fas fa-file-pdf fa-3x" style="color:red;"></i></a>
                        <input id="myinput_ijazah_kedinasan" type="hidden" name="path_ijazah_kedinasan"
                               value="{{ $file_lama['0']->pathIjazah }}" required/>
                    </div>
                </div>
            @else
                <div class="col-lg-1">
                    <div id="iconfile9" class="col-lg" style="display: none">
                        <a id="url_ijazah_kedinasan" target="_blank" href=""><i class="fas fa-file-pdf fa-3x"
                                                                                style="color:red;"></i></a>
                        <input id="myinput_ijazah_kedinasan" type="hidden" name="path_ijazah_kedinasan" value=""
                               required/>
                    </div>
                </div>
            @endif
        </div>
    </div>
    {{-- Transkrip Nilai --}}
    <div class="flex-column mb-8">
        <div class="row">
            <label class="col-lg-4 col-form-label fw-bold fs-6">Transkrip Nilai</label>
            <div class="col-lg-6">
                <input id="file_transkrip_nilai" type="file" class="form-control">
            </div>
            <div class="col-lg-1" style="width: 55px;">
                <a id="btn_file_transkrip_nilai" class="btn btn-info btn-icon"><i class="fa fa-upload"></i></a>
            </div>
            @if ($file_lama['0']->pathTranskrip != null)
                <div class="col-lg-1">
                    <div id="iconfile10" class="col-lg">
                        <a id="urlfile_transkrip_nilai" target="_blank"
                           href="{{ url('files/' . $file_lama['0']->pathTranskrip) }}"><i
                                class="fas fa-file-pdf fa-3x" style="color:red;"></i></a>
                        <input id="myinputfile_transkrip_nilai" type="hidden" name="path_file_transkrip_nilai"
                               value="{{ $file_lama['0']->pathTranskrip }}" required/>
                    </div>
                </div>
            @else
                <div class="col-lg-1">
                    <div id="iconfile10" class="col-lg" style="display: none">
                        <a id="urlfile_transkrip_nilai" target="_blank" href=""><i class="fas fa-file-pdf fa-3x"
                                                                                   style="color:red;"></i></a>
                        <input id="myinputfile_transkrip_nilai" type="hidden" name="path_file_transkrip_nilai" value=""
                               required/>
                    </div>
                </div>
            @endif
        </div>
    </div>
    {{-- Surat Keterangan Berbadan Sehat --}}
    <div class="flex-column mb-8">
        <div class="row">
            <label class="col-lg-4 col-form-label fw-bold fs-6">Surat Keterangan Berbadan Sehat</label>
            <div class="col-lg-6">
                <input id="file_sket_sehat" type="file" class="form-control">
            </div>
            <div class="col-lg-1" style="width: 55px;">
                <a id="btn_file_sket_sehat" class="btn btn-info btn-icon"><i class="fa fa-upload"></i></a>
            </div>
            @if ($file_lama['0']->pathSehat != null)
                <div class="col-lg-1">
                    <div id="iconfile11" class="col-lg">
                        <a id="urlfile_sket_sehat" target="_blank"
                           href="{{ url('files/' . $file_lama['0']->pathSehat) }}"><i class="fas fa-file-pdf fa-3x"
                                                                                      style="color:red;"></i></a>
                        <input id="myinputfile_sket_sehat" type="hidden" name="path_file_sket_sehat"
                               value="{{ $file_lama['0']->pathSehat }}" required/>
                    </div>
                </div>
            @else
                <div class="col-lg-1">
                    <div id="iconfile11" class="col-lg" style="display: none">
                        <a id="urlfile_sket_sehat" target="_blank" href=""><i class="fas fa-file-pdf fa-3x"
                                                                              style="color:red;"></i></a>
                        <input id="myinputfile_sket_sehat" type="hidden" name="path_file_sket_sehat" value=""
                               required/>
                    </div>
                </div>
            @endif
        </div>
    </div>
    {{-- SK CPNS --}}
    <div class="flex-column mb-8">
        <div class="row">
            <label class="col-lg-4 col-form-label fw-bold fs-6">SK CPNS</label>
            <div class="col-lg-6">
                <input id="file_sk_cpns" type="file" class="form-control">
            </div>
            <div class="col-lg-1" style="width: 55px;">
                <a id="btn_file_sk_cpns" class="btn btn-info btn-icon"><i class="fa fa-upload"></i></a>
            </div>
            @if ($file_lama['0']->pathCpns != null)
                <div class="col-lg-1">
                    <div id="iconfile12" class="col-lg">
                        <a id="urlfile_sk_cpns" target="_blank"
                           href="{{ url('files/' . $file_lama['0']->pathCpns) }}"><i class="fas fa-file-pdf fa-3x"
                                                                                     style="color:red;"></i></a>
                        <input id="myinputfile_sk_cpns" type="hidden" name="path_file_sk_cpns"
                               value="{{ $file_lama['0']->pathCpns }}" required/>
                    </div>
                </div>
            @else
                <div class="col-lg-1">
                    <div id="iconfile12" class="col-lg" style="display: none">
                        <a id="urlfile_sk_cpns" target="_blank" href=""><i class="fas fa-file-pdf fa-3x"
                                                                           style="color:red;"></i></a>
                        <input id="myinputfile_sk_cpns" type="hidden" name="path_file_sk_cpns" value="" required/>
                    </div>
                </div>
            @endif
        </div>
    </div>
    {{-- SK PNS --}}
    <div class="flex-column mb-8">
        <div class="row">
            <label class="col-lg-4 col-form-label fw-bold fs-6">SK PNS</label>
            <div class="col-lg-6">
                <input id="file_sk_pns" type="file" class="form-control">
            </div>
            <div class="col-lg-1" style="width: 55px;">
                <a id="btn_file_sk_pns" class="btn btn-info btn-icon"><i class="fa fa-upload"></i></a>
            </div>
            @if ($file_lama['0']->pathPns != null)
                <div class="col-lg-1">
                    <div id="iconfile13" class="col-lg">
                        <a id="urlfile_sk_pns" target="_blank"
                           href="{{ url('files/' . $file_lama['0']->pathPns) }}"><i class="fas fa-file-pdf fa-3x"
                                                                                    style="color:red;"></i></a>
                        <input id="myinputfile_sk_pns" type="hidden" name="path_file_sk_pns"
                               value="{{ $file_lama['0']->pathPns }}" required/>
                    </div>
                </div>
            @else
                <div class="col-lg-1">
                    <div id="iconfile13" class="col-lg" style="display: none">
                        <a id="urlfile_sk_pns" target="_blank" href=""><i class="fas fa-file-pdf fa-3x"
                                                                          style="color:red;"></i></a>
                        <input id="myinputfile_sk_pns" type="hidden" name="path_file_sk_pns" value="" required/>
                    </div>
                </div>
            @endif
        </div>
    </div>
    {{-- SK Pangkat Terakhir --}}
    <div class="flex-column mb-8">
        <div class="row">
            <label class="col-lg-4 col-form-label fw-bold fs-6">SK Pangkat Terakhir</label>
            <div class="col-lg-6">
                <input id="file_sk_pangkat" type="file" class="form-control">
            </div>
            <div class="col-lg-1" style="width: 55px;">
                <a id="btn_file_sk_pangkat" class="btn btn-info btn-icon"><i class="fa fa-upload"></i></a>
            </div>
            @if ($file_lama['0']->pathPangkat != null)
                <div class="col-lg-1">
                    <div id="iconfile14" class="col-lg">
                        <a id="urlfile_sk_pangkat" target="_blank"
                           href="{{ url('files/' . $file_lama['0']->pathPangkat) }}"><i
                                class="fas fa-file-pdf fa-3x" style="color:red;"></i></a>
                        <input id="myinputfile_sk_pangkat" type="hidden" name="path_file_sk_pangkat"
                               value="{{ $file_lama['0']->pathPangkat }}" required/>
                    </div>
                </div>
            @else
                <div class="col-lg-1">
                    <div id="iconfile14" class="col-lg" style="display: none">
                        <a id="urlfile_sk_pangkat" target="_blank" href=""><i class="fas fa-file-pdf fa-3x"
                                                                              style="color:red;"></i></a>
                        <input id="myinputfile_sk_pangkat" type="hidden" name="path_file_sk_pangkat" value=""
                               required/>
                    </div>
                </div>
            @endif
        </div>
    </div>
    {{-- PPKPNS Terakhir 1 --}}
    <div class="flex-column mb-8">
        <div class="row">
            <label class="col-lg-4 col-form-label fw-bold fs-6">PPKPNS 2 Tahun Terakhir (1)</label>
            <div class="col-lg-6">
                <input id="file_ppkpns_satu" type="file" class="form-control">
            </div>
            <div class="col-lg-1" style="width: 55px;">
                <a id="btn_file_ppkpns_satu" class="btn btn-info btn-icon"><i class="fa fa-upload"></i></a>
            </div>
            @if ($file_lama['0']->pathPpkpns1 != null)
                <div class="col-lg-1">
                    <div id="iconfile15" class="col-lg">
                        <a id="urlfile_ppkpns_satu" target="_blank"
                           href="{{ url('files/' . $file_lama['0']->pathPpkpns1) }}"><i
                                class="fas fa-file-pdf fa-3x" style="color:red;"></i></a>
                        <input id="myinputfile_ppkpns_satu" type="hidden" name="path_file_ppkpns_satu"
                               value="{{ $file_lama['0']->pathPpkpns1 }}" required/>
                    </div>
                </div>
            @else
                <div class="col-lg-1">
                    <div id="iconfile15" class="col-lg" style="display: none">
                        <a id="urlfile_ppkpns_satu" target="_blank" href=""><i class="fas fa-file-pdf fa-3x"
                                                                               style="color:red;"></i></a>
                        <input id="myinputfile_ppkpns_satu" type="hidden" name="path_file_ppkpns_satu" value=""
                               required/>
                    </div>
                </div>
            @endif
        </div>
    </div>
    {{-- PPKPNS Terakhir 2 --}}
    <div class="flex-column mb-8">
        <div class="row">
            <label class="col-lg-4 col-form-label fw-bold fs-6">PPKPNS 2 Tahun Terakhir (2)</label>
            <div class="col-lg-6">
                <input id="file_ppkpns_dua" type="file" class="form-control">
            </div>
            <div class="col-lg-1" style="width: 55px;">
                <a id="btn_file_ppkpns_dua" class="btn btn-info btn-icon"><i class="fa fa-upload"></i></a>
            </div>
            @if ($file_lama['0']->path_ppkpns2 != null)
                <div class="col-lg-1">
                    <div id="iconfile16" class="col-lg">
                        <a id="urlfile_ppkpns_dua" target="_blank"
                           href="{{ url('files/' . $file_lama['0']->path_ppkpns2) }}"><i
                                class="fas fa-file-pdf fa-3x" style="color:red;"></i></a>
                        <input id="myinputfile_ppkpns_dua" type="hidden" name="path_file_ppkpns_dua"
                               value="{{ $file_lama['0']->path_ppkpns2 }}" required/>
                    </div>
                </div>
            @else
                <div class="col-lg-1">
                    <div id="iconfile16" class="col-lg" style="display: none">
                        <a id="urlfile_ppkpns_dua" target="_blank" href=""><i class="fas fa-file-pdf fa-3x"
                                                                              style="color:red;"></i></a>
                        <input id="myinputfile_ppkpns_dua" type="hidden" name="path_file_ppkpns_dua" value=""
                               required/>
                    </div>
                </div>
            @endif
        </div>
    </div>


    <div class="text-center mb-8">
        <a href="{{ url('ibel/administrasi/langkah-tiga-mutasi/' . $id) }}" class="btn btn-sm btn-secondary my-1"><i
                class="fas fa-chevron-left"></i>Sebelumnya</a>
        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Selesai</button>
    </div>
</form>
