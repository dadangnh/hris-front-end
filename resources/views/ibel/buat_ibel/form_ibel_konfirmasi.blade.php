<h2 class="mb-10">Langkah 1 - Konfirmasi</h2>
<form class="form w-lg-1000px mx-auto" action=" " method="post" enctype="multipart/form-data">
    @csrf
    <!--begin::Group-->
    <div class="mb-5">
        <!--begin::Step 1-->
        <div class="flex-column current">
            <div class="card-body m-5 mt-0">
                <div class="row mb-3">
                    <label class="col-lg col-form-label fw-bold fs-4">Jarak Kantor anda dengan lokasi perkuliahan lebih
                        dari 60 kilometer.
                        <br>
                        Apakah anda sudah tidak menjalani perkuliahan tatap muka ?
                    </label>
                </div>

                {{-- ----------------Switch button----------------- --}}
                <div class="row mb-3">
                    <label class="col-lg-4 col-form-label fw-bold fs-4">Sudah Tidak menjalani Tatap Muka</label>
                    <div class="col-lg-3">
                        <div class="form-check form-switch form-check-custom form-check-solid">
                            <label class="form-check-label col-form-label fw-bold fs-6" for="flexSwitchDefault">
                                <input class="form-check-input" type="checkbox" name="pjjId" value="1"/>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <label class="col-lg-4 col-form-label fw-bold fs-4">Dokumen Keterangan</label>
                    <div class="col-lg">
                        <input type="file" class="form-control" name="dokKeterangan" placeholder="misal">
                    </div>
                </div>

            </div>
        </div>
        <div class="text-center mb-8">
            <a href="{{ url('ibel/administrasi/') }}" class="btn btn-sm btn-secondary my-1"><i
                    class="fas fa-chevron-left"></i>Sebelumnya</a>
            <button type="submit" class="btn btn-sm btn-success">Selanjutnya <i
                    class="fas fa-chevron-right"></i></button>
        </div>
    </div>
</form>
