<!--begin::Form-->
<h2 class="mb-10">Langkah 2 - Jadwal Perkuliahan</h2>
<form class="form w-lg-1000px mx-auto" id="step_dua"
      action=" " method="post">
    @csrf
    <!--begin::Group-->
    <div class="mb-5">
        <!--begin::Step 2-->
        <div class="flex-column">
            <div class="table-responsive-lg bg-gray-400 rounded border border-gray-300">
                <table class="table table-striped align-middle" id="tabel_tubel">
                    <thead class="fw-bolder bg-secondary fs-6">
                    <tr class="text-center">
                        <th class="fs-2">Hari</th>
                        <th class="fs-2">Jam Mulai Perkuliahan</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="text-center">
                        <td class="text-dark mb-1 fs-4">Senin
                        </td>
                        <td class="text-dark mb-1 fs-6 text-center">
                            <div class="row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4 jamSenin">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input class="form-control jamSenin2" type="text"
                                                   placeholder="HH:MM" name="data[1][jam]" data-input
                                                   value="{{ $ibelJadwal['0']->jamMulai }}">
                                        </div>
                                        <input type="hidden" name="data[1][id]"
                                               value="{{ $ibelJadwal['0']->id }}">
                                        <div class="text-start col-sm-6 input-group-append">
                                            <a class=" btn btn-outline-secondary btn-danger input-button"
                                               title="clear" data-clear>X</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4"></div>
                            </div>
                        </td>
                    </tr>
                    <tr class="text-center">
                        <td class="text-dark  mb-1 fs-4">Selasa</td>
                        <td class="text-dark  mb-1 fs-6">
                            <div class="row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4 jamSelasa">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input class="form-control jamSelasa2" type="text"
                                                   placeholder="HH:MM" name="data[2][jam]" data-input
                                                   value="{{ $ibelJadwal['1']->jamMulai }}">
                                        </div>
                                        <input type="hidden" name="data[2][id]"
                                               value="{{ $ibelJadwal['1']->id }}">
                                        <div class="text-start col-sm-6 input-group-append">
                                            <a class=" btn btn-outline-secondary btn-danger input-button"
                                               title="clear" data-clear>X</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4"></div>
                            </div>
                        </td>
                    </tr>
                    <tr class="text-center">
                        <td class="text-dark  mb-1 fs-4">Rabu</td>
                        <td class="text-dark  mb-1 fs-6">
                            <div class="row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4 jamRabu">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input class="form-control jamRabu2" type="text"
                                                   placeholder="HH:MM" name="data[3][jam]" data-input
                                                   value="{{ $ibelJadwal['2']->jamMulai }}">
                                        </div>
                                        <input type="hidden" name="data[3][id]"
                                               value="{{ $ibelJadwal['2']->id }}">
                                        <div class="text-start col-sm-6 input-group-append">
                                            <a class=" btn btn-outline-secondary btn-danger input-button"
                                               title="clear" data-clear>X</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4"></div>
                            </div>
                        </td>
                    </tr>
                    <tr class="text-center">
                        <td class="text-dark  mb-1 fs-4">Kamis</td>
                        <td class="text-dark  mb-1 fs-6">
                            <div class="row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4 jamKamis">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input class="form-control jamKamis2" type="text"
                                                   placeholder="HH:MM" name="data[4][jam]" data-input
                                                   value="{{ $ibelJadwal['3']->jamMulai }}">
                                        </div>
                                        <input type="hidden" name="data[4][id]"
                                               value="{{ $ibelJadwal['3']->id }}">
                                        <div class="text-start col-sm-6 input-group-append">
                                            <a class=" btn btn-outline-secondary btn-danger input-button"
                                               title="clear" data-clear>X</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4"></div>
                            </div>
                        </td>
                    </tr>
                    <tr class="text-center">
                        <td class="text-dark  mb-1 fs-4">Jumat</td>
                        <td class="text-dark  mb-1 fs-6">
                            <div class="row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4 jamJumat">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input class="form-control jamJumat2" type="text"
                                                   placeholder="HH:MM" name="data[5][jam]" data-input
                                                   value="{{ $ibelJadwal['4']->jamMulai }}">
                                        </div>
                                        <input type="hidden" name="data[5][id]"
                                               value="{{ $ibelJadwal['4']->id }}">
                                        <div class="text-start col-sm-6 input-group-append">
                                            <a class=" btn btn-outline-secondary btn-danger input-button"
                                               title="clear" data-clear>X</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4"></div>
                            </div>
                        </td>
                    </tr>
                    <tr class="text-center">
                        <td class="text-dark  mb-1 fs-4">Sabtu</td>
                        <td class="text-dark  mb-1 fs-6">
                            <div class="row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4 jamSabtu">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input class="form-control jamSabtu2" type="text"
                                                   placeholder="HH:MM" name="data[6][jam]" data-input
                                                   value="{{ $ibelJadwal['5']->jamMulai }}">
                                        </div>
                                        <input type="hidden" name="data[6][id]"
                                               value="{{ $ibelJadwal['5']->id }}">
                                        <div class="text-start col-sm-6 input-group-append">
                                            <a class=" btn btn-outline-secondary btn-danger input-button"
                                               title="clear" data-clear>X</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4"></div>
                            </div>
                        </td>
                    </tr>
                    <tr class="text-center">
                        <td class="text-dark  mb-1 fs-4">Minggu</td>
                        <td class="text-dark  mb-1 fs-6">
                            <div class="row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4 jamMinggu">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input class="form-control jamMinggu2" type="text"
                                                   placeholder="HH:MM" name="data[7][jam]" data-input
                                                   value="{{ $ibelJadwal['6']->jamMulai }}">
                                        </div>
                                        <input type="hidden" name="data[7][id]"
                                               value="{{ $ibelJadwal['6']->id }}">
                                        <div class="text-start col-sm-6 input-group-append">
                                            <a class=" btn btn-outline-secondary btn-danger input-button"
                                               title="clear" data-clear>X</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4"></div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>

    </div>
    <!--end::Group-->
    <div class="text-center mb-8">
        <a href="{{ url('ibel/administrasi/isi-data-utama/edit/' . $id) }}"
           class="btn btn-sm btn-secondary my-1"><i class="fas fa-chevron-left"></i> sebelumnya</a>
        <button type="submit" class="btn btn-sm btn-success">Selanjutnya <i
                class="fas fa-chevron-right"></i></button>
    </div>
</form>
<!--end::Form-->

<script>
    //validasi Jam
    document.getElementById("step_dua").addEventListener("submit", stepDua);

    function stepDua() {
        var senin = $('.jamSenin2').val();
        let senin2 = senin;
        let senin3 = senin2.slice(0, 2);

        var selasa = $('.jamSelasa2').val();
        let selasa2 = selasa;
        let selasa3 = selasa2.slice(0, 2);

        var rabu = $('.jamRabu2').val();
        let rabu2 = rabu;
        let rabu3 = rabu2.slice(0, 2);

        var kamis = $('.jamKamis2').val();
        let kamis2 = kamis;
        let kamis3 = kamis2.slice(0, 2);

        var jumat = $('.jamJumat2').val();
        let jumat2 = jumat;
        let jumat3 = jumat2.slice(0, 2);

        var sabtu = $('.jamSabtu2').val();
        let sabtu2 = sabtu;
        let sabtu3 = sabtu2.slice(0, 2);

        var minggu = $('.jamMinggu2').val();
        let minggu2 = minggu;
        let minggu3 = minggu2.slice(0, 2);

        let x = senin3 + selasa3 + rabu3 + kamis3 + jumat3;
        let z = sabtu3 > 0 && minggu3 > 0 && x >= 1717;
        if (x >= 171717 || z == true) {
            console.log(z);

        } else {
            event.preventDefault();
            console.log(x);
            swalError('Perhatikan Kembali Jadwal Perkuliahan');
        }
    }
</script>
