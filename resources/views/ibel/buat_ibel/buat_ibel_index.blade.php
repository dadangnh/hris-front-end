@extends('layout.master')

@section('content')
    <!--begin::Stepper-->
    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card mb-3 border-warning shadow-sm">
                    <div id="card-header" class="card-header">
                        <div class="card-title fw-bold fs-4 text-white">
                            Izin Pendidikan Anda
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="stepper stepper-links d-flex flex-column" id="kt_stepper_example_basic">
                            <div class="col"><a href="{{ url('ibel/administrasi') }}"
                                                class="btn btn-sm btn-secondary mb-5"><i class="fa fa-reply"></i>
                                    Kembali</a></div>
                            <!--begin::Form-->
                            @if ($tab == '1')
                                @include('ibel.buat_ibel.form_ibel_satu')
                            @endif
                            @if ($tab == '2')
                                @include('ibel.buat_ibel.form_ibel_dua')
                            @endif
                            @if ($tab == '3')
                                @include('ibel.buat_ibel.form_ibel_tiga')
                            @endif
                            @if ($tab == 'edit1')
                                @include('ibel.buat_ibel.form_ibel_satu_edit')
                            @endif
                            @if ($tab == 'edit2')
                                @include('ibel.buat_ibel.form_ibel_dua_edit')
                            @endif
                            @if ($tab == 'konfirmasi')
                                @include('ibel.buat_ibel.form_ibel_konfirmasi')
                            @endif
                            <!--end::Form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end::Stepper-->
@endsection
@section('js')
    <script>
        // let app_url = '{{ env('APP_URL') }}'
        //kalender
        $(".datepick").flatpickr();

        // get pt, prodi, akreditasi, dan lokasi
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // get pt ibel
            $("#getPT_ibel").select2({

                placeholder: 'Pilih perguruan tinggi',
                // minimumInputLength: 3,
                ajax: {
                    url: app_url + '/ibel/administrasi/get-data-pt',
                    dataType: 'json',
                    type: 'post',
                    delay: 300,
                    data: function (params) {
                        return {
                            // search: params.term,
                            pt_jenjang: $('#JenjangPendidikan').val()
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response

                        };
                    }
                }
            })

            // get data Prodi dan Akreditasi
            $("#prodiIdAkreditasi").select2({

                placeholder: 'Pilih program studi',
                // minimumInputLength: 3,
                ajax: {
                    url: app_url + '/ibel/administrasi/get-data-prodi-akreditasi',
                    dataType: 'json',
                    type: 'post',
                    delay: 300,
                    data: function (params) {
                        return {
                            pt_id: $('#getPT_ibel').val(),
                            pt_jenjang: $('#JenjangPendidikan').val()
                            // search: params.term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    }
                }
            })
            // get lokasi
            $("#lokasiKuliah").select2({

                placeholder: 'Pilih lokasi perkuliahan',
                // minimumInputLength: 3,
                ajax: {
                    url: app_url + '/ibel/administrasi/get-data-lokasi-kampus',
                    dataType: 'json',
                    type: 'post',
                    delay: 300,
                    data: function (params) {
                        return {
                            pt_id: $('#getPT_ibel').val(),
                            // search: params.term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    }
                }
            })


            // $("#nama_pt_ibel").select2({

            //     placeholder: 'Pilih pt',
            //     ajax: {
            //         url: '{{ env('APP_URL') }}' + '/ibel/administrasi/getDataPT1',
            //         dataType: 'json',
            //         type: 'post',
            //         delay: 300,
            //         data: function(data) {
            //             return {
            //                 pt_id: $('#lokasiKuliah').val()
            //                 // search: data.term
            //             };
            //         },
            //         processResults: function(response) {
            //             return {
            //                 results: response
            //             };
            //         }
            //     }
            // })


        });

        //get data Akreditasi
        $('#prodiIdAkreditasi').on('change', function () {
            var akreditasi = $('#prodiIdAkreditasi').val();
            let akreditasisplit = akreditasi.split(",");
            let akreditasigetvalue = akreditasisplit[2]
            $('#prodiInputCoba').val(akreditasigetvalue);
        })
        //get alamat by lokasi kuliah
        $('#lokasiKuliah').on('change', function () {
            var lokasi = $('#lokasiKuliah').val();
            let splitlokasi = lokasi.split(",");
            let getalamat = splitlokasi[2]
            $('#namaAlamatCoba').val(getalamat);
        })
        $('#myselect').change();

        // switch button
        $(document).ready(function () {
            $("#pjjValue").on("change", function (e) {
                const isOn = e.currentTarget.checked;

                if (isOn) {
                    $(".pjjCheck").show();
                    document.getElementById('testNameHidden').disabled = true;
                } else {
                    $(".pjjCheck").hide();
                    document.getElementById('testNameHidden').disabled = false;
                }
            });
        });

        // untuk form edit langkah satu jika sebelumnya sudah memilih pjj
        $(document).ready(function () {
            $("#pjjValueNotNull").on("change", function (e) {
                const isOn = e.currentTarget.checked;

                if (isOn == false) {
                    $(".pjjCheck").hide();
                    document.getElementById('testNameHidden').disabled = false;
                } else {
                    $(".pjjCheck").show();
                    document.getElementById('testNameHidden').disabled = true;
                }
            });
        });


        $(".jamSenin").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            minTime: "17:00",
            wrap: true,
        });
        $(".jamSelasa").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            minTime: "17:00",
            wrap: true,
        });
        $(".jamRabu").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            minTime: "17:00",
            wrap: true,
        });
        $(".jamKamis").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            minTime: "17:00",
            wrap: true,
        });
        $(".jamJumat").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            minTime: "17:00",
            wrap: true,
        });
        $(".jamSabtu").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            wrap: true,
        });
        $(".jamMinggu").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            wrap: true,
        });

        // spinner / loading button
        function BtnLoading(elem) {
            $(elem).attr("data-original-text", $(elem).html());
            $(elem).prop("disabled", true);
            $(elem).html('<i class="spinner-border spinner-border-sm"></i>');
            setTimeout(function () {
                $(elem).prop("disabled", false);
                $(elem).html($(elem).attr("data-original-text"));
            }, 20000);
        }

        // spinner off / reset button
        function BtnReset(elem) {
            $(elem).prop("disabled", false);
            $(elem).html($(elem).attr("data-original-text"));
        }


    </script>

    <script src="{{ asset('assets/js/ibel') }}/buat_ibel.js"></script>

@endsection
