@if ( $ibel['0']->flagStep == 5 )
    <h2 class="mb-10">Langkah 2 - Data Utama</h2>
@else
    <h2 class="mb-10">Langkah 1 - Data Utama</h2>
@endif
<form class="form w-lg-1000px mx-auto" enctype="multipart/form-data"
      action=" " method="post">
    @csrf
    <!--begin::Group-->
    <div class="mb-5">
        <!--begin::Step 1-->
        <div class="flex-column current">
            <div class="card-body m-5 mt-0">
                <input type="hidden" name="stepFlag" value="{{ $ibel['0']->flagStep }}">
                <div class="row mb-3">
                    <label class="col-lg-4 col-form-label fw-bold fs-6">Jenjang
                        Pendidikan</label>
                    {{-- {{ $ibel['0']->$pegawaiId }} --}}
                    <div class="col-lg">
                        <select id="JenjangPendidikan" name="jenjangPendidikanId" data-control="select2"
                                data-hide-search="true" data-placeholder="Pilih jenjang pendidikan" type="text"
                                class="form-select form-select-lg" required>
                            <option></option>
                            @if (empty($ibel['0']->jenjangPendidikan->id))
                                @foreach ($ref_jenjang as $p)
                                    <option value="{{ $p->id }}">
                                        {{ $p->jenjangPendidikan }}</option>
                                @endforeach
                            @else
                                @foreach ($ref_jenjang as $j)
                                    <option value="{{ $j->id }}"
                                        {{ $j->id == $ibel['0']->jenjangPendidikan->id ? 'selected' : '' }}>
                                        {{ $j->jenjangPendidikan }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-lg-4 col-form-label fw-bold fs-6">Jenjang Pendidikan pada
                        Persetujuan Pencantuman Gelar</label>
                    <div class="col-lg">
                        <select id="lokasiPendidikan[]" name="jenjangPendidikanIdGelar" data-control="select2"
                                data-hide-search="true" data-placeholder="Pilih jenjang pendidikan" type="text"
                                class="form-select form-select-lg" data-allow-clear="true">
                            <option></option>
                            @if (empty($ibel['0']->flagPencantumanGelar))
                                @foreach ($ref_jenjang as $p)
                                    <option value="{{ $p->id }}">
                                        {{ $p->jenjangPendidikan }}</option>
                                @endforeach
                            @else
                                @foreach ($ref_jenjang as $j)
                                    <option value="{{ $j->id }}"
                                        {{ $j->id == $ibel['0']->jenjangPendidikanPgId->id ? 'selected' : '' }}>
                                        {{ $j->jenjangPendidikan }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>

                <div class="row mb-3">
                    <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Persetujuan
                        Pencantuman Gelar</label>
                    <div class="col-lg">
                        <div class="input-group">
                            <input type="file" name="docPencantumanGelar" class="form-control single_number_mask"
                                   placeholder="hanya angka">
                        </div>
                    </div>
                    @if ($ibel['0']->pathPencantumanGelar != null)
                        <div class="col-lg">
                            <a target="_blank" href="{{ url('files/' . $ibel['0']->pathPencantumanGelar) }}"><i
                                    class="fas fa-file-pdf fa-3x" style="color:red"></i></a>
                            <input type="hidden" name="docPencantumanGelar_lama"
                                   value="{{ $ibel['0']->pathPencantumanGelar }}">
                        </div>
                    @endif
                </div>

                <div class="row mb-3">
                    <label class="col-lg-4 col-form-label fw-bold fs-6"> Periode
                        Pendaftaran</label>
                    <div class="col-lg">
                        <div class="position-relative d-flex align-items-center">
                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                <span class="symbol-label bg-secondary">
                                    <span class="svg-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             width="24px" height="24px"
                                             viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                      rx="1"></rect>
                                                <path
                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                    fill="#000000"></path>
                                            </g>
                                        </svg>
                                    </span>
                                </span>
                            </div>
                            <input class="form-control ps-12 flatpickr-input active datepick"
                                   placeholder="Tanggal mulai" name="periodePendmulai" type="text"
                                   value="{{ $ibel['0']->periodePendaftaranAwal }}" required>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="position-relative d-flex align-items-center">
                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                <span class="symbol-label bg-secondary">
                                    <span class="svg-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             width="24px" height="24px"
                                             viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                      rx="1"></rect>
                                                <path
                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                    fill="#000000"></path>
                                            </g>
                                        </svg>
                                    </span>
                                </span>
                            </div>
                            <input class="form-control ps-12 flatpickr-input active datepick"
                                   placeholder="Tanggal selesai" name="periodePendselesai" type="text"
                                   value="{{ $ibel['0']->periodePendaftaranAwal }}" required>
                        </div>
                    </div>
                </div>

                {{-- ----------------Switch button----------------- --}}
                <div class="row mb-3">
                    <label class="col-lg-4 col-form-label fw-bold fs-6">Pembelajaran Jarak
                        Jauh</label>
                    <div class="col-lg-3">
                        <div class="form-check form-switch form-check-custom form-check-solid">
                            <label class="form-check-label col-form-label fw-bold fs-6 pjjA" for="flexSwitchDefault">
                                @if ($ibel['0']->flagPjj == 1)
                                    <input id="pjjValueNotNull" class="form-check-input" type="checkbox" name="pjjId"
                                           checked value="1"/>
                                @else
                                    <input id="pjjValue" class="form-check-input" type="checkbox" name="pjjId"
                                           value="1"/>
                                @endif
                            </label>
                            @if ($ibel['0']->flagPjj == 1)
                                <input id='testNameHidden' type='hidden' value='0' name='pjjId' disabled>
                            @else
                                <input id='testNameHidden' type='hidden' value='0' name='pjjId'>
                            @endif
                        </div>
                    </div>
                </div>
                @if ($ibel['0']->flagPjj == 1)
                    <div class="row mb-3 pjjCheck">
                        <label class="col-lg-4 col-form-label fw-bold fs-6">Penetapan Prodi
                            PJJ</label>
                        <div class="col-lg">
                            <input type="file" class="form-control" name="penetepanProdiPJJ" placeholder="misal"
                                   value="">
                        </div>
                        <div class="col-lg">
                            <a target="_blank" href="{{ url('files/' . $ibel['0']->pathPenetapanPjj) }}"><i
                                    class="fas fa-file-pdf fa-3x" style="color:red"></i></a>
                            <input type="hidden" name="docpenetepanProdiPJJ_lama"
                                   value="{{ $ibel['0']->pathPenetapanPjj }}">
                        </div>
                    </div>
                @else
                    <div class="row mb-3 pjjCheck" style="display: none;">
                        <label class="col-lg-4 col-form-label fw-bold fs-6">Penetapan Prodi
                            PJJ</label>
                        <div class="col-lg">
                            <input type="file" class="form-control" name="penetepanProdiPJJ" placeholder="misal"
                                   value="">
                        </div>
                    </div>
                @endif
                {{-- get pt --}}
                <div class="row mb-3">
                    <label class="col-lg-4 col-form-label fw-bold fs-6">Perguruan Tinggi</label>
                    <div class="col-lg">
                        <select id="getPT_ibel" name="perguruanTinggiId" class="form-select form-select-lg" required>
                            @if ($ibel['0']->perguruanTinggiIbel != null)
                                <option
                                    value="{{ $ibel['0']->perguruanTinggiIbel->id }}|{{ $ibel['0']->perguruanTinggiIbel->namaPerguruanTinggi }}"
                                    selected>
                                    {{ $ibel['0']->perguruanTinggiIbel->namaPerguruanTinggi }}</option>
                            @endif
                        </select>
                    </div>
                </div>
                {{-- get prodi --}}
                <div class="row mb-3">
                    <label class="col-lg-4 col-form-label fw-bold fs-6">Program Studi</label>
                    <div class="col-lg">
                        <select id="prodiIdAkreditasi" name="prodiId" class="form-select form-select-lg">
                            @if ($ibel['0']->ptProdiAkreditasi != null)
                                <option
                                    value="{{ $ibel['0']->ptProdiAkreditasi->id }},{{ $ibel['0']->ptProdiAkreditasi->programStudiIbelId->namaProgramStudi }},{{ $ibel['0']->ptProdiAkreditasi->akreditasiId->akreditasi }}"
                                    selected>
                                    {{ $ibel['0']->ptProdiAkreditasi->programStudiIbelId->namaProgramStudi }}
                                </option>
                            @endif
                        </select>
                    </div>
                </div>
                {{-- get Akreditasi --}}
                <div class="row mb-3">
                    <label class="col-lg-4 col-form-label fw-bold fs-6"></label>
                    <div class="col-lg">
                        <label class="col-lg col-form-label fw-bold fs-6">Akreditasi : </label>
                        <input id="prodiInputCoba" type="text"
                               @if ($ibel['0']->ptProdiAkreditasi != null)
                                   value="{{ $ibel['0']->ptProdiAkreditasi->akreditasiId->akreditasi }}"
                               @endif
                               style="border:none;"/>
                    </div>
                </div>
                {{-- get Lokasi --}}
                <div class="row mb-3">
                    <label class="col-lg-4 col-form-label fw-bold fs-6">Lokasi
                        Perkuliahan</label>
                    <div class="col-lg">
                        <select id="lokasiKuliah" name="lokasiKuliah" class="form-select form-select-lg">
                            @if ($ibel['0']->rlokasiPerkuliahan != null)
                                <option
                                    value="{{ $ibel['0']->rlokasiPerkuliahan->id }},{{ $ibel['0']->rlokasiPerkuliahan->namaKampus }},{{ $ibel['0']->rlokasiPerkuliahan->alamat }}"
                                    selected>
                                    {{ $ibel['0']->rlokasiPerkuliahan->namaKampus }}
                                </option>
                            @endif
                        </select>
                    </div>
                </div>
                {{-- get alamat --}}
                <div class="row mb-3">
                    <label class="col-lg-4 col-form-label fw-bold fs-6"></label>
                    <div class="col-lg">
                        <label class="col-lg col-form-label fw-bold fs-6">Alamat : </label>
                        <input id="namaAlamatCoba" type="text"
                               @if ($ibel['0']->rlokasiPerkuliahan != null)
                                   value="{{ $ibel['0']->rlokasiPerkuliahan->alamat }}"
                               @endif
                               style="border:none;"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center mb-8">
            <a href="{{ url('ibel/administrasi/') }}" class="btn btn-sm btn-secondary my-1"><i
                    class="fas fa-chevron-left"></i>Batal</a>
            <button type="submit" class="btn btn-sm btn-success">Selanjutnya <i
                    class="fas fa-chevron-right"></i></button>
        </div>
    </div>
</form>
