<div class="table-responsive bg-gray-400 rounded border border-gray-300 shadow-sm">
    <table class="table table-striped align-middle">
        <thead class="fw-bolder bg-secondary fs-6">
        <tr class="text-right">
            <th class="text-center w-75px">No</th>
            <th class="text-center">Pemohon</th>
            <th class="text-center">Aksi</th>
        </tr>
        </thead>
        <tbody>

        @if (!empty($ibel['data']))
            @php
                $nomor = 1;
            @endphp

            @foreach ($ibel['data'] as $i)
                <tr id="{{ $i->id }}" class="text-dark fs-6">
                    <td class="text-center ps-2 align-top">
                        {{ $nomor++ }}
                    </td>

                    <td>
                        <div class="row">
                        <span>
                            <a href="javascript:void(0)" class="text-primary fw-bold btnTiket"
                               data-tiket="{{ $i->logStatusProbis->nomorTiket }}" data-nama="{{ $i->namaPegawai }}">
                                {{ $i->logStatusProbis->nomorTiket }}
                            </a>
                        </span>
                        </div>

                        <div class="row">
                            <div class="col-lg">
                                <div class="fw-bolder mt-3">
                                    {{ $i->namaPegawai }}
                                </div>
                                <div class="fw-normal">
                                    {{ $i->nip18 }}
                                    <br>
                                    {{ $i->namaKantor }}
                                </div>
                            </div>

                            <div class="col-lg">
                                <div class="fw-normal mt-3">
                                    Jenjang Pendidikan : {{ $i->jenjangPendidikan->jenjangPendidikan }}
                                    <br>
                                    Program Studi : {{ $i->ptProdiAkreditasi->programStudiIbelId->namaProgramStudi }}
                                    <br>
                                    Perkuliahan : {{ $i->flagPjj == 1 ? 'PJJ' : 'Reguler' }}
                                    <br>
                                    <br>
                                    Perguruan Tinggi : {{ $i->perguruanTinggiIbel->namaPerguruanTinggi }}
                                    <br>
                                    Lokasi : {{ $i->perguruanTinggiIbel->alamat }}
                                </div>
                            </div>

                            <div class="col-lg">
                                <div></div>
                                @if (!empty($ibel_jadwal))
                                    <div class="fw-normal mt-3">
                                        <div class="fw-bolder fs-6">
                                            Jadwal Perkuliahan
                                        </div>
                                        <div>
                                            <table class="table align-middle gy-0">
                                                @foreach ($ibel_jadwal as $key => $value)
                                                    @if ($i->id == $key)
                                                        @foreach ($value as $val)
                                                            <tr>
                                                                <td style="width: 50px">
                                                                    {{ AppHelper::instance()->number_to_indonesian_date($val['hari']) }}
                                                                </td>
                                                                <td class="min-w-100px">
                                                                    {{ $val['jamMulai'] }}
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                @else
                                    jadwal belum diinput
                                @endif
                            </div>
                        </div>
                    </td>

                    <td class="text-center col-lg-2 align-top">
                        @if ($tab == 'menunggu-pemeriksaan')
                            <a href="{{ env('APP_URL') . '/ibel/periksa/permohonan/' . $i->id }}"
                               class=" btn btn-sm btn-primary">
                                <i class="fa fa-paste"></i> Periksa
                            </a>
                        @else
                            <a href="{{ env('APP_URL') . '/ibel/periksa/lihat/' . $i->id }}"
                               class=" btn btn-icon btn-sm btn-info" data-bs-toggle="tooltip" data-bs-placement="top"
                               title="Lihat Permohonan">
                                <i class="fa fa-eye"></i>
                            </a>
                        @endif
                    </td>
                </tr>

            @endforeach

        @else

            <tr class="text-center">
                <td class="text-dark  mb-1 fs-6" colspan="5">Tidak terdapat data</td>
            </tr>

        @endif

        </tbody>
    </table>

</div>

@if (!empty($ibel))
    <!--begin::pagination-->
    <div class="d-flex flex-stack flex-wrap pt-5 p-3">
        <div class="fs-6 fw-bold text-gray-700"></div>

        @php
            $disabled = '';
            $nextPage= '';
            $disablednext = '';
            $hidden = '';
            $pagination = '';

            // tombol pagination
            if ($ibel['totalItems'] == 0) {
            $pagination = 'none';
            }

            // tombol back
            if($ibel['currentPage'] == 1){
            $disabled = 'disabled';
            }

            // tombol next
            if($ibel['currentPage'] != $ibel['totalPages']) {
            $nextPage = $ibel['currentPage'] + 1;
            }

            // tombol pagination
            if($ibel['currentPage'] == $ibel['totalPages'] || $ibel['totalPages'] == 0){
            $disablednext = 'disabled';
            $hidden = 'hidden';
            }

        @endphp

            <!--begin::Pages-->
        <ul class="pagination" style="display: {{ $pagination }}">
            <li class="page-item disabled"><a href="#" class="page-link">Halaman {{ $ibel['currentPage'] }} dari {{
                $ibel['totalPages']
                }}</a></li>
            <li class="page-item previous {{ $disabled }}"><a
                    href="{{ url()->current() . '?page=' . ($ibel['currentPage'] - 1) }}" class="page-link"><i
                        class="previous"></i></a></li>
            <li class="page-item active"><a href="{{ url()->current() . '?page=' . $ibel['currentPage'] }}"
                                            class="page-link">{{
                $ibel['currentPage'] }}</a></li>
            <li class="page-item" {{ $hidden }}><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                   class="page-link">{{
                $nextPage }}</a></li>
            <li class="page-item next {{ $disablednext }}"><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                              class="page-link"><i class="next"></i></a></li>
        </ul>
        <!--end::Pages-->

    </div>
    <!--end::paging-->
@endif
