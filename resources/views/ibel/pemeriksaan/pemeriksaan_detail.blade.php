@extends('layout.master')

@section('content')

    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="card border-warning">
                <div class="card-header" id="card-header">
                    <h3 class="card-title">
                        <span class="fw-bold fs-4 text-white">Pemeriksaan Permohonan</span>
                    </h3>
                </div>
                @include('ibel._component.detail_permohonan')
                @if ($action == true)
                    <div class="card-footer">
                        <div class="text-center">
                            <a href="{{ env(" APP_URL") . '/ibel/periksa/' }}" type="button"
                               class="btn btn-sm btn-secondary"
                               data-bs-dismiss="modal"><i class="fa fa-reply"></i>Batal</a>
                            <button type="button" class="btn btn-sm btn-danger btnTolakPermohonan"
                                    data-id="{{ $ibel['id'] }}"><i class="fa fa-times"></i>Tolak
                            </button>
                            <button type="button" class="btn btn-sm btn-success btnSetujuPermohonan"
                                    data-id="{{ $ibel['id'] }}"><i class="fa fa-check-circle"></i>Setuju
                            </button>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

    @include('ibel.modal_tiket')

@endsection

@section('js')
    <script src="{{ asset('assets/js/ibel') }}/periksa.js"></script>
    <script src="{{ asset('assets/js/ibel') }}/log-tiket.js"></script>
@endsection
