@extends('layout.master')

@section('content')

    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card border-warning">
                    <div class="card-header card-header-stretch" id="card-header">
                        <div class="card-toolbar">
                            <ul class="nav nav-tabs nav-line-tabs nav-stretch border-transparent fs-5 fw-bolder"
                                id="tabLps">
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'menunggu-pemeriksaan' ? 'active' : '' }}"
                                       href="{{ url('ibel/periksa/menunggu-pemeriksaan') }}"> Menunggu Pemeriksaan </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'ditolak' ? 'active' : '' }}"
                                       href="{{ url('ibel/periksa/ditolak') }}"> Ditolak </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'diteruskan' ? 'active' : '' }}"
                                       href="{{ url('ibel/periksa/diteruskan') }}"> Diteruskan </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @include('ibel._component.advance_search')
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'menunggu-pemeriksaan' ? 'active show' : '' }}"
                                 id="menunggu-pemeriksaan" role="tabpanel">
                                @if ($tab == 'menunggu-pemeriksaan')
                                    @include('ibel.pemeriksaan.pemeriksaan_tabel')
                                @endif
                            </div>

                            <div class="tab-pane fade {{ $tab == 'ditolak' ? 'active show' : '' }}" id="ditolak"
                                 role="tabpanel">
                                @if ($tab == 'ditolak')
                                    @include('ibel.pemeriksaan.pemeriksaan_tabel')
                                @endif
                            </div>

                            <div class="tab-pane fade {{ $tab == 'diteruskan' ? 'active show' : '' }}" id="diteruskan"
                                 role="tabpanel">
                                @if ($tab == 'diteruskan')
                                    @include('ibel.pemeriksaan.pemeriksaan_tabel')
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('ibel.modal_tiket')

@endsection

@section('js')

    <script src="{{ asset('assets/js/ibel') }}/periksa.js"></script>
    <script src="{{ asset('assets/js/ibel') }}/log-tiket.js"></script>

@endsection
