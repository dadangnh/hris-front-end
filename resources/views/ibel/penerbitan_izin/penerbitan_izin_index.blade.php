@extends('layout.master')

@section('content')

    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card border-warning">
                    <div class="card-header card-header-stretch" id="card-header">
                        <div class="card-toolbar">
                            <ul class="nav nav-tabs nav-line-tabs nav-stretch border-transparent fs-5 fw-bolder"
                                id="tabLps">
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'penerbitan' ? 'active' : '' }}"
                                       href="{{ url('ibel/penerbitan-izin/penerbitan') }}"> Penerbitan Izin Pendidikan
                                        Diluar Kedinasan (DS) </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'telah-diterbitkan' ? 'active' : '' }}"
                                       href="{{ url('ibel/penerbitan-izin/telah-diterbitkan') }}"> Telah ditandatangani
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @include('ibel._component.advance_search')
                            @if ($tab=='penerbitan')
                                <div class="col-lg-6 mb-4">
                                    <div class="text-end">
                                        <button class="btn btn-primary btn-sm fs-6 btnDsPermohonan">
                                            <i class="fas fa-pen-alt"></i> Tandatangani (DS)
                                        </button>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'penerbitan' ? 'active show' : '' }}" role="tabpanel">
                                @if ($tab == 'penerbitan')
                                    @include('ibel.penerbitan_izin.penerbitan_izin_tabel')
                                @endif
                            </div>

                            <div class="tab-pane fade {{ $tab == 'telah-diterbitkan' ? 'active show' : '' }}"
                                 role="tabpanel">
                                @if ($tab == 'telah-diterbitkan')
                                    @include('ibel.penerbitan_izin.penerbitan_izin_tabel')
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('ibel.modal_tiket')

    <div class="modal fade" id="modal_ds" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h4 class="text-white">Digital Signature</h4>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                    </div>
                </div>
                <form class="form" method="post" action="{{ env('APP_URL') . '/ibel/penerbitan-izin/ds' }}">
                    <div class="modal-body">
                        @csrf
                        <input id="id_permohonan_ibel" type="hidden" class="form-control" name="id_permohonan_ibel"/>
                        <div class="row">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Masukkan PIN DS Anda</label>
                            <div class="col-lg">
                                <input type="password" class="form-control" name="passphrase" placeholder="PIN"
                                       required/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="ms-auto">
                            <button type="button" class="btn btn-sm btn-secondary me-3" data-bs-dismiss="modal"><i
                                    class="fa fa-reply"></i>Batal
                            </button>
                            <button type="submit" class="btn btn-sm btn-success">
                                <i class="fas fa-pen-alt"></i> Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalPreview" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header card-header">
                    <h3 class="modal-title text-white namafile" id="exampleModalLabel">Preview File</h3>
                </div>
                <div class="modal-body embed-responsive">
                </div>
                <div class="text-center mb-8">
                    <button type="button" class="btn btn-sm btn-secondary my-1" data-bs-dismiss="modal"><i
                            class="fa fa-times"></i> Close
                    </button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script src="{{ asset('assets/js/ibel') }}/penerbitan-kep.js"></script>
    <script src="{{ asset('assets/js/ibel') }}/log-tiket.js"></script>

    <script>
        $(document).on("click", ".modalPreview", function () {
            var srce = $(this).data('url');
            $(".modal-body").html($('<iframe/>', {
                style: "width:100%;height:100%;",
                src: srce,
                frameborder: "1"
            }));
            var namafile = $(this).data('file');
            $(".modal-title").text("File " + namafile)
        });

    </script>

@endsection
