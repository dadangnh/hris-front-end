@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card mb-3 border-warning shadow-sm">
                    <div id="card-header" class="card-header">
                        <div class="card-title fw-bold fs-4 text-white">
                            Izin Pendidikan Anda
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10"></div>
                            <div class="col-md-2">
                                <div class="text-end mb-4">
                                    <a href="{{ url('ibel/administrasi/isi-data-utama') }}"
                                       class="btn btn-sm btn-primary fs-6 " id="btnKelola2">
                                        <i class="fa fa-plus"></i> Tambah
                                    </a>
                                    {{-- <button class="btn btn-primary btn-sm fs-6" type="button" hidden disabled>
                                        Loading...
                                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                    </button> --}}
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive bg-gray-400 rounded shadow-sm">
                            <table class="table table-striped align-middle datatable">
                                <thead class="fw-bolder bg-secondary fs-4">
                                <tr class="text-center">
                                    <th class="ps-2">No</th>
                                    <th>Izin Pendidikan</th>
                                    <th class="pe-2">Fungsi</th>
                                </tr>
                                </thead>
                                <tbody class="bg-secondary fs-6" id="izin_belajar">
                                @if (!empty($ibel['data']))
                                    @php
                                        $nomor = 1;
                                    @endphp
                                    @foreach ($ibel['data'] as $key => $i)
                                        <tr id="{{ $i['ibel']->id }}">
                                            <td class="text-center align-top">{{ $nomor++ }}</td>
                                            <td class="text-right">
                                                {{-- Status dan Tiket --}}
                                                <div class="row">
                                                    <div class="col-lg-2">
                                                        Status
                                                        <br>
                                                        <br>
                                                        <br>
                                                        @if (!empty($i['kep_seleksi']))
                                                            {{-- flag diterima --}}
                                                            @if ($i['kep_seleksi']->flagDiterima == 1 && $i['ibel']->logStatusProbis->output == 'Izin Pendidikan diluar Kedinasan diterbitkan')
                                                                <a href="javascript:void(0)"
                                                                   class="btn btn-outline btn-outline-primary btn-active-primary btn-sm fs-8 btnLihatKepIbel"
                                                                   data-nomor_surat_ibel="{{ $i['surat_izin']->nomorSurat }}"
                                                                   data-tanggal_surat_ibel="{{ $i['surat_izin']->tglSurat }}"
                                                                   data-dok_surat_ibel="{{ $i['surat_izin']->pathDs }}"
                                                                   data-nomor_kep_ibel="{{ $i['kep_seleksi']->nomorKep }}"
                                                                   data-tanggal_kep_ibel="{{ $i['kep_seleksi']->tglKep }}"
                                                                   data-dok_kep_ibel="{{ $i['kep_seleksi']->pathKeputusanSeleksi }}">
                                                                    <i class="fa fa-file"></i>
                                                                    Lihat Surat Izin
                                                                </a>

                                                                {{-- flag ditolak --}}
                                                            @elseif($i['kep_seleksi']->flagDiterima == 0 && $i['ibel']->logStatusProbis->output == 'ND Pemberitahuan Penolakan telah diunggah')
                                                                <a href="javascript:void(0)"
                                                                   class="btn btn-outline btn-outline-primary btn-active-primary btn-sm fs-8 btnLihatSuratTolak"
                                                                   data-nomor_surat_penolakan="{{ $i['surat_penolakan']->nomorSuratPenolakan }}"
                                                                   data-tanggal_surat_penolakan="{{ $i['surat_penolakan']->tglSuratPenolakan }}"
                                                                   data-dok_surat_penolakan="{{ $i['surat_penolakan']->pathSuratPenolakan }}"
                                                                   data-nomor_surat_pemeberitahuan="{{ $i['surat_penolakan']->nomorSuratPemberitahuan }}"
                                                                   data-tanggal_surat_pemberitahuan="{{ $i['surat_penolakan']->tglSuratPemberitahuan }}"
                                                                   data-dok_surat_pemberitahuan="{{ $i['surat_penolakan']->pathSuratPemberitahuan }}">
                                                                    <i class="fa fa-file"></i>
                                                                    Lihat Surat Penolakan
                                                                </a>
                                                            @endif
                                                        @endif
                                                    </div>
                                                    <div class="col-lg-10">
                                                        @if ($i['ibel']->logStatusProbis->output == 'Permohonan dikembalikan')
                                                            <span class="badge badge-light-warning fs-4 fw-bolder"
                                                                  style="padding-left: 0px;">{{ $i['ibel']->logStatusProbis->output }}</span>
                                                        @elseif ($i['ibel']->logStatusProbis->output == 'Kep hasil seleksi direkam, hasil ditolak')
                                                            <span class="badge badge-light-danger fs-4 fw-bolder"
                                                                  style="padding-left: 0px;">{{ $i['ibel']->logStatusProbis->output }}</span>
                                                        @elseif ($i['ibel']->logStatusProbis->output == 'ND Pemberitahuan Penolakan telah diunggah')
                                                            <span class="badge badge-light-danger fs-4 fw-bolder"
                                                                  style="padding-left: 0px;">{{ $i['ibel']->logStatusProbis->output }}</span>
                                                        @else
                                                            <span class="badge badge-light-success fs-4 fw-bolder"
                                                                  style="padding-left: 0px;">{{ $i['ibel']->logStatusProbis->output }}</span>
                                                        @endif
                                                        <br>
                                                        <a href="javascript:void(0)" class="text-primary btnTiket"
                                                           data-tiket="{{ $i['ibel']->logStatusProbis->nomorTiket }}"
                                                           data-nama="{{ $i['ibel']->namaPegawai }}">
                                                            {{ $i['ibel']->logStatusProbis->nomorTiket }} </a>
                                                        <br>
                                                    </div>
                                                </div>

                                                {{-- Data Pendidikan --}}
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <br>
                                                        Jenjang Pendidikan
                                                        <br>
                                                        Program Studi
                                                        <br>
                                                        PJJ/Reguler
                                                        <br>
                                                        <br>
                                                        Perguruan Tinggi
                                                        <br>
                                                        Lokasi Perkuliahan
                                                        <br>
                                                        <br>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <br>
                                                        : {{ $i['ibel']->jenjangPendidikan->jenjangPendidikan }}
                                                        <br>
                                                        :
                                                        {{ $i['ibel']->ptProdiAkreditasi->programStudiIbelId->namaProgramStudi }}
                                                        <br>
                                                        @if ($i['ibel']->flagPjj == 1)
                                                            : PJJ
                                                        @else
                                                            : Reguler
                                                        @endif
                                                        <br>
                                                        <br>
                                                        : {{ $i['ibel']->perguruanTinggiIbel->namaPerguruanTinggi }}
                                                        <br>
                                                        : {{ $i['ibel']->rlokasiPerkuliahan->namaKampus }}
                                                        <br>
                                                        : {{ $i['ibel']->rlokasiPerkuliahan->alamat }}
                                                        <br>

                                                    </div>
                                                    <div class="col-lg-5">
                                                        Jadwal Perkuliahan
                                                        <table class="table align-middle gy-0">
                                                            @if (!empty($ibel['data']))
                                                                @if (!empty($i['jadwal']))
                                                                    @foreach ($i['jadwal'] as $j)
                                                                        @if (!empty($j->jamMulai))
                                                                            <tr>
                                                                                <td style="width: 50px">
                                                                                    {{ AppHelper::instance()->number_to_indonesian_date($j->hari) }}
                                                                                </td>
                                                                                <td class="min-w-100px">
                                                                                    {{ $j->jamMulai }}
                                                                                </td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @else
                                                                Jadwal Belum Diinput
                                                            @endif
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg fw-bolder">Dokumen Persyaratan</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg">
                                                        <i class="fa fa-check"
                                                           style={{ $i['ibel']->pathTidakUpkp != null ? 'color:rgb(7,241,7)' : '' }}></i>
                                                        S. Pernyataan Tidak menuntut UPKP
                                                        <br>
                                                        <i class="fa fa-check"
                                                           style={{ $i['ibel']->pathKebutuhanProdi != null ? 'color:rgb(7,241,7)' : '' }}></i>
                                                        S. Pernyataan Prodi Sesuai Kebutuhan Organisasi
                                                        <br>
                                                        <i class="fa fa-check"
                                                           style={{ $i['ibel']->pathSkorsing != null ? 'color:rgb(7,241,7)' : '' }}></i>
                                                        S. Ket. Tidak Menjalani Skorsing
                                                        <br>
                                                        <i class="fa fa-check"
                                                           style={{ $i['ibel']->pathHukdis != null ? 'color:rgb(7,241,7)' : '' }}></i>
                                                        S. Ket. tidak sedang menjalani hukdis/pemeriksaan
                                                        <br>
                                                        <i class="fa fa-check"
                                                           style={{ $i['ibel']->pathJarak != null ? 'color:rgb(7,241,7)' : '' }}></i>
                                                        S. Ket. jarak lokasi perkuliahan
                                                        <br>
                                                        <i class="fa fa-check"
                                                           style={{ $i['ibel']->pathJadwal != null ? 'color:rgb(7,241,7)' : '' }}></i>
                                                        S. Ket. waktu pendidikan di luar jam kerja
                                                    </div>
                                                    <div class="col-lg">
                                                        <i class="fa fa-check"
                                                           style={{ $i['ibel']->pathBrosur != null ? 'color:rgb(7,241,7)' : '' }}></i>
                                                        Brosur Penerimaan Mahasiswa Baru
                                                        <br>
                                                        <i class="fa fa-check"
                                                           style={{ $i['ibel']->pathAkreditasi != null ? 'color:rgb(7,241,7)' : '' }}></i>
                                                        Keputusan/Sertifikat Akreditasi
                                                        <br>
                                                        <i class="fa fa-check"
                                                           style={{ $i['ibel']->pathIjazah != null ? 'color:rgb(7,241,7)' : '' }}></i>
                                                        Ijazah yang telah diakui
                                                        <br>
                                                        <i class="fa fa-check"
                                                           style={{ $i['ibel']->pathTranskrip != null ? 'color:rgb(7,241,7)' : '' }}></i>
                                                        Transkrip Nilai
                                                        <br>
                                                        <i class="fa fa-check"
                                                           style={{ $i['ibel']->pathSehat != null ? 'color:rgb(7,241,7)' : '' }}></i>
                                                        Surat Keterangan Berbadan Sehat
                                                        <br>
                                                        <i class="fa fa-check"
                                                           style={{ $i['ibel']->pathCpns != null ? 'color:rgb(7,241,7)' : '' }}></i>
                                                        SK CPNS
                                                        <br>
                                                        <i class="fa fa-check"
                                                           style={{ $i['ibel']->pathPns != null ? 'color:rgb(7,241,7)' : '' }}></i>
                                                        SK PNS
                                                        <br>
                                                        <i class="fa fa-check"
                                                           style={{ $i['ibel']->pathPangkat != null ? 'color:rgb(7,241,7)' : '' }}></i>
                                                        SK Pangkat Terakhir
                                                        <br>
                                                        <i class="fa fa-check"
                                                           style={{ $i['ibel']->pathPpkpns1 != null ? 'color:rgb(7,241,7)' : '' }}></i>
                                                        PPKPNS (1)
                                                        <br>
                                                        <i class="fa fa-check"
                                                           style={{ $i['ibel']->path_ppkpns2 != null ? 'color:rgb(7,241,7)' : '' }}></i>
                                                        PPKPNS (2)
                                                        <br>

                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center text-dark mb-1 fs-6 align-top">
                                                <div class="mt-2">
                                                    <a href="{{ env('APP_URL') . '/ibel/administrasi/lihat-draft/' . $i['ibel']->id }}"
                                                       class=" btn btn-icon btn-sm btn-info fs-6"
                                                       data-bs-toggle="tooltip" data-bs-placement="top"
                                                       title="Lihat Permohonan">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                    @if ($i['ibel']->logStatusProbis->output == 'Draft telah dibuat' || $i['ibel']->logStatusProbis->output == 'Permohonan dikembalikan')
                                                        <button class="btn btn-icon btn-sm btn-danger fs-6 btnHapusIbel"
                                                                data-bs-toggle="tooltip" data-bs-placement="top"
                                                                title="Hapus"
                                                                data-id_ref_ibel_hapus="{{ $i['ibel']->id }}">
                                                            <i class="fa fa-trash"></i>
                                                        </button>

                                                        <a @if ($i['ibel']->flagStep == 1) href="{{ url('ibel/administrasi/isi-jadwal-perkuliahan/' . $i['ibel']->id) }}"
                                                           @elseif ($i['ibel']->flagStep == 2)
                                                               href="{{ url('ibel/administrasi/upload-dokumen-persyaratan/' . $i['ibel']->id) }}"
                                                           @elseif ($i['ibel']->flagStep == 3)
                                                               href="{{ url('ibel/administrasi/upload-dokumen-persyaratan/' . $i['ibel']->id) }}"
                                                           @elseif ($i['ibel']->flagStep == 4)
                                                               href="{{ url('ibel/administrasi/konfirmasi-tatap-muka/' . $i['ibel']->id) }}"
                                                           @elseif ($i['ibel']->flagStep == 5)
                                                               href="{{ url('ibel/administrasi/isi-data-utama/edit/' . $i['ibel']->id) }}"
                                                           @elseif ($i['ibel']->flagStep == 7)
                                                               href="{{ url('ibel/administrasi/langkah-empat-mutasi/' . $i['ibel']->id) }}"
                                                           @endif
                                                           class="btn btn-icon btn-sm btn-warning fs-6 "
                                                           id="btnKelola2">
                                                            <i class="fa fa-edit"></i>
                                                        </a>

                                                        <button class="btn btn-sm btn-primary fs-6 btnKirimIbel"
                                                                data-url="{{ env('APP_URL') . '/ibel/administrasi/kirim' }}"
                                                                data-id="{{ $i['ibel']->id }}">
                                                            <i class="fa fa-paper-plane"></i> Kirim Laporan
                                                        </button>
                                                    @endif
                                                </div>
                                                @if ($i['ibel']->logStatusProbis->output == 'Izin Pendidikan diluar Kedinasan diterbitkan')
                                                    <div class="mt-2">
                                                        <a href="{{ url('/ibel/administrasi/konfirmasi-ibel/' . $i['ibel']->id) }}"
                                                           class="btn btn-sm btn-primary fs-6 " id="btnKelola3">
                                                            <i class="fa fa-plus"></i> Buat Izin Karena Mutasi
                                                        </a>
                                                    </div>
                                                @endif
                                            </td>

                                        </tr>
                                    @endforeach
                                @else
                                    <tr class="text-center">
                                        <td class="text-dark  mb-1 fs-6" colspan="7">Tidak terdapat data</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('ibel.modal_tiket')
    @include('ibel.administrasi.modal')
@endsection


@section('js')
    <script src="{{ asset('assets/js/ibel') }}/administrasi.js"></script>
    <script src="{{ asset('assets/js/ibel') }}/log-tiket.js"></script>
@endsection
