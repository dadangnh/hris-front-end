@extends('layout.master')

@section('content')
    <!--begin::Stepper-->
    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card mb-3 border-warning shadow-sm">
                    <div id="card-header" class="card-header">
                        <div class="card-title fw-bold fs-4 text-white">
                            Izin Pendidikan Anda
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="stepper stepper-links d-flex flex-column" id="kt_stepper_example_basic">
                            <div class="stepper-nav py-5">
                                <!--begin::Step 1-->
                                <div class="stepper-item current" data-kt-stepper-element="nav">
                                    <h3 class="stepper-title">Langkah 1 - Data Utama</h3>
                                </div>

                                <!--begin::Step 2-->
                                <div class="stepper-item" data-kt-stepper-element="nav">
                                    <h3 class="stepper-title">Langkah 2 - Jadwal Perkuliahan</h3>
                                </div>

                                <!--begin::Step 3-->
                                <div class="stepper-item" data-kt-stepper-element="nav">
                                    <h3 class="stepper-title">Langkah 3 - Unggah Dokumen Persyaratan</h3>
                                </div>

                                <!--begin::Step 4-->
                                <div class="stepper-item" data-kt-stepper-element="nav">
                                    <h3 class="stepper-title">Completed</h3>
                                </div>
                                <!--end::Step 4-->
                            </div>


                            <!--begin::Form-->
                            <form class="form w-lg-1000px mx-auto" id="kt_stepper_example_basic_form"
                                  action="{{ url('ibel/administrasi/tambah_izin') }}" method="post">
                                @csrf
                                <!--begin::Group-->
                                <div class="mb-5">
                                    <!--begin::Step 1-->
                                    <div class="flex-column current" data-kt-stepper-element="content">
                                        <div class="card-body m-5 mt-0">
                                            <div class="row mb-3">
                                                <label class="col-lg-4 col-form-label fw-bold fs-6">Jenjang
                                                    Pendidikan</label>
                                                <div class="col-lg">
                                                    <select id="JenjangPendidikan" name="jenjangPendidikanId"
                                                            data-control="select2" data-hide-search="true"
                                                            data-placeholder="Pilih jenjang pendidikan" type="text"
                                                            class="form-select form-select-lg" required>
                                                        <option selected disabled>Pilih Jenjang Pendidikan</option>
                                                        @foreach ($ref_prodi as $p)
                                                            <option value="{{ $p->id }}">
                                                                {{ $p->jenjangPendidikan }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <label class="col-lg-4 col-form-label fw-bold fs-6">Jenjang Pendidikan
                                                    pada
                                                    Persetujuan Pencantuman Gelar</label>
                                                <div class="col-lg">
                                                    <select id="lokasiPendidikan[]" name="jenjangPendidikanIdGelar"
                                                            data-control="select2" data-hide-search="true"
                                                            data-placeholder="Pilih jenjang pendidikan" type="text"
                                                            class="form-select form-select-lg" required>
                                                        <option selected disabled>Pilih Jenjang Pendidikan</option>
                                                        @foreach ($ref_prodi as $p)
                                                            <option value="{{ $p->id }}">
                                                                {{ $p->jenjangPendidikan }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row mb-3">
                                                <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Persetujuan
                                                    Pencantuman Gelar</label>
                                                <div class="col-lg">
                                                    <div class="input-group">
                                                        <input type="file" name="docPencantumanGelar"
                                                               class="form-control single_number_mask"
                                                               placeholder="hanya angka" value="" required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mb-3">
                                                <label class="col-lg-4 col-form-label fw-bold fs-6"> Periode
                                                    Pendaftaran</label>
                                                <div class="col-lg">
                                                    <div class="position-relative d-flex align-items-center">
                                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                            <span class="symbol-label bg-secondary">
                                                                <span class="svg-icon">
                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                         width="24px" height="24px" viewBox="0 0 24 24"
                                                                         version="1.1">
                                                                        <g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24"
                                                                                  height="24"></rect>
                                                                            <rect fill="#000000" opacity="0.3" x="4"
                                                                                  y="4"
                                                                                  width="4" height="4" rx="1"></rect>
                                                                            <path
                                                                                d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                                fill="#000000"></path>
                                                                        </g>
                                                                    </svg>
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <input
                                                            class="form-control ps-12 flatpickr-input active datepick"
                                                            placeholder="Tanggal mulai" name="periodePendmulai"
                                                            type="text"
                                                            value="" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg">
                                                    <div class="position-relative d-flex align-items-center">
                                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                            <span class="symbol-label bg-secondary">
                                                                <span class="svg-icon">
                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                         width="24px" height="24px" viewBox="0 0 24 24"
                                                                         version="1.1">
                                                                        <g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24"
                                                                                  height="24"></rect>
                                                                            <rect fill="#000000" opacity="0.3" x="4"
                                                                                  y="4"
                                                                                  width="4" height="4" rx="1"></rect>
                                                                            <path
                                                                                d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                                fill="#000000"></path>
                                                                        </g>
                                                                    </svg>
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <input
                                                            class="form-control ps-12 flatpickr-input active datepick"
                                                            placeholder="Tanggal selesai" name="periodePendselesai"
                                                            type="text" value="" required>
                                                    </div>
                                                </div>
                                            </div>

                                            {{-- ----------------Switch button----------------- --}}
                                            <div class="row mb-3">
                                                <label class="col-lg-4 col-form-label fw-bold fs-6">Pembelajaran Jarak
                                                    Jauh</label>
                                                <div class="col-lg-3">
                                                    {{-- <div class="position-relative d-flex align-items-center">
                                                        <label
                                                            class="form-check form-switch form-check-custom form-check-solid pjjA">
                                                        </label>
                                                    </div> --}}
                                                    <div
                                                        class="form-check form-switch form-check-custom form-check-solid">
                                                        <label class="form-check-label col-form-label fw-bold fs-6 pjjA"
                                                               for="flexSwitchDefault">
                                                            <input id="pjjValue" class="form-check-input"
                                                                   type="checkbox"
                                                                   name="pjjId" value="1"/>
                                                        </label>
                                                        <input id='testNameHidden' type='hidden' value='0' name='pjjId'>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mb-3 pjjCheck" style="display: none;">
                                                <label class="col-lg-4 col-form-label fw-bold fs-6">Penetapan Prodi
                                                    PJJ</label>
                                                <div class="col-lg">
                                                    <input type="file" class="form-control" name="penetepanProdiPJJ"
                                                           placeholder="misal" value="" required>
                                                </div>
                                            </div>

                                            <div class="row mb-3">
                                                <label class="col-lg-4 col-form-label fw-bold fs-6">Perguruan
                                                    Tinggi</label>
                                                <div class="col-lg">
                                                    <select id="kantorAsal" name="perguruanTinggiId"
                                                            class="form-select form-select-lg">

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <label class="col-lg-4 col-form-label fw-bold fs-6">Program
                                                    Studi</label>
                                                <div class="col-lg">
                                                    <select id="jabatanAsal" name="prodiId"
                                                            class="form-select form-select-lg">

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <label class="col-lg-4 col-form-label fw-bold fs-6"></label>
                                                <label class="col-lg col-form-label fw-bold fs-6">Akreditasi : </label>
                                            </div>
                                            <div class="row mb-3">
                                                <label class="col-lg-4 col-form-label fw-bold fs-6">Lokasi
                                                    Perkuliahan</label>
                                                <div class="col-lg">
                                                    <select id="lokasiKuliah" name="lokasiKuliah"
                                                            class="form-select form-select-lg">

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <label class="col-lg-4 col-form-label fw-bold fs-6"></label>
                                                <label class="col-lg col-form-label fw-bold fs-6">Alamat : </label>
                                            </div>
                                        </div>
                                    </div>

                                    <!--begin::Step 2-->
                                    <div class="flex-column" data-kt-stepper-element="content">
                                        <div class="table-responsive-lg bg-gray-400 rounded border border-gray-300">
                                            <table class="table table-striped align-middle" id="tabel_tubel">
                                                <thead class="fw-bolder bg-secondary fs-6">
                                                <tr class="text-center">
                                                    <th class="fs-2">Hari</th>
                                                    <th class="fs-2">Jam Mulai Perkuliahan</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr class="text-center">
                                                    <td class="text-dark mb-1 fs-4">Senin
                                                    </td>
                                                    <td class="text-dark mb-1 fs-6 text-center">
                                                        <div class="row">
                                                            <div class="col-sm-4"></div>
                                                            <div class="col-sm-4 jamSeninCoba">
                                                                {{-- <input
                                                                class="form-control form-control-solid flatpickr-input jamSenin2"
                                                                placeholder="Pick time"  type="number"
                                                                readonly="readonly" name="jamSenin"> --}}
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <input class="form-control jamSenin2"
                                                                               type="text" placeholder="HH:MM"
                                                                               name="jamSenin" data-input>
                                                                    </div>
                                                                    <div class="text-start col-sm-6 input-group-append">
                                                                        <a class=" btn btn-outline-secondary btn-danger input-button"
                                                                           title="clear" data-clear>X</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4"></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="text-center">
                                                    <td class="text-dark  mb-1 fs-4">Selasa</td>
                                                    <td class="text-dark  mb-1 fs-6">
                                                        <div class="row">
                                                            <div class="col-sm-4"></div>
                                                            <div class="col-sm-4 jamSelasa">
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <input class="form-control jamSelasa2"
                                                                               type="text" placeholder="HH:MM"
                                                                               name="jamSelasa" data-input>
                                                                    </div>
                                                                    <div
                                                                        class="text-start col-sm-6 input-group-append">
                                                                        <a class=" btn btn-outline-secondary btn-danger input-button"
                                                                           title="clear" data-clear>X</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4"></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="text-center">
                                                    <td class="text-dark  mb-1 fs-4">Rabu</td>
                                                    <td class="text-dark  mb-1 fs-6">
                                                        <div class="row">
                                                            <div class="col-sm-4"></div>
                                                            <div class="col-sm-4 jamRabu">
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <input class="form-control jamRabu2"
                                                                               type="text" placeholder="HH:MM"
                                                                               name="jamRabu" data-input>
                                                                    </div>
                                                                    <div
                                                                        class="text-start col-sm-6 input-group-append">
                                                                        <a class=" btn btn-outline-secondary btn-danger input-button"
                                                                           title="clear" data-clear>X</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4"></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="text-center">
                                                    <td class="text-dark  mb-1 fs-4">Kamis</td>
                                                    <td class="text-dark  mb-1 fs-6">
                                                        <div class="row">
                                                            <div class="col-sm-4"></div>
                                                            <div class="col-sm-4 jamKamis">
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <input class="form-control jamKamis2"
                                                                               type="text" placeholder="HH:MM"
                                                                               name="jamKamis" data-input>
                                                                    </div>
                                                                    <div
                                                                        class="text-start col-sm-6 input-group-append">
                                                                        <a class=" btn btn-outline-secondary btn-danger input-button"
                                                                           title="clear" data-clear>X</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4"></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="text-center">
                                                    <td class="text-dark  mb-1 fs-4">Jumat</td>
                                                    <td class="text-dark  mb-1 fs-6">
                                                        <div class="row">
                                                            <div class="col-sm-4"></div>
                                                            <div class="col-sm-4 jamJumat">
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <input class="form-control jamJumat2"
                                                                               type="text" placeholder="HH:MM"
                                                                               name="jamJumat" data-input>
                                                                    </div>
                                                                    <div
                                                                        class="text-start col-sm-6 input-group-append">
                                                                        <a class=" btn btn-outline-secondary btn-danger input-button"
                                                                           title="clear" data-clear>X</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4"></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="text-center">
                                                    <td class="text-dark  mb-1 fs-4">Sabtu</td>
                                                    <td class="text-dark  mb-1 fs-6">
                                                        <div class="row">
                                                            <div class="col-sm-4"></div>
                                                            <div class="col-sm-4 jamSabtu">
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <input class="form-control jamSabtu2"
                                                                               type="text" placeholder="HH:MM"
                                                                               name="jamSabtu" data-input>
                                                                    </div>
                                                                    <div
                                                                        class="text-start col-sm-6 input-group-append">
                                                                        <a class=" btn btn-outline-secondary btn-danger input-button"
                                                                           title="clear" data-clear>X</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4"></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="text-center">
                                                    <td class="text-dark  mb-1 fs-4">Minggu</td>
                                                    <td class="text-dark  mb-1 fs-6">
                                                        <div class="row">
                                                            <div class="col-sm-4"></div>
                                                            <div class="col-sm-4 jamMinggu">
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <input class="form-control jamMinggu2"
                                                                               type="text" placeholder="HH:MM"
                                                                               name="jamMinggu" data-input>
                                                                    </div>
                                                                    <div
                                                                        class="text-start col-sm-6 input-group-append">
                                                                        <a class=" btn btn-outline-secondary btn-danger input-button"
                                                                           title="clear" data-clear>X</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4"></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>

                                    <!--begin::Step 3-->
                                    <div class="flex-column" data-kt-stepper-element="content">

                                        <div class="row mb-3">
                                            <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Pernyataan Tidak
                                                Menuntut UPKP/DFA</label>
                                            <div class="col-lg-7">
                                                {{-- <input id="file_tidak_menuntut_upkp" type="file" name="file_tidak_menuntut_upkp" class="form-control" required> --}}
                                                <input id="file_tidak_menuntut_upkp" type="file"
                                                       name="file_tidak_menuntut_upkp" class="form-control">
                                            </div>
                                            <div class="col-lg">
                                                <button id="btn_file_tidak_menuntut_upkp" class="btn btn-info btn-icon">
                                                    <i
                                                        class="fa fa-upload"></i></button>
                                            </div>
                                        </div>
                                        {{-- <div class="row mb-3">
                                            <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen Persetujuan
                                                Pencantuman Gelar</label>
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <input type="file" name="lamaPendidikan"
                                                        class="form-control single_number_mask" placeholder="hanya angka"
                                                        value="" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Pernyataan tidak
                                                Menuntut UPKP/DFA</label>
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <input type="file" name="lamaPendidikan"
                                                        class="form-control single_number_mask" placeholder="hanya angka"
                                                        value="" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Pernyataan Prodi
                                                Sesuai Kebutuhan Organisasi</label>
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <input type="file" name="lamaPendidikan"
                                                        class="form-control single_number_mask" placeholder="hanya angka"
                                                        value="" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Keterangan tidak
                                                sedang menjalani Skorsing</label>
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <input type="file" name="lamaPendidikan"
                                                        class="form-control single_number_mask" placeholder="hanya angka"
                                                        value="" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Keterangan tidak
                                                sedang menjalani Hukdis sedang/berat/sedang dalam proses pemeriksaan</label>
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <input type="file" name="lamaPendidikan"
                                                        class="form-control single_number_mask" placeholder="hanya angka"
                                                        value="" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Keterangan tidak
                                                sedang menjalani izin belajar dengan jenjang pendidikan dan program studi
                                                yang sama</label>
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <input type="file" name="lamaPendidikan"
                                                        class="form-control single_number_mask" placeholder="hanya angka"
                                                        value="" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Keterangan jarak
                                                lokasi perkuliahan dari tempat kerja (non PJJ)</label>
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <input type="file" name="lamaPendidikan"
                                                        class="form-control single_number_mask" placeholder="hanya angka"
                                                        value="" required>
                                                </div>
                                            </div>
                                        </div> --}}
                                        {{-- <form id="uploadkhs" method="put" action="" enctype="multipart/form-data">
                                            {{ method_field('PUT') }}
                                            <div class="row mb-3">
                                                <label class="col-lg-3 col-form-label fw-bold fs-6">Unggah File 1</label>
                                                <div class="col-lg">
                                                    <input id="uidlps" type="hidden" name="">
                                                    <input id="filekhs_lama" type="hidden" name="">
                                                    <input id="filekhs" class="form-control" type="file" name="filekhs">
                                                    <span id="messageuploadkhs" class="form-text text-muted" style="display: none">File berhasil diupload.</span>
                                                    <div id="messagefilekhs" class="col-lg" style="display: none">
                                                        Download file KHS <a href="#" target="_blank" id="urlfilekhs">disini</a>.
                                                    </div>
                                                </div>
                                                <div class="form-label col-lg-2">
                                                    <button id="btnUploadKhs" type="submit" class="btn btn-primary fs-6">
                                                        <i class="fa fa-upload"></i> Upload
                                                    </button>
                                                    <button class="btn btn-primary btn-sm fs-6" type="button" hidden disabled>
                                                        Loading
                                                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                    </button>
                                                </div>
                                            </div>
                                        </form> --}}
                                    </div>

                                    <!--begin::Step 4-->
                                    <div class="flex-column" data-kt-stepper-element="content">
                                        <!--begin::Input group-->
                                        <div class="fv-row mb-10">
                                            <!--begin::Label-->
                                            <label class="form-label d-flex align-items-center">
                                                <span class="required">Input 1</span>
                                                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip"
                                                   title="Example tooltip"></i>
                                            </label>
                                            <!--end::Label-->

                                            <!--begin::Input-->
                                            <input type="text" class="form-control form-control-solid" name="input1"
                                                   placeholder="" value=""/>
                                            <!--end::Input-->
                                        </div>
                                        <!--end::Input group-->

                                        <!--begin::Input group-->
                                        <div class="fv-row mb-10">
                                            <!--begin::Label-->
                                            <label class="form-label">
                                                Input 2
                                            </label>
                                            <!--end::Label-->

                                            <!--begin::Input-->
                                            <input type="text" class="form-control form-control-solid" name="input2"
                                                   placeholder="" value=""/>
                                            <!--end::Input-->
                                        </div>
                                        <!--end::Input group-->

                                        <!--begin::Input group-->
                                        <div class="fv-row mb-10">
                                            <!--begin::Label-->
                                            <label class="form-label">
                                                Input 3
                                            </label>
                                            <!--end::Label-->

                                            <!--begin::Input-->
                                            <input type="text" class="form-control form-control-solid" name="input3"
                                                   placeholder="" value=""/>
                                            <!--end::Input-->
                                        </div>
                                        <!--end::Input group-->
                                    </div>
                                </div>
                                <!--end::Group-->

                                <!--begin::Actions-->
                                <div class="d-flex flex-stack">
                                    <!--begin::Wrapper-->
                                    <div class="me-2">
                                        <button type="button" class="btn btn-light btn-active-light-primary"
                                                data-kt-stepper-action="previous">
                                            Back
                                        </button>
                                    </div>
                                    <!--begin::Wrapper-->
                                    <div>
                                        <button type="button" class="btn btn-primary" data-kt-stepper-action="submit">
                                            <span class="indicator-label" form="kt_stepper_example_basic_form">
                                                Submit
                                            </span>
                                            <span class="indicator-progress">
                                                Please wait... <span
                                                    class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                            </span>
                                        </button>

                                        <button type="button" class="btn btn-primary" data-kt-stepper-action="next">
                                            Continue
                                        </button>
                                    </div>
                                    <!--end::Wrapper-->
                                </div>
                                <!--end::Actions-->
                            </form>
                            <!--end::Form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end::Stepper-->
@endsection

@section('js')
    <script>
        // Stepper lement
        var element = document.querySelector("#kt_stepper_example_basic");

        // stepper submit element
        var r = element.querySelector('[data-kt-stepper-action="submit"]');

        // Initialize Stepper
        var stepper = new KTStepper(element);

        // Handle next step
        stepper.on("kt.stepper.next", function (stepper) {

            // go next step
            stepper.goNext();
        });

        // Handle previous step
        stepper.on("kt.stepper.previous", function (stepper) {
            stepper.goPrevious(); // go previous step
        });

        // submitFormElement
        var formSub = document.querySelector('#kt_stepper_example_basic_form');

        r.addEventListener("click", (function (e) {
            var senin = $('.jamSenin2').val();
            let senin2 = senin;
            let senin3 = senin2.slice(0, 2);

            var selasa = $('.jamSelasa2').val();
            let selasa2 = selasa;
            let selasa3 = selasa2.slice(0, 2);

            var rabu = $('.jamRabu2').val();
            let rabu2 = rabu;
            let rabu3 = rabu2.slice(0, 2);

            var kamis = $('.jamKamis2').val();
            let kamis2 = kamis;
            let kamis3 = kamis2.slice(0, 2);

            var jumat = $('.jamJumat2').val();
            let jumat2 = jumat;
            let jumat3 = jumat2.slice(0, 2);

            var sabtu = $('.jamSabtu2').val();
            let sabtu2 = sabtu;
            let sabtu3 = sabtu2.slice(0, 2);

            var minggu = $('.jamMinggu2').val();
            let minggu2 = minggu;
            let minggu3 = minggu2.slice(0, 2);

            let x = senin3 + selasa3 + rabu3 + kamis3 + jumat3;
            let z = sabtu3 > 0 && minggu3 > 0 && x >= 1717;
            if ((x >= 171717) || (z == true)) {
                console.log(z);
                r.disabled = !0, r.setAttribute("data-kt-indicator", "on"), setTimeout((function () {
                    r.removeAttribute("data-kt-indicator"), r.disabled = !1, Swal.fire({
                        text: "Form has been successfully submitted!",
                        icon: "success",
                        buttonsStyling: !1,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    }).then((function () {
                        formSub.submit();
                    }))
                }), 2e3)
            } else {
                console.log(z);

                swalError('Perhatikan Kembali Jadwal Perkuliahan');
            }
        }));


        //kalender
        $(".datepick").flatpickr();

        // get kantor & jabatan
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // get kantor iam
            $("#kantorAsal").select2({

                placeholder: 'Pilih perguruan tinggi',
                minimumInputLength: 3,
                ajax: {
                    url: '{{ env('APP_URL') }}' + '/tubel/administrasi/get-kantor',
                    dataType: 'json',
                    type: 'post',
                    delay: 300,
                    data: function (params) {
                        return {
                            search: params.term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response

                        };
                    }
                }
            })

            // get jabatan iam
            $("#jabatanAsal").select2({

                placeholder: 'Pilih program studi',
                minimumInputLength: 3,
                ajax: {
                    url: '{{ env('APP_URL') }}' + '/tubel/administrasi/get-jabatan',
                    dataType: 'json',
                    type: 'post',
                    delay: 300,
                    data: function (params) {
                        return {
                            search: params.term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    }
                }
            })
            // get lokasi
            $("#lokasiKuliah").select2({

                placeholder: 'Pilih lokasi perkuliahan',
                minimumInputLength: 3,
                ajax: {
                    url: '{{ env('APP_URL') }}' + '/tubel/administrasi/get-jabatan',
                    dataType: 'json',
                    type: 'post',
                    delay: 300,
                    data: function (params) {
                        return {
                            search: params.term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    }
                }
            })
        });


        $(document).ready(function () {
            $(".pjjA input").on("change", function (e) {
                const isOn = e.currentTarget.checked;

                if (isOn) {
                    var cek1 = 1;
                    $(".pjjCheck").show();
                    document.getElementById('testNameHidden').disabled = true;
                } else {
                    $(".pjjCheck").hide();
                    document.getElementById('testNameHidden').disabled = false;
                }
            });
        });


        $(".jamSeninCoba").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            minTime: "17:00",
            wrap: true,
        });
        $(".jamSelasa").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            minTime: "17:00",
            wrap: true,
        });
        $(".jamRabu").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            minTime: "17:00",
            wrap: true,
        });
        $(".jamKamis").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            minTime: "17:00",
            wrap: true,
        });
        $(".jamJumat").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            minTime: "17:00",
            wrap: true,
        });
        $(".jamSabtu").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            wrap: true,
        });
        $(".jamMinggu").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            wrap: true,
        });

        $(document).on("click", "#btn_file_tidak_menuntut_upkp", function () {

            // console.log($("#file_tidak_menuntut_upkp").prop("files")[0])

            if ($("#file_tidak_menuntut_upkp").prop("files")[0] == undefined) {
                swalError('Tidak ada file yg diupload!')
            } else {
                var file_data = $("#file_tidak_menuntut_upkp").prop("files")[0]
                var form_data = new FormData()

                console.log(file_data);

                form_data.append("file_tidak_menuntut_upkp", file_data)
                form_data.append("pegawaiId", "{{ session()->get('user')['pegawaiId'] }}")

                $.ajax({
                    url: "{{ env('APP_URL') }}" + "/ibel/administrasi/upload",
                    dataType: 'script',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function (response) {
                        if (response == true) {
                            swalSuccess('File berhasil diupload!')
                        } else {
                            swalError("File gagal diupload!")
                        }
                    }


                });
            }
        });
    </script>
@endsection
