<div class="modal fade" id="logStatusModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Log Status Tiket</h3>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body scroll-y px-5 px-lg-10">
                <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                    <div class="row">
                        <label class="col-lg-2 col-form-label fw-bold fs-6">Nama</label>
                        <div class="col-lg-1 col-form-label fw-bold fs-6">:</div>
                        <label id="labelNama" class="col-lg col-form-label fw-bold fs-6"></label>
                    </div>
                    <div class="row">
                        <label class="col-lg-2 col-form-label fw-bold fs-6">Nomor Tiket</label>
                        <div class="col-lg-1 col-form-label fw-bold fs-6">:</div>
                        <label id="labelTiket" class="col-lg col-form-label fw-bold fs-6"></label>
                    </div>
                </div>
                <div class="text-center">
                    <span id="tiket-spinner" class="spinner-border spinner-border-md text-warning" role="status"
                          aria-hidden="true"></span>
                </div>
                <div class="table-responsive bg-gray-400 rounded border border-gray-300 log-table-induk">
                    <table class="table table-striped align-middle log-table">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
