<div class="col-lg-6 mb-4">
    <div class="text-start" data-kt-customer-table-toolbar="base">
        <button type="button" class="btn btn-sm btn-ligth btn-active-primary border border-gray-400 fs-6"
                data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" data-kt-menu-flip="top-end">
            <i class="fa fa-filter"></i> Advanced Search
        </button>
        <div class="menu menu-sub menu-sub-dropdown w-md border-warning mt-1" data-kt-menu="true">
            <div class="px-7 py-5">
                <div class="fs-4 text-dark fw-bolder">Filter Pencarian</div>
            </div>
            <div class="separator border-gray-200"></div>
            <form id="advancedSearch" action="" method="get">
                <div class="px-7 py-5">
                    <div class="col-lg mb-6">
                        <div class="row mb-4">
                            <label class="form-label fw-bold">Pegawai:</label>
                            <div class="input-group">
                                <input class="form-control form-select-solid" type="text" name="nama"
                                       placeholder="nama pegawai"
                                       value="{{ isset($_GET['nama']) ? $_GET['nama'] : '' }}">
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="form-label fs-5 fw-bold">NIP</label>
                            <div class="input-group">
                                <input class="form-control form-select-solid" type="text" name="nip"
                                       placeholder="nip 18 digit" value="{{ isset($_GET['nip']) ? $_GET['nip'] : '' }}">
                            </div>
                        </div>
                        <div class="row">
                            <label class="form-label fs-5 fw-bold">Tiket</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="tiket" placeholder="tiket"
                                       value="{{ isset($_GET['tiket']) ? $_GET['tiket'] : '' }}"/>

                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end">
                        <a href="{{ url()->current() }}" class="btn btn-light btn-active-light-primary me-2">Reset</a>
                        <button type="submit" class="btn btn-sm btn-primary" data-kt-menu-dismiss="true"
                                data-kt-customer-table-filter="filter">Search
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
