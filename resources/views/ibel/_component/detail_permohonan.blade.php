<div class="card-body">
    <div class="card px-16">
        <div class="col-lg row mb-3">
            <div class="col">
                <a href="{{ url()->previous() }}" class="btn btn-sm btn-secondary mb-3"><i
                        class="fa fa-reply"></i> Kembali</a>
            </div>
        </div>
        <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-8">
            <div class="m-6">
                <div class="row">
                    <div class="fs-6 fw-bolder">
                        <a href="javascript:void(0)" class="text-primary fw-bold btnTiket"
                           data-tiket="{{ $ibel['logStatusProbis']->nomorTiket }}"
                           data-nama="{{ $ibel['namaPegawai'] }}">
                            {{ $ibel['logStatusProbis']->nomorTiket }}
                        </a>
                    </div>
                </div>
                <div class="fs-1 fw-bolder mt-2 mb-2">
                    {{ $ibel['namaPegawai'] }}
                </div>
                <div class="fw-normal fs-6">
                    {{ $ibel['nip18'] }}
                    <br>
                    {{ $ibel['namaPangkat'] }}
                    <br>
                    {{ $ibel['namaJabatan'] }}
                    <br>
                    {{ $ibel['namaKantor'] }}
                    <br>
                </div>
            </div>
        </div>
        <div class="rounded border border-gray-300 p-4 d-flex flex-column">
            <div class="card mt-4">
                <div class="row mb-6">
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg">
                        <div class="row">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Jenjang Pendidikan</label>
                            <div class="col-lg">
                                <input type="text" class="form-control"
                                       value="{{ $ibel['jenjangPendidikan']->jenjangPendidikan }}" disabled/>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg">
                        <div class="row">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Jenjang pada Persetujuan
                                Pencantuman
                                Gelar</label>
                            <div class="col-lg">
                                <input id="jenjang_pendidikan_persetujuan" type="text" class="form-control"
                                       value="{{ $ibel['jenjangPendidikanPgId']->jenjangPendidikan ?? '-' }}" disabled/>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg">
                        <div class="row">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen Persetujuan
                                Pencantuman
                                Gelar</label>
                            <div class="col-lg">
                                <div class="d-flex flex-aligns-center mt-2 pe-10">
                                    <img alt="" class="w-25px me-3"
                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                    <div class="ms-1">
                                        <a href="{{ env('APP_URL') . '/ibel/files/' . $ibel['pathPencantumanGelar'] }}"
                                           target="_blank" class="fs-6 text-hover-primary">
                                            SK Persetujuan Pencantuman Gelar
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
                <div class="row mb-6">
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg">
                        <div class="row">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Periode Pendaftaran</label>
                            <div class="col-lg">
                                <div class="input-group">
                                    <div class="position-relative d-flex align-items-center col">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                         width="24px" height="24px" viewBox="0 0 24 24"
                                                         version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none"
                                                           fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24">
                                                            </rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                  width="4" height="4" rx="1">
                                                            </rect>
                                                            <path
                                                                d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12 flatpickr-input" id="datemonth"
                                               type="text"
                                               value="{{ AppHelper::instance()->indonesian_date($ibel['periodePendaftaranAwal'], 'j F Y', '') }}"
                                               disabled>
                                    </div>
                                    <div class="input-group-append">
                                        <span class="input-group-text text-gray-500">s.d.</span>
                                    </div>
                                    <div class="position-relative d-flex align-items-center col">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                         width="24px" height="24px" viewBox="0 0 24 24"
                                                         version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none"
                                                           fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24">
                                                            </rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                  width="4" height="4" rx="1">
                                                            </rect>
                                                            <path
                                                                d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12 flatpickr-input" id="datemonth"
                                               type="text"
                                               value="{{ AppHelper::instance()->indonesian_date($ibel['periodePendaftaranAkhir'], 'j F Y', '') }}"
                                               disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
                <div class="row mb-6">
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg">
                        <div class="row">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Pembelajaran Jarak
                                Jauh</label>
                            <div class="col-lg">
                                <input class="form-control-plaintext" type="text"
                                       value="{{ $ibel['flagPjj'] == 1 ? 'Ya' : 'Tidak' }}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
                <div class="row mb-6">
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg">
                        <div class="row">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Penetapan Prodi PJJ</label>
                            <div class="col-lg">
                                @if ($ibel['flagPjj'] == 1)

                                    <div class="d-flex flex-aligns-center mt-2 pe-10">
                                        <img alt="" class="w-25px me-3"
                                             src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1">
                                            <a href="{{ env('APP_URL') . '/ibel/files/' . $ibel['pathPenetapanPjj'] }}"
                                               target="_blank" class="fs-6 text-hover-primary">
                                                Penetapan Program Studi Pembelajaran Jarak Jauh
                                            </a>
                                        </div>
                                    </div>

                                @else
                                    <input class="form-control-plaintext" type="text" value="-" disabled>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
                <div class="row mb-6">
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg">
                        <div class="row">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Perguruan Tinggi</label>
                            <div class="col-lg">
                                <input type="text" class="form-control"
                                       value="{{ $ibel['ptProdiAkreditasi']->perguruanTinggiIbelId->namaPerguruanTinggi }}"
                                       disabled/>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
                <div class="row mb-6">
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg">
                        <div class="row">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Program Studi</label>
                            <div class="col-lg">
                                <input type="text" class="form-control"
                                       value="{{ $ibel['ptProdiAkreditasi']->programStudiIbelId->namaProgramStudi }}"
                                       disabled/>
                                <div class="col-lg fs-6 fw-normal mt-2 ps-2">
                                    Akreditasi: {{ $ibel['ptProdiAkreditasi']->akreditasiId->akreditasi }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
                <div class="row mb-6">
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg">
                        <div class="row">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Lokasi Perkuliahan</label>
                            <div class="col-lg">
                                <input type="text" class="form-control"
                                       value="{{ $ibel['rlokasiPerkuliahan']->namaKampus }}" disabled/>
                                <div class="col-lg fs-6 fw-normal mt-2 ps-2">
                                    Alamat: {{ $ibel['rlokasiPerkuliahan']->alamat }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg">
                        <div class="row">
                            <div class="rounded p-2 d-flex flex-column">
                                <div class="col-lg">
                                    <div class="fw-bold fs-5">
                                        Dokumen Persyaratan
                                    </div>
                                </div>
                                <div class="col-lg my-2">
                                    <table
                                        class="table table-rounded fs-6 table-row-bordered border g3-2 gs-2">
                                        <tbody>
                                        <tr>
                                            <td class="align-middle">
                                                <div class="d-flex flex-aligns-center pe-10">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1">
                                                        <a href="{{ env('APP_URL') . '/ibel/files/' . $ibel['pathTidakUpkp'] }}"
                                                           target="_blank"
                                                           class="fs-6 text-hover-primary">
                                                            S. Pernyataan tidak menuntut UPKP/DFA
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="align-middle">
                                                <div class="d-flex flex-aligns-center pe-10">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1">
                                                        <a href="{{ env('APP_URL') . '/ibel/files/' . $ibel['pathBrosur'] }}"
                                                           target="_blank"
                                                           class="fs-6 text-hover-primary">
                                                            Brosur penerimaan mahasiswa baru
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="align-middle">
                                                <div class="d-flex flex-aligns-center pe-10">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1">
                                                        <a href="{{ env('APP_URL') . '/ibel/files/' . $ibel['pathKebutuhanProdi'] }}"
                                                           target="_blank"
                                                           class="fs-6 text-hover-primary">
                                                            S. Pernyataan prodi sesuai kebutuhan
                                                            organisasi
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="align-middle">
                                                <div class="d-flex flex-aligns-center pe-10">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1">
                                                        <a href="{{ env('APP_URL') . '/ibel/files/' . $ibel['pathAkreditasi'] }}"
                                                           target="_blank"
                                                           class="fs-6 text-hover-primary">
                                                            Keputusan akreditasi
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="align-middle">
                                                <div class="d-flex flex-aligns-center pe-10">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1">
                                                        <a href="{{ env('APP_URL') . '/ibel/files/' . $ibel['pathSkorsing'] }}"
                                                           target="_blank"
                                                           class="fs-6 text-hover-primary">
                                                            S. Ket. tidak sedang menjalani Skorsing
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="align-middle">
                                                <div class="d-flex flex-aligns-center pe-10">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1">
                                                        <a href="{{ env('APP_URL') . '/ibel/files/' . $ibel['pathIjazah'] }}"
                                                           target="_blank"
                                                           class="fs-6 text-hover-primary">
                                                            Ijazah yang diakui
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="align-middle">
                                                <div class="d-flex flex-aligns-center pe-10">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1">
                                                        <a href="{{ env('APP_URL') . '/ibel/files/' . $ibel['pathHukdis'] }}"
                                                           target="_blank"
                                                           class="fs-6 text-hover-primary">
                                                            S. Ket. tidak sedang menjalani
                                                            Hukdis/Pemeriksaan
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="align-middle">
                                                <div class="d-flex flex-aligns-center pe-10">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1">
                                                        <a href="{{ env('APP_URL') . '/ibel/files/' . $ibel['pathTranskrip'] }}"
                                                           target="_blank"
                                                           class="fs-6 text-hover-primary">
                                                            Transkrip Nilai
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="align-middle">
                                                <div class="d-flex flex-aligns-center pe-10">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1">
                                                        <a href="{{ env('APP_URL') . '/ibel/files/' . $ibel['pathJarak'] }}"
                                                           target="_blank"
                                                           class="fs-6 text-hover-primary">
                                                            S. Ket. jarak lokasi perkuliahan
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="align-middle">
                                                <div class="d-flex flex-aligns-center pe-10">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1">
                                                        <a href="{{ env('APP_URL') . '/ibel/files/' . $ibel['pathSehat'] }}"
                                                           target="_blank"
                                                           class="fs-6 text-hover-primary">
                                                            Surat Keterangan Berbadan Sehat
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="align-middle">
                                                <div class="d-flex flex-aligns-center pe-10">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1">
                                                        <a href="{{ env('APP_URL') . '/ibel/files/' . $ibel['pathJadwal'] }}"
                                                           target="_blank"
                                                           class="fs-6 text-hover-primary">
                                                            S. Ket. waktu pendidikan diluar jam kerja
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="align-middle">
                                                <div class="d-flex flex-aligns-center pe-10">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1">
                                                        <a href="{{ env('APP_URL') . '/ibel/files/' . $ibel['pathCpns'] }}"
                                                           target="_blank"
                                                           class="fs-6 text-hover-primary">
                                                            SK CPNS
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="align-middle">
                                            </td>
                                            <td class="align-middle">
                                                <div class="d-flex flex-aligns-center pe-10">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1">
                                                        <a href="{{ env('APP_URL') . '/ibel/files/' . $ibel['pathPns'] }}"
                                                           target="_blank"
                                                           class="fs-6 text-hover-primary">
                                                            SK PNS
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="align-middle">
                                            </td>
                                            <td class="align-middle">
                                                <div class="d-flex flex-aligns-center pe-10">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1">
                                                        <a href="{{ env('APP_URL') . '/ibel/files/' . $ibel['pathPangkat'] }}"
                                                           target="_blank"
                                                           class="fs-6 text-hover-primary">
                                                            SK Pangkat Terakhir
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="align-middle">
                                            </td>
                                            <td class="align-middle">
                                                <div class="d-flex flex-aligns-center pe-10">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1">
                                                        <a href="{{ env('APP_URL') . '/ibel/files/' . $ibel['pathPpkpns1'] }}"
                                                           target="_blank"
                                                           class="fs-6 text-hover-primary">
                                                            PPKPNS (1)
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="align-middle">
                                            </td>
                                            <td class="align-middle">
                                                <div class="d-flex flex-aligns-center pe-10">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1">
                                                        <a href="{{ env('APP_URL') . '/ibel/files/' . $ibel['path_ppkpns2'] }}"
                                                           target="_blank"
                                                           class="fs-6 text-hover-primary">
                                                            PPKPNS (2)
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg">
                        <div class="row">
                            <div class="d-flex flex-column mb-6">
                                <div class="col-lg">
                                    <div class="fw-bold fs-5">
                                        Jadwal Perkuliahan
                                    </div>
                                </div>
                                <div class="col-lg my-2">
                                    <table class="table table-striped border border border-gray-300">
                                        <thead>
                                        <tr class="fw-bolder bg-secondary align-middle">
                                            <th class="text-center col-lg-4">Hari</th>
                                            <th class="text-center col-lg-8">Jam Perkuliahan</th>
                                        </tr>
                                        </thead>
                                        <tbody class="fs-6">

                                        @foreach ($ibel_jadwal as $ij)
                                            <tr>
                                                <td class="text-center align-middle">
                                                    {{
                                                    AppHelper::instance()->number_to_indonesian_date($ij->hari)
                                                    }}
                                                </td>
                                                <td class="text-center row">
                                                    <div class="col-lg"></div>
                                                    <div class="text-center col-lg-3">
                                                        <input type="text" class="form-control text-center"
                                                               value="{{ $ij->jamMulai ?? " -" }}" disabled="">
                                                    </div>
                                                    <div class="col-lg"></div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
