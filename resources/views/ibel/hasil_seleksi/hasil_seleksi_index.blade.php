@extends('layout.master')

@section('content')

    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card border-warning">
                    <div class="card-header card-header-stretch" id="card-header">
                        <div class="card-toolbar">
                            <ul class="nav nav-tabs nav-line-tabs nav-stretch border-transparent fs-5 fw-bolder"
                                id="tabLps">
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'perekaman' ? 'active' : '' }}"
                                       href="{{ url('ibel/hasil-seleksi/perekaman') }}"> Perekaman Hasil Seleksi </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'menunggu-persetujuan' ? 'active' : '' }}"
                                       href="{{ url('ibel/hasil-seleksi/menunggu-persetujuan') }}"> Menunggu Persetujuan
                                        Tim Seleksi </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @include('ibel._component.advance_search')
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'perekaman' ? 'active show' : '' }}" role="tabpanel">
                                @if ($tab == 'perekaman')
                                    @include('ibel.hasil_seleksi.hasil_seleksi_tabel')
                                @endif
                            </div>

                            <div class="tab-pane fade {{ $tab == 'menunggu-persetujuan' ? 'active show' : '' }}"
                                 role="tabpanel">
                                @if ($tab == 'menunggu-persetujuan')
                                    @include('ibel.hasil_seleksi.hasil_seleksi_tabel')
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('ibel.modal_tiket')
@endsection

@section('js')
    <script src="{{ asset('assets/js/ibel') }}/hasil-seleksi.js"></script>
    <script src="{{ asset('assets/js/ibel') }}/log-tiket.js"></script>
@endsection
