@extends('layout.master')

@section('content')

    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="card border-warning">
                <div class="card-header" id="card-header">
                    <h3 class="card-title">
                        <span class="fw-bold fs-4 text-white">Pemeriksaan Permohonan</span>
                    </h3>
                </div>
                @include('ibel._component.detail_permohonan')
                @if ($action == true)
                    <div class="card-body pt-0">
                        <div class="card px-16">
                            <div class="rounded border border-gray-300 p-4 d-flex flex-column">
                                <form method="post" action="">
                                    @csrf
                                    <input type="hidden" name="id_permohonan" value="{{ $ibel['id'] }}">
                                    <div class="card mb-6">
                                        <div class="row mb-3">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg">
                                                <div class="form-group">
                                                    <label class="fw-bold fs-3 mb-2">Keputusan Tim Seleksi</label>
                                                    <div class="radio-inline mt-2 radioKeputusan fs-5">
                                                        <label class="form-check-label pe-12">
                                                            <input class="form-check-input" type="radio"
                                                                   name="keputusan"
                                                                   value="1">
                                                            <span></span>Diterima</label>
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="radio"
                                                                   name="keputusan"
                                                                   value="0">
                                                            <span></span>Ditolak</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3"></div>
                                        </div>
                                        <div id="jangka_waktu" class="row mt-6 mb-3" hidden>
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg">
                                                <div class="form-group">
                                                    <label class="fw-bold fs-3 mb-2">Jangka Waktu Izin Belajar</label>
                                                    <div class="col-lg">
                                                        <input type="text"
                                                               class="fw-bold fs-6 form-control mt-2 bulan_mask"
                                                               placeholder="Tahun" name="tahun">
                                                    </div>
                                                    <div class="col-lg">
                                                        <input type="text"
                                                               class="fw-bold fs-6 form-control mt-2 bulan_mask"
                                                               placeholder="Bulan" name="bulan">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4"></div>
                                        </div>
                                        <div id="alasan_penolakan" class="row mt-6 mb-3" hidden>
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg">
                                                <div class="form-group">
                                                    <label class="fw-bold fs-3 mb-2">Alasan Penolakan</label>
                                                    <div class="col-lg">
                                                <textarea type="text" class="fw-bold fs-6 form-control mt-2" rows="4"
                                                          placeholder="isi alasan penolakan"
                                                          name="alasan_penolakan"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4"></div>
                                        </div>
                                        <div id="card_tim_seleksi" class="row mt-6 mb-3" hidden>
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg">
                                                <div class="form-group">
                                                    <label class="fw-bold fs-3 mb-2">Tim Seleksi</label>
                                                    <div class="col-lg">
                                                        <select id="tim_seleksi" name="tim_seleksi"
                                                                data-control="select2"
                                                                type="text" class="form-select form-select-lg"
                                                                data-placeholder="Pilih tim seleksi" required>
                                                            <option></option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4"></div>
                                        </div>
                                        <div id="buttonAction" class="row mt-12" hidden>
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg text-center">
                                                <a href="{{ env(" APP_URL") . '/ibel/periksa/' }}" type="button"
                                                   class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                                        class="fa fa-reply"></i>Batal</a>
                                                <button type="submit" class="btn btn-sm btn-success"><i
                                                        class="fa fa-check-circle"></i>Simpan</a>
                                            </div>
                                            <div class="col-lg-4"></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

    @include('ibel.modal_tiket')

@endsection

@section('js')
    <script src="{{ asset('assets/js/ibel') }}/hasil-seleksi.js"></script>
    <script src="{{ asset('assets/js/ibel') }}/log-tiket.js"></script>
@endsection
