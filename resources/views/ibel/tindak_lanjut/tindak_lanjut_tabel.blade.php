<div class="table-responsive bg-gray-400 rounded border border-gray-300 shadow-sm">
    <table class="table table-striped align-middle" id="tabel_tubel">
        <thead class="fw-bolder bg-secondary fs-6">
        <tr class="text-center align-middle">
            <th class="text-center w-50px ps-2">No</th>
            <th class="text-center" style="width: 65%">Pemohon</th>
            <th class="text-center w-300px">Dokumen</th>
            <th class="text-center pe-4">Fungsi</th>
        </tr>
        </thead>
        <tbody>

        @if (!empty($ibel['data']))
            @php
                $nomor = 1;
            @endphp

            @foreach ($ibel['data'] as $key => $i)

                <tr id="{{ $key }}" class="text-dark fs-6 col-lg">
                    <td class="text-center ps-2 align-top">
                        {{ $nomor++ }}
                    </td>

                    <td>
                        <div class="row">
                            <div class="col">
                                <a href="javascript:void(0)" class="text-primary fw-bold btnTiket"
                                   data-tiket="{{ $i['ibel']->logStatusProbis->nomorTiket }}"
                                   data-nama="{{ $i['ibel']->namaPegawai }}">
                                    {{ $i['ibel']->logStatusProbis->nomorTiket }}
                                </a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-3">
                                <div class="fw-bolder mt-3">
                                    {{ $i['ibel']->namaPegawai }}
                                </div>
                                <div class="fw-normal">
                                    {{ $i['ibel']->nip18 }}
                                    <br>
                                    {{ $i['ibel']->namaKantor }}
                                </div>

                                @if ($i['kep_seleksi']->flagDiterima == 1)
                                    <div class="mt-4">
                                <span class="text-success fw-bolder fs-1">
                                    DITERIMA
                                </span>
                                    </div>
                                @elseif($i['kep_seleksi']->flagDiterima == 0)
                                    <div class="mt-4">
                                <span class="text-danger fw-bolder fs-1">
                                    DITOLAK
                                </span>
                                    </div>
                                @endif

                            </div>

                            <div class="col-lg">
                                <div class="fw-normal mt-3">
                                    Jenjang Pendidikan : {{ $i['ibel']->jenjangPendidikan->jenjangPendidikan }}
                                    <br>
                                    Program Studi
                                    : {{ $i['ibel']->ptProdiAkreditasi->programStudiIbelId->namaProgramStudi }}
                                    <br>
                                    Perkuliahan : {{ $i['ibel']->flagPjj == 1 ? 'PJJ' : 'Reguler' }}
                                    <br>
                                    <br>
                                    Perguruan Tinggi : {{ $i['ibel']->perguruanTinggiIbel->namaPerguruanTinggi }}
                                    <br>
                                    Lokasi : {{ $i['ibel']->perguruanTinggiIbel->alamat }}
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div></div>
                                <div class="fw-normal mt-3">
                                    <div class="fw-bolder fs-6">
                                        Jadwal Perkuliahan
                                    </div>
                                    <div>
                                        <table class="table align-middle gy-0">
                                            @if (!empty($i['jadwal']))
                                                @foreach ($i['jadwal'] as $j)
                                                    @if (!empty($j->jamMulai))
                                                        <tr>
                                                            <td style="width: 50px">
                                                                {{ AppHelper::instance()->number_to_indonesian_date($j->hari) }}
                                                            </td>
                                                            <td class="min-w-100px">
                                                                {{ $j->jamMulai }}
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>

                    <td class="text-center align-top px-0">

                        {{-- flag diterima --}}

                        @if ($i['kep_seleksi']->flagDiterima == 1)

                            <div class="row fs-6">
                                <div class="col-lg">
                                    <div class="d-flex flex-aligns-center mt-2 p-0">
                                        <i class="fas fa-file-pdf fa-2x text-danger"></i>
                                        <div class="ms-3">
                                            <a href="{{ url('ibel/files/' . $i['kep_seleksi']->pathKeputusanSeleksi) }}"
                                               target="_blank" class="fs-6 text-hover-primary">
                                                Surat KEP Seleksi
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row fs-6">
                                <div class="row">
                                    <div class="d-flex flex-aligns-center mt-2">
                                        <i class="fas fa-file-pdf fa-2x text-danger"></i>
                                        <div class="ms-3">
                                            <a href="{{ url('ibel/files/') }} {{ $i['surat_izin'] ? $i['surat_izin']->pathDs : '' }}"
                                               target="_blank"
                                               class="fs-6 text-hover-primary">
                                                S. Izin Pendidikan diluar Kedinasan
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row fs-6">
                                <div class="col-lg">
                                    <div class="d-flex flex-aligns-center mt-2">
                                        <i class="far fa-file-word fa-2x text-primary"></i>
                                        <div class="ms-3">
                                            <a href="{{ url('ibel/tindak-lanjut/' . $key . '/nd_penyampaian') }}"
                                               class="fs-6 text-hover-primary">
                                                Template ND Penyampaian
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- end flag diterima --}}

                            {{-- flag ditolak --}}
                        @elseif($i['kep_seleksi']->flagDiterima == 0)

                            <div class="row fs-6">
                                <div class="col-lg">
                                    <div class="d-flex flex-aligns-center mt-2">
                                        <i class="far fa-file-word fa-2x text-primary"></i>
                                        <div class="ms-3">
                                            <a href="{{ url('ibel/tindak-lanjut/' . $key . '/template_surat_penolakan') }}"
                                               class="fs-6 text-hover-primary">
                                                Template Surat Penolakan
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row fs-6">
                                <div class="col-lg">
                                    <div class="d-flex flex-aligns-center mt-2">
                                        <i class="far fa-file-word fa-2x text-primary"></i>
                                        <div class="ms-3">
                                            <a href="{{ url('ibel/tindak-lanjut/' . $key . '/template_nd_pemberitahuan_penolakan') }}"
                                               class="fs-6 text-hover-primary">
                                                Template ND Pemb. Penolakan
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- flag ditolak, sudah upload file penolakan --}}

                            @if (!empty($i['surat_penolakan']->pathSuratPenolakan))
                                <div class="row fs-6">
                                    <div class="col-lg">
                                        <div class="d-flex flex-aligns-center mt-2">
                                            <i class="fas fa-file-pdf fa-2x text-danger"></i>
                                            <div class="ms-3">
                                                <a href="{{ url('ibel/files/' . $i['surat_penolakan']->pathSuratPenolakan) }}"
                                                   target="_blank"
                                                   class="fs-6 text-hover-primary">
                                                    Surat Penolakan
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if (!empty($i['surat_penolakan']->pathSuratPemberitahuan))
                                <div class="row fs-6">
                                    <div class="col-lg">
                                        <div class="d-flex flex-aligns-center mt-2">
                                            <i class="fas fa-file-pdf fa-2x text-danger"></i>
                                            <div class="ms-3">
                                                <a href="{{ url('ibel/files/' . $i['surat_penolakan']->pathSuratPemberitahuan) }}"
                                                   target="_blank"
                                                   class="fs-6 text-hover-primary">
                                                    ND Pemberitahuan Penolakan
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            {{-- end flag ditolak, sudah upload file penolakan --}}

                        @endif

                        {{-- end flag ditolak --}}

                    </td>

                    <td class="align-top mt-4 pe-2 p-0">
                        <div class="text-center mt-3">

                            <a href="{{ url('') . '/ibel/tindak-lanjut/lihat/' . $i['ibel']->id}}"
                               class=" btn btn-sm btn-icon btn-info" data-bs-toggle="tooltip" data-bs-placement="top"
                               data-bs-original-title="Lihat Detil">
                                <i class="fa fa-eye"></i>
                            </a>

                            @if ($i['kep_seleksi']->flagDiterima == 0 && !empty($i['ibel']->logStatusProbis->caseIdSelanjutnya))

                                <button class="btn btn-sm btn-icon btn-success btnRekamTindakLanjut"
                                        data-bs-toggle="tooltip"
                                        data-bs-placement="top" data-bs-original-title="Upload File"
                                        data-id_permohonan="{{ $i['ibel']->id }}"
                                        @if (!empty($i['surat_penolakan']))
                                            data-no_surat_permohonan="{{ $i['surat_penolakan']->nomorSuratPenolakan }}"
                                        data-tgl_surat_permohonan="{{ $i['surat_penolakan']->tglSuratPenolakan }}"
                                        data-path_surat_permohonan="{{ $i['surat_penolakan']->pathSuratPenolakan }}"
                                        data-no_nd_pemberitahuan="{{ $i['surat_penolakan']->nomorSuratPemberitahuan }}"
                                        data-tgl_nd_pemberitahuan="{{ $i['surat_penolakan']->tglSuratPemberitahuan }}"
                                        data-path_nd_pemberitahuan="{{ $i['surat_penolakan']->pathSuratPemberitahuan }}"
                                    @endif
                                >
                                    <i class="fa fa-upload"></i>
                                </button>

                                <div class="text-center mt-2">
                                    <button class="btn btn-sm btn-primary btnLanjutkanProses"
                                            data-id_permohonan="{{ $i['ibel']->id }}">
                                        <i class="fa fa-arrow-right"></i> Lanjutkan Proses
                                    </button>
                                </div>

                            @endif

                        </div>

                    </td>
                </tr>

            @endforeach

        @else

            <tr class="text-center">
                <td class="text-dark  mb-1 fs-6" colspan="5">Tidak terdapat data</td>
            </tr>

        @endif

        </tbody>
    </table>

</div>

@if (!empty($ibel))
    <!--begin::pagination-->
    <div class="d-flex flex-stack flex-wrap pt-5 p-3">
        <div class="fs-6 fw-bold text-gray-700"></div>

        @php
            $disabled = '';
            $nextPage= '';
            $disablednext = '';
            $hidden = '';
            $pagination = '';

            // tombol pagination
            if ($ibel['totalItems'] == 0) {
            $pagination = 'none';
            }

            // tombol back
            if($ibel['currentPage'] == 1){
            $disabled = 'disabled';
            }

            // tombol next
            if($ibel['currentPage'] != $ibel['totalPages']) {
            $nextPage = $ibel['currentPage'] + 1;
            }

            // tombol pagination
            if($ibel['currentPage'] == $ibel['totalPages'] || $ibel['totalPages'] == 0){
            $disablednext = 'disabled';
            $hidden = 'hidden';
            }

        @endphp

            <!--begin::Pages-->
        <ul class="pagination" style="display: {{ $pagination }}">
            <li class="page-item disabled"><a href="#" class="page-link">Halaman {{ $ibel['currentPage'] }} dari {{
                $ibel['totalPages']
                }}</a></li>
            <li class="page-item previous {{ $disabled }}"><a
                    href="{{ url()->current() . '?page=' . ($ibel['currentPage'] - 1) }}" class="page-link"><i
                        class="previous"></i></a></li>
            <li class="page-item active"><a href="{{ url()->current() . '?page=' . $ibel['currentPage'] }}"
                                            class="page-link">{{
                $ibel['currentPage'] }}</a></li>
            <li class="page-item" {{ $hidden }}><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                   class="page-link">{{
                $nextPage }}</a></li>
            <li class="page-item next {{ $disablednext }}"><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                              class="page-link"><i class="next"></i></a></li>
        </ul>
        <!--end::Pages-->

    </div>
    <!--end::paging-->
@endif
