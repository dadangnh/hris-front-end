@extends('layout.master')

@section('content')

    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card border-warning">
                    <div class="card-header" id="card-header">
                        <h3 class="card-title">
                            <span class="fw-bold fs-4 text-white">Permintaan Dokumen</span>
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @include('ibel._component.advance_search')
                        </div>
                        <div class="tab-content">
                            @include('ibel.permintaan_dokumen.permintaan_dokumen_tabel')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('ibel.modal_tiket')

@endsection

@section('js')
    <script src="{{ asset('assets/js/ibel') }}/log-tiket.js"></script>
@endsection
