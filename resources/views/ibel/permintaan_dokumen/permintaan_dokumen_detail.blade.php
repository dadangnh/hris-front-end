@extends('layout.master')

@section('content')

    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="card border-warning">
                <div class="card-header" id="card-header">
                    <h3 class="card-title">
                        <span class="fw-bold fs-4 text-white">Pemeriksaan Permohonan</span>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="card mb-6 px-16">
                        <div class="col-lg row mb-3">
                            <div class="col-lg">
                                <a href="javascript: history.go(-1)" class="btn btn-sm btn-secondary mb-3"><i
                                        class="fa fa-reply"></i> Kembali</a>
                            </div>
                        </div>
                        <div class="col-lg row">
                            <div class="col-lg">
                                <div class="d-flex align-items-center rounded p-5 px-5 bg-light-warning">
                                <span class="svg-icon svg-icon-3x svg-icon-warning me-5">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px"
                                         viewBox="0 0 24 24" version="1.1">
                                        <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"></circle>
                                        <rect fill="#000000" x="11" y="10" width="2" height="7" rx="1"></rect>
                                        <rect fill="#000000" x="11" y="7" width="2" height="2" rx="1"></rect>
                                    </svg>
                                </span>
                                    <div class="text-gray-700 fw-bold fs-6">
                                        <h2>Download dokumen berikut dan proses melalui NADINE.</h2>
                                        <div> Pastikan bahwa surat keterangan yang akan diterbitkan telah sesuai dengan
                                            keadaan yang sebenarnya. Periksa status hukuman disiplin pegawai yang
                                            bersangkutan.
                                            <div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-10">
                            <div class="row mb-12">
                                <div class="col-lg"></div>
                                <div class="col-lg-1 text-center">
                                    <i class="far fa-file-word fa-6x" style="color: #007BFF"></i>
                                </div>
                                <div class="col-lg-8 fw-bold fs-1 align-middle">
                                    Surat Keterangan tidak sedang menjalani Skorsing
                                    <br>
                                    <a href="{{ url()->current() . '/file_skorsing' }}"
                                       class="btn btn-sm btn-light-primary">
                                        <i class="fa fa-download"></i> Unduh
                                    </a>
                                </div>
                                <div class="col-lg"></div>
                            </div>
                            <div class="row mb-12">
                                <div class="col-lg"></div>
                                <div class="col-lg-1 text-center">
                                    <i class="far fa-file-word fa-6x" style="color: #007BFF"></i>
                                </div>
                                <div class="col-lg-8 fw-bold fs-1 align-middle">
                                    Surat Keterangan tidak sedang menjalani Hukdis sedang / berat / sedang dalam proses
                                    pemeriksaan
                                    <br>
                                    <a href="{{ url()->current() . '/file_hukdis' }}"
                                       class="btn btn-sm btn-light-primary">
                                        <i class="fa fa-download"></i> Unduh
                                    </a>
                                </div>
                                <div class="col-lg"></div>
                            </div>
                            <div class="row mb-12">
                                <div class="col-lg"></div>
                                <div class="col-lg-1 text-center">
                                    <i class="far fa-file-word fa-6x" style="color: #007BFF"></i>
                                </div>
                                <div class="col-lg-8 fw-bold fs-1 align-middle">
                                    Surat Keterangan jarak lokasi perkuliahan dari tempat kerja (non PJJ)
                                    <br>
                                    <a href="{{ url()->current() . '/file_jarak' }}"
                                       class="btn btn-sm btn-light-primary">
                                        <i class="fa fa-download"></i> Unduh
                                    </a>
                                </div>
                                <div class="col-lg"></div>
                            </div>
                            <div class="row mb-12">
                                <div class="col-lg"></div>
                                <div class="col-lg-1 text-center">
                                    <i class="far fa-file-word fa-6x" style="color: #007BFF"></i>
                                </div>
                                <div class="col-lg-8 fw-bold fs-1 align-middle">
                                    Surat Keterangan waktu pendidikan di luar jam kerja
                                    <br>
                                    <a href="{{ url()->current() . '/file_waktu' }}"
                                       class="btn btn-sm btn-light-primary">
                                        <i class="fa fa-download"></i> Unduh
                                    </a>
                                </div>
                                <div class="col-lg"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

@endsection
