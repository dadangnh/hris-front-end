{{-- modal tambah --}}
<div class="modal fade" id="add_ref_dasar" aria-hidden="true">
    <div @if ($tab == 'referensi_otorisasi') class="modal-dialog modal-xl" @endif class="modal-dialog modal-lg">
        <div class="modal-content rounded">
            <div class="modal-header">
                <h4 class="text-white">Form Tambah Data Referensi</h4>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form" method="post" enctype="multipart/form-data"
                  @if ($tab == 'referensi_pt') action="{{ url('ibel/referensi/pt/tambah') }}" @endif
                  @if ($tab == 'referensi_prodi') action="{{ url('ibel/referensi/prodi/tambah') }}" @endif
                  @if ($tab == 'referensi_lokasi') action="{{ url('ibel/referensi/lokasi/tambah') }}" @endif
                  @if ($tab == 'referensi_akreditasi') action="{{ url('ibel/referensi/prodi-akreditasi/tambah') }}"
                  @endif
                  @if ($tab == 'referensi_otorisasi') action="{{ url('ibel/referensi/otorisasi/tambah') }}" @endif
                  @if ($tab == 'tim_seleksi') action="{{ url('ibel/referensi/tim-otorisasi/tambah') }}" @endif>
                @csrf
                @if ($tab == 'referensi_prodi')
                    <div class="modal-body scroll-y px-5 px-lg-10">
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Nama Program Studi</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" name="nama_ref" rows="4"
                                       placeholder="isi nama program studi" required></textarea>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Jenjang
                                Pendidikan</label>
                            <div class="col-lg">
                                <select id="JenjangPendidikan" name="jenjangPendidikanId" data-control="select2"
                                        data-hide-search="true" data-placeholder="Pilih jenjang pendidikan" type="text"
                                        class="form-select " required>
                                    <option selected disabled>Pilih Jenjang Pendidikan</option>
                                    @foreach ($ref_prodi as $p)
                                        <option value="{{ $p->id }}">{{ $p->jenjangPendidikan }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                @endif

                @if ($tab == 'referensi_pt')
                    <div class="modal-body scroll-y px-5 px-lg-10">
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Nama Perguruan Tinggi</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" name="nama_ref" rows="4"
                                       placeholder="isi nama referensi" required></textarea>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Alamat</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" name="alamat_ref" rows="4"
                                       placeholder="isi alamat referensi" required></textarea>
                            </div>
                        </div>
                    </div>
                @endif

                @if ($tab == 'referensi_lokasi')
                    <div class="modal-body scroll-y px-5 px-lg-10">
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Nama Perguruan Tinggi</label>
                            <div class="col-lg">
                                <select id="ptId" name="ptId" data-control="select2" data-hide-search="true"
                                        data-placeholder="Pilih jenjang pendidikan" type="text" class="form-select "
                                        required>
                                    <option selected disabled>Pilih Perguruan Tinggi</option>
                                    @foreach ($ref_lokasi as $p)
                                        <option value="{{ $p->id }}">{{ $p->namaPerguruanTinggi }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Nama Kampus</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" name="nama_kampus" rows="4"
                                       placeholder="isi kampus" required></textarea>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Lokasi Pendidikan</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" name="lokasi" rows="4"
                                       placeholder="isi lokasi pendidikan" required></textarea>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Longitude</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" name="longi" rows="4"
                                       placeholder="104.xxxxx" required></textarea>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Latitude</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" name="lati" rows="4"
                                       placeholder="-1.xxxxxx" required></textarea>
                            </div>
                        </div>
                    </div>
                @endif

                @if ($tab == 'referensi_otorisasi')
                    <div class="modal-body scroll-y px-5 px-lg-10">
                        {{-- <div class="row mb-3">
                            <label class="col-lg-3 d-flex align-items-center fs-6 fw-bold mb-2">
                                <span class="required">Nama Pegawai</span>
                            </label>
                            <div class="col-lg">
                                <select id="tambah_data_pegawai" name="nama_pegawai" class="form-select form-select-lg"
                                    required>
                                    <option value=""></option>
                                </select>
                            </div>
                        </div> --}}
                        @if ( $mupang == true)
                            <div class="row mb-3">
                                <label class="col-lg-3 d-flex align-items-center fs-6 fw-bold mb-2">
                                    <span class="required">Nama Kantor</span>
                                </label>
                                <div class="col-lg">
                                    <select id="tambah_kantor_otorisasi" name="nama_kantor_otorisasi"
                                            class="form-select form-select-lg" required>
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>

                        @else
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Nama Kantor</label>
                                <div class="col-lg">
                                    <input class="form-control" type="text" name="nama_kantor_otorisasi2"
                                           rows="4" placeholder="" required value="{{ $namaKantorNonMupang }}"
                                           readonly></textarea>
                                </div>
                                <input type="hidden" id="tambah_kantor_otorisasi_non_mupang" name="idKantorNonMupang"
                                       value="{{ $idKantorNonMupang }}">
                            </div>

                        @endif

                        <div class="row mb-3">
                            <label class="col-lg-3 d-flex align-items-center fs-6 fw-bold mb-2">
                                <span class="required">Nama Pegawai</span>
                            </label>
                            <div class="col-lg">
                                <select id="tambah_data_pegawai_otorisasi" name="nama_pegawai"
                                        class="form-select form-select-lg" required>
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">NIP</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="nip_otorisasi_hris" name="nip_otorisasi"
                                       rows="4" placeholder="" required readonly></textarea>
                            </div>
                        </div>
                        {{-- <div class="row mb-3">
                            <label class="col-lg-3 d-flex align-items-center fs-6 fw-bold mb-2">
                                <span class="required">Nama Kantor</span>
                            </label>
                            <div class="col-lg">
                                <select id="tambah_perguruan_tinggi" name="nama_kantor_otorisasi"
                                    class="form-select form-select-lg" required>
                                    <option value=""></option>
                                </select>
                            </div>
                        </div> --}}
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tipe Otorisasi</label>
                            <div class="col-lg">
                                <select id="tipeOtorisasi" name="tipeOtorisasiId" data-control="select2"
                                        data-hide-search="true" data-placeholder="Pilih tipe otorisasi" type="text"
                                        class="form-select " required>
                                    <option selected disabled>Pilih Tipe Otorisasi</option>
                                    @foreach ($ref_otorisasi as $p)
                                        <option value="{{ $p->id }}">{{ $p->tipe }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                @endif

                @if ($tab == 'referensi_akreditasi')
                    <div class="modal-body scroll-y px-5 px-lg-10">
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Nama Perguruan Tinggi</label>
                            <div class="col-lg">
                                <select id="pt_akre" name="pr_akre" data-control="select2" data-hide-search="true"
                                        data-placeholder="Pilih jenjang pendidikan" type="text" class="form-select "
                                        required>
                                    <option selected disabled>Pilih Perguruan Tinggi</option>
                                    @foreach ($ref_akreditasi_pt as $p)
                                        <option value="{{ $p->id }}">{{ $p->namaPerguruanTinggi }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Program Studi</label>
                            <div class="col-lg">
                                <select id="prodi_akre" name="prodi_akre" data-control="select2" data-hide-search="true"
                                        data-placeholder="Pilih jenjang pendidikan" type="text" class="form-select "
                                        required>
                                    <option selected disabled>Pilih Program Studi</option>
                                    @foreach ($ref_akreditasi_prodi as $p)
                                        <option
                                            value="{{ $p->id }} | {{ $p->rjenjangPendidikanId->jenjangPendidikan }}">
                                            {{ $p->namaProgramStudi }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Jenjang Pendidikan</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="jenjang_pendidikan_prodi_akreditasi"
                                       rows="4" placeholder="" required readonly></textarea>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Akreditasi</label>
                            <div class="col-lg">
                                <select id="id_akreditasi" name="akreditasi" data-control="select2"
                                        data-hide-search="true" data-placeholder="Pilih jenjang pendidikan" type="text"
                                        class="form-select " required>
                                    <option selected disabled>Pilih Akreditasi</option>
                                    @foreach ($ref_akreditasi_id as $p)
                                        <option value="{{ $p->id }}">{{ $p->akreditasi }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Berakhir</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active datepick"
                                           placeholder="input tanggal berakhir" name="tgl_berakhir" type="text" value=""
                                           required>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if ($tab == 'tim_seleksi')
                    <div class="modal-body scroll-y px-5 px-lg-10">
                        {{-- <div class="row mb-3">
                            <label class="col-lg-3 d-flex align-items-center fs-6 fw-bold mb-2">
                                <span class="required">Nama Kantor</span>
                            </label>
                            <div class="col-lg">
                                <select id="tambah_kantor_otorisasi" name="nama_kantor_otorisasi"
                                    class="form-select form-select-lg" required>
                                    <option value=""></option>
                                </select>
                            </div>
                        </div> --}}
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6"> <span class="required"> Nama
                                    Tim</span></label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="nip_otorisasi_hris"
                                       name="nama_tim_otorisasi" rows="4" placeholder="" required></textarea>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="text-center mb-8">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                            class="fa fa-reply"></i>Batal
                    </button>
                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                </div>
            </form>

        </div>
    </div>
</div>

{{-- modal lihat detail --}}
<div class="modal fade" id="lihat_dasar_ref" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Lihat Detail Data</h3>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            @if ($tab == 'referensi_prodi')
                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Nama Program Studi</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="nama_ref_b" name="nama_ref"
                                   placeholder="Nama Negara" disabled>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Jenjang Pendidikan</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="alamat_ref_b" name="alamat_ref"
                                   placeholder="Jenjang Pendidikan" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Dibuat</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="tgl_ref_b" name="tgl_ref" rows="4"
                                   placeholder="id negara" disabled></textarea>
                        </div>
                    </div>
                </div>
            @endif
            @if ($tab == 'referensi_pt')
                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-4 col-form-label fw-bold fs-6">Nama Perguruan Tinggi</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="nama_ref_b" name="nama_ref"
                                   placeholder="Nama Negara" disabled>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-4 col-form-label fw-bold fs-6">Alamat</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="alamat_ref_b" name="alamat_ref"
                                   placeholder="Nama Negara" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Dibuat</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="tgl_ref_b" name="tgl_ref" rows="4"
                                   placeholder="id negara" disabled></textarea>
                        </div>
                    </div>
                </div>
            @endif
            @if ($tab == 'referensi_lokasi')
                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Nama Perguruan Tinggi</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="nama_pt" name="nama_pt"
                                   placeholder="Nama Negara" disabled>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Alamat Perguruan Tinggi</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="alamat_pt" name="nama_pt"
                                   placeholder="Nama Negara" disabled>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Nama Kampus</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="nama_kampus" name="nama_kampus"
                                   placeholder="Nama Negara" disabled>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Lokasi Pendidikan</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="lokasi" name="lokasi" rows="4"
                                   placeholder="id negara" disabled></textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Longitude</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="long" name="long" rows="4"
                                   placeholder="id negara" disabled></textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Latitude</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="lat" name="lat" rows="4"
                                   placeholder="id negara" disabled></textarea>
                        </div>
                    </div>
                </div>
            @endif
            @if ($tab == 'referensi_otorisasi')
                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Nama Pegawai</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="nama_pegawai_otoritas"
                                   name="nama_pegawai_otoritas" placeholder="Nama Negara" disabled>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">NIP</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="nip_otoritas" name="nip_otoritas"
                                   placeholder="Nama Negara" disabled>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Nama Kantor</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="nama_kantor_otoritas"
                                   name="nama_kantor_otoritas" rows="4" placeholder="id negara" disabled></textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tipe Otoritas</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="tipe_otoritas" name="tipe_otoritas" rows="4"
                                   placeholder="id negara" disabled></textarea>
                        </div>
                    </div>
                </div>
            @endif
            @if ($tab == 'referensi_akreditasi')
                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Nama Perguruan Tinggi</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="nama_pt_akr" name="nama_pt"
                                   placeholder="Nama Negara" disabled>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Alamat</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="alamat_pt" name="nama_kampus"
                                   placeholder="Nama Negara" disabled>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Program Studi</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="nama_prodi_akr" rows="4"
                                   placeholder="id negara" disabled></textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Jenjang Pendidikan</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="jenjang_akr_prodi" rows="4"
                                   placeholder="id negara" disabled></textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Akreditasi</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="akreditasi_prodi" rows="4"
                                   placeholder="id negara" disabled></textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Berakhir</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="tgl_berakhir_akr" rows="4"
                                   placeholder="id negara" disabled></textarea>
                        </div>
                    </div>
                </div>
            @endif
            @if ($tab == 'tim_seleksi')
                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Nama Tim</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="nama_tim_otorisasi" placeholder="Nama Negara"
                                   disabled>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Kantor Asal</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="kantor_asal_tim_otorisasi" placeholder=""
                                   disabled>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Ketua</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="--------------" rows="4" placeholder=""
                                   disabled></textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">JSekretaris</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="---------" rows="4" placeholder=""
                                   disabled></textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Anggota 1</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="----------" rows="4" placeholder=""
                                   disabled></textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Anggota 2</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="--------" rows="4" placeholder=""
                                   disabled></textarea>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>

{{-- modal edit --}}
<div class="modal fade" id="edit_dasar_ref" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Edit Data Referensi</h3>
                </div>

                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form" method="post" enctype="multipart/form-data"
                  @if ($tab == 'referensi_pt') action="{{ url('ibel/referensi/pt/update_pt') }}" @endif
                  @if ($tab == 'referensi_prodi') action="{{ url('ibel/referensi/prodi/update_prodi') }}" @endif
                  @if ($tab == 'referensi_lokasi') action="{{ url('ibel/referensi/lokasi/update_lokasi') }}" @endif
                  @if ($tab == 'referensi_akreditasi') action="{{ url('ibel/referensi/prodi-akreditasi/update_akreditasi') }}"
                  @endif
                  @if ($tab == 'referensi_otorisasi') action="{{ url('ibel/referensi/otorisasi/update_otorisasi') }}"
                  @endif
                  @if ($tab == 'tim_seleksi') action="{{ url('ibel/referensi/tim-otorisasi/update-tim-otorisasi') }}" @endif>
                @csrf
                <input type="hidden" id="id_ref_edit" name="id_ref_edit">

                <div class="modal-body scroll-y px-5 px-lg-10">
                    @if ($tab == 'referensi_prodi')
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Nama Program Studi</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="nama_ref_edit" name="nama_ref_edit"
                                       rows="4" placeholder="Edit nama yang akan diubah" required></textarea>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Jenjang
                                Pendidikan</label>
                            <div class="col-lg">
                                <select id="JenjangPendidikanprodi" name="jenjangPendidikanId" value=""
                                        data-control="select2" data-hide-search="true"
                                        data-placeholder="Pilih jenjang pendidikan" class="form-select" required>
                                    @foreach ($ref_prodi as $p)
                                        <option value="{{ $p->id }}">{{ $p->jenjangPendidikan }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif
                    @if ($tab == 'referensi_pt')
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Nama Perguruan Tinggi</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="nama_ref_edit" name="nama_ref_edit"
                                       rows="4" placeholder="Edit nama yang akan diubah" required></textarea>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Alamat</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="alamat_ref_edit" name="alamat_ref_edit"
                                       rows="4" placeholder="Edit nama yang akan diubah" required></textarea>
                            </div>
                        </div>
                    @endif
                    @if ($tab == 'referensi_lokasi')
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Nama Perguruan Tinggi</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="nama_pt_ref_lokasi" name="nama_kampus"
                                       rows="4" placeholder="Edit nama yang akan diubah" readonly></textarea>
                            </div>
                        </div>
                        <input type="hidden" id="pt_lokasi_kuliah" name="idPt">
                        {{-- <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Nama Perguruan Tinggi</label>
                            <div class="col-lg">
                                <select id="pt_lokasi_kuliah" name="idPt" value="" data-control="select2"
                                    data-hide-search="true" data-placeholder="Pilih jenjang pendidikan"
                                    class="form-select" rw>
                                    @foreach ($ref_lokasi as $p)
                                        <option value="{{ $p->id }}">{{ $p->namaPerguruanTinggi }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> --}}
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Nama Kampus</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="nama_kampus_edit" name="nama_kampus"
                                       rows="4" placeholder="Edit nama yang akan diubah" required></textarea>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Lokasi Pendidikan</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="lokasi_edit" name="lokasi" rows="4"
                                       placeholder="Edit nama yang akan diubah" required></textarea>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Longitude</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="long_edit" name="long" rows="4"
                                       placeholder="Edit nama yang akan diubah" required></textarea>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Latitude</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="lat_edit" name="lat" rows="4"
                                       placeholder="Edit nama yang akan diubah" required></textarea>
                            </div>
                        </div>
                    @endif
                    @if ($tab == 'referensi_akreditasi')
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Nama Perguruan Tinggi</label>
                            <div class="col-lg">
                                <select id="id_pt_akr" name="id_pt_akr" data-control="select2" data-hide-search="true"
                                        data-placeholder="Pilih jenjang pendidikan" type="text" class="form-select "
                                        disabled>
                                    <option selected>Pilih Perguruan Tinggi</option>
                                    @foreach ($ref_akreditasi_pt as $p)
                                        <option value="{{ $p->id }}">{{ $p->namaPerguruanTinggi }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="hidden" name="id_pt_akr" id="id_pt_akr2"/>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Program Studi</label>
                            <div class="col-lg">
                                <select id="id_prodi_akr" name="id_prodi_akr" data-control="select2"
                                        data-hide-search="true" data-placeholder="Pilih jenjang pendidikan" type="text"
                                        class="form-select " disabled>
                                    <option selected>Pilih Program Studi</option>
                                    @foreach ($ref_akreditasi_prodi as $p)
                                        <option value="{{ $p->id }}">{{ $p->namaProgramStudi }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="hidden" name="id_prodi_akr" id="id_prodi_akr2"/>
                        </div>
                        {{-- <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Jenjang Pendidikan</label>
                        <div class="col-lg">
                            <select id="jenjang_akreditasi" name="jenjang_akreditasi" data-control="select2"
                                data-hide-search="true" data-placeholder="Pilih jenjang pendidikan" type="text"
                                class="form-select " required>
                                <option selected disabled>Pilih Jenjang Pendidikan</option>
                                @foreach ($ref_akreditasi_jenjang as $p)
                                    <option value="{{ $p->id }}">{{ $p->jenjangPendidikan }}</option>
                                @endforeach
                            </select>
                        </div>
                        </div> --}}
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Akreditasi</label>
                            <div class="col-lg">
                                <select id="id_akreditasi_edit" name="id_akreditasi_edit" data-control="select2"
                                        data-hide-search="true" data-placeholder="Pilih jenjang pendidikan" type="text"
                                        class="form-select " required>
                                    <option selected disabled>Pilih Akreditasi</option>
                                    @foreach ($ref_akreditasi_id as $p)
                                        <option value="{{ $p->id }}">{{ $p->akreditasi }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Berakhir</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active datepick"
                                           placeholder="input tanggal berkahir" id="tgl_berakhir_akr"
                                           name="tgl_berakhir_akr" type="text" value="" required>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if ($tab == 'referensi_otorisasi')
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Nama Pegawai</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="nama_pegawai_oto_edit"
                                       name="nama_pegawai_oto_edit" rows="4" placeholder="Edit nama yang akan diubah"
                                       readonly></textarea>
                            </div>
                        </div>
                        <input type="hidden" id="nama_pegawai_oto_edit_id" name="nama_pegawai_oto_edit_id">
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">NIP</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="nip_oto_edit" name="nip_oto_edit"
                                       rows="4" placeholder="Edit nama yang akan diubah" readonly></textarea>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Kantor</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="nama_kantor_oto_edit"
                                       name="nama_kantor_oto_edit" rows="4" placeholder="Edit nama yang akan diubah"
                                       readonly></textarea>
                            </div>
                        </div>
                        <input type="hidden" id="nama_kantor_oto_edit_id" name="nama_kantor_oto_edit_id">
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Otorisasi</label>
                            <div class="col-lg">
                                <select id="tipe_oto_edit" name="OtorsisasiId" value="" data-control="select2"
                                        data-hide-search="true" data-placeholder="Pilih otorisasi" class="form-select"
                                        required>
                                    @foreach ($ref_otorisasi as $p)
                                        <option value="{{ $p->id }}">{{ $p->tipe }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif
                    @if ($tab == 'tim_seleksi')
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Nama Tim</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="nama_tim_otorisasi_edit"
                                       name="nama_tim_otorisasi" rows="4" placeholder="Edit nama yang akan diubah"
                                       required></textarea>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Kantor</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="kantor_asal_tim_otorisasi_edit"
                                       name="kantor_asal_tim_otorisasi_edit" rows="4"
                                       placeholder="Edit nama yang akan diubah" disabled></textarea>
                            </div>
                        </div>
                        <input type="hidden" id="id_kantor_tim_otorisasi_edit" name="id_kantor_tim_otorisasi_edit">
                    @endif
                </div>

                <div class="text-center mb-8">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                            class="fa fa-reply"></i>Batal
                    </button>
                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
