@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="row g-5 g-xxl-8">
                {{-- <div class="col-sm-4">
                    <div class="col-xl-12">
                        <div class="card card-sm-stretch mb-5 mb-xl-8 border-warning shadow-sm">
                            <div class="card-header card-header-stretch" id="card-header">
                                <div class="card-title fw-bold fs-4 text-white">
                                    Daftar Referensi
                                </div>

                            </div>
                            <div class="card-body" style="padding-top: 15px; padding-bottom: 2px;">
                                <div class="flex-column flex-lg-row-auto w-100 w-lg-275px mb-10 me-lg-20">
                                    <div class="mb-1">
                                        <div
                                            class="menu menu-rounded menu-column menu-title-gray-700 menu-state-title-primary menu-active-bg-light-primary fw-bold">
                                            <div class="menu-item mb-1">
                                                <a style="width: 336px;" href="{{ url('ibel/referensi') }}"
                                                    class="menu-link py-3 {{ $tab == 'referensi_pt' ? 'active' : '' }}">Referensi
                                                    Perguruan Tinggi</a>
                                            </div>
                                        </div>
                                        <div
                                            class="menu menu-rounded menu-column menu-title-gray-700 menu-state-title-primary menu-active-bg-light-primary fw-bold">
                                            <div class="menu-item mb-1">
                                                <a style="width: 336px;" href="{{ url('ibel/referensi/prodi') }}"
                                                    class="menu-link py-3 {{ $tab == 'referensi_prodi' ? 'active' : '' }}">Referensi
                                                    Program Studi</a>
                                            </div>
                                        </div>
                                        <div
                                            class="menu menu-rounded menu-column menu-title-gray-700 menu-state-title-primary menu-active-bg-light-primary fw-bold">
                                            <div class="menu-item mb-1">
                                                <a style="width: 336px;" href="{{ url('ibel/referensi/lokasi') }}"
                                                    class="menu-link py-3 {{ $tab == 'referensi_lokasi' ? 'active' : '' }}">Referensi
                                                    Lokasi Pendidikan</a>
                                            </div>
                                        </div>
                                        <div
                                            class="menu menu-rounded menu-column menu-title-gray-700 menu-state-title-primary menu-active-bg-light-primary fw-bold">
                                            <div class="menu-item mb-1">
                                                <a style="width: 336px;" href="{{ url('ibel/referensi/otorisasi') }}"
                                                    class="menu-link py-3 {{ $tab == 'referensi_otorisasi' ? 'active' : '' }}">Referensi
                                                    Otorisasi Izin Belajar</a>
                                            </div>
                                        </div>
                                        <div
                                            class="menu menu-rounded menu-column menu-title-gray-700 menu-state-title-primary menu-active-bg-light-primary fw-bold">
                                            <div class="menu-item mb-1">
                                                <a style="width: 336px;" href="{{ url('ibel/referensi/prodiAkr') }}"
                                                    class="menu-link py-3 {{ $tab == 'referensi_akreditasi' ? 'active' : '' }}">Referensi
                                                    Referensi Akreditasi Program Studi</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <div class="col-xl">
                    <div class="col-lg-12">
                        <div class="card mb-3 border-warning shadow-sm">
                            <div class="card-header card-header-stretch" id="card-header">
                                <div class="card-title fw-bold fs-4 text-white">
                                    @if ($tab == 'referensi_pt')
                                        Referensi Perguruan Tinggi
                                    @elseif ($tab == 'referensi_prodi')
                                        Referensi Program Studi
                                    @elseif ($tab == 'referensi_lokasi')
                                        Referensi Lokasi Pendidikan
                                    @elseif ($tab == 'referensi_otorisasi')
                                        Referensi Otorisasi Izin Belajar
                                    @elseif ($tab == 'referensi_akreditasi')
                                        Referensi Akreditasi Program Studi
                                    @elseif ($tab == 'tim_seleksi')
                                        Referensi Tim Seleksi
                                    @endif
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="tab-content">
                                    <div
                                        @if ($tab == 'referensi_pt') class="tab-pane fade active show " id="ref_pt"
                                        role="tabpanel"
                                        @elseif ($tab == 'referensi_prodi') class="tab-pane fade active show "
                                        id="ref_pt" role="tabpanel"
                                        @elseif ($tab == 'referensi_lokasi') class="tab-pane fade active show "
                                        id="ref_pt" role="tabpanel"
                                        @elseif ($tab == 'referensi_otorisasi') class="tab-pane fade active show "
                                        id="ref_pt" role="tabpanel"
                                        @elseif ($tab == 'referensi_akreditasi') class="tab-pane fade active show "
                                        id="ref_pt" role="tabpanel"
                                        @elseif ($tab == 'tim_seleksi') class="tab-pane fade active show "
                                        id="tim_seleksi" role="tabpanel"
                                        @endif>
                                        @if ($tab == 'referensi_pt')
                                            @include('ibel.referensi.referensi_ibel_tabel')
                                        @endif
                                        @if ($tab == 'referensi_prodi')
                                            @include('ibel.referensi.referensi_ibel_tabel')
                                        @endif
                                        @if ($tab == 'referensi_lokasi')
                                            @include('ibel.referensi.referensi_ibel_tabel')
                                        @endif
                                        @if ($tab == 'referensi_otorisasi')
                                            @include('ibel.referensi.referensi_ibel_tabel')
                                        @endif
                                        @if ($tab == 'referensi_akreditasi')
                                            @include('ibel.referensi.referensi_ibel_tabel')
                                        @endif
                                        @if ($tab == 'tim_seleksi')
                                            @include('ibel.referensi.referensi_ibel_tabel')
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('ibel.referensi.referensi_ibel_modal')
@endsection

@section('js')
    {{-- <script>
        let app_url = '{{ env('APP_URL') }}'
    </script> --}}
    <script src="{{ asset('assets/js/ibel') }}/referensi.js"></script>

@endsection
