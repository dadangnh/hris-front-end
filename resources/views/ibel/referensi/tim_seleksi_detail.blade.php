@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">

            <div class="card border-warning">
                <div class="card-header" id="card-header">
                    <h2 class="card-title">
                        <span
                            class="fw-bold fs-4 text-white">Kelola Anggota {{ $tampilkanNamaTim['0']->namaTim }}</span>
                    </h2>

                </div>
                <div class="card-body">
                    <div class=" col-md row mb-3">
                        <div class="col">
                            <a href="{{ url('ibel/referensi/tim-otorisasi') }}" class="btn btn-sm btn-secondary mb-3"><i
                                    class="fa fa-reply"></i> Kembali</a>
                        </div>
                    </div>
                    {{-- @if ($action != false)
                    <div class="row mb-8">
                        <div class="card col-lg">
                            <div class="accordion" id="kt_accordion_1">
                                <div class="accordion-item border-secondary">
                                    <h2 class="accordion-header" id="kt_accordion_1_header_2">
                                        <button class="accordion-button fs-4 fw-bold collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#kt_accordion_1_body_2" aria-expanded="false" aria-controls="kt_accordion_1_body_2">
                                            Cari Nama
                                        </button>
                                    </h2>
                                    <div id="kt_accordion_1_body_2" class="accordion-collapse collapse" aria-labelledby="kt_accordion_1_header_2" data-bs-parent="#kt_accordion_1">
                                        <div class="accordion-body">
                                            <div class="row mb-8">
                                                <div class="col-lg"></div>
                                                <div class="row col-lg-10">
                                                    <label class="col-lg-2 col-form-label fw-bold fs-6">Nama </label>
                                                    <div class="row col-lg">
                                                        <div class="col-lg-8">
                                                            <input class="form-control" type="text" id="pencarian_nama_nip" name="pencarian" placeholder="cari nama" required>
                                                        </div>
                                                        <div class="col-lg">
                                                            <button class="btn btn-md btn-secondary fs-6 btnCari">
                                                                <i class="fa fa-search"></i> Cari
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg"></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif --}}


                    {{-- data anggota --}}
                    <div class="row" id="hasil_pencarian">
                        <div class="row mb-2">
                            <div class="col-lg-1"></div>
                            <div class="col-lg">
                                <table id="kt_customers_table"
                                       class="caption-top table table-rounded table-striped border bg-gray-400 rounded border border-gray-300 gs-7">
                                    <caption class="fs-5">Referensi Anggota {{ $namaKantor }}</caption>
                                    <thead>
                                    <tr class="text-start fw-bolder bg-secondary">
                                        <th class="">No</th>
                                        <th class="">Nama Pegawai</th>
                                        <th class="">Kantor Asal</th>
                                        <th class="">Role Status</th>
                                        <th class="w-100px pe-2">
                                            <div
                                                class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                                <input class="form-check-input" type="checkbox" data-kt-check="true"
                                                       data-kt-check-target="#kt_customers_table .form-check-input"
                                                       value="1"/>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody id="table_nama_nip">

                                    @if ($dataAnggotaBykantor == null)
                                        <tr>
                                            <td class="text-center"
                                                colspan="{{ $dataAnggotaBykantor != null ? 4 : 5 }}">Tidak terdapat
                                                data
                                            </td>
                                        </tr>
                                    @else
                                        @php
                                            $no = 1;
                                        @endphp

                                        @foreach ($dataAnggotaBykantor as $l)
                                            <tr id="{{ $l->id }}">
                                                <td class="text-dark text-center ps-4">
                                                    {{ $no++ }}
                                                </td>
                                                <td class="text-dark">
                                                    {{ $l->namaPegawai }} <br>
                                                </td>
                                                <td class="text-dark">
                                                    {{ $l->namaKantor }}
                                                </td>
                                                <td>
                                                        <span class="badge badge-light-success fs-7 fw-bolder">
                                                            {{ $l->rtipeOtorisasiId->tipe }}</span>
                                                </td>
                                                <td>
                                                    <div
                                                        class="form-check form-check-sm form-check-custom form-check-solid">
                                                        <input id="check" class="form-check-input dataCheck"
                                                               type="checkbox" value="{{ $l->id }}"/>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif


                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-1"></div>
                        </div>
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-lg">
                                <div class="text-end">
                                    <button class="btn btn-sm btn-primary fs-6 mb-2 btnImportNipNama">
                                        <i class="fa fa-download"></i> Import
                                    </button>
                                </div>
                            </div>
                            <div class="col-lg-1"></div>
                        </div>
                    </div>

                    {{-- anggota Tim --}}
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg">

                            <div class="table-responsive bg-gray-400 rounded">
                                <table
                                    class="caption-top table table-rounded table-striped border bg-gray-400 rounded border border-gray-300 gs-7"
                                    id="kt_customers_table">
                                    <caption class="fs-5">Daftar Anggota {{ $tampilkanNamaTim['0']->namaTim }}
                                    </caption>
                                    <thead>
                                    <tr class="text-start fw-bolder bg-secondary align-middle">
                                        <th class="ps-4 text-center">No</th>
                                        <th class="">Nama Pegawai</th>
                                        <th class="">Kantor Asal</th>
                                        <th class="">Role Status</th>
                                        <th class="ps-4 text-center">Aksi</th>

                                    </tr>
                                    </thead>
                                    <tbody class="">

                                    @if ($anggota == null)
                                        <tr>
                                            <td class="text-center" colspan="{{ $anggota != null ? 4 : 5 }}">Tidak
                                                terdapat data
                                            </td>
                                        </tr>
                                    @else
                                        @php
                                            $no = 1;
                                        @endphp

                                        @foreach ($anggota as $l)
                                            <tr id="{{ $l->id }}">
                                                <td class="text-dark text-center ps-4">
                                                    {{ $no++ }}
                                                </td>
                                                <td class="text-dark">
                                                    {{ $l->otorisasiIzinBelajarId->namaPegawai }} <br>
                                                </td>
                                                <td class="text-dark">
                                                    {{ $l->otorisasiIzinBelajarId->namaKantor }}
                                                </td>
                                                <td>
                                                        <span class="badge badge-light-success fs-7 fw-bolder">
                                                            {{ $l->otorisasiIzinBelajarId->rtipeOtorisasiId->tipe }}</span>
                                                </td>

                                                @if ($action != false)
                                                    <td class="text-center">
                                                        <button
                                                            class="btn btn-sm btn-rounded btn-danger btn-icon btnDeleteAnggota"
                                                            data-id="{{ $l->id }}">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                @endif

                                            </tr>
                                        @endforeach
                                    @endif


                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('js')
    <script>
        // button pencarian by nama
        $(".btnCari").click(function () {
            var $this = $(this);

            //Call Button Loading Function
            BtnLoading($this);

            $.ajax({
                type: 'post',
                url: '{{ env('APP_URL') }}' + '/ibel/referensi/tim-otorisasi/cari-anggota',
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    keyword: $("#pencarian_nama_nip").val()
                },
                success: function (response) {
                    var myArray = []
                    if (response.totalItems == 0 || response.data == "") {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Gagal!',
                            text: "Anggota tidak ditemukan! ",
                            showConfirmButton: false,
                            timer: 2000
                        });


                    } else {
                        // alert(response.data)
                        myArray = response.data
                        $(this).prop("disabled", true);

                        $('#hasil_pencarian').show();

                        console.log(response)
                        buildTable(myArray)
                    }

                    // reset button
                    BtnReset($this);
                }
            });

        });

        // buat tabel data pencarian by nama
        function buildTable(data) {
            var table = document.getElementById('table_nama_nip')
            // var table = document.getElementsByClassName('form-check-input')

            for (var i = 0; i < data.length; i++) {
                var row =
                    `<tr>
                <td>${data[i].namaPegawai}</td>
                <td>${data[i].namaKantor}</td>
                <td>${data[i].rtipeOtorisasiId.tipe}</td>

                <td><div class="form-check form-check-sm form-check-custom form-check-solid">
                        <input id="check" class="form-check-input dataCheck" type="checkbox" value="${data[i].id}" />
                    </div>
                </td>

            </tr>`

                table.innerHTML += row
            }
        }

        // button import data by nama
        $(".btnImportNipNama").click(function () {
            // var checkedValue = $('.checkForm2:checked').val();
            var $this = $(this);

            BtnLoading($this);

            // set var check
            var checkedValue = null;

            // get element check
            var inputElements = document.getElementsByClassName('dataCheck');

            // looping data check pegawai, get data check
            var idpegawai = [];
            for (var i = 0; inputElements[i]; i++) {
                if (inputElements[i].checked) {
                    idpegawai.push(inputElements[i].value)

                }
            }

            // post looping
            $.ajax({
                type: 'post',
                url: '{{ env('APP_URL') }}' + '/ibel/referensi/tim-otorisasi/tambah-anggota',
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id_tim_otorisasi: '{{ $idTim }}',
                    id_anggota_tim_by_otorisasi: idpegawai
                },
                success: function (response) {
                    // console.log()
                    if (response['status'] == 0) {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Gagal',
                            text: response.message,
                            // text: 'database error (Conflict)',
                            showConfirmButton: false,
                            timer: 1600
                        });
                    } else {
                        location.reload();
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Berhasil',
                            // text: 'data berhasil ditambahkan',
                            text: response.output_berhasil + response.output_gagal,
                            showConfirmButton: false,
                            timer: 1600
                        });
                    }

                    // reset button
                    BtnReset($this);

                },
                error: function (err) {
                    console.log(err)
                }
            });


        });

        // button hapus lampiran (id)
        $(".btnDeleteAnggota").click(function () {
            var token = $("meta[name='csrf-token']").attr("content")
            var id = $(this).data('id')

            Swal.fire({
                title: 'Yakin akan menghapus data?',
                // text: "Data tidak dapat dikembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#C5584B',
                cancelButtonColor: '#B2B2B2',
                confirmButtonText: 'Hapus!',
                cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'post',
                        url: '{{ env('APP_URL') }}' +
                            '/ibel/referensi/tim-otorisasi/hapus-anggota',
                        data: {
                            _token: token,
                            id: $(this).data('id'),
                        },
                        success: function (response) {
                            // console.log(response)
                            $('#' + id).remove();
                            Swal.fire({
                                position: "center",
                                icon: "success",
                                title: response.message,
                                showConfirmButton: false,
                                title: "Berhasil",
                                text: "data berhasil dihapus",
                                timer: 2000,
                            });
                        },
                        error: function (err) {
                            console.log(err)
                        }
                    });

                }
            })
        });

        // spinner / loading button
        function BtnLoading(elem) {
            $(elem).attr("data-original-text", $(elem).html());
            $(elem).prop("disabled", true);
            $(elem).html('Loading... <i class="spinner-border spinner-border-sm"></i>');
        }

        // spinner off / reset button
        function BtnReset(elem) {
            $(elem).prop("disabled", false);
            $(elem).html($(elem).attr("data-original-text"));
        }
    </script>
@endsection
