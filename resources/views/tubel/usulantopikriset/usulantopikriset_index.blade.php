@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card border-warning">
                    <div class="card-header card-header-stretch" id="card-header">
                        <div class="card-toolbar">
                            <ul class="nav nav-tabs nav-line-tabs nav-stretch border-transparent fs-5 fw-bolder"
                                id="tabLps">
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'persetujuan' ? 'active' : '' }}"
                                       href="{{ url('tubel/riset/usulan-topik-riset/persetujuan') }}"> Menunggu
                                        Persetujuan </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'ditolak' ? 'active' : '' }}"
                                       href="{{ url('tubel/riset/usulan-topik-riset/ditolak') }}"> Ditolak </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'dikembalikan' ? 'active' : '' }}"
                                       href="{{ url('tubel/riset/usulan-topik-riset/dikembalikan') }}">
                                        Dikembalikan </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'pengesahan' ? 'active' : '' }}"
                                       href="{{ url('tubel/riset/usulan-topik-riset/pengesahan') }}"> Pengesahan </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'unggah-persetujuan' ? 'active' : '' }}"
                                       href="{{ url('tubel/riset/usulan-topik-riset/unggah-persetujuan') }}"> Unggah
                                        Persetujuan </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'riset-disetujui' ? 'active' : '' }}"
                                       href="{{ url('tubel/riset/usulan-topik-riset/riset-disetujui') }}"> Riset
                                        Disetujui </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'dashboard' ? 'active' : '' }}"
                                       href="{{ url('tubel/riset/usulan-topik-riset/dashboard') }}"> Dashboard </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'persetujuan' ? 'active show' : '' }} "
                                 id="persetujuan" role="tabpanel">
                                @if ($tab == 'persetujuan')
                                    @include('tubel.usulantopikriset.usulantopikriset_tabel')
                                @endif
                            </div>

                            <div class="tab-pane fade {{ $tab == 'ditolak' ? 'active show' : '' }} " id="ditolak"
                                 role="tabpanel">
                                @if ($tab == 'ditolak')
                                    @include('tubel.usulantopikriset.usulantopikriset_tabel')
                                @endif
                            </div>

                            <div class="tab-pane fade {{ $tab == 'dikembalikan' ? 'active show' : '' }} "
                                 id="dikembalikan" role="tabpanel">
                                @if ($tab == 'dikembalikan')
                                    @include('tubel.usulantopikriset.usulantopikriset_tabel')
                                @endif
                            </div>

                            <div class="tab-pane fade {{ $tab == 'pengesahan' ? 'active show' : '' }} " id="pengesahan"
                                 role="tabpanel">
                                @if ($tab == 'pengesahan')
                                    @include('tubel.usulantopikriset.usulantopikriset_tabel')
                                @endif
                            </div>

                            <div class="tab-pane fade {{ $tab == 'unggah-persetujuan' ? 'active show' : '' }} "
                                 id="unggah-persetujuan" role="tabpanel">
                                @if ($tab == 'unggah-persetujuan')
                                    @include('tubel.usulantopikriset.usulantopikriset_tabel')
                                @endif
                            </div>


                            <div class="tab-pane fade {{ $tab == 'riset-disetujui' ? 'active show' : '' }} "
                                 id="riset-disetujui" role="tabpanel">
                                @if ($tab == 'riset-disetujui')
                                    @include('tubel.usulantopikriset.usulantopikriset_tabel')
                                @endif
                            </div>

                            <div class="tab-pane fade {{ $tab == 'dashboard' ? 'active show' : '' }} " id="dashboard"
                                 role="tabpanel">
                                @if ($tab == 'dashboard')
                                    @include('tubel.usulantopikriset.usulantopikriset_tabel')
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('tubel.usulantopikriset.usulantopikriset_modal')
    @include('tubel.modal_tiket')

@endsection

@section('js')
    <script>
        let app_url = '{{ env('APP_URL') }}'
    </script>

    <script src="{{ asset('assets/js/tubel') }}/usulan-topik-riset.js"></script>
    <script src="{{ asset('assets/js/tubel') }}/log-tiket.js"></script>
@endsection
