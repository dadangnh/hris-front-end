<div class="row">
    <div class="col-lg-6 mb-4">
        <div class="text-start" data-kt-customer-table-toolbar="base">
            <button type="button" class="btn btn-sm btn-ligth btn-active-primary border border-gray-400 fs-6"
                    data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" data-kt-menu-flip="top-end">
                <i class="fa fa-filter"></i> Advanced Search
            </button>
            <div id="advancedSearch" class="menu menu-sub menu-sub-dropdown w-md-1000px border-warning mt-1"
                 data-kt-menu="true">
                <div class="px-7 py-5">
                    <div class="fs-4 text-dark fw-bolder">Filter Pencarian</div>
                </div>
                <div class="separator border-gray-200"></div>
                <form action="" method="get">
                    {{-- @csrf --}}
                    <div class="px-7 py-5">
                        <div class="row">
                            <div class="col-lg-6">
                                <label class="form-label fw-bold">Pegawai:</label>
                                <div class="row mb-4">
                                    <div class="col-lg-6">
                                        <input class="form-control form-select-solid" type="text" name="nama"
                                               placeholder="nama pegawai"
                                               value="{{ isset($_GET['nama']) ? $_GET['nama'] : '' }}">
                                    </div>
                                    <div class="col-lg-6">
                                        <input class="form-control form-select-solid" type="text" name="nip"
                                               placeholder="nip 18 digit"
                                               value="{{ isset($_GET['nip']) ? $_GET['nip'] : '' }}">
                                    </div>
                                </div>
                                <label class="form-label fw-bold">Jenjang Pendidikan:</label>
                                <div class="mb-4">
                                    <select id="search_jenjang" class="form-select" name="jenjang"
                                            data-control="select2" data-hide-search="true">
                                        <option></option>
                                        @if (isset($_GET['jenjang']))
                                            @foreach ($jenjang as $j)
                                                <option
                                                    value="{{ $j->id }}" {{ $_GET['jenjang'] == $j->id ? 'selected' : '' }}>{{ $j->jenjangPendidikan }}</option>
                                            @endforeach
                                        @else
                                            @foreach ($jenjang as $j)
                                                <option value="{{ $j->id }}">{{ $j->jenjangPendidikan }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <label class="form-label fw-bold">Lokasi Pendidikan:</label>
                                <div class="mb-4">
                                    <select id="search_lokasi" class="form-select" name="lokasi" data-control="select2"
                                            data-hide-search="false">
                                        <option></option>
                                        @foreach ($lokasi as $l)
                                            <option value="{{ $l->id }}">{{ $l->lokasi }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-label fw-bold">Topik Riset:</label>
                                <div class="row mb-4">
                                    <select id="topik_search" name="topik" class="form-control form-select"
                                            data-kt-select2="true" data-dropdown-parent="#advancedSearch">
                                        <option value=""></option>
                                    </select>
                                </div>
                                <label class="form-label fw-bold">Tiket:</label>
                                <div class="row mb-4">
                                    <div class="col-lg">
                                        <input class="form-control form-select-solid" type="text" name="tiket"
                                               placeholder="tiket"
                                               value="{{ isset($_GET['tiket']) ? $_GET['tiket'] : '' }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <a href="{{ url()->current() }}"
                               class="btn btn-light btn-active-light-primary me-2">Reset</a>
                            <button type="submit" class="btn btn-sm btn-primary" data-kt-menu-dismiss="true"
                                    data-kt-customer-table-filter="filter">Search
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="text-end mb-4">
            @if ($tab == 'disetujui')
                {{-- <button data-bs-toggle="modal" data-bs-target="#rekam_cuti" class="btn btn-primary btn-sm fs-6"> --}}
                <button class="btn btn-primary btn-sm fs-6 btnRekamCuti">
                    <i class="fa fa-plus"></i> Rekam Cuti
                </button>
            @endif
        </div>
    </div>
</div>

<div class="table-responsive bg-gray-400 rounded border border-gray-300">
    <table class="table table-striped align-middle" id="tabel_cuti">
        <thead class="fw-bolder bg-secondary fs-6">
        <tr>
            <th class="text-center ps-2">No</th>
            <th class="">Pegawai</th>
            <th class="">Topik / Judul</th>
            <th class="">Proposal / Persetujuan PT</th>

            @if ($tab == 'unggah-persetujuan')
                <th class="col-lg">Persetujuan DJP</th>
            @endif

            <th class="col-lg-2">No Tiket</th>
            <th class="text-center col-lg-2">Aksi</th>
        </tr>
        </thead>
        <tbody>

        @php
            $nomor = $currentPage == 1 ? 1 : ($currentPage - 1) * $size + 1;
        @endphp

        @if ($usulanriset->data != null)

            @foreach ($usulanriset->data as $u)

                <tr id="topikRiset-{{ $u->id }}">
                    <td class="text-center ps-2 fs-6">
                        {{ $nomor++ }}
                    </td>

                    <td class="fs-6">
                            <span class="text-dark fw-bolder">
                                {{ $u->dataIndukTubel->namaPegawai }}
                            </span>
                        <br>
                        {{ $u->dataIndukTubel->nip18 }}

                        <br>
                        <br>
                        Pendidikan: {{ $u->dataIndukTubel->jenjangPendidikanId->jenjangPendidikan }}
                        <br>
                        {{ $u->dataIndukTubel->lokasiPendidikanId->lokasi }}
                    </td>

                    <td class="fs-6">
                        Topik:
                        <br>
                        {{ $u->topik ? $u->topik->namaTopik : '' }}

                        <br>
                        <br>
                        Judul:
                        <br>
                        {{ $u->namaRiset ? $u->namaRiset : '' }}
                    </td>

                    <td class="fs-6">
                        Proposal:
                        <br>
                        <a href="{{ $u->pathProposal ? url('files/' . $u->pathProposal) : '#' }}" target="_blank"><i
                                class="fa fa-file-alt text-hover-primary"></i> Proposal.pdf</a>

                        <br>
                        <br>
                        Persetujuan PT/Dosen:
                        <br>
                        @if (!empty($u->tglPersetujuanPt))
                            tanggal: {{ AppHelper::instance()->indonesian_date($u->tglPersetujuanPt, 'j F Y', '') }}
                            <br>
                            <a href="{{ $u->pathDokumenPersetujuanPt ? url('files/' . $u->pathDokumenPersetujuanPt) : '#' }}"
                               target="_blank"><i class="fa fa-file-alt text-hover-primary"></i> Persetujuan.pdf</a>
                        @else
                            <i><span class="text-muted fw-bold text-muted  fs-6">{{ 'belum ada' }}</span></i>
                        @endif
                    </td>

                    @if ($tab == 'unggah-persetujuan')
                        <td class="fs-6">
                            <a href="#" class="btn btn-sm btn-info">
                                <i class="fa fa-download"></i>Unduh Draft ND
                            </a>
                        </td>
                    @endif


                    <td class="text-dark fs-6">
                        @if ($tab == 'ditolak')
                            <div>
                                <span
                                    class="badge badge-light-danger fs-7 fw-bolder">{{ $u->logStatusProbis->outputId->caseOutput }}</span>
                            </div>
                        @endif

                        <div class="">
                            Tiket:
                            <a href="javascript:void(0)" class="text-primary btnTiket"
                               data-tiket="{{ $u->logStatusProbis->nomorTiket }}"
                               data-nama="{{ $u->dataIndukTubel->namaPegawai }}">
                                {{ $u->logStatusProbis->nomorTiket }}
                            </a>
                        </div>
                    </td>

                    <td class="text-center">

                        @if ($u->logStatusProbis->output == 'Usulan topik dikirim' || $tab == 'pengesahan')

                            <button class="btn btn-sm btn-primary btnPeriksaUsulanRiset" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Edit"
                                    data-nama="{{ $u->dataIndukTubel->namaPegawai }}"
                                    data-nip="{{ $u->dataIndukTubel->nip18 }}"
                                    data-pangkat="{{ $u->dataIndukTubel->namaPangkat }}"
                                    data-jabatan="{{ $u->dataIndukTubel->namaJabatan }}"
                                    data-jenjang="{{ $u->dataIndukTubel->jenjangPendidikanId->jenjangPendidikan }}"
                                    data-program_beasiswa="{{ $u->dataIndukTubel->programBeasiswa }}"
                                    data-lokasi="{{ $u->dataIndukTubel->lokasiPendidikanId->lokasi }}"
                                    {{-- data-pt="{{ $u->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }}" --}} {{-- data-prodi="{{ $u->ptPesertaTubelId->programStudiId->programStudi }}" --}} data-st_tubel="{{ $u->dataIndukTubel->nomorSt }}"
                                    data-tgl_st_tubel="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tglSt, 'j F Y', '') }}"
                                    data-tgl_mulai="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tglMulaiSt, 'j F Y', '') }}"
                                    data-tgl_selesai="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tglSelesaiSt, 'j F Y', '') }}"
                                    data-kep_pembebasan="{{ $u->dataIndukTubel->nomorKepPembebasan }}"
                                    data-tgl_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tglKepPembebasan, 'j F Y', '') }}"
                                    data-tmt_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tmtKepPembebasan, 'j F Y', '') }}"
                                    data-email="{{ $u->dataIndukTubel->emailNonPajak }}"
                                    data-hp="{{ $u->dataIndukTubel->noHandphone }}"
                                    data-alamat="{{ $u->dataIndukTubel->alamatTinggal }}"
                                    data-id_usulan_riset_periksa="{{ $u->id }}"
                                    data-topik_riset_periksa="{{ $u->topik->namaTopik }}"
                                    data-judul_riset_periksa="{{ $u->namaRiset }}"
                                    data-file_proposal_periksa="{{ $u->pathProposal }}"
                                    data-tgl_persetujuan_pt_periksa="{{ AppHelper::instance()->indonesian_date($u->tglPersetujuanPt, 'j F Y', '') }}"
                                    data-dokumen_persetujuan_periksa="{{ $u->pathDokumenPersetujuanPt }}">
                                <i class="fa fa-file"></i> Periksa
                            </button>

                        @elseif ($u->logStatusProbis->output == 'Usulan topik dikirim')

                            <button class="btn btn-sm btn-primary btnPengesahanUsulanRiset" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Edit" {{-- data-id_usulan_riset_edit="{{ $u->id }}"
                                    data-id_pilihan_topik_edit="{{ $u->topik->id }}" data-pilihan_topik_edit="{{ $u->topik->namaTopik }}" data-judul_riset_edit="{{ $u->namaRiset }}"
                                    data-file_dokumen_proposal_edit="{{ $u->pathProposal }}" --}}>
                                <i class="fa fa-file"></i> Periksa / Pengesahan
                            </button>


                            {{-- @elseif($u->logStatusProbis->output == 'Usulan topik dikirim') --}}

                            <button class="btn btn-sm btn-primary btnPersetujuaniUsulanRiset" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Persetujuan" data-id="{{ $u->id }}">
                                <i class="fa fa-check"></i> Persetujuan
                            </button>

                        @endif

                        @if ($tab == 'ditolak' || $tab == 'dikembalikan' || $tab == 'riset-disetujui')
                            <button class="btn btn-sm btn-info btn-icon btnLihatUsulanRiset" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Lihat"
                                    data-nama="{{ $u->dataIndukTubel->namaPegawai }}"
                                    data-nip="{{ $u->dataIndukTubel->nip18 }}"
                                    data-pangkat="{{ $u->dataIndukTubel->namaPangkat }}"
                                    data-jabatan="{{ $u->dataIndukTubel->namaJabatan }}"
                                    data-jenjang="{{ $u->dataIndukTubel->jenjangPendidikanId->jenjangPendidikan }}"
                                    data-program_beasiswa="{{ $u->dataIndukTubel->programBeasiswa }}"
                                    data-lokasi="{{ $u->dataIndukTubel->lokasiPendidikanId->lokasi }}"
                                    {{-- data-pt="{{ $u->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }}" --}} {{-- data-prodi="{{ $u->ptPesertaTubelId->programStudiId->programStudi }}" --}} data-st_tubel="{{ $u->dataIndukTubel->nomorSt }}"
                                    data-tgl_st_tubel="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tglSt, 'j F Y', '') }}"
                                    data-tgl_mulai="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tglMulaiSt, 'j F Y', '') }}"
                                    data-tgl_selesai="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tglSelesaiSt, 'j F Y', '') }}"
                                    data-kep_pembebasan="{{ $u->dataIndukTubel->nomorKepPembebasan }}"
                                    data-tgl_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tglKepPembebasan, 'j F Y', '') }}"
                                    data-tmt_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tmtKepPembebasan, 'j F Y', '') }}"
                                    data-email="{{ $u->dataIndukTubel->emailNonPajak }}"
                                    data-hp="{{ $u->dataIndukTubel->noHandphone }}"
                                    data-alamat="{{ $u->dataIndukTubel->alamatTinggal }}"
                                    data-id_usulan_riset="{{ $u->id }}" data-topik_riset="{{ $u->topik->namaTopik }}"
                                    data-judul_riset="{{ $u->namaRiset }}" data-file_proposal="{{ $u->pathProposal }}"
                                    data-tgl_persetujuan_pt="{{ AppHelper::instance()->indonesian_date($u->tglPersetujuanPt, 'j F Y', '') }}"
                                    data-file_persetujuan_pt="{{ $u->pathDokumenPersetujuanPt }}"
                                    data-nomor_nd_persetujuan="{{ $u->nomorNdPersetujuanDjp }}"
                                    data-tgl_nd_persetujuan="{{ AppHelper::instance()->indonesian_date($u->tglPersetujuanDjp, 'j F Y', '') }}"
                                    data-dokumen_persetujuan_nd="{{ $u->pathNdPersetujuanDjp }}">
                                <i class="fa fa-eye"></i>
                            </button>

                        @elseif ($tab == 'unggah-persetujuan')
                            <button class="btn btn-sm btn-primary btnPeriksaUsulanRiset" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Edit"
                                    data-nama="{{ $u->dataIndukTubel->namaPegawai }}"
                                    data-nip="{{ $u->dataIndukTubel->nip18 }}"
                                    data-pangkat="{{ $u->dataIndukTubel->namaPangkat }}"
                                    data-jabatan="{{ $u->dataIndukTubel->namaJabatan }}"
                                    data-jenjang="{{ $u->dataIndukTubel->jenjangPendidikanId->jenjangPendidikan }}"
                                    data-program_beasiswa="{{ $u->dataIndukTubel->programBeasiswa }}"
                                    data-lokasi="{{ $u->dataIndukTubel->lokasiPendidikanId->lokasi }}"
                                    {{-- data-pt="{{ $u->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }}" --}} {{-- data-prodi="{{ $u->ptPesertaTubelId->programStudiId->programStudi }}" --}} data-st_tubel="{{ $u->dataIndukTubel->nomorSt }}"
                                    data-tgl_st_tubel="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tglSt, 'j F Y', '') }}"
                                    data-tgl_mulai="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tglMulaiSt, 'j F Y', '') }}"
                                    data-tgl_selesai="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tglSelesaiSt, 'j F Y', '') }}"
                                    data-kep_pembebasan="{{ $u->dataIndukTubel->nomorKepPembebasan }}"
                                    data-tgl_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tglKepPembebasan, 'j F Y', '') }}"
                                    data-tmt_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tmtKepPembebasan, 'j F Y', '') }}"
                                    data-email="{{ $u->dataIndukTubel->emailNonPajak }}"
                                    data-hp="{{ $u->dataIndukTubel->noHandphone }}"
                                    data-alamat="{{ $u->dataIndukTubel->alamatTinggal }}"
                                    data-id_usulan_riset_periksa="{{ $u->id }}"
                                    data-topik_riset_periksa="{{ $u->topik->namaTopik }}"
                                    data-judul_riset_periksa="{{ $u->namaRiset }}"
                                    data-file_proposal_periksa="{{ $u->pathProposal }}"
                                    data-tgl_persetujuan_pt_periksa="{{ AppHelper::instance()->indonesian_date($u->tglPersetujuanPt, 'j F Y', '') }}"
                                    data-dokumen_persetujuan_periksa="{{ $u->pathDokumenPersetujuanPt }}">
                                <i class="fa fa-file"></i> ND Pengesahan
                            </button>
                        @endif

                    </td>
                </tr>

            @endforeach

        @else

            <tr class="text-center">
                <td class="text-dark mb-1 fs-6" colspan="7">Tidak terdapat data</td>
            </tr>

        @endif

        </tbody>
    </table>

</div>

@if ($totalItems != 0)
    <!--begin::pagination-->
    <div class="d-flex flex-stack flex-wrap pt-5 p-3">
        <div class="fs-6 fw-bold text-gray-700"></div>

        @php
            $disabled = '';
            $nextPage = '';
            $disablednext = '';
            $hidden = '';
            $pagination = '';

            // tombol pagination
            if ($totalItems == 0) {
                // $pagination = 'none';
            }

            // tombol back
            if ($currentPage == 1) {
                $disabled = 'disabled';
            }

            // tombol next
            if ($currentPage != $totalPages) {
                $nextPage = $currentPage + 1;
            }

            // tombol pagination
            if ($currentPage == $totalPages || $totalPages == 0) {
                $disablednext = 'disabled';
                $hidden = 'hidden';
            }

        @endphp

        <ul class="pagination" style="display: {{ $pagination }}">
            <li class="page-item disabled"><a href="#" class="page-link">Halaman {{ $currentPage }}
                    dari {{ $totalPages }}</a></li>

            <li class="page-item previous {{ $disabled }}"><a
                    href="{{ url()->current() . '?page=' . ($currentPage - 1) }}" class="page-link"><i
                        class="previous"></i></a></li>
            <li class="page-item active"><a href="{{ url()->current() . '?page=' . $currentPage }}"
                                            class="page-link">{{ $currentPage }}</a></li>
            <li class="page-item" {{ $hidden }}><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                   class="page-link">{{ $nextPage }}</a></li>
            <li class="page-item next {{ $disablednext }}"><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                              class="page-link"><i class="next"></i></a></li>
        </ul>

    </div>
@endif
