<div class="modal fade" id="load_csv_modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Usulan Topik Riset</h4>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>

            <form id="upload_csv_form" action="" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body scroll-y px-lg-10 mb-3">
                    <div class="row mb-8">
                        @csrf
                        <div class="col-lg-2">
                            <label class="col-form-label fw-bold fs-6">File CSV</label>
                        </div>
                        <div class="col-lg">
                            <input id="file_csv" class="form-control" type="file" name="file_csv">
                            <span class="form-text text-muted mt-2">
                                <ul>
                                    <li>format csv</li>
                                    <li>delimiter titik koma ( ; )</li>
                                </ul>
                            </span>
                        </div>
                        <div class="form-label col-lg-3">
                            <button id="btnPreviewCsv" class="btn btn-primary btnPreviewCsv">
                                <i class="fa fa-upload"></i> Preview
                            </button>
                            <a href="{{ asset('assets/templates/topik_riset_template.xlsx') }}"
                               class="btn btn-icon btn-info solid ms-2" data-bs-toggle="tooltip" data-bs-placement="top"
                               title="template file">
                                <i class="fa fa-file"></i>
                            </a>
                            {{-- <button type="submit" class="btn btn-primary btnImportCsv">
                                <i class="fa fa-upload"></i> Import Data
                            </button> --}}
                            <button class="btn btn-primary btn-sm fs-6" type="button" hidden disabled>
                                Loading
                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            </button>
                        </div>
                    </div>
                    <div class="ps-3 mb-1 fs-6 labelPreview" hidden>
                        Preview
                    </div>
                    <div class="text-center">
                        <span id="tiket-spinner" class="spinner-border spinner-border-md text-warning" role="status"
                              aria-hidden="true" hidden></span>
                    </div>
                    <div
                        class="table-responsive bg-gray-400 rounded border border-gray-300 referensiRisetTableInduk shadow-sm"
                        style="height: 600px; overflow-y: auto;">
                        <table class="table table-striped align-middle referensiRisetTable">
                        </table>
                    </div>
                </div>
                <div class="text-center mb-4 btnAction">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                            class="fa fa-reply"></i>Batal
                    </button>
                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                </div>
            </form>

        </div>
    </div>
</div>
