@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card border-warning">
                    <div id="card-header" class="card-header">
                        <div class="card-title fw-bold fs-4 text-white">
                            Referensi Topik Riset
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6 mb-4">
                                <div class="text-start" data-kt-customer-table-toolbar="base">
                                    <button type="button"
                                            class="btn btn-sm btn-ligth btn-active-primary border border-gray-400 fs-6"
                                            data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start"
                                            data-kt-menu-flip="top-end">
                                        <i class="fa fa-filter"></i> Advanced Search
                                    </button>
                                    <div class="menu menu-sub menu-sub-dropdown w-md-500px border-warning mt-1"
                                         data-kt-menu="true">
                                        <div class="px-7 py-5">
                                            <div class="fs-4 text-dark fw-bolder">Filter Pencarian</div>
                                        </div>
                                        <div class="separator border-gray-200"></div>
                                        <form id="advancedSearch" action="" method="get">
                                            <div class="px-7 py-5">
                                                <div class="col-lg">
                                                    <label class="form-label fw-bold">Topik Riset:</label>
                                                    <div class="row mb-4">
                                                        <input class="form-control form-select-solid" type="text"
                                                               name="topik_riset" placeholder="topik riset"
                                                               value="{{ isset($_GET['topik_riset']) ? $_GET['topik_riset'] : '' }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg">
                                                    <label class="form-label fw-bold">Nomor Pengumuman:</label>
                                                    <div class="row mb-4">
                                                        <input class="form-control form-select-solid" type="text"
                                                               name="nomor_pengumuman" placeholder="nomor pengumuman"
                                                               value="{{ isset($_GET['nomor_pengumuman']) ? $_GET['nomor_pengumuman'] : '' }}">
                                                    </div>
                                                </div>
                                                {{-- <div class="col-lg">
                                                    <div class="row mb-8">
                                                        <label class="form-label fw-bold">Tanggal Pengumuman:</label>
                                                        <input type="text" class="form-control text-start datepick" name="tgl_pengumuman" placeholder="tgl pengumuman"
                                                            value="{{ isset($_GET['tgl_pengumuman']) ? $_GET['tgl_pengumuman'] : '' }}" data-dropdown-parent="#advancedSearch" />
                                                    </div>
                                                </div> --}}
                                                <div class="d-flex justify-content-end mt-8">
                                                    <a href="{{ url()->current() }}"
                                                       class="btn btn-light btn-active-light-primary me-2">Reset</a>
                                                    <button type="submit" class="btn btn-sm btn-primary"
                                                            data-kt-menu-dismiss="true"
                                                            data-kt-customer-table-filter="filter">Search
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="text-end mb-4">
                                    <button class="btn btn-primary btn-sm fs-6 btnLoadCsv">
                                        <i class="fa fa-upload"></i> Load CSV File
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="table-responsive bg-gray-400 rounded border border-gray-300">
                            <table id="referensi_topik_riset_table" class="table table-striped align-middle">
                                <thead class="fw-bolder bg-secondary fs-6">
                                <tr class="align-middle">
                                    <th class="text-center ps-2 w-80px">No</th>
                                    <th class="col-lg-5">Topik Riset</th>
                                    <th class="col-lg-3">Nomor Pengumuman</th>
                                    <th class="col-lg">Tanggal Pengumuman</th>
                                    <th class="col-lg-1 text-center">
                                        <div
                                            class="col-lg form-check form-check-md form-check-custom form-check-solid me-3">
                                            <div class="">
                                                <input id="check_all" class="form-check-input border border-gray-500"
                                                       type="checkbox" data-kt-check="true"
                                                       data-kt-check-target="#referensi_topik_riset_table .form-check-input"
                                                       value="1"/>
                                            </div>
                                            @if (1 == 1)
                                                <div class="ps-4">
                                                    <button class="btn btn-sm btn-icon btn-danger btnDelete">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </div>
                                            @endif
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>

                                @php
                                    $nomor = $currentPage == 1 ? 1 : ($currentPage - 1) * $size + 1;
                                @endphp

                                @if ($usulantopik->data != null)

                                    @foreach ($usulantopik->data as $u)
                                        <tr class="" id="topikRiset-{{ $u->id }}">
                                            <td class="text-dark text-center ps-4">
                                                {{ $nomor++ }}
                                            </td>

                                            <td class="text-dark mb-1 fs-6">
                                                {{ $u->namaTopik }}
                                            </td>

                                            <td class="text-dark mb-1 fs-6">
                                                {{ $u->nomorPengumuman }}
                                            </td>

                                            <td class="text-dark mb-1 fs-6">
                                                <span
                                                    class="fw-normal fs-6">{{ AppHelper::instance()->indonesian_date($u->tglPengumuman, 'j F Y', '') }}</span>
                                            </td>

                                            <td class="text-dark mb-1 fs-6">
                                                <div
                                                    class="form-check form-check-md form-check-custom form-check-solid">
                                                    <input id="check"
                                                           class="form-check-input border border-gray-500 dataCheck"
                                                           type="checkbox" value="{{ $u->id }}"/>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach

                                @else

                                    <tr class="text-center">
                                        <td class="text-dark  mb-1 fs-6" colspan="5">Tidak terdapat data</td>
                                    </tr>

                                @endif

                                </tbody>
                            </table>
                        </div>

                        @if ($totalItems != 0)
                            <!--begin::pagination-->
                            <div class="d-flex flex-stack flex-wrap pt-5 p-3">
                                <div class="fs-6 fw-bold text-gray-700"></div>

                                @php
                                    $disabled = '';
                                    $nextPage = '';
                                    $disablednext = '';
                                    $hidden = '';
                                    $pagination = '';

                                    // tombol pagination
                                    if ($totalItems == 0) {
                                        // $pagination = 'none';
                                    }

                                    // tombol back
                                    if ($currentPage == 1) {
                                        $disabled = 'disabled';
                                    }

                                    // tombol next
                                    if ($currentPage != $totalPages) {
                                        $nextPage = $currentPage + 1;
                                    }

                                    // tombol pagination
                                    if ($currentPage == $totalPages || $totalPages == 0) {
                                        $disablednext = 'disabled';
                                        $hidden = 'hidden';
                                    }

                                @endphp

                                <ul class="pagination" style="display: {{ $pagination }}">
                                    <li class="page-item disabled"><a href="#"
                                                                      class="page-link">Halaman {{ $currentPage }}
                                            dari {{ $totalPages }}</a></li>

                                    <li class="page-item previous {{ $disabled }}"><a
                                            href="{{ url()->current() . '?page=' . ($currentPage - 1) }}"
                                            class="page-link"><i class="previous"></i></a></li>
                                    <li class="page-item active"><a
                                            href="{{ url()->current() . '?page=' . $currentPage }}"
                                            class="page-link">{{ $currentPage }}</a></li>
                                    <li class="page-item" {{ $hidden }}><a
                                            href="{{ url()->current() . '?page=' . $nextPage }}"
                                            class="page-link">{{ $nextPage }}</a></li>
                                    <li class="page-item next {{ $disablednext }}"><a
                                            href="{{ url()->current() . '?page=' . $nextPage }}" class="page-link"><i
                                                class="next"></i></a></li>
                                </ul>

                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('tubel.usulantopikriset.kelolatopikriset_modal')

@endsection

@section('js')
    <script>
        // advanced search
        $("#advancedSearch").submit(function (e) {
            e.preventDefault();

            var query = $(this).serializeArray().filter(function (i) {
                return i.value;
            });

            window.location.href = $(this).attr('action') + (query ? '?' + $.param(query) : '');

        })
    </script>

    <script src="{{ asset('assets/js/tubel') }}/usulan-topik-riset.js"></script>
@endsection
