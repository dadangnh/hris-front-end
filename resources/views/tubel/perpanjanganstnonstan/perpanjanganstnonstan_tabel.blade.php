@php
    $halaman = isset($_GET['page']) ? (int) $_GET['page'] : 1;
    $halaman_awal = $halaman > 1 ? $halaman * $size - $size : 0;
    $nomor = $halaman_awal + 1;
@endphp

<div class="row">
    <div class="col-lg-6 mb-4">
        <div class="text-start" data-kt-customer-table-toolbar="base">
            <button type="button" class="btn btn-sm btn-ligth btn-active-primary border border-gray-400 fs-6"
                    data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" data-kt-menu-flip="top-end">
                <i class="fa fa-filter"></i> Advanced Search
            </button>
            <div class="menu menu-sub menu-sub-dropdown w-md-1000px border-warning mt-1" data-kt-menu="true">
                <div class="px-7 py-5">
                    <div class="fs-4 text-dark fw-bolder">Filter Pencarian</div>
                </div>
                <div class="separator border-gray-200"></div>
                <form action="" method="get">
                    {{-- @csrf --}}
                    <div class="px-7 py-5">
                        <div class="row">
                            <div class="col-lg-6">
                                <label class="form-label fw-bold">Pegawai:</label>
                                <div class="row mb-4">
                                    <div class="col-lg-6">
                                        <input class="form-control form-select-solid" type="text" name="nama"
                                               placeholder="nama pegawai"
                                               value="{{ isset($_GET['nama']) ? $_GET['nama'] : '' }}">
                                    </div>
                                    <div class="col-lg-6">
                                        <input class="form-control form-select-solid" type="text" name="nip"
                                               placeholder="nip 18 digit"
                                               value="{{ isset($_GET['nip']) ? $_GET['nip'] : '' }}">
                                    </div>
                                </div>
                                <label class="form-label fw-bold">Jenjang Pendidikan:</label>
                                <div class="mb-4">
                                    <select id="search_jenjang" class="form-select" name="jenjang"
                                            data-control="select2" data-hide-search="true">
                                        <option></option>
                                        @if (isset($_GET['jenjang']))
                                            @foreach ($jenjang as $j)
                                                <option
                                                    value="{{ $j->id }}" {{ $_GET['jenjang'] == $j->id ? 'selected' : '' }}>{{ $j->jenjangPendidikan }}</option>
                                            @endforeach
                                        @else
                                            @foreach ($jenjang as $j)
                                                <option value="{{ $j->id }}">{{ $j->jenjangPendidikan }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <label class="form-label fw-bold">Lokasi Pendidikan:</label>
                                <div class="mb-4">
                                    <select id="search_lokasi" class="form-select" name="lokasi" data-control="select2"
                                            data-hide-search="false">
                                        <option></option>
                                        @foreach ($lokasi as $l)
                                            <option value="{{ $l->id }}">{{ $l->lokasi }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row mb-4">
                                    <div class="col-lg-6">
                                        <label class="form-label fw-bold">No. ST Awal:</label>
                                        <input class="form-control form-select-solid" type="text" name="nost"
                                               placeholder="nomor st awal"
                                               value="{{ isset($_GET['nost']) ? $_GET['nost'] : '' }}">
                                    </div>
                                    <div class="col-lg-6">
                                        <label class="form-label fw-bold">Tiket:</label>
                                        <input class="form-control form-select-solid" type="text" name="tiket"
                                               placeholder="tiket"
                                               value="{{ isset($_GET['tiket']) ? $_GET['tiket'] : '' }}">
                                    </div>
                                </div>
                                <label class="form-label fw-bold">Tanggal Selesai:</label>
                                <div class="row mb-8">
                                    <div class="input-daterange input-group">
                                        <input type="text" class="form-control text-start date-search" name="tgl_mulai"
                                               placeholder="tanggal selesai (dari)"
                                               value="{{ isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : '' }}"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                s.d.
                                            </span>
                                        </div>
                                        <input type="text" class="form-control text-start date-search"
                                               name="tgl_selesai" placeholder="tanggal selesai (sampai)"
                                               value="{{ isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : '' }}"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <a href="{{ url()->current() }}"
                               class="btn btn-light btn-active-light-primary me-2">Reset</a>
                            <button type="submit" class="btn btn-sm btn-primary" data-kt-menu-dismiss="true"
                                    data-kt-customer-table-filter="filter">Search
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="table-responsive bg-gray-400 rounded border border-gray-300">
    <table class="table table-striped align-middle" id="tabel_keppengaktifan_kembali">

        <thead class="fw-bolder bg-secondary fs-6">
        <tr class="">
            <th class="ps-4 text-center">No</th>
            <th class="">Pegawai</th>
            <th class="">Pendidikan</th>
            <th class="">Tgl Selesai</th>
            <th class="">ST Awal</th>
            <th class="">No Tiket</th>
            <th class="text-center">Aksi</th>
        </tr>
        </thead>
        <tbody>
        @if ($perpanjanganstnonstan->data != null)

            @foreach ($perpanjanganstnonstan->data as $t)
                <tr class="text-right">

                    <td class="ps-4 text-center fs-6">
                        {{ $nomor++ }}
                    </td>

                    <td class="text-dark fw-bolder mb-1 fs-6 ps-4">
                        <a class="fw-bolder"
                           href="{{ url('tubel/monitoring/' . $t->dataIndukTubelId->id . '?tab=lps') }}">
                            {{ $t->dataIndukTubelId->namaPegawai }}
                        </a>
                        <br>
                        <span class="fw-normal fs-6">{{ $t->dataIndukTubelId->nip18 }}</span>
                    </td>

                    <td class="text-dark fw-bolder mb-1 fs-6">
                        {{ $t->dataIndukTubelId->jenjangPendidikanId->jenjangPendidikan }}
                        <br>
                        <span class="fw-normal fs-6">{{ $t->dataIndukTubelId->lokasiPendidikanId->lokasi }}</span>
                    </td>

                    <td class="text-dark mb-1 fs-6">
                        {{ AppHelper::instance()->indonesian_date($t->tglSelesaiStPerpanjangan, 'j F Y', '') }}
                    </td>

                    <td class="text-dark mb-1 fs-6">
                        {{ $t->noStAwal }}
                    </td>

                    <td class="text-dark mb-1 fs-6">
                        <a href="javascript:void(0)" class="text-primary btnTiket"
                           data-tiket="{{ $t->logStatusProbis->nomorTiket }}"
                           data-nama="{{ $t->dataIndukTubelId->namaPegawai }}">
                            {{ $t->logStatusProbis->nomorTiket }}
                        </a>
                    </td>

                    <td class="text-center text-dark fs-6">

                        @if ($tab != 'menunggu')

                            <button class="btn btn-icon btn-sm btn-info btnLihatPerpanjanganSt" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Lihat" data-id_permohonan_aktif="{{ $t->id }}"
                                    data-nama="{{ $t->dataIndukTubelId->namaPegawai }}"
                                    data-nip="{{ $t->dataIndukTubelId->nip18 }}"
                                    data-pangkat="{{ $t->dataIndukTubelId->namaPangkat }}"
                                    data-jabatan="{{ $t->dataIndukTubelId->namaJabatan }}"
                                    data-jenjang="{{ $t->dataIndukTubelId->jenjangPendidikanId->jenjangPendidikan }}"
                                    data-program_beasiswa="{{ $t->dataIndukTubelId->programBeasiswa }}"
                                    data-lokasi="{{ $t->dataIndukTubelId->lokasiPendidikanId->lokasi }}"
                                    {{-- data-pt="{{ $t->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }}" --}} {{-- data-prodi="{{ $t->ptPesertaTubelId->programStudiId->programStudi }}" --}}
                                    data-st_tubel="{{ $t->dataIndukTubelId->nomorSt }}"
                                    data-tgl_st_tubel="{{ AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tglSt, 'j F Y', '') }}"
                                    data-tgl_mulai="{{ AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tglMulaiSt, 'j F Y', '') }}"
                                    data-tgl_selesai="{{ AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tglSelesaiSt, 'j F Y', '') }}"
                                    data-kep_pembebasan="{{ $t->dataIndukTubelId->nomorKepPembebasan }}"
                                    data-tgl_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tglKepPembebasan, 'j F Y', '') }}"
                                    data-tmt_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tmtKepPembebasan, 'j F Y', '') }}"
                                    data-email="{{ $t->dataIndukTubelId->emailNonPajak }}"
                                    data-hp="{{ $t->dataIndukTubelId->emailNonPajak }}"
                                    data-alamat="{{ $t->dataIndukTubelId->alamatTinggal }}"
                                    data-alasan_perpanjangan_st_lihat="{{ $t->alasanPerpanjangan }}"
                                    data-tahapan_studi_lihat="{{ $t->tahapanStudi }}"
                                    data-tgl_selesai_perpanjangan_lihat="{{ AppHelper::instance()->indonesian_date($t->tglSelesaiPerpanjangan, 'j F Y', '') }}"
                                    data-st_awal_lihat="{{ $t->noStAwal }}"
                                    data-file_st_awal_lihat="{{ $t->pathStAwal }}"
                                    data-surat_persetujuan_kampus_lihat="{{ $t->alasanPerpanjangan }}"
                                    data-tgl_surat_persetujuan_kampus_lihat="{{ AppHelper::instance()->indonesian_date($t->tglPersetujuanKampus, 'j F Y', '') }}"
                                    data-file_surat_persetujuan_kampus_lihat="{{ $t->pathPersetujuanKampus }}"
                                    data-surat_persetujuan_sponsor_lihat="{{ $t->nomorPersetujuanSponsor }}"
                                    data-tgl_surat_persetujuan_sponsor_lihat="{{ AppHelper::instance()->indonesian_date($t->tglPersetujuanSponsor, 'j F Y', '') }}"
                                    data-file_surat_persetujuan_sponsor_lihat="{{ $t->pathPersetujuanSponsor }}"
                                    data-nomor_st_perpanjangan_lihat="{{ $t->nomorStPerpanjangan }}"
                                    data-tgl_st_perpanjangan_lihat="{{ AppHelper::instance()->indonesian_date($t->tglStPerpanjangan, 'j F Y', '') }}"
                                    data-tgl_mulai_st_perpanjangan_lihat="{{ AppHelper::instance()->indonesian_date($t->tglMulaiStPerpanjangan, 'j F Y', '') }}"
                                    data-tgl_selesai_st_perpanjangan_lihat="{{ AppHelper::instance()->indonesian_date($t->tglSelesaiStPerpanjangan, 'j F Y', '') }}"
                                    data-file_st_perpanjangan_lihat="{{ $t->pathStPerpanjangan }}">
                                <i class="fa fa-eye"></i>
                            </button>

                        @endif

                        @if ($t->logStatusProbis->output == 'Perpanjangan ST dikirim')

                            <button class="btn btn-sm btn-primary btnPeriksaPerpanjanganSt" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Rekam" data-id_permohonan_aktif="{{ $t->id }}"
                                    data-nama="{{ $t->dataIndukTubelId->namaPegawai }}"
                                    data-nip="{{ $t->dataIndukTubelId->nip18 }}"
                                    data-pangkat="{{ $t->dataIndukTubelId->namaPangkat }}"
                                    data-jabatan="{{ $t->dataIndukTubelId->namaJabatan }}"
                                    data-jenjang="{{ $t->dataIndukTubelId->jenjangPendidikanId->jenjangPendidikan }}"
                                    data-program_beasiswa="{{ $t->dataIndukTubelId->programBeasiswa }}"
                                    data-lokasi="{{ $t->dataIndukTubelId->lokasiPendidikanId->lokasi }}"
                                    {{-- data-pt="{{ $t->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }}" --}} {{-- data-prodi="{{ $t->ptPesertaTubelId->programStudiId->programStudi }}" --}}
                                    data-st_tubel="{{ $t->dataIndukTubelId->nomorSt }}"
                                    data-tgl_st_tubel="{{ AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tglSt, 'j F Y', '') }}"
                                    data-tgl_mulai="{{ AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tglMulaiSt, 'j F Y', '') }}"
                                    data-tgl_selesai="{{ AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tglSelesaiSt, 'j F Y', '') }}"
                                    data-kep_pembebasan="{{ $t->dataIndukTubelId->nomorKepPembebasan }}"
                                    data-tgl_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tglKepPembebasan, 'j F Y', '') }}"
                                    data-tmt_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tmtKepPembebasan, 'j F Y', '') }}"
                                    data-email="{{ $t->dataIndukTubelId->emailNonPajak }}"
                                    data-hp="{{ $t->dataIndukTubelId->emailNonPajak }}"
                                    data-alamat="{{ $t->dataIndukTubelId->alamatTinggal }}"
                                    data-alasan_perpanjangan_st_periksa="{{ $t->alasanPerpanjangan }}"
                                    data-tahapan_studi_periksa="{{ $t->tahapanStudi }}"
                                    data-tgl_selesai_perpanjangan_periksa="{{ AppHelper::instance()->indonesian_date($t->tglSelesaiPerpanjangan, 'j F Y', '') }}"
                                    data-st_awal_periksa="{{ $t->noStAwal }}"
                                    data-file_st_awal_periksa="{{ $t->pathStAwal }}"
                                    data-surat_persetujuan_kampus_periksa="{{ $t->nomorPersetujuanKampus }}"
                                    data-tgl_surat_persetujuan_kampus_periksa="{{ AppHelper::instance()->indonesian_date($t->tglPersetujuanKampus, 'j F Y', '') }}"
                                    data-file_surat_persetujuan_kampus_periksa="{{ $t->pathPersetujuanKampus }}"
                                    data-surat_persetujuan_sponsor_periksa="{{ $t->nomorPersetujuanSponsor }}"
                                    data-tgl_surat_persetujuan_sponsor_periksa="{{ AppHelper::instance()->indonesian_date($t->tglPersetujuanSponsor, 'j F Y', '') }}"
                                    data-file_surat_persetujuan_sponsor_periksa="{{ $t->pathPersetujuanSponsor }}">
                                <i class="fa fa-cog"></i> Periksa
                            </button>

                        @elseif ($t->logStatusProbis->output == 'Perpanjangan ST disetujui')

                            <button class="btn btn-sm btn-primary btnUploadStPerpanjanganSt" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Upload ST" data-id_permohonan_aktif="{{ $t->id }}"
                                    data-nama="{{ $t->dataIndukTubelId->namaPegawai }}"
                                    data-nip="{{ $t->dataIndukTubelId->nip18 }}"
                                    data-pangkat="{{ $t->dataIndukTubelId->namaPangkat }}"
                                    data-jabatan="{{ $t->dataIndukTubelId->namaJabatan }}"
                                    data-jenjang="{{ $t->dataIndukTubelId->jenjangPendidikanId->jenjangPendidikan }}"
                                    data-program_beasiswa="{{ $t->dataIndukTubelId->programBeasiswa }}"
                                    data-lokasi="{{ $t->dataIndukTubelId->lokasiPendidikanId->lokasi }}"
                                    {{-- data-pt="{{ $t->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }}" --}} {{-- data-prodi="{{ $t->ptPesertaTubelId->programStudiId->programStudi }}" --}}
                                    data-st_tubel="{{ $t->dataIndukTubelId->nomorSt }}"
                                    data-tgl_st_tubel="{{ AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tglSt, 'j F Y', '') }}"
                                    data-tgl_mulai="{{ AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tglMulaiSt, 'j F Y', '') }}"
                                    data-tgl_selesai="{{ AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tglSelesaiSt, 'j F Y', '') }}"
                                    data-kep_pembebasan="{{ $t->dataIndukTubelId->nomorKepPembebasan }}"
                                    data-tgl_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tglKepPembebasan, 'j F Y', '') }}"
                                    data-tmt_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tmtKepPembebasan, 'j F Y', '') }}"
                                    data-email="{{ $t->dataIndukTubelId->emailNonPajak }}"
                                    data-hp="{{ $t->dataIndukTubelId->emailNonPajak }}"
                                    data-alamat="{{ $t->dataIndukTubelId->alamatTinggal }}"
                                    data-alasan_perpanjangan_st_upload_st="{{ $t->alasanPerpanjangan }}"
                                    data-tahapan_studi_upload_st="{{ $t->tahapanStudi }}"
                                    data-tgl_selesai_perpanjangan_upload_st="{{ AppHelper::instance()->indonesian_date($t->tglSelesaiPerpanjangan, 'j F Y', '') }}"
                                    data-st_awal_upload_st="{{ $t->noStAwal }}"
                                    data-file_st_awal_upload_st="{{ $t->pathStAwal }}"
                                    data-surat_persetujuan_kampus_upload_st="{{ $t->alasanPerpanjangan }}"
                                    data-tgl_surat_persetujuan_kampus_upload_st="{{ AppHelper::instance()->indonesian_date($t->tglPersetujuanKampus, 'j F Y', '') }}"
                                    data-file_surat_persetujuan_kampus_upload_st="{{ $t->pathPersetujuanKampus }}"
                                    data-surat_persetujuan_sponsor_upload_st="{{ $t->nomorPersetujuanSponsor }}"
                                    data-tgl_surat_persetujuan_sponsor_upload_st="{{ AppHelper::instance()->indonesian_date($t->tglPersetujuanSponsor, 'j F Y', '') }}"
                                    data-file_surat_persetujuan_sponsor_upload_st="{{ $t->pathPersetujuanSponsor }}">
                                <i class="fa fa-upload"></i> Upload ST
                            </button>

                        @endif

                    </td>
                </tr>
            @endforeach
        @else
            <tr class="text-center">
                <td class="text-dark mb-1 fs-6" colspan="7">Tidak terdapat data</td>
            </tr>
        @endif

        </tbody>
    </table>

</div>

@if ($totalItems != 0)
    <!--begin::pagination-->
    <div class="d-flex flex-stack flex-wrap pt-5 p-3">
        <div class="fs-6 fw-bold text-gray-700"></div>

        @php
            $disabled = '';
            $nextPage = '';
            $disablednext = '';
            $hidden = '';
            $pagination = '';

            // tombol pagination
            if ($totalItems == 0) {
                // $pagination = 'none';
            }

            // tombol back
            if ($currentPage == 1) {
                $disabled = 'disabled';
            }

            // tombol next
            if ($currentPage != $totalPages) {
                $nextPage = $currentPage + 1;
            }

            // tombol pagination
            if ($currentPage == $totalPages || $totalPages == 0) {
                $disablednext = 'disabled';
                $hidden = 'hidden';
            }

        @endphp

            <!--begin::Pages-->
        <ul class="pagination" style="display: {{ $pagination }}">
            <li class="page-item disabled"><a href="#" class="page-link">Halaman {{ $currentPage }}
                    dari {{ $totalPages }}</a></li>
            <li class="page-item previous {{ $disabled }}"><a
                    href="{{ url()->current() . '?page=' . ($currentPage - 1) }}" class="page-link"><i
                        class="previous"></i></a></li>
            <li class="page-item active"><a href="{{ url()->current() . '?page=' . $currentPage }}"
                                            class="page-link">{{ $currentPage }}</a></li>
            <li class="page-item" {{ $hidden }}><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                   class="page-link">{{ $nextPage }}</a></li>
            <li class="page-item next {{ $disablednext }}"><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                              class="page-link"><i class="next"></i></a></li>
        </ul>
        <!--end::Pages-->

    </div>
    <!--end::paging-->
@endif
