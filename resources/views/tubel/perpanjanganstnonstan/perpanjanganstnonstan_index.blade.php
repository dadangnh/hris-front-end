@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card mb-3 border-warning shadow-sm">
                    <div class="card-header card-header-stretch" id="card-header">
                        <div class="card-toolbar">
                            <ul class="nav nav-tabs nav-line-tabs nav-stretch border-transparent fs-5 fw-bolder"
                                id="tabLps">
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'menunggu' ? 'active' : '' }}"
                                       href="{{ url('tubel/perpanjangan-st-nonstan') }}"> Menunggu Persetujuan </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'disetujui' ? 'active' : '' }}"
                                       href="{{ url('tubel/perpanjangan-st-nonstan/disetujui') }}"> Telah Disetujui </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'dikembalikan' ? 'active' : '' }}"
                                       href="{{ url('tubel/perpanjangan-st-nonstan/dikembalikan') }}"> Dikembalikan </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'ditolak' ? 'active' : '' }}"
                                       href="{{ url('tubel/perpanjangan-st-nonstan/ditolak') }}"> Ditolak </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'terbitst' ? 'active' : '' }}"
                                       href="{{ url('tubel/perpanjangan-st-nonstan/terbitst') }}"> Telah terbit ST </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'dashboard' ? 'active' : '' }}"
                                       href="{{ url('tubel/perpanjangan-st-nonstan/dashboard') }}"> Dashboard </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'menunggu' ? 'active show' : '' }} " id="menunggu"
                                 role="tabpanel">
                                @if ($tab == 'menunggu')
                                    @include('tubel.perpanjanganstnonstan.perpanjanganstnonstan_tabel')
                                @endif
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'disetujui' ? 'active show' : '' }} " id="disetujui"
                                 role="tabpanel">
                                @if ($tab == 'disetujui')
                                    @include('tubel.perpanjanganstnonstan.perpanjanganstnonstan_tabel')
                                @endif
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'dikembalikan' ? 'active show' : '' }} "
                                 id="dikembalikan" role="tabpanel">
                                @if ($tab == 'dikembalikan')
                                    @include('tubel.perpanjanganstnonstan.perpanjanganstnonstan_tabel')
                                @endif
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'ditolak' ? 'active show' : '' }} " id="ditolak"
                                 role="tabpanel">
                                @if ($tab == 'ditolak')
                                    @include('tubel.perpanjanganstnonstan.perpanjanganstnonstan_tabel')
                                @endif
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'terbitst' ? 'active show' : '' }} " id="terbitst"
                                 role="tabpanel">
                                @if ($tab == 'terbitst')
                                    @include('tubel.perpanjanganstnonstan.perpanjanganstnonstan_tabel')
                                @endif
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'dashboard' ? 'active show' : '' }} " id="dashboard"
                                 role="tabpanel">
                                @if ($tab == 'dashboard')
                                    @include('tubel.perpanjanganstnonstan.perpanjanganstnonstan_tabel')
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('tubel.perpanjanganstnonstan.perpanjanganstnonstan_modal')
    @include('tubel.modal_tiket')

@endsection


@section('js')
    <script>
        let app_url = '{{ env('APP_URL') }}'
    </script>

    <script src="{{ asset('assets/js/tubel') }}/perpanjangan-st-nonstan.js"></script>
    <script src="{{ asset('assets/js/tubel') }}/log-tiket.js"></script>
@endsection
