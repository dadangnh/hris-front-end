<div class="row">
    <div class="col-lg-6 mb-4">
        <div class="text-start" data-kt-customer-table-toolbar="base">
            <button t?ype="button" class="btn btn-sm btn-ligth btn-active-primary border border-gray-400 fs-6"
                    data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" data-kt-menu-flip="top-end">
                <i class="fa fa-filter"></i> Advanced Search
            </button>
            <div class="menu menu-sub menu-sub-dropdown w-300px w-md-1000px border-warning mt-1" data-kt-menu="true">
                <div class="px-7 py-5">
                    <div class="fs-4 text-dark fw-bolder">Filter Pencarian</div>
                </div>
                <div class="separator border-gray-200"></div>
                <form id="advancedSearch" action="" method="get">
                    <div class="px-7 py-5">
                        <div class="row">
                            <div class="col-lg-6">
                                <label class="form-label fw-bold">Pegawai:</label>
                                <div class="row mb-4">
                                    <div class="col-lg-6">
                                        <input class="form-control form-select-solid" type="text" name="nama"
                                               placeholder="nama pegawai"
                                               value="{{ isset($_GET['nama']) ? $_GET['nama'] : '' }}">
                                    </div>
                                    <div class="col-lg-6">
                                        <input class="form-control form-select-solid" type="text" name="nip"
                                               placeholder="nip 18 digit"
                                               value="{{ isset($_GET['nip']) ? $_GET['nip'] : '' }}">
                                    </div>
                                </div>
                                <label class="form-label fw-bold">Jenjang Pendidikan:</label>
                                <div class="mb-4">
                                    <select id="search_jenjang" class="form-select" name="jenjang"
                                            data-control="select2" data-hide-search="true">
                                        <option></option>
                                        @if (isset($_GET['jenjang']))
                                            @foreach ($jenjang as $j)
                                                <option
                                                    value="{{ $j->id }}" {{ $_GET['jenjang'] == $j->id ? 'selected' : '' }}>{{ $j->jenjangPendidikan }}</option>
                                            @endforeach
                                        @else
                                            @foreach ($jenjang as $j)
                                                <option value="{{ $j->id }}">{{ $j->jenjangPendidikan }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <label class="form-label fw-bold">Lokasi Pendidikan:</label>
                                <div class="mb-4">
                                    <select id="search_lokasi" class="form-select" name="lokasi" data-control="select2"
                                            data-hide-search="false">
                                        <option></option>
                                        @foreach ($lokasi as $l)
                                            <option value="{{ $l->id }}">{{ $l->lokasi }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="row mb-4">
                                    <div class="col-lg-6">
                                        <label class="form-label fw-bold">KEP Penempatan:</label>
                                        <input class="form-control form-select-solid" type="text" name="kep"
                                               placeholder="kep penempatan"
                                               value="{{ isset($_GET['nama']) ? $_GET['nama'] : '' }}">
                                    </div>
                                    <div class="col-lg-6">
                                        <label class="form-label fw-bold">Tiket:</label>
                                        <input class="form-control form-select-solid" type="text" name="tiket"
                                               placeholder="tiket"
                                               value="{{ isset($_GET['nip']) ? $_GET['nip'] : '' }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-label fw-bold">Kantor Asal:</label>
                                <div class="row mb-4">
                                    <div class="col-lg-12">
                                        <select id="kantorAsal" name="kantor" class="form-select form-select-lg"
                                                data-kt-select2="true" data-placeholder="kantor asal"
                                                data-dropdown-parent="#advancedSearch">
                                            @if (isset($_GET['kantor']))
                                                <option value="{{ $tubel->kantorIdAsal }}|{{ $tubel->namaKantorAsal }}"
                                                        selected>{{ $tubel->namaKantorAsal }}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <label class="form-label fw-bold">Jabatan Asal:</label>
                                <div class="row mb-4">
                                    <div class="col-lg-12">
                                        <select id="jabatanAsal" name="jabatan" class="form-select form-select-lg"
                                                data-kt-select2="true" data-placeholder="jabatan asal"
                                                data-dropdown-parent="#advancedSearch">
                                            @if (isset($_GET['jabatan']))
                                                <option
                                                    value="{{ $tubel->jabatanIdAsal }}|{{ $tubel->namaJabatanAsal }}"
                                                    selected>{{ $tubel->namaJabatanAsal }}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <label class="form-label fw-bold">Tanggal Selesai Cuti:</label>
                                <div class="row mb-8">
                                    <div class="input-daterange input-group">
                                        <input type="text" class="form-control text-start date-search" name="tgl_mulai"
                                               placeholder="tgl selesai cuti (dari)"
                                               value="{{ isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : '' }}"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                s.d.
                                            </span>
                                        </div>
                                        <input type="text" class="form-control text-start date-search"
                                               name="tgl_selesai" placeholder="tgl selesai cuti (sampai)"
                                               value="{{ isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : '' }}"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <a href="{{ url()->current() }}"
                               class="btn btn-light btn-active-light-primary me-2">Reset</a>
                            <button type="submit" class="btn btn-sm btn-primary" data-kt-menu-dismiss="true"
                                    data-kt-customer-table-filter="filter">Search
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="table-responsive bg-gray-400 rounded border border-gray-300">
    <table class="table table-striped align-middle" id="tabel_keppenempatancuti">
        <thead class="fw-bolder bg-secondary fs-6">
        <tr class="">
            <th class="ps-4">No</th>
            <th>Pegawai</th>
            <th>Data Pendukung</th>
            <th>Tiket</th>
            <th class="text-center pe-4">Aksi</th>
        </tr>
        </thead>
        <tbody>
        @if ($penempatancuti->data != null)

            @php
                $nomor = $currentPage == 1 ? 1 : ($currentPage - 1) * $size + 1;
            @endphp

            @foreach ($penempatancuti->data as $t)
                <tr class="text-right">

                    <td class="fs-6 ps-4">
                        {{ $nomor++ }}
                    </td>

                    <td class="text-dark fw-bolder mb-1 fs-6 ps-4">
                        <a class="fw-bolder" href="{{ url('tubel/monitoring/' . $t->indukTubel->id) }}">
                            {{ $t->indukTubel->namaPegawai }}
                        </a>
                        <br>
                        <span class="fw-bold fs-6">{{ $t->indukTubel->nip18 }}</span>
                    </td>

                    <td class="text-dark fw-bolder mb-1 fs-6">
                        {{ $t->nomorSuratKampus }}
                        <br>
                        <span
                            class="fw-normal fs-6">{{ AppHelper::instance()->indonesian_date($t->tglSuratKampus, 'j F Y', '') }}</span>

                        <br>
                        {{ $t->nomorSuratSponsor }}
                        <br>
                        <span
                            class="fw-normal fs-6">{{ AppHelper::instance()->indonesian_date($t->tglSuratKepSponsor, 'j F Y', '') }}</span>
                    </td>

                    <td class="text-dark mb-1 fs-6 ">
                        <a href="javascript:void(0)" class="text-primary btnTiket"
                           data-tiket="{{ $t->logStatusProbis->nomorTiket }}"
                           data-nama="{{ $t->dataIndukTubelId->namaPegawai }}">
                            {{ $t->logStatusProbis->nomorTiket }}
                        </a>
                    </td>

                    @if ($tab == 'rekam_kep')

                        <td class="text-center text-dark fs-6  pe-4">
                            <button class="btn btn-sm btn-primary m-2 btnRekamKep" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Rekam"
                                    data-nama="{{ $t->indukTubel->namaPegawai }}"
                                    data-nip="{{ $t->indukTubel->nip18 }}"
                                    data-idcuti="{{ $t->id }}"
                                    data-alasancuti="{{$t->alasanCuti}}"
                                    data-tglmulai="{{AppHelper::instance()->indonesian_date($t->tglMulaiCuti,'j F Y','')}}"
                                    data-tglselesai="{{AppHelper::instance()->indonesian_date($t->tglSelesaiCuti,'j F Y','')}}"
                                    data-suratkampus="{{$t->nomorSuratKampus}}"
                                    data-tglkampus="{{AppHelper::instance()->indonesian_date($t->tglSuratKampus,'j F Y','')}}"
                                    data-suratsponsor="{{$t->nomorSuratSponsor}}"
                                    data-tglsponsor="{{AppHelper::instance()->indonesian_date($t->tglSuratKepSponsor,'j F Y','')}}"
                                    data-filesuratsponsor="{{$t->pathSuratIjinCutiSponsor}}"
                                    data-filesuratkampus="{{$t->pathSuratIjinCutiKampus}}"
                            >
                                <i class="fa fa-file"></i> Rekam KEP
                            </button>
                        </td>

                    @elseif($tab == 'ditempatkan')

                        <td class="text-dark fw-bolder mb-1 fs-6">
                            {{ $t->nomorSuratKep }}
                            <br>
                            <span class="fw-normal fs-6">( {{AppHelper::instance()->indonesian_date($t->tglSuratKep,'j F Y','')}})</span>
                            <br>
                            <span
                                class="fw-normal fs-6">TMT: {{AppHelper::instance()->indonesian_date($t->tglTmtPenempatan,'j F Y','')}}</span>
                        </td>

                        <td class="text-center text-darkfs-6 text-center pe-4">
                            <button class="btn btn-icon btn-sm btn-info m-2 btnLihatKep" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Lihat"
                                    data-nama="{{ $t->indukTubel->namaPegawai }}"
                                    data-nip="{{ $t->indukTubel->nip18 }}"
                                    data-idcuti="{{ $t->id }}"
                                    data-alasancuti="{{$t->alasanCuti}}"
                                    data-tglmulai="{{AppHelper::instance()->indonesian_date($t->tglMulaiCuti,'j F Y','')}}"
                                    data-tglselesai="{{AppHelper::instance()->indonesian_date($t->tglSelesaiCuti,'j F Y','')}}"
                                    data-nomorkep="{{$t->nomorSuratKep}}"
                                    data-tglkep="{{AppHelper::instance()->indonesian_date($t->tglSuratKep,'j F Y','')}}"
                                    data-tgltmt="{{AppHelper::instance()->indonesian_date($t->tglTmtPenempatan,'j F Y','')}}"
                                    data-lokasipenempatan="{{$t->namaKantorPenempatan}}"
                                    data-file="{{$t->pathKepPenempatan}}"
                            >
                                <i class="fa fa-eye"></i>
                            </button>
                        </td>
                    @endif

                </tr>
            @endforeach

        @else
            <tr class="text-center">
                <td class="text-dark fs-6" colspan="{{ $tab == 'ditempatkan' ? 7 : 6 }}">Tidak terdapat data</td>
            </tr>
        @endif

        </tbody>
    </table>

</div>

@if ($totalItems != 0)
    <!--begin::pagination-->
    <div class="d-flex flex-stack flex-wrap pt-5 p-3">
        <div class="fs-6 fw-bold text-gray-700"></div>

        @php
            $disabled = '';
            $nextPage = '';
            $disablednext = '';
            $hidden = '';
            $pagination = '';

            // tombol pagination
            if ($totalItems == 0) {
                // $pagination = 'none';
            }

            // tombol back
            if ($currentPage == 1) {
                $disabled = 'disabled';
            }

            // tombol next
            if ($currentPage != $totalPages) {
                $nextPage = $currentPage + 1;
            }

            // tombol pagination
            if ($currentPage == $totalPages || $totalPages == 0) {
                $disablednext = 'disabled';
                $hidden = 'hidden';
            }

        @endphp

            <!--begin::Pages-->
        <ul class="pagination" style="display: {{ $pagination }}">
            <li class="page-item disabled"><a href="#" class="page-link">Halaman {{ $currentPage }}
                    dari {{ $totalPages }}</a></li>
            <li class="page-item previous {{ $disabled }}"><a
                    href="{{ url()->current() . '?page=' . ($currentPage - 1) }}" class="page-link"><i
                        class="previous"></i></a></li>
            <li class="page-item active"><a href="{{ url()->current() . '?page=' . $currentPage }}"
                                            class="page-link">{{ $currentPage }}</a></li>
            <li class="page-item" {{ $hidden }}><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                   class="page-link">{{ $nextPage }}</a></li>
            <li class="page-item next {{ $disablednext }}"><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                              class="page-link"><i class="next"></i></a></li>
        </ul>
        <!--end::Pages-->

    </div>
    <!--end::paging-->
@endif
