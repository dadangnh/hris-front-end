@php
    $halaman = isset($_GET['page']) ? (int) $_GET['page'] : 1;
    $halaman_awal = $halaman > 1 ? $halaman * $size - $size : 0;
    $nomor = $halaman_awal + 1;
@endphp

<div class="row">
    <div class="col-lg-6 mb-4">
        <div class="text-start" data-kt-customer-table-toolbar="base">
            <button type="button" class="btn btn-sm btn-ligth btn-active-primary border border-gray-400 fs-6"
                    data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" data-kt-menu-flip="top-end">
                <i class="fa fa-filter"></i> Advanced Search
            </button>
            <div class="menu menu-sub menu-sub-dropdown w-md-1000px border-warning mt-1" data-kt-menu="true">
                <div class="px-7 py-5">
                    <div class="fs-4 text-dark fw-bolder">Filter Pencarian</div>
                </div>
                <div class="separator border-gray-200"></div>
                <form id="advancedSearch" action="" method="get">
                    <div class="px-7 py-5">
                        <div class="row">
                            <div class="col-lg-6">
                                <label class="form-label fw-bold">Pegawai:</label>
                                <div class="row mb-4">
                                    <div class="col-lg-6">
                                        <input class="form-control form-select-solid" type="text" name="nama"
                                               placeholder="nama pegawai"
                                               value="{{ isset($_GET['nama']) ? $_GET['nama'] : '' }}">
                                    </div>
                                    <div class="col-lg-6">
                                        <input class="form-control form-select-solid" type="text" name="nip"
                                               placeholder="nip 18 digit"
                                               value="{{ isset($_GET['nip']) ? $_GET['nip'] : '' }}">
                                    </div>
                                </div>
                                <label class="form-label fw-bold">Jenjang Pendidikan:</label>
                                <div class="mb-4">
                                    {{-- <label class="form-label fs-5 fw-bold">Jenjang Pendidikan</label> --}}
                                    <select id="search_jenjang" class="form-select" name="jenjang"
                                            data-control="select2" data-hide-search="true">
                                        <option></option>
                                        @if (isset($_GET['jenjang']))
                                            @foreach ($jenjang as $j)
                                                <option
                                                    value="{{ $j->id }}" {{ $_GET['jenjang'] == $j->id ? 'selected' : '' }}>{{ $j->jenjangPendidikan }}</option>
                                            @endforeach
                                        @else
                                            @foreach ($jenjang as $j)
                                                <option value="{{ $j->id }}">{{ $j->jenjangPendidikan }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <label class="form-label fw-bold">Lokasi Pendidikan:</label>
                                <div class="mb-4">
                                    <select id="search_lokasi" class="form-select" name="lokasi" data-control="select2"
                                            data-hide-search="false">
                                        <option></option>
                                        @if (isset($_GET['lokasi']))
                                            @foreach ($lokasi as $l)
                                                <option
                                                    value="{{ $l->id }}" {{ $_GET['lokasi'] == $l->id ? 'selected' : '' }}>{{ $l->lokasi }}</option>
                                            @endforeach
                                        @else
                                            @foreach ($lokasi as $l)
                                                <option value="{{ $l->id }}">{{ $l->lokasi }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-label fw-bold">Tiket:</label>
                                <div class="row mb-4">
                                    <div class="col-lg-12">
                                        <input class="form-control form-select-solid" type="text" name="tiket"
                                               placeholder="tiket"
                                               value="{{ isset($_GET['nip']) ? $_GET['nip'] : '' }}">
                                    </div>
                                </div>
                                <div class="row mb-8">
                                    <label class="form-label fw-bold">Tanggal Selesai Cuti:</label>
                                    <div class="input-daterange input-group">
                                        <input type="text" class="form-control text-start date-search" name="tgl_mulai"
                                               placeholder="tgl selesai cuti (dari)"
                                               value="{{ isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : '' }}"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                s.d.
                                            </span>
                                        </div>
                                        <input type="text" class="form-control text-start date-search"
                                               name="tgl_selesai" placeholder="tgl selesai cuti (sampai)"
                                               value="{{ isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : '' }}"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <a href="{{ url()->current() }}"
                               class="btn btn-light btn-active-light-primary me-2">Reset</a>
                            <button type="submit" class="btn btn-sm btn-primary" data-kt-menu-dismiss="true"
                                    data-kt-customer-table-filter="filter">Search
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @if ($tab == 'telah_disetujui')
        <div class="col">
            <div class="text-end mb-4">
                <button data-bs-toggle="modal" data-bs-target="#rekam_perpanjangan_cuti"
                        class="btn btn-primary btn-sm fs-6">
                    <i class="fa fa-plus"></i> Rekam Cuti
                </button>
            </div>
        </div>
    @endif

</div>


<div class="table-responsive bg-gray-400 rounded border border-gray-300">
    <table class="table table-striped align-middle" id="tabel_cuti">
        <thead class="fw-bolder bg-secondary fs-6">
        <tr>
            <th class="text-center ps-2">No</th>
            <th class="">Pegawai</th>
            <th class="">Pendidikan</th>
            <th class="">Surat ijin dari PT</th>
            <th class="">Surat ijin dari Sponsor</th>
            <th class="">Mulai / Selesai</th>
            <th class="">Tiket</th>
            <th class="text-center pe-2">Aksi</th>
        </tr>
        </thead>
        <tbody>
        @if ($perpanjangancuti->data != null)

            @foreach ($perpanjangancuti->data as $c)
                <tr class="">

                    <td class="text-center ps-2">
                        {{ $nomor++ }}
                    </td>

                    <td class="text-dark fs-6">
                        <a class="fw-bolder"
                           href="{{ url('tubel/monitoring/' . $c->indukTubel->id . '?tab=perpanjangan') }}">
                            {{ $c->indukTubel->namaPegawai }}
                        </a>
                        <br>
                        {{ $c->indukTubel->nip18 }}
                        <br>
                        @if ($c->flagByAdmin != null)
                            <span class="text-muted fw-bold fs-7">direkam oleh admin</span>
                        @endif
                    </td>

                    <td class="text-dark fw-bolder fs-6">
                        {{ $c->indukTubel->jenjangPendidikanId ? $c->indukTubel->jenjangPendidikanId->jenjangPendidikan : null }}
                        <br>
                        <span
                            class="fw-normal fs-6">{{ $c->indukTubel->lokasiPendidikanId ? $c->indukTubel->lokasiPendidikanId->lokasi : null }}</span>
                    </td>

                    <td class="text-dark fw-bolder fs-6">
                        {{ $c->nomorSuratKampus }}
                        <br>
                        <span
                            class="fw-normal fs-6">{{ AppHelper::instance()->indonesian_date($c->tglSuratKampus, 'j F Y', '') }}</span>
                    </td>

                    <td class="text-dark fw-bolder fs-6">
                        {{ $c->nomorSuratSponsor }}
                        <br>
                        <span
                            class="fw-normal fs-6">{{ AppHelper::instance()->indonesian_date($c->tglSuratKepSponsor, 'j F Y', '') }}</span>
                    </td>

                    <td class="text-dark fs-6">
                        {{ AppHelper::instance()->indonesian_date($c->tglMulaiCuti, 'j F Y', '') }} -
                        <br>
                        {{-- <span class="fw-bold d-block fs-6">s.d</span> --}}
                        {{ AppHelper::instance()->indonesian_date($c->tglSelesaiCuti, 'j F Y', '') }}
                    </td>

                    <td class="text-dark fs-6">
                        <a href="javascript:void(0)" class="text-primary btnTiket"
                           data-tiket="{{ $c->logStatusProbis->nomorTiket }}"
                           data-nama="{{ $c->indukTubel->namaPegawai }}">
                            {{ $c->logStatusProbis->nomorTiket }}
                        </a>
                    </td>

                    <td class="text-center pe-2">

                        @if ($tab == 'telah_disetujui' || $tab == 'ditolak')
                            <button class="btn btn-icon btn-sm btn-info btnLihat" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Lihat" data-idcuti="{{ $c->id }}"
                                    data-alasancuti="{{ $c->alasanCuti }}"
                                    data-tglmulai="{{ AppHelper::instance()->indonesian_date($c->tglMulaiCuti, 'j F Y', '') }}"
                                    data-tglselesai="{{ AppHelper::instance()->indonesian_date($c->tglSelesaiCuti, 'j F Y', '') }}"
                                    data-suratkampus="{{ $c->nomorSuratKampus }}"
                                    data-tglkampus="{{ AppHelper::instance()->indonesian_date($c->tglSuratKampus, 'j F Y', '') }}"
                                    data-suratsponsor="{{ $c->nomorSuratSponsor }}"
                                    data-tglsponsor="{{ AppHelper::instance()->indonesian_date($c->tglSuratKepSponsor, 'j F Y', '') }}"
                                    data-filesuratsponsor="{{ $c->pathSuratIjinCutiSponsor }}"
                                    data-filesuratkampus="{{ $c->pathSuratIjinCutiKampus }}"
                                    data-nama="{{ $c->indukTubel->namaPegawai }}" data-nip="{{ $c->indukTubel->nip18 }}"
                                    data-pangkat="{{ $c->indukTubel->namaPangkat }}"
                                    data-jabatan="{{ $c->indukTubel->namaJabatan }}"
                                    data-jenjang="{{ $c->indukTubel->jenjangPendidikanId->jenjangPendidikan }}"
                                    data-program_beasiswa="{{ $c->indukTubel->programBeasiswa }}"
                                    data-lokasi="{{ $c->indukTubel->lokasiPendidikanId->lokasi }}"
                                    {{-- data-pt="{{ $c->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }}" --}} {{-- data-prodi="{{ $c->ptPesertaTubelId->programStudiId->programStudi }}" --}} data-st_tubel="{{ $c->indukTubel->nomorSt }}"
                                    data-tgl_st_tubel="{{ AppHelper::instance()->indonesian_date($c->indukTubel->tglSt, 'j F Y', '') }}"
                                    data-tgl_mulai="{{ AppHelper::instance()->indonesian_date($c->indukTubel->tglMulaiSt, 'j F Y', '') }}"
                                    data-tgl_selesai="{{ AppHelper::instance()->indonesian_date($c->indukTubel->tglSelesaiSt, 'j F Y', '') }}"
                                    data-kep_pembebasan="{{ $c->indukTubel->nomorKepPembebasan }}"
                                    data-tgl_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($c->indukTubel->tglKepPembebasan, 'j F Y', '') }}"
                                    data-tmt_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($c->indukTubel->tmtKepPembebasan, 'j F Y', '') }}"
                                    data-email="{{ $c->indukTubel->emailNonPajak }}"
                                    data-hp="{{ $c->indukTubel->emailNonPajak }}"
                                    data-alamat="{{ $c->indukTubel->alamatTinggal }}">
                                <i class="fa fa-eye"></i>
                            </button>
                        @else
                            <button class="btn btn-sm btn-primary btnPeriksa" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Periksa" data-idcuti="{{ $c->id }}"
                                    data-alasancuti="{{ $c->alasanCuti }}"
                                    data-tglmulai="{{ AppHelper::instance()->indonesian_date($c->tglMulaiCuti, 'j F Y', '') }}"
                                    data-tglselesai="{{ AppHelper::instance()->indonesian_date($c->tglSelesaiCuti, 'j F Y', '') }}"
                                    data-suratkampus="{{ $c->nomorSuratKampus }}"
                                    data-tglkampus="{{ AppHelper::instance()->indonesian_date($c->tglSuratKampus, 'j F Y', '') }}"
                                    data-suratsponsor="{{ $c->nomorSuratSponsor }}"
                                    data-tglsponsor="{{ AppHelper::instance()->indonesian_date($c->tglSuratKepSponsor, 'j F Y', '') }}"
                                    data-filesuratsponsor="{{ $c->pathSuratIjinCutiSponsor }}"
                                    data-filesuratkampus="{{ $c->pathSuratIjinCutiKampus }}"
                                    data-nama="{{ $c->indukTubel->namaPegawai }}" data-nip="{{ $c->indukTubel->nip18 }}"
                                    data-pangkat="{{ $c->indukTubel->namaPangkat }}"
                                    data-jabatan="{{ $c->indukTubel->namaJabatan }}"
                                    data-jenjang="{{ $c->indukTubel->jenjangPendidikanId->jenjangPendidikan }}"
                                    data-program_beasiswa="{{ $c->indukTubel->programBeasiswa }}"
                                    data-lokasi="{{ $c->indukTubel->lokasiPendidikanId->lokasi }}"
                                    {{-- data-pt="{{ $c->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }}" --}} {{-- data-prodi="{{ $c->ptPesertaTubelId->programStudiId->programStudi }}" --}} data-st_tubel="{{ $c->indukTubel->nomorSt }}"
                                    data-tgl_st_tubel="{{ AppHelper::instance()->indonesian_date($c->indukTubel->tglSt, 'j F Y', '') }}"
                                    data-tgl_mulai="{{ AppHelper::instance()->indonesian_date($c->indukTubel->tglMulaiSt, 'j F Y', '') }}"
                                    data-tgl_selesai="{{ AppHelper::instance()->indonesian_date($c->indukTubel->tglSelesaiSt, 'j F Y', '') }}"
                                    data-kep_pembebasan="{{ $c->indukTubel->nomorKepPembebasan }}"
                                    data-tgl_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($c->indukTubel->tglKepPembebasan, 'j F Y', '') }}"
                                    data-tmt_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($c->indukTubel->tmtKepPembebasan, 'j F Y', '') }}"
                                    data-email="{{ $c->indukTubel->emailNonPajak }}"
                                    data-hp="{{ $c->indukTubel->emailNonPajak }}"
                                    data-alamat="{{ $c->indukTubel->alamatTinggal }}">
                                <i class="fa fa-cog"></i>Periksa
                            </button>
                        @endif

                        @if ($c->flagByAdmin == 1)
                            <button class="btn btn-icon btn-sm btn-warning btnEdit" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Edit" data-idtubel="{{ $c->indukTubel->id }}"
                                    data-id_cuti="{{ $c->id }}"
                                    data-alasancuti="{{ $c->alasanCuti }}" data-tglmulai="{{ $c->tglMulaiCuti }}"
                                    data-tglselesai="{{ $c->tglSelesaiCuti }}"
                                    data-suratkampus="{{ $c->nomorSuratKampus }}"
                                    data-tglkampus="{{ $c->tglSuratKampus }}"
                                    data-suratsponsor="{{ $c->nomorSuratSponsor }}"
                                    data-tglsponsor="{{ $c->tglSuratKepSponsor }}"
                                    data-filesuratsponsor="{{ $c->pathSuratIjinCutiSponsor }}"
                                    data-filesuratkampus="{{ $c->pathSuratIjinCutiKampus }}"
                                    data-nama="{{ $c->indukTubel->namaPegawai }}"
                                    data-nip="{{ $c->indukTubel->nip18 }}"
                                    data-pangkat="{{ $c->indukTubel->namaPangkat }}"
                                    data-jabatan="{{ $c->indukTubel->namaJabatan }}"
                                    data-jenjang="{{ $c->indukTubel->jenjangPendidikanId->jenjangPendidikan }}"
                                    data-program_beasiswa="{{ $c->indukTubel->programBeasiswa }}"
                                    data-lokasi="{{ $c->indukTubel->lokasiPendidikanId->lokasi }}"
                                    {{-- data-pt="{{ $c->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }}" --}} {{-- data-prodi="{{ $c->ptPesertaTubelId->programStudiId->programStudi }}" --}} data-st_tubel="{{ $c->indukTubel->nomorSt }}"
                                    data-tgl_st_tubel="{{ AppHelper::instance()->indonesian_date($c->indukTubel->tglSt, 'j F Y', '') }}"
                                    data-tgl_mulai="{{ AppHelper::instance()->indonesian_date($c->indukTubel->tglMulaiSt, 'j F Y', '') }}"
                                    data-tgl_selesai="{{ AppHelper::instance()->indonesian_date($c->indukTubel->tglSelesaiSt, 'j F Y', '') }}"
                                    data-kep_pembebasan="{{ $c->indukTubel->nomorKepPembebasan }}"
                                    data-tgl_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($c->indukTubel->tglKepPembebasan, 'j F Y', '') }}"
                                    data-tmt_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($c->indukTubel->tmtKepPembebasan, 'j F Y', '') }}"
                                    data-email="{{ $c->indukTubel->emailNonPajak }}"
                                    data-hp="{{ $c->indukTubel->emailNonPajak }}"
                                    data-alamat="{{ $c->indukTubel->alamatTinggal }}">
                                <i class="fa fa-edit"></i>
                            </button>

                            <button class="btn btn-icon btn-sm btn-danger btnHapus" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Hapus" data-id="{{ $c->id }}">
                                <i class="fa fa-trash"></i>
                            </button>
                        @endif

                    </td>
                </tr>
            @endforeach

        @else

            <tr class="text-center">
                <td class="text-dark  mb-1 fs-6" colspan="8">Tidak terdapat data</td>
            </tr>

        @endif

        </tbody>
    </table>

    @if ($perpanjangancuti->data != null)
        <div class="d-flex flex-stack flex-wrap pt-5 p-3">
            <div class="fs-6 fw-bold text-gray-700"></div>
        </div>
    @endif
</div>

@if ($totalItems != 0)
    <!--begin::pagination-->
    <div class="d-flex flex-stack flex-wrap pt-5 p-3">
        <div class="fs-6 fw-bold text-gray-700"></div>

        @php
            $disabled = '';
            $nextPage = '';
            $disablednext = '';
            $hidden = '';
            $pagination = '';

            // tombol pagination
            if ($totalItems == 0) {
                // $pagination = 'none';
            }

            // tombol back
            if ($currentPage == 1) {
                $disabled = 'disabled';
            }

            // tombol next
            if ($currentPage != $totalPages) {
                $nextPage = $currentPage + 1;
            }

            // tombol pagination
            if ($currentPage == $totalPages || $totalPages == 0) {
                $disablednext = 'disabled';
                $hidden = 'hidden';
            }

        @endphp

            <!--begin::Pages-->
        <ul class="pagination" style="display: {{ $pagination }}">
            <li class="page-item disabled"><a href="#" class="page-link">Halaman {{ $currentPage }}
                    dari {{ $totalPages }}</a></li>
            <li class="page-item previous {{ $disabled }}"><a
                    href="{{ url()->current() . '?page=' . ($currentPage - 1) }}" class="page-link"><i
                        class="previous"></i></a></li>
            <li class="page-item active"><a href="{{ url()->current() . '?page=' . $currentPage }}"
                                            class="page-link">{{ $currentPage }}</a></li>
            <li class="page-item" {{ $hidden }}><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                   class="page-link">{{ $nextPage }}</a></li>
            <li class="page-item next {{ $disablednext }}"><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                              class="page-link"><i class="next"></i></a></li>
        </ul>
        <!--end::Pages-->

    </div>
    <!--end::paging-->
@endif
