<div class="modal fade" id="lihat_perpanjangan_cuti" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content rounded">
            <div class="modal-header">
                <h4 class="text-white">Form Perpanjangan Cuti Belajar</h4>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body scroll-y px-5 px-lg-10">
                <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-4">
                    <div class="row mb-2">
                        <div class="col-md col-lg">
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Nama</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="lihat_nama_setujui" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">NIP 18</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="lihat_nip_setujui" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Pangkat</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="lihat_pangkat_setujui" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row mb-6">
                                <label class="col-lg-4 col-md fw-bold fs-6">Jabatan</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="lihat_jabatan_setujui" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Jenjang Pendidikan</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="lihat_jenjang_setujui" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Program Beasiswa</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="lihat_program_beasiswa_setujui" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Lokasi Pendidikan</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="lihat_lokasi_setujui" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            {{-- <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Perguruan Tinggi</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="lihat_pt_setujui" class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Program Studi</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="lihat_prodi_setujui" class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                    </div>
                                </div> --}}
                        </div>
                        <div class="col-md col-lg">
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">ST Tubel</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="lihat_st_tubel_setujui"
                                          class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Tgl ST</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="lihat_tgl_st_tubel_setujui"
                                          class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Tgl Mulai & Selesai</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="lihat_tgl_mulai_selesai_setujui"
                                          class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">KEP Pembebasan</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="lihat_kep_pembebasan_setujui"
                                          class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Tgl KEP</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="lihat_tgl_kep_pembebasan_setujui"
                                          class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row mb-6">
                                <label class="col-lg-4 col-md fw-bold fs-6">TMT KEP</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="lihat_tmt_kep_pembebasan_setujui"
                                          class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Email</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="lihat_email_setujui" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Nomor HP</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="lihat_hp_setujui" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Alamat</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="lihat_alamat_setujui" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Mulai / selesai Cuti</label>
                        <div class="col-lg">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active" id="tgl_mulai_lihat"
                                       placeholder="" name="tglMulaiSt" type="text" value="" disabled>
                            </div>
                        </div>
                        <div class="col-lg">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active" id="tgl_selesai_lihat"
                                       placeholder="" name="tglSelesaiSt" type="text" value="" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Cuti</label>
                        <div class="col-lg">
                            <textarea class="form-control" id="alasan_cuti_lihat" type="text" name="alasan_cuti_lihat"
                                      rows="4" placeholder="" required disabled></textarea>
                        </div>
                    </div>

                    <br>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Kampus)</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" name="surat_izin_cuti_lihat" rows="4"
                                   id="surat_izin_cuti_kampus" placeholder="" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                        <div class="col-lg">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active" id="tgl_surat_kampus_lihat"
                                       placeholder="" name="tglSurat" type="text" value="" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3 mt-2">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                        <div class="col-lg">
                            <div class="d-flex flex-aligns-center mt-2 pe-10">
                                <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1">
                                    <a href="#" id="file_surat_kampus_lihat" target="_blank"
                                       class="fs-6 text-hover-primary">Surat Izin Cuti (Kampus).pdf</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Sponsor)</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" name="surat_izin_cuti_sponsor_lihat"
                                   id="surat_izin_cuti_sponsor_lihat" placeholder="" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                        <div class="col-lg">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active" id="tgl_surat_sponsor_lihat"
                                       placeholder="" name="tglSuratSponsor" type="text" value="" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3 mt-2">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                        <div class="col-lg">
                            <div class="d-flex flex-aligns-center mt-2 pe-10">
                                <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1">
                                    <a href="#" id="file_surat_sponsor_lihat" target="_blank"
                                       class="fs-6 text-hover-primary">Surat Izin Cuti (Sponsor).pdf</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="periksa_perpanjangan_cuti" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content rounded">
            <div class="modal-header">
                <h4 class="text-white">Form Penelitian Perpanjangan Cuti Belajar</h4>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form" method="post" action="">
                @csrf
                <input type="hidden" name="idTubel" value="">
                <input type="hidden" id="idCuti" name="idCuti" value="">

                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-4">
                        <div class="row mb-2">
                            <div class="col-md col-lg">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Nama</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="get_nama_setujui" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">NIP 18</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="get_nip_setujui" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Pangkat</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="get_pangkat_setujui"
                                              class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row mb-6">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Jabatan</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="get_jabatan_setujui"
                                              class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Jenjang Pendidikan</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="get_jenjang_setujui"
                                              class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Program Beasiswa</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="get_program_beasiswa_setujui"
                                              class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Lokasi Pendidikan</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="get_lokasi_setujui"
                                              class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                {{-- <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Perguruan Tinggi</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_pt_setujui" class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Program Studi</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_prodi_setujui" class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                        </div>
                                    </div> --}}
                            </div>
                            <div class="col-md col-lg">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">ST Tubel</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="get_st_tubel_setujui"
                                              class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Tgl ST</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="get_tgl_st_tubel_setujui"
                                              class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Tgl Mulai & Selesai</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="get_tgl_mulai_selesai_setujui"
                                              class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">KEP Pembebasan</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="get_kep_pembebasan_setujui" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Tgl KEP</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="get_tgl_kep_pembebasan_setujui"
                                              class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row mb-6">
                                    <label class="col-lg-4 col-md fw-bold fs-6">TMT KEP</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="get_tmt_kep_pembebasan_setujui"
                                              class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Email</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="get_email_setujui" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Nomor HP</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="get_hp_setujui" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Alamat</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="get_alamat_setujui"
                                              class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Mulai / selesai Cuti</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active" id="tglMulai"
                                           placeholder="" name="tglMulaiSt" type="text" value="" disabled>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active" id="tglSelesai"
                                           placeholder="" name="tglSelesaiSt" type="text" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Cuti</label>
                            <div class="col-lg">
                                <textarea class="form-control" id="alasan_cuti" type="text" name="alasan_cuti" rows="4"
                                          placeholder="" required disabled></textarea>
                            </div>
                        </div>

                        <br>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Kampus)</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" name="surat_izin_cuti" rows="4"
                                       id="surat_izin_cuti_kampus_periksa" placeholder="" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active" id="tglSuratKampus"
                                           placeholder="" name="tglSurat" type="text" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3 mt-2">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                            <div class="col-lg">
                                <div class="d-flex flex-aligns-center mt-2 pe-10">
                                    <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                    <div class="ms-1">
                                        <a href="#" id="filesuratkampus" target="_blank"
                                           class="fs-6 text-hover-primary">Surat Izin Cuti (Kampus).pdf</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Sponsor)</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" name="surat_izin_cuti_sponsor"
                                       id="surat_izin_cuti_sponsor" placeholder="" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active" id="tglSuratSponsor"
                                           placeholder="" name="tglSuratSponsor" type="text" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3 mt-2">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                            <div class="col-lg">
                                <div class="d-flex flex-aligns-center mt-2 pe-10">
                                    <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                    <div class="ms-1">
                                        <a href="#" id="filesuratsponsor" target="_blank"
                                           class="fs-6 text-hover-primary">Surat Izin Cuti (Sponsor).pdf</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="text-center mb-4">
                        <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                class="fa fa-reply"></i>Batal
                        </button>
                        <button type="button" id="btnTolakCuti" class="btn btn-sm btn-danger"
                                data-act="27bfb1f8-1482-4d80-a040-24f00bddb4e9"><i class="fa fa-times"></i>Tolak
                        </button>
                        <button type="button" id="btnSetujuCuti" class="btn btn-sm btn-success"
                                data-act="79a044cf-9d66-438b-ba31-dfa62ce97e2f"><i class="fa fa-check-circle"></i>Setuju
                        </button>
                    </div>

                </div>

            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="rekam_perpanjangan_cuti" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content rounded">
            <div class="modal-header">
                <h4 class="text-white">Form Perekaman Perpanjangan Cuti Belajar (by Admin)</h4>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form" id="" method="post" enctype="multipart/form-data"
                  action="{{ url('tubel/perpanjangancuti/admin') }}">
                @csrf
                <input id="idTubel" type="hidden" name="idTubel" value="">

                <div class="modal-body scroll-y">
                    <div class="row mb-3">
                        <div class="col-lg-2"></div>
                        <div class="col-lg">
                            <input class="form-control" type="text" name="tiket" id="tiket"
                                   placeholder="nomor tiket cuti pertama">
                        </div>
                        <div class="form-label col-lg-3">
                            <button id="btnGetCuti" class="btn btn-primary">
                                <i class="fa fa-search"></i> Cari
                            </button>
                            <button class="btn btn-primary btn-sm fs-6" type="button" hidden disabled>
                                Loading
                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            </button>
                        </div>
                    </div>
                    <div id="formRekamCutiAdmin" style="display: none">
                        <div class="rounded border border-gray-300 p-4 border-gray-300 d-flex flex-column mb-6">
                            <div class="row mb-2">
                                <div class="col-md col-lg">
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Nama</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_nama" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">NIP 18</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_nip" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Pangkat</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_pangkat" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-6">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Jabatan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_jabatan" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Jenjang Pendidikan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_jenjang" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Program Beasiswa</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_program_beasiswa"
                                                  class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Lokasi Pendidikan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_lokasi" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    {{-- <div class="form-group row">
                                            <label class="col-lg-4 col-md fw-bold fs-6">Perguruan Tinggi</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span id="get_pt" class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-md fw-bold fs-6">Program Studi</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span id="get_prodi" class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                            </div>
                                        </div> --}}
                                </div>
                                <div class="col-md col-lg">
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">ST Tubel</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_st_tubel" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Tgl ST</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_tgl_st_tubel"
                                                  class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Tgl Mulai & Selesai</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_tgl_mulai_selesai"
                                                  class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">KEP Pembebasan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_kep_pembebasan"
                                                  class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Tgl KEP</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_tgl_kep_pembebasan" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-6">
                                        <label class="col-lg-4 col-md fw-bold fs-6">TMT KEP</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_tmt_kep_pembebasan" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Email</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_email" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Nomor HP</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_hp" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Alamat</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_alamat" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Mulai / selesai Cuti</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active datepick"
                                           id="tgl_mulai_cuti_perpanjangan_rekam" placeholder="Pilih tanggal mulai"
                                           name="tgl_mulai_cuti_perpanjangan_rekam" type="text" value="">
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active datepick"
                                           id="tgl_selesai_cuti_perpanjangan_rekam" placeholder="Pilih tanggal selesai"
                                           name="tgl_selesai_cuti_perpanjangan_rekam" type="text"
                                           value="">
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Cuti</label>
                            <div class="col-lg">
                                <textarea class="form-control" id="alasan_cuti_perpanjangan_rekam" type="text"
                                          name="alasan_cuti_perpanjangan_rekam" rows="4"
                                          placeholder="alasan pengajuan cuti belajar" required></textarea>
                            </div>
                        </div>
                        <br>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Kampus)</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" name="surat_izin_cuti_kampus_perpanjangan_rekam"
                                       rows="4" id="surat_izin_cuti_kampus_perpanjangan_rekam"
                                       placeholder="surat izin cuti">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active datepick"
                                           id="tgl_surat_kampus_perpanjangan_rekam" placeholder="Pilih tanggal"
                                           name="tgl_surat_kampus_perpanjangan_rekam" type="text" value="">
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Unggah Surat</label>
                            <div class="col-lg">
                                <input class="form-control" type="file" name="file_cuti_perpanjangan_rekam">
                            </div>
                        </div>
                        <br>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Sponsor)</label>
                            <div class="col-lg">
                                <input class="form-control" type="text"
                                       name="surat_izin_cuti_sponsor_perpanjangan_rekam"
                                       id="surat_izin_cuti_sponsor_perpanjangan_rekam"
                                       placeholder="surat izin cuti (sponsor)">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active datepick"
                                           id="tgl_surat_sponsor_perpanjangan_rekam" placeholder="Pilih tanggal"
                                           name="tgl_surat_sponsor_perpanjangan_rekam" type="text" value="">
                                </div>
                            </div>
                        </div>
                        <div class="row mb-12">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Unggah Surat</label>
                            <div class="col-lg">
                                <input class="form-control" type="file" name="file_sponsors_perpanjangan_rekam"
                                       required>
                            </div>
                        </div>

                        <div class="text-center mb-3">
                            <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                    class="fa fa-reply"></i>Batal
                            </button>
                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="edit_perpanjangan_cuti" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content rounded">
            <div class="modal-header">
                <h4 class="text-white">Form Perekaman Perpanjangan Cuti Belajar (by Admin)</h4>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form" id="" method="post" enctype="multipart/form-data"
                  action="{{ url('tubel/perpanjangancuti/update') }}">
                @csrf
                <input id="idTubelEdit" type="hidden" name="idTubel" value="">
                <input id="id_cuti" type="hidden" name="id_cuti" value="">

                <div class="modal-body scroll-y">
                    <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                        <div class="row mb-2">
                            <div class="col-md col-lg">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Nama</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="nama_edit" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">NIP 18</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="nip_edit" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Pangkat</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="pangkat_edit" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row mb-2">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Jabatan</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="jabatan_edit" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Jenjang Pendidikan</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="jenjang_edit" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Program Beasiswa</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="program_beasiswa_edit"
                                              class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Lokasi Pendidikan</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="lokasi_edit" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                {{-- <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Perguruan Tinggi</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="pt_edit" class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Program Studi</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="prodi_edit" class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                        </div>
                                    </div> --}}
                            </div>
                            <div class="col-md col-lg">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">ST Tubel</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="st_tubel_edit" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Tgl ST</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="tgl_st_tubel_edit" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Tgl Mulai & Selesai</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="tgl_mulai_selesai_edit"
                                              class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">KEP Pembebasan</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="kep_pembebasan_edit"
                                              class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Tgl KEP</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="tgl_kep_pembebasan_edit"
                                              class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row mb-2">
                                    <label class="col-lg-4 col-md fw-bold fs-6">TMT KEP</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="tmt_kep_pembebasan_edit"
                                              class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Email</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="email_edit" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Nomor HP</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="hp_edit" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Alamat</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span id="alamat_edit" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Mulai / selesai Cuti</label>
                        <div class="col-lg">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active"
                                       id="tgl_mulai_cuti_perpanjangan_edit" placeholder="Pilih tanggal mulai"
                                       name="tgl_mulai_cuti_perpanjangan_edit" type="text" value="">
                            </div>
                        </div>
                        <div class="col-lg">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active"
                                       id="tgl_selesai_cuti_perpanjangan_edit" placeholder="Pilih tanggal selesai"
                                       name="tgl_selesai_cuti_perpanjangan_edit" type="text" value="">
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Cuti</label>
                        <div class="col-lg">
                            <textarea class="form-control" id="alasan_cuti_perpanjangan_edit" type="text"
                                      name="alasan_cuti_perpanjangan_edit" rows="4"
                                      placeholder="alasan pengajuan cuti belajar" required></textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Kampus)</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" name="surat_izin_cuti_kampus_perpanjangan_edit"
                                   rows="4" id="surat_izin_cuti_kampus_perpanjangan_edit" placeholder="surat izin cuti">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                        <div class="col-lg">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active"
                                       id="tgl_surat_kampus_perpanjangan_edit" placeholder="Pilih tanggal"
                                       name="tgl_surat_kampus_perpanjangan_edit" type="text" value="">
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Upload File</label>
                        <div class="col-lg">
                            <input type="hidden" id="file_cuti_kampus_edit" name="file_cuti_perpanjangan_edit_lama">
                            <a href="" target="_blank" id="file_cuti_kampus" class="form-control-plaintext"><i
                                    class="fa fa-file"></i> Surat Izin Cuti (Sponsor).pdf</a>
                            <input class="form-control" type="file" name="file_cuti_perpanjangan_edit">
                        </div>
                    </div>
                    <br>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Sponsor)</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" name="surat_izin_cuti_sponsor_perpanjangan_edit"
                                   id="surat_izin_cuti_sponsor_perpanjangan_edit"
                                   placeholder="surat izin cuti (sponsor)">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                        <div class="col-lg">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active"
                                       id="tgl_surat_sponsor_perpanjangan_edit" placeholder="Pilih tanggal"
                                       name="tgl_surat_sponsor_perpanjangan_edit" type="text" value="">
                            </div>
                        </div>
                    </div>
                    <div class="row mb-12">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Upload File</label>
                        <div class="col-lg">
                            <input type="hidden" id="file_cuti_sponsor_edit"
                                   name="file_sponsors_perpanjangan_edit_lama">
                            <a href="" target="_blank" id="file_cuti_sponsor" class="form-control-plaintext"><i
                                    class="fa fa-file"></i> Surat Izin Cuti (Sponsor).pdf</a>
                            <input class="form-control" type="file" name="file_sponsors_perpanjangan_edit">
                        </div>
                    </div>

                    <div class="text-center mb-3">
                        <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                class="fa fa-reply"></i>Batal
                        </button>
                        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
