@extends('layout.master')

@section('content')

    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card border-warning">
                    <div class="card-header card-header-stretch" id="card-header">
                        <div class="card-toolbar">
                            <ul class="nav nav-tabs nav-line-tabs nav-stretch border-transparent fs-5 fw-bolder"
                                id="tabLps">
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'menunggu_persetujuan' ? 'active' : '' }}"
                                       href="{{ url('tubel/lps') }}"> Menunggu Persetujuan </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'telah_disetujui' ? 'active' : '' }}"
                                       href="{{ url('tubel/lps/setuju') }}"> Telah Disetujui </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'ditolak' ? 'active' : '' }}"
                                       href="{{ url('tubel/lps/tolak') }}"> Ditolak </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'dashboard' ? 'active' : '' }}"
                                       href="{{ url('tubel/lps/dashboard') }}"> Dashboard </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6 mb-4">
                                <div class="text-start" data-kt-customer-table-toolbar="base">
                                    <button type="button"
                                            class="btn btn-sm btn-ligth btn-active-primary border border-gray-400 fs-6"
                                            data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start"
                                            data-kt-menu-flip="top-end">
                                        <i class="fa fa-filter"></i> Advanced Search
                                    </button>
                                    <div class="menu menu-sub menu-sub-dropdown w-md-1000px border-warning mt-1"
                                         data-kt-menu="true">
                                        <div class="px-7 py-5">
                                            <div class="fs-4 text-dark fw-bolder">Filter Pencarian</div>
                                        </div>
                                        <div class="separator border-gray-200"></div>
                                        <form id="advancedSearch" action="" method="get">
                                            <div class="px-7 py-5">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="row mb-4">
                                                            <label class="form-label fw-bold">Pegawai:</label>
                                                            <div class="col-lg-6">
                                                                <input class="form-control form-select-solid"
                                                                       type="text" name="nama"
                                                                       placeholder="nama pegawai"
                                                                       value="{{ isset($_GET['nama']) ? $_GET['nama'] : '' }}">
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <input class="form-control form-select-solid"
                                                                       type="text" name="nip" placeholder="nip 18 digit"
                                                                       value="{{ isset($_GET['nip']) ? $_GET['nip'] : '' }}">
                                                            </div>
                                                        </div>
                                                        <div class="mb-4">
                                                            <label class="form-label fw-bold">Jenjang Pendidikan
                                                                :</label>
                                                            <select id="search_jenjang" class="form-select"
                                                                    name="jenjang" data-control="select2"
                                                                    data-hide-search="true">
                                                                <option></option>
                                                                @if (isset($_GET['jenjang']))
                                                                    @foreach ($jenjang as $j)
                                                                        <option
                                                                            value="{{ $j->id }}" {{ $_GET['jenjang'] == $j->id ? 'selected' : '' }}>{{ $j->jenjangPendidikan }}</option>
                                                                    @endforeach
                                                                @else
                                                                    @foreach ($jenjang as $j)
                                                                        <option
                                                                            value="{{ $j->id }}">{{ $j->jenjangPendidikan }}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <div class="mb-4">
                                                            <label class="form-label fw-bold">Lokasi Pendidikan:</label>
                                                            <select id="search_lokasi" class="form-select" name="lokasi"
                                                                    data-control="select2" data-hide-search="false">
                                                                <option></option>
                                                                @if (isset($_GET['lokasi']))
                                                                    @foreach ($lokasi as $l)
                                                                        <option
                                                                            value="{{ $l->id }}" {{ $_GET['lokasi'] == $l->id ? 'selected' : '' }}>{{ $l->lokasi }}</option>
                                                                    @endforeach
                                                                @else
                                                                    @foreach ($lokasi as $l)
                                                                        <option
                                                                            value="{{ $l->id }}">{{ $l->lokasi }}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="row mb-4">
                                                            <div class="col-lg-6">
                                                                <label class="form-label fs-5 fw-bold">Tahun
                                                                    Akademik</label>
                                                                <div class="d-flex flex-column">
                                                                    <div
                                                                        class="position-relative d-flex align-items-center">
                                                                        {{-- <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                                            <span class="symbol-label bg-secondary">
                                                                                <span class="svg-icon">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                                            <path
                                                                                                d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                                                fill="#000000"></path>
                                                                                        </g>
                                                                                    </svg>
                                                                                </span>
                                                                            </span>
                                                                        </div> --}}
                                                                        <input id="search_tahun"
                                                                               class="form-control text-start"
                                                                               placeholder="tahun laporan" name="tahun"
                                                                               type="text">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <label class="form-label fs-5 fw-bold">Tiket</label>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="tiket"
                                                                           placeholder="tiket"
                                                                           value="{{ isset($_GET['tiket']) ? $_GET['tiket'] : '' }}"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mb-8">

                                                            @php
                                                                $check = '';
                                                                $value = null;
                                                                $text = 'checked="checked"';
                                                                if (isset($_GET['semester'])) {
                                                                    if ($_GET['semester'] == 1) {
                                                                        $value = 1;
                                                                        $text = 'checked="checked"';
                                                                    } elseif ($_GET['semester'] == 2) {
                                                                        $value = 2;
                                                                        $text = 'checked="checked"';
                                                                    } elseif ($_GET['semester'] == null) {
                                                                        $value = null;
                                                                        $text = 'checked="checked"';
                                                                    }
                                                                }
                                                            @endphp

                                                            <label class="form-label fs-5 fw-bold mb-3">Semester</label>
                                                            <div class="d-flex flex-column flex-wrap fw-bold"
                                                                 data-kt-customer-table-filter="payment_type">
                                                                <label
                                                                    class="form-check form-check-sm form-check-custom form-check-solid mb-3 me-5">
                                                                    <input class="form-check-input" type="radio"
                                                                           name="semester"
                                                                           value="" {{ $value == null ? $text : '' }} />
                                                                    <span
                                                                        class="form-check-label text-gray-600">Semua</span>
                                                                </label>
                                                                <label
                                                                    class="form-check form-check-sm form-check-custom form-check-solid mb-3 me-5">
                                                                    <input class="form-check-input" type="radio"
                                                                           name="semester"
                                                                           value="1" {{ $value == 1 ? $text : '' }} />
                                                                    <span
                                                                        class="form-check-label text-gray-600">Ganjil</span>
                                                                </label>
                                                                <label
                                                                    class="form-check form-check-sm form-check-custom form-check-solid">
                                                                    <input class="form-check-input" type="radio"
                                                                           name="semester"
                                                                           value="2" {{ $value == 2 ? $text : '' }} />
                                                                    <span
                                                                        class="form-check-label text-gray-600">Genap</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-end">
                                                    <a href="{{ url()->current() }}"
                                                       class="btn btn-light btn-active-light-primary me-2">Reset</a>
                                                    <button type="submit" class="btn btn-sm btn-primary"
                                                            data-kt-menu-dismiss="true"
                                                            data-kt-customer-table-filter="filter">Search
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'menunggu_persetujuan' ? 'active show' : '' }}"
                                 id="menunggu_persetujuan" role="tabpanel">
                                @if ($tab == 'menunggu_persetujuan')
                                    @include('tubel.lps.lps_tabel')
                                @endif
                            </div>

                            <div class="tab-pane fade {{ $tab == 'telah_disetujui' ? 'active show' : '' }}"
                                 id="telah_disetujui" role="tabpanel">
                                @if ($tab == 'telah_disetujui')
                                    @include('tubel.lps.lps_tabel')
                                @endif
                            </div>

                            <div class="tab-pane fade {{ $tab == 'ditolak' ? 'active show' : '' }}" id="ditolak"
                                 role="tabpanel">
                                @if ($tab == 'ditolak')
                                    @include('tubel.lps.lps_tabel')
                                @endif
                            </div>

                            <div class="tab-pane fade {{ $tab == 'dashboard' ? 'active show' : '' }}" id="dashboard"
                                 role="tabpanel">
                                @if ($tab == 'dashboard')
                                    @include('tubel.lps.lps_tabel')
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="periksa_lps" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <div class="modal-title">
                        <h3 class="text-white">{{ $tab == 'menunggu_persetujuan' ? 'Form' : '' }} Laporan Perkembangan
                            Studi</h3>
                    </div>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                 width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                                   fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                    <rect fill="#000000" opacity="0.5"
                                          transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                          x="0" y="7" width="16" height="2" rx="1"/>
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <form action="" method="post">
                    @csrf

                    <div class="modal-body scroll-y px-5 px-lg-10">
                        <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                            <div class="row mb-2">
                                <div class="col-md col-lg">
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Nama</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_nama" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">NIP 18</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_nip" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Pangkat</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_pangkat" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-6">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Jabatan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_jabatan" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Jenjang Pendidikan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_jenjang" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Program Beasiswa</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_program_beasiswa"
                                                  class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Lokasi Pendidikan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_lokasi" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Perguruan Tinggi</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_pt" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Program Studi</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_prodi" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md col-lg">
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">ST Tubel</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_st_tubel" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Tgl ST</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_tgl_st_tubel"
                                                  class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Tgl Mulai & Selesai</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_tgl_mulai_selesai"
                                                  class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">KEP Pembebasan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_kep_pembebasan"
                                                  class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Tgl KEP</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_tgl_kep_pembebasan" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-6">
                                        <label class="col-lg-4 col-md fw-bold fs-6">TMT KEP</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_tmt_kep_pembebasan" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Email</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_email" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Nomor HP</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_hp" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Alamat</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="get_alamat" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-4">
                            <div class="row mb-6 mt-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Nama perguran Tinggi</label>
                                <div class="col-lg-8">
                                    <input id="perguruantinggi_teliti" type="text"
                                           class="form-control form-control form-control-solid" disabled/>
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Semester ke-</label>
                                <div class="col-lg-8">
                                    <input id="semesterke_teliti" type="text"
                                           class="form-control form-control form-control-solid" disabled/>
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Semester</label>
                                <div class="col-lg-8">
                                    <input id="semester_teliti" type="text"
                                           class="form-control form-control form-control-solid" disabled/>
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Tahun Akademik</label>
                                <div class="col-lg-8">
                                    <input id="tahun_teliti" type="text"
                                           class="form-control form-control form-control-solid" disabled/>
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">IPK</label>
                                <div class="col-lg-8">
                                    <input id="ipk_teliti" type="text"
                                           class="form-control form-control form-control-solid" disabled/>
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Jumlah SKS</label>
                                <div class="col-lg-8">
                                    <input id="jumlah_sks_teliti" type="text"
                                           class="form-control form-control form-control-solid" disabled/>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                                <div class="col-lg-4">
                                    <div class="col-sm">
                                        <div class="d-flex flex-aligns-center mt-2 pe-10">
                                            <img alt="" class="w-25px me-3"
                                                 src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1">
                                                <a href="#" id="file_lps_teliti" target="_blank"
                                                   class="fs-6 text-hover-primary">Laporan Perkembangan Studi.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm mt-4">
                                        <div class="d-flex flex-aligns-center mt-2 pe-10">
                                            <img alt="" class="w-25px me-3"
                                                 src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1">
                                                <a href="#" id="file_khs_teliti" target="_blank"
                                                   class="fs-6 text-hover-primary">Kartu Hasil Studi.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if ($tab == 'menunggu_persetujuan')
                        <div class="text-center mb-8">
                            <input type="hidden" id="id_lps" value="">
                            <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                    class="fa fa-reply"></i>Batal
                            </button>
                            <button type="button" class="btn btn-sm btn-danger btnTolak">
                                <i class="fa fa-times"></i> Tolak
                            </button>
                            <button type="button" class="btn btn-sm btn-success btnSetuju">
                                <i class="fa fa-check-circle"></i> Setuju
                            </button>
                        </div>
                    @endif

                </form>

            </div>
        </div>
    </div>

    @include('tubel.modal_tiket')

@endsection

@section('js')
    <script>
        let app_url = '{{ env('APP_URL') }}'

        $("#advancedSearch").submit(function (e) {
            e.preventDefault();

            var query = $(this).serializeArray().filter(function (i) {
                return i.value;
            });

            window.location.href = $(this).attr('action') + (query ? '?' + $.param(query) : '');

        })
    </script>

    <script src="{{ asset('assets/js/tubel') }}/lps.js"></script>
    <script src="{{ asset('assets/js/tubel') }}/log-tiket.js"></script>
@endsection
