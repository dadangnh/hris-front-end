<div class="table-responsive bg-gray-400 rounded border border-gray-300">
    <table class="table table-striped  align-middle" id="tabel_tubel">
        <thead class="fw-bolder bg-secondary fs-6">
        <tr class="text-right">
            <th class="text-center">&nbsp;No</th>
            <th class="">Data Pegawai</th>
            <th class="">Jenjang Pendidikan</th>
            <th class="">Semester / IPK / Jumlah SKS</th>
            <th class="">Perguruan Tinggi / Program Studi</th>

            @if ($tab != 'ditolak')
                <th class="">Tiket</th>
            @endif

            @if ($tab=='ditolak')
                <th class="">Alasan Tolak</th>
            @endif

            <th class="text-center">Aksi</th>
        </tr>
        </thead>
        <tbody>
        @if ($lps->data != null)
            @php
                $no = 1;
            @endphp

            @foreach ($lps->data as $t)
                <tr id="{{ $t->id }}" class="text-right">
                    <td class="text-center text-dark  mb-1 fs-6">
                        {{$no++}}
                    </td>

                    <td class="text-dark fw-bolder mb-1 fs-6">
                        <a class="fw-bolder"
                           href="{{ url('tubel/monitoring/'. $t->dataIndukTubelId->id . '?tab=lps') }}">
                            {{ $t->dataIndukTubelId->namaPegawai }}
                        </a>
                        <br>
                        {{-- {{ $t->dataIndukTubelId->namaPangkat }} <br> --}}
                        <span class="fw-normal fs-6">{{ $t->dataIndukTubelId->nip18 }}</span>
                    </td>

                    <td class="text-dark  mb-1 fs-6">
                        {{ $t->dataIndukTubelId->jenjangPendidikanId ? $t->dataIndukTubelId->jenjangPendidikanId->jenjangPendidikan : null }}
                        <br>
                        {{ $t->dataIndukTubelId->lokasiPendidikanId->lokasi }}
                    </td>

                    <td class="text-dark  mb-1 fs-6">
                        Semester : {{$t->semesterKe}}
                        {{-- <br> --}}
                        @if ($t->semesterKe % 2 == 0)
                            (Genap
                        @else
                            (Ganjil
                        @endif / {{$t->tahunAkademik}}) <br>
                        IPK : {{$t->ipk}} <br>
                        Jumlah SKS : {{$t->jumlahSks}}
                    </td>

                    <td class="text-dark mb-1 fs-6">
                        {{ $t->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }}
                        <br> {{ $t->ptPesertaTubelId->programStudiId->programStudi }}
                    </td>

                    @if ($tab != 'ditolak')
                        <td class="text-dark mb-1 fs-6 ">
                            <a href="javascript:void(0)" class="text-primary btnTiket"
                               data-tiket="{{ $t->logStatusProbis->nomorTiket }}"
                               data-nama="{{ $t->dataIndukTubelId->namaPegawai }}">
                                {{ $t->logStatusProbis->nomorTiket }}
                            </a>
                        </td>
                    @endif

                    @if ($tab=='ditolak')
                        <td class="text-dark mb-1 fs-6">
                            {{$t->logStatusProbis->keterangan}} <br>
                            Tiket: {{$t->logStatusProbis->nomorTiket}}
                        </td>
                    @endif

                    <td class="text-center text-dark mb-1 fs-6">

                        @if ($tab=='menunggu_persetujuan')

                            <button class="btn btn-sm btn-primary m-2 btnTeliti" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Periksa"
                                    data-id="{{ $t->id }}"
                                    data-nama="{{ $t->dataIndukTubelId->namaPegawai }}"
                                    data-nip="{{ $t->dataIndukTubelId->nip18 }}"
                                    data-pangkat="{{ $t->dataIndukTubelId->namaPangkat }}"
                                    data-jabatan="{{ $t->dataIndukTubelId->namaJabatan }}"
                                    data-jenjang="{{ $t->dataIndukTubelId->jenjangPendidikanId->jenjangPendidikan }}"
                                    data-program_beasiswa="{{ $t->dataIndukTubelId->programBeasiswa }}"
                                    data-lokasi="{{ $t->dataIndukTubelId->lokasiPendidikanId->lokasi }}"
                                    data-pt="{{ $t->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }}"
                                    data-prodi="{{ $t->ptPesertaTubelId->programStudiId->programStudi }}"
                                    data-st_tubel="{{ $t->dataIndukTubelId->nomorSt }}"
                                    data-tgl_st_tubel="{{AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tglSt,'j F Y','') }}"
                                    data-tgl_mulai="{{AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tglMulaiSt,'j F Y','')}}"
                                    data-tgl_selesai="{{AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tglSelesaiSt,'j F Y','')}}"
                                    data-kep_pembebasan="{{ $t->dataIndukTubelId->nomorKepPembebasan }}"
                                    data-tgl_kep_pembebasan="{{AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tglKepPembebasan,'j F Y','')}}"
                                    data-tmt_kep_pembebasan="{{AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tmtKepPembebasan,'j F Y','')}}"
                                    data-email="{{ $t->dataIndukTubelId->emailNonPajak }}"
                                    data-hp="{{ $t->dataIndukTubelId->noHandphone }}"
                                    data-alamat="{{ $t->dataIndukTubelId->alamatTinggal }}"
                                    data-file_lps="{{ $t->pathLps }}"
                                    data-file_khs="{{ $t->pathKhs }}"
                                    data-smt="{{ $t->semesterKe }}"
                                    data-smtganjilgenap="{{ $t->flagSemester }}"
                                    data-tahun="{{ $t->tahunAkademik }}"
                                    data-ipk="{{ $t->ipk }}"
                                    data-sks="{{ $t->jumlahSks }}"
                            >
                                <i class="fa fa-cog"></i> Periksa
                                </a>

                                @elseif(($tab=='telah_disetujui') || ($tab=='ditolak'))

                                    <button class="btn btn-sm btn-info btn-icon m-2 btnTeliti" data-bs-toggle="tooltip"
                                            data-bs-placement="top" title="Detail"
                                            data-id="{{ $t->id }}"
                                            data-nama="{{ $t->dataIndukTubelId->namaPegawai }}"
                                            data-nip="{{ $t->dataIndukTubelId->nip18 }}"
                                            data-pangkat="{{ $t->dataIndukTubelId->namaPangkat }}"
                                            data-jabatan="{{ $t->dataIndukTubelId->namaJabatan }}"
                                            data-jenjang="{{ $t->dataIndukTubelId->jenjangPendidikanId->jenjangPendidikan }}"
                                            data-program_beasiswa="{{ $t->dataIndukTubelId->programBeasiswa }}"
                                            data-lokasi="{{ $t->dataIndukTubelId->lokasiPendidikanId->lokasi }}"
                                            data-pt="{{ $t->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }}"
                                            data-prodi="{{ $t->ptPesertaTubelId->programStudiId->programStudi }}"
                                            data-st_tubel="{{ $t->dataIndukTubelId->nomorSt }}"
                                            data-tgl_st_tubel="{{AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tglSt,'j F Y','') }}"
                                            data-tgl_mulai="{{AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tglMulaiSt,'j F Y','')}}"
                                            data-tgl_selesai="{{AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tglSelesaiSt,'j F Y','')}}"
                                            data-kep_pembebasan="{{ $t->dataIndukTubelId->nomorKepPembebasan }}"
                                            data-tgl_kep_pembebasan="{{AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tglKepPembebasan,'j F Y','')}}"
                                            data-tmt_kep_pembebasan="{{AppHelper::instance()->indonesian_date($t->dataIndukTubelId->tmtKepPembebasan,'j F Y','')}}"
                                            data-email="{{ $t->dataIndukTubelId->emailNonPajak }}"
                                            data-hp="{{ $t->dataIndukTubelId->noHandphone }}"
                                            data-alamat="{{ $t->dataIndukTubelId->alamatTinggal }}"
                                            data-file_lps="{{ $t->pathLps }}"
                                            data-file_khs="{{ $t->pathKhs }}"
                                            data-smt="{{ $t->semesterKe }}"
                                            data-smtganjilgenap="{{ $t->flagSemester }}"
                                            data-tahun="{{ $t->tahunAkademik }}"
                                            data-ipk="{{ $t->ipk }}"
                                            data-sks="{{ $t->jumlahSks }}"
                                    >
                                        <i class="fa fa-eye"></i>
                                        </a>

                                        @elseif($tab=='tolak')

                                            <a href="{{ url('tubel/detaillps/'. $t->id) }}"
                                               class="btn btn-sm btn-icon btn-info m-2" data-bs-toggle="tooltip"
                                               data-bs-placement="top" title="Detail">
                                                <i class="fa fa-eye"></i>
                                            </a>

                        @endif

                    </td>
                </tr>
            @endforeach

        @else

            <tr class="text-center">
                <td class="text-dark  mb-1 fs-6" colspan="8">Tidak terdapat data</td>
            </tr>

        @endif

        </tbody>
    </table>

</div>

@if ($totalItems != 0)
    <!--begin::pagination-->
    <div class="d-flex flex-stack flex-wrap pt-5 p-3">
        <div class="fs-6 fw-bold text-gray-700"></div>

        @php
            $disabled = '';
            $nextPage= '';
            $disablednext = '';
            $hidden = '';
            $pagination = '';

            // tombol pagination
            if ($totalItems == 0) {
                $pagination = 'none';
            }

            // tombol back
            if($currentPage == 1){
                $disabled = 'disabled';
            }

            // tombol next
            if($currentPage != $totalPages) {
                $nextPage = $currentPage + 1;
            }

            // tombol pagination
            if($currentPage == $totalPages || $totalPages == 0){
                $disablednext = 'disabled';
                $hidden = 'hidden';
            }

        @endphp

            <!--begin::Pages-->
        <ul class="pagination" style="display: {{ $pagination }}">
            <li class="page-item disabled"><a href="#" class="page-link">Halaman {{ $currentPage }}
                    dari {{ $totalPages }}</a></li>
            <li class="page-item previous {{ $disabled }}"><a
                    href="{{ url()->current() . '?page=' . ($currentPage - 1) }}" class="page-link"><i
                        class="previous"></i></a></li>
            <li class="page-item active"><a href="{{ url()->current() . '?page=' . $currentPage }}"
                                            class="page-link">{{ $currentPage }}</a></li>
            <li class="page-item" {{ $hidden }}><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                   class="page-link">{{ $nextPage }}</a></li>
            <li class="page-item next {{ $disablednext }}"><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                              class="page-link"><i class="next"></i></a></li>
        </ul>
        <!--end::Pages-->

    </div>
    <!--end::paging-->
@endif
