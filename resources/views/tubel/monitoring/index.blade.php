@extends('layout.master')

@section('content')
    @php
        $halaman = isset($_GET['page']) ? (int) $_GET['page'] : 1;
        $halaman_awal = $halaman > 1 ? $halaman * $size - $size : 0;
        $nomor = $halaman_awal + 1;
    @endphp

    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card mb-3 border-warning shadow-sm">
                    <div id="card-header" class="card-header">
                        <div class="card-title fw-bold fs-4 text-white">
                            Monitoring Pegawai Tugas Belajar
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6 mb-4">
                                <div class="text-start" data-kt-customer-table-toolbar="base">
                                    <button type="button"
                                            class="btn btn-sm btn-ligth btn-active-primary border border-gray-400 fs-6"
                                            data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start"
                                            data-kt-menu-flip="top-end">
                                        <i class="fa fa-filter"></i> Advanced Search
                                    </button>
                                    <div id="hasil"></div>
                                    <div class="menu menu-sub menu-sub-dropdown w-md-1000px border-warning mt-1"
                                         data-kt-menu="true">
                                        <div class="px-7 py-5">
                                            <div class="fs-4 text-dark fw-bolder">Filter Pencarian</div>
                                        </div>
                                        <form id="advancedSearch" action="" method="get">
                                            <div class="px-7 py-5">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="row mb-6">
                                                            <label class="form-label fw-bold">Pegawai:</label>
                                                            <div class="col-lg-6">
                                                                <input class="form-control form-select-solid"
                                                                       type="text" name="nama"
                                                                       placeholder="nama pegawai"
                                                                       value="{{ isset($_GET['nama']) ? $_GET['nama'] : '' }}">
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <input class="form-control form-select-solid"
                                                                       type="text" name="nip" placeholder="nip 18 digit"
                                                                       value="{{ isset($_GET['nip']) ? $_GET['nip'] : '' }}">
                                                            </div>
                                                        </div>
                                                        <div class="row mb-6">
                                                            <label class="form-label fw-bold">Jenjang Pendidikan</label>
                                                            <select id="search_jenjang" class="form-select"
                                                                    name="jenjang"
                                                                    data-placeholder="pilih jenjang pendidikan"
                                                                    data-control="select2" data-hide-search="true">
                                                                <option></option>
                                                                @if (isset($_GET['jenjang']))
                                                                    @foreach ($jenjang as $j)
                                                                        <option
                                                                            value="{{ $j->id }}" {{ $_GET['jenjang'] == $j->id ? 'selected' : '' }}>{{ $j->jenjangPendidikan }}</option>
                                                                    @endforeach
                                                                @else
                                                                    @foreach ($jenjang as $j)
                                                                        <option
                                                                            value="{{ $j->id }}">{{ $j->jenjangPendidikan }}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <div class="row mb-6">
                                                            <label class="form-label fw-bold">Lokasi Pendidikan</label>
                                                            <select id="search_lokasi" class="form-select" name="lokasi"
                                                                    data-placeholder="pilih lokasi pendidikan"
                                                                    data-control="select2" data-hide-search="false">
                                                                <option></option>
                                                                @foreach ($lokasi as $l)
                                                                    <option
                                                                        value="{{ $l->id }}">{{ $l->lokasi }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="row mb-6">
                                                            <label class="form-label fw-bold">Status Tugas
                                                                Belajar:</label>
                                                            <select class="form-select" name="status"
                                                                    data-kt-select2="true" data-control="select2"
                                                                    data-placeholder="pilih status"
                                                                    data-hide-search="true">
                                                                <option></option>
                                                                @foreach ($status as $s)
                                                                    <option
                                                                        value="{{ $s['id'] }}">{{ $s['caseOutput'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="row mb-6">
                                                            <label class="form-label fw-bold">Tanggal Mulai
                                                                Tubel:</label>
                                                            <div class="input-daterange input-group">
                                                                <input type="text"
                                                                       class="form-control text-start date-search"
                                                                       name="tgl_mulai_dari"
                                                                       placeholder="tanggal mulai (dari)"
                                                                       value="{{ isset($_GET['tgl_mulai_dari']) ? $_GET['tgl_mulai_dari'] : '' }}"/>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        s.d.
                                                                    </span>
                                                                </div>
                                                                <input type="text"
                                                                       class="form-control text-start date-search"
                                                                       name="tgl_mulai_selesai"
                                                                       placeholder="tanggal mulai (sampai)"
                                                                       value="{{ isset($_GET['tgl_mulai_selesai']) ? $_GET['tgl_mulai_selesai'] : '' }}"/>
                                                            </div>
                                                        </div>
                                                        <div class="row mb-6">
                                                            <label class="form-label fw-bold">Tanggal Selesai
                                                                Tubel:</label>
                                                            <div class="input-daterange input-group">
                                                                <input type="text"
                                                                       class="form-control text-start date-search"
                                                                       name="tgl_selesai_dari"
                                                                       placeholder="tanggal selesai (dari)"
                                                                       value="{{ isset($_GET['tgl_selesai_dari']) ? $_GET['tgl_selesai_dari'] : '' }}"/>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        s.d.
                                                                    </span>
                                                                </div>
                                                                <input type="text"
                                                                       class="form-control text-start date-search"
                                                                       name="tgl_selesai_selesai"
                                                                       placeholder="tanggal selesai (sampai)"
                                                                       value="{{ isset($_GET['tgl_selesai_selesai']) ? $_GET['tgl_selesai_selesai'] : '' }}"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-end">
                                                    <a href="{{ url()->current() }}"
                                                       class="btn btn-light btn-active-light-primary me-2">Reset</a>
                                                    <button type="submit" class="btn btn-sm btn-primary"
                                                            data-kt-menu-dismiss="true"
                                                            data-kt-customer-table-filter="filter">Search
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="table-responsive bg-gray-400 rounded border border-gray-300">
                            <table class="table table-striped align-middle" id="tabel_tubel">
                                <thead class="fw-bolder bg-secondary fs-6">
                                <tr class="text-right" style="vertical-align: middle;">
                                    <th class="text-center">&nbsp; No</th>
                                    <th class="">Data Pegawai</th>
                                    <th class="">Jenjang Pendidikan</th>
                                    <th class="">Program Beasiswa / <br> Lokasi</th>
                                    <th class="">Perguruan Tinggi / <br> Program Studi</th>
                                    <th class="">Lama Pendidikan / <br> Total Semester</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if ($tubel->data != null)

                                    @foreach ($tubel->data as $t)
                                        <tr class="text-right">
                                            <td class="text-center text-dark  mb-1 fs-6">
                                                {{ $nomor++ }}
                                            </td>

                                            <td class="text-dark fw-bolder mb-1 fs-6">
                                                {{ $t->namaPegawai }} <br>
                                                {{-- {{ $t->namaPangkat }} <br> --}}
                                                <span class="fw-normal fs-6">{{ $t->nip18 }}</span>
                                            </td>

                                            <td class="text-dark  mb-1 fs-6">
                                                {{ $t->jenjangPendidikanId ? $t->jenjangPendidikanId->jenjangPendidikan : null }}

                                            </td>

                                            <td class="text-dark  mb-1 fs-6">
                                                {{ $t->programBeasiswa }} <br>
                                                {{ $t->lokasiPendidikanId ? $t->lokasiPendidikanId->lokasi : null }}
                                            </td>

                                            <td class="text-dark  mb-1 fs-6">

                                                @foreach ($perguruantinggi[$t->id] as $p)
                                                    {{ $p }} <br>
                                                @endforeach

                                            </td>

                                            <td class="text-dark  mb-1 fs-6">
                                                {{ $t->lamaPendidikan ? $t->lamaPendidikan . ' Tahun' : '' }} <br>
                                                {{ $t->totalSemester ? $t->totalSemester . ' Semester' : '' }}
                                            </td>

                                            <td class="text-center text-dark  mb-1 fs-6">
                                                <span
                                                    class="badge badge-light-success fs-7 fw-bolder">{{ ucwords(strtolower($t->logStatusProbis->caseId->namaCase)) }}</span>
                                            </td>

                                            <td class="text-center text-dark  mb-1 fs-6">
                                                <a href="{{ url('tubel/monitoring/' . $t->id) }}"
                                                   class="btn btn-sm btn-primary m-2" data-bs-toggle="tooltip"
                                                   data-bs-placement="top" title="Detail">
                                                    <i class="fa fa-eye"></i>Detail
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach

                                @else
                                    <tr class="text-center">
                                        <td class="text-dark  mb-1 fs-6" colspan="6">Tidak terdapat data</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>

                        @if ($totalItems != 0)
                            <!--begin::pagination-->
                            <div class="d-flex flex-stack flex-wrap pt-5 p-3">
                                <div class="fs-6 fw-bold text-gray-700"></div>

                                @php
                                    $disabled = '';
                                    $nextPage = '';
                                    $disablednext = '';
                                    $hidden = '';
                                    $pagination = '';

                                    // tombol pagination
                                    if ($totalItems == 0) {
                                        // $pagination = 'none';
                                    }

                                    // tombol back
                                    if ($currentPage == 1) {
                                        $disabled = 'disabled';
                                    }

                                    // tombol next
                                    if ($currentPage != $totalPages) {
                                        $nextPage = $currentPage + 1;
                                    }

                                    // tombol pagination
                                    if ($currentPage == $totalPages || $totalPages == 0) {
                                        $disablednext = 'disabled';
                                        $hidden = 'hidden';
                                    }

                                @endphp

                                    <!--begin::Pages-->
                                <ul class="pagination" style="display: {{ $pagination }}">
                                    <li class="page-item disabled"><a href="#"
                                                                      class="page-link">Halaman {{ $currentPage }}
                                            dari {{ $totalPages }}</a></li>
                                    <li class="page-item previous {{ $disabled }}"><a
                                            href="{{ url()->current() . '?page=' . ($currentPage - 1) }}"
                                            class="page-link"><i class="previous"></i></a></li>
                                    @if ($currentPage != 1)
                                        <li class="page-item"><a
                                                href="{{ url()->current() . '?page=' . ($currentPage - 1) }}"
                                                class="page-link">{{ $currentPage - 1 }}</a></li>
                                    @endif
                                    <li class="page-item active"><a
                                            href="{{ url()->current() . '?page=' . $currentPage }}"
                                            class="page-link">{{ $currentPage }}</a></li>
                                    <li class="page-item" {{ $hidden }}><a
                                            href="{{ url()->current() . '?page=' . $nextPage }}"
                                            class="page-link">{{ $nextPage }}</a></li>
                                    <li class="page-item next {{ $disablednext }}"><a
                                            href="{{ url()->current() . '?page=' . $nextPage }}" class="page-link"><i
                                                class="next"></i></a></li>
                                </ul>
                                <!--end::Pages-->

                            </div>
                            <!--end::paging-->
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('js')
    <script>
        // advanced search
        $("#advancedSearch").submit(function (e) {
            e.preventDefault();

            var query = $(this).serializeArray().filter(function (i) {
                return i.value;
            });

            window.location.href = $(this).attr('action') + (query ? '?' + $.param(query) : '');

        })

        // placeholder advance search
        $(window).on("load", function () {
            // jenjang
            $selectElement = $('#search_jenjang').select2({
                allowClear: true,
                minimumResultsForSearch: -1
            });

            // lokasi
            $selectElement = $('#search_lokasi').select2({
                allowClear: true,
                minimumResultsForSearch: -1
            });
        });

        // date format
        $(".date-search").flatpickr({
            dateFormat: "d-m-Y"
        });
    </script>
@endsection
