<div class="table-responsive bg-gray-400 rounded border border-gray-300">
    <table class="table table-striped  align-middle" id="tabel_tubel">
        <thead class="fw-bolder bg-secondary fs-6">
            <tr class="text-right">
                <th class="text-center ps-2">No</th>
                <th class="">Pegawai</th>
                <th colspan="2">Data Pendukung</th>
                <th class="">Tiket</th>
                @if ($tab !== 'ditolak')
                    <th class="text-center">Aksi</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @if (!empty($hasil_akhir))

                @php
                    $nomor = $currentPage == 1 ? 1 : ($currentPage - 1) * $size + 1;
                @endphp

                @foreach ($hasil_akhir as $ha)

                    <tr id="ha-{{ $ha->id }}" class="text-right">
                        <td class="text-center text-dark fs-6 ps-2">
                            {{ $nomor++ }}
                        </td>

                        <td class="text-dark fs-6">
                            <div class="mb-0">
                                <a class="fw-bold" href="#">
                                    {{ $ha->statusLaporDiri->dataIndukTubelId->namaPegawai }}
                                </a>
                            </div>
                            <span class="fw-normal fs-6">{{ $ha->statusLaporDiri->dataIndukTubelId->nip18 }}</span>
                            <div class="mt-4 mb-0">
                                Pendidikan :
                            </div>
                            <div class="mb-2">
                                {{ $ha->statusLaporDiri->dataIndukTubelId->jenjangPendidikanId->jenjangPendidikan }} - {{ $ha->statusLaporDiri->dataIndukTubelId->lokasiPendidikanId->lokasi }}
                            </div>
                            <div class="fw-bolder mt-4 mb-2">
                                <span class="d-inline-block position-relative">
                                    <h2>{{ $ha->statusLaporDiriHasilStudi->status }}</h2>
                                    @php
                                        switch ($ha->statusLaporDiriHasilStudi->status) {
                                            case 'Lulus':
                                                $color = 'success';
                                                break;
                                            case 'Belum Lulus':
                                                $color = 'warning';
                                                break;
                                            case 'Mengundurkan Diri':
                                                $color = 'danger';
                                                break;
                                            case 'Drop Out':
                                                $color = 'danger';
                                                break;
                                        }
                                    @endphp
                                    <span class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-{{ $color }} translate rounded"></span>
                                </span>
                            </div>
                        </td>

                        @switch($ha->statusLaporDiriHasilStudi->status)
                            @case('Lulus')
                                <td class="text-dark fs-6">
                                    <div class="mb-6">
                                        Tanggal Lulus : {{ AppHelper::instance()->indonesian_date($ha->statusLaporDiri->tanggalLulus, 'j F Y', '') }}
                                        <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                            <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1">
                                                <a href="{{ env('APP_URL') }}/files/{{ $ha->statusLaporDiri->pathLaporanSelesaiStudi }}" target="_blank" class="fs-6 text-hover-primary">Laporan Selesai Studi.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-dark fs-6">
                                    <div class="mb-3">
                                        Karya Tulis : {{ $ha->statusLaporDiri->tipeDokPenelitian->tipeDokumen }}
                                        <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                            <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1">
                                                <a href="{{ env('APP_URL') }}/files/{{ $ha->statusLaporDiri->pathDokumenPenelitian }}" target="_blank" class="fs-6 text-hover-primary">Dokumen Penelitian.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            @break

                            @case('Belum Lulus')
                                <td class="text-dark fs-6">
                                    <div class="mb-0">
                                        Tahap Perkuliahan:
                                        <br>
                                        {{ $ha->statusLaporDiri->tahapBelumLulus->tahap }}
                                    </div>
                                </td>
                                <td class="text-dark fs-6">
                                    <div class="mb-6">
                                        Timeline Rencana Penyelesaian Studi:
                                        <div class="d-flex flex-aligns-center mt-3 pe-10 pe-lg-20">
                                            <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1">
                                                <a href="{{ url('files/' . $ha->statusLaporDiri->pathTimelineRencanaStudi) }}" target="_blank" class="fs-6 text-hover-primary">Timeline.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-6">
                                        Draft Karya Tulis: {{ $ha->statusLaporDiri->tipeDokPenelitian->tipeDokumen }}
                                        <div class="d-flex flex-aligns-center mt-3 pe-10 pe-lg-20">
                                            <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1">
                                                <a href="{{ url('files/' . $ha->statusLaporDiri->pathDokumenPenelitian) }}" target="_blank" class="fs-6 text-hover-primary">Draft Karya Tulis.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>

                            @break

                            @case('Mengundurkan Diri')
                                <td class="text-dark fs-6">
                                    <div>
                                        Semester: {{ $ha->statusLaporDiri->semesterKe }}
                                        <br>
                                        Tahun: {{ $ha->statusLaporDiri->tahunAkademik }}
                                    </div>
                                    <div class="mt-4">
                                        Alasan: {{ $ha->statusLaporDiri->alasanMundur }}
                                    </div>
                                </td>
                                <td class="text-dark fs-6">
                                    <div class="mb-6">
                                        Surat Persetujuan dari Perguruan Tinggi
                                        <br>
                                        Nomor: {{ $ha->statusLaporDiri->noSuratPersetujuanMundur }}
                                        <br>
                                        Tanggal: {{ AppHelper::instance()->indonesian_date($ha->statusLaporDiri->tglSuratPersetujuanMundur, 'j F Y', '') }}
                                        <div class="d-flex flex-aligns-center mt-3 pe-10 pe-lg-20">
                                            <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1">
                                                <a href="{{ url('files/' . $ha->statusLaporDiri->pathSuratPersetujuanMundur) }}" target="_blank" class="fs-6 text-hover-primary">Surat Persetujuan.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            @break

                            @case('Drop Out')
                                <td class="text-dark fs-6">
                                    <div class="mb-0">
                                        Semester: {{ $ha->statusLaporDiri->semesterKe }}
                                        <br>
                                        Tahun: {{ $ha->statusLaporDiri->tahunAkademik }}
                                    </div>
                                </td>
                                <td class="text-dark fs-6">
                                    <div class="mb-6">
                                        Surat Keterangan dari Perguruan Tinggi
                                        <br>
                                        Nomor: {{ $ha->statusLaporDiri->nomorSuratDo }}
                                        <br>
                                        Tanggal: {{ AppHelper::instance()->indonesian_date($ha->statusLaporDiri->tglSuratDo, 'j F Y', '') }}
                                        <div class="d-flex flex-aligns-center mt-3 pe-10 pe-lg-20">
                                            <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1">
                                                <a href="{{ url('files/' . $ha->statusLaporDiri->pathSuratDo) }}" target="_blank" class="fs-6 text-hover-primary">Surat Keterangan.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            @break
                            @default

                        @endswitch
                        </td>

                        <td class="text-dark fs-6 ">
                            <a href="javascript:void(0)" class="text-primary btnTiket" data-tiket="{{ $ha->logStatusProbis->nomorTiket }}" data-nama="{{ $ha->statusLaporDiri->dataIndukTubelId->namaPegawai }}">
                                {{ $ha->logStatusProbis->nomorTiket }}
                            </a>
                        </td>

                        <td class="text-center text-dark fs-6">
                            @if ($tab == 'persetujuan')
                                @if ($ha->statusLaporDiriHasilStudi->status == 'Lulus')
                                    <button class="btn btn-sm btn-primary m-2 btnPeriksaHasilAkhir" data-bs-toggle="tooltip" data-bs-placement="top" title="Periksa"
                                        data-status="{{ $ha->statusLaporDiriHasilStudi->status }}"
                                        data-id="{{ $ha->id }}"
                                        data-nama="{{ $ha->statusLaporDiri->dataIndukTubelId->namaPegawai }}"
                                        data-nip="{{ $ha->statusLaporDiri->dataIndukTubelId->nip18 }}"
                                        data-pangkat="{{ $ha->statusLaporDiri->dataIndukTubelId->namaPangkat }}"
                                        data-jabatan="{{ $ha->statusLaporDiri->dataIndukTubelId->namaJabatan }}"
                                        data-jenjang="{{ $ha->statusLaporDiri->dataIndukTubelId->jenjangPendidikanId->jenjangPendidikan }}"
                                        data-program_beasiswa="{{ $ha->statusLaporDiri->dataIndukTubelId->programBeasiswa }}"
                                        data-lokasi="{{ $ha->statusLaporDiri->dataIndukTubelId->lokasiPendidikanId->lokasi }}"
                                        data-st_tubel="{{ $ha->statusLaporDiri->dataIndukTubelId->nomorSt }}"
                                        data-tgl_st_tubel="{{ AppHelper::instance()->indonesian_date($ha->statusLaporDiri->dataIndukTubelId->tglSt, 'j F Y', '') }}"
                                        data-tgl_mulai="{{ AppHelper::instance()->indonesian_date($ha->statusLaporDiri->dataIndukTubelId->tglMulaiSt, 'j F Y', '') }}"
                                        data-tgl_selesai="{{ AppHelper::instance()->indonesian_date($ha->statusLaporDiri->dataIndukTubelId->tglSelesaiSt, 'j F Y', '') }}"
                                        data-kep_pembebasan="{{ $ha->statusLaporDiri->dataIndukTubelId->nomorKepPembebasan }}"
                                        data-tgl_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($ha->statusLaporDiri->dataIndukTubelId->tglKepPembebasan, 'j F Y', '') }}"
                                        data-tmt_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($ha->statusLaporDiri->dataIndukTubelId->tmtKepPembebasan, 'j F Y', '') }}"
                                        data-email="{{ $ha->statusLaporDiri->dataIndukTubelId->emailNonPajak }}"
                                        data-hp="{{ $ha->statusLaporDiri->dataIndukTubelId->emailNonPajak }}"
                                        data-tgl_kelulusan="{{ AppHelper::instance()->indonesian_date($ha->statusLaporDiri->tanggalLulus, 'j F Y', '') }}"
                                        data-tipe_dokumen_penelitian="{{ $ha->statusLaporDiri->tipeDokPenelitian->tipeDokumen }}"
                                        data-file_dokumen_penelitian="{{ $ha->statusLaporDiri->pathDokumenPenelitian }}"
                                        data-file_lps="{{ $ha->statusLaporDiri->pathLaporanSelesaiStudi }}"

                                        @php
                                            $nama_pt = $ipk = $no_skl = $tgl_skl = $file_skl = $no_ijazah = $tgl_ijazah = $file_ijazah = $no_transkrip = $tgl_transkrip = $file_transkrip = 1;
                                            @endphp

                                        @foreach ($perguruantinggi as $key => $item)
                                            data-count_pt="{{ count($item) }}"
                                            @if ($key == $ha->statusLaporDiri->dataIndukTubelId->id)

                                                @foreach ($item as $value)
                                                    data-nama_pt_{{ $nama_pt++ }}="{{ $value['namaPerguruanTinggi'] }}"
                                                    data-ipk_pt_{{ $ipk++ }}="{{ $value['ipk'] }}"
                                                    data-no_skl_pt_{{ $no_skl++ }}="{{ $value['nomorSkl'] }}"
                                                    data-tgl_skl_pt_{{ $tgl_skl++ }}="{{ AppHelper::instance()->indonesian_date($value['tglSkl'], 'j F Y', '') }}"
                                                    data-file_skl_pt_{{ $file_skl++ }}="{{ $value['pathSkl'] }}"
                                                    data-no_ijazah_pt_{{ $no_ijazah++ }}="{{ $value['nomorIjazah'] }}"
                                                    data-tgl_ijazah_pt_{{ $tgl_ijazah++ }}="{{ AppHelper::instance()->indonesian_date($value['tglIjazah'], 'j F Y', '') }}"
                                                    data-file_ijazah_pt_{{ $file_ijazah++ }}="{{ $value['pathIjazah'] }}"
                                                    data-no_transkrip_pt_{{ $no_transkrip++ }}="{{ $value['nomorTranskripNilai'] }}"
                                                    data-tgl_transkrip_pt_{{ $tgl_transkrip++ }}="{{ AppHelper::instance()->indonesian_date($value['tglTranskripNilai'], 'j F Y', '') }}"
                                                    data-file_transkrip_pt_{{ $file_transkrip++ }}="{{ $value['pathTranskripNilai'] }}"
                                                @endforeach
                                            @endif
                                        @endforeach
                                        >
                                        <i class="fa fa-file"></i> Periksa
                                    </button>
                                @endif
                            @endif
                        </td>

                    </tr>

                @endforeach

            @else

                <tr class="text-center">
                    <td class="text-dark  fs-6" colspan="6">Tidak terdapat data</td>
                </tr>

            @endif

        </tbody>
    </table>

</div>

@if ($totalItems != 0)
    <div class="d-flex flex-stack flex-wrap pt-5 p-3">
        <div class="fs-6 fw-bold text-gray-700"></div>

        @php
            $disabled = '';
            $nextPage = '';
            $disablednext = '';
            $hidden = '';
            $pagination = '';

            // tombol pagination
            if ($totalItems == 0) {
                $pagination = 'none';
            }

            // tombol back
            if ($currentPage == 1) {
                $disabled = 'disabled';
            }

            // tombol next
            if ($currentPage != $totalPages) {
                $nextPage = $currentPage + 1;
            }

            // tombol pagination
            if ($currentPage == $totalPages || $totalPages == 0) {
                $disablednext = 'disabled';
                $hidden = 'hidden';
            }

        @endphp

        <!--begin::Pages-->
        <ul class="pagination" style="display: {{ $pagination }}">
            <li class="page-item disabled"><a href="#" class="page-link">Halaman {{ $currentPage }} dari {{ $totalPages }}</a></li>
            <li class="page-item previous {{ $disabled }}"><a href="{{ url()->current() . '?page=' . ($currentPage - 1) }}" class="page-link"><i class="previous"></i></a></li>
            <li class="page-item active"><a href="{{ url()->current() . '?page=' . $currentPage }}" class="page-link">{{ $currentPage }}</a></li>
            <li class="page-item" {{ $hidden }}><a href="{{ url()->current() . '?page=' . $nextPage }}" class="page-link">{{ $nextPage }}</a></li>
            <li class="page-item next {{ $disablednext }}"><a href="{{ url()->current() . '?page=' . $nextPage }}" class="page-link"><i class="next"></i></a></li>
        </ul>
        <!--end::Pages-->

    </div>
    <!--end::paging-->
@endif
