<div class="table-responsive bg-gray-400 rounded border border-gray-300">
    <table class="table table-striped  align-middle" id="tabel_tubel">
        <thead class="fw-bolder bg-secondary fs-6">
        <tr class="text-right">
            <th class="text-center ps-2">No</th>
            <th class="">Pegawai</th>
            <th colspan="2">Data Pendukung</th>
            <th class="">Tiket</th>
            @if ($tab == 'menunggu-verifikasi')
                <th class="text-center">Aksi</th>
            @endif
        </tr>
        </thead>
        <tbody>
        @if ($verifikasi)

            @php
                $nomor = $currentPage == 1 ? 1 : ($currentPage - 1) * $size + 1;
            @endphp

            @foreach ($verifikasi as $ld)

                <tr id="ld-{{ $ld->id }}" class="text-right">
                    <td class="text-center text-dark fs-6 ps-2">
                        {{ $nomor++ }}
                    </td>

                    <td class="text-dark fs-6">
                        <div class="mb-0">
                            <a class="fw-bold" href="#">
                                {{ $ld->statusLaporDiri->dataIndukTubelId->namaPegawai }}
                            </a>
                        </div>
                        <span class="fw-normal fs-6">{{ $ld->statusLaporDiri->dataIndukTubelId->nip18 }}</span>
                        <div class="mt-4 mb-0">
                            Pendidikan :
                        </div>
                        <div class="mb-2">
                            {{ $ld->statusLaporDiri->dataIndukTubelId->jenjangPendidikanId->jenjangPendidikan }}
                            - {{ $ld->statusLaporDiri->dataIndukTubelId->lokasiPendidikanId->lokasi }}
                        </div>
                    </td>

                    <td class="text-dark fs-6">
                        Tanggal Lulus
                        : {{ AppHelper::instance()->indonesian_date($ld->statusLaporDiri->tanggalLulus, 'j F Y', '') }}
                        <br>
                        IPK
                        : {{ AppHelper::instance()->indonesian_date($ld->statusLaporDiri->tanggalLulus, 'j F Y', '') }}
                        <div class="d-flex flex-aligns-center mt-4 pe-10 pe-lg-20">
                            <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                            <div class="ms-1">
                                <a href="{{ env('APP_URL') }}/files/{{ $ld->statusLaporDiri->pathLaporanSelesaiStudi }}"
                                   class="fs-6 text-hover-primary">Laporan Selesai Studi.pdf</a>
                            </div>
                        </div>
                    </td>
                    <td class="text-dark fs-6">
                        <div class="mb-3">
                            Karya Tulis : {{ $ld->statusLaporDiri->tipeDokPenelitian->tipeDokumen }}
                            <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1">
                                    <a href="{{ env('APP_URL') }}/files/{{ $ld->statusLaporDiri->pathDokumenPenelitian }}"
                                       class="fs-6 text-hover-primary">Dokumen Penelitian.pdf</a>
                                </div>
                            </div>
                        </div>
                    </td>

                    <td class="text-dark fs-6 ">
                        <a href="javascript:void(0)" class="text-primary btnTiket"
                           data-tiket="{{ $ld->logStatusProbis->nomorTiket }}"
                           data-nama="{{ $ld->statusLaporDiri->dataIndukTubelId->namaPegawai }}">
                            {{ $ld->logStatusProbis->nomorTiket }}
                        </a>
                    </td>

                    @if ($tab == 'menunggu-verifikasi')
                        <td class="text-center text-dark fs-6">

                            <button class="btn btn-sm btn-primary m-2 btnPeriksaIjazah" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Periksa"
                                    data-status="{{ $ld->statusLaporDiri->statusLaporDiri->status }}"
                                    data-id="{{ $ld->id }}"
                                    data-nama="{{ $ld->statusLaporDiri->dataIndukTubelId->namaPegawai }}"
                                    data-nip="{{ $ld->statusLaporDiri->dataIndukTubelId->nip18 }}"
                                    data-pangkat="{{ $ld->statusLaporDiri->dataIndukTubelId->namaPangkat }}"
                                    data-jabatan="{{ $ld->statusLaporDiri->dataIndukTubelId->namaJabatan }}"
                                    data-jenjang="{{ $ld->statusLaporDiri->dataIndukTubelId->jenjangPendidikanId->jenjangPendidikan }}"
                                    data-program_beasiswa="{{ $ld->statusLaporDiri->dataIndukTubelId->programBeasiswa }}"
                                    data-lokasi="{{ $ld->statusLaporDiri->dataIndukTubelId->lokasiPendidikanId->lokasi }}"
                                    data-st_tubel="{{ $ld->statusLaporDiri->dataIndukTubelId->nomorSt }}"
                                    data-tgl_st_tubel="{{ AppHelper::instance()->indonesian_date($ld->statusLaporDiri->dataIndukTubelId->tglSt, 'j F Y', '') }}"
                                    data-tgl_mulai="{{ AppHelper::instance()->indonesian_date($ld->statusLaporDiri->dataIndukTubelId->tglMulaiSt, 'j F Y', '') }}"
                                    data-tgl_selesai="{{ AppHelper::instance()->indonesian_date($ld->statusLaporDiri->dataIndukTubelId->tglSelesaiSt, 'j F Y', '') }}"
                                    data-kep_pembebasan="{{ $ld->statusLaporDiri->dataIndukTubelId->nomorKepPembebasan }}"
                                    data-tgl_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($ld->statusLaporDiri->dataIndukTubelId->tglKepPembebasan, 'j F Y', '') }}"
                                    data-tmt_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($ld->statusLaporDiri->dataIndukTubelId->tmtKepPembebasan, 'j F Y', '') }}"
                                    data-email="{{ $ld->statusLaporDiri->dataIndukTubelId->emailNonPajak }}"
                                    data-hp="{{ $ld->statusLaporDiri->dataIndukTubelId->emailNonPajak }}"
                                    data-alamat="{{ $ld->statusLaporDiri->dataIndukTubelId->alamatTinggal }}" @php
                                        $nama_pt = $ipk = $no_skl = $tgl_skl = $file_skl = $no_ijazah = $tgl_ijazah = $file_ijazah = $no_transkrip = $tgl_transkrip = $file_transkrip = 1;
                                    @endphp @foreach ($perguruantinggi as $key => $item)
                                @if ($key == $ld->statusLaporDiri->dataIndukTubelId->id)
                                    data-count_pt="{{ count($item) }}"

                                    @foreach ($item as $value)
                                        data-nama_pt_{{ $nama_pt++ }}="{{ $value['namaPerguruanTinggi'] }}"
                                    data-ipk_pt_{{ $ipk++ }}="{{ $value['ipk'] ?? '-' }}"
                                    data-no_skl_pt_{{ $no_skl++ }}="{{ $value['nomorSkl'] ?? '-' }}"
                                    data-tgl_skl_pt_{{ $tgl_skl++ }}="{{ $value['tglSkl'] ? AppHelper::instance()->indonesian_date($value['tglSkl'], 'j F Y', '') : '-' }}"
                                    data-file_skl_pt_{{ $file_skl++ }}="{{ $value['pathSkl'] ?? '-' }}"
                                    data-no_ijazah_pt_{{ $no_ijazah++ }}="{{ $value['nomorIjazah'] ?? '-' }}"
                                    data-tgl_ijazah_pt_{{ $tgl_ijazah++ }}="{{ $value['tglIjazah'] ? AppHelper::instance()->indonesian_date($value['tglIjazah'], 'j F Y', '') : '-' }}"
                                    data-file_ijazah_pt_{{ $file_ijazah++ }}="{{ $value['pathIjazah'] ?? '-' }}"
                                    data-no_transkrip_pt_{{ $no_transkrip++ }}="{{ $value['nomorTranskripNilai'] ?? '-' }}"
                                    data-tgl_transkrip_pt_{{ $tgl_transkrip++ }}="{{ $value['tglTranskripNilai'] ? AppHelper::instance()->indonesian_date($value['tglTranskripNilai'], 'j F Y', '') : '-' }}"
                                    data-file_transkrip_pt_{{ $file_transkrip++ }}="{{ $value['pathTranskripNilai'] ?? '-' }}"
                                @endforeach
                                @endif
                                @endforeach
                            >
                                <i class="fa fa-file"></i> Periksa
                            </button>
                        </td>
                    @endif
                </tr>
            @endforeach

        @else

            <tr class="text-center">
                <td class="text-dark  fs-6" colspan="6">Tidak terdapat data</td>
            </tr>

        @endif

        </tbody>
    </table>

</div>

@if ($totalItems != 0)
    <div class="d-flex flex-stack flex-wrap pt-5 p-3">
        <div class="fs-6 fw-bold text-gray-700"></div>

        @php
            $disabled = '';
            $nextPage = '';
            $disablednext = '';
            $hidden = '';
            $pagination = '';

            // tombol pagination
            if ($totalItems == 0) {
                $pagination = 'none';
            }

            // tombol back
            if ($currentPage == 1) {
                $disabled = 'disabled';
            }

            // tombol next
            if ($currentPage != $totalPages) {
                $nextPage = $currentPage + 1;
            }

            // tombol pagination
            if ($currentPage == $totalPages || $totalPages == 0) {
                $disablednext = 'disabled';
                $hidden = 'hidden';
            }

        @endphp

            <!--begin::Pages-->
        <ul class="pagination" style="display: {{ $pagination }}">
            <li class="page-item disabled"><a href="#" class="page-link">Halaman {{ $currentPage }}
                    dari {{ $totalPages }}</a></li>
            <li class="page-item previous {{ $disabled }}"><a
                    href="{{ url()->current() . '?page=' . ($currentPage - 1) }}" class="page-link"><i
                        class="previous"></i></a></li>
            <li class="page-item active"><a href="{{ url()->current() . '?page=' . $currentPage }}"
                                            class="page-link">{{ $currentPage }}</a></li>
            <li class="page-item" {{ $hidden }}><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                   class="page-link">{{ $nextPage }}</a></li>
            <li class="page-item next {{ $disablednext }}"><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                              class="page-link"><i class="next"></i></a></li>
        </ul>
        <!--end::Pages-->

    </div>
    <!--end::paging-->
@endif
