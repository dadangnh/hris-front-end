<div class="table-responsive bg-gray-400 rounded border border-gray-300">
    <table class="table table-striped  align-middle" id="tabel_tubel">
        <thead class="fw-bolder bg-secondary fs-6">
            <tr class="text-right">
                <th class="text-center ps-2">No</th>
                <th class="">Pegawai</th>
                <th colspan="2">Data Pendukung</th>
                <th class="">Tiket</th>
                @if ($tab == 'persetujuan')
                    <th class="text-center">Aksi</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @if ($lapordiri !== null)

                @php
                    $nomor = $currentPage == 1 ? 1 : ($currentPage - 1) * $size + 1;
                @endphp

                @foreach ($lapordiri as $ld)

                    <tr id="ld-{{ $ld->id }}" class="text-right">
                        <td class="text-center text-dark fs-6 ps-2">
                            {{ $nomor++ }}
                        </td>

                        <td class="text-dark fs-6">
                            <div class="mb-0">
                                <a class="fw-bold" href="#">
                                    {{ $ld->dataIndukTubelId->namaPegawai }}
                                </a>
                            </div>
                            <span class="fw-normal fs-6">{{ $ld->dataIndukTubelId->nip18 }}</span>
                            <div class="mt-4 mb-0">
                                Pendidikan :
                            </div>
                            <div class="mb-2">
                                {{ $ld->dataIndukTubelId->jenjangPendidikanId->jenjangPendidikan }} - {{ $ld->dataIndukTubelId->lokasiPendidikanId->lokasi }}
                            </div>
                            <div class="fw-bolder mt-4 mb-2">
                                <span class="d-inline-block position-relative">
                                    {{-- <h1>{{ strtoupper($ld->statusLaporDiri->status) }}</h1> --}}
                                    <h2>{{ $ld->statusLaporDiri->status }}</h2>
                                    @php
                                        switch ($ld->statusLaporDiri->status) {
                                            case 'Lulus':
                                                $color = 'success';
                                                break;
                                            case 'Belum Lulus':
                                                $color = 'warning';
                                                break;
                                            case 'Mengundurkan Diri':
                                                $color = 'danger';
                                                break;
                                            case 'Drop Out':
                                                $color = 'danger';
                                                break;
                                        }
                                    @endphp
                                    <span class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-{{ $color }} translate rounded"></span>
                                </span>
                            </div>
                        </td>

                        @switch($ld->statusLaporDiri->status)
                            @case('Lulus')
                                <td class="text-dark fs-6">
                                    <div class="mb-6">
                                        Tanggal Lulus : {{ AppHelper::instance()->indonesian_date($ld->tanggalLulus, 'j F Y', '') }}
                                        <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                            <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1">
                                                <a href="{{ env('APP_URL') }}/files/{{ $ld->pathLaporanSelesaiStudi }}" class="fs-6 text-hover-primary">Laporan Selesai Studi.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-dark fs-6">
                                    <div class="mb-3">
                                        Karya Tulis : {{ $ld->tipeDokPenelitian->tipeDokumen }}
                                        <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                            <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1">
                                                <a href="{{ env('APP_URL') }}/files/{{ $ld->pathDokumenPenelitian }}" class="fs-6 text-hover-primary">Dokumen Penelitian.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            @break

                            @case('Belum Lulus')
                                <td class="text-dark fs-6">
                                    <div class="mb-0">
                                        Tahap Perkuliahan:
                                        <br>
                                        {{ $ld->tahapBelumLulus->tahap ?? '' }}
                                    </div>
                                </td>
                                <td class="text-dark fs-6">
                                    <div class="mb-6">
                                        Timeline Rencana Penyelesaian Studi:
                                        <div class="d-flex flex-aligns-center mt-3 pe-10 pe-lg-20">
                                            <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1">
                                                <a href="{{ url('files/' . $ld->pathTimelineRencanaStudi) }}" target="_blank" class="fs-6 text-hover-primary">Timeline.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-6">
                                        Draft Karya Tulis: {{ $ld->tipeDokPenelitian->tipeDokumen }}
                                        <div class="d-flex flex-aligns-center mt-3 pe-10 pe-lg-20">
                                            <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1">
                                                <a href="{{ url('files/' . $ld->pathDokumenPenelitian) }}" target="_blank" class="fs-6 text-hover-primary">Draft Karya Tulis.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>

                            @break

                            @case('Mengundurkan Diri')
                                <td class="text-dark fs-6">
                                    <div>
                                        Semester: {{ $ld->semesterKe }}
                                        <br>
                                        Tahun: {{ $ld->tahunAkademik }}
                                    </div>
                                    <div class="mt-4">
                                        Alasan: {{ $ld->alasanMundur }}
                                    </div>
                                </td>
                                <td class="text-dark fs-6">
                                    <div class="mb-6">
                                        Surat Persetujuan dari Perguruan Tinggi
                                        <br>
                                        Nomor: {{ $ld->noSuratPersetujuanMundur }}
                                        <br>
                                        Tanggal: {{ AppHelper::instance()->indonesian_date($ld->tglSuratPersetujuanMundur, 'j F Y', '') }}
                                        <div class="d-flex flex-aligns-center mt-3 pe-10 pe-lg-20">
                                            <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1">
                                                <a href="{{ url('files/' . $ld->pathSuratPersetujuanMundur) }}" target="_blank" class="fs-6 text-hover-primary">Surat Persetujuan.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            @break

                            @case('Drop Out')
                                <td class="text-dark fs-6">
                                    <div class="mb-0">
                                        Semester: {{ $ld->semesterKe }}
                                        <br>
                                        Tahun: {{ $ld->tahunAkademik }}
                                    </div>
                                </td>
                                <td class="text-dark fs-6">
                                    <div class="mb-6">
                                        Surat Keterangan dari Perguruan Tinggi
                                        <br>
                                        Nomor: {{ $ld->nomorSuratDo }}
                                        <br>
                                        Tanggal: {{ AppHelper::instance()->indonesian_date($ld->tglSuratDo, 'j F Y', '') }}
                                        <div class="d-flex flex-aligns-center mt-3 pe-10 pe-lg-20">
                                            <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1">
                                                <a href="{{ url('files/' . $ld->pathSuratDo) }}" target="_blank" class="fs-6 text-hover-primary">Surat Keterangan.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            @break
                            @default

                        @endswitch
                        </td>

                        <td class="text-dark fs-6 ">
                            <a href="javascript:void(0)" class="text-primary btnTiket" data-tiket="{{ $ld->logStatusProbis->nomorTiket }}" data-nama="{{ $ld->dataIndukTubelId->namaPegawai }}">
                                {{ $ld->logStatusProbis->nomorTiket }}
                            </a>
                        </td>

                        @if ($tab == 'persetujuan')
                            <td class="text-center text-dark fs-6">

                                @switch($ld->statusLaporDiri->status)
                                    @case('Lulus')
                                        <button class="btn btn-sm btn-primary m-2 btnPeriksa" data-bs-toggle="tooltip" data-bs-placement="top" title="Periksa"
                                            data-status="{{ $ld->statusLaporDiri->status }}"
                                            data-pt="{{ count($perguruantinggi) }}"
                                            data-id="{{ $ld->id }}"
                                            data-tgl_kelulusan="{{ AppHelper::instance()->indonesian_date($ld->tanggalLulus, 'j F Y', '') }}"
                                            data-tipe_dokumen_penelitian="{{ $ld->tipeDokPenelitian->tipeDokumen }}"
                                            data-file_draft_dokumen_penelitian="{{ $ld->pathDokumenPenelitian }}" data-file_lps="{{ $ld->pathLaporanSelesaiStudi }}"

                                            @php
                                                $nama_pt = $ipk = $no_skl = $tgl_skl = $file_skl = $no_ijazah = $tgl_ijazah = $file_ijazah = $no_transkrip = $tgl_transkrip = $file_transkrip = 1;
                                            @endphp

                                            @foreach ($perguruantinggi as $key => $item)
                                                @if ($key == $ld->dataIndukTubelId->id)
                                                data-count_pt="{{ count($item) }}"

                                                @foreach ($item as $value)
                                                    data-nama_pt_{{ $nama_pt++ }}="{{ $value['namaPerguruanTinggi'] }}"
                                                    data-ipk_pt_{{ $ipk++ }}="{{ $value['ipk'] ?? '-' }}"
                                                    data-no_skl_pt_{{ $no_skl++ }}="{{ $value['nomorSkl'] ?? '-' }}"
                                                    data-tgl_skl_pt_{{ $tgl_skl++ }}="{{ $value['tglSkl'] ? AppHelper::instance()->indonesian_date($value['tglSkl'], 'j F Y', '') : '-' }}"
                                                    data-file_skl_pt_{{ $file_skl++ }}="{{ $value['pathSkl'] ?? '-' }}"
                                                    data-no_ijazah_pt_{{ $no_ijazah++ }}="{{ $value['nomorIjazah'] ?? '-' }}"
                                                    data-tgl_ijazah_pt_{{ $tgl_ijazah++ }}="{{ $value['tglIjazah'] ? AppHelper::instance()->indonesian_date($value['tglIjazah'], 'j F Y', '') : '-' }}"
                                                    data-file_ijazah_pt_{{ $file_ijazah++ }}="{{ $value['pathIjazah'] ?? '-' }}"
                                                    data-no_transkrip_pt_{{ $no_transkrip++ }}="{{ $value['nomorTranskripNilai'] ?? '-' }}"
                                                    data-tgl_transkrip_pt_{{ $tgl_transkrip++ }}="{{ $value['tglTranskripNilai'] ? AppHelper::instance()->indonesian_date($value['tglTranskripNilai'], 'j F Y', '') : '-' }}"
                                                    data-file_transkrip_pt_{{ $file_transkrip++ }}="{{ $value['pathTranskripNilai'] ?? '-' }}"
                                                @endforeach
                                            @endif
                                            @endforeach
                                            >
                                            <i class="fa fa-file"></i> Periksa
                                        </button>

                                    @break

                                    @case('Belum Lulus')
                                        <button class="btn btn-sm btn-primary m-2 btnPeriksa" data-bs-toggle="tooltip" data-bs-placement="top" title="Periksa" data-id="{{ $ld->id }}" data-status="{{ $ld->statusLaporDiri->status }}"
                                            data-tahapan_studi="{{ $ld->tahapBelumLulus->tahap }}" data-file_timeline="{{ $ld->pathTimelineRencanaStudi }}" data-tipe_dokumen_penelitian="{{ $ld->tipeDokPenelitian->tipeDokumen }}"
                                            data-file_draft_dokumen_penelitian="{{ $ld->pathDokumenPenelitian }}">
                                            <i class="fa fa-file"></i> Periksa
                                        </button>
                                    @break

                                    @case('Mengundurkan Diri')
                                        <button class="btn btn-sm btn-primary m-2 btnPeriksa" data-bs-toggle="tooltip" data-bs-placement="top" title="Periksa" data-id="{{ $ld->id }}" data-status="{{ $ld->statusLaporDiri->status }}"
                                            data-alasan_pengunduran_diri="{{ $ld->alasanMundur }}" data-mundur_semester="{{ $ld->semesterKe }}" data-tahun_akademik="{{ $ld->tahunAkademik }}"
                                            data-file_permohonan="{{ $ld->pathSuratPermohonanMundur }}" data-no_surat_persetujuan="{{ $ld->noSuratPersetujuanMundur }}"
                                            data-tgl_surat_persetujuan="{{ AppHelper::instance()->indonesian_date($ld->tglSuratPersetujuanMundur, 'j F Y', '') }}"
                                            data-file_dokumen_surat_persetujuan="{{ $ld->pathSuratPersetujuanMundur }}">
                                            <i class="fa fa-file"></i> Periksa
                                        </button>
                                    @break

                                    @case('Drop Out')
                                        <button class="btn btn-sm btn-primary m-2 btnPeriksa" data-bs-toggle="tooltip" data-bs-placement="top" title="Periksa" data-status="{{ $ld->statusLaporDiri->status }}" data-id="{{ $ld->id }}"
                                            data-status="{{ $ld->statusLaporDiri->status }}" data-semester_do="{{ $ld->semesterKe }}" data-tahun_do="{{ $ld->tahunAkademik }}" data-no_surat_do="{{ $ld->nomorSuratDo }}"
                                            data-tgl_surat_keterangan_do="{{ AppHelper::instance()->indonesian_date($ld->tglSuratDo, 'j F Y', '') }}" data-file_surat_keterangan_do="{{ $ld->pathSuratDo }}">
                                            <i class="fa fa-file"></i> Periksa
                                        </button>
                                    @break

                                @endswitch

                                @if ($tab == 'persetujuan')
                                    <br>
                                    <a href="#" class="btn btn-sm btn-info">
                                        <i class="fa fa-download"></i>Draft ND
                                    </a>
                                @endif

                            </td>
                        @endif


                    </tr>

                @endforeach

            @else

                <tr class="text-center">
                    <td class="text-dark  fs-6" colspan="6">Tidak terdapat data</td>
                </tr>

            @endif

        </tbody>
    </table>

</div>

@if ($totalItems != 0)
    <div class="d-flex flex-stack flex-wrap pt-5 p-3">
        <div class="fs-6 fw-bold text-gray-700"></div>

        @php
            $disabled = '';
            $nextPage = '';
            $disablednext = '';
            $hidden = '';
            $pagination = '';

            // tombol pagination
            if ($totalItems == 0) {
                $pagination = 'none';
            }

            // tombol back
            if ($currentPage == 1) {
                $disabled = 'disabled';
            }

            // tombol next
            if ($currentPage != $totalPages) {
                $nextPage = $currentPage + 1;
            }

            // tombol pagination
            if ($currentPage == $totalPages || $totalPages == 0) {
                $disablednext = 'disabled';
                $hidden = 'hidden';
            }

        @endphp

        <!--begin::Pages-->
        <ul class="pagination" style="display: {{ $pagination }}">
            <li class="page-item disabled"><a href="#" class="page-link">Halaman {{ $currentPage }} dari {{ $totalPages }}</a></li>
            <li class="page-item previous {{ $disabled }}"><a href="{{ url()->current() . '?page=' . ($currentPage - 1) }}" class="page-link"><i class="previous"></i></a></li>
            <li class="page-item active"><a href="{{ url()->current() . '?page=' . $currentPage }}" class="page-link">{{ $currentPage }}</a></li>
            <li class="page-item" {{ $hidden }}><a href="{{ url()->current() . '?page=' . $nextPage }}" class="page-link">{{ $nextPage }}</a></li>
            <li class="page-item next {{ $disablednext }}"><a href="{{ url()->current() . '?page=' . $nextPage }}" class="page-link"><i class="next"></i></a></li>
        </ul>
        <!--end::Pages-->

    </div>
    <!--end::paging-->
@endif
