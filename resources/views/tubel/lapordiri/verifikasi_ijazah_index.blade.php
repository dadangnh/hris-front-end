@extends('layout.master')

@section('content')

    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card border-warning">
                    <div class="card-header card-header-stretch" id="card-header">
                        <div class="card-toolbar">
                            <ul class="nav nav-tabs nav-line-tabs nav-stretch border-transparent fs-5 fw-bolder"
                                id="tabLps">
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'menunggu-verifikasi' ? 'active' : '' }}"
                                       href="{{ url('tubel/pelaporan/verifikasi-ijazah/menunggu-verifikasi') }}">
                                        Menunggu Verifikasi </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'ditolak' ? 'active' : '' }}"
                                       href="{{ url('tubel/pelaporan/verifikasi-ijazah/ditolak') }}"> Ditolak </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'terverifikasi' ? 'active' : '' }}"
                                       href="{{ url('tubel/pelaporan/verifikasi-ijazah/terverifikasi') }}">
                                        Terverifikasi Penempatan </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'dashboard' ? 'active' : '' }}"
                                       href="{{ url('tubel/pelaporan/verifikasi-ijazah/dashboard') }}"> Dashboard </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6 mb-4">
                                <div class="text-start" data-kt-customer-table-toolbar="base">
                                    <button type="button"
                                            class="btn btn-sm btn-ligth btn-active-primary border border-gray-400 fs-6"
                                            data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start"
                                            data-kt-menu-flip="top-end">
                                        <i class="fa fa-filter"></i> Advanced Search
                                    </button>
                                    <div class="menu menu-sub menu-sub-dropdown w-md-1000px border-warning mt-1"
                                         data-kt-menu="true">
                                        <div class="px-7 py-5">
                                            <div class="fs-4 text-dark fw-bolder">Filter Pencarian</div>
                                        </div>
                                        <div class="separator border-gray-200"></div>
                                        <form id="advancedSearch" action="" method="get">
                                            <div class="px-7 py-5">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="row mb-4">
                                                            <label class="form-label fw-bold">Pegawai:</label>
                                                            <div class="col-lg-6">
                                                                <input class="form-control form-select-solid"
                                                                       type="text" name="nama"
                                                                       placeholder="nama pegawai"
                                                                       value="{{ isset($_GET['nama']) ? $_GET['nama'] : '' }}">
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <input class="form-control form-select-solid"
                                                                       type="text" name="nip" placeholder="nip 18 digit"
                                                                       value="{{ isset($_GET['nip']) ? $_GET['nip'] : '' }}">
                                                            </div>
                                                        </div>
                                                        <div class="mb-4">
                                                            <label class="form-label fw-bold">Jenjang Pendidikan
                                                                :</label>
                                                            <select id="search_jenjang" class="form-select"
                                                                    name="jenjang" data-control="select2"
                                                                    data-hide-search="true">
                                                                <option></option>
                                                                @if (isset($_GET['jenjang']))
                                                                    @foreach ($jenjang as $j)
                                                                        <option
                                                                            value="{{ $j->id }}" {{ $_GET['jenjang'] == $j->id ? 'selected' : '' }}>{{ $j->jenjangPendidikan }}</option>
                                                                    @endforeach
                                                                @else
                                                                    @foreach ($jenjang as $j)
                                                                        <option
                                                                            value="{{ $j->id }}">{{ $j->jenjangPendidikan }}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <div class="mb-4">
                                                            <label class="form-label fw-bold">Lokasi Pendidikan:</label>
                                                            <select id="search_lokasi" class="form-select" name="lokasi"
                                                                    data-control="select2" data-hide-search="false">
                                                                <option></option>
                                                                @if (isset($_GET['lokasi']))
                                                                    @foreach ($lokasi as $l)
                                                                        <option
                                                                            value="{{ $l->id }}" {{ $_GET['lokasi'] == $l->id ? 'selected' : '' }}>{{ $l->lokasi }}</option>
                                                                    @endforeach
                                                                @else
                                                                    @foreach ($lokasi as $l)
                                                                        <option
                                                                            value="{{ $l->id }}">{{ $l->lokasi }}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="row mb-4">
                                                            <div class="col-lg-6">
                                                                <label class="form-label fs-5 fw-bold">Tahun
                                                                    Akademik</label>
                                                                <div class="d-flex flex-column">
                                                                    <div
                                                                        class="position-relative d-flex align-items-center">
                                                                        {{-- <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                                            <span class="symbol-label bg-secondary">
                                                                                <span class="svg-icon">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                                            <path
                                                                                                d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                                                fill="#000000"></path>
                                                                                        </g>
                                                                                    </svg>
                                                                                </span>
                                                                            </span>
                                                                        </div> --}}
                                                                        <input id="search_tahun"
                                                                               class="form-control text-start"
                                                                               placeholder="tahun laporan" name="tahun"
                                                                               type="text">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <label class="form-label fs-5 fw-bold">Tiket</label>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="tiket"
                                                                           placeholder="tiket"
                                                                           value="{{ isset($_GET['tiket']) ? $_GET['tiket'] : '' }}"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mb-8">

                                                            @php
                                                                $check = '';
                                                                $value = null;
                                                                $text = 'checked="checked"';
                                                                if (isset($_GET['semester'])) {
                                                                    if ($_GET['semester'] == 1) {
                                                                        $value = 1;
                                                                        $text = 'checked="checked"';
                                                                    } elseif ($_GET['semester'] == 2) {
                                                                        $value = 2;
                                                                        $text = 'checked="checked"';
                                                                    } elseif ($_GET['semester'] == null) {
                                                                        $value = null;
                                                                        $text = 'checked="checked"';
                                                                    }
                                                                }
                                                            @endphp

                                                            <label class="form-label fs-5 fw-bold mb-3">Semester</label>
                                                            <div class="d-flex flex-column flex-wrap fw-bold"
                                                                 data-kt-customer-table-filter="payment_type">
                                                                <label
                                                                    class="form-check form-check-sm form-check-custom form-check-solid mb-3 me-5">
                                                                    <input class="form-check-input" type="radio"
                                                                           name="semester"
                                                                           value="" {{ $value == null ? $text : '' }} />
                                                                    <span
                                                                        class="form-check-label text-gray-600">Semua</span>
                                                                </label>
                                                                <label
                                                                    class="form-check form-check-sm form-check-custom form-check-solid mb-3 me-5">
                                                                    <input class="form-check-input" type="radio"
                                                                           name="semester"
                                                                           value="1" {{ $value == 1 ? $text : '' }} />
                                                                    <span
                                                                        class="form-check-label text-gray-600">Ganjil</span>
                                                                </label>
                                                                <label
                                                                    class="form-check form-check-sm form-check-custom form-check-solid">
                                                                    <input class="form-check-input" type="radio"
                                                                           name="semester"
                                                                           value="2" {{ $value == 2 ? $text : '' }} />
                                                                    <span
                                                                        class="form-check-label text-gray-600">Genap</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-end">
                                                    <a href="{{ url()->current() }}"
                                                       class="btn btn-light btn-active-light-primary me-2">Reset</a>
                                                    <button type="submit" class="btn btn-sm btn-primary"
                                                            data-kt-menu-dismiss="true"
                                                            data-kt-customer-table-filter="filter">Search
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'menunggu-verifikasi' ? 'active show' : '' }}"
                                 id="menunggu-verifikasi" role="tabpanel">
                                @if ($tab == 'menunggu-verifikasi')
                                    @include('tubel.lapordiri.verifikasi_ijazah_tabel')
                                @endif
                            </div>

                            <div class="tab-pane fade {{ $tab == 'ditolak' ? 'active show' : '' }}" id="ditolak"
                                 role="tabpanel">
                                @if ($tab == 'ditolak')
                                    @include('tubel.lapordiri.verifikasi_ijazah_tabel')
                                @endif
                            </div>

                            <div class="tab-pane fade {{ $tab == 'terverifikasi' ? 'active show' : '' }}"
                                 id="terverifikasi" role="tabpanel">
                                @if ($tab == 'terverifikasi')
                                    @include('tubel.lapordiri.verifikasi_ijazah_tabel')
                                @endif
                            </div>


                            <div class="tab-pane fade {{ $tab == 'dashboard' ? 'active show' : '' }}" id="dashboard"
                                 role="tabpanel">
                                @if ($tab == 'dashboard')
                                    @include('tubel.lapordiri.verifikasi_ijazah_tabel')
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('tubel.modal_tiket')
    @include('tubel.lapordiri.verifikasi_ijazah_modal')

@endsection

@section('js')
    <script>
        let app_url = '{{ env('APP_URL') }}'

        $("#advancedSearch").submit(function (e) {
            e.preventDefault();

            var query = $(this).serializeArray().filter(function (i) {
                return i.value;
            });

            window.location.href = $(this).attr('action') + (query ? '?' + $.param(query) : '');

        })
    </script>

    <script src="{{ asset('assets/js/tubel') }}/lapor-diri.js"></script>
    <script src="{{ asset('assets/js/tubel') }}/log-tiket.js"></script>
@endsection
