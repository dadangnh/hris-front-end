<div class="modal fade" id="periksa_lapor_diri" aria-hidden="true">
    <div class="modal-dialog mw-1000px">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Form Penelitian Laporan Berakhir Masa Studi</h3>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body scroll-y px-5 px-lg-10">
                <div class="form_opsi form_hasil_studi_lulus" hidden>
                    <div class="fw-bolder mb-4 fs-3">
                        <span class="d-inline-block position-relative">
                            <h1>Lulus</h1>
                            <span
                                class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-success translate rounded"></span>
                        </span>
                    </div>
                    <div class="rounded border border-gray-300 p-5 d-flex flex-column mb-4">
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Kelulusan</label>
                            <div class="col-lg-4">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12" id="tgl_kelulusan_l_periksa" type="text" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Tipe Dokumen Penelitian</label>
                            <div class="col-lg-4">
                                <input class="form-control ipk_mask" type="text" id="opsi_dokumen_penelitian_l_periksa"
                                       disabled>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Penelitian</label>
                            <div class="col-lg">
                                <div class="col-lg">
                                    <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                        <img alt="" class="w-25px me-3"
                                             src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1 fw-bold">
                                            <a id="file_penelitian_l_periksa" href="" target="_blank"
                                               class="fs-6 text-hover-primary fw-bold">File Penelitian.pdf</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Laporan Selesai Studi</label>
                            <div class="col-lg">
                                <div class="col-lg">
                                    <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                        <img alt="" class="w-25px me-3"
                                             src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1 fw-bold">
                                            <a id="file_lps_l_periksa" href="" target="_blank"
                                               class="fs-6 text-hover-primary fw-bold">File Penelitian.pdf</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="data_pt_1" hidden>
                        <div class="rounded border border-gray-300 p-5 d-flex flex-column mb-4" hidden>
                            <h2 class="text-dark font-weight-bolder mb-4" id="nama_pt_1"></h2>
                            <div class="row form-group mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">IPK (skala 4)</label>
                                <div class="col-lg">
                                    <input class="form-control ipk_mask" id="ipk_periksa_ijazah_1" type="text" disabled>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor SKL</label>
                                <div class="col-lg">
                                    <input class="form-control" type="text" id="nomor_skl_periksa_ijazah_1" disabled>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal SKL</label>
                                <div class="col-lg-4">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                         width="24px"
                                                         height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none"
                                                           fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                  height="4" rx="1"></rect>
                                                            <path
                                                                d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12" id="tgl_skl_periksa_ijazah_1" type="text"
                                               disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen SKL</label>
                                <div class="col-lg">
                                    <div class="file_skl_1">
                                        <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                            <img alt="" class="w-25px me-3"
                                                 src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1 fw-bold">
                                                <a href="" target="_blank" id="file_skl_1"
                                                   class="fs-6 text-hover-primary fw-bold">Surat Keterangan
                                                    Lulus.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="msg_file_skl_1 ps-2 mt-2" hidden>
                                        <i>Belum ada</i>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Ijazah</label>
                                <div class="col-lg">
                                    <input class="form-control" type="text" id="nomor_ijazah_periksa_ijazah_1" disabled>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Ijazah</label>
                                <div class="col-lg-4">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                         width="24px"
                                                         height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none"
                                                           fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                  height="4" rx="1"></rect>
                                                            <path
                                                                d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12" id="tgl_ijazah_periksa_ijazah_1" type="text"
                                               disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Ijazah</label>
                                <div class="col-lg">
                                    <div class="file_ijazah_1">
                                        <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                            <img alt="" class="w-25px me-3"
                                                 src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1 fw-bold">
                                                <a href="" target="_blank" id="file_ijazah_1"
                                                   class="fs-6 text-hover-primary fw-bold">Ijazah.pdf</a>
                                            </div>
                                        </div>
                                        <input type="hidden" id="file_ijazah_lama_1" disabled>
                                    </div>
                                    <div class="msg_file_ijazah_1 ps-2 mt-2" hidden>
                                        <i>Belum ada</i>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Transkrip</label>
                                <div class="col-lg">
                                    <input class="form-control" type="text" id="nomor_transkrip_periksa_ijazah_1"
                                           disabled>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Transkrip</label>
                                <div class="col-lg-4">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                         width="24px"
                                                         height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none"
                                                           fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                  height="4" rx="1"></rect>
                                                            <path
                                                                d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12" id="tgl_transkrip_periksa_ijazah_1"
                                               type="text" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">File Transkrip</label>
                                <div class="col-lg">
                                    <div class="file_transkrip_1">
                                        <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                            <img alt="" class="w-25px me-3"
                                                 src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1 fw-bold">
                                                <a href="" target="_blank" id="file_transkrip_1"
                                                   class="fs-6 text-hover-primary fw-bold">Transkrip.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="msg_file_transkrip_1 ps-2 mt-2" hidden>
                                        <i>Belum ada</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="data_pt_2" hidden>
                        <div class="rounded border border-gray-300 p-5 d-flex flex-column mb-4" hidden>
                            <h2 class="text-dark font-weight-bolder mb-4" id="nama_pt_2"></h2>
                            <div class="row form-group mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">IPK (skala 4)</label>
                                <div class="col-lg">
                                    <input class="form-control ipk_mask" id="ipk_periksa_ijazah_2" type="text" disabled>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor SKL</label>
                                <div class="col-lg">
                                    <input class="form-control" type="text" id="nomor_skl_periksa_ijazah_2" disabled>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal SKL</label>
                                <div class="col-lg-4">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                         width="24px"
                                                         height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none"
                                                           fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                  height="4" rx="1"></rect>
                                                            <path
                                                                d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12" id="tgl_skl_periksa_ijazah_2" type="text"
                                               disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">File SKL</label>
                                <div class="col-lg">
                                    <div class="file_skl_2">
                                        <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                            <img alt="" class="w-25px me-3"
                                                 src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1 fw-bold">
                                                <a href="" target="_blank" id="file_skl_2"
                                                   class="fs-6 text-hover-primary fw-bold">Surat Keterangan
                                                    Lulus.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="msg_file_skl_2 ps-2 mt-2" hidden>
                                        <i>Belum ada</i>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Ijazah</label>
                                <div class="col-lg">
                                    <input class="form-control" type="text" id="nomor_ijazah_periksa_ijazah_2" disabled>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Ijazah</label>
                                <div class="col-lg-4">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                         width="24px"
                                                         height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none"
                                                           fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                  height="4" rx="1"></rect>
                                                            <path
                                                                d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12" id="tgl_ijazah_periksa_ijazah_2" type="text"
                                               disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">File Ijazah</label>
                                <div class="col-lg">
                                    <div class="file_ijazah_2">
                                        <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                            <img alt="" class="w-25px me-3"
                                                 src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1 fw-bold">
                                                <a href="" target="_blank" id="file_ijazah_2"
                                                   class="fs-6 text-hover-primary fw-bold">Ijazah.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="msg_file_ijazah_2 ps-2 mt-2" hidden>
                                        <i>Belum ada</i>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Transkrip</label>
                                <div class="col-lg">
                                    <input class="form-control" type="text" id="nomor_transkrip_periksa_ijazah_2"
                                           disabled>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Transkrip</label>
                                <div class="col-lg-4">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                         width="24px"
                                                         height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none"
                                                           fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                  height="4" rx="1"></rect>
                                                            <path
                                                                d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12" id="tgl_transkrip_periksa_ijazah_2"
                                               type="text" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">File Transkrip</label>
                                <div class="col-lg">
                                    <div class="file_transkrip_2">
                                        <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                            <img alt="" class="w-25px me-3"
                                                 src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1 fw-bold">
                                                <a href="" target="_blank" id="file_transkrip_2"
                                                   class="fs-6 text-hover-primary fw-bold">Transkrip.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="msg_file_transkrip_2 ps-2 mt-2" hidden>
                                        <i>Belum ada</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="data_pt_3" hidden>
                        <div class="rounded border border-gray-300 p-5 d-flex flex-column mb-4" hidden>
                            <h2 class="text-dark font-weight-bolder mb-4" id="nama_pt_3"></h2>
                            <div class="row form-group mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">IPK (skala 4)</label>
                                <div class="col-lg">
                                    <input class="form-control ipk_mask" id="ipk_periksa_ijazah_3" type="text" disabled>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor SKL</label>
                                <div class="col-lg">
                                    <input class="form-control" type="text" id="nomor_skl_periksa_ijazah_3" disabled>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal SKL</label>
                                <div class="col-lg-4">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                         width="24px"
                                                         height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none"
                                                           fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                  height="4" rx="1"></rect>
                                                            <path
                                                                d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12" id="tgl_skl_periksa_ijazah_3" type="text"
                                               disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">File SKL</label>
                                <div class="col-lg">
                                    <div class="file_skl_3">
                                        <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                            <img alt="" class="w-25px me-3"
                                                 src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1 fw-bold">
                                                <a href="" target="_blank" id="file_skl_3"
                                                   class="fs-6 text-hover-primary fw-bold">Surat Keterangan
                                                    Lulus.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="msg_file_skl_3 ps-2 mt-2" hidden>
                                        <i>Belum ada</i>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Ijazah</label>
                                <div class="col-lg">
                                    <input class="form-control" type="text" id="nomor_ijazah_periksa_ijazah_3" disabled>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Ijazah</label>
                                <div class="col-lg-4">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                         width="24px"
                                                         height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none"
                                                           fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                  height="4" rx="1"></rect>
                                                            <path
                                                                d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12" id="tgl_ijazah_periksa_ijazah_3" type="text"
                                               disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">File Ijazah</label>
                                <div class="col-lg">
                                    <div class="file_ijazah_3">
                                        <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                            <img alt="" class="w-25px me-3"
                                                 src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1 fw-bold">
                                                <a href="" target="_blank" id="file_ijazah_3"
                                                   class="fs-6 text-hover-primary fw-bold">Ijazah.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="msg_file_ijazah_3 ps-2 mt-2" hidden>
                                        <i>Belum ada</i>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Transkrip</label>
                                <div class="col-lg">
                                    <input class="form-control" type="text" id="nomor_transkrip_periksa_ijazah_3"
                                           disabled>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Transkrip</label>
                                <div class="col-lg-4">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                         width="24px"
                                                         height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none"
                                                           fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                  height="4" rx="1"></rect>
                                                            <path
                                                                d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12" id="tgl_transkrip_periksa_ijazah_3"
                                               type="text" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">File Transkrip</label>
                                <div class="col-lg">
                                    <div class="file_transkrip">
                                        <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                            <img alt="" class="w-25px me-3"
                                                 src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                            <div class="ms-1 fw-bold">
                                                <a href="" target="_blank" id="file_transkrip_3"
                                                   class="fs-6 text-hover-primary fw-bold">Transkrip.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="form_opsi form_hasil_studi_belum_lulus" hidden>
                    <div class="fw-bolder mb-4 fs-3">
                        <span class="d-inline-block position-relative">
                            <h1>Belum Lulus</h1>
                            <span
                                class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-warning translate rounded"></span>
                        </span>
                    </div>
                    <div class="rounded border border-gray-300 p-4 d-flex flex-column mt-6">
                        <div class="row mb-3 mt-4">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Tahapan Studi</label>
                            <div class="col-lg">
                                <input id="tahapan_studi_bl_periksa" type="text" class="form-control" disabled/>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Tipe Dokumen Penelitian</label>
                            <div class="col-lg">
                                <input id="tipe_dokumen_penelitian_periksa" type="text" class="form-control" disabled/>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Draft Dokumen Penelitian</label>
                            <div class="col-lg">
                                <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                    <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                    <div class="ms-1 fw-bold">
                                        <a id="file_draft_dokumen_penelitian_bl_periksa" href="" target="_blank"
                                           class="fs-6 text-hover-primary fw-bold">Draft Dokumen Penelitian.pdf</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Timeline Rencana Penyelesaian
                                Studi</label>
                            <div class="col-lg">
                                <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                    <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                    <div class="ms-1 fw-bold">
                                        <a id="file_timeline_bl_periksa" href="" target="_blank"
                                           class="fs-6 text-hover-primary fw-bold">Timeline.pdf</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_opsi form_hasil_studi_drop_out" hidden>
                    <div class="fw-bolder mb-4 fs-3">
                        <span class="d-inline-block position-relative">
                            <h1>Drop Out</h1>
                            <span
                                class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-danger translate rounded"></span>
                        </span>
                    </div>
                    <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6 mt-6">
                        <div class="row mb-3 mt-4">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">DO Pada Semester</label>
                            <div class="col-lg">
                                <input type="text" class="form-control" id="semester_do_periksa" disabled/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Tahun Akademik</label>
                            <div class="col-lg">
                                <input type="text" class="form-control" id="tahun_do_periksa" disabled/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">No Surat Keterangan</label>
                            <div class="col-lg">
                                <input type="text" class="form-control" id="no_surat_do_periksa" disabled/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Surat Keterangan</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12" id="tgl_surat_keterangan_do_periksa" type="text"
                                           disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen</label>
                            <div class="col-lg">
                                <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                    <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                    <div class="ms-1 fw-bold">
                                        <a id="file_surat_keterangan_do_periksa" href="" target="_blank"
                                           class="fs-6 text-hover-primary fw-bold">Surat Keterangan.pdf</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_opsi form_hasil_studi_mengundurkan_diri" hidden>
                    <div class="fw-bolder mb-4 fs-3">
                        <span class="d-inline-block position-relative">
                            <h1>Mengundurkan Diri</h1>
                            <span
                                class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-danger translate rounded"></span>
                        </span>
                    </div>
                    <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6 mt-6">
                        <div class="row mb-3 mt-4">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Alasan Pengunduran diri</label>
                            <div class="col-lg">
                                <textarea class="form-control" type="text" id="alasan_pengunduran_diri_md_periksa"
                                          rows="4" placeholder="isi alasan pengunduran diri" disabled></textarea>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Mundur Pada Semester</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control semester_mask" id="mundur_semester_md_periksa"
                                       placeholder="semester" disabled/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Tahun Akademik</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control tahun_mask" id="tahun_akademik_md_periksa"
                                       placeholder="tahun" disabled/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Surat Permohonan</label>
                            <div class="col-lg">
                                <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                    <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                    <div class="ms-1 fw-bold">
                                        <a id="file_permohonan_md_periksa" href="" target="_blank"
                                           class="fs-6 text-hover-primary fw-bold">Surat Permohonan.pdf</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Surat persetujuan</label>
                            <div class="col-lg">
                                <input type="text" class="form-control form-control"
                                       id="no_surat_persetujuan_md_periksa" placeholder="nomor surat" disabled/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Surat Persetujuan</label>
                            <div class="col-lg-4">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12" id="tgl_surat_persetujuan_md_periksa"
                                           placeholder="Pilih tanggal surat" type="text" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Surat Persetujuan</label>
                            <div class="col-lg">
                                <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                    <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                    <div class="ms-1 fw-bold">
                                        <a id="file_dokumen_surat_persetujuan_md_periksa" href="" target="_blank"
                                           class="fs-6 text-hover-primary fw-bold">Surat Persetujuan.pdf</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center mb-2 mt-10">
                    <input id="id_lapor_diri" type="hidden"/>
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                            class="fa fa-reply"></i>Batal
                    </button>
                    <button type="button" class="btn btn-sm btn-danger btnTolak" data-id=""><i class="fa fa-times"></i>Tolak
                    </button>
                    <button type="button" class="btn btn-sm btn-success btnSetuju" data-id=""><i
                            class="fa fa-check-circle"></i>Setuju
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
