<div class="modal fade" id="modal_periksa_ijazah" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Form Verifikasi Ijazah</h3>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body scroll-y px-5 px-lg-10">
                <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                    <div class="row mb-2">
                        <div class="col-md col-lg">
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Nama</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span class="get_nama fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">NIP 18</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span class="get_nip fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Pangkat</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span class="get_pangkat fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row mb-6">
                                <label class="col-lg-4 col-md fw-bold fs-6">Jabatan</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span class="get_jabatan fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Jenjang Pendidikan</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span class="get_jenjang fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Program Beasiswa</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span class="get_program_beasiswa fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Lokasi Pendidikan</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span class="get_lokasi fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md col-lg">
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">ST Tubel</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span class="get_st_tubel fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Tgl ST</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span class="get_tgl_st_tubel fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Tgl Mulai & Selesai</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span class="get_tgl_mulai_selesai fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">KEP Pembebasan</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span class="get_kep_pembebasan fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Tgl KEP</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span class="get_tgl_kep_pembebasan fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row mb-6">
                                <label class="col-lg-4 col-md fw-bold fs-6">TMT KEP</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span class="get_tmt_kep_pembebasan fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Email</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span class="get_email fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Nomor HP</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span class="get_hp fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Alamat</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span class="get_alamat fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="data_pt_1" hidden>
                    <div class="rounded border border-gray-300 p-5 d-flex flex-column mb-4" hidden>
                        <h2 class="text-dark font-weight-bolder mb-4" id="nama_pt_1"></h2>
                        {{-- <div class="row form-group mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">IPK (skala 4)</label>
                            <div class="col-lg">
                                <input class="form-control ipk_mask" id="ipk_periksa_ijazah_1" type="text" disabled>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor SKL</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="nomor_skl_periksa_ijazah_1" disabled>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal SKL</label>
                            <div class="col-lg-4">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12" id="tgl_skl_periksa_ijazah_1" type="text" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">File SKL</label>
                            <div class="col-lg">
                                <div class="file_skl">
                                    <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                        <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1 fw-bold">
                                            <a href="" target="_blank" id="file_skl_1" class="fs-6 text-hover-primary fw-bold">Surat Keterangan Lulus.pdf</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Ijazah</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="nomor_ijazah_periksa_ijazah_1" disabled>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Ijazah</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12" id="tgl_ijazah_periksa_ijazah_1" type="text"
                                           disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">File Ijazah</label>
                            <div class="col-lg">
                                <div class="file_ijazah">
                                    <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                        <img alt="" class="w-25px me-3"
                                             src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1 fw-bold">
                                            <a href="" target="_blank" id="file_ijazah_1"
                                               class="fs-6 text-hover-primary fw-bold">Ijazah.pdf</a>
                                        </div>
                                    </div>
                                    <input type="hidden" id="file_ijazah_lama_1" disabled>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Transkrip</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="nomor_transkrip_periksa_ijazah_1" disabled>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Transkrip</label>
                            <div class="col-lg-4">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12" id="tgl_transkrip_periksa_ijazah_1" type="text" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">File Transkrip</label>
                            <div class="col-lg">
                                <div class="file_transkrip">
                                    <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                        <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1 fw-bold">
                                            <a href="" target="_blank" id="file_transkrip_1" class="fs-6 text-hover-primary fw-bold">Transkrip.pdf</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
                <div class="data_pt_2" hidden>
                    <div class="rounded border border-gray-300 p-5 d-flex flex-column mb-4" hidden>
                        <h2 class="text-dark font-weight-bolder mb-4" id="nama_pt_2"></h2>
                        {{-- <div class="row form-group mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">IPK (skala 4)</label>
                            <div class="col-lg">
                                <input class="form-control ipk_mask" id="ipk_periksa_ijazah_2" type="text" disabled>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor SKL</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="nomor_skl_periksa_ijazah_2" disabled>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal SKL</label>
                            <div class="col-lg-4">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12" id="tgl_skl_periksa_ijazah_2" type="text" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">File SKL</label>
                            <div class="col-lg">
                                <div class="file_skl">
                                    <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                        <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1 fw-bold">
                                            <a href="" target="_blank" id="file_skl_2" class="fs-6 text-hover-primary fw-bold">Surat Keterangan Lulus.pdf</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Ijazah</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="nomor_ijazah_periksa_ijazah_2" disabled>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Ijazah</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12" id="tgl_ijazah_periksa_ijazah_2" type="text"
                                           disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">File Ijazah</label>
                            <div class="col-lg">
                                <div class="file_ijazah">
                                    <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                        <img alt="" class="w-25px me-3"
                                             src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1 fw-bold">
                                            <a href="" target="_blank" id="file_ijazah_2"
                                               class="fs-6 text-hover-primary fw-bold">Ijazah.pdf</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Transkrip</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="nomor_transkrip_periksa_ijazah_2" disabled>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Transkrip</label>
                            <div class="col-lg-4">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12" id="tgl_transkrip_periksa_ijazah_2" type="text" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">File Transkrip</label>
                            <div class="col-lg">
                                <div class="file_transkrip">
                                    <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                        <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1 fw-bold">
                                            <a href="" target="_blank" id="file_transkrip_2" class="fs-6 text-hover-primary fw-bold">Transkrip.pdf</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
                <div class="data_pt_3" hidden>
                    <div class="rounded border border-gray-300 p-5 d-flex flex-column mb-4" hidden>
                        <h2 class="text-dark font-weight-bolder mb-4" id="nama_pt_3"></h2>
                        {{-- <div class="row form-group mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">IPK (skala 4)</label>
                            <div class="col-lg">
                                <input class="form-control ipk_mask" id="ipk_periksa_ijazah_3" type="text" disabled>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor SKL</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="nomor_skl_periksa_ijazah_3" disabled>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal SKL</label>
                            <div class="col-lg-4">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12" id="tgl_skl_periksa_ijazah_3" type="text" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">File SKL</label>
                            <div class="col-lg">
                                <div class="file_skl">
                                    <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                        <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1 fw-bold">
                                            <a href="" target="_blank" id="file_skl_3" class="fs-6 text-hover-primary fw-bold">Surat Keterangan Lulus.pdf</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Ijazah</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="nomor_ijazah_periksa_ijazah_3" disabled>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Ijazah</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12" id="tgl_ijazah_periksa_ijazah_3" type="text"
                                           disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">File Ijazah</label>
                            <div class="col-lg">
                                <div class="file_ijazah">
                                    <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                        <img alt="" class="w-25px me-3"
                                             src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1 fw-bold">
                                            <a href="" target="_blank" id="file_ijazah_3"
                                               class="fs-6 text-hover-primary fw-bold">Ijazah.pdf</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Transkrip</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="nomor_transkrip_periksa_ijazah_3" disabled>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Transkrip</label>
                            <div class="col-lg-4">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12" id="tgl_transkrip_periksa_ijazah_3" type="text" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">File Transkrip</label>
                            <div class="col-lg">
                                <div class="file_transkrip">
                                    <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                        <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1 fw-bold">
                                            <a href="" target="_blank" id="file_transkrip_3" class="fs-6 text-hover-primary fw-bold">Transkrip.pdf</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
            <div class="text-center mb-10">
                <input id="id_hasil_studi" name="id_hasil_studi" type="hidden"/>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                        class="fa fa-reply"></i>Batal
                </button>
                <button type="button" class="btn btn-sm btn-danger btnTolakverifikasiIjazah" data-id=""><i
                        class="fa fa-times"></i>Tolak
                </button>
                <button type="button" class="btn btn-sm btn-success btnSetujuverifikasiIjazah" data-id=""><i
                        class="fa fa-check-circle"></i>Setuju
                </button>
            </div>

        </div>
    </div>
</div>
