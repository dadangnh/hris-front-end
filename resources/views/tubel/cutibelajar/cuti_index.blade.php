@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card border-warning">
                    <div class="card-header card-header-stretch" id="card-header">
                        <div class="card-toolbar">
                            <ul class="nav nav-tabs nav-line-tabs nav-stretch border-transparent fs-5 fw-bolder"
                                id="tabLps">
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'menunggu_persetujuan' ? 'active' : '' }}"
                                       href="{{ url('tubel/cuti') }}"> Menunggu Persetujuan </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'disetujui' ? 'active' : '' }}"
                                       href="{{ url('tubel/cuti/setuju') }}"> Telah Disetujui </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'ditolak' ? 'active' : '' }}"
                                       href="{{ url('tubel/cuti/tolak') }}"> Ditolak </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'selesai' ? 'active' : '' }}"
                                       href="{{ url('tubel/cuti/selesai') }}"> Telah Ditempatkan (Cuti Belajar) </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'dashboard' ? 'active' : '' }}"
                                       href="{{ url('tubel/cuti/dashboard') }}"> Dashboard </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'menunggu_persetujuan' ? 'active show' : '' }} "
                                 id="menunggu_persetujuan" role="tabpanel">
                                @if ($tab == 'menunggu_persetujuan')
                                    @include('tubel.cutibelajar.cuti_tabel')
                                @endif
                            </div>

                            <div class="tab-pane fade {{ $tab == 'disetujui' ? 'active show' : '' }} " id="disetujui"
                                 role="tabpanel">
                                @if ($tab == 'disetujui')
                                    @include('tubel.cutibelajar.cuti_tabel')
                                @endif
                            </div>

                            <div class="tab-pane fade {{ $tab == 'ditolak' ? 'active show' : '' }} " id="ditolak"
                                 role="tabpanel">
                                @if ($tab == 'ditolak')
                                    @include('tubel.cutibelajar.cuti_tabel')
                                @endif
                            </div>

                            <div class="tab-pane fade {{ $tab == 'selesai' ? 'active show' : '' }} " id="selesai"
                                 role="tabpanel">
                                @if ($tab == 'selesai')
                                    @include('tubel.cutibelajar.cuti_tabel')
                                @endif
                            </div>

                            <div class="tab-pane fade {{ $tab == 'dashboard' ? 'active show' : '' }} " id="dashboard"
                                 role="tabpanel">
                                @if ($tab == 'dashboard')
                                    @include('tubel.cutibelajar.cuti_tabel')
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('tubel.cutibelajar.cuti_modal')

    @include('tubel.modal_tiket')

@endsection

@section('js')
    <script>
        let app_url = '{{ env('APP_URL') }}'

        // advanced search
        $("#advancedSearch").submit(function (e) {
            e.preventDefault();

            var query = $(this).serializeArray().filter(function (i) {
                return i.value;
            });

            window.location.href = $(this).attr('action') + (query ? '?' + $.param(query) : '');

        })
    </script>

    <script src="{{ asset('assets/js/tubel') }}/cuti-belajar.js"></script>
    <script src="{{ asset('assets/js/tubel') }}/log-tiket.js"></script>
@endsection
