@php
    $halaman = isset($_GET['page']) ? (int)$_GET['page'] : 1;
    $halaman_awal = ($halaman > 1) ? ($halaman * $size) - $size : 0;
    $nomor = $halaman_awal + 1;
@endphp

<div class="row">
    <div class="col-lg-6 mb-4">
        <div class="text-start" data-kt-customer-table-toolbar="base">
            <button type="button" class="btn btn-sm btn-ligth btn-active-primary border border-gray-400 fs-6"
                    data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" data-kt-menu-flip="top-end">
                <i class="fa fa-filter"></i> Advanced Search
            </button>
            <div class="menu menu-sub menu-sub-dropdown w-md-1000px border-warning mt-1" data-kt-menu="true">
                <div class="px-7 py-5">
                    <div class="fs-4 text-dark fw-bolder">Filter Pencarian</div>
                </div>
                <div class="separator border-gray-200"></div>
                <form id="advancedSearch" action="" method="get">
                    <div class="px-7 py-5">
                        <div class="row">
                            <div class="col-lg-6">
                                <label class="form-label fw-bold">Pegawai:</label>
                                <div class="row mb-4">
                                    <div class="col-lg-6">
                                        <input class="form-control form-select-solid" type="text" name="nama"
                                               placeholder="nama pegawai"
                                               value="{{ isset($_GET['nama']) ? $_GET['nama'] : '' }}">
                                    </div>
                                    <div class="col-lg-6">
                                        <input class="form-control form-select-solid" type="text" name="nip"
                                               placeholder="nip 18 digit"
                                               value="{{ isset($_GET['nip']) ? $_GET['nip'] : '' }}">
                                    </div>
                                </div>
                                <label class="form-label fw-bold">Jenjang Pendidikan:</label>
                                <div class="mb-4">
                                    <select id="search_jenjang" class="form-select" name="jenjang"
                                            data-control="select2" data-hide-search="true">
                                        <option></option>
                                        @if (isset($_GET['jenjang']))
                                            @foreach ($jenjang as $j)
                                                <option
                                                    value="{{ $j->id }}" {{ $_GET['jenjang'] == $j->id ? 'selected' : '' }}>{{ $j->jenjangPendidikan }}</option>
                                            @endforeach
                                        @else
                                            @foreach ($jenjang as $j)
                                                <option value="{{ $j->id }}">{{ $j->jenjangPendidikan }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <label class="form-label fw-bold">Lokasi Pendidikan:</label>
                                <div class="mb-4">
                                    <select id="search_lokasi" class="form-select" name="lokasi" data-control="select2"
                                            data-hide-search="false">
                                        <option></option>
                                        @foreach ($lokasi as $l)
                                            <option value="{{ $l->id }}">{{ $l->lokasi }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row mb-4">
                                    <div class="col-lg-6">
                                        <label class="form-label fw-bold">No.ST:</label>
                                        <input class="form-control form-select-solid" type="text" name="nost"
                                               placeholder="nomor st"
                                               value="{{ isset($_GET['nost']) ? $_GET['nost'] : '' }}">
                                    </div>
                                    <div class="col-lg-6">
                                        <label class="form-label fw-bold">KEP Pembebesan:</label>
                                        <input class="form-control form-select-solid" type="text" name="kep"
                                               placeholder="kep pembebasan"
                                               value="{{ isset($_GET['kep']) ? $_GET['kep'] : '' }}">
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col-lg-12">
                                        <label class="form-label fw-bold">Tiket:</label>
                                        <input class="form-control form-select-solid" type="text" name="tiket"
                                               placeholder="tiket"
                                               value="{{ isset($_GET['tiket']) ? $_GET['tiket'] : '' }}">
                                    </div>
                                </div>
                                <div class="row mb-8">
                                    <label class="form-label fw-bold">Tanggal Pengaktifan:</label>
                                    <div class="input-daterange input-group">
                                        <input type="text" class="form-control text-start date-search" name="tgl_mulai"
                                               placeholder="tgl mulai aktif"
                                               value="{{ isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : '' }}"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                s.d.
                                            </span>
                                        </div>
                                        <input type="text" class="form-control text-start date-search"
                                               name="tgl_selesai" placeholder="tgl selesai aktif"
                                               value="{{ isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : '' }}"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <a href="{{ url()->current() }}"
                               class="btn btn-light btn-active-light-primary me-2">Reset</a>
                            <button type="submit" class="btn btn-sm btn-primary" data-kt-menu-dismiss="true"
                                    data-kt-customer-table-filter="filter">Search
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="table-responsive bg-gray-400 rounded border border-gray-300">
    <table class="table table-striped align-middle" id="tabel_keppengaktifan_kembali">
        <thead class="fw-bolder bg-secondary fs-6">
        <tr class="">
            <th class="ps-4">No</th>
            <th class="">Pegawai</th>
            <th class="">Pendidikan</th>
            <th class="">Tgl Mulai Aktif</th>
            <th class="">Surat Kembali Aktif</th>
            <th class="">No Tiket</th>
            <th class="text-center">Aksi</th>
        </tr>
        </thead>
        <tbody>
        @if ($pengaktifan_kembali->data != null)

            @foreach ($pengaktifan_kembali->data as $t)
                <tr class="text-right">

                    <td class="ps-4 fs-6">
                        {{ $nomor++ }}
                    </td>

                    <td class="text-dark fw-bolder mb-1 fs-6">
                        <a class="fw-bolder"
                           href="{{ url('tubel/monitoring/'. $t->indukTubel->id . '?tab=pengaktifan') }}">
                            {{$t->indukTubel->namaPegawai}}
                        </a>
                        <br>
                        <span class="text-muted fw-bold text-muted fs-6">{{ $t->indukTubel->nip18 }}</span>
                    </td>

                    <td class="text-dark fw-bolder mb-1 fs-6">
                        {{ $t->indukTubel->jenjangPendidikanId->jenjangPendidikan }}
                        <br>
                        <span
                            class="text-muted fw-bold text-muted fs-6">{{ $t->indukTubel->lokasiPendidikanId ? $t->indukTubel->lokasiPendidikanId->lokasi : '' }}</span>
                    </td>

                    <td class="text-dark fw-bolder mb-1 fs-6">
                        {{ AppHelper::instance()->indonesian_date($t->tglMulaiAktif,'j F Y','') }}
                    </td>

                    <td class="text-dark fw-bolder mb-1 fs-6">
                        {{ $t->nomorSuratPengaktifanKembaliKampus }}
                        <br>
                        <span
                            class="text-muted fw-bold text-muted fs-6">{{ AppHelper::instance()->indonesian_date($t->tglSuratPengaktifanKembaliKampus,'j F Y','') }}</span>
                    </td>

                    <td class="text-dark fw-bolder mb-1 fs-6">
                        <a href="javascript:void(0)" class="text-primary btnTiket"
                           data-tiket="{{ $t->logStatusProbis->nomorTiket }}"
                           data-nama="{{ $t->indukTubel->namaPegawai }}">
                            {{ $t->logStatusProbis->nomorTiket }}
                        </a>
                    </td>

                    <td class="text-center text-dark  mb-1 fs-6">

                        @if ($t->logStatusProbis->output == 'Permohonan aktif dikirim')

                            <button class="btn btn-sm btn-primary m-2 btnPeriksaPermohonanAktif"
                                    data-id="{{ $t->id }}"
                                    data-nama="{{ $t->indukTubel->namaPegawai }}"
                                    data-nip="{{ $t->indukTubel->nip18 }}"
                                    data-pangkat="{{ $t->indukTubel->namaPangkat }}"
                                    data-jabatan="{{ $t->indukTubel->namaJabatan }}"
                                    data-jenjang="{{ $t->indukTubel->jenjangPendidikanId->jenjangPendidikan }}"
                                    data-program_beasiswa="{{ $t->indukTubel->programBeasiswa }}"
                                    data-lokasi="{{ $t->indukTubel->lokasiPendidikanId ? $t->indukTubel->lokasiPendidikanId->lokasi : '' }}"
                                    {{-- data-pt="{{ $t->indukTubel->perguruanTinggiId->namaPerguruanTinggi }}" --}}
                                    {{-- data-prodi="{{ $t->indukTubel->programStudiId->programStudi }}" --}}
                                    data-st_tubel="{{ $t->indukTubel->nomorSt }}"
                                    data-tgl_st_tubel="{{AppHelper::instance()->indonesian_date($t->indukTubel->tglSt,'j F Y','') }}"
                                    data-tgl_mulai="{{AppHelper::instance()->indonesian_date($t->indukTubel->tglMulaiSt,'j F Y','')}}"
                                    data-tgl_selesai="{{AppHelper::instance()->indonesian_date($t->indukTubel->tglSelesaiSt,'j F Y','')}}"
                                    data-kep_pembebasan="{{ $t->indukTubel->nomorKepPembebasan }}"
                                    data-tgl_kep_pembebasan="{{AppHelper::instance()->indonesian_date($t->indukTubel->tglKepPembebasan,'j F Y','')}}"
                                    data-tmt_kep_pembebasan="{{AppHelper::instance()->indonesian_date($t->indukTubel->tmtKepPembebasan,'j F Y','')}}"
                                    data-email="{{ $t->indukTubel->emailNonPajak }}"
                                    data-hp="{{ $t->indukTubel->noHandphone }}"
                                    data-alamat="{{ $t->indukTubel->alamatTinggal }}"
                                    data-tglmulaiaktif="{{AppHelper::instance()->indonesian_date($t->tglMulaiAktif,'j F Y','') }}"
                                    data-nosuratkembali="{{ $t->nomorSuratPengaktifanKembaliKampus }}"
                                    data-tglsurat="{{AppHelper::instance()->indonesian_date($t->tglSuratPengaktifanKembaliKampus,'j F Y','') }}"
                                    data-file="{{ $t->pathSuratPengaktifanKembaliKampus }}"

                                    data-bs-toggle="tooltip" data-bs-placement="top" title="Periksa">
                                <i class="fa fa-copy"></i> Periksa
                            </button>

                        @else

                            <button class="btn btn-icon btn-sm btn-info m-2 btnLihatPermohonanAktif"
                                    data-bs-toggle="tooltip" data-bs-placement="top" title="Lihat"
                                    data-nama_lihat="{{ $t->indukTubel->namaPegawai }}"
                                    data-nip_lihat="{{ $t->indukTubel->nip18 }}"
                                    data-pangkat_lihat="{{ $t->indukTubel->namaPangkat }}"
                                    data-jabatan_lihat="{{ $t->indukTubel->namaJabatan }}"
                                    data-jenjang_lihat="{{ $t->indukTubel->jenjangPendidikanId->jenjangPendidikan }}"
                                    data-program_beasiswa_lihat="{{ $t->indukTubel->programBeasiswa }}"
                                    data-lokasi_lihat="{{ $t->indukTubel->lokasiPendidikanId ? $t->indukTubel->lokasiPendidikanId->lokasi : '' }}"
                                    {{-- data-pt_lihat="{{ $t->indukTubel->perguruanTinggiId->namaPerguruanTinggi }}" --}}
                                    {{-- data-prodi_lihat="{{ $t->indukTubel->programStudiId->programStudi }}" --}}
                                    data-st_tubel_lihat="{{ $t->indukTubel->nomorSt }}"
                                    data-tgl_st_tubel_lihat="{{AppHelper::instance()->indonesian_date($t->indukTubel->tglSt,'j F Y','') }}"
                                    data-tgl_mulai_lihat="{{AppHelper::instance()->indonesian_date($t->indukTubel->tglMulaiSt,'j F Y','')}}"
                                    data-tgl_selesai_lihat="{{AppHelper::instance()->indonesian_date($t->indukTubel->tglSelesaiSt,'j F Y','')}}"
                                    data-kep_pembebasan_lihat="{{ $t->indukTubel->nomorKepPembebasan }}"
                                    data-tgl_kep_pembebasan_lihat="{{AppHelper::instance()->indonesian_date($t->indukTubel->tglKepPembebasan,'j F Y','')}}"
                                    data-tmt_kep_pembebasan_lihat="{{AppHelper::instance()->indonesian_date($t->indukTubel->tmtKepPembebasan,'j F Y','')}}"
                                    data-email_lihat="{{ $t->indukTubel->emailNonPajak }}"
                                    data-hp_lihat="{{ $t->indukTubel->noHandphone }}"
                                    data-alamat_lihat="{{ $t->indukTubel->alamatTinggal }}"
                                    data-tglmulaiaktif_lihat="{{AppHelper::instance()->indonesian_date($t->tglMulaiAktif,'j F Y','') }}"
                                    data-nosuratkembali_lihat="{{ $t->nomorSuratPengaktifanKembaliKampus }}"
                                    data-tglsurat_lihat="{{AppHelper::instance()->indonesian_date($t->tglSuratPengaktifanKembaliKampus,'j F Y','') }}"
                                    data-file_lihat="{{ $t->pathSuratPengaktifanKembaliKampus }}"
                            >
                                <i class="fa fa-eye"></i>
                            </button>

                            {{-- @elseif ($tab == 'terbitst')

                                <button class="btn btn-icon btn-sm btn-info m-2 btnLihatKepPembebasan" data-bs-toggle="tooltip" data-bs-placement="top" title="Lihat"
                                    data-id_permohonan_aktif="{{ $t->id }}"
                                    data-nama="{{ $t->indukTubel->namaPegawai }}"
                                    data-nip="{{ $t->indukTubel->nip18 }}"
                                    data-tglaktifkembali="{{ AppHelper::instance()->indonesian_date($t->tanggalSt,'j F Y','') }}"
                                    data-nomorst="{{ $t->indukTubel->nip18 }}"
                                    data-tglst="{{ AppHelper::instance()->indonesian_date($t->tanggalSt,'j F Y','') }}"
                                    data-tglmulaist="{{ AppHelper::instance()->indonesian_date($t->tglMulaiSt,'j F Y','') }}"
                                    data-tglselesaist="{{ AppHelper::instance()->indonesian_date($t->tglSelesaiSt,'j F Y','') }}"
                                    data-nomorkep="{{ $t->nomorKepPembebasan }}"
                                    data-tglkep="{{ AppHelper::instance()->indonesian_date($t->tglKepPembebasan,'j F Y','') }}"
                                    data-tmt="{{ AppHelper::instance()->indonesian_date($t->tglTmtKepPembebasan,'j F Y','') }}"
                                    data-file="{{ $t->pathKepPembebasan }}"
                                >
                                    <i class="fa fa-eye"></i>
                                </button> --}}

                        @endif

                    </td>
                </tr>
            @endforeach

        @else
            <tr class="text-center">
                <td class="text-dark mb-1 fs-6" colspan="7">Tidak terdapat data</td>
            </tr>
        @endif

        </tbody>
    </table>

</div>

@if ($totalItems != 0)
    <!--begin::pagination-->
    <div class="d-flex flex-stack flex-wrap pt-5 p-3">
        <div class="fs-6 fw-bold text-gray-700"></div>

        @php
            $disabled = '';
            $nextPage= '';
            $disablednext = '';
            $hidden = '';
            $pagination = '';

            // tombol pagination
            if ($totalItems == 0) {
                // $pagination = 'none';
            }

            // tombol back
            if($currentPage == 1){
                $disabled = 'disabled';
            }

            // tombol next
            if($currentPage != $totalPages) {
                $nextPage = $currentPage + 1;
            }

            // tombol pagination
            if($currentPage == $totalPages || $totalPages == 0){
                $disablednext = 'disabled';
                $hidden = 'hidden';
            }

        @endphp

            <!--begin::Pages-->
        <ul class="pagination" style="display: {{ $pagination }}">
            <li class="page-item disabled"><a href="#" class="page-link">Halaman {{ $currentPage }}
                    dari {{ $totalPages }}</a></li>
            <li class="page-item previous {{ $disabled }}"><a
                    href="{{ url()->current() . '?page=' . ($currentPage - 1) }}" class="page-link"><i
                        class="previous"></i></a></li>
            <li class="page-item active"><a href="{{ url()->current() . '?page=' . $currentPage }}"
                                            class="page-link">{{ $currentPage }}</a></li>
            <li class="page-item" {{ $hidden }}><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                   class="page-link">{{ $nextPage }}</a></li>
            <li class="page-item next {{ $disablednext }}"><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                              class="page-link"><i class="next"></i></a></li>
        </ul>
        <!--end::Pages-->

    </div>
    <!--end::paging-->
@endif
