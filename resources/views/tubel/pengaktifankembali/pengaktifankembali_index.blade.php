@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card mb-3 border-warning shadow-sm">
                    <div class="card-header card-header-stretch" id="card-header">
                        <div class="card-toolbar">
                            <ul class="nav nav-tabs nav-line-tabs nav-stretch border-transparent fs-5 fw-bolder"
                                id="tabLps">
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'menunggu_persetujuan' ? 'active' : '' }}"
                                       href="{{ url('tubel/pengaktifan-kembali') }}"> Menunggu Persetujuan </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'telah_disetujui' ? 'active' : '' }}"
                                       href="{{ url('tubel/pengaktifan-kembali/disetujui') }}"> Telah Disetujui </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'ditolak' ? 'active' : '' }}"
                                       href="{{ url('tubel/pengaktifan-kembali/ditolak') }}"> Ditolak </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'terbitst' ? 'active' : '' }}"
                                       href="{{ url('tubel/pengaktifan-kembali/terbit-st') }}"> Telah Terbit ST </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'terbitkep' ? 'active' : '' }}"
                                       href="{{ url('tubel/pengaktifan-kembali/terbit-kep') }}"> Telah Terbit KEP
                                        Pembebasan </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'dashboard' ? 'active' : '' }}"
                                       href="{{ url('tubel/pengaktifan-kembali/dashboard') }}"> Dashboard </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'menunggu_persetujuan' ? 'active show' : '' }}"
                                 id="menunggu_persetujuan" role="tabpanel">
                                @if ($tab == 'menunggu_persetujuan')
                                    @include('tubel.pengaktifankembali.pengaktifankembali_tabel')
                                @endif
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'telah_disetujui' ? 'active show' : '' }}"
                                 id="telah_disetujui" role="tabpanel">
                                @if ($tab == 'telah_disetujui')
                                    @include('tubel.pengaktifankembali.pengaktifankembali_tabel')
                                @endif
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'ditolak' ? 'active show' : '' }}" id="ditolak"
                                 role="tabpanel">
                                @if ($tab == 'ditolak')
                                    @include('tubel.pengaktifankembali.pengaktifankembali_tabel')
                                @endif
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'terbitst' ? 'active show' : '' }}" id="terbitst"
                                 role="tabpanel">
                                @if ($tab == 'terbitst')
                                    @include('tubel.pengaktifankembali.pengaktifankembali_tabel')
                                @endif
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'terbitkep' ? 'active show' : '' }}" id="terbitkep"
                                 role="tabpanel">
                                @if ($tab == 'terbitkep')
                                    @include('tubel.pengaktifankembali.pengaktifankembali_tabel')
                                @endif
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'dashboard' ? 'active show' : '' }}" id="dashboard"
                                 role="tabpanel">
                                @if ($tab == 'dashboard')
                                    @include('tubel.pengaktifankembali.pengaktifankembali_tabel')
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('tubel.pengaktifankembali.pengaktifankembali_modal')
    @include('tubel.modal_tiket')

@endsection


@section('js')
    <script>
        let app_url = '{{ env('APP_URL') }}'

        // advanced search
        $("#advancedSearch").submit(function (e) {
            e.preventDefault();

            var query = $(this).serializeArray().filter(function (i) {
                return i.value;
            });

            window.location.href = $(this).attr('action') + (query ? '?' + $.param(query) : '');

        })
    </script>

    <script src="{{ asset('assets/js/tubel') }}/pengaktifan-kembali.js"></script>
    <script src="{{ asset('assets/js/tubel') }}/log-tiket.js"></script>

@endsection
