@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card mb-3 border-warning shadow-sm">
                    <div class="card-header card-header-stretch" id="card-header">
                        <div class="card-toolbar">
                            <ul class="nav nav-tabs nav-line-tabs nav-stretch border-transparent fs-5 fw-bolder"
                                id="tabLps">
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'perpanjangan' ? 'active' : '' }}"
                                       href="{{ url('tubel/perpanjangan-st-stan') }}"> Perpanjangan ST PKN STAN </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'telah_upload' ? 'active' : '' }}"
                                       href="{{ url('tubel/perpanjangan-st-stan/uploadst') }}"> ST telah diupload </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">

                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'perpanjangan' ? 'active show' : '' }} "
                                 id="perpanjangan" role="tabpanel">
                                @if ($tab == 'perpanjangan')
                                    @include('tubel.perpanjanganststan.perpanjanganststan_tabel')
                                @endif
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'telah_upload' ? 'active show' : '' }} "
                                 id="telah_upload" role="tabpanel">
                                @if ($tab == 'telah_upload')
                                    @include('tubel.perpanjanganststan.perpanjanganststan_tabel')
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('tubel.perpanjanganststan.perpanjanganststan_modal')

@endsection

@section('js')
    <script>
        let app_url = '{{ env('APP_URL') }}'
    </script>

    <script src="{{ asset('assets/js/tubel') }}/perpanjangan-st-stan.js"></script>
    <script src="{{ asset('assets/js/tubel') }}/log-tiket.js"></script>

@endsection
