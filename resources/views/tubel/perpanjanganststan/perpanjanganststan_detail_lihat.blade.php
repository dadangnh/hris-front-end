@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">

            <div class="card border-warning">
                <div class="card-header" id="card-header">
                    <h2 class="card-title">
                        <span class="fw-bold fs-4 text-white">Perpanjangan ST Tubel</span>
                    </h2>

                </div>
                <div class="card-body"
                ">
                <div class="col-md row mb-3">
                    <div class="col">
                        <a href="{{ redirect()->back() }}" class="btn btn-sm btn-secondary mb-3"><i
                                class="fa fa-reply"></i> Kembali</a>
                    </div>
                </div>
                <div class="row mb-8">
                    <div class="card col-lg">
                        <div class="accordion" id="kt_accordion_1">
                            <div class="accordion-item border-secondary">
                                <h2 class="accordion-header" id="kt_accordion_1_header_1">
                                    <button class="accordion-button fs-4 fw-bold collapsed" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#kt_accordion_1_body_1"
                                            aria-expanded="true" aria-controls="kt_accordion_1_body_1">
                                        Import dari ST Tubel Awal
                                    </button>
                                </h2>
                                <div id="kt_accordion_1_body_1" class="accordion-collapse collapse"
                                     aria-labelledby="kt_accordion_1_header_1" data-bs-parent="#kt_accordion_1">
                                    <div class="accordion-body">
                                        <form action="{{ env("APP_URL") . '/tubel/perpanjanganststan/tubel_add' }}"
                                              method="post">
                                            @csrf
                                            <div class="row mb-3">
                                                <div class="col-lg"></div>
                                                <div class="row col-lg-8">
                                                    <label class="col-lg-3 col-form-label fw-bold fs-6">No ST</label>
                                                    <div class="col-lg">
                                                        <input type="hidden" name="id_perpanjangan"
                                                               value="{{ $perpanjanganststan->id }}">
                                                        <input class="form-control" type="text" id="nomor_st_awal"
                                                               name="nomor_st_awal" placeholder="nomor st awal"
                                                               required>
                                                    </div>
                                                </div>
                                                <div class="col-lg"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg"></div>
                                                <div class="row col-lg-8">
                                                    <div class="text-end">
                                                        <button type="submit" class="btn btn-sm btn-primary fs-6">
                                                            <i class="fa fa-download"></i> Import
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col-lg"></div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item border-secondary">
                                <h2 class="accordion-header" id="kt_accordion_1_header_2">
                                    <button class="accordion-button fs-4 fw-bold collapsed" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#kt_accordion_1_body_2"
                                            aria-expanded="false" aria-controls="kt_accordion_1_body_2">
                                        Cari Nama / NIP
                                    </button>
                                </h2>
                                <div id="kt_accordion_1_body_2" class="accordion-collapse collapse"
                                     aria-labelledby="kt_accordion_1_header_2" data-bs-parent="#kt_accordion_1">
                                    <div class="accordion-body">
                                        <div class="row mb-8">
                                            <div class="col-lg"></div>
                                            <div class="row col-lg-10">
                                                <label class="col-lg-2 col-form-label fw-bold fs-6">Nama / NIP</label>
                                                <div class="row col-lg">
                                                    <div class="col-lg-8">
                                                        <input class="form-control" type="text" id="pencarian_nama_nip"
                                                               name="pencarian" placeholder="cari nama atau nip"
                                                               required>
                                                    </div>
                                                    <div class="col-lg">
                                                        <button class="btn btn-md btn-secondary fs-6 btnCari">
                                                            <i class="fa fa-search"></i> Cari
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg"></div>
                                        </div>
                                        <div id="hasil_pencarian" style="display: none">
                                            <div class="row mb-2">
                                                <div class="col-lg-1"></div>
                                                <div class="col-lg">
                                                    <table class="table table-rounded border rounded border gs-7">
                                                        <thead>
                                                        <tr class="text-start fw-bolder bg-secondary">
                                                            <th class="">Nama</th>
                                                            <th class="">ST Awal</th>
                                                            <th class="">Status Tubel</th>
                                                            <th class="w-100px pe-2">
                                                                {{-- Select All --}}
                                                                <div
                                                                    class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                                                    <input id="check_all" class="form-check-input"
                                                                           type="checkbox" data-kt-check="true"
                                                                           data-kt-check-target="#kt_customers_table .form-check-input"
                                                                           value="1"/>
                                                                </div>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="table_nama_nip">
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-lg-1"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-1"></div>
                                                <div class="col-lg">
                                                    <div class="text-end">
                                                        <button class="btn btn-sm btn-primary fs-6 btnImportNipNama">
                                                            <i class="fa fa-download"></i> Import
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col-lg-1"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg">
                        <div class="col-md row mb-3 text-end">
                            @if ($lampiran != null)
                                <div class="col">
                                    <a href="{{ url('tubel/perpanjanganststan/cetak') }}"
                                       class="btn btn-md btn-success mb-3"><i class="fa fa-print"></i> Cetak</a>
                                </div>
                            @endif
                        </div>
                        <div class="table-responsive bg-gray-400 rounded">
                            <table
                                class="table table-rounded table-striped border bg-gray-400 rounded border border-gray-300gs-7"
                                id="kt_customers_table">
                                <thead>
                                <tr class="text-start fw-bolder bg-secondary align-middle">
                                    <th class="ps-4">Nama</th>
                                    <th class="">ST Awal</th>
                                    <th class="">Status Tubel</th>
                                    <th class="w-150px text-center">
                                        @if ($lampiran != null)
                                            <button class="btn btn-sm btn-danger btnDeleteAllLampiran"
                                                    data-id="{{ $perpanjanganststan->id }}">
                                                <i class="fa fa-trash"></i>
                                                Delete All
                                            </button>
                                        @endif
                                    </th>
                                    {{-- <th class="w-100px pe-2">
                                        <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                            <input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_customers_table .form-check-input" value="1" />
                                        </div>
                                    </th> --}}
                                </tr>
                                </thead>
                                <tbody class="fw-bold text-gray-600">

                                @if ($lampiran == null)

                                    <tr>
                                        <td class="text-center" colspan="4">Tidak terdapat data</td>
                                    </tr>

                                @else

                                    @foreach ($lampiran as $l)
                                        <tr id="{{ $l->id }}">
                                            <td class="ps-4">
                                                {{ $l->indukTubel->namaPegawai }}
                                            </td>
                                            <td>
                                                {{ $l->indukTubel->nomorSt }}
                                                <br>
                                                {{AppHelper::instance()->indonesian_date($l->indukTubel->tglSt,'j F Y','')}}
                                            </td>
                                            <td>
                                                <span
                                                    class="badge badge-light-success fs-7 fw-bolder"> {{ $l->logStatusProbis->caseId->namaCase }}</span>
                                            </td>
                                            <td class="text-center">
                                                <button
                                                    class="btn btn-sm btn-rounded btn-danger btn-icon btnDeleteLampiran"
                                                    data-id="{{ $perpanjanganststan->id }}"
                                                    data-id_lampiran="{{ $l->id }}">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif


                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-1"></div>
                </div>
            </div>
        </div>

    </div>
    </div>

@endsection

@section('js')

    <script>

        // button pencarian by nama / nip
        $(".btnCari").click(function () {
            var $this = $(this);

            //Call Button Loading Function
            BtnLoading($this);

            // $("#table_nama_nip").remove();

            $.ajax({
                type: 'post',
                url: '{{ env('APP_URL') }}' + '/tubel/perpanjanganststan/nama_nip_find',
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    keyword: $("#pencarian_nama_nip").val()
                },
                success: function (response) {
                    var myArray = []
                    if (response.status == 0 || response.data == "") {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Gagal!',
                            text: "Pencarian tidak ditemukan! ",
                            showConfirmButton: false,
                            timer: 2000
                        });


                    } else {
                        myArray = response.data
                        $(this).prop("disabled", true);

                        $('#hasil_pencarian').show();

                        console.log(response)
                        buildTable(myArray)
                    }

                    // reset button
                    BtnReset($this);
                }
            });

        });

        // button check all
        $("#check_all").click(function () {
            var $this = $(this);

            if ($this.prop('checked', true)) {
                $('.form-check-input').attr('checked', true);
                // $('.form-check-input').prop('checked', true).change();
            } else {
                $('.form-check-input').attr('checked', false);
                // $('.form-check-input').prop('checked', false).change();
            }
        });

        // buat tabel data pencarian by nama, nip
        function buildTable(data) {
            var table = document.getElementById('table_nama_nip')
            // var table = document.getElementsByClassName('form-check-input')

            for (var i = 0; i < data.length; i++) {
                if (data[i].flagStan == 1 && data[i].logStatusProbis.caseId.id == '2d3eb4b6-e4a5-45e4-a4f5-6d41835eb217') {
                    var row =
                        `<tr>
                        <td>${data[i].namaPegawai}</td>
                        <td>${data[i].nomorSt}</td>

                        <td> <span class="badge badge-light-success fs-7 fw-bolder">${data[i].logStatusProbis.caseId.namaCase}</span></td>
                        <td><div class="form-check form-check-sm form-check-custom form-check-solid">
                                <input id="check" class="form-check-input" type="checkbox" value="${data[i].id}" />
                            </div>
                        </td>

                    </tr>`

                    table.innerHTML += row
                }
            }
        }

        // button import data by st
        $(".btnImportTubel").click(function () {
            var $this = $(this);

            BtnLoading($this);

            $.ajax({
                type: 'post',
                url: '{{ env('APP_URL') }}' + '/tubel/perpanjanganststan/tubel_add',
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                },
                success: function (response) {
                    // console.log(response);
                    location.reload();

                    if (response['status'] == 0) {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Gagal',
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600
                        });
                    } else {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Berhasil',
                            text: 'data berhasil ditambahkan',
                            showConfirmButton: false,
                            timer: 1600
                        });
                    }

                    // reset button
                    BtnReset($this);

                },
                error: function (err) {
                    console.log(err)
                }
            });

        });

        // button import data by nip, nama
        $(".btnImportNipNama").click(function () {
            // var checkedValue = $('.checkForm2:checked').val();
            var $this = $(this);

            BtnLoading($this);

            var checkedValue = null;
            var inputElements = document.getElementsByClassName('form-check-input');
            // var inputElements = document.getElementById('check');

            // looping
            for (var i = 0; inputElements[i]; i++) {
                if (inputElements[i].checked) {
                    checkedValue = inputElements[i].value;
                    // console.log(inputElements[i].value)

                    // post looping
                    $.ajax({
                        type: 'post',
                        url: '{{ env('APP_URL') }}' + '/tubel/perpanjanganststan/nip_nama_add',
                        data: {
                            _token: $("meta[name='csrf-token']").attr("content"),
                            id_perpanjangan: '{{ $perpanjanganststan->id }}',
                            id_tubel: checkedValue
                        },
                        success: function (response) {
                            // console.log(response);
                            location.reload();

                            if (response['status'] == 0) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1600
                                });
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'data berhasil ditambahkan',
                                    showConfirmButton: false,
                                    timer: 1600
                                });
                            }

                            // reset button
                            BtnReset($this);

                        },
                        error: function (err) {
                            console.log(err)
                        }
                    });

                }
            }


        });

        // button import by tubel
        $(".btnImportTubel").click(function () {
            $.ajax({
                type: 'post',
                url: '{{ env('APP_URL') }}' + '/tubel/perpanjanganststan/tubel_add',
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    keyword: $("#nomor_st_awal").val()
                },
                success: function (response) {
                    if (response.status == 0) {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Gagal!',
                            text: "Data tidak ditemukan! ",
                            showConfirmButton: false,
                            timer: 2000
                        });

                    } else {
                        console.log(response)
                        // Swal.fire({
                        //     position: 'center',
                        //     icon: 'success',
                        //     title: 'Berhasil',
                        //     text: "Data berhasil ditambahkan",
                        //     showConfirmButton: false,
                        //     timer: 2000
                        // });

                        $(this).prop("disabled", true);
                    }

                    // reset button
                    BtnReset($this);
                }
            });

        });

        // button hapus lampiran (id)
        $(".btnDeleteLampiran").click(function () {
            var token = $("meta[name='csrf-token']").attr("content")
            var id = $(this).data('id_lampiran')

            Swal.fire({
                title: 'Yakin akan menghapus data?',
                // text: "Data tidak dapat dikembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#C5584B',
                cancelButtonColor: '#B2B2B2',
                confirmButtonText: 'Hapus!',
                cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'post',
                        url: '{{ env('APP_URL') }}' + '/tubel/perpanjanganststan/lampiran/delete',
                        data: {
                            _token: token,
                            id: $(this).data('id'),
                            id_lampiran: $(this).data('id_lampiran')
                        },
                        success: function (response) {
                            // console.log(response)
                            $('#' + id).remove();

                            if (response.status == 1) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'data berhasil di hapus',
                                    showConfirmButton: false,
                                    timer: 1800
                                });
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1800
                                });
                            }
                        },
                        error: function (err) {
                            console.log(err)
                        }
                    });

                }
            })
        });

        // button hapus lampiran (all)
        $(".btnDeleteAllLampiran").click(function () {
            var token = $("meta[name='csrf-token']").attr("content")
            var id = $(this).data('id_lampiran')

            Swal.fire({
                title: 'Yakin akan menghapus semua data?',
                // text: "Data tidak dapat dikembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#C5584B',
                cancelButtonColor: '#B2B2B2',
                confirmButtonText: 'Hapus!',
                cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'post',
                        url: '{{ env('APP_URL') }}' + '/tubel/perpanjanganststan/lampiran/delete_all',
                        data: {
                            _token: token,
                            id: $(this).data('id'),
                        },
                        success: function (response) {
                            if (response.status == 1) {
                                location.reload();
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'data berhasil di hapus',
                                    showConfirmButton: false,
                                    timer: 1800
                                });
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1800
                                });
                            }
                        },
                        error: function (err) {
                            console.log(err)
                        }
                    });

                }
            })
        });

        // spinner / loading button
        function BtnLoading(elem) {
            $(elem).attr("data-original-text", $(elem).html());
            $(elem).prop("disabled", true);
            $(elem).html('Loading... <i class="spinner-border spinner-border-sm"></i>');
        }

        // spinner off / reset button
        function BtnReset(elem) {
            $(elem).prop("disabled", false);
            $(elem).html($(elem).attr("data-original-text"));
        }
    </script>

@endsection
