<div class="modal fade" id="add_dasar_st" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Form Dasar Perpanjangan ST</h3>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form" method="post" enctype="multipart/form-data"
                  action="{{ url('tubel/perpanjanganststan/dasarst') }}">
                @csrf

                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Perpanjangan</label>
                        <div class="col-lg">
                            <textarea class="form-control" type="text" id="alasan_perpanjangan_st"
                                      name="alasan_perpanjangan_st" rows="4" placeholder="alasan perpanjangan"
                                      required></textarea>
                        </div>
                    </div>
                    <div class="row mb-8">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Selesai Perpanjangan</label>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active datepick"
                                       id="tgl_selesai_perpanjangan" placeholder="Pilih tanggal"
                                       name="tgl_selesai_perpanjangan" type="text" value="" required>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">No ST Awal</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="nomor_st_awal" name="nomor_st_awal"
                                   placeholder="nomor st awal" required>
                        </div>
                    </div>
                    <div class="row mb-8">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                        <div class="col-lg">
                            <input class="form-control" type="file" name="file_dokumen" required>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Keterangan</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="keterangan" name="keterangan"
                                   placeholder="keterangan dokumen pendukung" required>
                        </div>
                    </div>
                    <div class="row mb-6">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen Pendukung</label>
                        <div class="col-lg">
                            <input class="form-control" type="file" name="file_dokumen_pendukung" required>
                        </div>
                    </div>

                </div>

                <div class="text-center mb-8">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                            class="fa fa-reply"></i>Batal
                    </button>
                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="lihat_dasar_st" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Lihat Dasar Perpanjangan ST</h3>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body scroll-y px-5 px-lg-10">
                <div class="row mb-3">
                    <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Perpanjangan</label>
                    <div class="col-lg">
                        <textarea class="form-control" type="text" id="alasan_perpanjangan_st_lihat"
                                  name="alasan_perpanjangan_st_lihat" rows="4" placeholder="alasan perpanjangan"
                                  disabled></textarea>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Selesai Perpanjangan</label>
                    <div class="col-lg-4">
                        <div class="position-relative d-flex align-items-center">
                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                <span class="symbol-label bg-secondary">
                                    <span class="svg-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             width="24px" height="24px"
                                             viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                      rx="1"></rect>
                                                <path
                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                    fill="#000000"></path>
                                            </g>
                                        </svg>
                                    </span>
                                </span>
                            </div>
                            <input class="form-control ps-12 flatpickr-input active" id="tgl_selesai_perpanjangan_lihat"
                                   placeholder="Pilih tanggal" name="tgl_selesai_perpanjangan_lihat" type="text"
                                   value="" disabled>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-lg-3 col-form-label fw-bold fs-6">No ST Awal</label>
                    <div class="col-lg">
                        <input class="form-control" type="text" id="nomor_st_awal_lihat" name="nomor_st_awal_lihat"
                               placeholder="nomor st awal" disabled>
                    </div>
                </div>
                <div class="row mb-8">
                    <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                    <div class="col-lg">
                        <a href="{{ env('APP_URL' . '/files/') }}" target="_blank" id="file_dokumen_lihat"
                           class="form-control-plaintext"><i class="fa fa-file"></i> Dokumen.pdf</a>
                    </div>
                </div>
                <div class="row">
                    <label class="col-lg-3 col-form-label fw-bold fs-6">Keterangan</label>
                    <div class="col-lg">
                        <input class="form-control" type="text" id="keterangan_lihat" name="keterangan"
                               placeholder="keterangan dokumen pendukung" disabled>
                    </div>
                </div>
                <div class="row mb-6">
                    <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen Pendukung</label>
                    <div class="col-lg">
                        <a href="{{ env('APP_URL' . '/files/') }}" target="_blank" id="file_dokumen_pendukung_lihat"
                           class="form-control-plaintext"><i class="fa fa-file"></i> Dokumen Pendukung.pdf</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit_dasar_st" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Edit Dasar Perpanjangan ST</h3>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form" method="post" enctype="multipart/form-data"
                  action="{{ url('tubel/perpanjangan-st-stan/updatedasarst') }}">
                @csrf
                <input type="hidden" id="id_perpanjangan_stan_edit" name="id_perpanjangan_stan_edit">

                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Perpanjangan</label>
                        <div class="col-lg">
                            <textarea class="form-control" type="text" id="alasan_perpanjangan_st_edit"
                                      name="alasan_perpanjangan_st_edit" rows="4" placeholder="alasan perpanjangan"
                                      required></textarea>
                        </div>
                    </div>
                    <div class="row mb-8">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Selesai Perpanjangan</label>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active datepick"
                                       id="tgl_selesai_perpanjangan_edit" placeholder="Pilih tanggal"
                                       name="tgl_selesai_perpanjangan_edit" type="text" value="" required>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">No ST Awal</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="nomor_st_awal_edit" name="nomor_st_awal_edit"
                                   placeholder="nomor st awal" required>
                        </div>
                    </div>
                    <div class="row mb-8">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                        <div class="col-lg">
                            <input class="form-control" type="file" name="file_dokumen_edit" required>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Keterangan</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="keterangan_edit" name="keterangan_edit"
                                   placeholder="keterangan dokumen pendukung" required>
                        </div>
                    </div>
                    <div class="row mb-6">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen Pendukung</label>
                        <div class="col-lg">
                            <input class="form-control" type="file" name="file_dokumen_pendukung_edit" required>
                        </div>
                    </div>

                </div>

                <div class="text-center mb-8">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                            class="fa fa-reply"></i>Batal
                    </button>
                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="upload_st" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Upload ST Perpanjangan Tugas Belajar</h3>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form" method="post" enctype="multipart/form-data"
                  action="{{ url('tubel/perpanjanganststan/uploadst') }}">
                @csrf

                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Nomor Surat Tugas</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="id_perpanjangan_stan"
                                   name="id_perpanjangan_stan" hidden>
                            <input class="form-control" type="text" id="nomor_st_upload_st" name="nomor_st_upload_st"
                                   placeholder="nomor st" required>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat Tugas</label>
                        <div class="col-lg">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active datepick" id="tgl_st_upload_st"
                                       placeholder="Pilih tanggal" name="tgl_st_upload_st" type="text" value=""
                                       required>
                            </div>
                        </div>
                        <div class="col-lg"></div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Mulai / Selesai</label>
                        <div class="col-lg">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active datepick"
                                       id="tgl_mulai_upload_st" placeholder="Pilih tanggal" name="tgl_mulai_upload_st"
                                       type="text" value="" required>
                            </div>
                        </div>
                        <div class="col-lg">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active datepick"
                                       id="tgl_selesai_upload_st" placeholder="Pilih tanggal"
                                       name="tgl_selesai_upload_st" type="text" value="" required>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-8">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                        <div class="col-lg">
                            <input class="form-control" type="file" name="file_upload_st" required>
                        </div>
                    </div>
                </div>

                <div class="text-center mb-8">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                            class="fa fa-reply"></i>Batal
                    </button>
                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
