@php
    $halaman = isset($_GET['page']) ? (int) $_GET['page'] : 1;
    $halaman_awal = $halaman > 1 ? $halaman * $size - $size : 0;
    $nomor = $halaman_awal + 1;
@endphp

<div class="row">
    <div class="col-lg-6 mb-4">
        <div class="text-start" data-kt-customer-table-toolbar="base">
            <button type="button" class="btn btn-sm btn-ligth btn-active-primary border border-gray-400 fs-6"
                    data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" data-kt-menu-flip="top-end">
                <i class="fa fa-filter"></i> Advanced Search
            </button>
            <div id="advancedSearch" class="menu menu-sub menu-sub-dropdown w-md-400px border-warning mt-1"
                 data-kt-menu="true">
                <div class="px-7 py-5">
                    <div class="fs-4 text-dark fw-bolder">Filter Pencarian</div>
                </div>
                <div class="separator border-gray-200"></div>
                <form action="" method="get">
                    {{-- @csrf --}}
                    <div class="px-7 py-5">
                        <div class="row mb-4">
                            <div class="col-lg-12">
                                <label class="form-label fw-bold">No.ST Awal:</label>
                                <input class="form-control form-select-solid" type="text" name="stawal"
                                       placeholder="nomor st awal"
                                       value="{{ isset($_GET['stawal']) ? $_GET['stawal'] : '' }}">
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-lg-12">
                                <label class="form-label fw-bold">No. ST Perpanjangan:</label>
                                <input class="form-control form-select-solid" type="text" name="stperpanjang"
                                       placeholder="nomor st perpanjangan"
                                       value="{{ isset($_GET['stperpanjang']) ? $_GET['stperpanjang'] : '' }}">
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-lg-12">
                                <label class="form-label fw-bold">Tgl Selesai Perpanjangan:</label>
                                <input type="text" class="form-control text-start date-search" name="tglselesai"
                                       placeholder="tgl selesai perpanjangan"
                                       value="{{ isset($_GET['tglselesai']) ? $_GET['tglselesai'] : '' }}"
                                       data-dropdown-parent="#advancedSearch"/>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <a href="{{ url()->current() }}"
                               class="btn btn-light btn-active-light-primary me-2">Reset</a>
                            <button type="submit" class="btn btn-sm btn-primary" data-kt-menu-dismiss="true"
                                    data-kt-customer-table-filter="filter">Search
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="text-end mb-4">
            @if ($tab == 'perpanjangan')
                <button data-bs-toggle="modal" data-bs-target="#add_dasar_st" class="btn btn-primary btn-sm fs-6">
                    <i class="fa fa-plus"></i> Buat Dasar ST
                </button>
            @endif
        </div>
    </div>
</div>

<div class="table-responsive bg-gray-400 rounded border border-gray-300">
    <table class="table table-striped align-middle" id="tabel_keppengaktifan_kembali">

        <thead class="fw-bolder bg-secondary fs-6">
        <tr class="">
            <th class="ps-4">No</th>
            <th class="">ST Awal</th>

            @if ($tab == 'perpanjangan')
                <th class="">Alasan Perpanjangan</th>
            @endif

            @if ($tab == 'telah_upload')
                <th class="">ST Perpanjangan</th>
                <th class="">Tgl Mulai - Tgl Selesai</th>
            @endif

            <th class="text-center">Status</th>
            <th class="text-center">Aksi</th>
        </tr>
        </thead>
        <tbody>
        @if ($perpanjanganststan->data != null)

            @foreach ($perpanjanganststan->data as $p)
                <tr id="{{ $p->id }}" class="text-right">

                    <td class="ps-4 fs-6">
                        {{ $nomor++ }}
                    </td>

                    <td class="text-dark fs-6 ps-4">
                        {{ $p->noStAwal }}
                    </td>

                    @if ($tab == 'perpanjangan')
                        <td class="text-dark fs-6 ps-4">
                            {{ $p->alasanPerpanjangan }}
                        </td>
                    @endif

                    @if ($tab == 'telah_upload')

                        <td class="text-dark fw-bolder mb-1 fs-6">
                            {{ $p->nomorStPerpanjangan }}
                            <br>

                            <span
                                class="fw-normal fs-6">{{ AppHelper::instance()->indonesian_date($p->tglStPerpanjangan, 'j F Y', '') }}</span>
                        </td>

                        <td class="text-dark mb-1 fs-6">
                            {{ AppHelper::instance()->indonesian_date($p->tglMulaiStPerpanjangan, 'j F Y', '') }} -
                            <br>
                            {{ AppHelper::instance()->indonesian_date($p->tglSelesaiStPerpanjangan, 'j F Y', '') }}
                        </td>

                    @endif

                    <td class="text-dark fs-6 text-center">
                        <span class="badge badge-light-success fs-7 fw-bolder">{{ $p->logStatusProbis->output }}</span>
                    </td>

                    <td class="text-center text-dark mb-1 fs-6">

                        <button class="btn btn-icon btn-sm btn-info btnLihat" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="Lihat"
                                data-alasan_perpanjangan_st_lihat="{{ $p->alasanPerpanjangan }}"
                                data-tgl_selesai_perpanjangan_lihat="{{ AppHelper::instance()->indonesian_date($p->tglSelesaiPerpanjangan, 'j F Y', '') }}"
                                data-no_st_awal_lihat="{{ $p->noStAwal }}"
                                data-file_lihat="{{ $p->pathStAwal }}"
                                data-ket_lihat="{{ $p->noStAwal }}"
                                data-file_pendukung_lihat="{{ $p->pathDokumenPendukung }}">
                            <i class="fa fa-eye"></i>
                        </button>

                        @if ($tab == 'telah_upload')
                            <a href="{{ url('tubel/perpanjangan-st-stan/' . $p->id . '/lihat') }}"
                               class="btn btn-icon btn-sm btn-success" data-bs-toggle="tooltip" data-bs-placement="top"
                               title="Detail">
                                <i class="fa fa-users"></i>
                            </a>
                        @else
                            <a href="{{ url('tubel/perpanjangan-st-stan/' . $p->id . '/add') }}"
                               class="btn btn-icon btn-sm btn-success" data-bs-toggle="tooltip" data-bs-placement="top"
                               title="Detail">
                                <i class="fa fa-users"></i>
                            </a>
                        @endif


                        @if ($tab == 'perpanjangan')

                            <button class="btn btn-icon btn-sm btn-warning btnEdit" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Edit" data-id_perpanjangan_stan_edit="{{ $p->id }}"
                                    data-alasan_perpanjangan_st_edit="{{ $p->alasanPerpanjangan }}"
                                    data-tgl_selesai_perpanjangan_edit="{{ $p->tglSelesaiPerpanjangan }}"
                                    data-no_st_awal_edit="{{ $p->noStAwal }}"
                                    data-file_edit="{{ $p->pathStAwal }}" data-ket_edit="{{ $p->noStAwal }}"
                                    data-file_pendukung_edit="{{ $p->pathDokumenPendukung }}">
                                <i class="fa fa-edit"></i>
                            </button>

                            <button class="btn btn-icon btn-sm btn-danger btnHapus" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Hapus"
                                    data-id_perpanjangan_stan_hapus="{{ $p->id }}">
                                <i class="fa fa-trash"></i>
                            </button>

                            @if ($p->countLampiran > 0)
                                <br>

                                <button class="btn btn-sm btn-primary btnUploadStPerpanjanganSt mt-2"
                                        data-bs-toggle="tooltip" data-bs-placement="top" title="Upload ST"
                                        data-id_perpanjangan_stan_upload_st="{{ $p->id }}">
                                    <i class="fa fa-upload"></i> Upload ST
                                </button>
                            @endif

                        @endif

                    </td>
                </tr>
            @endforeach
        @else
            <tr class="text-center">
                <td class="text-dark mb-1 fs-6" colspan="7">Tidak terdapat data</td>
            </tr>
        @endif

        </tbody>
    </table>

</div>

@if ($totalItems != 0)
    <!--begin::pagination-->
    <div class="d-flex flex-stack flex-wrap pt-5 p-3">
        <div class="fs-6 fw-bold text-gray-700"></div>

        @php
            $disabled = '';
            $nextPage = '';
            $disablednext = '';
            $hidden = '';
            $pagination = '';

            // tombol pagination
            if ($totalItems == 0) {
                // $pagination = 'none';
            }

            // tombol back
            if ($currentPage == 1) {
                $disabled = 'disabled';
            }

            // tombol next
            if ($currentPage != $totalPages) {
                $nextPage = $currentPage + 1;
            }

            // tombol pagination
            if ($currentPage == $totalPages || $totalPages == 0) {
                $disablednext = 'disabled';
                $hidden = 'hidden';
            }

        @endphp

            <!--begin::Pages-->
        <ul class="pagination" style="display: {{ $pagination }}">
            <li class="page-item disabled"><a href="#" class="page-link">Halaman {{ $currentPage }}
                    dari {{ $totalPages }}</a></li>
            <li class="page-item previous {{ $disabled }}"><a
                    href="{{ url()->current() . '?page=' . ($currentPage - 1) }}" class="page-link"><i
                        class="previous"></i></a></li>
            <li class="page-item active"><a href="{{ url()->current() . '?page=' . $currentPage }}"
                                            class="page-link">{{ $currentPage }}</a></li>
            <li class="page-item" {{ $hidden }}><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                   class="page-link">{{ $nextPage }}</a></li>
            <li class="page-item next {{ $disablednext }}"><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                              class="page-link"><i class="next"></i></a></li>
        </ul>
        <!--end::Pages-->

    </div>
    <!--end::paging-->
@endif
