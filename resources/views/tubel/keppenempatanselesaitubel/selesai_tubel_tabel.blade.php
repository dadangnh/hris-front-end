<div class="table-responsive bg-gray-400 rounded border border-gray-300">
    <table class="table table-striped  align-middle" id="tabel_tubel">
        <thead class="fw-bolder bg-secondary fs-6">
        <tr class="text-right">
            <th class="text-center ps-2">No</th>
            <th class="">Pegawai</th>
            <th colspan="2">Data Pendukung</th>
            <th class="">Tiket</th>
            <th class="text-center">Aksi</th>
        </tr>
        </thead>
        <tbody>
        @if ($selesaitubel->data != null)

            @php
                $nomor = $currentPage == 1 ? 1 : ($currentPage - 1) * $size + 1;
            @endphp

            @foreach ($selesaitubel->data as $st)

                <tr id="ld-{{ $st->id }}" class="text-right">
                    <td class="text-center text-dark fs-6 ps-2">
                        {{ $nomor++ }}
                    </td>

                    <td class="text-dark fs-6">
                        <div class="mb-0">
                            <a class="fw-bold" href="#">
                                {{ $st->dataIndukTubelId->namaPegawai }}
                            </a>
                        </div>
                        <span class="fw-normal fs-6">{{ $st->dataIndukTubelId->nip18 }}</span>
                        <div class="mt-4 mb-0">
                            Pendidikan :
                        </div>
                        <div class="mb-2">
                            {{ $st->dataIndukTubelId->jenjangPendidikanId->jenjangPendidikan }}
                            - {{ $st->dataIndukTubelId->lokasiPendidikanId->lokasi }}
                        </div>
                        <div class="fw-bolder mt-4 mb-2">
                                <span class="d-inline-block position-relative">
                                    <h2>{{ $st->statusLaporDiriHasilStudi ? $st->statusLaporDiriHasilStudi->status : $st->statusLaporDiri->status }}</h2>
                                    @php
                                        switch ($st->statusLaporDiriHasilStudi ? $st->statusLaporDiriHasilStudi->status : $st->statusLaporDiri->status) {
                                            case 'Lulus':
                                                $color = 'success';
                                                break;
                                            case 'Belum Lulus':
                                                $color = 'warning';
                                                break;
                                            case 'Mengundurkan Diri':
                                                $color = 'danger';
                                                break;
                                            case 'Drop Out':
                                                $color = 'danger';
                                                break;
                                        }
                                    @endphp
                                    <span
                                        class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-{{ $color }} translate rounded"></span>
                                </span>
                        </div>
                    </td>

                    @switch($st->statusLaporDiri->status)
                        @case('Lulus')
                            <td class="text-dark fs-6">
                                <div class="mb-6">
                                    Tanggal Lulus
                                    : {{ AppHelper::instance()->indonesian_date($st->tanggalLulus, 'j F Y', '') }}
                                    <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                        <img alt="" class="w-25px me-3"
                                             src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1">
                                            <a href="{{ env('APP_URL') }}/files/{{ $st->pathLaporanSelesaiStudi }}"
                                               target="_blank" class="fs-6 text-hover-primary">Laporan Selesai
                                                Studi.pdf</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="text-dark fs-6">
                                <div class="mb-3">
                                    Karya Tulis : {{ $st->tipeDokPenelitian->tipeDokumen }}
                                    <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                        <img alt="" class="w-25px me-3"
                                             src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1">
                                            <a href="{{ env('APP_URL') }}/files/{{ $st->pathDokumenPenelitian }}"
                                               target="_blank" class="fs-6 text-hover-primary">Dokumen
                                                Penelitian.pdf</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            @break

                        @case('Belum Lulus')
                            <td class="text-dark fs-6">
                                <div class="mb-0">
                                    Tahap Perkuliahan:
                                    <br>
                                    {{ $st->tahapBelumLulus == null ? '' : $st->tahapBelumLulus->tahap }}
                                </div>
                            </td>
                            <td class="text-dark fs-6">
                                <div class="mb-6">
                                    Timeline Rencana Penyelesaian Studi:
                                    <div class="d-flex flex-aligns-center mt-3 pe-10 pe-lg-20">
                                        <img alt="" class="w-25px me-3"
                                             src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1">
                                            <a href="{{ url('files/' . $st->pathTimelineRencanaStudi) }}"
                                               target="_blank" class="fs-6 text-hover-primary">Timeline.pdf</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-6">
                                    Draft Karya Tulis: {{ $st->tipeDokPenelitian->tipeDokumen }}
                                    <div class="d-flex flex-aligns-center mt-3 pe-10 pe-lg-20">
                                        <img alt="" class="w-25px me-3"
                                             src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1">
                                            <a href="{{ url('files/' . $st->pathDokumenPenelitian) }}" target="_blank"
                                               class="fs-6 text-hover-primary">Draft Karya Tulis.pdf</a>
                                        </div>
                                    </div>
                                </div>
                            </td>

                            @break

                        @case('Mengundurkan Diri')
                            <td class="text-dark fs-6">
                                <div>
                                    Semester: {{ $st->semesterKe }}
                                    <br>
                                    Tahun: {{ $st->tahunAkademik }}
                                </div>
                                <div class="mt-4">
                                    Alasan: {{ $st->alasanMundur }}
                                </div>
                            </td>
                            <td class="text-dark fs-6">
                                <div class="mb-6">
                                    Surat Persetujuan dari Perguruan Tinggi
                                    <br>
                                    Nomor: {{ $st->noSuratPersetujuanMundur }}
                                    <br>
                                    Tanggal: {{ AppHelper::instance()->indonesian_date($st->tglSuratPersetujuanMundur, 'j F Y', '') }}
                                    <div class="d-flex flex-aligns-center mt-3 pe-10 pe-lg-20">
                                        <img alt="" class="w-25px me-3"
                                             src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1">
                                            <a href="{{ url('files/' . $st->pathSuratPersetujuanMundur) }}"
                                               target="_blank" class="fs-6 text-hover-primary">Surat Persetujuan.pdf</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            @break

                        @case('Drop Out')
                            <td class="text-dark fs-6">
                                <div class="mb-0">
                                    Semester: {{ $st->semesterKe }}
                                    <br>
                                    Tahun: {{ $st->tahunAkademik }}
                                </div>
                            </td>
                            <td class="text-dark fs-6">
                                <div class="mb-6">
                                    Surat Keterangan dari Perguruan Tinggi
                                    <br>
                                    Nomor: {{ $st->nomorSuratDo }}
                                    <br>
                                    Tanggal: {{ AppHelper::instance()->indonesian_date($st->tglSuratDo, 'j F Y', '') }}
                                    <div class="d-flex flex-aligns-center mt-3 pe-10 pe-lg-20">
                                        <img alt="" class="w-25px me-3"
                                             src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1">
                                            <a href="{{ url('files/' . $st->pathSuratDo) }}" target="_blank"
                                               class="fs-6 text-hover-primary">Surat Keterangan.pdf</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            @break
                        @default

                    @endswitch

                    <td class="text-dark fs-6 ">
                        <a href="javascript:void(0)" class="text-primary btnTiket"
                           data-tiket="{{ $st->logStatusProbis->nomorTiket }}"
                           data-nama="{{ $st->dataIndukTubelId->namaPegawai }}">
                            {{ $st->logStatusProbis->nomorTiket }}
                        </a>
                    </td>

                    <td class="text-center text-dark fs-6">
                        @if ($tab == 'rekam-kep')

                            <button class="btn btn-sm btn-primary m-2 btnUpload" data-bs-toggle="modal"
                                    data-bs-target="#modal_upload_kep" data-id="{{ $st->id }}">
                                <i class="fa fa-upload"></i> Upload
                            </button>

                        @elseif ($tab == 'ditempatkan')

                            <button class="btn btn-sm btn-icon btn-info m-2 btnLihat"
                                    data-nomor_kep="{{ $st->nomorSuratKepPenempatan }}"
                                    data-tgl_kep="{{ AppHelper::instance()->indonesian_date($st->tglSuratKepPenempatan, 'j F Y', '') }}"
                                    data-tmt_kep="{{ AppHelper::instance()->indonesian_date($st->tglTmtSuratKepPenempatan, 'j F Y', '') }}"
                                    data-lokasi_penempatan="{{ $st->namaKantorPenempatan }}"
                                    data-file_kep="{{ $st->pathSuratKepPenempatan }}">
                                <i class="fa fa-eye"></i>
                            </button>

                        @endif
                    </td>

                </tr>

            @endforeach

        @else

            <tr class="text-center">
                <td class="text-dark  fs-6" colspan="6">Tidak terdapat data</td>
            </tr>

        @endif

        </tbody>
    </table>

</div>

@if ($totalItems != 0)
    <div class="d-flex flex-stack flex-wrap pt-5 p-3">
        <div class="fs-6 fw-bold text-gray-700"></div>

        @php
            $disabled = '';
            $nextPage = '';
            $disablednext = '';
            $hidden = '';
            $pagination = '';

            // tombol pagination
            if ($totalItems == 0) {
                $pagination = 'none';
            }

            // tombol back
            if ($currentPage == 1) {
                $disabled = 'disabled';
            }

            // tombol next
            if ($currentPage != $totalPages) {
                $nextPage = $currentPage + 1;
            }

            // tombol pagination
            if ($currentPage == $totalPages || $totalPages == 0) {
                $disablednext = 'disabled';
                $hidden = 'hidden';
            }

        @endphp

            <!--begin::Pages-->
        <ul class="pagination" style="display: {{ $pagination }}">
            <li class="page-item disabled"><a href="#" class="page-link">Halaman {{ $currentPage }}
                    dari {{ $totalPages }}</a></li>
            <li class="page-item previous {{ $disabled }}"><a
                    href="{{ url()->current() . '?page=' . ($currentPage - 1) }}" class="page-link"><i
                        class="previous"></i></a></li>
            <li class="page-item active"><a href="{{ url()->current() . '?page=' . $currentPage }}"
                                            class="page-link">{{ $currentPage }}</a></li>
            <li class="page-item" {{ $hidden }}><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                   class="page-link">{{ $nextPage }}</a></li>
            <li class="page-item next {{ $disablednext }}"><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                              class="page-link"><i class="next"></i></a></li>
        </ul>
        <!--end::Pages-->

    </div>
    <!--end::paging-->
@endif
