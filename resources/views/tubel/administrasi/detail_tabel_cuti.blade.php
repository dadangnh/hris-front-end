<div class="table-responsive rounded border border-gray-300">
    <table class="table table-row-dashed table-row-gray-300 align-middle" id="tabelCuti">
        <thead class="fw-bolder bg-secondary fs-6">
        <tr>
            <th class="ps-4 min-w-200px">Surat Izin dari PT</th>
            <th class="min-w-200px">Surat izin dari Sponsor</th>
            <th class="min-w-200px">Mulai / Selesai</th>
            <th class="text-center min-w-100px">Status</th>
            <th class="text-center min-w-100px">Aksi</th>
        </tr>
        </thead>

        <tbody id="bodyCuti">
        @if ($cuti != null)
            @foreach ($cuti as $c)
                <tr class="" id="cut{{ $c->id }}">
                    <td class="ps-3 fs-6">
                        {{ $c->nomorSuratKampus }}
                        <br>
                        {{ AppHelper::instance()->indonesian_date($c->tglSuratKampus, 'j F Y', '') }}
                        @if ($c->flagPerpanjangan == 1)
                            <br>
                            <span class="text-muted fw-bold fs-7 mb-2">Perpanjangan Cuti</span>
                        @endif

                        @if ($c->flagByAdmin == 1)
                            <br>
                            <span class="fs-8">direkam oleh admin</span>
                        @endif

                    </td>

                    <td class="fs-6">
                        {{ $c->nomorSuratSponsor }}
                        <br>
                        {{ AppHelper::instance()->indonesian_date($c->tglSuratKepSponsor, 'j F Y', '') }}
                        <br>
                    </td>

                    <td class="mb-1 fs-6">
                        {{ AppHelper::instance()->indonesian_date($c->tglMulaiCuti, 'j F Y', '') }} s.d.
                        <br>
                        {{ AppHelper::instance()->indonesian_date($c->tglSelesaiCuti, 'j F Y', '') }}
                    </td>

                    <td class="text-center text-dark mb-1 fs-6">

                        @if ($c->logStatusProbis->output == 'Permohonan cuti dikembalikan' || $c->logStatusProbis->output == 'Perpanjangan cuti dikembalikan')
                            <span
                                class="badge badge-light-danger fs-7 fw-bolder">{{ $c->logStatusProbis->output }}</span>
                        @else
                            <span
                                class="badge badge-light-success fs-7 fw-bolder">{{ $c->logStatusProbis->output }}</span>
                        @endif

                        <br>
                        Tiket:
                        <a href="javascript:void(0)" class="text-primary btnTiket"
                           data-tiket="{{ $c->logStatusProbis->nomorTiket }}"
                           data-nama="{{ $c->indukTubel->namaPegawai }}">
                            {{ $c->logStatusProbis->nomorTiket }}
                        </a>

                    </td>
                    <td class="text-center">

                        @if ($action == true)

                            @if ($c->logStatusProbis->output == 'Draft permohonan cuti dibuat' || $c->logStatusProbis->output == 'Permohonan cuti dikembalikan' || $c->logStatusProbis->output == 'Draft perpanjangan cuti dibuat' || $c->logStatusProbis->output == 'Perpanjangan cuti dikembalikan')

                                <button class="btn btn-sm btn-icon btn-info btnLihatCuti" data-bs-toggle="tooltip"
                                        data-bs-placement="top" title="Lihat" data-id="{{ $c->id }}"
                                        data-tglmulai="{{ AppHelper::instance()->indonesian_date($c->tglMulaiCuti, 'j F Y', '') }}"
                                        data-tglselesai="{{ AppHelper::instance()->indonesian_date($c->tglSelesaiCuti, 'j F Y', '') }}"
                                        data-alasan="{{ $c->alasanCuti }}" data-nokampus="{{ $c->nomorSuratKampus }}"
                                        data-tglsuratkampus="{{ AppHelper::instance()->indonesian_date($c->tglSuratKampus, 'j F Y', '') }}"
                                        data-nosponsor="{{ $c->nomorSuratSponsor }}"
                                        data-tglsuratsponsor="{{ AppHelper::instance()->indonesian_date($c->tglSuratKepSponsor, 'j F Y', '') }}"
                                        data-filesuratsponsor="{{ env('APP_URL') . '/files/' . $c->pathSuratIjinCutiSponsor }}"
                                        data-filesuratkampus="{{ env('APP_URL') . '/files/' . $c->pathSuratIjinCutiKampus }}"
                                        data-nomorkep="{{ $c->nomorSuratKep }}"
                                        data-tglkep="{{ $c->tglSuratKep ? AppHelper::instance()->indonesian_date($c->tglSuratKep, 'j F Y', '') : '' }}"
                                        data-tgltmt="{{ $c->tglTmtPenempatan ? AppHelper::instance()->indonesian_date($c->tglTmtPenempatan, 'j F Y', '') : '' }}"
                                        data-lokasipenempatan="{{ $c->namaKantorPenempatan }}"
                                        data-file="{{ $c->pathKepPenempatan }}">
                                    <i class="fa fa-eye"></i>
                                </button>

                                <button class="btn btn-sm btn-icon btn-warning btnEditCuti" data-bs-placement="top"
                                        title="Edit" data-bs-toggle="tooltip" data-id="{{ $c->id }}"
                                        data-tglmulai="{{ $c->tglMulaiCuti }}"
                                        data-tglselesai="{{ $c->tglSelesaiCuti }}" data-alasan="{{ $c->alasanCuti }}"
                                        data-nokampus="{{ $c->nomorSuratKampus }}"
                                        data-tglsuratkampus="{{ $c->tglSuratKampus }}"
                                        data-nosponsor="{{ $c->nomorSuratSponsor }}"
                                        data-tglsuratsponsor="{{ $c->tglSuratKepSponsor }}"
                                        data-pathsuratkampus_lama="{{ $c->pathSuratIjinCutiKampus }}"
                                        data-pathsuratkampus="{{ env('APP_URL') . '/files/' . $c->pathSuratIjinCutiKampus }}"
                                        data-pathsuratsponsor_lama="{{ $c->pathSuratIjinCutiSponsor }}"
                                        data-pathsuratsponsor="{{ env('APP_URL') . '/files/' . $c->pathSuratIjinCutiSponsor }}">
                                    <i class="fa fa-edit"></i>
                                </button>

                                <button data-id="{{ $c->id }}" class="btn btn-sm btn-icon btn-danger btnHapusCuti"
                                        data-url="{{ env('APP_URL') . '/tubel/administrasi/cuti-belajar' }}"
                                        data-bs-toggle="tooltip"
                                        data-bs-placement="top" title="Hapus">
                                    <i class="fa fa-trash"></i>
                                </button>

                                @if ($c->logStatusProbis->output == 'Draft perpanjangan cuti dibuat' || $c->logStatusProbis->output == 'Perpanjangan cuti dikembalikan')
                                    <button class="btn btn-sm btn-primary btnKirimCuti"
                                            data-url="{{ env('APP_URL') . '/tubel/administrasi/cuti-belajar/update' }}"
                                            data-bs-toggle="tooltip" data-bs-placement="top" title="Kirim"
                                            data-id="{{ $c->id }}" data-act="45d0e173-be8e-465b-a35e-f38c6ea2fc9d">
                                        <i class="fa fa-paper-plane"></i> Kirim
                                    </button>
                                @else
                                    <button class="btn btn-sm btn-primary btnKirimCuti"
                                            data-url="{{ env('APP_URL') . '/tubel/administrasi/cuti-belajar/update' }}"
                                            data-bs-toggle="tooltip" data-bs-placement="top" title="Kirim"
                                            data-id="{{ $c->id }}" data-act="2edff958-c7cf-4e66-9627-8c7521ed5931">
                                        <i class="fa fa-paper-plane"></i> Kirim
                                    </button>
                                @endif

                            @else

                                <button class="btn btn-sm btn-icon btn-info btnLihatCuti" data-bs-toggle="tooltip"
                                        data-bs-placement="top" title="Lihat" data-id="{{ $c->id }}"
                                        data-tglmulai="{{ AppHelper::instance()->indonesian_date($c->tglMulaiCuti, 'j F Y', '') }}"
                                        data-tglselesai="{{ AppHelper::instance()->indonesian_date($c->tglSelesaiCuti, 'j F Y', '') }}"
                                        data-alasan="{{ $c->alasanCuti }}" data-nokampus="{{ $c->nomorSuratKampus }}"
                                        data-tglsuratkampus="{{ AppHelper::instance()->indonesian_date($c->tglSuratKampus, 'j F Y', '') }}"
                                        data-nosponsor="{{ $c->nomorSuratSponsor }}"
                                        data-tglsuratsponsor="{{ AppHelper::instance()->indonesian_date($c->tglSuratKepSponsor, 'j F Y', '') }}"
                                        data-filesuratsponsor="{{ env('APP_URL') . '/files/' . $c->pathSuratIjinCutiSponsor }}"
                                        data-filesuratkampus="{{ env('APP_URL') . '/files/' . $c->pathSuratIjinCutiKampus }}"
                                        data-nomorkep="{{ $c->nomorSuratKep }}"
                                        data-tglkep="{{ $c->tglSuratKep ? AppHelper::instance()->indonesian_date($c->tglSuratKep, 'j F Y', '') : '' }}"
                                        data-tgltmt="{{ $c->tglTmtPenempatan ? AppHelper::instance()->indonesian_date($c->tglTmtPenempatan, 'j F Y', '') : '' }}"
                                        data-lokasipenempatan="{{ $c->namaKantorPenempatan }}"
                                        data-file="{{ $c->pathKepPenempatan }}">
                                    <i class="fa fa-eye"></i>
                                </button>

                                @if ($c->flagPerpanjangan != 1 && $c->logStatusProbis->output == 'Kep Penempatan diterbitkan')
                                    <button class="btn btn-icon btn-sm btn-warning btnPerpanjangCuti"
                                            data-idcuti="{{ $c->id }}" data-bs-toggle="tooltip" data-bs-placement="top"
                                            title="Perpanjang Cuti">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                @endif

                            @endif

                        @else

                            <button class="btn btn-sm btn-icon btn-info btnLihatCuti" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Lihat" data-id="{{ $c->id }}"
                                    data-tglmulai="{{ AppHelper::instance()->indonesian_date($c->tglMulaiCuti, 'j F Y', '') }}"
                                    data-tglselesai="{{ AppHelper::instance()->indonesian_date($c->tglSelesaiCuti, 'j F Y', '') }}"
                                    data-alasan="{{ $c->alasanCuti }}" data-nokampus="{{ $c->nomorSuratKampus }}"
                                    data-tglsuratkampus="{{ AppHelper::instance()->indonesian_date($c->tglSuratKampus, 'j F Y', '') }}"
                                    data-nosponsor="{{ $c->nomorSuratSponsor }}"
                                    data-tglsuratsponsor="{{ AppHelper::instance()->indonesian_date($c->tglSuratKepSponsor, 'j F Y', '') }}"
                                    data-filesuratsponsor="{{ env('APP_URL') . '/files/' . $c->pathSuratIjinCutiSponsor }}"
                                    data-filesuratkampus="{{ env('APP_URL') . '/files/' . $c->pathSuratIjinCutiKampus }}"
                                    data-nomorkep="{{ $c->nomorSuratKep }}"
                                    data-tglkep="{{ AppHelper::instance()->indonesian_date($c->tglSuratKep, 'j F Y', '') }}"
                                    data-tgltmt="{{ AppHelper::instance()->indonesian_date($c->tglTmtPenempatan, 'j F Y', '') }}"
                                    data-lokasipenempatan="{{ $c->namaKantorPenempatan }}"
                                    data-file="{{ env('APP_URL') . '/files/' . $c->pathKepPenempatan }}">
                                <i class="fa fa-eye"></i>
                            </button>

                        @endif

                    </td>
                </tr>
            @endforeach

        @else

            <tr class="text-center">
                <td class="text-dark  mb-1 fs-6" colspan="6">Tidak terdapat data</td>
            </tr>

        @endif

        </tbody>
    </table>
</div>
