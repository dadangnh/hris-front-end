<div class="table-responsive rounded border border-gray-300">
    <table class="table table-row-dashed table-row-gray-300 align-middle" id="tabelPengaktifan">
        <thead class="fw-bolder bg-secondary fs-6">
        <tr>
            <th class="ps-4 min-w-200px">Tanggal Aktif</th>
            <th class="min-w-200px">Surat Aktif Kembali</th>
            <th class="text-center min-w-100px">Status</th>
            <th class="text-center min-w-100px">Aksi</th>
        </tr>
        </thead>

        <tbody id="bodyPengaktifan">
        @if ($pengaktifan_kembali != null)

            @foreach ($pengaktifan_kembali as $p)
                <tr class="" id="cut{{ $p->id }}">
                    <td class="ps-4 fs-6">
                        {{ AppHelper::instance()->indonesian_date($p->tglMulaiAktif, 'j F Y', '') }}
                    </td>
                    <td class="fs-6">
                        {{ $p->nomorSuratPengaktifanKembaliKampus }}
                        <br>
                        {{ AppHelper::instance()->indonesian_date($p->tglSuratPengaktifanKembaliKampus, 'j F Y', '') }}
                    </td>
                    <td class="text-center fs-6">
                        @if ($p->logStatusProbis->output == 'Permohonan aktif dikembalikan')
                            <span
                                class="badge badge-light-danger fs-7 fw-bolder">{{ $p->logStatusProbis->output }}</span>
                        @else
                            <span
                                class="badge badge-light-success fs-7 fw-bolder">{{ $p->logStatusProbis->output }}</span>
                        @endif

                        <br>
                        Tiket:
                        <a href="javascript:void(0)" class="text-primary btnTiket"
                           data-tiket="{{ $p->logStatusProbis->nomorTiket }}"
                           data-nama="{{ $p->indukTubel->namaPegawai }}">
                            {{ $p->logStatusProbis->nomorTiket }}
                        </a>
                    </td>
                    <td class="text-center">

                        @if ($action == true)

                            @if ($p->logStatusProbis->outputId->caseId == '3a07b47f-ae14-4ea7-b81d-65834e1a1fc9' || $p->logStatusProbis->output == 'Permohonan aktif dikembalikan')

                                <button class="btn btn-sm btn-icon btn-info btnLihatPermohonanAktif"
                                        data-bs-toggle="tooltip" data-bs-placement="top" title="Lihat"
                                        data-tglmulaiaktif="{{ AppHelper::instance()->indonesian_date($p->tglMulaiAktif, 'j F Y', '') }}"
                                        data-nosurat="{{ $p->nomorSuratPengaktifanKembaliKampus }}"
                                        data-tglsurat="{{ AppHelper::instance()->indonesian_date($p->tglSuratPengaktifanKembaliKampus, 'j F Y', '') }}"
                                        data-file="{{ $p->pathSuratPengaktifanKembaliKampus }}"
                                        data-status="{{ $p->logStatusProbis->output }}" data-nost="{{ $p->nomorSt }}"
                                        data-tglst="{{ AppHelper::instance()->indonesian_date($p->tanggalSt, 'j F Y', '') }}"
                                        data-tglmulaist="{{ AppHelper::instance()->indonesian_date($p->tglMulaiSt, 'j F Y', '') }}"
                                        data-tglselesaist="{{ AppHelper::instance()->indonesian_date($p->tglSelesaiSt, 'j F Y', '') }}"
                                        data-filest="{{ $p->pathSt }}" data-nomorkep="{{ $p->nomorKepPembebasan }}"
                                        data-tglkep="{{ AppHelper::instance()->indonesian_date($p->tglKepPembebasan, 'j F Y', '') }}"
                                        data-tmt="{{ AppHelper::instance()->indonesian_date($p->tglTmtKepPembebasan, 'j F Y', '') }}"
                                        data-filekep="{{ $p->pathKepPembebasan }}">
                                    <i class="fa fa-eye"></i>
                                </button>

                                <button class="btn btn-sm btn-icon btn-warning btnEditPermohonanAktif"
                                        data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"
                                        data-id="{{ $p->id }}"
                                        data-tglmulaiaktif="{{ $p->tglMulaiAktif }}"
                                        data-nosurat="{{ $p->nomorSuratPengaktifanKembaliKampus }}"
                                        data-tglsurat="{{ $p->tglSuratPengaktifanKembaliKampus }}"
                                        data-file="{{ $p->pathSuratPengaktifanKembaliKampus }}">
                                    <i class="fa fa-edit"></i>
                                </button>

                                <button class="btn btn-sm btn-icon btn-danger btnHapusPermohonanAktif"
                                        data-id="{{ $p->id }}" data-bs-toggle="tooltip" data-bs-placement="top"
                                        title="Hapus">
                                    <i class="fa fa-trash"></i>
                                </button>

                                <button class="btn btn-sm btn-primary btnKirimPermohonanAktif" data-id="{{ $p->id }}"
                                        data-bs-toggle="tooltip" data-bs-placement="top" title="Kirim">
                                    <i class="fa fa-paper-plane"></i> Kirim
                                </button>

                            @else

                                <button class="btn btn-sm btn-icon btn-info btnLihatPermohonanAktif"
                                        data-bs-toggle="tooltip" data-bs-placement="top" title="Lihat"
                                        data-tglmulaiaktif="{{ AppHelper::instance()->indonesian_date($p->tglMulaiAktif, 'j F Y', '') }}"
                                        data-nosurat="{{ $p->nomorSuratPengaktifanKembaliKampus }}"
                                        data-tglsurat="{{ AppHelper::instance()->indonesian_date($p->tglSuratPengaktifanKembaliKampus, 'j F Y', '') }}"
                                        data-file="{{ $p->pathSuratPengaktifanKembaliKampus }}"
                                        data-status="{{ $p->logStatusProbis->output }}" data-nost="{{ $p->nomorSt }}"
                                        data-tglst="{{ AppHelper::instance()->indonesian_date($p->tanggalSt, 'j F Y', '') }}"
                                        data-tglmulaist="{{ AppHelper::instance()->indonesian_date($p->tglMulaiSt, 'j F Y', '') }}"
                                        data-tglselesaist="{{ AppHelper::instance()->indonesian_date($p->tglSelesaiSt, 'j F Y', '') }}"
                                        data-filest="{{ $p->pathSt }}" data-nomorkep="{{ $p->nomorKepPembebasan }}"
                                        data-tglkep="{{ AppHelper::instance()->indonesian_date($p->tglKepPembebasan, 'j F Y', '') }}"
                                        data-tmt="{{ AppHelper::instance()->indonesian_date($p->tglTmtKepPembebasan, 'j F Y', '') }}"
                                        data-filekep="{{ $p->pathKepPembebasan }}">
                                    <i class="fa fa-eye"></i>
                                </button>

                            @endif

                        @else

                            <button class="btn btn-sm btn-icon btn-info btnLihatPermohonanAktif"
                                    data-bs-toggle="tooltip" data-bs-placement="top" title="Lihat"
                                    data-tglmulaiaktif="{{ AppHelper::instance()->indonesian_date($p->tglMulaiAktif, 'j F Y', '') }}"
                                    data-nosurat="{{ $p->nomorSuratPengaktifanKembaliKampus }}"
                                    data-tglsurat="{{ AppHelper::instance()->indonesian_date($p->tglSuratPengaktifanKembaliKampus, 'j F Y', '') }}"
                                    data-file="{{ $p->pathSuratPengaktifanKembaliKampus }}"
                                    data-status="{{ $p->logStatusProbis->output }}" data-nost="{{ $p->nomorSt }}"
                                    data-tglst="{{ AppHelper::instance()->indonesian_date($p->tanggalSt, 'j F Y', '') }}"
                                    data-tglmulaist="{{ AppHelper::instance()->indonesian_date($p->tglMulaiSt, 'j F Y', '') }}"
                                    data-tglselesaist="{{ AppHelper::instance()->indonesian_date($p->tglSelesaiSt, 'j F Y', '') }}"
                                    data-filest="{{ $p->pathSt }}" data-nomorkep="{{ $p->nomorKepPembebasan }}"
                                    data-tglkep="{{ AppHelper::instance()->indonesian_date($p->tglKepPembebasan, 'j F Y', '') }}"
                                    data-tmt="{{ AppHelper::instance()->indonesian_date($p->tglTmtKepPembebasan, 'j F Y', '') }}"
                                    data-filekep="{{ $p->pathKepPembebasan }}">
                                <i class="fa fa-eye"></i>
                            </button>

                        @endif

                    </td>
                </tr>
            @endforeach

        @else

            <tr class="text-center">
                <td class="text-dark mb-1 fs-6" colspan="4">Tidak terdapat data</td>
            </tr>

        @endif

        </tbody>
    </table>
</div>
