@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">

            <div class="card border-warning">
                <div class="card-header" id="card-header">
                    <h3 class="card-title">
                        <span class="fw-bold fs-4 text-white">Administrasi Pegawai Tugas Belajar</span>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="col-md row mb-3">
                        <div class="col">
                            @if ($action == true)
                                <a href="{{ url('tubel/administrasi') }}" class="btn btn-sm btn-secondary mb-3"><i
                                        class="fa fa-reply"></i> Kembali</a>
                            @else
                                <a href="{{ url('tubel/monitoring') }}" class="btn btn-sm btn-secondary mb-3"><i
                                        class="fa fa-reply"></i> Kembali</a>
                            @endif
                        </div>
                        @if ($action == true)
                            <div class="col text-end">
                                <a href="{{ url('tubel/administrasi/' . $tubel->id . '/update') }}"
                                   class="btn btn-sm btn-primary fs-6">
                                    <i class="fa fa-edit"></i> Perbarui Data
                                </a>
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                            <div class="row mb-3">
                                <div class="col-md col-lg">
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Jenjang Pendidikan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->jenjangPendidikanId ? $tubel->jenjangPendidikanId->jenjangPendidikan : '#N/A' }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Program Beasiswa</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->programBeasiswa ? $tubel->programBeasiswa : '#N/A' }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Kampus STAN</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->flagStan == 0 ? 'Tidak' : 'Ya' }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Lokasi Pendidikan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->lokasiPendidikanId ? $tubel->lokasiPendidikanId->lokasi : '#N/A' }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Lama Pendidikan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->lamaPendidikan ? $tubel->lamaPendidikan . ' Tahun' : '#N/A' }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Total Semester</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->totalSemester ? $tubel->totalSemester . ' Semester' : '#N/A' }}</span>
                                        </div>
                                    </div>
                                    <div class="mb-2">
                                        <br>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Kantor Asal</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaKantorAsal ? $tubel->namaKantorAsal : '#N/A' }}
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Jabatan Asal</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaJabatanAsal ? $tubel->namaJabatanAsal : '#N/A' }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md col-lg">
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Nomor Surat Tugas</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->nomorSt ? $tubel->nomorSt : '#N/A' }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Tanggal</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->tglSt ? AppHelper::instance()->indonesian_date($tubel->tglSt, 'j F Y', '') : '#N/A' }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Tanggal Mulai</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->tglMulaiSt ? AppHelper::instance()->indonesian_date($tubel->tglMulaiSt, 'j F Y', '') : '#N/A' }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Tanggal Selesai</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->tglSelesaiSt ? AppHelper::instance()->indonesian_date($tubel->tglSelesaiSt, 'j F Y', '') : '#N/A' }} </span>
                                        </div>
                                    </div>
                                    <div class="mb-2">
                                        <br>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Nomor KEP Pembebasan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->nomorKepPembebasan ? $tubel->nomorKepPembebasan : '#N/A' }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Tanggal</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->tglKepPembebasan ? AppHelper::instance()->indonesian_date($tubel->tglKepPembebasan, 'j F Y', '') : '#N/A' }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">TMT</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->tmtKepPembebasan ? AppHelper::instance()->indonesian_date($tubel->tmtKepPembebasan, 'j F Y', '') : '#N/A' }} </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md col-lg">
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Email Non Kedinasan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->emailNonPajak ? $tubel->emailNonPajak : '#N/A' }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Nomor Handphone</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->noHandphone ? $tubel->noHandphone : '#N/A' }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Alamat</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->alamatTinggal ? $tubel->alamatTinggal : '#N/A' }} </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-8">
                            <div class="col-lg row mb-6">
                                <div class="col-lg text-end">
                                    @if ($action == true)
                                        <button class="btn btn-sm btn-primary fs-6 btnAddPt"
                                                data-program_pt="{{ $tubel->lokasiPendidikanId ? $tubel->lokasiPendidikanId->lokasi : '' }}"
                                                data-jml_pt="{{ $countperguruantinggi }}">
                                            <i class="fa fa-plus"></i> Tambah Perguruan Tinggi
                                        </button>
                                    @endif
                                </div>
                            </div>
                            <div class="table-responsive bg-gray-400 rounded border border-gray-300">
                                <table class="table table-row-dashed table-row-gray-300 align-middle">
                                    <thead>
                                    <tr class="fw-bolder bg-secondary">
                                        <th class="ps-4 min-w-200px">Nama Perguruan Tinggi</th>
                                        <th class="min-w-125px">Program Studi</th>
                                        <th class="min-w-125px">Negara</th>
                                        <th class="min-w-150px">Tanggal Mulai Studi</th>

                                        @if ($action == true)
                                            <th class="min-w-100px text-center">Aksi</th>
                                        @endif

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if ($perguruantinggi != null)
                                        @foreach ($perguruantinggi as $p)
                                            <tr id="pid{{ $p->id }}" class="">
                                                <td class="ps-4 text-dark fs-6">
                                                    {{ $p->perguruanTinggiId->namaPerguruanTinggi }}
                                                </td>
                                                <td class="text-dark fs-6">
                                                    {{ $p->programStudiId->programStudi }}
                                                </td>
                                                <td class="text-dark fs-6">
                                                    {{ $p->negaraPtId->namaNegara }}
                                                </td>
                                                <td class="text-dark fs-6">
                                                    {{ AppHelper::instance()->indonesian_date($p->tglMulai, 'j F Y', '') }}
                                                </td>

                                                @if ($action == true)
                                                    <td class="text-center">
                                                        <a href="javascript:void(0)"
                                                           class="btn btn-sm btn-icon btn-warning btnEditPt"
                                                           data-toggle="modal" data-target="#update_perguruan_tinggi"
                                                           data-id="{{ $p->id }}"
                                                           data-id_perguruan_tinggi="{{ $p->perguruanTinggiId->id }}"
                                                           data-id_program_studi="{{ $p->programStudiId->id }}"
                                                           data-id_negara="{{ $p->negaraPtId->id }}"
                                                           data-tgl_mulai_pendidikan="{{ $p->tglMulai }}"
                                                           data-bs-toggle="tooltip" data-bs-placement="top"
                                                           title="Edit">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        <button class="btn btn-sm btn-icon btn-danger btnHapusPt"
                                                                data-id="{{ $p->id }}"
                                                                data-url="{{ env('APP_URL') . '/tubel/administrasi/perguruan-tinggi/' }}"
                                                                data-bs-toggle="tooltip" data-bs-placement="top"
                                                                title="Hapus">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                @endif

                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="5">
                                                Tidak terdapat data
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                    <div class="card border-warning mt-3" id="tab">
                        <div class="card-header card-header-stretch" id="card-header">
                            <div class="card-toolbar">
                                <ul class="nav nav-tabs nav-line-tabs nav-stretch border-transparent fs-5 fw-bolder"
                                    id="kt_security_summary_tabs">
                                    <li class="nav-item">
                                        <a class="nav-link text-active-primary {{ $tab == 'lps' ? 'active' : '' }}"
                                           href="{{ $action == true ? url('tubel/administrasi/' . $tubel->id . '?tab=lps') : url('tubel/monitoring/' . $tubel->id . '?tab=lps') }}">Laporan
                                            Perkembangan Studi</a>
                                    </li>
                                    <div class="d-flex align-items-center">
                                        <span id="nav-space" class="bullet h-25px w-1px"></span>
                                    </div>
                                    <li class="nav-item">
                                        <a class="nav-link text-active-primary {{ $tab == 'cuti' ? 'active' : '' }}"
                                           href="{{ $action == true ? url('tubel/administrasi/' . $tubel->id . '?tab=cuti') : url('tubel/monitoring/' . $tubel->id . '?tab=cuti') }}">Pengajuan
                                            Cuti Belajar</a>
                                    </li>
                                    <div class="d-flex align-items-center">
                                        <span id="nav-space" class="bullet h-25px w-1px"></span>
                                    </div>
                                    <li class="nav-item">
                                        <a class="nav-link text-active-primary {{ $tab == 'pengaktifan' ? 'active' : '' }}"
                                           href="{{ $action == true ? url('tubel/administrasi/' . $tubel->id . '?tab=pengaktifan') : url('tubel/monitoring/' . $tubel->id . '?tab=pengaktifan') }}">Permohonan
                                            Aktif Kembali</a>
                                    </li>
                                    <div class="d-flex align-items-center">
                                        <span id="nav-space" class="bullet h-25px w-1px"></span>
                                    </div>
                                    <li class="nav-item">
                                        <a class="nav-link text-active-primary {{ $tab == 'perpanjangan' ? 'active' : '' }}"
                                           href="{{ $action == true ? url('tubel/administrasi/' . $tubel->id . '?tab=perpanjangan') : url('tubel/monitoring/' . $tubel->id . '?tab=perpanjangan') }}">Perpanjangan
                                            ST Tubel</a>
                                    </li>
                                    <div class="d-flex align-items-center">
                                        <span id="nav-space" class="bullet h-25px w-1px"></span>
                                    </div>
                                    <li class="nav-item">
                                        <a class="nav-link text-active-primary {{ $tab == 'usulanriset' ? 'active' : '' }}"
                                           href="{{ $action == true ? url('tubel/administrasi/' . $tubel->id . '?tab=usulanriset') : url('tubel/monitoring/' . $tubel->id . '?tab=riset') }}">Usulan
                                            Topik Riset</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane fade {{ $tab == 'lps' ? 'active show' : '' }}" id="lps"
                                     role="tabpanel">
                                    @if ($action == true)
                                        <div class="row">
                                            <div class="col">
                                                <div class="text-end mb-4">
                                                    <button href="#" data-bs-toggle="modal"
                                                            data-bs-target="#add_laporan_perkembangan_studi"
                                                            class="btn btn-primary btn-sm fs-6">
                                                        <i class="fa fa-plus"></i> Buat Laporan
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if ($tab == 'lps')
                                        @include('tubel.administrasi.detail_tabel_lps')
                                    @endif
                                </div>

                                <div class="tab-pane fade {{ $tab == 'cuti' ? 'active show' : '' }}" id="cuti"
                                     role="tabpanel">
                                    @if ($action == true)
                                        <div class="row">
                                            <div class="col">
                                                <div class="text-end mb-4">
                                                    <button href="#" data-bs-toggle="modal" data-bs-target="#add_cuti"
                                                            class="btn btn-primary btn-sm fs-6">
                                                        <i class="fa fa-plus"></i> Buat Permohonan
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if ($tab == 'cuti')
                                        @include('tubel.administrasi.detail_tabel_cuti')
                                    @endif
                                </div>

                                <div class="tab-pane fade {{ $tab == 'pengaktifan' ? 'active show' : '' }}"
                                     id="pengaktifan" role="tabpanel">
                                    @if ($action == true)
                                        <div class="row">
                                            <div class="col">
                                                <div class="text-end mb-4">
                                                    <button href="#" data-bs-toggle="modal"
                                                            data-bs-target="#add_permohonan_aktif"
                                                            class="btn btn-primary btn-sm fs-6">
                                                        <i class="fa fa-plus"></i> Buat Permohonan
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if ($tab == 'pengaktifan')
                                        @include('tubel.administrasi.detail_tabel_pengaktifankembali')
                                    @endif
                                </div>

                                <div class="tab-pane fade {{ $tab == 'perpanjangan' ? 'active show' : '' }}"
                                     id="perpanjangan" role="tabpanel">
                                    @if ($action == true)
                                        <div class="row">
                                            <div class="col">
                                                <div class="text-end mb-4">
                                                    <button class="btn btn-primary btn-sm fs-6 btnPerpanjanganSt"
                                                            data-stan="{{ $tubel->flagStan }}">
                                                        <i class="fa fa-plus"></i> Buat Permohonan
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if ($tab == 'perpanjangan')
                                        @include('tubel.administrasi.detail_tabel_perpanjanganst')
                                    @endif
                                </div>

                                <div class="tab-pane fade {{ $tab == 'usulanriset' ? 'active show' : '' }}" id="riset"
                                     role="tabpanel">
                                    @if ($action == true)
                                        <div class="row">
                                            <div class="col">
                                                <div class="text-end mb-4">
                                                    <button class="btn btn-primary btn-sm fs-6 btnUsulanRiset"
                                                            data-bs-toggle="modal" data-bs-target="#add_usulan_riset">
                                                        <i class="fa fa-plus"></i> Buat Usulan
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if ($tab == 'usulanriset')
                                        @include('tubel.administrasi.detail_tabel_usulantopikriset')
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    @if ($tab == 'usulanriset')
                        <div class="card border-warning mt-3">
                            <div class="card-header {{ $selesai_studi == null ? '' : 'ribbon' }}" id="card-header">
                                <div class="card-title fw-bold fs-4 text-white">
                                    Selesai Studi
                                </div>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <h2>Laporan berakhir Masa Studi</h2>
                                </div>
                                @if ($action == true)
                                    @if ($selesai_studi == null || $selesai_studi->logStatusProbis->output !== 'Kep Penempatan diterbitkan')
                                        <div class="row mb-2">
                                            <div class="mb-3 text-end">
                                                <a href="{{ asset('assets/templates') }}/Format_Laporan_Selesai_Studi.rtf"
                                                   class="btn btn-outline btn-outline-primary btn-active-light-primary btn-sm fs-6">
                                                    <i class="fa fa-file"></i> Unduh Form Laporan Selesai Studi
                                                </a>
                                            </div>
                                            <div class="d-flex justify-content-end mb-2 text-end">
                                                @if ($selesai_studi == null)
                                                    <button class="btn btn-primary btn-sm fs-6 me-2"
                                                            data-bs-toggle="modal"
                                                            data-bs-target="#add_laporan_hasil_studi">
                                                        <i class="fa fa-plus"></i> Buat Laporan
                                                    </button>
                                                @endif
                                                @if ($selesai_studi !== null)
                                                    @if ($selesai_studi->logStatusProbis->output !== 'Laporan Berakhir Masa Studi dikirim' && $selesai_studi->logStatusProbis->output !== 'Laporan Berakhir Masa Studi diterima')
                                                        <button class="btn btn-sm btn-primary fs-6 btnKirimLaporanStudi"
                                                                data-id="{{ $selesai_studi->id }}">
                                                            <i class="fa fa-paper-plane"></i> Kirim Laporan
                                                        </button>

                                                        @switch($selesai_studi->statusLaporDiri->status)
                                                            @case('Lulus')
                                                                @php
                                                                    $ipk = $no_skl = $tgl_skl = $file_skl = $no_ijazah = $tgl_ijazah = $file_ijazah = $no_transkrip = $tgl_transkrip = $file_transkrip = 1;
                                                                @endphp

                                                                <button
                                                                    class="btn btn-sm btn-outline btn-outline-primary btn-active-light-primary fs-6 ms-2 btnEditHasilStudi"
                                                                    data-status="{{ $selesai_studi->statusLaporDiri->status }}"
                                                                    data-pt="{{ $countperguruantinggi }}"
                                                                    data-id="{{ $selesai_studi->id }}"
                                                                    data-tgl_kelulusan="{{ $selesai_studi->tanggalLulus }}"
                                                                    data-tipe_dokumen_penelitian="{{ $selesai_studi->tipeDokPenelitian->id }}"
                                                                    data-file_draft_dokumen_penelitian="{{ $selesai_studi->pathDokumenPenelitian }}"
                                                                    data-file_lps="{{ $selesai_studi->pathLaporanSelesaiStudi }}"
                                                                    @foreach ($perguruantinggi as $pt)
                                                                        data-ipk_pt_{{ $ipk++ }}="{{ $pt->ipk }}"
                                                                    data-no_skl_pt_{{ $no_skl++ }}="{{ $pt->nomorSkl }}"
                                                                    data-tgl_skl_pt_{{ $tgl_skl++ }}="{{ $pt->tglSkl }}"
                                                                    data-file_skl_pt_{{ $file_skl++ }}="{{ $pt->pathSkl }}"
                                                                    data-no_ijazah_pt_{{ $no_ijazah++ }}="{{ $pt->nomorIjazah }}"
                                                                    data-tgl_ijazah_pt_{{ $tgl_ijazah++ }}="{{ $pt->tglIjazah }}"
                                                                    data-file_ijazah_pt_{{ $file_ijazah++ }}="{{ $pt->pathIjazah }}"
                                                                    data-no_transkrip_pt_{{ $no_transkrip++ }}="{{ $pt->nomorTranskripNilai }}"
                                                                    data-tgl_transkrip_pt_{{ $tgl_transkrip++ }}="{{ $pt->tglTranskripNilai }}"
                                                                    data-file_transkrip_pt_{{ $file_transkrip++ }}="{{ $pt->pathTranskripNilai }}"
                                                                    @endforeach
                                                                >
                                                                    <i class="fa fa-edit"></i>Edit
                                                                </button>
                                                                @break

                                                            @case('Belum Lulus')
                                                                <button
                                                                    class="btn btn-sm btn-outline btn-outline-primary btn-active-light-primary fs-6 ms-2 btnEditHasilStudi"
                                                                    data-status="{{ $selesai_studi->statusLaporDiri->status }}"
                                                                    data-id="{{ $selesai_studi->id }}"
                                                                    data-tahapan_studi="{{ $selesai_studi->tahapBelumLulus->id }}"
                                                                    data-file_timeline="{{ $selesai_studi->pathTimelineRencanaStudi }}"
                                                                    data-tipe_dokumen_penelitian="{{ $selesai_studi->tipeDokPenelitian->id }}"
                                                                    data-file_draft_dokumen_penelitian="{{ $selesai_studi->pathDokumenPenelitian }}">
                                                                    <i class="fa fa-edit"></i>Edit
                                                                </button>
                                                                @break

                                                            @case('Belum Lulus')
                                                                <button
                                                                    class="btn btn-sm btn-outline btn-outline-primary btn-active-light-primary fs-6 ms-2 btnEditHasilStudi"
                                                                    data-status="{{ $selesai_studi->statusLaporDiri->status }}"
                                                                    data-id="{{ $selesai_studi->id }}"
                                                                    data-tahapan_studi="{{ $selesai_studi->tahapBelumLulus->id }}"
                                                                    data-file_timeline="{{ $selesai_studi->pathTimelineRencanaStudi }}"
                                                                    data-tipe_dokumen_penelitian="{{ $selesai_studi->tipeDokPenelitian->id }}"
                                                                    data-file_draft_dokumen_penelitian="{{ $selesai_studi->pathDokumenPenelitian }}">
                                                                    <i class="fa fa-edit"></i>Edit
                                                                </button>
                                                                @break

                                                            @case('Mengundurkan Diri')
                                                                <button
                                                                    class="btn btn-sm btn-outline btn-outline-primary btn-active-light-primary fs-6 ms-2 btnEditHasilStudi"
                                                                    data-status="{{ $selesai_studi->statusLaporDiri->status }}"
                                                                    data-id="{{ $selesai_studi->id }}"
                                                                    data-alasan_pengunduran_diri="{{ $selesai_studi->alasanMundur }}"
                                                                    data-mundur_semester="{{ $selesai_studi->semesterKe }}"
                                                                    data-tahun_akademik="{{ $selesai_studi->tahunAkademik }}"
                                                                    data-file_permohonan="{{ $selesai_studi->pathSuratPermohonanMundur }}"
                                                                    data-no_surat_persetujuan="{{ $selesai_studi->noSuratPersetujuanMundur }}"
                                                                    data-tgl_surat_persetujuan="{{ $selesai_studi->tglSuratPersetujuanMundur }}"
                                                                    data-file_dokumen_surat_persetujuan="{{ $selesai_studi->pathSuratPersetujuanMundur }}">
                                                                    <i class="fa fa-edit"></i>Edit
                                                                </button>
                                                                @break

                                                            @case('Drop Out')
                                                                <button
                                                                    class="btn btn-sm btn-outline btn-outline-primary btn-active-light-primary fs-6 ms-2 btnEditHasilStudi"
                                                                    data-status="{{ $selesai_studi->statusLaporDiri->status }}"
                                                                    data-id="{{ $selesai_studi->id }}"
                                                                    data-status="{{ $selesai_studi->statusLaporDiri->status }}"
                                                                    data-semester_do="{{ $selesai_studi->semesterKe }}"
                                                                    data-tahun_do="{{ $selesai_studi->tahunAkademik }}"
                                                                    data-no_surat_do="{{ $selesai_studi->nomorSuratDo }}"
                                                                    data-tgl_surat_keterangan_do="{{ $selesai_studi->tglSuratDo }}"
                                                                    data-file_surat_keterangan_do="{{ $selesai_studi->pathSuratDo }}">
                                                                    <i class="fa fa-edit"></i>Edit
                                                                </button>
                                                                @break

                                                        @endswitch
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                    @if ($selesai_studi != null)
                                        @if ($selesai_studi->logStatusProbis->output == 'Kep Penempatan diterbitkan')
                                            <div class="row">
                                                <div class="mb-3 text-end">
                                                    <a href="{{ env('APP_URL') }}/files/{{ $selesai_studi->pathSuratKepPenempatan }}"
                                                       target="_blank"
                                                       class="btn btn-outline btn-outline-primary btn-active-primary btn-sm fs-6">
                                                        <i class="fa fa-file"></i>
                                                        File KEP Penempatan
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="row mt-3">
                                            <div class="col-lg">
                                                <div class="fs-2tx fw-bolder mb-4">
                                    <span class="d-inline-block position-relative">
                                        <h1>{{ strtoupper($selesai_studi->statusLaporDiri->status) }}</h1>
                                        @php
                                            switch ($selesai_studi->statusLaporDiri->status) {
                                                case 'Lulus':
                                                    $color = 'success';
                                                    break;
                                                case 'Belum Lulus':
                                                    $color = 'warning';
                                                    break;
                                                case 'Mengundurkan Diri':
                                                    $color = 'danger';
                                                    break;
                                                case 'Drop Out':
                                                    $color = 'danger';
                                                    break;
                                            }
                                        @endphp
                                        <span
                                            class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-{{ $color }} translate rounded"></span>
                                    </span>
                                                    @if ($selesai_studi->statusLaporDiri->status == 'Lulus')
                                                        @php
                                                            $count_pt = count($perguruantinggi);
                                                            $ijazah = 0;

                                                            foreach ($perguruantinggi as $key => $item) {
                                                                if ($item->nomorIjazah) {
                                                                    $ijazah++;
                                                                }
                                                            }
                                                        @endphp

                                                        @if ($ijazah == 0)
                                                            <span
                                                                class="text-muted fw-bold fs-7">Belum Ada Ijazah</span>
                                                        @elseif($ijazah == $count_pt)
                                                            <span class="text-muted fw-bold fs-7">Ijazah Lengkap</span>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg">
                                                <div class="ps-2 fw-bolder my-2 text-end">
                                                    @if ($selesai_studi !== null)
                                                        Status :
                                                        @switch($selesai_studi->logStatusProbis->output)
                                                            @case('Laporan Berakhir Masa Studi ditolak')
                                                                <span
                                                                    class="badge badge-light-danger fs-7 fw-bolder">{{ $selesai_studi == null ? '' : $selesai_studi->logStatusProbis->output }}</span>
                                                                @break

                                                            @default
                                                                <span
                                                                    class="badge badge-light-success fs-7 fw-bolder">{{ $selesai_studi == null ? '' : $selesai_studi->logStatusProbis->output }}</span>
                                                        @endswitch
                                                        <br>
                                                        <a href="javascript:void(0)" class="text-primary btnTiket"
                                                           data-tiket="{{ $selesai_studi->logStatusProbis->nomorTiket }}"
                                                           data-nama="{{ $selesai_studi->dataIndukTubelId->namaPegawai }}">
                                                            {{ $selesai_studi->logStatusProbis->nomorTiket }}
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endif

                                @if ($selesai_studi != null)
                                    @include('tubel.administrasi.detail_tabel_lapor_diri')
                                @endif
                            </div>
                            <div class="separator separator-dashed border-gray-300 m-3"></div>
                            <div class="card-body">
                                <div class="row">
                                    <h2>Hasil Akhir Studi</h2>
                                </div>
                                @if (!empty($hasil_akhir))
                                    @if (empty($selesai_studi->statusLaporDiriHasilStudi))
                                        <div class="col-lg text-end">
                                            <button class="btn btn-primary btn-sm fs-6 me-2" data-bs-toggle="modal"
                                                    data-bs-target="#add_laporan_akhir_hasil_studi">
                                                <i class="fa fa-plus"></i> Rekam hasil Studi
                                            </button>
                                        </div>
                                    @elseif ($hasil_akhir->logStatusProbis->output == 'Ijazah diunggah')
                                        <div class="col-lg text-end">
                                            <button class="btn btn-primary btn-sm fs-6 me-2 btnRekamIjazah"
                                                    data-bs-toggle="modal"
                                                    data-bs-target="#add_laporan_akhir_hasil_studi_ijazah"
                                                    data-id="{{ $hasil_akhir->id }}">
                                                <i class="fa fa-plus"></i> Rekam Ijazah
                                            </button>
                                        </div>
                                    @else
                                        @if ($hasil_akhir->logStatusProbis->output != 'Hasil Akhir Studi Mengundurkan Diri' && $hasil_akhir->logStatusProbis->output != 'Hasil Akhir Studi Lulus dikirim' && $hasil_akhir->logStatusProbis->output !== 'Hasil Akhir Studi Lulus diterima' && $hasil_akhir->logStatusProbis->output !== 'Ijazah dikirim' && $hasil_akhir->logStatusProbis->output !== 'Hasil Akhir Studi DO')
                                            <div class="d-flex justify-content-end mb-2 text-end">
                                                @if ($selesai_studi !== null || $selesai_studi->logStatusProbis->output !== 'Laporan Berakhir Masa Studi dikirim' )
                                                    @if ($hasil_akhir->logStatusProbis->output == 'Ijazah ditolak')
                                                        <button class="btn btn-sm btn-primary fs-6 btnKirimIjazah"
                                                                data-id="{{ $hasil_akhir->id }}">
                                                            <i class="fa fa-paper-plane"></i> Kirim Ijazah
                                                        </button>
                                                    @else
                                                        <button class="btn btn-sm btn-primary fs-6 btnKirimHasilAkhir"
                                                                data-id="{{ $hasil_akhir->id }}">
                                                            <i class="fa fa-paper-plane"></i> Kirim Laporan
                                                        </button>
                                                    @endif

                                                    @switch($hasil_akhir->statusLaporDiriHasilStudi->status)
                                                        @case('Lulus')
                                                            @if ($hasil_akhir->logStatusProbis->output == 'Ijazah dikirim' || $hasil_akhir->logStatusProbis->output == 'Ijazah ditolak')
                                                                @php
                                                                    $ipk = $no_skl = $tgl_skl = $file_skl = $no_ijazah = $tgl_ijazah = $file_ijazah = $no_transkrip = $tgl_transkrip = $file_transkrip = 1;
                                                                @endphp

                                                                <button
                                                                    class="btn btn-sm btn-outline btn-outline-primary btn-active-light-primary fs-6 ms-2 btnEditIjazah"
                                                                    @foreach ($perguruantinggi as $pt)
                                                                        data-id="{{ $hasil_akhir->id }}"
                                                                    data-pt="{{ $countperguruantinggi }}"
                                                                    data-no_skl_pt_{{ $no_skl++ }}="{{ $pt->nomorSkl }}"
                                                                    data-no_skl_pt_{{ $no_skl++ }}="{{ $pt->nomorSkl }}"
                                                                    data-tgl_skl_pt_{{ $tgl_skl++ }}="{{ $pt->tglSkl }}"
                                                                    data-file_skl_pt_{{ $file_skl++ }}="{{ $pt->pathSkl }}"
                                                                    data-no_ijazah_pt_{{ $no_ijazah++ }}="{{ $pt->nomorIjazah }}"
                                                                    data-tgl_ijazah_pt_{{ $tgl_ijazah++ }}="{{ $pt->tglIjazah }}"
                                                                    data-file_ijazah_pt_{{ $file_ijazah++ }}="{{ $pt->pathIjazah }}"
                                                                    data-no_transkrip_pt_{{ $no_transkrip++ }}="{{ $pt->nomorTranskripNilai }}"
                                                                    data-tgl_transkrip_pt_{{ $tgl_transkrip++ }}="{{ $pt->tglTranskripNilai }}"
                                                                    data-file_transkrip_pt_{{ $file_transkrip++ }}="{{ $pt->pathTranskripNilai }}"
                                                                    @endforeach
                                                                >
                                                                    <i class="fa fa-edit"></i>Edit
                                                                </button>
                                                            @else
                                                                @php
                                                                    $ipk = $no_skl = $tgl_skl = $file_skl = $no_ijazah = $tgl_ijazah = $file_ijazah = $no_transkrip = $tgl_transkrip = $file_transkrip = 1;
                                                                @endphp

                                                                <button
                                                                    class="btn btn-sm btn-outline btn-outline-primary btn-active-light-primary fs-6 ms-2 btnEditHasilAkhirStudi"
                                                                    data-status="{{ $hasil_akhir->statusLaporDiriHasilStudi->status }}"
                                                                    data-pt="{{ $countperguruantinggi }}"
                                                                    data-id="{{ $hasil_akhir->id }}"
                                                                    data-tgl_kelulusan="{{ $hasil_akhir->statusLaporDiri->tanggalLulus }}"
                                                                    data-tipe_dokumen_penelitian="{{ $hasil_akhir->statusLaporDiri->tipeDokPenelitian->id }}"
                                                                    data-file_draft_dokumen_penelitian="{{ $hasil_akhir->statusLaporDiri->pathDokumenPenelitian }}"
                                                                    data-file_lps="{{ $hasil_akhir->statusLaporDiri->pathLaporanSelesaiStudi }}"
                                                                    @foreach ($perguruantinggi as $pt)
                                                                        data-ipk_pt_{{ $ipk++ }}="{{ $pt->ipk }}"
                                                                    data-no_skl_pt_{{ $no_skl++ }}="{{ $pt->nomorSkl }}"
                                                                    data-tgl_skl_pt_{{ $tgl_skl++ }}="{{ $pt->tglSkl }}"
                                                                    data-file_skl_pt_{{ $file_skl++ }}="{{ $pt->pathSkl }}"
                                                                    data-no_ijazah_pt_{{ $no_ijazah++ }}="{{ $pt->nomorIjazah }}"
                                                                    data-tgl_ijazah_pt_{{ $tgl_ijazah++ }}="{{ $pt->tglIjazah }}"
                                                                    data-file_ijazah_pt_{{ $file_ijazah++ }}="{{ $pt->pathIjazah }}"
                                                                    data-no_transkrip_pt_{{ $no_transkrip++ }}="{{ $pt->nomorTranskripNilai }}"
                                                                    data-tgl_transkrip_pt_{{ $tgl_transkrip++ }}="{{ $pt->tglTranskripNilai }}"
                                                                    data-file_transkrip_pt_{{ $file_transkrip++ }}="{{ $pt->pathTranskripNilai }}"
                                                                    @endforeach
                                                                >
                                                                    <i class="fa fa-edit"></i>Edit
                                                                </button>

                                                            @endif
                                                            @break

                                                        @case('Mengundurkan Diri')
                                                            <button
                                                                class="btn btn-sm btn-outline btn-outline-primary btn-active-light-primary fs-6 ms-2 btnEditHasilAkhirStudi"
                                                                data-status="{{ $hasil_akhir->statusLaporDiriHasilStudi->status }}"
                                                                data-id="{{ $hasil_akhir->id }}"
                                                                data-alasan_pengunduran_diri="{{ $hasil_akhir->statusLaporDiri->alasanMundur }}"
                                                                data-mundur_semester="{{ $hasil_akhir->statusLaporDiri->semesterKe }}"
                                                                data-tahun_akademik="{{ $hasil_akhir->statusLaporDiri->tahunAkademik }}"
                                                                data-file_permohonan="{{ $hasil_akhir->statusLaporDiri->pathSuratPermohonanMundur }}"
                                                                data-no_surat_persetujuan="{{ $hasil_akhir->statusLaporDiri->noSuratPersetujuanMundur }}"
                                                                data-tgl_surat_persetujuan="{{ $hasil_akhir->statusLaporDiri->tglSuratPersetujuanMundur }}"
                                                                data-file_dokumen_surat_persetujuan="{{ $hasil_akhir->statusLaporDiri->pathSuratPersetujuanMundur }}">
                                                                <i class="fa fa-edit"></i>Edit
                                                            </button>
                                                            @break

                                                        @case('Drop Out')
                                                            <button
                                                                class="btn btn-sm btn-outline btn-outline-primary btn-active-light-primary fs-6 ms-2 btnEditHasilAkhirStudi"
                                                                data-status="{{ $hasil_akhir->statusLaporDiriHasilStudi->status }}"
                                                                data-id="{{ $hasil_akhir->id }}"
                                                                data-semester_do="{{ $hasil_akhir->statusLaporDiri->semesterKe }}"
                                                                data-tahun_do="{{ $hasil_akhir->statusLaporDiri->tahunAkademik }}"
                                                                data-no_surat_do="{{ $hasil_akhir->statusLaporDiri->nomorSuratDo }}"
                                                                data-tgl_surat_keterangan_do="{{ $hasil_akhir->statusLaporDiri->tglSuratDo }}"
                                                                data-file_surat_keterangan_do="{{ $hasil_akhir->statusLaporDiri->pathSuratDo }}">
                                                                <i class="fa fa-edit"></i>Edit
                                                            </button>
                                                            @break

                                                    @endswitch
                                                @endif
                                            </div>
                                        @endif
                                        @if ($selesai_studi->statusLaporDiriHasilStudi != null)
                                            <div class="row mt-3">
                                                <div class="col-lg">
                                                    <div class="fs-2tx fw-bolder mb-4">
                                <span class="d-inline-block position-relative">
                                    <h1>{{ strtoupper($hasil_akhir->statusLaporDiriHasilStudi->status) }}</h1>
                                    @php
                                        switch ($hasil_akhir->statusLaporDiriHasilStudi->status) {
                                            case 'Lulus':
                                                $color = 'success';
                                                break;
                                            case 'Mengundurkan Diri':
                                                $color = 'danger';
                                                break;
                                            case 'Drop Out':
                                                $color = 'danger';
                                                break;
                                        }
                                    @endphp
                                    <span
                                        class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-{{ $color }} translate rounded"></span>
                                </span>
                                                        @if ($selesai_studi->statusLaporDiriHasilStudi->status == 'Lulus')
                                                            @php
                                                                $count_pt = count($perguruantinggi);
                                                                $ijazah = 0;

                                                                foreach ($perguruantinggi as $key => $item) {
                                                                    if ($item->nomorIjazah) {
                                                                        $ijazah++;
                                                                    }
                                                                }
                                                            @endphp

                                                            @if ($ijazah == 0)
                                                                <span
                                                                    class="text-muted fw-bold fs-7">Belum Ada Ijazah</span>
                                                            @elseif($ijazah == $count_pt)
                                                                <span
                                                                    class="text-muted fw-bold fs-7">Ijazah Lengkap</span>
                                                            @endif
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg">
                                                    <div class="ps-2 fw-bolder my-2 text-end">
                                                        Status :
                                                        @if ($hasil_akhir->logStatusProbis->output == 'Hasil Akhir Studi ditolak' || $hasil_akhir->logStatusProbis->output == 'Hasil Akhir Studi Lulus ditolak' || $hasil_akhir->logStatusProbis->output == 'Ijazah ditolak')
                                                            <span
                                                                class="badge badge-light-danger fs-7 fw-bolder">{{ $hasil_akhir == null ? '' : $hasil_akhir->logStatusProbis->output }}</span>
                                                        @else
                                                            <span
                                                                class="badge badge-light-success fs-7 fw-bolder">{{ $hasil_akhir == null ? '' : $hasil_akhir->logStatusProbis->output }}</span>
                                                        @endif
                                                        <br>
                                                        <a id="tiket_hasil_akhir" href="javascript:void(0)"
                                                           class="text-primary btnTiket"
                                                           data-tiket="{{ $hasil_akhir->logStatusProbis->nomorTiket }}"
                                                           data-nama="{{ $selesai_studi->dataIndukTubelId->namaPegawai }}">
                                                            {{ $hasil_akhir->logStatusProbis->nomorTiket }}
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            @include('tubel.administrasi.detail_tabel_lapor_diri_akhir')
                                        @endif
                                    @endif
                                @else
                                    <i>Belum ada</i>
                                @endif
                            </div>
                        </div>
                    @endif
                </div>
            </div>

        </div>
    </div>

    @include('tubel.administrasi.detail_modal')
    @include('tubel.modal_tiket')

@endsection

@section('js')
    <script>
        // scroll to view
        if ('{{ !empty($_GET['tab']) }}') {
            document.getElementById("tab").scrollIntoView();
        }
    </script>

    <script src="{{ asset('assets/js/tubel') }}/administrasi-detail.js"></script>
    <script src="{{ asset('assets/js/tubel') }}/log-tiket.js"></script>
@endsection
