@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card border-warning shadow-sm">
                        <div class="card-header" id="card-header">
                            <div class="card-title">
                                {{-- <span class="card-icon">
                                    <i class="fa fa-address-book text-white"></i>
                                </span> --}}
                                <h3 class="fw-bold fs-4 text-white">
                                    Data Induk Pegawai Tugas Belajar
                                </h3>
                            </div>
                        </div>
                        <form id="formTubel" action="" method="post">
                            {{ method_field('patch') }}
                            @csrf
                            <div class="card-body mb-0">
                                <input type="text" name="pegawaiInputId" hidden
                                       value="{{ session()->get('user')['pegawaiId'] }}">
                                <table class="table border gs-7">
                                    <tbody>
                                    <tr class="fw-bold fs-6 text-gray-800 border-bottom border-gray-200">
                                        <div class="col-lg">
                                            <td class="fs-5 col-2">Nama Pegawai</td>
                                            <td class="col-3">: {{ $tubel->namaPegawai }}</td>
                                        </div>
                                        <div class="col-lg">
                                            <td class="fs-5 col-2">Jabatan</td>
                                            <td class="col">: {{ $tubel->namaJabatan }}</td>
                                        </div>
                                    </tr>
                                    <tr class="fw-bold fs-6 text-gray-800 border-bottom border-gray-200">
                                        <div class="col-lg">
                                            <td class="fs-5 col-2">NIP 18</td>
                                            <td class="col">: {{ $tubel->nip18 }}</td>
                                        </div>
                                        <div class="col-lg">
                                            <td class="fs-5 col-2">Unit Organisasi</td>
                                            <td class="col">: {{ $tubel->namaUnitOrg }}</td>
                                        </div>
                                    </tr>
                                    <tr class="fw-bold fs-6 text-gray-800 border-bottom border-gray-200">
                                        <div class="col-lg">
                                            <td class="fs-5 col-2">Pangkat</td>
                                            <td class="col">: {{ $tubel->namaPangkat }}</td>
                                        </div>
                                        <div class="col-lg">
                                            <td class="fs-5 col-2">Kantor</td>
                                            <td class="col">: {{ $tubel->namaKantor }}</td>
                                        </div>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            {{-- <div class="row"> --}}
                            {{-- <div class="col-lg-1"></div> --}}
                            {{-- <div class="col-lg"> --}}
                            <div class="card-body m-5 mt-0">
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Program Beasiswa</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control form-control-solid"
                                               name="programBeasiswa" value="{{ $tubel->programBeasiswa }}" readonly>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Jenjang Pendidikan</label>
                                    <div class="col-lg-3">
                                        <select id="JenjangPendidikan" name="jenjangPendidikanId" data-control="select2"
                                                data-hide-search="true" data-placeholder="Pilih jenjang pendidikan"
                                                type="text" class="form-select form-select-lg"
                                                required>
                                            @if (empty($tubel->jenjangPendidikanId))
                                                <option></option>
                                                @foreach ($jenjang as $j)
                                                    <option value="{{ $j->id }}">{{ $j->jenjangPendidikan }}</option>
                                                @endforeach
                                            @else
                                                @foreach ($jenjang as $j)
                                                    <option
                                                        value="{{ $j->id }}" {{ $j->id == $tubel->jenjangPendidikanId->id ? 'selected' : '' }}>{{ $j->jenjangPendidikan }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-6">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Lokasi Pendidikan</label>
                                    <div class="col-lg-3">
                                        <select id="lokasiPendidikan[]" name="lokasiPendidikanId" data-control="select2"
                                                data-hide-search="true" data-placeholder="Pilih lokasi pendidikan"
                                                type="text" class="form-select form-select-lg" required>
                                            @if (empty($tubel->lokasiPendidikanId))
                                                <option></option>
                                                @foreach ($lokasi as $l)
                                                    <option value="{{ $l->id }}">{{ $l->lokasi }}</option>
                                                @endforeach
                                            @else
                                                @foreach ($lokasi as $l)
                                                    <option
                                                        value="{{ $l->id }}" {{ $l->id == $tubel->lokasiPendidikanId->id ? 'selected' : '' }}>{{ $l->lokasi }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                {{-- <div class="row mb-3">
                                        <label class="col-lg-3 col-form-label fw-bold fs-6">Lama Pendidikan</label>
                                        <div class="col-lg-3">
                                            <input type="text" name="lamaPendidikan" class="form-control" placeholder="hanya angka" value="{{ $tubel->lamaPendidikan }}" aria-describedby="lamaPendidikan" required>
                                        </div>
                                    </div> --}}
                                <div class="row form-group mb-6">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Pendidikan</label>
                                    <div class="col">
                                        <div class="form-check form-check-custom mb-2">
                                            {{-- {{ dd($tubel) }} --}}
                                            <input class="form-check-input" type="radio" value="1" name="fgstan"
                                                   id="flexRadioDefault"
                                                   required {{ $tubel->flagStan == 1 || $tubel->programBeasiswa == 'PKN STAN' ? 'checked' : '' }} />
                                            <label class="form-check-label" for="flexRadioDefault">
                                                PKN STAN
                                            </label>
                                        </div>
                                        <div class="form-check form-check-custom">
                                            <input class="form-check-input" type="radio" value="0" name="fgstan"
                                                   id="flexRadioDefault2"
                                                   required {{ $tubel->flagStan == 0 && $tubel->programBeasiswa != 'PKN STAN' ? 'checked' : '' }} />
                                            <label class="form-check-label" for="flexRadioDefault">
                                                Non PKN STAN
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Lama Pendidikan</label>
                                    <div class="col">
                                        <div class="input-group">
                                            <div class="position-relative d-flex align-items-center">
                                                <input type="text" name="lamaPendidikan"
                                                       class="form-control single_number_mask" placeholder="hanya angka"
                                                       value="{{ $tubel->lamaPendidikan }}" required>
                                            </div>
                                            <div class="symbol symbol-45px me-4 position-left ms-2">
                                                <span class="symbol-label bg-white">
                                                    <span class="text" width="24px" height="24px" viewBox="0 0 24 24">
                                                        Tahun
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Total Semester</label>
                                    <div class="col">
                                        <div class="input-group">
                                            <div class="position-relative d-flex align-items-center">
                                                <input type="text" name="totalSemester"
                                                       class="form-control semester_mask" placeholder="hanya angka"
                                                       value="{{ $tubel->totalSemester }}" required>
                                            </div>
                                            <div class="symbol symbol-45px me-4 position-left ms-5">
                                                <span class="symbol-label bg-white">
                                                    <span class="text" width="24px" height="24px" viewBox="0 0 24 24">
                                                        Semester
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Nomor ST & Tgl ST</label>
                                    <div class="col-lg-3">
                                        <input type="text" name="nomorSt" class="form-control" placeholder="nomor st"
                                               value="{{ $tubel->nomorSt }}" required>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                             height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none"
                                                               fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                      height="4" rx="1"></rect>
                                                                <path
                                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                    fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 flatpickr-input active datepick"
                                                   placeholder="Pilih tanggal " name="tglSt" type="text"
                                                   value="{{ $tubel->tglSt }}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Tgl mulai dan selesai ST</label>
                                    <div class="col-lg-3">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                             height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none"
                                                               fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                      height="4" rx="1"></rect>
                                                                <path
                                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                    fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 flatpickr-input active datepick"
                                                   placeholder="Pilih tanggal " name="tglMulaiSt" type="text"
                                                   value="{{ $tubel->tglMulaiSt }}" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                             height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none"
                                                               fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                      height="4" rx="1"></rect>
                                                                <path
                                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                    fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 flatpickr-input active datepick"
                                                   placeholder="Pilih tanggal " name="tglSelesaiSt" type="text"
                                                   value="{{ $tubel->tglSelesaiSt }}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Nomor & Tgl KEP
                                        Pembebasan</label>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="nomorKepPembebasan"
                                               placeholder="misal: KEP-" value="{{ $tubel->nomorKepPembebasan }}"
                                               required>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                             height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none"
                                                               fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                      height="4" rx="1"></rect>
                                                                <path
                                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                    fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 flatpickr-input active datepick"
                                                   placeholder="Pilih tanggal " name="tglKepPembebasan" type="text"
                                                   value="{{ $tubel->tglKepPembebasan }}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">TMT KEP Pembebasan</label>
                                    <div class="col-lg-3">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                             height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none"
                                                               fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                      height="4" rx="1"></rect>
                                                                <path
                                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                    fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 flatpickr-input active datepick"
                                                   placeholder="Pilih tanggal " name="tmtKepPembebasan" type="text"
                                                   value="{{ $tubel->tmtKepPembebasan }}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Email Non Kedinasan</label>
                                    <div class="col-lg-3">
                                        <input type="email" class="form-control" name="emailNonPajak"
                                               placeholder="Selain email pajak " value="{{ $tubel->emailNonPajak }}"
                                               required>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Nomor Handphone</label>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="noHandphone"
                                               placeholder="misal: 08XXXXXXXXX" value="{{ $tubel->noHandphone }}"
                                               required>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Alamat</label>
                                    <div class="col-lg-6">
                                        <textarea type="text" class="form-control" name="alamatTinggal" rows="4"
                                                  placeholder="Alamat tempat tinggal "
                                                  required>{{ $tubel->alamatTinggal }}</textarea>
                                    </div>
                                </div>
                                {{-- <div class="row mb-3">
                                        <label class="col-lg-3 col-form-label fw-bold fs-6">Kantor Asal</label>
                                        <div class="col-lg-3">
                                            <input type="text" class="form-control" name="namaKantorAsal" placeholder="kantor asal" value="{{ $tubel->noHandphone }}" disabled>
                                        </div>
                                        <button class="btn btn-md btn-icon btn-secondary btnCariKantor">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <input type="hidden" id="idNamaKantorAsal" name="idNamaKantorAsal">
                                                <input type="text" class="form-control" id="namaKantorAsal" name="namaKantorAsal" placeholder="kantor asal"  readonly="readonly">
                                                <div class="input-group-append">
                                                </div>
                                                <button class="btn btn-icon btn-secondary" data-bs-toggle="modal" data-bs-target="#cari_kantor_asal" type="button" >
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div> --}}
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Kantor Asal</label>
                                    <div class="col-lg-6">
                                        <select id="kantorAsal" name="kantorAsal" class="form-select form-select-lg">
                                            @if ($tubel->kantorIdAsal != null)
                                                <option value="{{ $tubel->kantorIdAsal }}|{{ $tubel->namaKantorAsal }}"
                                                        selected>{{ $tubel->namaKantorAsal }}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Jabatan Asal</label>
                                    <div class="col-lg-6">
                                        <select id="jabatanAsal" name="jabatanAsal" class="form-select form-select-lg">
                                            @if ($tubel->jabatanIdAsal != null)
                                                <option
                                                    value="{{ $tubel->jabatanIdAsal }}|{{ $tubel->namaJabatanAsal }}"
                                                    selected>{{ $tubel->namaJabatanAsal }}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            {{-- </div> --}}
                            {{-- <div class="col-lg-1"></div> --}}
                            {{-- </div> --}}

                            <div class="card-footer">
                                <div class="text-center">
                                    <a href="{{ url()->previous() }}" class="btn btn-sm btn-secondary my-1"><i
                                            class="fa fa-reply"></i>Batal</a>
                                    <button type="submit" class="btn btn-sm btn-success my-1">
                                        <i class="fa fa-save"></i> Simpan
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $(".datepick").flatpickr();

        // masking
        Inputmask({
            mask: "9",
            repeat: 4,
            greedy: false,
        }).mask(".tahun_mask");

        Inputmask({
            mask: "9",
            repeat: 1,
            greedy: false,
        }).mask(".single_number_mask");

        Inputmask({
            mask: "9",
            repeat: 2,
            greedy: false,
        }).mask(".semester_mask");

        Inputmask({
            mask: "9.99",
        }).mask(".ipk_mask");

        $("#formTubel").validate({
            rules: {
                lamaPendidikan: {
                    required: true,
                    digits: true,
                    maxlength: 2
                },
                totalSemester: {
                    required: true,
                    digits: true,
                    maxlength: 2
                },
                emailNonPajak: {
                    required: true,
                    email: true
                },
                noHandphone: {
                    required: true,
                    digits: true
                },
                noHandphone: {
                    required: true,
                    digits: true
                },
                noHandphone: {
                    required: true,
                    digits: true
                }
            }
        });

        // get kantor & jabatan
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // get kantor iam
            $("#kantorAsal").select2({

                placeholder: 'Pilih kantor asal',
                minimumInputLength: 3,
                ajax: {
                    url: '{{ env('APP_URL') }}' + '/tubel/administrasi/get-kantor',
                    dataType: 'json',
                    type: 'post',
                    delay: 300,
                    data: function (params) {
                        return {
                            search: params.term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response

                        };
                    }
                }
            })

            // get jabatan iam
            $("#jabatanAsal").select2({

                placeholder: 'Pilih jabatan asal',
                minimumInputLength: 3,
                ajax: {
                    url: '{{ env('APP_URL') }}' + '/tubel/administrasi/get-jabatan',
                    dataType: 'json',
                    type: 'post',
                    delay: 300,
                    data: function (params) {
                        return {
                            search: params.term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    }
                }
            })
        });

        // spinner / loading button
        function BtnLoading(elem) {
            $(elem).attr("data-original-text", $(elem).html());
            $(elem).prop("disabled", true);
            $(elem).html('<i class="spinner-border spinner-border-sm"></i>  Loading');
        }

        // spinner off / reset button
        function BtnReset(elem) {
            $(elem).prop("disabled", false);
            $(elem).html($(elem).attr("data-original-text"));
        }
    </script>
@endsection
