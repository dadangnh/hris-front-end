@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">

            <div class="card border-warning">
                <div class="card-header" id="card-header">
                    <h3 class="card-title">
                        <span class="fw-bold fs-4 text-white">Detail Tugas Belajar</span>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="col-md row mb-3">
                        <div class="col">
                            <a href="javascript: history.go(-1)" class="btn btn-sm btn-secondary mb-3"><i
                                    class="fa fa-reply"></i> Kembali</a>
                        </div>
                        <div class="col text-end">
                        </div>
                    </div>
                    <div class="card">
                        <div class="rounded border p-4 d-flex flex-column mb-6">
                            <div class="row mb-3">
                                <div class="col-md col-lg">
                                    <div class="form-group row mb-1">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Jenjang Pendidikan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->jenjangPendidikanId ? $tubel->jenjangPendidikanId->jenjangPendidikan : '#N/A' }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-1">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Program Beasiswa</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->programBeasiswa ? $tubel->programBeasiswa : '#N/A' }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-1">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Kampus Stan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->flagStan == 0 ? 'Tidak' : 'Ya' }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-1">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Lokasi</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->lokasiPendidikanId ? $tubel->lokasiPendidikanId->lokasi : '#N/A'  }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-1">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Lama Pendidikan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->lamaPendidikan ? $tubel->lamaPendidikan . ' Tahun' : '#N/A'}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-1">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Total Semester</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->totalSemester ? $tubel->totalSemester . ' Semester': '#N/A' }}</span>
                                        </div>
                                    </div>
                                    <div class="mb-1">
                                        <br>
                                    </div>
                                    <div class="form-group row mb-1">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Kantor Asal</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaKantor ? $tubel->namaKantor : '#N/A'  }}
                                        </div>
                                    </div>
                                    <div class="form-group row mb-1">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Jabatan Asal</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaJabatan ? $tubel->namaJabatan : '#N/A'  }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md col-lg">
                                    <div class="form-group row mb-1">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Nomor Surat Tugas</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->nomorSt ? $tubel->nomorSt : '#N/A' }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-1">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Tanggal</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ AppHelper::instance()->indonesian_date($tubel->tglSt,'j F Y','') }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-1">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Tanggal Mulai</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ AppHelper::instance()->indonesian_date($tubel->tglMulaiSt,'j F Y','') }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-1">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Tanggal Selesai</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ AppHelper::instance()->indonesian_date($tubel->tglSelesaiSt,'j F Y','') }} </span>
                                        </div>
                                    </div>
                                    <div class="mb-1">
                                        <br>
                                    </div>
                                    <div class="form-group row mb-1">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Nomor KEP Pembebasan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->nomorKepPembebasan ? $tubel->nomorKepPembebasan : '#N/A' }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-1">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Tanggal</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ AppHelper::instance()->indonesian_date($tubel->tglKepPembebasan,'j F Y','') }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-1">
                                        <label class="col-lg-5 col-md fw-bold fs-6">TMT</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ AppHelper::instance()->indonesian_date($tubel->tmtKepPembebasan,'j F Y','') }} </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md col-lg">
                                    <div class="form-group row mb-1">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Email Non Kedinasan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->emailNonPajak ? $tubel->emailNonPajak : '#N/A' }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-1">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Nomor Handphone</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->noHandphone ? $tubel->noHandphone : '#N/A' }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-1">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Alamat</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->alamatTinggal ? $tubel->alamatTinggal : '#N/A' }} </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rounded border p-4 d-flex flex-column mb-8">

                            <div class="table-responsive bg-gray-400 rounded border border-gray-300">
                                <table class="table table-striped align-middle">
                                    <thead>
                                    <tr class="fw-bolder bg-secondary">
                                        <th class="ps-4 min-w-200px rounded-start">Nama Perguruan Tinggi</th>
                                        <th class="min-w-125px">Program Studi</th>
                                        <th class="min-w-125px">Negara</th>
                                        <th class="min-w-150px">Tanggal Mulai Studi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($perguruantinggi as $p)
                                        <tr id="pid{{ $p->id }}" class="">
                                            <td class="ps-4 text-dark  mb-1 fs-6">
                                                {{ $p->perguruanTinggiId->namaPerguruanTinggi }}
                                                {{-- <span id="namaPerguruanTinggi" value="{{ $p->perguruanTinggiId->namaPerguruanTinggi }}" hidden></span> --}}
                                            </td>
                                            <td class="text-dark  mb-1 fs-6">
                                                {{ $p->programStudiId->programStudi }}
                                            </td>
                                            <td class="text-dark  mb-1 fs-6">
                                                {{ $p->negaraPtId->namaNegara }}
                                            </td>
                                            <td class="text-dark  mb-1 fs-6">
                                                {{ $p->tglMulai }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <!--end::Table body-->
                                </table>
                                <!--end::Table-->
                            </div>
                        </div>
                    </div>
                    <div class="card border-warning" id="tab">
                        <div class="card-header card-header-stretch" id="card-header">
                            <div class="card-toolbar">
                                <ul class="nav nav-tabs nav-line-tabs nav-stretch border-transparent fs-5 fw-bolder"
                                    id="kt_security_summary_tabs">
                                    <li class="nav-item">
                                        <a class="nav-link text-active-primary <?php if ($tab=='lps'){ echo 'active';} ?>"
                                           href="{{ url('tubel/pegawai/'.$tubel->id.'?url=tubel/pegawai&tab=lps#tab') }}">Laporan
                                            Perkembangan Studi</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-active-primary <?php if ($tab=='cuti'){ echo 'active';} ?>"
                                           href="{{ url('tubel/pegawai/'.$tubel->id.'?url=tubel/pegawai&tab=cuti#tab') }}">Pengajuan
                                            Cuti Belajar</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div class="card-body">
                            <!--begin::Tab content-->
                            <div class="tab-content">

                                <div class="tab-pane fade <?php if ($tab=='lps'){ echo 'active show';} ?> " id="lps"
                                     role="tabpanel">
                                    @if ($tab=='lps')
                                        @include('tubel.detail_tabel_lps')
                                    @endif
                                </div>

                                <div class="tab-pane fade <?php if ($tab=='cuti'){ echo 'active show';} ?> " id="cuti"
                                     role="tabpanel">
                                    @if ($tab=='cuti')
                                        @include('tubel.detail_tabel_cuti')
                                    @endif
                                </div>
                            </div>
                            <!--end::Tab content-->
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--end::Container-->
    </div>

    {{-- modal lihat laporan perkembangan studi --}}
    <div class="modal fade" id="get_laporan_perkembangan_studi" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-900px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h4 class="modal-title">Form Laporan Perkembangan Studi</h4>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                         aria-label="Close">
                        <span class="svg-icon svg-icon-2x"></span>
                    </div>
                </div>
                <form class="form" method="post" action="{{ url('tubel/administrasi/'.$tubel->id.'/lps') }}">
                    @csrf
                    {{ method_field('post') }}
                    <div class="modal-body scroll-y px-5 px-lg-10">
                        <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                            <div class="row mb-2">
                                <div class="col-md col-lg">
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Nama</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaPegawai }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">NIP 18</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->nip18 }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Pangkat</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaPangkat }} </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md col-lg">
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Jabatan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaJabatan }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Unit Organisasi</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaUnitOrg }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Kantor</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span
                                                class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaKantor }} </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Nama perguran Tinggi</label>
                                <div class="col-lg">
                                    <input id="perguruantinggi" type="text"
                                           class="form-control form-control form-control-solid" disabled/>
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Semester ke-</label>
                                <div class="col-lg">
                                    <input id="semesterke" type="text"
                                           class="form-control form-control form-control-solid" disabled/>
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Semester</label>
                                <div class="col-lg">
                                    <input id="semester" type="text"
                                           class="form-control form-control form-control-solid" disabled/>
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Tahun Akademik</label>
                                <div class="col-lg">
                                    <input id="tahun" type="text" class="form-control form-control form-control-solid"
                                           disabled/>
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">IPK</label>
                                <div class="col-lg">
                                    <input id="lihatipk" type="text"
                                           class="form-control form-control form-control-solid" disabled/>
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Jumlah SKS</label>
                                <div class="col-lg">
                                    <input id="jumlah_sks" type="text"
                                           class="form-control form-control form-control-solid" disabled/>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ url('assets/dist') }}/assets/js/custom/modals/create-app.js"></script>
    <script src="{{ url('assets/dist') }}/assets/js/custom/modals/create-account.js"></script>

    <script>
        // form perguruan tinggi
        $("#tgl_mulai_pendidikan").flatpickr();
        $("#tgl_mulai_pendidikan2").flatpickr();
        $("#tglSurat").flatpickr();
        $("#tglSuratSponsor").flatpickr();

        // button lihat lps
        $(".btnLihatLps").click(function () {
            $('#get_laporan_perkembangan_studi').modal('show');

            var id = $(this).data('id')

            $('#perguruantinggi').val($(this).data('pt')).change();
            $('#semesterke').val($(this).data('smtke')).change();
            $('#semester').val($(this).data('smt')).change();
            $('#tahun').val($(this).data('th')).change();
            $('#lihatipk').val($(this).data('ipk')).change();
            $('#jumlah_sks').val($(this).data('sks')).change();
        });

    </script>
@endsection
