@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card mb-3 border-warning shadow-sm">
                    <div id="card-header" class="card-header">
                        <div class="card-title fw-bold fs-4 text-white">
                            Administrasi Pegawai Tugas Belajar
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10"></div>
                            <div class="col-md-2">
                                <div class="text-end mb-4">
                                    <button id="btnTarikData" class="btn btn-sm btn-primary fs-6">
                                        <i class="fa fa-download text-white"></i>Tarik Data
                                    </button>
                                    <button class="btn btn-primary btn-sm fs-6" type="button" hidden disabled>
                                        Loading...
                                        <span class="spinner-border spinner-border-sm" role="status"
                                              aria-hidden="true"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive bg-gray-400 rounded border border-gray-300">
                            <table class="table table-striped align-middle" id="tabel_tubel">
                                <thead class="fw-bolder bg-secondary fs-6">
                                <tr class="text-right">
                                    <th class="ps-4 text-center">No</th>
                                    <th class="">Jenjang Pendidikan</th>
                                    <th class="">Program Beasiswa / Lokasi</th>
                                    <th class="">Perguruan Tinggi</th>
                                    <th class="">Lama Pendidikan / Total Semester</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center pe-4">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if ($tubel != null)
                                    @php
                                        $nomor = 1;
                                    @endphp

                                    @foreach ($tubel as $t)
                                        <tr class="text-right">
                                            <td class="ps-4 text-center">
                                                {{ $nomor++ }}
                                            </td>

                                            <td class="-dark  mb-1 fs-6">
                                                {{ $t->jenjangPendidikanId ? $t->jenjangPendidikanId->jenjangPendidikan : '' }}
                                            </td>
                                            <td class="text-dark  mb-1 fs-6">
                                                {{ $t->programBeasiswa }}
                                                <br> {{ $t->lokasiPendidikanId ? $t->lokasiPendidikanId->lokasi : '' }}
                                            </td>
                                            <td class="text-dark  mb-1 fs-6">
                                                @if (!empty($perguruantinggi))
                                                    @foreach ($perguruantinggi as $p)
                                                        @if ($p['idTubel'] == $t->id)
                                                            {{ $p['namaPt'] }}
                                                            <br>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td class="text-dark  mb-1 fs-6">
                                                {{ $t->lamaPendidikan ? $t->lamaPendidikan . ' Tahun' : null }} <br>
                                                {{ $t->totalSemester ? $t->totalSemester . ' Semester' : null }}

                                            </td>
                                            <td class="text-center text-dark  mb-1 fs-6">
                                                <span
                                                    class="badge badge-light-success fs-7 fw-bolder">{{ $t->logStatusProbis ? $t->logStatusProbis->outputId->caseOutput : null }}</span>
                                            </td>
                                            <td class="text-center text-dark mb-1 fs-6 pe-4">
                                                <a href="{{ url('tubel/administrasi/' . $t->id) }}"
                                                   class="btn btn-sm btn-primary">
                                                    <i class="fa fa-cog"></i>Kelola
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach

                                @else
                                    <tr class="text-center">
                                        <td class="text-dark  mb-1 fs-6" colspan="7">Tidak terdapat data</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('assets/js/tubel') }}/administrasi.js"></script>
@endsection
