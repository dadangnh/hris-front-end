@switch($selesai_studi->statusLaporDiri->status)

    @case('Lulus')
        <div class="table-responsive rounded border border-gray-300">
            <table class="table table-row-dashed table-row-gray-300 align-middle">
                <thead class="fw-bold bg-secondary fs-6">
                <tr>
                    <th class="ps-4" style="width: 150px !important;">Tanggal Lulus</th>
                    <th class="col-lg-8">Data Pendukung</th>
                    <th class="col-lg">Karya Tulis</th>
                </tr>
                </thead>

                <tbody id="body">
                @if ($selesai_studi != null)

                    <tr id="usulantopikriset-{{ $selesai_studi->id }}">
                        <td class="ps-4 fs-6">
                            {{ AppHelper::instance()->indonesian_date($selesai_studi->tanggalLulus, 'j F Y', '') }}
                        </td>
                        <td class="fs-6">
                            <div class="row mb-4">
                                <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                    <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                    <div class="ms-1 fw-bold">
                                        <a href="{{ url('files/' . $selesai_studi->pathLaporanSelesaiStudi) }}"
                                           target="_blank" class="fs-6 text-hover-primary fw-bold">Laporan Selesai
                                            Studi.pdf</a>
                                    </div>
                                </div>
                            </div>
                            @foreach ($perguruantinggi as $pt)
                                <div class="separator separator-dashed border-gray-300 mt-4 mb-3"></div>
                                <div class="row">
                                    <div class="fs-3 fw-bolder">
                                        {{ $pt->perguruanTinggiId->namaPerguruanTinggi }}
                                    </div>
                                    <div class="col-lg">
                                        <div class="mt-1">
                                            <b>Transkrip</b>
                                            <br>
                                            IPK :
                                            @if ($pt->ipk)
                                                {{ $pt->ipk }}
                                            @else
                                                <i><span class="text-muted fw-bold text-muted fs-6">belum ada</span></i>
                                            @endif
                                            <br>
                                            Nomor:
                                            @if ($pt->nomorTranskripNilai)
                                                {{ $pt->nomorTranskripNilai }}
                                            @else
                                                <i><span class="text-muted fw-bold text-muted fs-6">belum ada</span></i>
                                            @endif
                                            <br>
                                            Tanggal:
                                            @if ($pt->tglTranskripNilai)
                                                {{ AppHelper::instance()->indonesian_date($pt->tglTranskripNilai, 'j F Y', '') }}
                                            @else
                                                <i><span class="text-muted fw-bold text-muted fs-6">belum ada</span></i>
                                            @endif
                                            <br>
                                            @if ($pt->pathTranskripNilai)
                                                <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1 fw-bold">
                                                        <a href="{{ url('files/' . $pt->pathTranskripNilai) }}"
                                                           target="_blank" class="fs-6 text-hover-primary">Transkrip
                                                            Nilai.pdf</a>
                                                    </div>
                                                </div>
                                            @else
                                                File: <i><span
                                                        class="text-muted fw-bold text-muted  fs-6">belum ada</span></i>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg">
                                        <b>Surat Keterangan Lulus</b>
                                        <br>
                                        Nomor:
                                        @if ($pt->nomorSkl)
                                            {{ $pt->nomorSkl }}
                                        @else
                                            <i><span class="text-muted fw-bold text-muted fs-6">belum ada</span></i>
                                        @endif
                                        <br>
                                        Tanggal:
                                        @if ($pt->tglSkl)
                                            {{ AppHelper::instance()->indonesian_date($pt->tglSkl, 'j F Y', '') }}
                                        @else
                                            <i><span class="text-muted fw-bold text-muted fs-6">belum ada</span></i>
                                        @endif
                                        <br>
                                        @if ($pt->pathSkl)
                                            <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                                <img alt="" class="w-25px me-3"
                                                     src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                <div class="ms-1 fw-bold">
                                                    <a href="{{ url('files/' . $pt->pathSkl) }}" target="_blank"
                                                       class="fs-6 text-hover-primary">Surat Keterangan Lulus.pdf</a>
                                                </div>
                                            </div>
                                        @else
                                            File: <i><span class="text-muted fw-bold text-muted  fs-6">belum ada</span></i>
                                        @endif
                                    </div>

                                    @if ($pt->nomorIjazah)
                                        <div class="col-lg">
                                            <b>Ijazah</b>
                                            <br>

                                            Nomor:
                                            @if ($pt->nomorIjazah)
                                                {{ $pt->nomorIjazah }}
                                            @else
                                                <i><span class="text-muted fw-bold text-muted fs-6">belum ada</span></i>
                                            @endif

                                            <br>
                                            Tanggal:
                                            @if ($pt->tglIjazah)
                                                {{ AppHelper::instance()->indonesian_date($pt->tglIjazah, 'j F Y', '') }}
                                            @else
                                                <i><span class="text-muted fw-bold text-muted fs-6">belum ada</span></i>
                                            @endif

                                            <br>
                                            @if ($pt->pathIjazah)
                                                <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1 fw-bold">
                                                        <a href="{{ url('files/' . $pt->pathIjazah) }}" target="_blank"
                                                           class="fs-6 text-hover-primary">Ijazah.pdf</a>
                                                    </div>
                                                </div>
                                            @else
                                                File: <i><span
                                                        class="text-muted fw-bold text-muted  fs-6">belum ada</span></i>
                                            @endif
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        </td>
                        <td class="fs-6">
                            Jenis Penelitian: {{ $selesai_studi->tipeDokPenelitian->tipeDokumen }}
                            <br>
                            <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1 fw-bold">
                                    <a href="{{ url('files/' . $selesai_studi->pathDokumenPenelitian) }}"
                                       target="_blank" class="fs-6 text-hover-primary fw-bold">Dokumen
                                        Penelitian.pdf</a>
                                </div>
                            </div>
                        </td>
                    </tr>

                @else

                    <tr class="text-center">
                        <td class="text-dark  mb-1 fs-6" colspan="4">Tidak terdapat data</td>
                    </tr>

                @endif

                </tbody>
            </table>
        </div>
        @break
    @case('Belum Lulus')
        <div class="table-responsive rounded border border-gray-300">
            <table class="table table-row-dashed table-row-gray-300 align-middle">
                <thead class="fw-bold bg-secondary fs-6">
                <tr>
                    <th class="ps-4 col-lg">Tahap Perkuliahan</th>
                    <th class="col-lg">Timeline Rencana Penyelesaian Studi</th>
                    <th class="col-lg">Draft Dokumen Penelitian</th>
                </tr>
                </thead>

                <tbody id="body">
                @if ($selesai_studi != null)

                    <tr id="usulantopikriset-{{ $selesai_studi->id }}">
                        <td class="ps-4 fs-6">
                            {{ $selesai_studi->tahapBelumLulus->tahap }}
                        </td>
                        <td class="fs-6">
                            <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1 fw-bold">
                                    <a href="{{ url('files/' . $selesai_studi->pathTimelineRencanaStudi) }}"
                                       target="_blank" class="fs-6 text-hover-primary fw-bold">Timeline.pdf</a>
                                </div>
                            </div>
                        </td>
                        <td class="fs-6">
                            Jenis Penelitian: {{ $selesai_studi->tipeDokPenelitian->tipeDokumen }}
                            <br>
                            <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1 fw-bold">
                                    <a href="{{ url('files/' . $selesai_studi->pathDokumenPenelitian) }}"
                                       target="_blank" class="fs-6 text-hover-primary fw-bold">Draft Dokumen
                                        Penelitian.pdf</a>
                                </div>
                            </div>
                        </td>
                    </tr>

                @else

                    <tr class="text-center">
                        <td class="text-dark  mb-1 fs-6" colspan="4">Tidak terdapat data</td>
                    </tr>

                @endif

                </tbody>
            </table>
        </div>
        @break
    @case('Mengundurkan Diri')
        <div class="table-responsive rounded border border-gray-300">
            <table class="table table-row-dashed table-row-gray-300 align-middle">
                <thead class="fw-bold bg-secondary fs-6">
                <tr>
                    <th class="ps-4 col-lg">Semester / Tahun Akademik</th>
                    <th class="col-lg-4">Alasan</th>
                    <th class="col-lg">Surat Permohonan</th>
                    <th class="col-lg">Surat Persetujuan dari Perguruan Tinggi</th>
                </tr>
                </thead>

                <tbody id="body">
                @if ($selesai_studi != null)

                    <tr id="usulantopikriset-{{ $selesai_studi->id }}">
                        <td class="ps-4 fs-6">
                            Semester : {{ $selesai_studi->semesterKe }}
                            <br>
                            Tahun : {{ $selesai_studi->tahunAkademik }}
                        </td>
                        <td class="fs-6">
                            {{ $selesai_studi->alasanMundur }}
                        </td>
                        <td class="fs-6">
                            <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1 fw-bold">
                                    <a href="{{ url('files/' . $selesai_studi->pathSuratPermohonanMundur) }}"
                                       target="_blank" class="fs-6 text-hover-primary fw-bold">Surat Permohonan.pdf</a>
                                </div>
                            </div>
                        </td>
                        <td class="fs-6">
                            No. {{ $selesai_studi->noSuratPersetujuanMundur }},
                            tgl {{ AppHelper::instance()->indonesian_date($selesai_studi->tglSuratPersetujuanMundur, 'j F Y', '') }}
                            <br>
                            <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1 fw-bold">
                                    <a href="{{ url('files/' . $selesai_studi->pathSuratPersetujuanMundur) }}"
                                       target="_blank" class="fs-6 text-hover-primary fw-bold">Surat Persetujuan.pdf</a>
                                </div>
                            </div>
                        </td>
                    </tr>

                @else

                    <tr class="text-center">
                        <td class="text-dark  mb-1 fs-6" colspan="4">Tidak terdapat data</td>
                    </tr>

                @endif

                </tbody>
            </table>
        </div>

        @break
    @case('Drop Out')
        <div class="table-responsive rounded border border-gray-300">
            <table class="table table-row-dashed table-row-gray-300 align-middle">
                <thead class="fw-bold bg-secondary fs-6">
                <tr>
                    <th class="ps-4 col-lg">Semester / Tahun Akademik</th>
                    <th class="col-lg">Surat Keterangan dari Perguruan Tinggi</th>
                </tr>
                </thead>

                <tbody id="body">
                @if ($selesai_studi != null)

                    <tr id="usulantopikriset-{{ $selesai_studi->id }}">
                        <td class="ps-4 fs-6">
                            Semester : {{ $selesai_studi->semesterKe }}
                            <br>
                            Tahun : {{ $selesai_studi->tahunAkademik }}
                        </td>
                        <td class="fs-6">
                            No. {{ $selesai_studi->nomorSuratDo }},
                            tgl {{ AppHelper::instance()->indonesian_date($selesai_studi->tglSuratDo, 'j F Y', '') }}
                            <br>
                            <div class="d-flex flex-aligns-center mt-2 pe-10 pe-lg-20">
                                <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1 fw-bold">
                                    <a href="{{ url('files/' . $selesai_studi->pathSuratDo) }}" target="_blank"
                                       class="fs-6 text-hover-primary fw-bold">Surat Keterangan.pdf</a>
                                </div>
                            </div>
                        </td>
                    </tr>

                @else

                    <tr class="text-center">
                        <td class="text-dark  mb-1 fs-6" colspan="4">Tidak terdapat data</td>
                    </tr>

                @endif

                </tbody>
            </table>
        </div>

        @break
    @default

@endswitch
