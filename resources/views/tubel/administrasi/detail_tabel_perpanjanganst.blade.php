<div class="table-responsive rounded border border-gray-300">
    <table class="table table-row-dashed table-row-gray-300 align-middle">
        <thead class="fw-bolder bg-secondary fs-6">
        <tr>
            <th class="ps-4 min-w-200px">Surat Tugas Awal</th>
            <th class="min-w-200px">Tanggal Selesai Perpanjangan</th>
            <th class="text-center min-w-100px">Status</th>
            <th class="text-center min-w-100px">Aksi</th>
        </tr>
        </thead>

        <tbody id="body">
        @if ($perpanjangan != null)

            @foreach ($perpanjangan as $p)

                <tr id="{{ $p->id }}">
                    <td class="ps-4 fs-6">
                        {{ $p->noStAwal }}
                    </td>
                    <td class="fs-6">
                        {{ AppHelper::instance()->indonesian_date($p->tglSelesaiPerpanjangan, 'j F Y', '') }}
                    </td>
                    <td class="text-center text-dark  mb-1 fs-6">

                        @if ($p->logStatusProbis->output == 'Perpanjangan ST ditolak' || $p->logStatusProbis->output == 'Perpanjangan ST dikembalikan untuk diperbaiki')
                            <span
                                class="badge badge-light-danger fs-7 fw-bolder">{{ $p->logStatusProbis->output }}</span>
                        @else
                            <span
                                class="badge badge-light-success fs-7 fw-bolder">{{ $p->logStatusProbis->output }}</span>
                        @endif
                        <br>
                        Tiket:
                        <a href="javascript:void(0)" class="text-primary btnTiket"
                           data-tiket="{{ $p->logStatusProbis->nomorTiket }}"
                           data-nama="{{ $p->dataIndukTubelId->namaPegawai }}">
                            {{ $p->logStatusProbis->nomorTiket }}
                        </a>
                    </td>
                    <td class="text-center">

                        @if ($action == true)

                            @if ($p->logStatusProbis->output == 'Draft perpanjangan ST dibuat' || $p->logStatusProbis->output == 'Perpanjangan ST dikembalikan untuk diperbaiki')

                                @if ($p->perpanjanganStStanId != null)

                                    <button class="btn btn-sm btn-icon btn-info btnLihatPerpanjanganStStan"
                                            data-bs-toggle="tooltip" data-bs-placement="top" title="Lihat"
                                            data-id_stan="{{ $p->perpanjanganStStanId->id }}"
                                            data-alasan_perpanjangan_st_stan="{{ $p->perpanjanganStStanId->alasanPerpanjangan }}"
                                            data-tgl_selesai_perpanjangan_stan="{{ AppHelper::instance()->indonesian_date($p->perpanjanganStStanId->tglSelesaiPerpanjangan, 'j F Y', '') }}"
                                            data-st_awal_stan="{{ $p->perpanjanganStStanId->noStAwal }}"
                                            data-file_st_awal_stan="{{ $p->perpanjanganStStanId->pathStAwal }}"
                                            data-keterangan="{{ $p->perpanjanganStStanId->keteranganDokumenPendukung }}"
                                            data-dokumen_pendukung="{{ $p->perpanjanganStStanId->pathDokumenPendukung }}"
                                            data-nomor_st_stan="{{ $p->pathStAwal }}"
                                            data-tgl_st_stan="{{ AppHelper::instance()->indonesian_date($p->perpanjanganStStanId->tglSelesaiPerpanjangan, 'j F Y', '') }}"
                                            data-tgl_mulai_st_stan="{{ AppHelper::instance()->indonesian_date($p->perpanjanganStStanId->tglSelesaiPerpanjangan, 'j F Y', '') }}"
                                            data-tgl_selesai_st_stan="{{ AppHelper::instance()->indonesian_date($p->perpanjanganStStanId->tglSelesaiPerpanjangan, 'j F Y', '') }}"
                                            data-file_st_stan="{{ $p->pathStAwal }}">
                                        <i class="fa fa-eye"></i>
                                    </button>

                                @else

                                    <button class="btn btn-sm btn-icon btn-info btnLihatPerpanjanganSt"
                                            data-bs-toggle="tooltip" data-bs-placement="top" title="Lihat"
                                            data-alasan_perpanjangan_st_lihat="{{ $p->alasanPerpanjangan }}"
                                            data-tahapan_studi_lihat="{{ $p->tahapanStudi }}"
                                            data-tgl_selesai_perpanjangan_lihat="{{ AppHelper::instance()->indonesian_date($p->tglSelesaiPerpanjangan, 'j F Y', '') }}"
                                            data-st_awal_lihat="{{ $p->noStAwal }}"
                                            data-file_st_awal_lihat="{{ $p->pathStAwal }}"
                                            data-surat_persetujuan_kampus_lihat="{{ $p->alasanPerpanjangan }}"
                                            data-tgl_surat_persetujuan_kampus_lihat="{{ AppHelper::instance()->indonesian_date($p->tglPersetujuanKampus, 'j F Y', '') }}"
                                            data-file_surat_persetujuan_kampus_lihat="{{ $p->pathPersetujuanKampus }}"
                                            data-surat_persetujuan_sponsor_lihat="{{ $p->nomorPersetujuanSponsor }}"
                                            data-tgl_surat_persetujuan_sponsor_lihat="{{ AppHelper::instance()->indonesian_date($p->tglPersetujuanSponsor, 'j F Y', '') }}"
                                            data-file_surat_persetujuan_sponsor_lihat="{{ $p->pathPersetujuanSponsor }}"
                                            data-status="{{ $p->logStatusProbis->output }}"
                                            data-nomor_st_perpanjangan_lihat="{{ $p->nomorStPerpanjangan }}"
                                            data-tgl_st_perpanjangan_lihat="{{ AppHelper::instance()->indonesian_date($p->tglStPerpanjangan, 'j F Y', '') }}"
                                            data-tgl_mulai_st_perpanjangan_lihat="{{ AppHelper::instance()->indonesian_date($p->tglMulaiStPerpanjangan, 'j F Y', '') }}"
                                            data-tgl_selesai_st_perpanjangan_lihat="{{ AppHelper::instance()->indonesian_date($p->tglSelesaiStPerpanjangan, 'j F Y', '') }}"
                                            data-file_st_perpanjangan_lihat="{{ $p->pathStPerpanjangan }}">
                                        <i class="fa fa-eye"></i>
                                    </button>

                                @endif

                                <button class="btn btn-sm btn-icon btn-warning btnEditPerpanjanganSt"
                                        data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"
                                        data-id_perpanjangan_st="{{ $p->id }}"
                                        data-alasan_perpanjangan_st_edit="{{ $p->alasanPerpanjangan }}"
                                        data-tahapan_studi_edit="{{ $p->tahapanStudi }}"
                                        data-tgl_selesai_perpanjangan_edit="{{ $p->tglSelesaiStPerpanjangan }}"
                                        data-st_awal_edit="{{ $p->noStAwal }}"
                                        data-file_st_awal_edit="{{ $p->pathStAwal }}"
                                        data-surat_persetujuan_kampus_edit="{{ $p->alasanPerpanjangan }}"
                                        data-tgl_surat_persetujuan_kampus_edit="{{ $p->tglPersetujuanKampus }}"
                                        data-file_surat_persetujuan_kampus_edit="{{ $p->pathPersetujuanKampus }}"
                                        data-surat_persetujuan_sponsor_edit="{{ $p->nomorPersetujuanSponsor }}"
                                        data-tgl_surat_persetujuan_sponsor_edit="{{ $p->tglPersetujuanSponsor }}"
                                        data-file_surat_persetujuan_sponsor_edit="{{ $p->pathPersetujuanSponsor }}">
                                    <i class="fa fa-edit"></i>
                                </button>


                                <button class="btn btn-sm btn-icon btn-danger btnHapusPerpanjanganSt"
                                        data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus"
                                        data-id="{{ $p->id }}">
                                    <i class="fa fa-trash"></i>
                                </button>

                                <button class="btn btn-sm btn-primary btnKirimPerpanjanganSt" data-bs-toggle="tooltip"
                                        data-bs-placement="top" title="Kirim" data-id="{{ $p->id }}">
                                    <i class="fa fa-paper-plane"></i> Kirim
                                </button>

                            @else

                                @if ($p->perpanjanganStStanId != null)

                                    <button class="btn btn-sm btn-icon btn-info btnLihatPerpanjanganStStan"
                                            data-bs-toggle="tooltip" data-bs-placement="top" title="Lihat"
                                            data-id_stan="{{ $p->perpanjanganStStanId->id }}"
                                            data-alasan_perpanjangan_st_stan="{{ $p->perpanjanganStStanId->alasanPerpanjangan }}"
                                            data-tgl_selesai_perpanjangan_stan="{{ AppHelper::instance()->indonesian_date($p->perpanjanganStStanId->tglSelesaiPerpanjangan, 'j F Y', '') }}"
                                            data-st_awal_stan="{{ $p->perpanjanganStStanId->noStAwal }}"
                                            data-file_st_awal_stan="{{ $p->perpanjanganStStanId->pathStAwal }}"
                                            data-keterangan="{{ $p->perpanjanganStStanId->keteranganDokumenPendukung }}"
                                            data-dokumen_pendukung="{{ $p->perpanjanganStStanId->pathDokumenPendukung }}"
                                            data-nomor_st_stan="{{ $p->pathStAwal }}"
                                            data-tgl_st_stan="{{ AppHelper::instance()->indonesian_date($p->perpanjanganStStanId->tglSelesaiPerpanjangan, 'j F Y', '') }}"
                                            data-tgl_mulai_st_stan="{{ AppHelper::instance()->indonesian_date($p->perpanjanganStStanId->tglSelesaiPerpanjangan, 'j F Y', '') }}"
                                            data-tgl_selesai_st_stan="{{ AppHelper::instance()->indonesian_date($p->perpanjanganStStanId->tglSelesaiPerpanjangan, 'j F Y', '') }}"
                                            data-file_st_stan="{{ $p->pathStAwal }}">
                                        <i class="fa fa-eye"></i>
                                    </button>

                                @else

                                    <button class="btn btn-sm btn-icon btn-info btnLihatPerpanjanganSt"
                                            data-bs-toggle="tooltip" data-bs-placement="top" title="Lihat"
                                            data-alasan_perpanjangan_st_lihat="{{ $p->alasanPerpanjangan }}"
                                            data-tahapan_studi_lihat="{{ $p->tahapanStudi }}"
                                            data-tgl_selesai_perpanjangan_lihat="{{ AppHelper::instance()->indonesian_date($p->tglSelesaiPerpanjangan, 'j F Y', '') }}"
                                            data-st_awal_lihat="{{ $p->noStAwal }}"
                                            data-file_st_awal_lihat="{{ $p->pathStAwal }}"
                                            data-surat_persetujuan_kampus_lihat="{{ $p->alasanPerpanjangan }}"
                                            data-tgl_surat_persetujuan_kampus_lihat="{{ AppHelper::instance()->indonesian_date($p->tglPersetujuanKampus, 'j F Y', '') }}"
                                            data-file_surat_persetujuan_kampus_lihat="{{ $p->pathPersetujuanKampus }}"
                                            data-surat_persetujuan_sponsor_lihat="{{ $p->nomorPersetujuanSponsor }}"
                                            data-tgl_surat_persetujuan_sponsor_lihat="{{ AppHelper::instance()->indonesian_date($p->tglPersetujuanSponsor, 'j F Y', '') }}"
                                            data-file_surat_persetujuan_sponsor_lihat="{{ $p->pathPersetujuanSponsor }}"
                                            data-status="{{ $p->logStatusProbis->output }}"
                                            data-nomor_st_perpanjangan_lihat="{{ $p->nomorStPerpanjangan }}"
                                            data-tgl_st_perpanjangan_lihat="{{ AppHelper::instance()->indonesian_date($p->tglStPerpanjangan, 'j F Y', '') }}"
                                            data-tgl_mulai_st_perpanjangan_lihat="{{ AppHelper::instance()->indonesian_date($p->tglMulaiStPerpanjangan, 'j F Y', '') }}"
                                            data-tgl_selesai_st_perpanjangan_lihat="{{ AppHelper::instance()->indonesian_date($p->tglSelesaiStPerpanjangan, 'j F Y', '') }}"
                                            data-file_st_perpanjangan_lihat="{{ $p->pathStPerpanjangan }}">
                                        <i class="fa fa-eye"></i>
                                    </button>

                                @endif

                            @endif

                        @else

                            <button class="btn btn-sm btn-icon btn-info btnLihatPerpanjanganSt" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Lihat"
                                    data-alasan_perpanjangan_st_lihat="{{ $p->alasanPerpanjangan }}"
                                    data-tahapan_studi_lihat="{{ $p->tahapanStudi }}"
                                    data-tgl_selesai_perpanjangan_lihat="{{ AppHelper::instance()->indonesian_date($p->tglSelesaiPerpanjangan, 'j F Y', '') }}"
                                    data-st_awal_lihat="{{ $p->noStAwal }}"
                                    data-file_st_awal_lihat="{{ $p->pathStAwal }}"
                                    data-surat_persetujuan_kampus_lihat="{{ $p->nomorPersetujuanKampus }}"
                                    data-tgl_surat_persetujuan_kampus_lihat="{{ AppHelper::instance()->indonesian_date($p->tglPersetujuanKampus, 'j F Y', '') }}"
                                    data-file_surat_persetujuan_kampus_lihat="{{ $p->pathPersetujuanKampus }}"
                                    data-surat_persetujuan_sponsor_lihat="{{ $p->nomorPersetujuanSponsor }}"
                                    data-tgl_surat_persetujuan_sponsor_lihat="{{ AppHelper::instance()->indonesian_date($p->tglPersetujuanSponsor, 'j F Y', '') }}"
                                    data-file_surat_persetujuan_sponsor_lihat="{{ $p->pathPersetujuanSponsor }}"
                                    data-status="{{ $p->logStatusProbis->output }}"
                                    data-nomor_st_perpanjangan_lihat="{{ $p->nomorStPerpanjangan }}"
                                    data-tgl_st_perpanjangan_lihat="{{ AppHelper::instance()->indonesian_date($p->tglStPerpanjangan, 'j F Y', '') }}"
                                    data-tgl_mulai_st_perpanjangan_lihat="{{ AppHelper::instance()->indonesian_date($p->tglMulaiStPerpanjangan, 'j F Y', '') }}"
                                    data-tgl_selesai_st_perpanjangan_lihat="{{ AppHelper::instance()->indonesian_date($p->tglSelesaiStPerpanjangan, 'j F Y', '') }}"
                                    data-file_st_perpanjangan_lihat="{{ $p->pathStPerpanjangan }}">
                                <i class="fa fa-eye"></i>
                            </button>

                        @endif

                    </td>
                </tr>
            @endforeach

        @else

            <tr class="text-center">
                <td class="text-dark  mb-1 fs-6" colspan="6">Tidak terdapat data</td>
            </tr>

        @endif

        </tbody>
    </table>
</div>
