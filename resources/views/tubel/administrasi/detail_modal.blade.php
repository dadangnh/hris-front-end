<div class="modal fade" id="add_perguruan_tinggi" aria-hidden="true">
    <div class="modal-dialog mw-600px">
        <div class="modal-content rounded">
            <div class="modal-header">
                <h4 class="text-white">Form Perguruan Tinggi</h4>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form" method="post" action="{{ url('/tubel/administrasi/perguruan-tinggi/' . $tubel->id) }}">
                @csrf
                {{ method_field('post') }}
                <div class="modal-body scroll-y px-10 px-lg-15">
                    <div class="d-flex flex-column mb-8 fv-row">
                        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                            <span class="required">Nama Perguruan Tinggi</span>
                        </label>
                        <select id="tambah_perguruan_tinggi" name="pt" class="form-select form-select-lg">
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="d-flex flex-column mb-8 fv-row">
                        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                            <span class="required">Program Studi</span>
                        </label>
                        <select id="tambah_pt_prodi" name="prodi" data-control="select2" type="text"
                                class="form-select form-select-lg" required>
                            <option></option>
                        </select>
                    </div>
                    <div class="d-flex flex-column mb-8 fv-row">
                        <label class="required fs-6 fw-bold mb-2">Negara</label>
                        <select id="tambah_pt_negara" name="negara" data-control="select2" type="text"
                                class="form-select form-select-lg" required>
                            <option></option>
                        </select>
                    </div>
                    <div class="d-flex flex-column mb-8 fv-row">
                        <label class="required fs-6 fw-bold mb-2">Tanggal mulai pendidikan</label>
                        <div class="position-relative d-flex align-items-center">
                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                <span class="symbol-label bg-secondary">
                                    <span class="svg-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             width="24px" height="24px"
                                             viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                      rx="1"></rect>
                                                <path
                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                    fill="#000000"></path>
                                            </g>
                                        </svg>
                                    </span>
                                </span>
                            </div>
                            <input class="form-control ps-12 flatpickr-input active" id="tgl_mulai_pendidikan2"
                                   placeholder="Pilih tanggal" name="tglMulai" type="text" required>
                        </div>
                    </div>
                </div>
                <div class="text-center mb-8">
                    <button type="button" class="btn btn-sm btn-secondary me-3" data-bs-dismiss="modal"><i
                            class="fa fa-reply"></i>Batal
                    </button>
                    <button type="submit" class="btn btn-sm btn-success">
                        <i class="fa fa-save"></i> Simpan
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="update_perguruan_tinggi" aria-hidden="true">
    <div class="modal-dialog mw-600px">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Form Update Perguruan Tinggi</h3>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form id="form_update_perguruan_tinggi" class="form" method="post"
                  action="{{ url('/tubel/administrasi/perguruan-tinggi/' . $tubel->id) }}">
                @csrf
                {{ method_field('PATCH') }}
                <input type="hidden" id="idTubel" name="idTubel" value="{{ $tubel->id }}">
                <input type="hidden" id="idPerguruanTinggi" name="idPerguruanTinggi" value="">
                <div class="modal-body scroll-y px-10 px-lg-15">
                    <div class="d-flex flex-column mb-8 fv-row">
                        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                            <span class="required">Nama Perguruan Tinggi</span>
                        </label>
                        <select class="form-select" data-control="select2" data-hide-search="true"
                                data-placeholder="Pilih negara ..." id="edit_perguruan_tinggi" name="perguruan_tinggi"
                                value="" required>
                            @foreach ($ref_perguruantinggi->data as $pt)
                                <option value="{{ $pt->id }}">{{ $pt->namaPerguruanTinggi }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="d-flex flex-column mb-8 fv-row">
                        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                            <span class="required">Program Studi</span>
                        </label>
                        <select class="form-select" data-control="select2" data-hide-search="true"
                                data-placeholder="Pilih negara ..." id="edit_pt_prodi" name="prodi" value="" required>
                            @foreach ($ref_prodi as $p)
                                <option value="{{ $p->id }}">{{ $p->programStudi }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="d-flex flex-column mb-8 fv-row">
                        <label class="required fs-6 fw-bold mb-2">Negara</label>
                        <select class="form-select" data-control="select2" data-hide-search="true"
                                data-placeholder="Pilih negara ..." id="edit_pt_negara" name="negara" value="" required>
                            @foreach ($ref_negara->data as $n)
                                <option value="{{ $n->id }}">{{ $n->namaNegara }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="d-flex flex-column mb-8 fv-row">
                        <label class="required fs-6 fw-bold mb-2">Tanggal mulai pendidikan</label>
                        <div class="position-relative d-flex align-items-center">
                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                <span class="symbol-label bg-secondary">
                                    <span class="svg-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             width="24px" height="24px"
                                             viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                      rx="1"></rect>
                                                <path
                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                    fill="#000000"></path>
                                            </g>
                                        </svg>
                                    </span>
                                </span>
                            </div>
                            <input class="form-control ps-12 flatpickr-input active" id="tgl_mulai_pendidikan"
                                   placeholder="Pilih tanggal..." name="tgl_mulai_pendidikan" type="text" value=""
                                   required>
                        </div>
                    </div>
                </div>
                <div class="text-center mb-8">
                    <button type="button" class="btn btn-sm btn-secondary me-3" data-bs-dismiss="modal"><i
                            class="fa fa-reply"></i>Batal
                    </button>
                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="add_laporan_perkembangan_studi" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog mw-900px">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Form Laporan Perkembangan Studi</h3>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form id="formAddLps" class="form" method="post"
                  action="{{ url('tubel/administrasi/' . $tubel->id . '/lps') }}">
                @csrf
                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                        <div class="row mb-2">
                            <div class="col-md col-lg-5">
                                <div class="form-group row mb-2">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Nama</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span
                                            class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaPegawai }} </span>
                                    </div>
                                </div>
                                <div class="form-group row mb-2">
                                    <label class="col-lg-4 col-md fw-bold fs-6">NIP 18</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->nip18 }} </span>
                                    </div>
                                </div>
                                <div class="form-group row mb-2">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Pangkat</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span
                                            class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaPangkat }} </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md col-lg-7">
                                <div class="form-group row mb-2">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Jabatan</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span
                                            class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaJabatan }} </span>
                                    </div>
                                </div>
                                <div class="form-group row mb-2">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Unit Organisasi</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span
                                            class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaUnitOrg }} </span>
                                    </div>
                                </div>
                                <div class="form-group row mb-2">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Kantor</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span
                                            class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaKantor }} </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="rounded border border-gray-300 p-4 d-flex flex-column">
                        <div class="row mb-6">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Nama perguruan Tinggi</label>
                            <div class="col-lg">
                                <select class="form-select" data-control="select2" data-hide-search="true"
                                        name="perguruantinggi" required>
                                    <option selected disabled>Pilih Perguruan Tinggi</option>
                                    @foreach ($perguruantinggi as $pt)
                                        <option
                                            value="{{ $pt->id }}">{{ $pt->perguruanTinggiId->namaPerguruanTinggi }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Semester ke-</label>
                            <div class="col-lg">
                                <input type="text" class="form-control semester_mask" name="semesterke"
                                       placeholder="semester ke" required/>
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Semester</label>
                            <div class="col-lg">
                                <div class="form-check form-check-custom mb-2">
                                    <input class="form-check-input" type="radio" value="1" name="semester"
                                           id="flexRadioDefault" required/>
                                    <label class="form-check-label" for="flexRadioDefault">
                                        Ganjil
                                    </label>
                                </div>
                                <div class="form-check form-check-custom">
                                    <input class="form-check-input" type="radio" value="2" name="semester"
                                           id="flexRadioDefault2" required/>
                                    <label class="form-check-label" for="flexRadioDefault2">
                                        Genap
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tahun Akademik</label>
                            <div class="col-lg">
                                <input type="text" class="form-control tahun_mask" name="tahun" placeholder="tahun"
                                       required/>
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">IPK (skala 4)</label>
                            <div class="col-lg">
                                <input type="text" class="form-control ipk_mask" name="ipk" placeholder="ipk" required/>
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Jumlah SKS</label>
                            <div class="col-lg">
                                <input type="text" class="form-control" name="jumlah_sks" placeholder="jumlah sks"
                                       required/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center mb-8">
                    <button type="button" class="btn btn-sm btn-secondary me-3" data-bs-dismiss="modal"><i
                            class="fa fa-reply"></i>Batal
                    </button>
                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="upload_file_lps" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog mw-900px">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Attachment Laporan perkembangan Studi</h3>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body scroll-y px-5 px-lg-10">
                <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                    <form id="uploadkhs" method="put" action="" enctype="multipart/form-data">
                        {{ method_field('PUT') }}
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Unggah KHS</label>
                            <div class="col-lg">
                                <input id="uidlps" type="hidden" name="">
                                <input id="filekhs_lama" type="hidden" name="">
                                <input id="filekhs" class="form-control" type="file" name="filekhs">
                                <span id="messageuploadkhs" class="form-text text-muted" style="display: none">File berhasil diupload.</span>
                                <div id="messagefilekhs" class="col-lg" style="display: none">
                                    Download file KHS <a href="#" target="_blank" id="urlfilekhs">disini</a>.
                                </div>
                            </div>
                            <div class="form-label col-lg-2">
                                <button id="btnUploadKhs" type="submit" class="btn btn-primary fs-6">
                                    <i class="fa fa-upload"></i> Upload
                                </button>
                                <button class="btn btn-primary btn-sm fs-6" type="button" hidden disabled>
                                    Loading
                                    <span class="spinner-border spinner-border-sm" role="status"
                                          aria-hidden="true"></span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                    <div class="row mb-0">
                        <label class="col-lg-3"></label>
                        <div class="col-lg">
                            Silahkan download template laporan <a id="template_lps" href="">disini</a>.
                        </div>
                    </div>
                    <form id="uploadlps" method="put" action="" enctype="multipart/form-data">
                        {{ method_field('PUT') }}
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Unggah Laporan</label>
                            <div class="col-lg">
                                <input id="unggahidlps" type="hidden" name="">
                                <input id="filelps" class="form-control" type="file" name="filelps">
                                <input id="filelps_lama" type="hidden" name="">
                                <span id="messageuploadlps" class="form-text text-muted" style="display: none">File berhasil diupload.</span>
                                <div id="messagefilelps" class="col-lg" style="display: none">
                                    Download file LPS <a href="" target="_blank" id="urlfilelps">disini</a>.
                                </div>
                            </div>
                            <div class="form-label col-lg-2">
                                <button id="btnUploadLps" class="btn btn-primary">
                                    <i class="fa fa-upload"></i> Upload
                                </button>
                                <button class="btn btn-primary btn-sm fs-6" type="button" hidden disabled>
                                    Loading
                                    <span class="spinner-border spinner-border-sm" role="status"
                                          aria-hidden="true"></span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="lihat_laporan_perkembangan_studi" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog mw-900px">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Laporan Perkembangan Studi</h3>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form" method="post" action="{{ url('tubel/administrasi/' . $tubel->id . '/lps') }}">
                @csrf
                {{ method_field('post') }}
                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                        <div class="row mb-2">
                            <div class="col-md col-lg">
                                <div class="form-group row mb-2">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Nama</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span
                                            class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaPegawai }} </span>
                                    </div>
                                </div>
                                <div class="form-group row mb-2">
                                    <label class="col-lg-4 col-md fw-bold fs-6">NIP 18</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->nip18 }} </span>
                                    </div>
                                </div>
                                <div class="form-group row mb-2">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Pangkat</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span
                                            class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaPangkat }} </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md col-lg">
                                <div class="form-group row mb-2">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Jabatan</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span
                                            class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaJabatan }} </span>
                                    </div>
                                </div>
                                <div class="form-group row mb-2">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Unit Organisasi</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span
                                            class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaUnitOrg }} </span>
                                    </div>
                                </div>
                                <div class="form-group row mb-2">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Kantor</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span
                                            class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaKantor }} </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                        <div class="row mb-6">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Nama perguran Tinggi</label>
                            <div class="col-lg">
                                <input id="perguruantinggi" type="text" class="form-control form-control-solid"
                                       disabled/>
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Semester ke-</label>
                            <div class="col-lg">
                                <input id="semesterke" type="text" class="form-control form-control-solid" disabled/>
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Semester</label>
                            <div class="col-lg">
                                <input id="semester" type="text" class="form-control form-control-solid" disabled/>
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tahun Akademik</label>
                            <div class="col-lg">
                                <input id="tahun" type="text" class="form-control form-control-solid" disabled/>
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">IPK</label>
                            <div class="col-lg">
                                <input id="lihatipk" type="text" class="form-control form-control-solid" disabled/>
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Jumlah SKS</label>
                            <div class="col-lg">
                                <input id="jumlah_sks" type="text" class="form-control form-control-solid" disabled/>
                            </div>
                        </div>
                        <div class="row" id="fileLpsKhs">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                            <div class="col-lg">
                                <div class="col-sm">
                                    <div class="d-flex flex-aligns-center mt-2 pe-10">
                                        <img alt="" class="w-25px me-3"
                                             src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1">
                                            <a href="#" id="file_lps_lihat" target="_blank"
                                               class="fs-6 text-hover-primary">Laporan Perkembangan Studi.pdf</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm mt-4">
                                    <div class="d-flex flex-aligns-center mt-2 pe-10">
                                        <img alt="" class="w-25px me-3"
                                             src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1">
                                            <a href="#" id="file_khs_lihat" target="_blank"
                                               class="fs-6 text-hover-primary">Kartu Hasil Studi.pdf</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="edit_laporan_perkembangan_studi" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog mw-900px">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Form Update Laporan Perkembangan Studi</h3>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form id="formEditLps" class="form" method="post"
                  action="{{ env('APP_URL') }}/tubel/administrasi/{{ $tubel->id }}/lps">
                {{ method_field('PATCH') }}
                @csrf
                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                        <div class="row mb-2">
                            <div class="col-md col-lg-5">
                                <div class="form-group row mb-2">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Nama</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span
                                            class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaPegawai }} </span>
                                    </div>
                                </div>
                                <div class="form-group row mb-2">
                                    <label class="col-lg-4 col-md fw-bold fs-6">NIP 18</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->nip18 }} </span>
                                    </div>
                                </div>
                                <div class="form-group row mb-2">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Pangkat</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span
                                            class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaPangkat }} </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md col-lg">
                                <div class="form-group row mb-2">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Jabatan</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span
                                            class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaJabatan }} </span>
                                    </div>
                                </div>
                                <div class="form-group row mb-2">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Unit Organisasi</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span
                                            class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaUnitOrg }} </span>
                                    </div>
                                </div>
                                <div class="form-group row mb-2">
                                    <label class="col-lg-4 col-md fw-bold fs-6">Kantor</label>
                                    <div class="col-lg d-flex align-items-center">
                                        <span
                                            class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaKantor }} </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="rounded border border-gray-300 p-4 d-flex flex-column">
                        <div class="row mb-6">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Nama Perguruan Tinggi</label>
                            <div class="col-lg">
                                <input type="hidden" id="idLps" name="idLps">
                                <div class="col-lg">
                                    <select class="form-select" data-control="select2" data-hide-search="true"
                                            id="eperguruantinggi" name="perguruantinggi" required>
                                        @if ($perguruantinggi != null)
                                            @foreach ($perguruantinggi as $pt)
                                                <option class="optPt"
                                                        value="{{ $pt->id }}">{{ $pt->perguruanTinggiId->namaPerguruanTinggi }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Semester ke-</label>
                            <div class="col-lg">
                                <input id="esemesterke" name="semesterke" type="text"
                                       class="form-control semester_mask"/>
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Semester</label>
                            <div class="col-lg">
                                <div class="col-lg">
                                    <div class="form-check form-check-custom mb-2">
                                        <input class="form-check-input" type="radio" value="1" name="fg_semester"
                                               id="rGanjil"/>
                                        <label class="form-check-label" for="flexRadioDefault">
                                            Ganjil
                                        </label>
                                    </div>
                                    <div class="form-check form-check-custom">
                                        <input class="form-check-input" type="radio" value="2" name="fg_semester"
                                               id="rGenap"/>
                                        <label class="form-check-label" for="flexRadioDefault">
                                            Genap
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tahun Akademik</label>
                            <div class="col-lg">
                                <input type="text" id="etahun" name="tahun" class="form-control tahun_mask"/>
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">IPK</label>
                            <div class="col-lg">
                                <input type="text" id="eipk" name="ipk" class="form-control ipk_mask"/>
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Jumlah SKS</label>
                            <div class="col-lg">
                                <input id="ejumlah_sks" type="text" name="jumlah_sks" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center mb-8">
                    <button type="button" class="btn btn-sm btn-secondary me-3" data-bs-dismiss="modal"><i
                            class="fa fa-reply"></i>Batal
                    </button>
                    <button type="submit" class="btn btn-sm btn-success btnUpdateLps"
                            data-url="{{ env('APP_URL') . '/tubel/administrasi/' }}'"><i class="fa fa-save"></i> Simpan
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="add_cuti" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog mw-900px">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Form Permohonan Cuti Belajar</h3>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form" method="post" enctype="multipart/form-data"
                  action="{{ url('tubel/administrasi/cuti-belajar') }}">
                @csrf

                <input type="hidden" name="idTubel" value="{{ $tubel->id }}">

                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Mulai / selesai Cuti</label>
                        <div class="col-sm">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active datepick" id="tglMulai"
                                       placeholder="Pilih tanggal mulai" name="tglMulaiSt" type="text"
                                       value="{{ old('tglMulaiSt') }}" required>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active datepick" id="tglSelesai"
                                       placeholder="Pilih tanggal selesai" name="tglSelesaiSt" type="text"
                                       value="{{ old('tglSelesaiSt') }}" required>
                            </div>
                        </div>

                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Cuti</label>
                        <div class="col-lg">
                            <textarea class="form-control" type="text" name="alasan_cuti" rows="4"
                                      placeholder="alasan pengajuan cuti belajar" required></textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Kampus)</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" name="surat_izin_cuti" rows="4"
                                   placeholder="surat izin cuti" required>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                        <div class="col-lg">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active" id="tglSurat"
                                       placeholder="Pilih tanggal " name="tglSurat" type="text"
                                       value="{{ old('tglKepPembebasan') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Upload File</label>
                        <div class="col-lg">
                            <input class="form-control" type="file" name="file_cuti">
                        </div>
                    </div>
                    <br>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Sponsor)</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" name="surat_izin_cuti_sponsor"
                                   placeholder="surat izin cuti (sponsor)" required>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                        <div class="col-lg">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active" id="tglSuratSponsor"
                                       placeholder="Pilih tanggal " name="tglSuratSponsor" type="text"
                                       value="{{ old('tglKepPembebasan') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Upload File</label>
                        <div class="col-lg">
                            <input class="form-control" type="file" name="file_sponsors" required>
                        </div>
                    </div>
                </div>

                <div class="text-center mb-8">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                            class="fa fa-reply"></i>Batal
                    </button>

                    <button type="submit" id="btnSimpanCuti" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="edit_cuti" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog mw-900px">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Form Update Permohonan Cuti Belajar</h3>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form formEditCuti" method="post" enctype="multipart/form-data"
                  action="{{ url('tubel/administrasi/cuti-belajar') }}">
                {{ method_field('PATCH') }}
                @csrf

                <input type="hidden" name="idTubel" value="{{ $tubel->id }}">
                <input type="hidden" id="idCuti" name="id_cuti" value="">

                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Mulai / selesai Cuti</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active datepick" id="tglMulaiEdit"
                                           placeholder="Pilih tanggal mulai" name="tglMulaiSt" type="text"
                                           value="{{ old('tglMulaiSt') }}" required>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active datepick"
                                           id="tglSelesaiEdit" placeholder="Pilih tanggal selesai" name="tglSelesaiSt"
                                           type="text" value="{{ old('tglSelesaiSt') }}" required>
                                </div>
                            </div>

                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Cuti</label>
                            <div class="col-lg">
                                <textarea class="form-control" type="text" id="alasanCutiEdit" name="alasan_cuti"
                                          rows="4" placeholder="alasan pengajuan cuti belajar" required></textarea>
                            </div>
                        </div>
                        <br>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Kampus)</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="noSuratKamEdit" name="surat_izin_cuti"
                                       rows="4" placeholder="surat izin cuti" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active datepick"
                                           id="tglSuratKamEdit" placeholder="Pilih tanggal " name="tglSurat" type="text"
                                           value="{{ old('tglKepPembebasan') }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Upload File</label>
                            <div class="col-lg">
                                <input type="hidden" id="file_cuti_lama" name="file_cuti_lama">
                                <a href="{{ env('APP_URL' . '/files/') }}" target="_blank" id="file_cuti"
                                   class="form-control-plaintext"><i class="fa fa-file"></i> Surat Izin Cuti
                                    (Kampus).pdf</a>
                                <input class="form-control" type="file" name="file_cuti">
                            </div>
                        </div>
                        <br>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Sponsor)</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="noSuratSponsorEdit"
                                       name="surat_izin_cuti_sponsor" placeholder="surat izin cuti (sponsor)" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active datepick"
                                           id="tglSuratSponsorEdit" placeholder="Pilih tanggal " name="tglSuratSponsor"
                                           type="text" value="{{ old('tglKepPembebasan') }}"
                                           required>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Upload File</label>
                            <div class="col-lg">
                                <input type="hidden" id="file_sponsors_lama" name="file_sponsors_lama">
                                <a href="" target="_blank" id="file_sponsors" class="form-control-plaintext"><i
                                        class="fa fa-file"></i> Surat Izin Cuti (Sponsor).pdf</a>
                                <input class="form-control" type="file" name="file_sponsors">
                            </div>
                        </div>
                    </div>

                    <div class="text-center mb-8">
                        <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                class="fa fa-reply"></i>Batal
                        </button>
                        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="lihat_cuti" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog mw-900px">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Data Permohonan Cuti Belajar</h3>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form formEditCuti" method="post" action="{{ url('tubel/administrasi/updatecuti') }}">
                @csrf

                <input type="hidden" name="idTubel" value="{{ $tubel->id }}">
                <input type="hidden" id="idCuti" name="idCuti" value="">

                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Mulai / selesai Cuti</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active" id="tglMulaiLihat"
                                           placeholder="Pilih tanggal mulai" name="tglMulaiSt" type="text"
                                           value="{{ old('tglMulaiSt') }}" disabled>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active" id="tglSelesaiLihat"
                                           placeholder="Pilih tanggal selesai" name="tglSelesaiSt" type="text"
                                           value="{{ old('tglSelesaiSt') }}" disabled>
                                </div>
                            </div>

                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Cuti</label>
                            <div class="col-lg">
                                <textarea class="form-control" type="text" id="alasanCutiLihat" name="alasan_cuti"
                                          rows="4" placeholder="alasan pengajuan cuti belajar" disabled></textarea>
                            </div>
                        </div>
                        <br>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Kampus)</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="noSuratKamLihat" name="surat_izin_cuti"
                                       rows="4" placeholder="surat izin cuti" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active" id="tglSuratKamLihat"
                                           placeholder="Pilih tanggal " name="tglSurat" type="text"
                                           value="{{ old('tglKepPembebasan') }}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3 mt-2">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                            <div class="col-lg">
                                <div class="d-flex flex-aligns-center mt-2 pe-10">
                                    <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                    <div class="ms-1">
                                        <a href="#" id="filesuratkampuslihat" target="_blank"
                                           class="fs-6 text-hover-primary">Surat Izin Cuti (Kampus).pdf</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Sponsor)</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="noSuratSponsorLihat"
                                       name="surat_izin_cuti_sponsor" placeholder="surat izin cuti (sponsor)" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active" id="tglSuratSponsorLihat"
                                           placeholder="Pilih tanggal " name="tglSuratSponsor" type="text"
                                           value="{{ old('tglKepPembebasan') }}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                            <div class="col-lg">
                                <div class="d-flex flex-aligns-center mt-2 pe-10">
                                    <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                    <div class="ms-1">
                                        <a href="#" id="filesuratsponsorlihat" target="_blank"
                                           class="fs-6 text-hover-primary">Surat Izin Cuti (Sponsor).pdf</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="kep_penempatan" class="rounded border border-gray-300 p-4 d-flex flex-column mb-6"
                         style="display: none !important">
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Nomor KEP</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" name="nomor_kep_lihat" id="nomor_kep_lihat"
                                       placeholder="nomor kep penempatan" disabled>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal KEP</label>
                            <div class="col-lg-4">
                                <input class="form-control" id="tgl_kep_lihat" placeholder="Pilih tanggal"
                                       name="tgl_kep_lihat" type="text" value="" disabled>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">TMT Penempatan</label>
                            <div class="col-lg-4">
                                <input class="form-control" id="tmt_penempatan_lihat" placeholder="Pilih tanggal"
                                       name="tmt_penempatan_lihat" type="text" value="" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Lokasi Penempatan</label>
                            <div class="col-lg">
                                <input id="lokasi_penempatan_lihat" type="text" class="form-control col-sm-4"
                                       name="lokasi_penempatan_lihat" disabled>
                            </div>
                        </div>
                        <div class="row mb-3 mt-2">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                            <div class="col-lg">
                                <div class="d-flex flex-aligns-center mt-2 pe-10">
                                    <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                    <div class="ms-1">
                                        <a href="#" id="file_kep_lihat" target="_blank" class="fs-6 text-hover-primary">KEP
                                            Penempatan.pdf</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="add_permohonan_aktif" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog mw-900px">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Form Permohonan Aktif Kembali</h4>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form" id="formAddPengaktifan" method="post" enctype="multipart/form-data"
                  action="{{ url('tubel/administrasi/pengaktifan-kembali') }}">
                @csrf

                <input type="hidden" name="idTubel" value="{{ $tubel->id }}">

                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Mulai Aktif</label>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active" id="tglMulaiAktif"
                                       placeholder="Pilih tanggal " name="tglMulaiAktif" type="text"
                                       value="{{ old('tglKepPembebasan') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">No Surat Aktif Kembali</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="suratAktifKembali" name="suratAktifKembali"
                                   placeholder="nomor surat" required>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active" id="tglSuratPengaktifan"
                                       placeholder="Pilih tanggal " name="tglSuratPengaktifan" type="text"
                                       value="{{ old('tglKepPembebasan') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-10">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Upload File</label>
                        <div class="col-lg">
                            <input class="form-control" type="file" name="file_pengaktifan" required>
                        </div>
                    </div>
                    <div class="text-center mb-4">
                        <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                class="fa fa-reply"></i>Batal
                        </button>
                        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="edit_permohonan_aktif" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog mw-900px">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Form Update Permohonan Aktif Kembali</h4>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form" id="formEditPengaktifan" method="post" enctype="multipart/form-data"
                  action="{{ url('tubel/administrasi/pengaktifan-kembali') }}">
                {{ method_field('PATCH') }}
                @csrf
                <input type="hidden" name="idTubel" value="{{ $tubel->id }}">
                <input type="hidden" id="id_permohonan_aktif" name="id_permohonan_aktif">
                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Mulai Aktif</label>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active datepick" id="tglMulaiAktifEdit"
                                       placeholder="Pilih tanggal " name="tglMulaiAktif" type="text"
                                       value="{{ old('tglKepPembebasan') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">No Surat Aktif Kembali</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="suratAktifKembaliEdit"
                                   name="suratAktifKembaliEdit" placeholder="nomor surat" required>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active datepick"
                                       id="tglSuratPengaktifanEdit" placeholder="Pilih tanggal "
                                       name="tglSuratPengaktifanEdit" type="text" value="{{ old('tglKepPembebasan') }}"
                                       required>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3 mb-10">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Upload File</label>
                        <div class="col-lg">
                            <input type="hidden" id="file_pengaktifan_edit_lama" name="file_pengaktifan_edit_lama">
                            <a href="" target="_blank" id="file_pengaktifan_edit" class="form-control-plaintext"><i
                                    class="fa fa-file"></i> Dokumen.pdf</a>
                            <input class="form-control" type="file" id="file_pengaktifan_edit"
                                   name="file_pengaktifan_edit">
                        </div>
                    </div>
                    <div class="text-center mb-4">
                        <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                class="fa fa-reply"></i>Batal
                        </button>
                        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="lihat_permohonan_aktif" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog mw-900px">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Data Permohonan Aktif Kembali</h4>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body scroll-y px-5 px-lg-10">
                <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-4">
                    <div class="row mb-3 mt-2">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Mulai Aktif</label>
                        <div class="col-lg-8">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active" id="tglMulaiAktifLihat"
                                       placeholder="Pilih tanggal " name="tglMulaiAktif" type="text" value="" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">No Surat Aktif Kembali</label>
                        <div class="col-lg-8">
                            <input class="form-control" type="text" id="suratAktifKembaliLihat"
                                   name="suratAktifKembaliLihat" placeholder="nomor surat" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                        <div class="col-lg-8">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active" id="tglSuratPengaktifanLihat"
                                       placeholder="Pilih tanggal " name="tglSuratPengaktifan" type="text"
                                       value="{{ old('tglKepPembebasan') }}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                        <div class="col-lg">
                            <div class="d-flex flex-aligns-center mt-2 pe-10">
                                <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1">
                                    <a href="{{ env('APP_URL' . '/files/') }}" id="filePengaktifanLihat" target="_blank"
                                       class="fs-6 text-hover-primary">Permohonan Aktif Kembali.pdf</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="st_aktif_kembali" class="rounded border border-gray-300 p-4 d-flex flex-column mb-8"
                     style="display: none !important">
                    <div class="row mb-3 mt-2">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Nomor Surat Tugas</label>
                        <div class="col-lg-8">
                            <input class="form-control" type="text" name="nomor_surat_tugas_lihat"
                                   id="nomor_surat_tugas_lihat" placeholder="nomor surat tugas" disabled>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat Tugas</label>
                        <div class="col-lg-8">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input" id="tgl_surat_tugas_lihat"
                                       placeholder="Pilih tanggal" name="tgl_surat_tugas_lihat" type="text" value=""
                                       disabled>
                            </div>
                        </div>
                        <div class="col-lg"></div>
                    </div>
                    <div class="row">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Mulai / Selesai ST</label>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input" id="tgl_mulai_lihat"
                                       placeholder="Pilih tanggal" name="tgl_mulai_lihat" type="text" value="" disabled>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input" id="tgl_selesai_lihat"
                                       placeholder="Pilih tanggal" name="tgl_selesai_lihat" type="text" value=""
                                       disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                        <div class="col-lg">
                            <div class="d-flex flex-aligns-center mt-2 pe-10">
                                <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1">
                                    <a href="#" id="file_surat_tugas_lihat" target="_blank"
                                       class="fs-6 text-hover-primary">Surat Tugas.pdf</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="kep_pembebasan" class="rounded border border-gray-300 p-4 d-flex flex-column mb-8"
                     style="display: none !important">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Nomor KEP Pembebasan</label>
                        <div class="col-lg-8">
                            <input class="form-control" type="text" name="nomor_kep_pembebasan_lihat"
                                   id="nomor_kep_pembebasan_lihat" placeholder="nomor kep pembebasan" disabled>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal KEP Pembebasan</label>
                        <div class="col-lg-8">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input" id="tgl_kep_pembebasan_lihat"
                                       placeholder="Pilih tanggal" name="tgl_kep_pembebasan_lihat" type="text" value=""
                                       disabled>
                            </div>
                        </div>
                        <div class="col-lg"></div>
                    </div>
                    <div class="row">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">TMT</label>
                        <div class="col-lg-8    ">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input" id="tmt_lihat"
                                       placeholder="Pilih tanggal" name="tmt_lihat" type="text" value="" disabled>
                            </div>
                        </div>
                        <div class="col-lg"></div>
                    </div>
                    <div class="row mt-2">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                        <div class="col-lg">
                            <div class="d-flex flex-aligns-center mt-2 pe-10">
                                <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1">
                                    <a href="#" id="file_kep" target="_blank" class="fs-6 text-hover-primary">KEP
                                        Pembebasan.pdf</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add_perpanjangan_cuti_belajar" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog mw-900px">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Form Permohonan Perpanjangan Cuti Belajar</h4>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form" id="formAddCuti" method="post" enctype="multipart/form-data"
                  action="{{ url('tubel/administrasi/cuti-belajar-perpanjang') }}">
                @csrf

                <input type="hidden" name="idTubel" value="{{ $tubel->id }}">
                <input type="hidden" name="idCuti" id="id_cuti_perpanjang">

                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Mulai / selesai Cuti</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active datepick"
                                           id="tgl_mulai_perpanjang" placeholder="Pilih tanggal mulai"
                                           name="tgl_mulai_perpanjang" type="text" value="" required>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active datepick"
                                           id="tgl_selesai_perpanjang" placeholder="Pilih tanggal selesai"
                                           name="tgl_selesai_perpanjang" type="text"
                                           value="{{ old('tglSelesaiSt') }}" required>
                                </div>
                            </div>

                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Cuti</label>
                            <div class="col-lg">
                                <textarea class="form-control" type="text" name="alasan_cuti_perpanjang" rows="4"
                                          placeholder="alasan pengajuan cuti belajar" required></textarea>
                            </div>
                        </div>
                        <br>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Kampus)</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" name="surat_izin_cuti_perpanjang" rows="4"
                                       placeholder="surat izin cuti" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active datepick"
                                           id="tgl_surat_perpanjang" placeholder="Pilih tanggal "
                                           name="tgl_surat_perpanjang" type="text" value="{{ old('tglKepPembebasan') }}"
                                           required>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Upload File</label>
                            <div class="col-lg">
                                <input class="form-control" type="file" name="file_cuti_perpanjang">
                            </div>
                        </div>
                        <br>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Sponsor)</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" name="surat_izin_cuti_sponsor_perpanjang"
                                       placeholder="surat izin cuti (sponsor)" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active datepick"
                                           id="tgl_surat_sponsor_perpanjang" placeholder="Pilih tanggal "
                                           name="tgl_surat_sponsor_perpanjang" type="text"
                                           value="{{ old('tglKepPembebasan') }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Upload File</label>
                            <div class="col-lg">
                                <input class="form-control" type="file" name="file_sponsors_perpanjang" required>
                            </div>
                        </div>
                    </div>

                    <div class="text-center mb-8">
                        <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                class="fa fa-reply"></i>Batal
                        </button>
                        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="add_perpanjangan_st" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog mw-900px">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Form Perpanjangan ST Tubel</h4>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form" method="post" enctype="multipart/form-data"
                  action="{{ url('tubel/administrasi/perpanjangan-st-tubel') }}">
                @csrf

                <input type="hidden" name="idTubel" value="{{ $tubel->id }}">

                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Perpanjangan</label>
                        <div class="col-lg">
                            <textarea class="form-control" type="text" id="alasan_perpanjangan_st_add"
                                      name="alasan_perpanjangan_st" rows="4" placeholder="alasan perpanjangan"
                                      required></textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tahapan Studi</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="tahapan_studi_add" name="tahapan_studi_add"
                                   rows="4" placeholder="tahapan studi" required>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Selesai Perpanjangan</label>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active datepick"
                                       id="tgl_selesai_perpanjangan_add" placeholder="Pilih tanggal"
                                       name="tgl_selesai_perpanjangan_add" type="text" value="" required>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">ST Awal</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="st_awal_add" name="st_awal_add"
                                   placeholder="surat tugas awal" required>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Upload File</label>
                        <div class="col-lg">
                            <input class="form-control" type="file" name="file_st_awal_add">
                        </div>
                    </div>
                    <br>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Persetujuan Kampus</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="surat_persetujuan_kampus_add"
                                   name="surat_persetujuan_kampus_add" placeholder="nomor surat persetujuan Kampus"
                                   required>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal</label>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active datepick"
                                       id="tgl_surat_persetujuan_kampus_add" placeholder="Pilih tanggal"
                                       name="tgl_surat_persetujuan_kampus_add" type="text" value="" required>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Upload File</label>
                        <div class="col-lg">
                            <input class="form-control" type="file" name="file_surat_persetujuan_kampus_add">
                        </div>
                    </div>
                    <br>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Persetujuan Sponsor</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="surat_persetujuan_sponsor_add"
                                   name="surat_persetujuan_sponsor_add" placeholder="nomor surat persetujuan sponsor"
                                   required>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal</label>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active datepick"
                                       id="tgl_surat_persetujuan_sponsor_add" placeholder="Pilih tanggal"
                                       name="tgl_surat_persetujuan_sponsor_add" type="text" value="" required>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Upload File</label>
                        <div class="col-lg">
                            <input class="form-control" type="file" name="file_surat_persetujuan_sponsor_add">
                        </div>
                    </div>
                </div>

                <div class="text-center mb-8">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                            class="fa fa-reply"></i>Batal
                    </button>
                    <button type="submit" id="btnSimpanCuti" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="edit_perpanjangan_st" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog mw-900px">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Form Update Perpanjangan ST Tubel</h4>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form" method="post" enctype="multipart/form-data"
                  action="{{ url('tubel/administrasi/perpanjangan-st-tubel') }}">
                {{ method_field('PATCH') }}
                @csrf

                <input type="hidden" id="id_tubel" name="id_tubel" value="{{ $tubel->id }}">
                <input type="hidden" id="id_perpanjangan_st" name="id_perpanjangan_st">

                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Perpanjangan</label>
                        <div class="col-lg">
                            <textarea class="form-control" type="text" id="alasan_perpanjangan_st_edit"
                                      name="alasan_perpanjangan_st_edit" rows="4" placeholder="alasan perpanjangan"
                                      required></textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tahapan Studi</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="tahapan_studi_edit" name="tahapan_studi_edit"
                                   rows="4" placeholder="tahapan studi" required>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Selesai Perpanjangan</label>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active datepick"
                                       id="tgl_selesai_perpanjangan_edit" placeholder="Pilih tanggal"
                                       name="tgl_selesai_perpanjangan_edit" type="text" value="" required>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">ST Awal</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="st_awal_edit" name="st_awal_edit"
                                   placeholder="surat tugas awal" required>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Upload File</label>
                        <div class="col-lg">
                            <input class="form-control" type="file" name="file_st_awal_edit">
                            <input type="hidden" id="file_st_awal_lama" name="file_st_awal_lama">
                            <a href="{{ env('APP_URL' . '/files/') }}" target="_blank" id="file_st_awal_edit"
                               class="form-control-plaintext"><i class="fa fa-file"></i> ST Awal.pdf</a>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Persetujuan Kampus</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="surat_persetujuan_kampus_edit"
                                   name="surat_persetujuan_kampus_edit" placeholder="nomor surat persetujuan Kampus"
                                   required>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal</label>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active datepick"
                                       id="tgl_surat_persetujuan_kampus_edit" placeholder="Pilih tanggal"
                                       name="tgl_surat_persetujuan_kampus_edit" type="text" value="" required>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Upload File</label>
                        <div class="col-lg">
                            <input class="form-control" type="file" name="file_surat_persetujuan_kampus_edit">
                            <input type="hidden" id="file_surat_persetujuan_kampus_lama"
                                   name="file_surat_persetujuan_kampus_lama">
                            <a href="{{ env('APP_URL' . '/files/') }}" target="_blank"
                               id="file_surat_persetujuan_kampus_edit" class="form-control-plaintext"><i
                                    class="fa fa-file"></i> Surat Persetujuan Kampus.pdf</a>
                        </div>
                    </div>
                    {{-- <br> --}}
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Persetujuan Sponsor</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="surat_persetujuan_sponsor_edit"
                                   name="surat_persetujuan_sponsor_edit" placeholder="nomor surat persetujuan sponsor"
                                   required>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal</label>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active datepick"
                                       id="tgl_surat_persetujuan_sponsor_edit" placeholder="Pilih tanggal"
                                       name="tgl_surat_persetujuan_sponsor_edit" type="text" value="" required>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Upload File</label>
                        <div class="col-lg">
                            <input class="form-control" type="file" name="file_surat_persetujuan_sponsor_edit">
                            <input type="hidden" id="file_surat_persetujuan_sponsor_lama"
                                   name="file_surat_persetujuan_sponsor_lama">
                            <a href="{{ env('APP_URL' . '/files/') }}" target="_blank"
                               id="file_surat_persetujuan_sponsor_edit" class="form-control-plaintext"><i
                                    class="fa fa-file"></i> Surat Persetujuan Sponsor.pdf</a>
                        </div>
                    </div>
                </div>

                <div class="text-center mb-8">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                            class="fa fa-reply"></i>Batal
                    </button>

                    <button type="submit" id="btnSimpanCuti" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="lihat_perpanjangan_st" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog mw-1000px">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Data Perpanjangan ST Tubel</h4>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body scroll-y px-5 px-lg-10">
                <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Perpanjangan</label>
                        <div class="col-lg">
                            <textarea class="form-control" type="text" id="alasan_perpanjangan_st_lihat"
                                      name="alasan_perpanjangan_st_lihat" rows="4" placeholder="alasan perpanjangan"
                                      disabled></textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tahapan Studi</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="tahapan_studi_lihat" name="tahapan_studi_lihat"
                                   rows="4" placeholder="tahapan studi" disabled>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Selesai Perpanjangan</label>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active"
                                       id="tgl_selesai_perpanjangan_lihat" placeholder="Pilih tanggal"
                                       name="tgl_selesai_perpanjangan_lihat" type="text" value="" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">ST Awal</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="st_awal_lihat" name="st_awal_lihat"
                                   placeholder="surat tugas awal" disabled>
                        </div>
                    </div>
                    <div class="row mb-3 mt-2">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                        <div class="col-lg">
                            <div class="d-flex flex-aligns-center mt-2 pe-10">
                                <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1">
                                    <a href="{{ env('APP_URL' . '/files/') }}" id="file_st_awal_lihat" target="_blank"
                                       class="fs-6 text-hover-primary">ST Awal.pdf</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Persetujuan Kampus</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="surat_persetujuan_kampus_lihat"
                                   name="surat_persetujuan_kampus_lihat" placeholder="nomor surat persetujuan Kampus"
                                   disabled>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal</label>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active"
                                       id="tgl_surat_persetujuan_kampus_lihat" placeholder="Pilih tanggal"
                                       name="tgl_surat_persetujuan_kampus_lihat" type="text" value="" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-4 mt-2">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                        <div class="col-lg">
                            <div class="d-flex flex-aligns-center mt-2 pe-10">
                                <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1">
                                    <a href="{{ env('APP_URL' . '/files/') }}" id="file_surat_persetujuan_kampus_lihat"
                                       target="_blank" class="fs-6 text-hover-primary">Surat Persetujuan Kampus.pdf</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Persetujuan Sponsor</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="surat_persetujuan_sponsor_lihat"
                                   name="surat_persetujuan_sponsor_lihat" placeholder="nomor surat persetujuan sponsor"
                                   disabled>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal</label>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active"
                                       id="tgl_surat_persetujuan_sponsor_lihat" placeholder="Pilih tanggal"
                                       name="tgl_surat_persetujuan_sponsor_lihat" type="text" value="" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3 mt-2">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                        <div class="col-lg">
                            <div class="d-flex flex-aligns-center mt-2 pe-10">
                                <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1">
                                    <a href="{{ env('APP_URL' . '/files/') }}" id="file_surat_persetujuan_sponsor_lihat"
                                       target="_blank" class="fs-6 text-hover-primary">Surat Persetujuan Sponsor.pdf</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="upload_st_perpanjangan" class="rounded border border-gray-300 p-4 d-flex flex-column"
                     style="display: none !important">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Nomor ST</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="nomor_st_lihat_st" name="nomor_st_lihat_st"
                                   disabled>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal ST</label>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active" id="tgl_lihat_st"
                                       name="tgl_lihat_st" type="text" value="" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Mulai / Selesai ST</label>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active" id="tgl_mulai_lihat_st"
                                       name="tgl_mulai_lihat_st" type="text" value="" disabled>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active" id="tgl_selesai_lihat_st"
                                       name="tgl_selesai_lihat_st" type="text" value="" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3 mb-10 mt-2">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                        <div class="col-lg">
                            <div class="d-flex flex-aligns-center mt-2 pe-10">
                                <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1">
                                    <a href="{{ env('APP_URL' . '/files/') }}" id="file_lihat_st" target="_blank"
                                       class="fs-6 text-hover-primary">Surat Tugas.pdf</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="lihat_perpanjangan_st_stan" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog mw-900px">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Data Perpanjangan ST STAN</h4>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body scroll-y px-5 px-lg-10">
                <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Perpanjangan</label>
                        <div class="col-lg-8">
                            <textarea class="form-control" type="text" id="alasan_perpanjangan_st_stan"
                                      name="alasan_perpanjangan_st_stan" rows="4" placeholder="alasan perpanjangan"
                                      disabled></textarea>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Selesai Perpanjangan</label>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active"
                                       id="tgl_selesai_perpanjangan_stan" placeholder="Pilih tanggal"
                                       name="tgl_selesai_perpanjangan_stan" type="text" value="" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">ST Awal</label>
                        <div class="col-lg-8">
                            <input class="form-control" type="text" id="st_awal_stan" name="st_awal_stan"
                                   placeholder="surat tugas awal" disabled>
                        </div>
                    </div>
                    <div class="row mb-3 mt-2">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                        <div class="col-lg-8">
                            <div class="d-flex flex-aligns-center mt-2 pe-10">
                                <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1">
                                    <a href="{{ env('APP_URL' . '/files/') }}" id="file_st_awal_stan" target="_blank"
                                       class="fs-6 text-hover-primary">ST Awal.pdf</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Keterangan</label>
                        <div class="col-lg-8">
                            <input class="form-control" type="text" id="keterangan" name="keterangan"
                                   placeholder="surat tugas awal" disabled>
                        </div>
                    </div>
                    <div class="row mb-3 mt-2">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                        <div class="col-lg-8">
                            <div class="d-flex flex-aligns-center mt-2 pe-10">
                                <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1">
                                    <a href="{{ env('APP_URL' . '/files/') }}" id="dokumen_pendukung" target="_blank"
                                       class="fs-6 text-hover-primary">Dokumen.pdf</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="data_st_stan" class="rounded border border-gray-300 p-4 d-flex flex-column"
                     style="display: none !important">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Nomor Surat Tugas</label>
                        <div class="col-lg-8">
                            <input class="form-control" type="text" id="nomor_st_stan" name="nomor_st_stan"
                                   placeholder="-" disabled>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat Tugas</label>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active" id="tgl_st_stan"
                                       placeholder="-" name="tgl_st_stan" type="text" value="" disabled>
                            </div>
                        </div>
                        <div class="col-lg"></div>
                    </div>
                    <div class="row">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Mulai / Selesai</label>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active" id="tgl_mulai_st_stan"
                                       placeholder="-" name="tgl_mulai_st_stan" type="text" value="" disabled>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"></rect>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control ps-12 flatpickr-input active" id="tgl_selesai_st_stan"
                                       placeholder="-" name="tgl_selesai_st_stan" type="text" value="" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-8 mt-2">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen</label>
                        <div class="col-lg">
                            <div class="d-flex flex-aligns-center mt-2 pe-10">
                                <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1">
                                    <a href="{{ env('APP_URL' . '/files/') }}" id="file_st_stan" target="_blank"
                                       class="fs-6 text-hover-primary">Dokumen.pdf</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add_usulan_riset" aria-hidden="true">
    <div class="modal-dialog mw-900px">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Usulan Topik Riset</h4>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form" method="post" enctype="multipart/form-data"
                  action="{{ url('tubel/administrasi/usulan-topik-riset') }}">
                @csrf

                <input type="hidden" name="id_tubel" value="{{ $tubel->id }}">

                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Pilihan Topik</label>
                        <div class="col-lg">
                            <select id="pilihan_topik_add" name="topik_riset_add" class="form-select form-select-lg"
                                    required>
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Judul Riset</label>
                        <div class="col-lg">
                            <textarea class="form-control" type="text" name="judul_riset_add" rows="4"
                                      placeholder="Judul riset" required></textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen Proposal</label>
                        <div class="col-lg">
                            <input class="form-control" type="file" name="file_dokumen_proposal_add" required>
                        </div>
                    </div>
                </div>

                <div class="text-center mb-8">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                            class="fa fa-reply"></i>Batal
                    </button>
                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="persetujuan_usulan_riset" aria-hidden="true">
    <div class="modal-dialog mw-900px">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Form Persetujuan Perguruan Tinggi / Dosen</h4>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form id="formPersetujuanUsulanRiset" class="form" method="post" enctype="multipart/form-data"
                  action="{{ url('tubel/administrasi/usulan-topik-riset/persetujuan') }}">
                @csrf

                <input type="hidden" id="id_usulan_topik_riset_setuju" name="id_usulan_topik_riset_setuju">

                <div class="modal-body scroll-y px-5 px-lg-10">
                    {{-- <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6"> --}}
                    <div class="row mb-6">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Setuju / Menolak</label>
                        <div class="col-lg mt-3">
                            <div class="form-check form-check-custom mb-3">
                                <input class="form-check-input" type="radio" value="1" name="persetujuan"
                                       id="rdSetujuUsulanRiset" required/>
                                <label class="form-check-label" for="rdSetujuUsulanRiset">
                                    Setuju
                                </label>
                            </div>
                            <div class="form-check form-check-custom">
                                <input class="form-check-input" type="radio" value="0" name="persetujuan"
                                       id="rdTolakUsulanRiset" required/>
                                <label class="form-check-label" for="rdTolakUsulanRiset">
                                    Menolak
                                </label>
                            </div>
                        </div>
                    </div>
                    {{-- </div> --}}
                    {{-- <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6" hidden> --}}
                    {{-- <div id="btnSetujuUsulanRisetClick" class="rounded border border-gray-300 p-4 d-flex flex-column mb-6"> --}}
                    <div id="btnSetujuUsulanRisetClick">
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Persetujuan</label>
                            <div class="col-lg-4">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active datepick"
                                           placeholder="Pilih tanggal" name="tgl_persetujuan_usulan_riset" type="text"
                                           value="">
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen Persetujuan</label>
                            <div class="col-lg">
                                <input class="form-control" type="file" name="file_dokumen_persetujuan_usulan_riset">
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3" id="btnTolakUsulanRisetClick">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Penolakan</label>
                        <div class="col-lg">
                            <textarea class="form-control" type="text" name="alasan_penolakan_usulan_riset" rows="5"
                                      placeholder="alasan penolakan"></textarea>
                        </div>
                    </div>
                </div>
                {{-- </div> --}}

                <div id="btnActionUsulanRiset" class="text-center mb-8">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                            class="fa fa-reply"></i>Batal
                    </button>
                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="edit_usulan_riset" aria-hidden="true">
    <div class="modal-dialog mw-900px">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Usulan Topik Riset</h4>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form" method="post" enctype="multipart/form-data"
                  action="{{ url('tubel/administrasi/usulan-topik-riset') }}">
                {{ method_field('patch') }}
                @csrf

                <input type="hidden" name="idTubel" value="{{ $tubel->id }}">
                <input type="hidden" id="id_usulan_riset_edit" name="id_usulan_riset_edit">

                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Pilihan Topik</label>
                        <div class="col-lg">
                            <select id="pilihan_topik_edit" name="pilihan_topik_edit" data-control="select2" type="text"
                                    class="form-select form-select-lg pilihan_topik_riset" required>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Judul Riset</label>
                        <div class="col-lg">
                            <textarea class="form-control" type="text" id="judul_riset_edit" name="judul_riset_edit"
                                      rows="4" placeholder="Judul riset" required></textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen Proposal</label>
                        <div class="col-lg">
                            <input type="hidden" id="file_dokumen_proposal_lama" name="file_dokumen_proposal_lama">
                            <a href="" target="_blank" id="file_dokumen_proposal" class="form-control-plaintext"><i
                                    class="fa fa-file"></i> Proposal.pdf</a>
                            <input class="form-control" type="file" name="file_dokumen_proposal_edit">
                        </div>
                    </div>
                </div>

                <div class="text-center mb-8">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                            class="fa fa-reply"></i>Batal
                    </button>
                    <button type="submit" id="btnSimpanCuti" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="lihat_usulan_riset" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h4 class="text-white">Form Pengesahan Usulan Topik Riset</h4>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <input id="id_usulan_topik_riset" type="hidden">
            <div class="modal-body scroll-y">
                <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                    <div class="row mb-2">
                        <div class="col-md col-lg">
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Nama</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="nama_lihat" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">NIP 18</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="nip_lihat" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Pangkat</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="pangkat_lihat" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row mb-6">
                                <label class="col-lg-4 col-md fw-bold fs-6">Jabatan</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="jabatan_lihat" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Jenjang Pendidikan</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="jenjang_lihat" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Program Beasiswa</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="program_beasiswa_lihat"
                                          class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Lokasi Pendidikan</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="lokasi_lihat" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            {{-- <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Perguruan Tinggi</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="pt_lihat" class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Program Studi</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span id="prodi_lihat" class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                        </div>
                                    </div> --}}
                        </div>
                        <div class="col-md col-lg">
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">ST Tubel</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="st_tubel_lihat" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Tgl ST</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="tgl_st_tubel_lihat" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Tgl Mulai & Selesai</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="tgl_mulai_selesai_lihat"
                                          class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">KEP Pembebasan</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="kep_pembebasan_lihat" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Tgl KEP</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span class="fw-bolder fs-6 text-gray-600 me-2 tgl_kep_pembebasan_lihat">: </span>
                                </div>
                            </div>
                            <div class="form-group row mb-6">
                                <label class="col-lg-4 col-md fw-bold fs-6">TMT KEP</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="tmt_kep_pembebasan_lihat"
                                          class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Email</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="email_lihat" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Nomor HP</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="hp_lihat" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-md fw-bold fs-6">Alamat</label>
                                <div class="col-lg d-flex align-items-center">
                                    <span id="alamat_lihat" class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Topik Riset</label>
                        <div class="col-lg">
                            <textarea class="form-control" id="topik_riset_lihat" type="text" rows="4"
                                      disabled></textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Judul Riset</label>
                        <div class="col-lg">
                            <textarea class="form-control" id="judul_riset_lihat" type="text" rows="4"
                                      disabled></textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Proposal</label>
                        <div class="col-lg">
                            <div class="d-flex flex-aligns-center mt-2 pe-10">
                                <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1">
                                    <a href="{{ env('APP_URL' . '/files/') }}" id="dokumen_proposal_lihat"
                                       target="_blank" class="fs-6 text-hover-primary">Proposal.pdf</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="topik_riset_disahkan">
                    <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Persetujuan PT / Dosen</label>
                            <div class="col-lg-4">
                                <input class="form-control" id="tgl_persetujuan_lihat" type="text" disabled>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen Persetujuan</label>
                            <div class="col-lg">
                                <div class="d-flex flex-aligns-center mt-2 pe-10">
                                    <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                    <div class="ms-1">
                                        <a href="{{ env('APP_URL' . '/files/') }}" id="dokumen_persetujuan_lihat"
                                           target="_blank" class="fs-6 text-hover-primary">Persetujuan.pdf</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="riset_disetujui">
                    <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Nomor ND Persetujuan</label>
                            <div class="col-lg-4">
                                <input class="form-control" type="text" id="nomor_nd_persetujuan_lihat" disabled>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal ND Persetujuan</label>
                            <div class="col-lg-4">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                              height="4" rx="1"></rect>
                                                        <path
                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                            fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12" id="tgl_nd_persetujuan_lihat" type="text"
                                           disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Dokumen ND Persetujuan</label>
                            <div class="col-lg">
                                <div class="d-flex flex-aligns-center mt-2 pe-10">
                                    <img alt="" class="w-25px me-3" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                    <div class="ms-1">
                                        <a href="{{ env('APP_URL' . '/files/') }}" id="dokumen_persetujuan_nd_lihat"
                                           target="_blank" class="fs-6 text-hover-primary">ND Persetujuan.pdf</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if ($tab == 'usulanriset')

    <div class="modal fade" id="add_laporan_hasil_studi" aria-hidden="true">
        <div class="modal-dialog mw-1000px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <div class="modal-title">
                        <h3 class="text-white">Form Laporan Berakhir Masa Studi</h4>
                    </div>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg"
                                 width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                                   fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                    <rect fill="#000000" opacity="0.5"
                                          transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                          x="0" y="7" width="16" height="2" rx="1"/>
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Hasil Studi</label>
                        <div class="col-lg">
                            <select class="form-select opsi_hasil_studi" data-control="select2" data-hide-search="true"
                                    data-placeholder="Pilih hasil studi" name="hasil_studi" required>
                                <option></option>
                                @foreach ($ref_status_lapor_diri as $rs)
                                    <option value="{{ $rs->id }}">{{ $rs->status }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form_opsi form_hasil_studi_lulus" hidden>
                        <form action="{{ url('tubel/administrasi/selesai-studi/lulus') }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id_tubel" value="{{ $tubel->id }}">
                            <div class="fw-bolder mb-4 fs-3 mt-8">
                                <span class="d-inline-block position-relative">
                                    <h1>Lulus</h1>
                                    <span
                                        class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-success translate rounded"></span>
                                </span>
                            </div>
                            <div class="rounded border border-gray-300 p-5 d-flex flex-column mb-4">
                                <div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Kelulusan</label>
                                        <div class="col-lg-4">
                                            <div class="position-relative d-flex align-items-center">
                                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                    <span class="symbol-label bg-secondary">
                                                        <span class="svg-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="24px"
                                                                 height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                          width="4" height="4" rx="1"></rect>
                                                                    <path
                                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                        fill="#000000"></path>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    </span>
                                                </div>
                                                <input class="form-control ps-12 datepick" id="tgl_kelulusan"
                                                       name="tgl_kelulusan" placeholder="Pilih tanggal kelulusan"
                                                       type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Tipe Dokumen
                                            Penelitian</label>
                                        <div class="col-lg-4">
                                            <select class="form-select" data-control="select2" data-hide-search="true"
                                                    data-placeholder="Pilih dokumen penelitian"
                                                    name="opsi_dokumen_penelitian" required>
                                                <option></option>
                                                @foreach ($ref_dokumen_penelitian as $rd)
                                                    <option value="{{ $rd->id }}">{{ $rd->tipeDokumen }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Penelitian</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="file" name="file_penelitian" required>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Laporan Selesai
                                            Studi</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="file" name="file_lps" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @foreach ($perguruantinggi as $pt)
                                <div class="rounded border border-gray-300 p-5 d-flex flex-column mb-4">
                                    <h2 class="text-dark font-weight-bolder mb-4">{{ $pt->perguruanTinggiId->namaPerguruanTinggi }}</h2>
                                    <div class="row form-group mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">IPK (skala 4)</label>
                                        <div class="col-lg-4">
                                            <input class="form-control ipk_mask" type="text"
                                                   name="pt[{{ $pt->id }}][ipk]" placeholder="ipk">
                                        </div>
                                    </div>
                                    <div class="row mb-3 mt-2">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor SKL</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="text" name="pt[{{ $pt->id }}][nomor_skl]"
                                                   placeholder="nomor skl">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal SKL</label>
                                        <div class="col-lg-4">
                                            <div class="position-relative d-flex align-items-center">
                                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                    <span class="symbol-label bg-secondary">
                                                        <span class="svg-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="24px"
                                                                 height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                          width="4" height="4" rx="1"></rect>
                                                                    <path
                                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                        fill="#000000"></path>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    </span>
                                                </div>
                                                <input class="form-control ps-12 datepick"
                                                       name="pt[{{ $pt->id }}][tgl_skl]" placeholder="Pilih tanggal skl"
                                                       type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">File SKL</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="file" name="pt[{{ $pt->id }}][file_skl]">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Ijazah</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="text"
                                                   name="pt[{{ $pt->id }}][nomor_ijazah]" placeholder="nomor ijazah">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Ijazah</label>
                                        <div class="col-lg-4">
                                            <div class="position-relative d-flex align-items-center">
                                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                    <span class="symbol-label bg-secondary">
                                                        <span class="svg-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="24px"
                                                                 height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                          width="4" height="4" rx="1"></rect>
                                                                    <path
                                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                        fill="#000000"></path>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    </span>
                                                </div>
                                                <input class="form-control ps-12 datepick"
                                                       name="pt[{{ $pt->id }}][tgl_ijazah]"
                                                       placeholder="Pilih tanggal ijazah" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">File Ijazah</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="file"
                                                   name="pt[{{ $pt->id }}][file_ijazah]">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Transkrip</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="text"
                                                   name="pt[{{ $pt->id }}][nomor_transkrip]"
                                                   placeholder="nomor transkrip">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Transkrip</label>
                                        <div class="col-lg-4">
                                            <div class="position-relative d-flex align-items-center">
                                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                    <span class="symbol-label bg-secondary">
                                                        <span class="svg-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="24px"
                                                                 height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                          width="4" height="4" rx="1"></rect>
                                                                    <path
                                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                        fill="#000000"></path>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    </span>
                                                </div>
                                                <input class="form-control ps-12 datepick"
                                                       name="pt[{{ $pt->id }}][tgl_transkrip]"
                                                       placeholder="Pilih tanggal transkrip" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">File Transkrip</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="file"
                                                   name="pt[{{ $pt->id }}][file_transkrip]">
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="text-center mb-2 mt-10">
                                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                        class="fa fa-reply"></i>Batal
                                </button>
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="form_opsi form_hasil_studi_belum_lulus" hidden>
                        <form action="{{ url('tubel/administrasi/selesai-studi/belum-lulus') }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="tipe" value="belum_lulus">
                            <input type="hidden" name="id_tubel" value="{{ $tubel->id }}">
                            <div class="fw-bolder mb-4 fs-3 mt-8">
                                <span class="d-inline-block position-relative">
                                    <h1>Belum Lulus</h1>
                                    <span
                                        class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-warning translate rounded"></span>
                                </span>
                            </div>
                            <div class="rounded border border-gray-300 p-4 d-flex flex-column mt-6">
                                <div class="row mb-3 mt-4">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tahapan Studi</label>
                                    <div class="col-lg">
                                        <select class="form-select" data-control="select2" data-hide-search="true"
                                                data-placeholder="Pilih dokumen penelitian" name="opsi_tahapan_studi"
                                                required>
                                            <option></option>
                                            @foreach ($ref_tahap_belum_lulus as $bl)
                                                <option value="{{ $bl->id }}">{{ $bl->tahap }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Timeline Rencana
                                        Penyelesaian Studi</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="file" name="file_timeline_studi_add" required>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tipe Dokumen Penelitian</label>
                                    <div class="col-lg">
                                        <select class="form-select" data-control="select2" data-hide-search="true"
                                                data-placeholder="Pilih dokumen penelitian"
                                                name="opsi_dokumen_penelitian" required>
                                            <option></option>
                                            @foreach ($ref_dokumen_penelitian as $rd)
                                                <option value="{{ $rd->id }}">{{ $rd->tipeDokumen }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Draft Dokumen Penelitian</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="file" name="file_draft_dokumen_penelitian_add"
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center mb-2 mt-10">
                                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                        class="fa fa-reply"></i>Batal
                                </button>
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="form_opsi form_hasil_studi_drop_out" hidden>
                        <form action="{{ url('tubel/administrasi/selesai-studi/drop-out') }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id_tubel" value="{{ $tubel->id }}">
                            <div class="fw-bolder mb-4 fs-3 mt-8">
                                <span class="d-inline-block position-relative">
                                    <h1>Drop Out</h1>
                                    <span
                                        class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-danger translate rounded"></span>
                                </span>
                            </div>
                            <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6 mt-6">
                                <div class="row mb-3 mt-4">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">DO Pada Semester</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control semester_mask" name="semester_do"
                                               placeholder="semester" required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tahun Akademik</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control tahun_mask" name="tahun"
                                               placeholder="tahun" required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">No Surat Keterangan</label>
                                    <div class="col-lg">
                                        <input type="text" class="form-control" name="no_surat_keterangan"
                                               placeholder="nomor surat" required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Surat Keterangan</label>
                                    <div class="col-lg-4">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                             width="24px"
                                                             height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none"
                                                               fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                      height="4" rx="1"></rect>
                                                                <path
                                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                    fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 datepick" name="tgl_surat_keterangan"
                                                   placeholder="Pilih tanggal surat" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Unggah Dokumen</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="file" name="file_surat_keterangan" required>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center mb-2 mt-10">
                                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                        class="fa fa-reply"></i>Batal
                                </button>
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="form_opsi form_hasil_studi_mengundurkan_diri" hidden>
                        <form action="{{ url('tubel/administrasi/selesai-studi/mengundurkan-diri') }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id_tubel" value="{{ $tubel->id }}">
                            <div class="fw-bolder mb-4 fs-3 mt-8">
                                <span class="d-inline-block position-relative">
                                    <h1>Mengundurkan Diri</h1>
                                    <span
                                        class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-danger translate rounded"></span>
                                </span>
                            </div>
                            <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6 mt-6">
                                <div class="row mb-3 mt-4">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Alasan Pengunduran diri</label>
                                    <div class="col-lg">
                                        <textarea class="form-control" type="text" name="alasan_pengunduran_diri"
                                                  rows="4" placeholder="isi alasan pengunduran diri"
                                                  required></textarea>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Mundur Pada Semester</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control semester_mask" name="mundur_semester"
                                               placeholder="semester" required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tahun Akademik</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control tahun_mask" name="tahun_akademik"
                                               placeholder="tahun" required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Permohonan</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="file" name="file_dokumen_permohonan" required>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Surat persetujuan</label>
                                    <div class="col-lg">
                                        <input type="text" class="form-control" name="no_surat_persetujuan"
                                               placeholder="nomor surat" required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Surat
                                        Persetujuan</label>
                                    <div class="col-lg-4">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                             width="24px"
                                                             height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none"
                                                               fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                      height="4" rx="1"></rect>
                                                                <path
                                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                    fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 datepick" name="tgl_surat_persetujuan"
                                                   placeholder="Pilih tanggal surat" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Surat
                                        Persetujuan</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="file" name="file_dokumen_surat_persetujuan"
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center mb-2 mt-10">
                                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                        class="fa fa-reply"></i>Batal
                                </button>
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_edit_laporan_hasil_studi" aria-hidden="true">
        <div class="modal-dialog mw-1000px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <div class="modal-title">
                        <h3 class="text-white">Form Laporan Berakhir Masa Studi</h4>
                    </div>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg"
                                 width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                                   fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                    <rect fill="#000000" opacity="0.5"
                                          transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                          x="0" y="7" width="16" height="2" rx="1"/>
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Hasil Studi</label>
                        <div class="col-lg">
                            <select class="form-select opsi_hasil_studi_md_edit" data-control="select2"
                                    data-hide-search="true" data-placeholder="Pilih hasil studi" name="hasil_studi"
                                    required>
                                <option></option>
                                @foreach ($ref_status_lapor_diri as $rs)
                                    @if ($rs->status == 'Belum Lulus')
                                        @continue
                                    @endif
                                    <option value="{{ $rs->id }}">{{ $rs->status }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form_opsi_md_edit form_edit_hasil_studi_lulus" hidden>
                        <form action="{{ url('tubel/administrasi/hasil-studi/lulus') }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id_tubel" value="{{ $tubel->id }}">
                            <input type="hidden" id="laporan_akhir_l_edit" name="id_laporan_akhir">
                            <div class="fw-bolder mb-4 fs-3 mt-8">
                                <span class="d-inline-block position-relative">
                                    <h1>Lulus</h1>
                                    <span
                                        class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-success translate rounded"></span>
                                </span>
                            </div>
                            <div class="rounded border border-gray-300 p-5 d-flex flex-column mb-4">
                                <div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Kelulusan</label>
                                        <div class="col-lg-4">
                                            <div class="position-relative d-flex align-items-center">
                                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                    <span class="symbol-label bg-secondary">
                                                        <span class="svg-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="24px"
                                                                 height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                          width="4" height="4" rx="1"></rect>
                                                                    <path
                                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                        fill="#000000"></path>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    </span>
                                                </div>
                                                <input class="form-control ps-12 datepick" id="tgl_kelulusan_edit"
                                                       name="tgl_kelulusan" placeholder="Pilih tanggal kelulusan"
                                                       type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Tipe Dokumen
                                            Penelitian</label>
                                        <div class="col-lg-4">
                                            <select class="form-select" data-control="select2" data-hide-search="true"
                                                    data-placeholder="Pilih dokumen penelitian"
                                                    id="opsi_dokumen_penelitian_edit" name="opsi_dokumen_penelitian"
                                                    required>
                                                <option></option>
                                                @foreach ($ref_dokumen_penelitian as $rd)
                                                    <option value="{{ $rd->id }}">{{ $rd->tipeDokumen }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Penelitian</label>
                                        <div class="col-lg">
                                            <div class="file_penelitian" hidden>
                                                <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1 fw-bold">
                                                        <a href="" target="_blank" id="file_penelitian_edit"
                                                           class="fs-6 text-hover-primary fw-bold">Dokumen
                                                            Penelitian.pdf</a>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="file_penelitian_edit_lama"
                                                       name="file_penelitian_lama">
                                            </div>
                                            <input class="form-control" type="file" name="file_penelitian">
                                        </div>
                                    </div>
                                    <div class="row mb-3 mt-2">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Laporan Selesai
                                            Studi</label>
                                        <div class="col-lg">
                                            <div class="file_lps" hidden>
                                                <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1 fw-bold">
                                                        <a href="" target="_blank" id="file_lps_edit"
                                                           class="fs-6 text-hover-primary fw-bold">Laporan Selesai
                                                            Studi.pdf</a>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="file_lps_edit_lama" name="file_lps_lama">
                                            </div>
                                            <input class="form-control" type="file" name="file_lps">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @php
                                $ipk = $no_skl = $tgl_skl = $file_skl1 = $file_skl2 = $no_ijazah = $tgl_ijazah = $file_ijazah1 = $file_ijazah2 = $no_transkrip = $tgl_transkrip = $file_transkrip1 = $file_transkrip2 = 1;
                            @endphp

                            @foreach ($perguruantinggi as $pt)
                                <div class="rounded border border-gray-300 p-5 d-flex flex-column mb-4">
                                    <h2 class="text-dark font-weight-bolder mb-4">{{ $pt->perguruanTinggiId->namaPerguruanTinggi }}</h2>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">IPK (skala 4)</label>
                                        <div class="col-lg-4">
                                            <input type="hidden" name="pt[{{ $pt->id }}][id]" value="{{ $pt->id }}">
                                            <input class="form-control ipk_mask" id="ipk_edit_{{ $ipk++ }}" type="text"
                                                   name="pt[{{ $pt->id }}][ipk]" placeholder="ipk">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor SKL</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="text" id="nomor_skl_edit_{{ $no_skl++ }}"
                                                   name="pt[{{ $pt->id }}][nomor_skl]" placeholder="nomor skl">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal SKL</label>
                                        <div class="col-lg-4">
                                            <div class="position-relative d-flex align-items-center">
                                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                    <span class="symbol-label bg-secondary">
                                                        <span class="svg-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="24px"
                                                                 height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                          width="4" height="4" rx="1"></rect>
                                                                    <path
                                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                        fill="#000000"></path>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    </span>
                                                </div>
                                                <input class="form-control ps-12 datepick"
                                                       id="tgl_skl_edit_{{ $tgl_skl++ }}"
                                                       name="pt[{{ $pt->id }}][tgl_skl]" placeholder="Pilih tanggal skl"
                                                       type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">File SKL</label>
                                        <div class="col-lg">
                                            <div class="file_skl_{{ $file_skl1 }}" hidden>
                                                <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1 fw-bold">
                                                        <a href="" target="_blank" id="file_skl_{{ $file_skl1++ }}"
                                                           class="fs-6 text-hover-primary fw-bold">Surat Keterangan
                                                            Lulus.pdf</a>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="file_skl_lama_{{ $file_skl2++ }}"
                                                       name="pt[{{ $pt->id }}][file_skl_lama]">
                                            </div>
                                            <input class="form-control" type="file" name="pt[{{ $pt->id }}][file_skl]">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Ijazah</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="text"
                                                   id="nomor_ijazah_edit_{{ $no_ijazah++ }}"
                                                   name="pt[{{ $pt->id }}][nomor_ijazah]" placeholder="nomor ijazah">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Ijazah</label>
                                        <div class="col-lg-4">
                                            <div class="position-relative d-flex align-items-center">
                                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                    <span class="symbol-label bg-secondary">
                                                        <span class="svg-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="24px"
                                                                 height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                          width="4" height="4" rx="1"></rect>
                                                                    <path
                                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                        fill="#000000"></path>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    </span>
                                                </div>
                                                <input class="form-control ps-12 datepick"
                                                       id="tgl_ijazah_edit_{{ $tgl_ijazah++ }}"
                                                       name="pt[{{ $pt->id }}][tgl_ijazah]"
                                                       placeholder="Pilih tanggal ijazah" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">File Ijazah</label>
                                        <div class="col-lg">
                                            <div class="file_ijazah_{{ $file_ijazah1 }}" hidden>
                                                <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1 fw-bold">
                                                        <a href="" target="_blank"
                                                           id="file_ijazah_{{ $file_ijazah1++ }}"
                                                           class="fs-6 text-hover-primary fw-bold">Ijazah.pdf</a>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="file_ijazah_lama_{{ $file_ijazah2++ }}"
                                                       name="pt[{{ $pt->id }}][file_ijazah_lama]">
                                            </div>
                                            <input class="form-control" type="file"
                                                   name="pt[{{ $pt->id }}][file_ijazah]">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Transkrip</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="text"
                                                   id="nomor_transkrip_edit_{{ $no_transkrip++ }}"
                                                   name="pt[{{ $pt->id }}][nomor_transkrip]"
                                                   placeholder="nomor transkrip">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Transkrip</label>
                                        <div class="col-lg-4">
                                            <div class="position-relative d-flex align-items-center">
                                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                    <span class="symbol-label bg-secondary">
                                                        <span class="svg-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="24px"
                                                                 height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                          width="4" height="4" rx="1"></rect>
                                                                    <path
                                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                        fill="#000000"></path>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    </span>
                                                </div>
                                                <input class="form-control ps-12 datepick"
                                                       id="tgl_transkrip_edit_{{ $tgl_transkrip++ }}"
                                                       name="pt[{{ $pt->id }}][tgl_transkrip]"
                                                       placeholder="Pilih tanggal transkrip"
                                                       type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">File Transkrip</label>
                                        <div class="col-lg">
                                            <div class="file_transkrip_{{ $file_transkrip1 }}" hidden>
                                                <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1 fw-bold">
                                                        <a href="" target="_blank"
                                                           id="file_transkrip_{{ $file_transkrip1++ }}"
                                                           class="fs-6 text-hover-primary fw-bold">Transkrip.pdf</a>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="file_transkrip_lama_{{ $file_transkrip2++ }}"
                                                       name="pt[{{ $pt->id }}][file_transkrip_lama]">
                                            </div>
                                            <input class="form-control" type="file"
                                                   name="pt[{{ $pt->id }}][file_transkrip]">
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="text-center mb-2 mt-10">
                                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                        class="fa fa-reply"></i>Batal
                                </button>
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="form_opsi_md_edit form_edit_hasil_studi_drop_out" hidden>
                        <form action="{{ url('tubel/administrasi/hasil-studi/drop-out') }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id_tubel" value="{{ $tubel->id }}">
                            <div class="fw-bolder mb-4 fs-3 mt-8">
                                <span class="d-inline-block position-relative">
                                    <h1>Drop Out</h1>
                                    <span
                                        class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-danger translate rounded"></span>
                                </span>
                            </div>
                            <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6 mt-6">
                                <div class="row mb-3 mt-4">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">DO Pada Semester</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control semester_mask"
                                               id="semester_do_edit_akhir" name="semester_do" placeholder="semester"
                                               required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tahun Akademik</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control tahun_mask" id="tahun_do_edit_akhir"
                                               name="tahun" placeholder="tahun" required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">No Surat Keterangan</label>
                                    <div class="col-lg">
                                        <input type="text" class="form-control" id="no_surat_keterangan_do_edit_akhir"
                                               name="no_surat_keterangan" placeholder="nomor surat" required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Surat Keterangan</label>
                                    <div class="col-lg-4">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                             width="24px"
                                                             height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none"
                                                               fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                      height="4" rx="1"></rect>
                                                                <path
                                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                    fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 datepick"
                                                   id="tgl_surat_keterangan_do_edit_akhir" name="tgl_surat_keterangan"
                                                   placeholder="Pilih tanggal surat" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Unggah Dokumen</label>
                                    <div class="col-lg">
                                        <div class="file_surat_keterangan_do">
                                            <div class="file_surat_keterangan_do" hidden>
                                                <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1 fw-bold">
                                                        <a href="" target="_blank" id="file_surat_keterangan_do_akhir"
                                                           class="fs-6 text-hover-primary fw-bold">Surat
                                                            Persetujuan.pdf</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" id="file_surat_keterangan_do_lama_akhir"
                                                   name="file_surat_keterangan_do_lama">
                                        </div>
                                        <input class="form-control" type="file" name="file_surat_keterangan">
                                    </div>
                                </div>
                            </div>
                            <div class="text-center mb-2 mt-10">
                                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                        class="fa fa-reply"></i>Batal
                                </button>
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="form_opsi_md_edit form_edit_hasil_studi_mengundurkan_diri" hidden>
                        <form action="{{ url('tubel/administrasi/hasil-studi/mengundurkan-diri') }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id_tubel" value="{{ $tubel->id }}">
                            <div class="fw-bolder mb-4 fs-3 mt-8">
                                <span class="d-inline-block position-relative">
                                    <h1>Mengundurkan Diri</h1>
                                    <span
                                        class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-danger translate rounded"></span>
                                </span>
                            </div>
                            <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6 mt-6">
                                <div class="row mb-3 mt-4">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Alasan Pengunduran diri</label>
                                    <div class="col-lg">
                                        <textarea class="form-control" id="alasan_pengunduran_diri_md_edit_akhir"
                                                  type="text" name="alasan_pengunduran_diri" rows="4"
                                                  placeholder="isi alasan pengunduran diri" required></textarea>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Mundur Pada Semester</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control semester_mask"
                                               id="mundur_semester_md_edit_akhir" name="mundur_semester"
                                               placeholder="semester" required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tahun Akademik</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control tahun_mask"
                                               id="tahun_akademik_md_edit_akhir" name="tahun_akademik"
                                               placeholder="tahun" required/>
                                    </div>
                                </div>
                                <div class="row mb-5">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Permohonan</label>
                                    <div class="col-lg">
                                        <div class="file_dokumen_permohonan" hidden>
                                            <div
                                                class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20 file_modal file_dokumen_permohonan">
                                                <img alt="" class="w-25px me-3"
                                                     src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                <div class="ms-1 fw-bold">
                                                    <a href="" target="_blank" id="file_dokumen_permohonan_akhir"
                                                       class="fs-6 text-hover-primary fw-bold">Surat Permohonan.pdf</a>
                                                </div>
                                            </div>
                                            <input type="hidden" id="file_dokumen_permohonan_lama_akhir"
                                                   name="file_dokumen_permohonan_lama">
                                        </div>
                                        <input class="form-control" type="file" name="file_dokumen_permohonan">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Surat persetujuan</label>
                                    <div class="col-lg">
                                        <input type="text" class="form-control" id="no_surat_persetujuan_md_edit_akhir"
                                               name="no_surat_persetujuan" placeholder="nomor surat" required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Surat
                                        Persetujuan</label>
                                    <div class="col-lg-4">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                             width="24px"
                                                             height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none"
                                                               fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                      height="4" rx="1"></rect>
                                                                <path
                                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                    fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 datepick"
                                                   id="tgl_surat_persetujuan_md_edit_akhir" name="tgl_surat_persetujuan"
                                                   placeholder="Pilih tanggal surat" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Surat
                                        Persetujuan</label>
                                    <div class="col-lg">
                                        <div class="file_dokumen_surat_persetujuan" hidden>
                                            <div
                                                class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20 file_modal file_dokumen_surat_persetujuan">
                                                <img alt="" class="w-25px me-3"
                                                     src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                <div class="ms-1 fw-bold">
                                                    <a href="" target="_blank" id="file_dokumen_surat_persetujuan_akhir"
                                                       class="fs-6 text-hover-primary fw-bold">Surat Persetujuan.pdf</a>
                                                </div>
                                            </div>
                                            <input type="hidden" id="file_dokumen_surat_persetujuan_lama_akhir"
                                                   name="file_dokumen_surat_persetujuan_lama">
                                        </div>
                                        <input class="form-control" type="file" name="file_dokumen_surat_persetujuan">
                                    </div>
                                </div>
                            </div>
                            <div class="text-center mb-2 mt-10">
                                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                        class="fa fa-reply"></i>Batal
                                </button>
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add_laporan_akhir_hasil_studi" aria-hidden="true">
        <div class="modal-dialog mw-1000px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <div class="modal-title">
                        <h3 class="text-white">Form Laporan Berakhir Masa Studi</h4>
                    </div>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg"
                                 width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                                   fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                    <rect fill="#000000" opacity="0.5"
                                          transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                          x="0" y="7" width="16" height="2" rx="1"/>
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Hasil Studi</label>
                        <div class="col-lg">
                            <select id="opsi_hasil_akhir_studi" class="form-select" data-control="select2"
                                    data-hide-search="true" data-placeholder="Pilih hasil studi" name="hasil_studi"
                                    required>
                                <option></option>
                                @foreach ($ref_status_lapor_diri as $rs)
                                    @if ($rs->status == 'Belum Lulus')
                                        @continue
                                    @endif
                                    <option value="{{ $rs->id }}">{{ $rs->status }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form_opsi form_hasil_studi_lulus" hidden>
                        <form action="{{ url('tubel/administrasi/hasil-studi/lulus') }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id_tubel" value="{{ $tubel->id }}">
                            <div class="fw-bolder mb-4 fs-3 mt-8">
                                <span class="d-inline-block position-relative">
                                    <h1>Lulus</h1>
                                    <span
                                        class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-success translate rounded"></span>
                                </span>
                            </div>
                            <div class="rounded border border-gray-300 p-5 d-flex flex-column mb-4">
                                <div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Kelulusan</label>
                                        <div class="col-lg-4">
                                            <div class="position-relative d-flex align-items-center">
                                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                    <span class="symbol-label bg-secondary">
                                                        <span class="svg-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="24px"
                                                                 height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                          width="4" height="4" rx="1"></rect>
                                                                    <path
                                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                        fill="#000000"></path>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    </span>
                                                </div>
                                                <input class="form-control ps-12 datepick" id="tgl_kelulusan"
                                                       name="tgl_kelulusan" placeholder="Pilih tanggal kelulusan"
                                                       type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Tipe Dokumen
                                            Penelitian</label>
                                        <div class="col-lg-4">
                                            <select class="form-select" data-control="select2" data-hide-search="true"
                                                    data-placeholder="Pilih dokumen penelitian"
                                                    name="opsi_dokumen_penelitian" required>
                                                <option></option>
                                                @foreach ($ref_dokumen_penelitian as $rd)
                                                    <option value="{{ $rd->id }}">{{ $rd->tipeDokumen }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Penelitian</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="file" name="file_penelitian" required>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Laporan Selesai
                                            Studi</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="file" name="file_lps" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @foreach ($perguruantinggi as $pt)
                                <div class="rounded border border-gray-300 p-5 d-flex flex-column mb-4 mt-6">
                                    <h2 class="text-dark font-weight-bolder mb-4">{{ $pt->perguruanTinggiId->namaPerguruanTinggi }}</h2>
                                    <div class="row form-group mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">IPK (skala 4)</label>
                                        <div class="col-lg-4">
                                            <input type="hidden" name="pt[{{ $pt->id }}][id]" value="{{ $pt->id }}">
                                            <input class="form-control ipk_mask" type="text"
                                                   name="pt[{{ $pt->id }}][ipk]" placeholder="ipk">
                                        </div>
                                    </div>
                                    <div class="row mb-3 mt-2">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor SKL</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="text" name="pt[{{ $pt->id }}][nomor_skl]"
                                                   placeholder="nomor skl">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal SKL</label>
                                        <div class="col-lg-4">
                                            <div class="position-relative d-flex align-items-center">
                                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                    <span class="symbol-label bg-secondary">
                                                        <span class="svg-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="24px"
                                                                 height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                          width="4" height="4" rx="1"></rect>
                                                                    <path
                                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                        fill="#000000"></path>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    </span>
                                                </div>
                                                <input class="form-control ps-12 datepick"
                                                       name="pt[{{ $pt->id }}][tgl_skl]" placeholder="Pilih tanggal skl"
                                                       type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">File SKL</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="file" name="pt[{{ $pt->id }}][file_skl]">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Ijazah</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="text"
                                                   name="pt[{{ $pt->id }}][nomor_ijazah]" placeholder="nomor ijazah">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Ijazah</label>
                                        <div class="col-lg-4">
                                            <div class="position-relative d-flex align-items-center">
                                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                    <span class="symbol-label bg-secondary">
                                                        <span class="svg-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="24px"
                                                                 height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                          width="4" height="4" rx="1"></rect>
                                                                    <path
                                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                        fill="#000000"></path>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    </span>
                                                </div>
                                                <input class="form-control ps-12 datepick"
                                                       name="pt[{{ $pt->id }}][tgl_ijazah]"
                                                       placeholder="Pilih tanggal ijazah" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">File Ijazah</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="file"
                                                   name="pt[{{ $pt->id }}][file_ijazah]">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Transkrip</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="text"
                                                   name="pt[{{ $pt->id }}][nomor_transkrip]"
                                                   placeholder="nomor transkrip">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Transkrip</label>
                                        <div class="col-lg-4">
                                            <div class="position-relative d-flex align-items-center">
                                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                    <span class="symbol-label bg-secondary">
                                                        <span class="svg-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="24px"
                                                                 height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                          width="4" height="4" rx="1"></rect>
                                                                    <path
                                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                        fill="#000000"></path>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    </span>
                                                </div>
                                                <input class="form-control ps-12 datepick"
                                                       name="pt[{{ $pt->id }}][tgl_transkrip]"
                                                       placeholder="Pilih tanggal transkrip" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">File Transkrip</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="file"
                                                   name="pt[{{ $pt->id }}][file_transkrip]">
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="text-center mb-2 mt-10">
                                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                        class="fa fa-reply"></i>Batal
                                </button>
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="form_opsi form_hasil_studi_belum_lulus" hidden>
                        <form action="{{ url('tubel/administrasi/hasil-studi/belum-lulus') }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="tipe" value="belum_lulus">
                            <input type="hidden" name="id_tubel" value="{{ $tubel->id }}">
                            <div class="fw-bolder mb-4 fs-3 mt-8">
                                <span class="d-inline-block position-relative">
                                    <h1>Belum Lulus</h1>
                                    <span
                                        class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-warning translate rounded"></span>
                                </span>
                            </div>
                            <div class="rounded border border-gray-300 p-4 d-flex flex-column mt-6">
                                <div class="row mb-3 mt-4">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tahapan Studi</label>
                                    <div class="col-lg">
                                        <select id="opsi_tahapan_studi" class="form-select" data-control="select2"
                                                data-hide-search="true" data-placeholder="Pilih dokumen penelitian"
                                                name="opsi_tahapan_studi" required>
                                            <option></option>
                                            @foreach ($ref_tahap_belum_lulus as $bl)
                                                <option value="{{ $bl->id }}">{{ $bl->tahap }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Timeline Rencana
                                        Penyelesaian Studi</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="file" name="file_timeline_studi_add" required>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tipe Dokumen Penelitian</label>
                                    <div class="col-lg">
                                        <select class="form-select" data-control="select2" data-hide-search="true"
                                                data-placeholder="Pilih dokumen penelitian"
                                                name="opsi_dokumen_penelitian" required>
                                            <option></option>
                                            @foreach ($ref_dokumen_penelitian as $rd)
                                                <option value="{{ $rd->id }}">{{ $rd->tipeDokumen }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Draft Dokumen Penelitian</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="file" name="file_draft_dokumen_penelitian_add"
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center mb-2 mt-10">
                                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                        class="fa fa-reply"></i>Batal
                                </button>
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="form_opsi form_hasil_studi_drop_out" hidden>
                        <form action="{{ url('tubel/administrasi/hasil-studi/drop-out') }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id_tubel" value="{{ $tubel->id }}">
                            <div class="fw-bolder mb-4 fs-3 mt-8">
                                <span class="d-inline-block position-relative">
                                    <h1>Drop Out</h1>
                                    <span
                                        class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-danger translate rounded"></span>
                                </span>
                            </div>
                            <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6 mt-6">
                                <div class="row mb-3 mt-4">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">DO Pada Semester</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control semester_mask" name="semester_do"
                                               placeholder="semester" required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tahun Akademik</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control tahun_mask" name="tahun"
                                               placeholder="tahun" required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">No Surat Keterangan</label>
                                    <div class="col-lg">
                                        <input type="text" class="form-control" name="no_surat_keterangan"
                                               placeholder="nomor surat" required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Surat Keterangan</label>
                                    <div class="col-lg-4">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                             width="24px"
                                                             height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none"
                                                               fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                      height="4" rx="1"></rect>
                                                                <path
                                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                    fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 datepick" name="tgl_surat_keterangan"
                                                   placeholder="Pilih tanggal surat" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Unggah Dokumen</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="file" name="file_surat_keterangan" required>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center mb-2 mt-10">
                                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                        class="fa fa-reply"></i>Batal
                                </button>
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="form_opsi form_hasil_studi_mengundurkan_diri" hidden>
                        <form action="{{ url('tubel/administrasi/hasil-studi/mengundurkan-diri') }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id_tubel" value="{{ $tubel->id }}">
                            <div class="fw-bolder mb-4 fs-3 mt-8">
                                <span class="d-inline-block position-relative">
                                    <h1>Mengundurkan Diri</h1>
                                    <span
                                        class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-danger translate rounded"></span>
                                </span>
                            </div>
                            <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6 mt-6">
                                <div class="row mb-3 mt-4">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Alasan Pengunduran diri</label>
                                    <div class="col-lg">
                                        <textarea class="form-control" type="text" name="alasan_pengunduran_diri"
                                                  rows="4" placeholder="isi alasan pengunduran diri"
                                                  required></textarea>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Mundur Pada Semester</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control semester_mask" name="mundur_semester"
                                               placeholder="semester" required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tahun Akademik</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control tahun_mask" name="tahun_akademik"
                                               placeholder="tahun" required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Permohonan</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="file" name="file_dokumen_permohonan" required>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Surat persetujuan</label>
                                    <div class="col-lg">
                                        <input type="text" class="form-control" name="no_surat_persetujuan"
                                               placeholder="nomor surat" required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Surat
                                        Persetujuan</label>
                                    <div class="col-lg-4">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                             width="24px"
                                                             height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none"
                                                               fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                      height="4" rx="1"></rect>
                                                                <path
                                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                    fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 datepick" name="tgl_surat_persetujuan"
                                                   placeholder="Pilih tanggal surat" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Surat
                                        Persetujuan</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="file" name="file_dokumen_surat_persetujuan"
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center mb-2 mt-10">
                                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                        class="fa fa-reply"></i>Batal
                                </button>
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add_laporan_akhir_hasil_studi_ijazah" aria-hidden="true">
        <div class="modal-dialog mw-1000px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <div class="modal-title">
                        <h3 class="text-white">Form Laporan Berakhir Masa Studi</h4>
                    </div>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg"
                                 width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                                   fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                    <rect fill="#000000" opacity="0.5"
                                          transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                          x="0" y="7" width="16" height="2" rx="1"/>
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body scroll-y px-5 px-lg-10">
                    <form action="{{ url('tubel/administrasi/hasil-studi/ijazah') }}" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id_tubel" value="{{ $tubel->id }}">
                        <input type="hidden" id="id_hasil_studi_ijazah" name="id_hasil_studi">
                        <div class="fw-bolder mb-4 fs-3">
                            <span class="d-inline-block position-relative">
                                <h1>Lulus</h1>
                                <span
                                    class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-success translate rounded"></span>
                            </span>
                        </div>
                        @foreach ($perguruantinggi as $pt)
                            <div class="rounded border border-gray-300 p-5 d-flex flex-column mb-4 mt-6">
                                <h2 class="text-dark font-weight-bolder mb-4">{{ $pt->perguruanTinggiId->namaPerguruanTinggi }}</h2>
                                {{-- <div class="row form-group mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">IPK (skala 4)</label>
                                    <div class="col-lg-4">
                                        <input type="hidden" name="pt[{{ $pt->id }}][id]" value="{{ $pt->id }}">
                                        <input class="form-control ipk_mask" type="text" name="pt[{{ $pt->id }}][ipk]" placeholder="ipk">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor SKL</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="text" name="pt[{{ $pt->id }}][nomor_skl]" placeholder="nomor skl">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal SKL</label>
                                    <div class="col-lg-4">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                <path
                                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                    fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 datepick" name="pt[{{ $pt->id }}][tgl_skl]" placeholder="Pilih tanggal skl" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">File SKL</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="file" name="pt[{{ $pt->id }}][file_skl]">
                                    </div>
                                </div> --}}
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Ijazah</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="text" name="pt[{{ $pt->id }}][nomor_ijazah]"
                                               placeholder="nomor ijazah">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Ijazah</label>
                                    <div class="col-lg-4">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                             width="24px"
                                                             height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none"
                                                               fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                      height="4" rx="1"></rect>
                                                                <path
                                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                    fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 datepick"
                                                   name="pt[{{ $pt->id }}][tgl_ijazah]"
                                                   placeholder="Pilih tanggal ijazah" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">File Ijazah</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="file" name="pt[{{ $pt->id }}][file_ijazah]">
                                    </div>
                                </div>
                                {{-- <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Transkrip</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="text" name="pt[{{ $pt->id }}][nomor_transkrip]" placeholder="nomor transkrip">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Transkrip</label>
                                    <div class="col-lg-4">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                <path
                                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                    fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 datepick" name="pt[{{ $pt->id }}][tgl_transkrip]" placeholder="Pilih tanggal transkrip" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">File Transkrip</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="file" name="pt[{{ $pt->id }}][file_transkrip]">
                                    </div>
                                </div> --}}
                            </div>
                        @endforeach
                        <div class="text-center mb-2 mt-10">
                            <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                    class="fa fa-reply"></i>Batal
                            </button>
                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit_laporan_akhir_hasil_studi_ijazah" aria-hidden="true">
        <div class="modal-dialog mw-1000px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <div class="modal-title">
                        <h3 class="text-white">Form Laporan Berakhir Masa Studi</h4>
                    </div>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg"
                                 width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                                   fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                    <rect fill="#000000" opacity="0.5"
                                          transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                          x="0" y="7" width="16" height="2" rx="1"/>
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body scroll-y px-5 px-lg-10">
                    <form action="{{ url('tubel/administrasi/hasil-studi/ijazah') }}" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" id="id_laporan_akhir_ijazah_edit" name="id_hasil_studi">
                        <div class="fw-bolder mb-4 fs-3">
                            <span class="d-inline-block position-relative">
                                <h1>Lulus</h1>
                                <span
                                    class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-success translate rounded"></span>
                            </span>
                        </div>
                        @php
                            $no_ijazah = $tgl_ijazah = $file_ijazah = 1;
                        @endphp

                        @foreach ($perguruantinggi as $pt)
                            <div class="rounded border border-gray-300 p-5 d-flex flex-column mb-4 mt-6">
                                <h2 class="text-dark font-weight-bolder mb-4">{{ $pt->perguruanTinggiId->namaPerguruanTinggi }}</h2>
                                {{-- <div class="row form-group mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">IPK (skala 4)</label>
                                    <div class="col-lg-4">
                                        <input type="hidden" name="pt[{{ $pt->id }}][id]" value="{{ $pt->id }}">
                                        <input class="form-control ipk_mask" type="text" name="pt[{{ $pt->id }}][ipk]" placeholder="ipk">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor SKL</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="text" name="pt[{{ $pt->id }}][nomor_skl]" placeholder="nomor skl">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal SKL</label>
                                    <div class="col-lg-4">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                <path
                                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                    fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 datepick" name="pt[{{ $pt->id }}][tgl_skl]" placeholder="Pilih tanggal skl" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">File SKL</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="file" name="pt[{{ $pt->id }}][file_skl]">
                                    </div>
                                </div> --}}
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Ijazah</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="text"
                                               id="nomor_ijazah_ha_edit_{{ $no_ijazah++ }}"
                                               name="pt[{{ $pt->id }}][nomor_ijazah]" placeholder="nomor ijazah">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Ijazah</label>
                                    <div class="col-lg-4">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                             width="24px"
                                                             height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none"
                                                               fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                      height="4" rx="1"></rect>
                                                                <path
                                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                    fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 datepick"
                                                   id="tgl_ijazah_ha_edit_{{ $tgl_ijazah++ }}"
                                                   name="pt[{{ $pt->id }}][tgl_ijazah]"
                                                   placeholder="Pilih tanggal ijazah" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">File Ijazah</label>
                                    <div class="col-lg">
                                        <div class="file_ijazah_ha_{{ $file_ijazah }}" hidden>
                                            <div
                                                class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20 file_modal file_dokumen_permohonan">
                                                <img alt="" class="w-25px me-3"
                                                     src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                <div class="ms-1 fw-bold">
                                                    <a href="" target="_blank" id="file_ijazah_ha_{{ $file_ijazah }}"
                                                       class="fs-6 text-hover-primary fw-bold">Dokumen Ijazah.pdf</a>
                                                </div>
                                            </div>
                                            <input type="hidden" id="file_ijazah_ha_lama_{{ $file_ijazah }}"
                                                   name="pt[{{ $pt->id }}][file_ijazah_lama]">
                                        </div>
                                        <input class="form-control" type="file" id="file_ijazah_ha_{{ $file_ijazah++ }}"
                                               name="pt[{{ $pt->id }}][file_ijazah]">
                                    </div>
                                </div>
                                {{-- <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Transkrip</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="text" name="pt[{{ $pt->id }}][nomor_transkrip]" placeholder="nomor transkrip">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Transkrip</label>
                                    <div class="col-lg-4">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                <path
                                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                    fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 datepick" name="pt[{{ $pt->id }}][tgl_transkrip]" placeholder="Pilih tanggal transkrip" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">File Transkrip</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="file" name="pt[{{ $pt->id }}][file_transkrip]">
                                    </div>
                                </div> --}}
                            </div>
                        @endforeach
                        <div class="text-center mb-2 mt-10">
                            <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                    class="fa fa-reply"></i>Batal
                            </button>
                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_edit_laporan_akhir_hasil_studi" aria-hidden="true">
        <div class="modal-dialog mw-1000px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <div class="modal-title">
                        <h3 class="text-white">Form Laporan Berakhir Masa Studi</h4>
                    </div>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg"
                                 width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                                   fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                    <rect fill="#000000" opacity="0.5"
                                          transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                          x="0" y="7" width="16" height="2" rx="1"/>
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Hasil Studi</label>
                        <div class="col-lg">
                            <select class="form-select opsi_hasil_studi_md_edit" data-control="select2"
                                    data-hide-search="true" data-placeholder="Pilih hasil studi" name="hasil_studi"
                                    required>
                                <option></option>
                                @foreach ($ref_status_lapor_diri as $rs)
                                    @if ($rs->status == 'Belum Lulus')
                                        @continue
                                    @endif
                                    <option value="{{ $rs->id }}">{{ $rs->status }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form_opsi_md_edit form_edit_hasil_studi_lulus" hidden>
                        <form action="{{ url('tubel/administrasi/hasil-studi/lulus') }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id_tubel" value="{{ $tubel->id }}">
                            <input type="hidden" id="laporan_akhir_l_edit" name="id_laporan_akhir">
                            <div class="fw-bolder mb-4 fs-3 mt-8">
                                <span class="d-inline-block position-relative">
                                    <h1>Lulus</h1>
                                    <span
                                        class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-success translate rounded"></span>
                                </span>
                            </div>
                            <div class="rounded border border-gray-300 p-5 d-flex flex-column mb-4">
                                <div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Kelulusan</label>
                                        <div class="col-lg-4">
                                            <div class="position-relative d-flex align-items-center">
                                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                    <span class="symbol-label bg-secondary">
                                                        <span class="svg-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="24px"
                                                                 height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                          width="4" height="4" rx="1"></rect>
                                                                    <path
                                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                        fill="#000000"></path>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    </span>
                                                </div>
                                                <input class="form-control ps-12 datepick" id="tgl_kelulusan_l_edit"
                                                       name="tgl_kelulusan" placeholder="Pilih tanggal kelulusan"
                                                       type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Tipe Dokumen
                                            Penelitian</label>
                                        <div class="col-lg-4">
                                            <select class="form-select" data-control="select2" data-hide-search="true"
                                                    data-placeholder="Pilih dokumen penelitian"
                                                    id="opsi_dokumen_penelitian_l_edit" name="opsi_dokumen_penelitian"
                                                    required>
                                                <option></option>
                                                @foreach ($ref_dokumen_penelitian as $rd)
                                                    <option value="{{ $rd->id }}">{{ $rd->tipeDokumen }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Penelitian</label>
                                        <div class="col-lg">
                                            <div class="file_penelitian" hidden>
                                                <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1 fw-bold">
                                                        <a href="" target="_blank" id="file_penelitian_l_edit"
                                                           class="fs-6 text-hover-primary fw-bold">Dokumen
                                                            Penelitian.pdf</a>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="file_penelitian_l_edit_lama"
                                                       name="file_penelitian_lama">
                                            </div>
                                            <input class="form-control" type="file" name="file_penelitian">
                                        </div>
                                    </div>
                                    <div class="row mb-3 mt-2">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Laporan Selesai
                                            Studi</label>
                                        <div class="col-lg">
                                            <div class="file_lps" hidden>
                                                <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1 fw-bold">
                                                        <a href="" target="_blank" id="file_lps_l_edit"
                                                           class="fs-6 text-hover-primary fw-bold">Laporan Selesai
                                                            Studi.pdf</a>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="file_lps_l_edit_lama" name="file_lps_lama">
                                            </div>
                                            <input class="form-control" type="file" name="file_lps">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @php
                                $ipk = $no_skl = $tgl_skl = $file_skl1 = $file_skl2 = $no_ijazah = $tgl_ijazah = $file_ijazah1 = $file_ijazah2 = $no_transkrip = $tgl_transkrip = $file_transkrip1 = $file_transkrip2 = 1;
                            @endphp

                            @foreach ($perguruantinggi as $pt)
                                <div class="rounded border border-gray-300 p-5 d-flex flex-column mb-4">
                                    <h2 class="text-dark font-weight-bolder mb-4">{{ $pt->perguruanTinggiId->namaPerguruanTinggi }}</h2>
                                    <div class="row form-group mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">IPK (skala 4)</label>
                                        <div class="col-lg-4">
                                            <input type="hidden" name="pt[{{ $pt->id }}][id]" value="{{ $pt->id }}">
                                            <input class="form-control ipk_mask" id="ipk_l_edit_{{ $ipk++ }}"
                                                   type="text" name="pt[{{ $pt->id }}][ipk]" placeholder="ipk">
                                        </div>
                                    </div>
                                    <div class="row mb-3 mt-2">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor SKL</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="text"
                                                   id="nomor_skl_l_edit_{{ $no_skl++ }}"
                                                   name="pt[{{ $pt->id }}][nomor_skl]" placeholder="nomor skl">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal SKL</label>
                                        <div class="col-lg-4">
                                            <div class="position-relative d-flex align-items-center">
                                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                    <span class="symbol-label bg-secondary">
                                                        <span class="svg-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="24px"
                                                                 height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                          width="4" height="4" rx="1"></rect>
                                                                    <path
                                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                        fill="#000000"></path>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    </span>
                                                </div>
                                                <input class="form-control ps-12 datepick"
                                                       id="tgl_skl_l_edit_{{ $tgl_skl++ }}"
                                                       name="pt[{{ $pt->id }}][tgl_skl]" placeholder="Pilih tanggal skl"
                                                       type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">File SKL</label>
                                        <div class="col-lg">
                                            <div class="file_skl" hidden>
                                                <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1 fw-bold">
                                                        <a href="" target="_blank" id="file_skl_{{ $file_skl1++ }}"
                                                           class="fs-6 text-hover-primary fw-bold">Surat Keterangan
                                                            Lulus.pdf</a>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="file_skl_lama_{{ $file_skl2++ }}"
                                                       name="pt[{{ $pt->id }}][file_skl_lama]">
                                            </div>
                                            <input class="form-control" type="file" name="pt[{{ $pt->id }}][file_skl]">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Ijazah</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="text"
                                                   id="nomor_ijazah_l_edit_{{ $no_ijazah++ }}"
                                                   name="pt[{{ $pt->id }}][nomor_ijazah]" placeholder="nomor ijazah">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Ijazah</label>
                                        <div class="col-lg-4">
                                            <div class="position-relative d-flex align-items-center">
                                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                    <span class="symbol-label bg-secondary">
                                                        <span class="svg-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="24px"
                                                                 height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                          width="4" height="4" rx="1"></rect>
                                                                    <path
                                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                        fill="#000000"></path>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    </span>
                                                </div>
                                                <input class="form-control ps-12 datepick"
                                                       id="tgl_ijazah_l_edit_{{ $tgl_ijazah++ }}"
                                                       name="pt[{{ $pt->id }}][tgl_ijazah]"
                                                       placeholder="Pilih tanggal ijazah" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">File Ijazah</label>
                                        <div class="col-lg">
                                            <div class="file_ijazah" hidden>
                                                <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1 fw-bold">
                                                        <a href="" target="_blank"
                                                           id="file_ijazah_{{ $file_ijazah1++ }}"
                                                           class="fs-6 text-hover-primary fw-bold">Ijazah.pdf</a>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="file_ijazah_lama_{{ $file_ijazah2++ }}"
                                                       name="pt[{{ $pt->id }}][file_ijazah_lama]">
                                            </div>
                                            <input class="form-control" type="file"
                                                   name="pt[{{ $pt->id }}][file_ijazah]">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Transkrip</label>
                                        <div class="col-lg">
                                            <input class="form-control" type="text"
                                                   id="nomor_transkrip_l_edit_{{ $no_transkrip++ }}"
                                                   name="pt[{{ $pt->id }}][nomor_transkrip]"
                                                   placeholder="nomor transkrip">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Transkrip</label>
                                        <div class="col-lg-4">
                                            <div class="position-relative d-flex align-items-center">
                                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                    <span class="symbol-label bg-secondary">
                                                        <span class="svg-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="24px"
                                                                 height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                          width="4" height="4" rx="1"></rect>
                                                                    <path
                                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                        fill="#000000"></path>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    </span>
                                                </div>
                                                <input class="form-control ps-12 datepick"
                                                       id="tgl_transkrip_l_edit_{{ $tgl_transkrip++ }}"
                                                       name="pt[{{ $pt->id }}][tgl_transkrip]"
                                                       placeholder="Pilih tanggal transkrip"
                                                       type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">File Transkrip</label>
                                        <div class="col-lg">
                                            <div class="file_transkrip" hidden>
                                                <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1 fw-bold">
                                                        <a href="" target="_blank"
                                                           id="file_transkrip_{{ $file_transkrip1++ }}"
                                                           class="fs-6 text-hover-primary fw-bold">Transkrip.pdf</a>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="file_transkrip_lama_{{ $file_transkrip2++ }}"
                                                       name="pt[{{ $pt->id }}][file_transkrip_lama]">
                                            </div>
                                            <input class="form-control" type="file"
                                                   name="pt[{{ $pt->id }}][file_transkrip]">
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="text-center mb-2 mt-10">
                                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                        class="fa fa-reply"></i>Batal
                                </button>
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="form_opsi_md_edit form_edit_hasil_studi_drop_out" hidden>
                        <form action="{{ url('tubel/administrasi/hasil-studi/drop-out') }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id_tubel" value="{{ $tubel->id }}">
                            <div class="fw-bolder mb-4 fs-3 mt-8">
                                <span class="d-inline-block position-relative">
                                    <h1>Drop Out</h1>
                                    <span
                                        class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-danger translate rounded"></span>
                                </span>
                            </div>
                            <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6 mt-6">
                                <div class="row mb-3 mt-4">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">DO Pada Semester</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control semester_mask"
                                               id="semester_do_edit_akhir" name="semester_do" placeholder="semester"
                                               required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tahun Akademik</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control tahun_mask" id="tahun_do_edit_akhir"
                                               name="tahun" placeholder="tahun" required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">No Surat Keterangan</label>
                                    <div class="col-lg">
                                        <input type="text" class="form-control" id="no_surat_keterangan_do_edit_akhir"
                                               name="no_surat_keterangan" placeholder="nomor surat" required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Surat Keterangan</label>
                                    <div class="col-lg-4">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                             width="24px"
                                                             height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none"
                                                               fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                      height="4" rx="1"></rect>
                                                                <path
                                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                    fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 datepick"
                                                   id="tgl_surat_keterangan_do_edit_akhir" name="tgl_surat_keterangan"
                                                   placeholder="Pilih tanggal surat" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Unggah Dokumen</label>
                                    <div class="col-lg">
                                        <div class="file_surat_keterangan_do">
                                            <div class="file_surat_keterangan_do" hidden>
                                                <div class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20">
                                                    <img alt="" class="w-25px me-3"
                                                         src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                    <div class="ms-1 fw-bold">
                                                        <a href="" target="_blank" id="file_surat_keterangan_do_akhir"
                                                           class="fs-6 text-hover-primary fw-bold">Surat
                                                            Persetujuan.pdf</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" id="file_surat_keterangan_do_lama_akhir"
                                                   name="file_surat_keterangan_do_lama">
                                        </div>
                                        <input class="form-control" type="file" name="file_surat_keterangan">
                                    </div>
                                </div>
                            </div>
                            <div class="text-center mb-2 mt-10">
                                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                        class="fa fa-reply"></i>Batal
                                </button>
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="form_opsi_md_edit form_edit_hasil_studi_mengundurkan_diri" hidden>
                        <form action="{{ url('tubel/administrasi/hasil-studi/mengundurkan-diri') }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id_tubel" value="{{ $tubel->id }}">
                            <div class="fw-bolder mb-4 fs-3 mt-8">
                                <span class="d-inline-block position-relative">
                                    <h1>Mengundurkan Diri</h1>
                                    <span
                                        class="d-inline-block position-absolute h-5px bottom-0 end-0 start-0 bg-danger translate rounded"></span>
                                </span>
                            </div>
                            <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6 mt-6">
                                <div class="row mb-3 mt-4">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Alasan Pengunduran diri</label>
                                    <div class="col-lg">
                                        <textarea class="form-control" id="alasan_pengunduran_diri_md_edit_akhir"
                                                  type="text" name="alasan_pengunduran_diri" rows="4"
                                                  placeholder="isi alasan pengunduran diri" required></textarea>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Mundur Pada Semester</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control semester_mask"
                                               id="mundur_semester_md_edit_akhir" name="mundur_semester"
                                               placeholder="semester" required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tahun Akademik</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control tahun_mask"
                                               id="tahun_akademik_md_edit_akhir" name="tahun_akademik"
                                               placeholder="tahun" required/>
                                    </div>
                                </div>
                                <div class="row mb-5">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Permohonan</label>
                                    <div class="col-lg">
                                        <div class="file_dokumen_permohonan" hidden>
                                            <div
                                                class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20 file_modal file_dokumen_permohonan">
                                                <img alt="" class="w-25px me-3"
                                                     src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                <div class="ms-1 fw-bold">
                                                    <a href="" target="_blank" id="file_dokumen_permohonan_akhir"
                                                       class="fs-6 text-hover-primary fw-bold">Surat Permohonan.pdf</a>
                                                </div>
                                            </div>
                                            <input type="hidden" id="file_dokumen_permohonan_lama_akhir"
                                                   name="file_dokumen_permohonan_lama">
                                        </div>
                                        <input class="form-control" type="file" name="file_dokumen_permohonan">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Nomor Surat persetujuan</label>
                                    <div class="col-lg">
                                        <input type="text" class="form-control" id="no_surat_persetujuan_md_edit_akhir"
                                               name="no_surat_persetujuan" placeholder="nomor surat" required/>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Tanggal Surat
                                        Persetujuan</label>
                                    <div class="col-lg-4">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                             width="24px"
                                                             height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none"
                                                               fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                      height="4" rx="1"></rect>
                                                                <path
                                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                    fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 datepick"
                                                   id="tgl_surat_persetujuan_md_edit_akhir" name="tgl_surat_persetujuan"
                                                   placeholder="Pilih tanggal surat" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">Dokumen Surat
                                        Persetujuan</label>
                                    <div class="col-lg">
                                        <div class="file_dokumen_surat_persetujuan" hidden>
                                            <div
                                                class="d-flex flex-aligns-center mt-2 mb-3 pe-10 pe-lg-20 file_modal file_dokumen_surat_persetujuan">
                                                <img alt="" class="w-25px me-3"
                                                     src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                                <div class="ms-1 fw-bold">
                                                    <a href="" target="_blank" id="file_dokumen_surat_persetujuan_akhir"
                                                       class="fs-6 text-hover-primary fw-bold">Surat Persetujuan.pdf</a>
                                                </div>
                                            </div>
                                            <input type="hidden" id="file_dokumen_surat_persetujuan_lama_akhir"
                                                   name="file_dokumen_surat_persetujuan_lama">
                                        </div>
                                        <input class="form-control" type="file" name="file_dokumen_surat_persetujuan">
                                    </div>
                                </div>
                            </div>
                            <div class="text-center mb-2 mt-10">
                                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                                        class="fa fa-reply"></i>Batal
                                </button>
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endif
