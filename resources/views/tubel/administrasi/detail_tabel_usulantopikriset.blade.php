<div class="table-responsive rounded border border-gray-300">
    {{-- <table class="table table-row-dashed table-row-gray-300 align-middle"> --}}
    <table class="table table-row-dashed table-row-gray-300 align-middle">
        <thead class="fw-bolder bg-secondary fs-6">
        <tr>
            <th class="ps-4 col-lg-3">Topik / Judul</th>
            <th class="col-lg-3">Proposal / Persetujuan PT</th>
            <th class="col-lg">Pengesahan</th>
            <th class="col-lg-2">Status</th>
            <th class="text-center col-lg-2">Aksi</th>
        </tr>
        </thead>

        <tbody id="body">
        @if ($usulanriset != null)

            @foreach ($usulanriset as $u)

                <tr id="usulantopikriset-{{ $u->id }}">
                    <td class="ps-4 fs-6">
                        <div>
                            Topik:
                            <br>
                            {{ $u->topik ? $u->topik->namaTopik : '' }}
                        </div>

                        <div class="mt-3">
                            Judul:
                            <br>
                            {{ $u->namaRiset }}
                        </div>
                    </td>

                    <td class="fs-6">
                        <div>
                            Proposal:
                            <br>
                            <div class="d-flex flex-aligns-center mt-1 pe-6">
                                <img alt="" class="w-20px me-1" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1">
                                    <a href="{{ $u->pathProposal ? url('files/' . $u->pathProposal) : '#' }}"
                                       target="_blank" class="fs-6 text-hover-primary">Proposal.pdf</a>
                                </div>
                            </div>
                        </div>

                        <div class="mt-3">
                            Persetujuan PT/Dosen:
                            <br>
                            @if ($u->tglPersetujuanPt !== null)
                                tanggal: {{ AppHelper::instance()->indonesian_date($u->tglPersetujuanPt, 'j F Y', '') }}
                                <br>
                                <div class="d-flex flex-aligns-center mt-1 pe-6">
                                    <img alt="" class="w-20px me-1" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                    <div class="ms-1">
                                        <a href="{{ $u->pathDokumenPersetujuanPt ? url('files/' . $u->pathDokumenPersetujuanPt) : '#' }}"
                                           target="_blank" class="fs-6 text-hover-primary">Persetujuan.pdf</a>
                                    </div>
                                </div>
                            @else
                                <i><span class="text-muted fw-bold text-muted  fs-6">{{ 'belum ada' }}</span></i>
                            @endif
                        </div>
                    </td>

                    <td class="fs-6">
                        @if ($u->nomorNdPersetujuanDjp !== null)
                            {{ $u->nomorNdPersetujuanDjp }}
                            <br>
                            tanggal: {{ AppHelper::instance()->indonesian_date($u->tglPersetujuanDjp, 'j F Y', '') }}
                            <br>
                            <div class="d-flex flex-aligns-center mt-1 pe-6">
                                <img alt="" class="w-20px me-1" src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                <div class="ms-1">
                                    <a href="{{ $u->pathNdPersetujuanDjp ? url('files/' . $u->pathNdPersetujuanDjp) : '#' }}"
                                       target="_blank" class="fs-6 text-hover-primary">Pengesahan.pdf</a>
                                </div>
                            </div>
                        @else
                            <i><span class="text-muted fw-bold text-muted  fs-6">{{ 'belum ada' }}</span></i>
                        @endif
                    </td>

                    <td class="text-dark mb-1 fs-6">

                        @if ($u->logStatusProbis->output == 'Draft usulan topik dibuat' || $u->logStatusProbis->output == 'Usulan topik dikirim' || $u->logStatusProbis->output == 'Usulan topik disetujui' || $u->logStatusProbis->output == 'Perguruan Tinggi setuju topik' || $u->logStatusProbis->output == 'Persetujuan PT dikirim' || $u->logStatusProbis->output == 'Topik Riset disahkan' || $u->logStatusProbis->output == 'Surat Persetujuan Topik diunggah')
                            <span
                                class="badge badge-light-success fs-7 fw-bolder">{{ $u->logStatusProbis->output }}</span>
                        @elseif ($u->logStatusProbis->output == 'Usulan topik dikembalikan')
                            <span
                                class="badge badge-light-warning fs-7 fw-bolder">{{ $u->logStatusProbis->output }}</span>
                        @elseif ($u->logStatusProbis->output == 'Usulan topik ditolak' || $u->logStatusProbis->output == 'Perguruan Tinggi menolak topik')
                            <span
                                class="badge badge-light-danger fs-7 fw-bolder">{{ $u->logStatusProbis->output }}</span>
                        @endif
                        <br>
                        Tiket:
                        <a href="javascript:void(0)" class="text-primary btnTiket"
                           data-tiket="{{ $u->logStatusProbis->nomorTiket }}"
                           data-nama="{{ $u->dataIndukTubel->namaPegawai }}">
                            {{ $u->logStatusProbis->nomorTiket }}
                        </a>

                    </td>

                    <td class="text-center">

                        @if ($u->logStatusProbis->output == 'Draft usulan topik dibuat' || $u->logStatusProbis->output == 'Usulan topik dikembalikan')

                            <button class="btn btn-sm btn-icon btn-warning btnEditUsulanRiset" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Edit" data-id_usulan_riset_edit="{{ $u->id }}"
                                    data-id_pilihan_topik_edit="{{ $u->topik->id }}"
                                    data-pilihan_topik_edit="{{ $u->topik->namaTopik }}"
                                    data-judul_riset_edit="{{ $u->namaRiset }}"
                                    data-file_dokumen_proposal_edit="{{ $u->pathProposal }}">
                                <i class="fa fa-edit"></i>
                            </button>

                            <button class="btn btn-sm btn-icon btn-danger btnHapusUsulanRiset" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Hapus" data-id="{{ $u->id }}">
                                <i class="fa fa-trash"></i>
                            </button>

                            <button class="btn btn-sm btn-primary btnKirimUsulanRiset" data-id="{{ $u->id }}"
                                    data-act="dea0e97e-308d-416f-942e-38874c108bcd">
                                <i class="fa fa-paper-plane"></i> Kirim
                            </button>

                        @elseif($u->logStatusProbis->output == 'Perguruan Tinggi setuju topik')

                            <button class="btn btn-sm btn-info btn-icon btnLihatUsulanRiset" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Lihat" data-case="{{ $u->logStatusProbis->output }}"
                                    data-nama="{{ $u->dataIndukTubel->namaPegawai }}"
                                    data-nip="{{ $u->dataIndukTubel->nip18 }}"
                                    data-pangkat="{{ $u->dataIndukTubel->namaPangkat }}"
                                    data-jabatan="{{ $u->dataIndukTubel->namaJabatan }}"
                                    data-jenjang="{{ $u->dataIndukTubel->jenjangPendidikanId->jenjangPendidikan }}"
                                    data-program_beasiswa="{{ $u->dataIndukTubel->programBeasiswa }}"
                                    data-lokasi="{{ $u->dataIndukTubel->lokasiPendidikanId->lokasi }}"
                                    {{-- data-pt="{{ $u->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }}" --}} {{-- data-prodi="{{ $u->ptPesertaTubelId->programStudiId->programStudi }}" --}}
                                    data-st_tubel="{{ $u->dataIndukTubel->nomorSt }}"
                                    data-tgl_st_tubel="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tglSt, 'j F Y', '') }}"
                                    data-tgl_mulai="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tglMulaiSt, 'j F Y', '') }}"
                                    data-tgl_selesai="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tglSelesaiSt, 'j F Y', '') }}"
                                    data-kep_pembebasan="{{ $u->dataIndukTubel->nomorKepPembebasan }}"
                                    data-tgl_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tglKepPembebasan, 'j F Y', '') }}"
                                    data-tmt_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tmtKepPembebasan, 'j F Y', '') }}"
                                    data-email="{{ $u->dataIndukTubel->emailNonPajak }}"
                                    data-hp="{{ $u->dataIndukTubel->noHandphone }}"
                                    data-alamat="{{ $u->dataIndukTubel->alamatTinggal }}"
                                    data-id_usulan_riset="{{ $u->id }}" data-topik_riset="{{ $u->topik->namaTopik }}"
                                    data-judul_riset="{{ $u->namaRiset }}" data-file_proposal="{{ $u->pathProposal }}"
                                    data-tgl_persetujuan_pt="{{ AppHelper::instance()->indonesian_date($u->tglPersetujuanPt, 'j F Y', '') }}"
                                    data-file_persetujuan_pt="{{ $u->pathDokumenPersetujuanPt }}">
                                <i class="fa fa-eye"></i>
                            </button>

                            <button class="btn btn-sm btn-primary btnKirimUsulanRiset" data-id="{{ $u->id }}"
                                    data-act="72b47eed-7ad2-4dca-bf5d-f3d05727c719">
                                <i class="fa fa-paper-plane"></i> Kirim
                            </button>

                        @elseif($u->logStatusProbis->output == 'Usulan topik disetujui')

                            <button class="btn btn-sm btn-primary btnPersetujuaniUsulanRiset" data-id="{{ $u->id }}">
                                <i class="fa fa-check"></i> Persetujuan
                            </button>

                        @else

                            <button class="btn btn-sm btn-info btn-icon btnLihatUsulanRiset" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Lihat" data-case="{{ $u->logStatusProbis->output }}"
                                    data-nama="{{ $u->dataIndukTubel->namaPegawai }}"
                                    data-nip="{{ $u->dataIndukTubel->nip18 }}"
                                    data-pangkat="{{ $u->dataIndukTubel->namaPangkat }}"
                                    data-jabatan="{{ $u->dataIndukTubel->namaJabatan }}"
                                    data-jenjang="{{ $u->dataIndukTubel->jenjangPendidikanId->jenjangPendidikan }}"
                                    data-program_beasiswa="{{ $u->dataIndukTubel->programBeasiswa }}"
                                    data-lokasi="{{ $u->dataIndukTubel->lokasiPendidikanId->lokasi }}"
                                    {{-- data-pt="{{ $u->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }}" --}} {{-- data-prodi="{{ $u->ptPesertaTubelId->programStudiId->programStudi }}" --}}
                                    data-st_tubel="{{ $u->dataIndukTubel->nomorSt }}"
                                    data-tgl_st_tubel="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tglSt, 'j F Y', '') }}"
                                    data-tgl_mulai="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tglMulaiSt, 'j F Y', '') }}"
                                    data-tgl_selesai="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tglSelesaiSt, 'j F Y', '') }}"
                                    data-kep_pembebasan="{{ $u->dataIndukTubel->nomorKepPembebasan }}"
                                    data-tgl_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tglKepPembebasan, 'j F Y', '') }}"
                                    data-tmt_kep_pembebasan="{{ AppHelper::instance()->indonesian_date($u->dataIndukTubel->tmtKepPembebasan, 'j F Y', '') }}"
                                    data-email="{{ $u->dataIndukTubel->emailNonPajak }}"
                                    data-hp="{{ $u->dataIndukTubel->noHandphone }}"
                                    data-alamat="{{ $u->dataIndukTubel->alamatTinggal }}"
                                    data-id_usulan_riset="{{ $u->id }}" data-topik_riset="{{ $u->topik->namaTopik }}"
                                    data-judul_riset="{{ $u->namaRiset }}" data-file_proposal="{{ $u->pathProposal }}"
                                    data-tgl_persetujuan_pt="{{ AppHelper::instance()->indonesian_date($u->tglPersetujuanPt, 'j F Y', '') }}"
                                    data-file_persetujuan_pt="{{ $u->pathDokumenPersetujuanPt }}"
                                    data-nomor_nd_persetujuan="{{ $u->nomorNdPersetujuanDjp }}"
                                    data-tgl_nd_persetujuan="{{ AppHelper::instance()->indonesian_date($u->tglPersetujuanDjp, 'j F Y', '') }}"
                                    data-dokumen_persetujuan_nd="{{ $u->pathNdPersetujuanDjp }}">
                                <i class="fa fa-eye"></i>
                            </button>

                        @endif


                    </td>
                </tr>

            @endforeach

        @else

            <tr class="text-center">
                <td class="text-dark  mb-1 fs-6" colspan="6">Tidak terdapat data</td>
            </tr>

        @endif

        </tbody>
    </table>
</div>
