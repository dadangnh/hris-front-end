<div class="table-responsive rounded border border-gray-300">
    <table class="table table-row-dashed table-row-gray-300 align-middle">
        <thead>
        <tr class="fw-bolder bg-secondary ">
            <th class="ps-4 min-w-150px">Semester</th>
            <th class="min-w-150px">IPK / Jumlah SKS</th>
            <th class="min-w-250px">Perguruan Tinggi / Program Studi</th>
            <th class="min-w-100px">Attachment</th>
            <th class="min-w-100px text-center">Status</th>
            <th class="min-w-100px text-center">Aksi</th>
        </tr>
        </thead>
        <tbody>
        @if ($lps != null)
            @foreach ($lps as $l)
                <tr id="lid{{ $l->id }}" class="">

                    <td class="ps-4 text-dark fs-6">
                        Semester {{ $l->semesterKe }}
                        <br>
                        @if ($l->flagSemester == 1)
                            Ganjil
                        @elseif ($l->flagSemester == 2)
                            Genap
                        @endif
                    </td>
                    <td class="text-dark fs-6">
                        {{ $l->ipk }} <br> {{ $l->jumlahSks }} SKS
                    </td>
                    <td class="text-dark fs-6">
                        {{ $l->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }}
                        <br> {{ $l->ptPesertaTubelId->programStudiId->programStudi }}
                    </td>

                    <td class="fs-6">
                        @if ($action == true)

                            @if ($l->logStatusProbis->output == 'Draft LPS dibuat' || $l->logStatusProbis->output == 'LPS dikembalikan')
                                <button class="btn btn-sm btn-primary2 btnUploadLps" data-idlps="{{ $l->id }}"
                                        data-urlkhs="{{ $l->pathKhs }}" data-urllps="{{ $l->pathLps }}">
                                    <i class="fa fa-upload"></i> Attachment
                                </button>
                            @else
                                <div class="col-sm">
                                    <div class="d-flex flex-aligns-center mt-2 pe-10">
                                        <img alt="" class="w-25px me-3"
                                             src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1">
                                            <a href="{{ $l->pathLps ? url('files/' . $l->pathLps) : '#' }}"
                                               target="_blank" class="fs-6 text-hover-primary">Laporan Perkembangan
                                                Studi.pdf</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <div class="d-flex flex-aligns-center mt-2 pe-10">
                                        <img alt="" class="w-25px me-3"
                                             src="{{ asset('assets/media') }}/svg/files/pdf.svg">
                                        <div class="ms-1">
                                            <a href="{{ $l->pathKhs ? url('files/' . $l->pathKhs) : '#' }}"
                                               target="_blank" class="fs-6 text-hover-primary">Kartu Hasil Studi.pdf</a>
                                        </div>
                                    </div>
                                </div>

                                {{-- <div class="col-sm">
                                    <a href="{{ $l->pathLps ? url('files/' . $l->pathLps) : '#' }}" target="_blank"><i class="fa fa-file-alt text-hover-primary"></i> Laporan Perkembangan Studi.pdf</a>
                                </div> --}}
                                {{-- <div class="col-sm">
                                    <a href="{{ $l->pathKhs ? url('files/' . $l->pathKhs) : '#' }}" target="_blank"><i class="fa fa-file-alt text-hover-primary"></i> Kartu Hasil Studi.pdf</a>
                                </div> --}}
                            @endif

                        @else
                            @if ($l->pathLps)
                                <div class="col-sm">
                                    <a href="{{ $l->pathLps ? url('files/' . $l->pathLps) : '#' }}" target="_blank"><i
                                            class="fa fa-file-alt text-hover-primary"></i> Laporan Perkembangan
                                        Studi</a>
                                </div>
                            @else
                                <span class="font-weight-bold text-danger fs-7">LPS belum diupload</span>
                                <br>
                            @endif

                            @if ($l->pathKhs)
                                <div class="col-sm">
                                    <a href="{{ $l->pathKhs ? url('files/' . $l->pathKhs) : '#' }}" target="_blank"><i
                                            class="fa fa-file-alt text-hover-primary"></i> Kartu Hasil Studi.pdf</a>
                                </div>
                            @else
                                <span class="font-weight-bold text-danger fs-7">KHS belum diupload</span>
                            @endif

                        @endif
                    </td>

                    <td class="text-center text-dark fs-6">
                        @if ($l->logStatusProbis->outputId->id == '27741cda-2ddf-4b98-8cfb-f9ba4ce3c9bd')
                            <span
                                class="badge badge-light-danger fs-7 fw-bolder">{{ $l->logStatusProbis->output }}</span>
                            <br>
                            Tiket:
                            <a href="javascript:void(0)" class="text-primary btnTiket"
                               data-tiket="{{ $l->logStatusProbis->nomorTiket }}"
                               data-nama="{{ $l->dataIndukTubelId->namaPegawai }}">
                                {{ $l->logStatusProbis->nomorTiket }}
                            </a>
                        @else
                            <span
                                class="badge badge-light-success fs-7 fw-bolder">{{ $l->logStatusProbis->output }}</span>
                            <br>
                            Tiket:
                            <a href="javascript:void(0)" class="text-primary btnTiket"
                               data-tiket="{{ $l->logStatusProbis->nomorTiket }}"
                               data-nama="{{ $l->dataIndukTubelId->namaPegawai }}">
                                {{ $l->logStatusProbis->nomorTiket }}
                            </a>
                        @endif
                    </td>
                    <td class="text-center">

                        <button class="btn btn-sm btn-icon btn-info btnLihatLps" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="Lihat" data-id="{{ $l->id }}"
                                data-pt="{{ $l->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }}"
                                data-smtke="{{ $l->semesterKe }}" data-smt="{{ $l->flagSemester }}"
                                data-th="{{ $l->tahunAkademik }}"
                                data-ipk="{{ $l->ipk }}" data-sks="{{ $l->jumlahSks }}" data-filelps="{{ $l->pathLps }}"
                                data-filekhs="{{ $l->pathKhs }}">
                            <i class="fa fa-eye"></i>
                        </button>

                        @if ($action == true)

                            @if ($l->logStatusProbis->output == 'Draft LPS dibuat' || $l->logStatusProbis->output == 'LPS dikembalikan')

                                <button class="btn btn-sm btn-icon btn-warning btnEditLps" data-id="{{ $l->id }}"
                                        data-uidpt="{{ $l->ptPesertaTubelId->id }}"
                                        data-idpt="{{ $l->ptPesertaTubelId->perguruanTinggiId->id }}"
                                        data-pttubel="{{ $l->ptPesertaTubelId->id }}"
                                        data-pt="{{ $l->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }}"
                                        data-smtke="{{ $l->semesterKe }}"
                                        data-smt="{{ $l->flagSemester }}" data-th="{{ $l->tahunAkademik }}"
                                        data-ipk="{{ $l->ipk }}" data-sks="{{ $l->jumlahSks }}" data-bs-toggle="tooltip"
                                        data-bs-placement="top"
                                        title="Edit">
                                    <i class="fa fa-edit"></i>
                                </button>

                                <button class="btn btn-sm btn-icon btn-danger btnHapusLps" data-id="{{ $l->id }}"
                                        data-url="{{ url('tubel/administrasi/' . $l->id . '/lps') }}"
                                        data-bs-toggle="tooltip" data-bs-placement="top"
                                        title="Hapus">
                                    <i class="fa fa-trash"></i>
                                </button>

                                <button class="btn btn-sm btn-primary btnKirimLps" data-bs-toggle="tooltip"
                                        data-bs-placement="top" title="Kirim" data-id="{{ $l->id }}"
                                        data-outputid="{{ $l->logStatusProbis->outputId->id ?? '' }}"
                                        data-output="{{ $l->logStatusProbis->outputId->caseOutput }}"
                                        data-url="{{ url('/tubel/administrasi/lps/kirim') }}">
                                    <i class="fa fa-paper-plane"></i> Kirim
                                </button>

                            @endif

                        @endif

                    </td>
                </tr>
            @endforeach
        @else
            <tr class="text-center">
                <td class="text-dark fs-6" colspan="6">Tidak terdapat data</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>
