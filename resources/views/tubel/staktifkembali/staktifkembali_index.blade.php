@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card mb-3 border-warning shadow-sm">
                    <div class="card-header card-header-stretch" id="card-header">
                        <div class="card-toolbar">
                            <ul class="nav nav-tabs nav-line-tabs nav-stretch border-transparent fs-5 fw-bolder"
                                id="tabLps">
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'rekamst' ? 'active' : '' }}"
                                       href="{{ url('tubel/st-aktif-kembali') }}"> Rekam ST </a>
                                </li>
                                <div class="d-flex align-items-center">
                                    <span id="nav-space" class="bullet h-25px w-1px"></span>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary {{ $tab == 'telah_direkam' ? 'active' : '' }}"
                                       href="{{ url('tubel/st-aktif-kembali/direkam') }}"> ST Telah Direkam </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'rekamst' ? 'active show' : '' }} " id="rekamst"
                                 role="tabpanel">
                                @if ($tab == 'rekamst')
                                    @include('tubel.staktifkembali.staktifkembali_tabel')
                                @endif
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade {{ $tab == 'telah_direkam' ? 'active show' : '' }} "
                                 id="telah_direkam" role="tabpanel">
                                @if ($tab == 'telah_direkam')
                                    @include('tubel.staktifkembali.staktifkembali_tabel')
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('tubel.staktifkembali.staktifkembali_modal')
    @include('tubel.modal_tiket')

@endsection


@section('js')
    <script>
        let app_url = '{{ env('APP_URL') }}'

        // advanced search
        $("#advancedSearch").submit(function (e) {
            e.preventDefault();

            var query = $(this).serializeArray().filter(function (i) {
                return i.value;
            });

            window.location.href = $(this).attr('action') + (query ? '?' + $.param(query) : '');

        })
    </script>

    <script src="{{ asset('assets/js/tubel') }}/st-aktif-kembali.js"></script>
    <script src="{{ asset('assets/js/tubel') }}/log-tiket.js"></script>

@endsection
