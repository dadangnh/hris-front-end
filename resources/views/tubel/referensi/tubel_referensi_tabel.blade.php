@php // $nomor = 1;
$halaman = isset($_GET['page']) ? (int) $_GET['page'] : 1;
$halaman_awal = $halaman > 1 ? $halaman * $size - $size : 0;
$nomor = $halaman_awal + 1;
@endphp

<div class="d-flex flex-column flex-lg-row">
    <div class="flex-column flex-lg-row-auto">
        <form action="" method="get">
            <div class="d-flex flex-column flex-xl-row">
                <div class="position-relative w-md-200px me-md-2">
                    <span class="svg-icon svg-icon-3 svg-icon-gray-500 position-absolute top-50 translate-middle ms-6">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"></rect>
                                <path
                                    d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z"
                                    fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                <path
                                    d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z"
                                    fill="#000000" fill-rule="nonzero"></path>
                            </g>
                        </svg>
                    </span>
                    <input type="text" class="form-control form-control-solid ps-10" name="cari"
                           value="{{ isset($_GET['cari']) ? $_GET['cari'] : '' }}" placeholder="Search">
                </div>
            </div>
        </form>
    </div>
    @if ($tab == 'referensi_negara' || $tab == 'referensi_pt' || $tab == 'referensi_prodi')
        <div class="flex-lg-row-fluid">
            <div class="text-end mb-4">
                <button data-bs-toggle="modal" data-bs-target="#add_ref" class="btn btn-primary btn-sm fs-6">
                    <i class="fa fa-plus"></i> Tambah
                </button>
            </div>
        </div>
    @endif
</div>


<div class="table-responsive bg-gray-400 rounded border border-gray-300">
    <table class="table table-striped align-middle" id="tabel_keppengaktifan_kembali">

        <thead class="fw-bolder bg-secondary fs-6">
        <tr class="">
            <th class="text-center ps-4">No</th>

            @if ($tab == 'referensi_negara')
                <th class="">Nama Negara</th>
            @endif
            @if ($tab == 'referensi_pt')
                <th class="">Nama Perguruan Tinggi</th>
            @endif
            @if ($tab == 'referensi_prodi')
                <th class="">Nama Program Studi</th>
            @endif
            @if ($tab == 'referensi_jenjang')
                <th class="">Nama Jenjang Pendidikan</th>
            @endif
            @if ($tab == 'referensi_lokasi')
                <th class="">Lokasi Pendidikan</th>
            @endif

            <th class="text-center">Aksi</th>
        </tr>
        </thead>
        <tbody>

        {{-- Referensi Negara --}}
        @if ($tab == 'referensi_negara')
            @if ($negara->data != null)

                @foreach ($negara->data as $p)
                    <tr id="{{ $p->id }}" class="text-right">

                        <td class="text-dark text-center ps-4">
                            {{ $nomor++ }}
                        </td>

                        <td class="text-dark fs-6 ps-4">
                            {{ $p->namaNegara }}
                        </td>

                        <td class="text-center text-dark mb-1 fs-6">
                            <button class="btn btn-icon btn-sm btn-warning btnEdit" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Edit" data-id_ref="{{ $p->id }}"
                                    data-nama_ref="{{ $p->namaNegara }}">
                                <i class="fa fa-edit"></i>
                            </button>

                            <button class="btn btn-icon btn-sm btn-danger btnHapus" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Hapus"
                                    data-id_ref_negara_hapus="{{ $p->id }}">
                                <i class="fa fa-trash"></i>
                            </button>

                        </td>
                    </tr>
                @endforeach
            @else
                <tr class="text-center">
                    <td class="text-dark mb-1 fs-6" colspan="7">Tidak terdapat data</td>
                </tr>
            @endif
        @endif

        {{-- Referensi Perguruan Tinggi --}}
        @if ($tab == 'referensi_pt')
            @if ($pt->data != null)

                @foreach ($pt->data as $p)
                    <tr id="{{ $p->id }}" class="text-right">

                        <td class="text-dark text-center ps-4">
                            {{ $nomor++ }}
                        </td>

                        <td class="text-dark fs-6 ps-4">
                            {{ $p->namaPerguruanTinggi }}
                        </td>

                        <td class="text-center text-dark mb-1 fs-6">

                            <button class="btn btn-icon btn-sm btn-warning btnEdit" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Edit" data-id_ref="{{ $p->id }}"
                                    data-nama_ref="{{ $p->namaPerguruanTinggi }}">
                                <i class="fa fa-edit"></i>
                            </button>

                            <button class="btn btn-icon btn-sm btn-danger btnHapusPT" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Hapus"
                                    data-id_ref_negara_hapus="{{ $p->id }}">
                                <i class="fa fa-trash"></i>
                            </button>

                        </td>
                    </tr>
                @endforeach
            @else
                <tr class="text-center">
                    <td class="text-dark mb-1 fs-6" colspan="7">Tidak terdapat data</td>
                </tr>
            @endif
        @endif

        {{-- Referensi Program Studi --}}
        @if ($tab == 'referensi_prodi')
            @if ($prodi->data != null)

                @foreach ($prodi->data as $p)
                    <tr id="{{ $p->id }}" class="text-right">

                        <td class="text-dark text-center ps-4">
                            {{ $nomor++ }}
                        </td>

                        <td class="text-dark fs-6 ps-4">
                            {{ $p->programStudi }}
                        </td>

                        <td class="text-center text-dark mb-1 fs-6">

                            <button class="btn btn-icon btn-sm btn-warning btnEdit" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Edit" data-id_ref="{{ $p->id }}"
                                    data-nama_ref="{{ $p->programStudi }}">
                                <i class="fa fa-edit"></i>
                            </button>

                            <button class="btn btn-icon btn-sm btn-danger btnHapusProdi" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Hapus"
                                    data-id_ref_prodi_hapus="{{ $p->id }}">
                                <i class="fa fa-trash"></i>
                            </button>

                        </td>
                    </tr>
                @endforeach
            @else
                <tr class="text-center">
                    <td class="text-dark mb-1 fs-6" colspan="7">Tidak terdapat data</td>
                </tr>
            @endif
        @endif

        {{-- Referensi Jenjang Pendidikan --}}
        @if ($tab == 'referensi_jenjang')
            @if ($jenjang->data != null)

                @foreach ($jenjang->data as $p)
                    <tr id="{{ $p->id }}" class="text-right">

                        <td class="text-dark text-center ps-4">
                            {{ $nomor++ }}
                        </td>

                        <td class="text-dark fs-6 ps-4">
                            {{ $p->jenjangPendidikan }}
                        </td>

                        <td class="text-center text-dark mb-1 fs-6">

                            <button class="btn btn-icon btn-sm btn-warning btnEdit" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Edit" data-id_ref="{{ $p->id }}"
                                    data-nama_ref="{{ $p->jenjangPendidikan }}">
                                <i class="fa fa-edit"></i>
                            </button>

                            <button class="btn btn-icon btn-sm btn-danger btnHapusJenjang" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Hapus"
                                    data-id_ref_jenjang_hapus="{{ $p->id }}">
                                <i class="fa fa-trash"></i>
                            </button>

                        </td>
                    </tr>
                @endforeach
            @else
                <tr class="text-center">
                    <td class="text-dark mb-1 fs-6" colspan="7">Tidak terdapat data</td>
                </tr>
            @endif
        @endif

        {{-- Referensi Lokasi Pendidikan --}}
        @if ($tab == 'referensi_lokasi')
            @if ($lokasi->data != null)

                @foreach ($lokasi->data as $p)
                    <tr id="{{ $p->id }}" class="text-right">

                        <td class="text-dark text-center ps-4">
                            {{ $nomor++ }}
                        </td>

                        <td class="text-dark fs-6 ps-4">
                            {{ $p->lokasi }}
                        </td>

                        <td class="text-center text-dark mb-1 fs-6">

                            <button class="btn btn-icon btn-sm btn-warning btnEdit" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Edit" data-id_ref="{{ $p->id }}"
                                    data-nama_ref="{{ $p->lokasi }}">
                                <i class="fa fa-edit"></i>
                            </button>

                            <button class="btn btn-icon btn-sm btn-danger btnHapusLokasi" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Hapus"
                                    data-id_ref_lokasi_hapus="{{ $p->id }}">
                                <i class="fa fa-trash"></i>
                            </button>

                        </td>
                    </tr>
                @endforeach
            @else
                <tr class="text-center">
                    <td class="text-dark mb-1 fs-6" colspan="7">Tidak terdapat data</td>
                </tr>
            @endif
        @endif


        </tbody>
    </table>

</div>

@if ($totalItems != 0)
    <!--begin::pagination-->
    <div class="d-flex flex-stack flex-wrap pt-5 p-3">
        <div class="fs-6 fw-bold text-gray-700"></div>

        @php
            $disabled = '';
            $nextPage = '';
            $disablednext = '';
            $hidden = '';
            $pagination = '';

            // tombol pagination
            if ($totalItems == 0) {
                // $pagination = 'none';
            }

            // tombol back
            if ($currentPage == 1) {
                $disabled = 'disabled';
            }

            // tombol next
            if ($currentPage != $totalPages) {
                $nextPage = $currentPage + 1;
            }

            // tombol pagination
            if ($currentPage == $totalPages || $totalPages == 0) {
                $disablednext = 'disabled';
                $hidden = 'hidden';
            }

        @endphp

            <!--begin::Pages-->
        <ul class="pagination" style="display: {{ $pagination }}">
            <li class="page-item disabled"><a href="#" class="page-link">Total {{ $totalItems }} data, Halaman
                    {{ $currentPage }} dari {{ $totalPages }} </a></li>
            <li class="page-item previous {{ $disabled }}"><a
                    href="{{ url()->current() . '?page=' . ($currentPage - 1) }}" class="page-link"><i
                        class="previous"></i></a></li>
            <li class="page-item active"><a href="{{ url()->current() . '?page=' . $currentPage }}"
                                            class="page-link">{{ $currentPage }}</a></li>
            <li class="page-item" {{ $hidden }}><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                   class="page-link">{{ $nextPage }}</a></li>
            <li class="page-item next {{ $disablednext }}"><a href="{{ url()->current() . '?page=' . $nextPage }}"
                                                              class="page-link"><i class="next"></i></a></li>
        </ul>
        <!--end::Pages-->

    </div>
    <!--end::paging-->
@endif
