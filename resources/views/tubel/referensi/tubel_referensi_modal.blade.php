{{-- modal tambah referensi --}}
<div class="modal fade" id="add_ref" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Form Penambahan Data Referensi</h3>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form" method="post" enctype="multipart/form-data"
                  @if ($tab == 'referensi_negara')
                      action="{{ url('/tubel/referensi/tambah') }}"
                  @endif

                  @if ($tab == 'referensi_pt')
                      action="{{ url('/tubel/referensi/pt/tambah') }}"
                  @endif

                  @if ($tab == 'referensi_prodi')
                      action="{{ url('/tubel/referensi/prodi/tambah') }}"
                @endif
            >
                @csrf

                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Nama</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="nama_ref" name="nama_ref" rows="4"
                                   placeholder="isi nama referensi" required></textarea>
                        </div>
                    </div>

                </div>

                <div class="text-center mb-8">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                            class="fa fa-reply"></i>Batal
                    </button>
                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- modal edit referensi --}}
<div class="modal fade" id="edit_dasar_ref" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content rounded">
            <div class="modal-header">
                <div class="modal-title">
                    <h3 class="text-white">Edit Data Referensi</h3>
                </div>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                               fill="#000000">
                                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                <rect fill="#000000" opacity="0.5"
                                      transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                      x="0" y="7" width="16" height="2" rx="1"/>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <form class="form" method="post" enctype="multipart/form-data"
                  @if ($tab == 'referensi_negara')
                      action="{{ url('/tubel/referensi/update_negara') }}"
                  @endif

                  @if ($tab == 'referensi_pt')
                      action="{{ url('/tubel/referensi/pt/update_pt') }}"
                  @endif

                  @if ($tab == 'referensi_prodi')
                      action="{{ url('/tubel/referensi/prodi/update_prodi') }}"
                @endif
            >
                @csrf
                <input type="hidden" id="id_ref_edit" name="id_ref_edit">

                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="row mb-3">
                        <label class="col-lg-3 col-form-label fw-bold fs-6">Nama</label>
                        <div class="col-lg">
                            <input class="form-control" type="text" id="nama_ref_edit" name="nama_ref_edit" rows="4"
                                   placeholder="Edit nama yang akan diubah" required></textarea>
                        </div>
                    </div>

                </div>

                <div class="text-center mb-8">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i
                            class="fa fa-reply"></i>Batal
                    </button>
                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

