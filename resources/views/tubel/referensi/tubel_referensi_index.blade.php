@extends('layout.master')

@section('content')

    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="row g-5 g-xxl-8">
                <div class="col-sm-4">
                    <div class="col-xl-12">
                        <div class="card card-sm-stretch mb-5 mb-xl-8 border-warning shadow-sm">
                            <div class="card-header card-header-stretch" id="card-header">
                                <div class="card-title fw-bold fs-4 text-white">
                                    Daftar Referensi
                                </div>
                            </div>
                            <div class="card-body" style="
                                    padding-top: 15px;
                                    padding-bottom: 2px;
                                ">
                                <div class="flex-column flex-lg-row-auto w-100 w-lg-275px mb-10 me-lg-20">
                                    <div class="mb-1">
                                        <div
                                            class="menu menu-rounded menu-column menu-title-gray-700 menu-state-title-primary menu-active-bg-light-primary fw-bold">
                                            <div class="menu-item mb-1">
                                                <a href="{{ url('/tubel/referensi/') }}"
                                                   class="menu-link py-3 {{ $tab == 'referensi_negara' ? 'active' : '' }}">Referensi
                                                    Negara</a>
                                            </div>
                                            <div class="menu-item mb-1">
                                                <a href="{{ url('/tubel/referensi/pt') }}"
                                                   class="menu-link py-3 {{ $tab == 'referensi_pt' ? 'active' : '' }}">Referensi
                                                    Perguruan Tinggi</a>
                                            </div>
                                            <div class="menu-item mb-1">
                                                <a href="{{ url('/tubel/referensi/prodi') }}"
                                                   class="menu-link py-3 {{ $tab == 'referensi_prodi' ? 'active' : '' }}">Referensi
                                                    Program Studi</a>
                                            </div>
                                            <div class="menu-item mb-1">
                                                <a href="{{ url('/tubel/referensi/jenjang') }}"
                                                   class="menu-link py-3 {{ $tab == 'referensi_jenjang' ? 'active' : '' }}">Referensi
                                                    Jenjang Pendidikan</a>
                                            </div>
                                            <div class="menu-item mb-1">
                                                <a href="{{ url('/tubel/referensi/lokasi') }}"
                                                   class="menu-link py-3 {{ $tab == 'referensi_lokasi' ? 'active' : '' }}">Referensi
                                                    Lokasi Pendidikan</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8">
                    <div class="col-lg-12">
                        <div class="card mb-3 border-warning shadow-sm">
                            <div class="card-header card-header-stretch" id="card-header">
                                <div class="card-title fw-bold fs-4 text-white">
                                    @if ($tab == 'referensi_negara')
                                        Referensi Negara
                                    @elseif ($tab == 'referensi_pt')
                                        Referensi Perguruan Tinggi
                                    @elseif ($tab == 'referensi_prodi')
                                        Referensi Program Studi
                                    @elseif ($tab == 'referensi_jenjang')
                                        Referensi Jenjang Pendidikan
                                    @elseif ($tab == 'referensi_lokasi')
                                        Referensi Lokasi Pendidikan
                                    @endif
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade {{ $tab == 'referensi_negara' ? 'active show' : '' }} "
                                         id="ref_negara" role="tabpanel">
                                        @if ($tab == 'referensi_negara')
                                            @include('tubel.referensi.tubel_referensi_tabel')
                                        @endif
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane fade {{ $tab == 'referensi_pt' ? 'active show' : '' }} "
                                         id="ref_pt" role="tabpanel">
                                        @if ($tab == 'referensi_pt')
                                            @include('tubel.referensi.tubel_referensi_tabel')
                                        @endif
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane fade {{ $tab == 'referensi_prodi' ? 'active show' : '' }} "
                                         id="ref_prodi" role="tabpanel">
                                        @if ($tab == 'referensi_prodi')
                                            @include('tubel.referensi.tubel_referensi_tabel')
                                        @endif
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane fade {{ $tab == 'referensi_jenjang' ? 'active show' : '' }} "
                                         id="ref_jenjang" role="tabpanel">
                                        @if ($tab == 'referensi_jenjang')
                                            @include('tubel.referensi.tubel_referensi_tabel')
                                        @endif
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane fade {{ $tab == 'referensi_lokasi' ? 'active show' : '' }} "
                                         id="ref_lokasi" role="tabpanel">
                                        @if ($tab == 'referensi_lokasi')
                                            @include('tubel.referensi.tubel_referensi_tabel')
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('tubel.referensi.tubel_referensi_modal')

@endsection

@section('js')
    <script src="{{ asset('assets/js/tubel') }}/referensi.js"></script>
    <script src="{{ asset('assets/js/tubel') }}/log-tiket.js"></script>
@endsection
