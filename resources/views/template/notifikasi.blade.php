@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <div class="row">
                <div class="card card-xl-stretch mb-xl-8">
                    <!--begin::Body-->
                    <div class="card-body fs-6 py-15 px-10 py-lg-15 px-lg-15 text-gray-700">
                        <!--begin::Section-->
                        <div class="">
                            <!--begin::Heading-->
                            <h1 class="anchor fw-bolder mb-5">
                                <a href="#notifikasi"></a>Notifikasi
                            </h1>
                            <!--end::Heading-->
                            <!--begin::Block-->
                            <div class="py-5">
                                <ul class="py-0">
                                    <li class="py-2">Menggunakan library
                                        <code>SweetAlert 2,</code> dokumentasi ada di <a
                                            href="https://sweetalert2.github.io/">
                                            <code>https://sweetalert2.github.io/</code>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!--end::Block-->
                        </div>
                        <!--end::Section-->
                        <!--begin::Section-->
                        <div class="">
                            <!--begin::Demo-->
                            <div class="py-1">
                                <div class="rounded border p-10">
                                    <button type="button" class="btn btn-primary" onclick="success()">Notifikasi
                                        sukses
                                    </button>
                                </div>
                            </div>
                            <div class="py-1">
                                <div class="rounded border p-10">
                                    <button type="button" class="btn btn-warning" onclick="confirm()">Notifikasi
                                        konfirmasi
                                    </button>
                                </div>
                            </div>
                            <div class="py-1">
                                <div class="rounded border p-10">
                                    <button type="button" class="btn btn-danger" onclick="fail()">Notifikasi gagal
                                    </button>
                                </div>
                            </div>
                            <div class="py-1">
                                <div class="rounded border p-10">
                                    <button type="button" class="btn btn-secondary" onclick="info()">Notifikasi info
                                    </button>
                                </div>
                            </div>
                            <!--end::Demo-->

                            {{-- <!--begin::Code-->
                            <div class="py-5">
                                <!--begin::Highlight-->
                                <div class="highlight">
                                    <button class="highlight-copy btn" data-bs-toggle="tooltip" title=""
                                        data-bs-original-title="Copy code">copy</button>
                                    <ul class="nav nav-pills" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-bs-toggle="tab"
                                                href="#kt_highlight_61040e7021323" role="tab">JAVASCRIPT</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-bs-toggle="tab" href="#kt_highlight_61040e7021329"
                                                role="tab">HTML</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade show active" id="kt_highlight_61040e7021323"
                                            role="tabpanel">
                                            <div class="highlight-code">
                                                <pre class=" language-javascript"
                                                    tabindex="0"><code class=" language-javascript"><span class="token function">$</span><span class="token punctuation">(</span><span class="token string">"#kt_datepicker_1"</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">flatpickr</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

                                                        <span class="token function">$</span><span class="token punctuation">(</span><span class="token string">"#kt_datepicker_2"</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">flatpickr</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span></code></pre>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="kt_highlight_61040e7021329" role="tabpanel">
                                            <div class="highlight-code">
                                                <pre class=" language-html"
                                                    tabindex="0"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>mb-10<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
                                                    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>label</span> <span class="token attr-name">for</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span><span class="token punctuation">"</span></span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>form-label<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Default input style<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>label</span><span class="token punctuation">&gt;</span></span>
                                                    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>input</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>form-control<span class="token punctuation">"</span></span> <span class="token attr-name">placeholder</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>Pick a date<span class="token punctuation">"</span></span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>kt_datepicker_1<span class="token punctuation">"</span></span><span class="token punctuation">/&gt;</span></span>
                                                    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>

                                                    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>mb-0<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
                                                    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>label</span> <span class="token attr-name">for</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span><span class="token punctuation">"</span></span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>form-label<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Solid background style<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>label</span><span class="token punctuation">&gt;</span></span>
                                                    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>input</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>form-control form-control-solid<span class="token punctuation">"</span></span> <span class="token attr-name">placeholder</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>Pick a date<span class="token punctuation">"</span></span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>kt_datepicker_2<span class="token punctuation">"</span></span><span class="token punctuation">/&gt;</span></span>
                                                    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Highlight-->
                            </div>
                            <!--end::Code--> --}}
                        </div>
                        <!--end::Section-->
                    </div>
                    <!--end::Body-->
                </div>

            </div>
        </div>
        <!--end::Container-->
    </div>

@endsection


@section('js')
    <script>
        // let timerInterval
        // Swal.fire({
        //     title: 'Notifikasi',
        //     html: 'library SweetAlert2',
        //     timer: 2000,
        //     timerProgressBar: true,
        //     willClose: () => {
        //         clearInterval(timerInterval)
        //     }
        // }).then((result) => {
        //     /* Read more about handling dismissals below */
        //     if (result.dismiss === Swal.DismissReason.timer) {
        //         console.log('I was closed by the timer')
        //     }
        // })


        function success() {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Data berhasil disimpan!<br>',
                showConfirmButton: false,
                // width: 600,
                // padding: '5em',
                timer: 1200
            })
        }

        function confirm() {
            Swal.fire({
                title: 'Yakin akan menghapus data?',
                text: "Data tidak dapat dikembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Data berhasil dihapus!',
                        showConfirmButton: false,
                        // width: 600,
                        // padding: '5em',
                        timer: 1200
                    })
                }
            })
        }

        function fail() {
            Swal.fire({
                icon: 'error',
                title: 'Data gagal tersimpan!',
            })
        }

        function info() {
            Swal.fire(
                'Butuh bantuan?',
                'silahkan hubungi .......',
                'question'
            )
        }
    </script>
@endsection
