@extends('layout_new.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <div class="row">
                <div class="col-lg-3">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="mb-4 card border-warning shadow-sm">
                                <div class="card-header" id="card-header">
                                    <h3 class="card-title">
                                        <span class="fw-bold fs-2 text-white">Tugas</span>
                                    </h3>
                                </div>
                                <div class="card-body">
                                    <div class="card-title h5">Daftar Tugas Saya</div>
                                    <p class="card-text">Some quick example text to build on the card title and make up
                                        the
                                        bulk of
                                        the content.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-6">
                            <div class="mb-4 card border-warning shadow-sm">
                                <div class="card-header" id="card-header">
                                    <h3 class="card-title">
                                        <span class="fw-bold fs-2 text-white">Agenda</span>
                                    </h3>
                                </div>
                                <div class="card-body">
                                    <div class="list-group list-group-flush">
                                        <div class="list-group-item">
                                            <div class="row">
                                                <div class="col-8 text-left"><a href="#">Pengelolaan Kinerja</a></div>
                                                <div class="h5">28 Feb 21</div>
                                            </div>
                                        </div>
                                        <div class="list-group-item">
                                            <div class="row">
                                                <div class="col-8 text-left"><a href="#">Sosialisasi</a></div>
                                                <div class="h5">3 Mar 21</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-6">
                            <div class="mb-4 card border-warning shadow-sm">
                                <div class="card-header" id="card-header">
                                    <h3 class="card-title">
                                        <span class="fw-bold fs-2 text-white">Jejaring</span>
                                    </h3>
                                </div>
                                <div class="card-body">
                                    <div class="list-group list-group-flush">
                                        <div class="list-group-item">
                                            <div class="row">
                                                <div class="col-11"><a href="#">Pegawai Ulang Tahun</a></div>
                                                <div>128</div>
                                            </div>
                                        </div>
                                        <div class="list-group-item">
                                            <div class="row">
                                                <div class="col-11"><a href="#">Berita Duka</a></div>
                                                <div>1</div>
                                            </div>
                                        </div>
                                        <div class="list-group-item">
                                            <div class="row">
                                                <div class="col-11"><a href="#">Pegawai Terdekat</a></div>
                                                <div>20</div>
                                            </div>
                                        </div>
                                        <div class="list-group-item">
                                            <div class="row">
                                                <div class="col-11"><a href="#">Pernah Ketemu</a></div>
                                                <div>512</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card border-warning shadow-sm">
                        <div class="card-header" id="card-header">
                            <h3 class="card-title">
                                <span class="fw-bold fs-2 text-white">Berita</span>
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="list-group list-group-flush">
                                <div class="list-group-item">
                                    <div class="row">
                                        <div>1.</div>
                                        <div class="col-10 mx-2">Pengumuman : ND-6820/PJ.01/2020<br><a href="#">Pengumuman
                                                Rencana
                                                Umum Pengadaan (RUP) Tahun Anggaran 2021</a></div>
                                        <div>30 Jan 2021</div>
                                    </div>
                                </div>
                                <div class="list-group-item">
                                    <div class="row">
                                        <div>2.</div>
                                        <div class="col-10 mx-2">Pengumuman : ND-6820/PJ.01/2020<br><a href="#">Pengumuman
                                                Rencana
                                                Umum Pengadaan (RUP) Tahun Anggaran 2021</a></div>
                                        <div>30 Jan 2021</div>
                                    </div>
                                </div>
                                <div class="list-group-item">
                                    <div class="row">
                                        <div>3.</div>
                                        <div class="col-10 mx-2">Pengumuman : ND-6820/PJ.01/2020<br><a href="#">Pengumuman
                                                Rencana
                                                Umum Pengadaan (RUP) Tahun Anggaran 2021</a></div>
                                        <div>30 Jan 2021</div>
                                    </div>
                                </div>
                                <div class="list-group-item">
                                    <div class="row">
                                        <div>4.</div>
                                        <div class="col-10 mx-2">Pengumuman : ND-6820/PJ.01/2020<br><a href="#">Pengumuman
                                                Rencana
                                                Umum Pengadaan (RUP) Tahun Anggaran 2021</a></div>
                                        <div>30 Jan 2021</div>
                                    </div>
                                </div>
                                <div class="list-group-item">
                                    <div class="row">
                                        <div>5.</div>
                                        <div class="col-10 mx-2">Pengumuman : ND-6820/PJ.01/2020<br><a href="#">Pengumuman
                                                Rencana
                                                Umum Pengadaan (RUP) Tahun Anggaran 2021</a></div>
                                        <div>30 Jan 2021</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="my-4 card border-warning shadow-sm">
                        <div class="card-header" id="card-header">
                            <h3 class="card-title">
                                <span class="fw-bold fs-2 text-white">Beasiswa</span>
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="list-group list-group-flush">
                                <div class="list-group-item">
                                    <div class="row">
                                        <div>1.</div>
                                        <div class="col-10 mx-2">Tk. Pasca Sarjana (S2)<br><a href="#">Beasiswa
                                                Pascasarjana
                                                (S2)
                                                University of Tsukuba 2021</a></div>
                                        <div>30 Jan 2021</div>
                                    </div>
                                </div>
                                <div class="list-group-item">
                                    <div class="row">
                                        <div>2.</div>
                                        <div class="col-10 mx-2">Tk. Pasca Sarjana (S2)<br><a href="#">Beasiswa
                                                Pascasarjana
                                                (S2)
                                                University of Tsukuba 2021</a></div>
                                        <div>30 Jan 2021</div>
                                    </div>
                                </div>
                                <div class="list-group-item">
                                    <div class="row">
                                        <div>3.</div>
                                        <div class="col-10 mx-2">Tk. Pasca Sarjana (S2)<br><a href="#">Beasiswa
                                                Pascasarjana
                                                (S2)
                                                University of Tsukuba 2021</a></div>
                                        <div>30 Jan 2021</div>
                                    </div>
                                </div>
                                <div class="list-group-item">
                                    <div class="row">
                                        <div>4.</div>
                                        <div class="col-10 mx-2">Tk. Pasca Sarjana (S2)<br><a href="#">Beasiswa
                                                Pascasarjana
                                                (S2)
                                                University of Tsukuba 2021</a></div>
                                        <div>30 Jan 2021</div>
                                    </div>
                                </div>
                                <div class="list-group-item">
                                    <div class="row">
                                        <div>5.</div>
                                        <div class="col-10 mx-2">Tk. Pasca Sarjana (S2)<br><a href="#">Beasiswa
                                                Pascasarjana
                                                (S2)
                                                University of Tsukuba 2021</a></div>
                                        <div>30 Jan 2021</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="row">
                        <div class="col-lg-12 col-md-6">
                            <div class="mb-4 card border-warning shadow-sm">
                                <div class="card-header" id="card-header">
                                    <h3 class="card-title">
                                        <span class="fw-bold fs-2 text-white">Selamat datang</span>
                                    </h3>
                                </div>
                                <div class="text-center p-4 card-body"><img
                                        src="{{ url('assets/dist') }}/assets/media/avatars/blank.png" alt="User Avatar"
                                        class="rounded-circle img-thumbnail" style="width: 150px; height: 150px;">
                                    <div class="my-2 card-title h5">{{ session('user')['namaPegawai'] }}</div>
                                    <p class="card-text">{{ session('user')['jabatan'] }}
                                        <br>{{ session('user')['unit'] }}
                                    </p>
                                </div>
                                <div class="text-center card-footer">
                                    <div class="row">
                                        <div class="col">Kinerja<h3 class="text-success">92%</h3>
                                        </div>
                                        <div class="col">Jam Kerja<h3 class="text-success">98%</h3>
                                        </div>
                                        <div class="col">Jamlat<h3 class="text-success">85%</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-6">
                            <div class="text-center card border-warning shadow-sm">
                                <div class="card-header" id="card-header">
                                    <h3 class="card-title">
                                        <span class="fw-bold fs-2 text-white">Favourite</span>
                                    </h3>
                                </div>
                                <div class="list-group list-group-flush"><a href="/layananmandiri">
                                        <button
                                            class="list-group-item list-group-item-action">Layanan Mandiri
                                        </button>
                                    </a><a
                                        href="#link2" data-rb-event-key="#link2"
                                        class="list-group-item list-group-item-action">Control Panel UPK</a><a
                                        href="#link3"
                                        data-rb-event-key="#link3" class="list-group-item list-group-item-action">Pengajuan
                                        Cuti</a><a href="#link4" data-rb-event-key="#link4"
                                                   class="list-group-item list-group-item-action">Monitoring
                                        Presensi</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>



@endsection
