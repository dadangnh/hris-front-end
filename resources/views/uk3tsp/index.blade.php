@extends('layout_new.master')
<?php
// dump($usulanJKU);
?>
@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <!--begin::Card-->

            <div class="card mb-3 border-warning shadow-sm">
                <div id="card-header" class="card-header">
                    <div class="card-title fw-bold fs-4 text-white">
                        Administrasi UK3TSP
                    </div>
                </div>
                <!--end::Card title-->

                <!--begin::Card toolbar-->
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6 mb-4">
                            <div class="text-start" data-kt-customer-table-toolbar="base">
                                <button type="button" class="btn btn-light-primary  btn-active-primary"
                                        data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start"
                                        data-kt-menu-flip="top-end">
                                    <i class="fa fa-filter"></i> Advance Search
                                </button>
                                <div class="menu menu-sub menu-sub-dropdown w-md-800px border-warning mt-1"
                                     data-kt-menu="true">
                                    <div class="px-7 py-5">
                                        <div class="fs-4 text-dark fw-bolder">Filter Pencarian</div>
                                    </div>
                                    <div class="separator border-gray-200"></div>
                                    <form id="advancedSearch" action="" method="get">
                                        <?php
                                        if (isset($_GET['pmk'])) {
                                            $searchValue = $_GET['pmk'];
                                        } else if (isset($_GET['nomorTicket'])) {
                                            $searchValue = $_GET['nomorTicket'];
                                        } else if (isset($_GET['keterangan'])) {
                                            $searchValue = $_GET['keterangan'];
                                        } else if (isset($_GET['tanggal'])) {
                                            $searchValue = $_GET['tanggal'];
                                        } else {
                                            $searchValue = "";
                                        }
                                        ?>
                                        <div class="px-7 py-5">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <label class="form-label fw-bold">Nomor PMK:</label>
                                                    <div class="row mb-4">
                                                        <div class="col-lg-12">
                                                            <input class="form-control form-select-solid" type="text"
                                                                   name="pmk" placeholder="Nomor PMK"
                                                                   value="{{ isset($_GET['pmk']) ? $_GET['pmk'] : '' }}">
                                                        </div>
                                                    </div>
                                                    <label class="form-label fw-bold">Nomor Ticket:</label>
                                                    <div class="row mb-4">
                                                        <div class="col-lg-12">
                                                            <input class="form-control form-select-solid" type="text"
                                                                   name="nomorTicket" placeholder="Nomor Ticket"
                                                                   value="{{ isset($_GET['nomorTicket']) ? $_GET['nomorTicket'] : '' }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="form-label fw-bold">Keterangan:</label>
                                                    <div class="row mb-4">
                                                        <div class="col-lg-12">
                                                            <input class="form-control form-select-solid" type="text"
                                                                   name="keterangan" placeholder="keterangan"
                                                                   value="{{ isset($_GET['keterangan']) ? $_GET['keterangan'] : '' }}">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-end">
                                                <a href="{{ url()->current() }}"
                                                   class="btn btn-light btn-active-light-primary me-2">Reset</a>
                                                <button type="submit" class="btn btn-sm btn-primary"
                                                        data-kt-menu-dismiss="true"
                                                        data-kt-customer-table-filter="filter" id="searchData"
                                                        value="{{ $searchValue }}">Search
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="text-end mb-2">
                                <button type="button" class="btn btn-light-primary btnTambahPermohonan"
                                        data-bs-toggle="modal" data-bs-target="#kt_modal_permohonan">
                                    <!--begin::Svg Icon | path: icons/duotone/Interface/Plus-Square.svg-->

                                    <!--end::Svg Icon-->
                                    <i class="fa fa-plus"></i>
                                    Tambah UK3TSP
                                </button>

                            </div>
                        </div>
                    </div>
                    <!--end::Card toolbar-->
                </div>
                <!--end::Card header-->

                <!--begin::Card body-->
                <div class="card-body pt-0">
                    <!--begin::Table-->
                    <div class="table-responsive bg-gray-200 rounded border border-gray-300">
                        <table class="table align-middle table-striped fs-6 gy-5 mb-0" id="tabel_tubel"

                            <!--begin::Table head-->
                            <thead>
                            <!--begin::Table row-->
                            <tr class="text-start text-white fw-bolder fs-7 text-uppercase gs-0"
                                style="background-color:#02275d";">
                                <th class="min-w-50px text-center">No</th>
                                <th class="min-w-50px text-center">Nomor Ticket</th>
                                <th class="min-w-100px">PMK</th>
                                <th class="min-w-100px">Tanggal PMK</th>
                                <th class="text-center min-w-50px">Daftar Kantor</th>
                                {{-- <th class="text-center min-w-50px">Kantor</th> --}}
                                <th class="text-center min-w-120px">Keterangan</th>
                                <th class="text-center min-w-100px">Status</th>
                                <th class="text-center min-w-120px">Actions</th>
                            </tr>
                            <!--end::Table row-->
                            </thead>
                            <!--end::Table head-->
                            <!--begin::Table body-->
                            <tbody class="fw-bold text-gray-600">
                            <?php $no = $numberOfElements; ?>
                            @foreach ($usulanJKU as $usulan)

                                <tr id="libur{{$usulan->id}}">
                                    <td class="text-center">{{ $no }}</td>
                                    <td>{{ $usulan->nomorTicket }}</td>
                                    <td>{{ $usulan->pmk }}</td>
                                    <td>
                                        {{ AppHelper::convertDateDMY($usulan->tanggal) }}
                                    </td>
                                    @if(isset($usulan->kantor ))
                                            <?php
                                            $kantorId = "";
                                            foreach ($usulan->kantor as $item) {
                                                if ('' == $kantorId) {
                                                    $kantorId .= $item->nama;
                                                } else {
                                                    $kantorId .= "|" . $item->nama;
                                                }
                                            }
                                            ?>
                                    @endif
                                    <td class="text-center listKantor"
                                        data-bs-toggle="modal" data-bs-target="#kt_modal_1"
                                        data-id-usulan="{{ $usulan->id }}"
                                        data-pmk="{{ $usulan->pmk }}"
                                        data-kantor="{{ (isset($kantorId)) ? $kantorId : "" }}">
                                        <a href="#"><u>List Kantor<u>

                                    </td>
                                    {{-- <td>
                                        @if(isset($usulan->kantor ))
                                            @foreach ($usulan->kantor as $item)
                                        <li class="d-flex align-items-center py-1">
                                            <span class="bullet bg-warning me-3"></span>

                                            {{ (isset($item->nama)) ?  $item->nama : ''}}<br>
                                            @endforeach
                                        @endif
                                        </li>
                                    </td> --}}
                                    <td>
                                        {{ $usulan->keterangan }}
                                    </td>
                                    <td>
                                        @if((1 == $usulan->status))
                                            <span class="badge badge-light-warning">Menunggu Persetujuan</span>
                                        @elseif ((2 == $usulan->status))
                                            <span class="badge badge-light-success">Disetujui</span>
                                        @elseif ((0 == $usulan->status))
                                            <span class="badge badge-light-danger">Draft</span>
                                        @elseif ((3 == $usulan->status))
                                            <span class="badge badge-light-danger">Permohonan Ditolak</span>
                                        @endif
                                        {{-- {!! (1 == $usulan->status)? "<span class=\"badge badge-light-warning\">Menunggu Persetujuan</span>" : "<span class=\"badge badge-light-danger\">Draft</span>" !!} --}}
                                    </td>
                                    <td class="text-center">
                                        @if((0 == $usulan->status))
                                            {{-- <button class="btn btn-sm btn-icon btn-warning btnEditUsulan mt-1" data-id="{{ $usulan->id }}" data-url="{{url('uk3tsp/administrasi/showFormEditUsulan')}}"
                                                data-id-usulan="{{ $usulan->id }}"
                                                data-pmk="{{ $usulan->pmk }}"
                                                data-tanggal="{{ AppHelper::convertDateMDY($usulan->tanggal) }}"
                                                data-keterangan="{{ $usulan->keterangan }}"
                                                data-kantor-ids="{{ json_encode($usulan->kantor) }}"

                                                data-bs-toggle="tooltip" data-bs-placement="top" title="Edit">
                                                <i class="fa fa-edit"></i>
                                            </button> --}}

                                            <button class="btn btn-sm btn-icon btn-warning btnEditUsulan mt-1"
                                                    data-id="{{ $usulan->id }}"
                                                    data-url="{{url('uk3tsp/administrasi/showFormEditUsulan')}}"
                                                    data-bs-toggle="tooltip" data-bs-placement="top" title="Edit">
                                                <i class="fa fa-edit"></i>
                                            </button>

                                            <button class="btn btn-sm btn-icon btn-danger btnHapusUsulan mt-1"
                                                    data-url="{{url('uk3tsp/administrasi/hapusUsulan')}}"
                                                    data-id="{{ $usulan->id }}"
                                                    data-bs-toggle="tooltip"
                                                    data-bs-placement="top" title="Hapus">
                                                <i class="fa fa-trash"></i>
                                            </button>

                                            <button class="btn btn-sm btn-primary btnKirimUsulan mt-1"
                                                    data-id="{{ $usulan->id }}"
                                                    data-url="{{url('uk3tsp/administrasi/kirimUsulan')}}"
                                                    data-bs-toggle="tooltip" data-bs-placement="top" title="Kirim">
                                                <i class="fa fa-paper-plane"></i> Kirim
                                            </button>
                                        @endif
                                        @if((3 == $usulan->status))
                                            {{-- <button class="btn btn-sm btn-icon btn-warning btnEditUsulan mt-1" data-id="{{ $usulan->id }}" data-url="{{url('uk3tsp/administrasi/showFormEditUsulan')}}"
                                                data-id-usulan="{{ $usulan->id }}"
                                                data-pmk="{{ $usulan->pmk }}"
                                                data-tanggal="{{ AppHelper::convertDateMDY($usulan->tanggal) }}"
                                                data-keterangan="{{ $usulan->keterangan }}"
                                                data-kantorIds="{{ (isset($usulan->kantor[0]->nama))}}"

                                                data-bs-toggle="tooltip" data-bs-placement="top" title="Edit">
                                                <i class="fa fa-edit"></i>
                                            </button> --}}
                                            <button class="btn btn-sm btn-icon btn-warning btnEditUsulan mt-1"
                                                    data-id="{{ $usulan->id }}"
                                                    data-url="{{url('uk3tsp/administrasi/showFormEditUsulan')}}"
                                                    data-bs-toggle="tooltip" data-bs-placement="top" title="Edit">
                                                <i class="fa fa-edit"></i>
                                            </button>

                                            <button class="btn btn-sm btn-icon btn-danger btnHapusUsulan mt-1"
                                                    data-url="{{url('uk3tsp/administrasi/hapusUsulan')}}"
                                                    data-id="{{ $usulan->id }}"
                                                    data-bs-toggle="tooltip"
                                                    data-bs-placement="top" title="Hapus">
                                                <i class="fa fa-trash"></i>
                                            </button>

                                            <button class="btn btn-sm btn-primary btnKirimUsulan mt-1"
                                                    data-id="{{ $usulan->id }}"
                                                    data-url="{{url('uk3tsp/administrasi/kirimUsulan')}}"
                                                    data-bs-toggle="tooltip" data-bs-placement="top" title="Kirim">
                                                <i class="fa fa-paper-plane"></i> Kirim
                                            </button>
                                        @endif
                                    </td>
                                </tr>
                                    <?php $no++; ?>
                            @endforeach
                            </tbody>
                            <!--end::Table body-->
                        </table>
                    </div>
                    <!--end::Table-->
                    @if ($totalItems != 0)
                        <!--begin::pagination-->
                        <div class="d-flex flex-stack flex-wrap pt-5 p-3">
                            <div class="fs-6 fw-bold text-gray-700"></div>

                            @php
                                $disabled = '';
                                $nextPage= '';
                                $disablednext = '';
                                $hidden = '';
                                $pagination = '';

                                // tombol pagination
                                if ($totalItems == 0) {
                                    // $pagination = 'none';
                                }

                                // tombol back
                                if($currentPage == 1){
                                    $disabled = 'disabled';
                                }

                                // tombol next
                                if($currentPage != $totalPages) {
                                    $nextPage = $currentPage + 1;
                                }

                                // tombol pagination
                                if($currentPage == $totalPages || $totalPages == 0){
                                    $disablednext = 'disabled';
                                    $hidden = 'hidden';
                                }

                            @endphp

                            <ul class="pagination" style="display: {{ $pagination }}">
                                <li class="page-item disabled"><a href="#" class="page-link">Halaman {{ $currentPage }}
                                        dari {{ $totalPages }}</a></li>

                                <li class="page-item previous {{ $disabled }}"><a
                                        href="{{ url()->current() . '?page=' . ($currentPage - 1) }}" class="page-link"><i
                                            class="previous"></i></a></li>
                                <li class="page-item active"><a href="{{ url()->current() . '?page=' . $currentPage }}"
                                                                class="page-link">{{ $currentPage }}</a></li>
                                <li class="page-item" {{ $hidden }}><a
                                        href="{{ url()->current() . '?page=' . $nextPage }}"
                                        class="page-link">{{ $nextPage }}</a></li>
                                <li class="page-item next {{ $disablednext }}"><a
                                        href="{{ url()->current() . '?page=' . $nextPage }}" class="page-link"><i
                                            class="next"></i></a></li>
                            </ul>

                        </div>
                    @endif
                    <!-- end::Pagination -->
                </div>
            </div>
            <!--end::Card body-->
        </div>
        <!--end::Card-->
    </div>

    <!--begin::Modal - Add permohonan-->
    <div class="modal fade" id="kt_modal_permohonan" tabindex="-1" aria-hidden="true">
        <!--begin::Modal dialog-->
        <div class="modal-dialog modal-dialog-centered mw-800px">
            <!--begin::Modal content-->
            <div class="modal-content">
                <!--begin::Modal header-->
                <div class="modal-header" style="background-color:#02275d;">
                    <!--begin::Modal title-->
                    <h2 class="fw-bolder fs-4 text-white">Form UK3TSP </h2>
                    <!--end::Modal title-->
                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-icon-primary" style="background-color:#ffffff;"
                         data-bs-dismiss="modal">
                        <!--begin::Svg Icon | path: icons/duotone/Navigation/Close.svg-->
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                 width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                                   fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                    <rect fill="#000000" opacity="0.5"
                                          transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                          x="0" y="7" width="16" height="2" rx="1"/>
                                </g>
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </div>
                    <!--end::Close-->
                </div>
                <!--end::Modal header-->
                <!--begin::Modal body-->
                <div class="modal-body scroll-y mx-5 mx-xl-15 my-5">
                    <!--begin::Form-->
                    <form class="form w-lg-500px mx-auto" id="kt_basic_form"
                          action="{{ route('uk3tsp.createPermohonan') }}" method="post">
                        @csrf

                        {{-- menampilkan error validasi --}}
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <!--begin::Input group-->
                        <div class="fv-row mb-10">
                            <!--begin::Label-->
                            <label for="" class="required fs-6 fw-bold mb-2">Nomor PMK</label>
                            <!--end::Label-->
                            <input class="form-control form-control-solid" placeholder="" id="pmk" type="text"
                                   name="pmk">
                        </div>
                        <!--end::Input group-->

                        <!--begin::Input group-->
                        <div class="fv-row mb-10">
                            <!--begin::Label-->
                            <label class="required fs-6 fw-bold mb-2">Tanggal PMK</label>
                            <!--end::Label-->
                            <!--begin::Wrapper-->
                            <div class="position-relative d-flex align-items-center">
                                <!--begin::Icon-->
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <!--begin::Svg Icon | path: icons/duotone/Layout/Layout-grid.svg-->
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"/>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"/>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"/>
                                                </g>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </span>
                                </div>
                                <!--end::Icon-->
                                <!--begin::Input-->
                                <input class="form-control form-control-solid ps-12" placeholder="Pick date rage"
                                       id="tanggal" name="tanggal"/>
                                <input type="hidden" class="form-control form-control-solid" name="idUsulan"
                                       id="idUsulan" placeholder="" value=""/>
                                <!--end::Input-->
                            </div>
                            <!--end::Wrapper-->
                        </div>
                        <!--end::Input group-->

                        <!--begin::Input group-->
                        <div class="fv-row mb-10">
                            <!--begin::Label-->
                            <label for="" class="required fs-6 fw-bold mb-2">Kantor</label>
                            <!--end::Label-->
                            <select class="form-select form-select-solid select-kantor"
                                    data-placeholder="Select an option" data-allow-clear="true" name="kantorIds[]"
                                    id="kantorIds" multiple="multiple">
                                <option></option>
                            </select>
                        </div>
                        <!--end::Input group-->

                        <!--begin::Input group-->
                        <div class="fv-row mb-8">
                            <!--begin::Label-->
                            <label class="required fs-6 fw-bold mb-2">Keterangan</label>
                            <!--end::Label-->
                            <!--begin::Input-->
                            <textarea class="form-control form-control-solid" rows="3"
                                      placeholder="Mohon isi penjelasan" id="keterangan"
                                      name="keterangan">{{ old('keterangan') }}</textarea>
                            <!--end::Input-->
                        </div>
                        <!--end::Input group-->

                        <!--begin::Input group-->
                        <div class="fv-row mb-8">
                            <button type="button" class="btn btn-primary" data-action="submit">
                                <span class="indicator-label">
                                    Submit
                                </span>
                                <span class="indicator-progress">
                                    Please wait... <span
                                        class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                </span>
                            </button>
                        </div>
                        <!--end::Input group-->
                </div>

                </form>
            </div>
            <!--end::Modal body-->
        </div>
        <!--end::Modal content-->
    </div>
    <!--end::Modal dialog-->
    </div>
    <!--end::Modal - Edit permohonan-->
    </div>
    <!--begin::Modal - Add permohonan-->

    {{-- modal list kantor --}}
    <div class="modal fade" tabindex="-1" id="kt_modal_1">
        <div class="modal-dialog mw-750px">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title text-white">Daftar Unit Kerja</h3>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                         aria-label="Close">
                        <span class="svg-icon svg-icon-2x"></span>
                    </div>
                    <!--end::Close-->
                </div>

                <div class="modal-body">
                    <div class="table-responsive bg-gray-200 rounded border border-gray-300">
                        <table class="table table-striped align-middle" id="tabel_list_kantor">

                            <thead class="fw-bolder bg-secondary fs-6">
                            <tr class="">
                                <th class="text-center ps-4">No</th>
                                <th>Nama Kantor</th>
                            </tr>
                            </thead>
                            <tbody class="fw-bold text-gray-600" id="list_body">

                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{-- modal list kantor --}}

@endsection

@section('js')
    <script>

        let app_url = '{{ env('APP_URL') }}'

        // advanced search
        $("#advancedSearch").submit(function (e) {
            e.preventDefault();

            var query = $(this).serializeArray().filter(function (i) {
                return i.value;
            });

            window.location.href = $(this).attr('action') + (query ? '?' + $.param(query) : '');

        })
        $("#tanggal").daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1901,
                maxYear: parseInt(moment().format("YYYY"), 10),


            }
        );


        var r = document.querySelector('[data-action="submit"]');
        // form element
        var formSub = document.querySelector('#kt_basic_form');

        r.addEventListener("click", (function (e) {
            r.disabled = !0, r.setAttribute("data-kt-indicator", "on"), setTimeout((function () {
                r.removeAttribute("data-kt-indicator"), r.disabled = !1, Swal.fire({
                    text: "Form has been successfully submitted!",
                    icon: "success",
                    buttonsStyling: !1,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                }).then((function () {
                    formSub.submit();
                }))
            }), 2e3)
        }));

        $(".btnTambahPermohonan").click(function () {
            $('#kt_modal_permohonan').modal('show');

            $('#idUsulan').val('').change();
            $('#pmk').val('').change();
            $('#tanggal').val('');
            $('#kantorIds').empty();
            $('#keterangan').val('').change();
            //$('#keterangan').val(null).change();

        });

        $(".btnEditUsulan").click(function () {
            //$('#kt_modal_permohonan').modal('show');
            var idUsulan = $(this).data('id');
            var url = $(this).data("url");

            window.location.href = url + '?string=' + btoa(idUsulan);

        });

        // $(".btnEditUsulan").click(function(){
        //     $('#kt_modal_permohonan').modal('show');

        //     // window.location.href    = url+'?string='+btoa(idUsulan);

        //     var idUsulan   = $(this).data('id-usulan');
        //     var pmk        = $(this).data('pmk');
        //     let tanggal    = $(this).data('tanggal');
        //     var kantor     = $(this).data('kantorIds');
        //     var keterangan = $(this).data('keterangan');

        //     $('#idUsulan').val(idUsulan).change();
        //     $('#pmk').val(pmk).change();
        //     $('#tanggal').val(tanggal).change();
        //     $('#kantorIds').val(kantor).change();
        //     $('#keterangan').val(keterangan).change();

        //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        //     if(0 < kantorIds.length){
        //         var kantorIdsSelect = $('#kantorIds');

        //         kantorIdsSelect.empty();
        //         //$('#provinsiIds').select2().val(null).trigger("change");

        //         // Fetch the preselected item, and add to the control
        //         for(let i = 0; i < kantorIds.length; i++){
        //             //$('#provinsiIds').append($("<option selected='selected'></option>").val(provinsiIds[i]).text("provinsi "+i)).trigger('change');
        //             $.ajax({
        //                 type: 'GET',
        //                 url: '/uk3tsp/getKantor?kantorId=' + kantorIds[i]
        //             }).then(function (data) {
        //                 // create the option and append to Select2
        //                 var option = new Option(data.text, data.id, true, true);
        //                 kantorIdsSelect.append(option).trigger('change');


        //                 alert(JSON.stringify(data));

        //                 // manually trigger the `select2:select` event
        //                 kantorIdsSelect.trigger({
        //                     type: 'select2:select',
        //                     params: {
        //                         data: data
        //                     }
        //                 });
        //             });
        //         }
        //         //$('#provinsiIds').select2().val(provinsiIds).trigger("change");
        //     }

        // });


        $(".listKantor").click(function () {
            $('#kt_modal_1').modal('show');

            var idUsulan = $(this).data('id-usulan');
            var pmk = $(this).data('pmk');
            var kantor = $(this).data('kantor');


            $('#pmk').val(pmk).change();
            $('#kantor').val(kantor).change();


            // alert(JSON.stringify($listKantor));
            // alert($listKantor);

            var listKantor = kantor.split("|");


            for (var i = 0; i < listKantor.length; i++) {
                console.log(listKantor[i])

                ;

                var name = listKantor;

                document.getElementById("list_body").innerHTML = "";
                var j = 1;
                for (var i = 0; i < listKantor.length; i++) {
                    //var numcols = $('#tabel_list_kantor').find('thead th').length;
                    //$('<td />').appendTo(row);
                    var row = $('<tr/>');
                    //Insert name
                    var name_td = '<td class= "text-center">' + j + '</td>' + '<td ><span class="bullet bg-warning me-3"></span>' + listKantor[i] + '</td>';
                    row.append(name_td);
                    //fill the rest with empty td's
                    row.appendTo('#tabel_list_kantor');
                    j++
                }
            }
            // alert(listKantor);
        });


        $(".btnHapusUsulan").click(function () {
            var id = $(this).data("id");
            var url = $(this).data("url");
            var token = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                title: 'Yakin akan menghapus data?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#C5584B',
                cancelButtonColor: '#B2B2B2',
                confirmButtonText: 'Hapus!',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'post',
                        url: url,
                        data: {
                            _token: token,
                            idUsulan: id
                        },
                        success: function (response) {
                            //alert(JSON.stringify(response));
                            if (response['status'] === 1) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'data berhasil di hapus',
                                    showConfirmButton: false,
                                    timer: 1500
                                });

                                $('#libur' + id).remove();

                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal menghapus data!',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                console.log(response)
                            }
                        },
                        error: function (err) {
                            console.log(err)
                        }
                    });
                }
            })
        });

        $(".btnKirimUsulan").click(function () {
            var id = $(this).data("id");
            var url = $(this).data("url");
            var token = $("meta[name='csrf-token']").attr("content");
            //alert(id);
            //alert(url+'?string='+btoa(id));
            Swal.fire({
                title: 'Yakin akan mengirim usulan?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009ef7',
                cancelButtonColor: '#C5584B',
                confirmButtonText: 'Kirim!',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'post',
                        url: url + '?string=' + btoa(id),
                        data: {
                            _token: token,
                            idUsulan: id
                        },
                        success: function (response) {
                            //alert(JSON.stringify(response));
                            if (response['status'] === 1) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'usulan berhasil di kirim',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                window.location.href = document.URL;
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal mengirim usulan!',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            }
                        },
                        error: function (err) {
                            console.log(err)
                        }
                    });
                }
            })

        });

        $(".select-kantor").select2({
            minimumInputLength: 1,
            ajax: {
                url: "{{ url('/uk3tsp/getKantorList') }}",
                dataType: 'json',
                delay: 250,
                allowClear: true,
                data: function (params) {

                    return {
                        q: params.term
                    };
                },
                processResults: function (data) {
                    // alert(JSON.stringify(data));
                    return {
                        results: data
                    };
                }
            }
        });


    </script>
@endsection
