@extends('layout_new.master')
<?php
// dump($selectedKantor);
?>

@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <!--begin::Card-->
            <div class="card card-flush">
                <!--begin::Card header-->
                <div class="card-header" id="card-header">
                    <!--begin::Card title-->
                    <div class="card-title fw-bold fs-4 text-white">
                        Form Edit Permohonan Usulan
                    </div>
                    <!--end::Card title-->
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body">
                    <!--begin::Stepper-->
                    <!--begin::Form-->
                    <form class="form w-lg-500px mx-auto" id="kt_basic_form"
                          action="{{ route('uk3tsp.createPermohonan') }}" method="post">
                        @csrf

                        {{-- menampilkan error validasi --}}
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <!--begin::Input group-->
                        <div class="fv-row mb-10">
                            <!--begin::Label-->
                            <label for="" class="required fs-6 fw-bold mb-2">Nomor PMK</label>
                            <!--end::Label-->
                            <input class="form-control form-control-solid" placeholder="" id="pmk" type="text"
                                   name="pmk" value="{{ $dataUsulan->pmk }}">
                        </div>
                        <!--end::Input group-->

                        <!--begin::Input group-->
                        <div class="fv-row mb-10">
                            <!--begin::Label-->
                            <label class="required fs-6 fw-bold mb-2">Tanggal PMK</label>
                            <!--end::Label-->
                            <!--begin::Wrapper-->
                            <div class="position-relative d-flex align-items-center">
                                <!--begin::Icon-->
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <!--begin::Svg Icon | path: icons/duotone/Layout/Layout-grid.svg-->
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"/>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4"
                                                          rx="1"/>
                                                    <path
                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                        fill="#000000"/>
                                                </g>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </span>
                                </div>
                                <!--end::Icon-->
                                <!--begin::Input-->
                                <input class="form-control form-control-solid ps-12" placeholder="Pick date rage"
                                       id="tanggal" name="tanggal" value="{{ $dataUsulan->tanggal }}"/>
                                <input type="hidden" class="form-control form-control-solid" name="idUsulan"
                                       id="idUsulan" placeholder="" value="{{ $dataUsulan->id }}"/>
                                <!--end::Input-->
                            </div>
                            <!--end::Wrapper-->
                        </div>
                        <!--end::Input group-->

                        <!--begin::Input group-->
                        <div class="fv-row mb-10">
                            <!--begin::Label-->
                            <label for="" class="required fs-6 fw-bold mb-2">Kantor</label>
                            <!--end::Label-->
                            <select class="form-select form-select-solid select-kantor"
                                    data-placeholder="Select an option" data-allow-clear="true" name="kantorIds[]"
                                    id="kantorIds" multiple="multiple">
                                <option>
                                @if (!empty($dataUsulan->kantor))
                                    @foreach ($selectedKantor as $kantor)
                                        <option value="{{ $kantor['id'] }}" selected>{{ $kantor['nama'] }}</option>
                                        @endforeach
                                        @endif
                                        </option>
                            </select>
                        </div>
                        <!--end::Input group-->

                        <!--begin::Input group-->
                        <div class="fv-row mb-8">
                            <!--begin::Label-->
                            <label class="required fs-6 fw-bold mb-2">Keterangan</label>
                            <!--end::Label-->
                            <!--begin::Input-->
                            <textarea class="form-control form-control-solid" rows="3" id="keterangan"
                                      name="keterangan">{{ $dataUsulan->keterangan }}
                            </textarea>
                            <!--end::Input-->
                        </div>
                        <!--end::Input group-->

                        <!--begin::Input group-->
                        <div class="fv-row mb-8">
                            <button class="btn btn-danger btnKembali " data-bs-toggle="tooltip" data-bs-placement="top"
                                    title="Kembali">
                                <i class="fa fa-long-arrow-alt-left"></i> Kembali
                            </button>
                            <button type="button" class="btn btn-primary" data-action="submit">
                                <span class="indicator-label">
                                    Submit
                                </span>
                                <span class="indicator-progress">
                                    Please wait... <span
                                        class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                </span>
                            </button>

                        </div>
                        <!--end::Input group-->
                </div>

                </form>
                <!--end::Form-->
            </div>
            <!--end::Stepper-->
        </div>
    </div>
    </div>
    </div>
@endsection

@section('js')
    <script>
        // $(".btnKembali").on('click',function(){
        //     window.location.href    = "{{ url('/uk3tsp/administrasi') }}";
        // });

        // $(".select-kantor").select2({
        //         ajax: {
        //             url: "{{ url('/uk3tsp/getKantorList') }}",
        //             dataType: 'json',
        //             delay: 250,
        //             allowClear: true,
        //             data: function (params) {
        //                 return {
        //                     q: params.term, // search term
        //                     page: params.page
        //                 };
        //             },
        //             processResults: function (data, params) {
        //                 // parse the results into the format expected by Select2
        //                 // since we are using custom formatting functions we do not need to
        //                 // alter the remote JSON data, except to indicate that infinite
        //                 // scrolling can be used
        //                 params.page = params.page || 1;

        //                 return {
        //                     results: data.kantors,
        //                     pagination: {
        //                         more: (params.page * 30) < data.total_count
        //                     }
        //                 };
        //             },
        //             cache: true
        //         },
        //         placeholder: 'Search for a Kantor',
        //         minimumInputLength: 1,
        //         templateResult: formatRepo,
        //         templateSelection: formatRepoSelection
        //     });

        $(".select-kantor").select2({
            minimumInputLength: 1,
            ajax: {
                url: "{{ url('/uk3tsp/getKantorList') }}",
                dataType: 'json',
                delay: 250,
                allowClear: true,
                data: function (params) {

                    return {
                        q: params.term
                    };
                },
                processResults: function (data) {
                    // alert(JSON.stringify(data));
                    return {
                        results: data
                    };
                }
            }
        });

        // function formatRepo (repo) {
        // if (repo.loading) {
        //     return repo.nama;
        // }

        // var $container = $(
        //     "<div class='select2-result-text'>" + repo.nama + "</div>"
        // );

        // return $container;
        // }

        // function formatRepoSelection (repo) {
        //     return repo.nama;
        // }

        var r = document.querySelector('[data-action="submit"]');
        // form element
        var formSub = document.querySelector('#kt_basic_form');

        r.addEventListener("click", (function (e) {
            r.disabled = !0, r.setAttribute("data-kt-indicator", "on"), setTimeout((function () {
                r.removeAttribute("data-kt-indicator"), r.disabled = !1, Swal.fire({
                    text: "Form has been successfully submitted!",
                    icon: "success",
                    buttonsStyling: !1,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                }).then((function () {
                    formSub.submit();
                }))
            }), 2e3)
        }));

    </script>

@endsection
