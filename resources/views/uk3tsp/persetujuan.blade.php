@extends('layout_new.master')

@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <!--begin::Card-->
            @if(isset($alert_message))
                {!! $alert_message !!}
            @else

                <div class="card mb-3 border-warning shadow-sm">
                    <div id="card-header" class="card-header">
                        <div class="card-title fw-bold fs-4 text-white">
                            Administrasi UK3TSP
                        </div>
                    </div>
                    <!--end::Card title-->

                    <!--begin::Card toolbar-->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6 mb-4">
                                <div class="text-start" data-kt-customer-table-toolbar="base">
                                    <button type="button" class="btn btn-light-primary  btn-active-primary"
                                            data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start"
                                            data-kt-menu-flip="top-end">
                                        <i class="fa fa-filter"></i> Advance Search
                                    </button>
                                    <div class="menu menu-sub menu-sub-dropdown w-md-800px border-warning mt-1"
                                         data-kt-menu="true">
                                        <div class="px-7 py-5">
                                            <div class="fs-4 text-dark fw-bolder">Filter Pencarian</div>
                                        </div>
                                        <div class="separator border-gray-200"></div>
                                        <form id="advancedSearch" action="" method="get">
                                                <?php
                                                if (isset($_GET['pmk'])) {
                                                    $searchValue = $_GET['pmk'];
                                                } else if (isset($_GET['nomorTicket'])) {
                                                    $searchValue = $_GET['nomorTicket'];
                                                } else if (isset($_GET['keterangan'])) {
                                                    $searchValue = $_GET['keterangan'];
                                                } else if (isset($_GET['kantorIds'])) {
                                                    $searchValue = $_GET['kantorIds'];
                                                } else {
                                                    $searchValue = "";
                                                }
                                                ?>
                                            <div class="px-7 py-5">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <label class="form-label fw-bold">Nomor PMK:</label>
                                                        <div class="row mb-4">
                                                            <div class="col-lg-12">
                                                                <input class="form-control form-select-solid"
                                                                       type="text" name="pmk" placeholder="Nomor PMK"
                                                                       value="{{ isset($_GET['pmk']) ? $_GET['pmk'] : '' }}">
                                                            </div>
                                                        </div>
                                                        <label class="form-label fw-bold">Nomor Ticket:</label>
                                                        <div class="row mb-4">
                                                            <div class="col-lg-12">
                                                                <input class="form-control form-select-solid"
                                                                       type="text" name="nomorTicket"
                                                                       placeholder="Nomor Ticket"
                                                                       value="{{ isset($_GET['nomorTicket']) ? $_GET['nomorTicket'] : '' }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="form-label fw-bold">Keterangan:</label>
                                                        <div class="row mb-4">
                                                            <div class="col-lg-12">
                                                                <input class="form-control form-select-solid"
                                                                       type="text" name="keterangan"
                                                                       placeholder="keterangan"
                                                                       value="{{ isset($_GET['keterangan']) ? $_GET['keterangan'] : '' }}">
                                                            </div>
                                                        </div>
                                                        <label class="form-label fw-bold">Kantor:</label>
                                                        <div class="row mb-4">
                                                            <div class="col-lg-12">
                                                                <input class="form-control form-select-solid"
                                                                       type="text" name="kantorIds" placeholder="kantor"
                                                                       value="{{ isset($_GET['kantorIds']) ? $_GET['kantorIds'] : '' }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-end">
                                                    <a href="{{ url()->current() }}"
                                                       class="btn btn-light btn-active-light-primary me-2">Reset</a>
                                                    <button type="submit" class="btn btn-sm btn-primary"
                                                            data-kt-menu-dismiss="true"
                                                            data-kt-customer-table-filter="filter" id="searchData"
                                                            value="{{ $searchValue }}">Search
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="col-md-6">
                                <div class="text-end mb-2">
                                    <button type="button" class="btn btn-light-primary btnTambahPermohonan" data-bs-toggle="modal" data-bs-target="#kt_modal_permohonan">
                                        <!--begin::Svg Icon | path: icons/duotone/Interface/Plus-Square.svg-->
                                        <span class="svg-icon svg-icon-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <path opacity="0.25" fill-rule="evenodd" clip-rule="evenodd" d="M6.54184 2.36899C4.34504 2.65912 2.65912 4.34504 2.36899 6.54184C2.16953 8.05208 2 9.94127 2 12C2 14.0587 2.16953 15.9479 2.36899 17.4582C2.65912 19.655 4.34504 21.3409 6.54184 21.631C8.05208 21.8305 9.94127 22 12 22C14.0587 22 15.9479 21.8305 17.4582 21.631C19.655 21.3409 21.3409 19.655 21.631 17.4582C21.8305 15.9479 22 14.0587 22 12C22 9.94127 21.8305 8.05208 21.631 6.54184C21.3409 4.34504 19.655 2.65912 17.4582 2.36899C15.9479 2.16953 14.0587 2 12 2C9.94127 2 8.05208 2.16953 6.54184 2.36899Z" fill="#12131A" />
                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M12 17C12.5523 17 13 16.5523 13 16V13H16C16.5523 13 17 12.5523 17 12C17 11.4477 16.5523 11 16 11H13V8C13 7.44772 12.5523 7 12 7C11.4477 7 11 7.44772 11 8V11H8C7.44772 11 7 11.4477 7 12C7 12.5523 7.44771 13 8 13H11V16C11 16.5523 11.4477 17 12 17Z" fill="#12131A" />
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                        Tambah UK3TSP
                                    </button>
                                </div>
                            </div> --}}
                        </div>
                        <!--end::Card toolbar-->
                    </div>
                    <!--end::Card header-->

                    <!--begin::Card body-->
                    <div class="card-body pt-0">
                        <!--begin::Table-->
                        <div class="table-responsive bg-gray-200 rounded border border-gray-300">
                            <table class="table table-striped align-middle" id="tabel_tubel">

                                <!--begin::Table head-->
                                <thead>
                                <!--begin::Table row-->
                                <tr class="text-start text-white fw-bolder fs-7 text-uppercase gs-0"
                                    style="background-color:#02275d;">
                                    <th class="min-w-50px text-center">No</th>
                                    <th class="min-w-50px text-center">Nomor Ticket</th>
                                    <th class="min-w-100px">PMK</th>
                                    <th class="min-w-100px">Tanggal PMK</th>
                                    <th class="text-center min-w-50px">Detail Kantor</th>
                                    <th class="min-w-100px">Keterangan</th>
                                    <th class="min-w-100px">Status</th>
                                    <th class="text-center min-w-120px">Actions</th>
                                </tr>
                                <!--end::Table row-->
                                </thead>
                                <!--end::Table head-->
                                <!--begin::Table body-->
                                <tbody class="fw-bold text-gray-600">
                                    <?php $no = $numberOfElements; ?>
                                @foreach ($usulanJKU as $usulan)
                                    <tr id="libur{{$usulan->id}}">
                                        <td class="text-center">{{ $no }}</td>
                                        <td>{{ $usulan->nomorTicket }}</td>
                                        <td>{{ $usulan->pmk }}</td>
                                        <td>
                                            {{ AppHelper::convertDateDMY($usulan->tanggal) }}
                                        </td>
                                        @if(isset($usulan->kantor ))
                                                <?php
                                                $kantorId = "";
                                                foreach ($usulan->kantor as $item) {
                                                    if ('' == $kantorId) {
                                                        $kantorId .= $item->nama;
                                                    } else {
                                                        $kantorId .= "|" . $item->nama;
                                                    }
                                                }
                                                ?>
                                        @endif
                                        <td class="text-center listKantor"
                                            data-bs-toggle="modal" data-bs-target="#kt_modal_1"
                                            data-id-usulan="{{ $usulan->id }}"
                                            data-pmk="{{ $usulan->pmk }}"
                                            data-kantor="{{ (isset($kantorId)) ? $kantorId : "" }}">
                                            <a href="#"><u>List Kantor<u>

                                        </td>
                                        <td>
                                            {{ $usulan->keterangan }}
                                        </td>
                                        <td>
                                            {!! (2 == $usulan->status)? "<span class=\"badge badge-light-success\">Disetujui</span>" : "<span class=\"badge badge-light-danger\">Menunggu Persetujuan</span>" !!}
                                        </td>
                                        <td class="text-center">
                                            @if(2 != $usulan->status)
                                                <button class="btn btn-sm btn-danger mt-1" data-bs-toggle="modal"
                                                        data-bs-target="#kt_modal_tolak"
                                                        id="btnTolakPermohonan"
                                                        data-url="{{url('uk3tsp/persetujuan/tolakUsulan')}}"
                                                        data-id="{{ $usulan->id }}" title="Tolak">
                                                    <i class="fa fa-thumbs-down"></i>Tolak
                                                </button>

                                                <button class="btn btn-sm btn-success btnKirimUsulan mt-1"
                                                        data-id="{{ $usulan->id }}"
                                                        data-url="{{url('uk3tsp/persetujuan/approveUsulan')}}"
                                                        data-bs-toggle="tooltip" data-bs-placement="top"
                                                        title="Approve">
                                                    <i class="fa fa-thumbs-up"></i> Approve
                                                </button>
                                            @endif
                                        </td>
                                    </tr>
                                        <?php $no++; ?>
                                @endforeach
                                </tbody>
                                <!--end::Table body-->
                            </table>
                        </div>
                        <!--end::Table-->
                        @if ($totalItems != 0)
                            <!--begin::pagination-->
                            <div class="d-flex flex-stack flex-wrap pt-5 p-3">
                                <div class="fs-6 fw-bold text-gray-700"></div>

                                @php
                                    $disabled = '';
                                    $nextPage= '';
                                    $disablednext = '';
                                    $hidden = '';
                                    $pagination = '';

                                    // tombol pagination
                                    if ($totalItems == 0) {
                                        // $pagination = 'none';
                                    }

                                    // tombol back
                                    if($currentPage == 1){
                                        $disabled = 'disabled';
                                    }

                                    // tombol next
                                    if($currentPage != $totalPages) {
                                        $nextPage = $currentPage + 1;
                                    }

                                    // tombol pagination
                                    if($currentPage == $totalPages || $totalPages == 0){
                                        $disablednext = 'disabled';
                                        $hidden = 'hidden';
                                    }

                                @endphp

                                <ul class="pagination" style="display: {{ $pagination }}">
                                    <li class="page-item disabled"><a href="#"
                                                                      class="page-link">Halaman {{ $currentPage }}
                                            dari {{ $totalPages }}</a></li>

                                    <li class="page-item previous {{ $disabled }}"><a
                                            href="{{ url()->current() . '?page=' . ($currentPage - 1) }}"
                                            class="page-link"><i class="previous"></i></a></li>
                                    <li class="page-item active"><a
                                            href="{{ url()->current() . '?page=' . $currentPage }}"
                                            class="page-link">{{ $currentPage }}</a></li>
                                    <li class="page-item" {{ $hidden }}><a
                                            href="{{ url()->current() . '?page=' . $nextPage }}"
                                            class="page-link">{{ $nextPage }}</a></li>
                                    <li class="page-item next {{ $disablednext }}"><a
                                            href="{{ url()->current() . '?page=' . $nextPage }}" class="page-link"><i
                                                class="next"></i></a></li>
                                </ul>

                            </div>
                        @endif
                        <!-- end::Pagination -->
                    </div>
                </div>
            @endif
            <!--end::Card body-->
        </div>
        <!--end::Card-->
    </div>

    <div class="modal fade" tabindex="-1" id="kt_modal_tolak">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Tolak Usulan</h5>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                         aria-label="Close">
                        <span class="svg-icon svg-icon-2x"></span>
                    </div>
                    <!--end::Close-->
                </div>

                <div class="modal-body">
                    <!--begin::Input group-->
                    <div class="fv-row mb-8">
                        <!--begin::Label-->
                        <label class="required fs-6 fw-bold mb-2">Alasan Penolakan</label>
                        <!--end::Label-->
                        <!--begin::Input-->
                        <textarea class="form-control form-control-solid" rows="3"
                                  placeholder="Mohon isi alasan penolakan" id="keteranganTolak" name="keteranganTolak"
                                  required></textarea>
                        <!--end::Input-->
                    </div>
                    <!--end::Input group-->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button class="btn btn-sm btn-danger btnTolakUsulan mt-1"
                            data-url="{{url('uk3tsp/persetujuan/tolakUsulan')}}"
                            id="btnTolakUsulan" data-id-usulan=""
                            data-bs-toggle="tooltip" data-bs-placement="top" title="Tolak">
                        <i class="fa fa-thumbs-down"></i>Tolak
                    </button>
                </div>
            </div>
        </div>
    </div>

    {{-- modal list kantor --}}
    <div class="modal fade" tabindex="-1" id="kt_modal_1">
        <div class="modal-dialog mw-600px">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Daftar Unit Kerja</h5>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                         aria-label="Close">
                        <span class="svg-icon svg-icon-2x"></span>
                    </div>
                    <!--end::Close-->
                </div>

                <div class="modal-body">
                    <div class="table-responsive bg-gray-400 rounded border border-gray-300">
                        <table class="table table-striped align-middle" id="tabel_list_kantor">

                            <thead class="fw-bolder bg-secondary fs-6">
                            <tr class="">
                                <th class="text-center ps-4">No</th>
                                <th>Nama Kantor</th>
                            </tr>
                            </thead>
                            <tbody class="fw-bold text-gray-600" id="list_body">

                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{-- modal list kantor --}}

@endsection

@section('js')
    <script>


        // advanced search
        $("#advancedSearch").submit(function (e) {
            e.preventDefault();

            var query = $(this).serializeArray().filter(function (i) {
                return i.value;
            });

            window.location.href = $(this).attr('action') + (query ? '?' + $.param(query) : '');

        })
        $(".btnKirimUsulan").click(function () {
            var id = $(this).data("id");
            var url = $(this).data("url");
            var token = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                title: 'Yakin akan menyetujui data?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#50CD89',
                cancelButtonColor: '#B2B2B2',
                confirmButtonText: 'Approve!',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    blockUI.block();
                    $.ajax({
                        type: 'post',
                        url: url,
                        data: {
                            _token: token,
                            idUsulan: id
                        },
                        success: function (response) {
                            btoa
                            if (response['status'] === 1) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'permohonan berhasil di setujui',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                blockUI.release();
                                window.location.href = document.URL;
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Proses approval gagal!',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                blockUI.release();
                                location.reload();
                            }
                        },
                        error: function (err) {
                            console.log(err);
                            blockUI.release();
                        }
                    });
                }
            })
        });

        $("#btnTolakPermohonan").click(function () {
            $('#btnTolakUsulan').data('id'); //getter
            $('#btnTolakUsulan').attr('data-id-usulan', $(this).data("id")); //setter
        });

        $("#btnTolakUsulan").click(function () {
            var id = $(this).data("id-usulan");
            var url = $(this).data("url");
            var keteranganTolak = $('#keteranganTolak').val();
            var token = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                title: 'Yakin akan menolak usulan ini?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#C5584B',
                cancelButtonColor: '#B2B2B2',
                confirmButtonText: 'Tolak!',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'post',
                        url: url,
                        data: {
                            _token: token,
                            idUsulan: id,
                            keteranganTolak: keteranganTolak,
                            statusUsulan: 3
                        },
                        success: function (response) {
                            if (response['status'] === 1) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'usulan berhasil di tolak',
                                    showConfirmButton: false,
                                    timer: 1500
                                });

                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal menolak usulan!',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                blockUI.release();
                                location.reload();
                            }
                            $('#kt_modal_tolak').modal('toggle');
                            window.location.href = document.URL;
                        },
                        error: function (err) {
                            //console.log(err)
                        }
                    });
                }
            })
        });

        $(".listKantor").click(function () {
            $('#kt_modal_1').modal('show');

            var idUsulan = $(this).data('id-usulan');
            var pmk = $(this).data('pmk');
            var kantor = $(this).data('kantor');


            $('#pmk').val(pmk).change();
            $('#kantor').val(kantor).change();

            var listKantor = kantor.split("|");

            for (var i = 0; i < listKantor.length; i++) {
                console.log(listKantor[i])

                var name = listKantor;

                document.getElementById("list_body").innerHTML = "";
                var j = 1;
                for (var i = 0; i < listKantor.length; i++) {

                    var row = $('<tr/>');

                    var name_td = '<td class= "text-center">' + j + '</td>' + '<td ><span class="bullet bg-warning me-3"></span>' + listKantor[i] + '</td>';
                    row.append(name_td);

                    row.appendTo('#tabel_list_kantor');
                    j++
                }
            }
        });


    </script>

@endsection
