@extends('layout_new.master')

<?php

if (null === $cutiStatus || 0 == $cutiStatus->hakCuti) {
    $kuotaCuti = 0;
    $kuotaTahunLalu = 0;
    $kuotaTahunIni = 0;
    $kuotaCutiTambahan = 0;
} else {
    $kuotaCuti = $cutiStatus->hakCuti + $sisaCutiSebelumnya - $cutiDipakai;
    $kuotaTahunLalu = $sisaCutiSebelumnya - $cutiDipakai;
    $kuotaTahunIni = $cutiStatus->hakCuti;
    if ($kuotaTahunLalu <= 0) {
        $kuotaTahunIni += $kuotaTahunLalu;
        $kuotaTahunLalu = 0;
    }
}

if (0 == $hakcutitambahan && null === $cutiStatus) {
    $kuotaCutiTambahan = 0;
} elseif(1 == $hakcutitambahan && null === $cutiStatus) {
    $kuotaCutiTambahan = 0;
} else {
    $kuotaCutiTambahan = $cutiStatus->hakCuti - $cutiTambahanDipakai;
}
?>
<style>
    .holiday {
        background-color: white;
        color: #9c2517 !important;
    }

    .addReadMore.showlesscontent .SecSec,
    .addReadMore.showlesscontent .readLess {
        display: none;
    }

    .addReadMore.showmorecontent .readMore {
        display: none;
    }

    .addReadMore .readMore,
    .addReadMore .readLess {
        font-weight: bold;
        margin-left: 2px;
        color: blue;
        cursor: pointer;
    }

    .addReadMoreWrapTxt.showmorecontent .SecSec,
    .addReadMoreWrapTxt.showmorecontent .readLess {
        display: block;
    }
</style>
@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <div class="card card-stretch-50 card-bordered mb-5">
                <div class="card-header" id="card-header">
                    <h1 class="card-title text-white"><i class="bi bi-calculator-fill fs-2x text-white"></i> Kuota Cuti Tahunan (<span
                            class="label text-warning">{{ $namaPegawai }}</span>)</h1>
                </div>
                <div class="card-body">
                    <h2 class="me-auto">{{ date('Y') }}</h2>
                    <div class="progress" style="height: 30px;">
                        <div class="progress-bar progress-bar-striped bg-primary progress-bar-animated"
                             role="progressbar" style="width: {{ ($kuotaTahunIni/12)*100 }}%" aria-valuenow="100"
                             aria-valuemin="0" aria-valuemax="100">
                        <strong style="font-size:1.4rem" class="text-white">{{ $kuotaTahunIni }}</strong>

                        </div>
                    </div>

                    <h2 class="me-auto mt-5">{{ date('Y')-1 }}</h2>
                    <div class="progress" style="height: 30px;">
                        <div class="progress-bar progress-bar-striped bg-primary progress-bar-animated"
                             role="progressbar" style="width: {{ ($kuotaTahunLalu/12)*100 }}%" aria-valuenow="100"
                             aria-valuemin="0" aria-valuemax="100">
                            <strong style="font-size:1.4rem" class="text-white">{{ $kuotaTahunLalu }}</strong>
                        </div>
                    </div>

                    <h2 class="me-auto mt-5">{{ date('Y')-2 }}</h2>
                    <div class="progress" style="height: 30px;">
                        <div class="progress-bar progress-bar-striped bg-primary progress-bar-animated"
                             role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0"
                             aria-valuemax="100">
                            <strong style="font-size:1.4rem" class="text-white">0</strong>
                        </div>
                    </div>
                    @if(1 == $hakcutitambahan)
                        <h2 class="me-auto mt-5">Cuti Tambahan</h2>
                        <div class="progress" style="height: 30px;">
                            <div class="progress-bar progress-bar-striped bg-success progress-bar-animated"
                                 role="progressbar" style="width: {{ ($kuotaCutiTambahan/12)*100 }}%"
                                 aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                <strong style="font-size:1.4rem" class="text-white">{{ $kuotaCutiTambahan }}</strong>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="card-footer text-black" style="font-size:1.4rem"><strong>Total : {{ $kuotaCuti + $kuotaCutiTambahan }}</strong></div>
            </div>
            <!--begin::Modal header-->

            <!--end::Modal header-->
            <div class="card ">
                <div class="modal-header card-stretch-50 card-bordered  p-6" id="card-header">
                    <strong>
                    <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x fs-6 ms-2">
                        <li class="nav-item">
                            <a class="nav-link active text-white" data-bs-toggle="tab" href="#kt_permohonan">Permohonan</a>
                        </li>
                        @if(1 == $hakcutitambahan)
                            <li class="nav-item">
                                <a class="nav-link text-white" data-bs-toggle="tab" href="#kt_permohonan_ctt">Cuti Tambahan</a>
                            </li>
                        @endif
                        <li class="nav-item">
                            <a class="nav-link text-white" data-bs-toggle="tab" href="#kt_riwayat">Riwayat Cuti</a>
                        </li>
                    </ul>
                    </strong>
                </div>

                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="kt_permohonan" role="tabpanel">
                        <!--begin::Card-->
                        <div class="card card-flush">
                            <!--begin::Card header-->
                            <div class="card-header mt-6">
                                <!--begin::Card title-->
                                <div class="card-title">
                                    <!--begin::Search-->
                                    <div class="d-flex align-items-center position-relative my-1 me-5">

                                    </div>
                                    <!--end::Search-->
                                </div>
                                <!--end::Card title-->
                                <!--begin::Card toolbar-->
                                <div class="card-toolbar">
                                    <!--begin::Button-->
                                    <button type="button" class="btn btn-light-primary btnTambahPermohonan"
                                            data-bs-toggle="modal" data-bs-target="#kt_modal_permohonan">
                                        <i class="fa fa-plus"></i>
                                        Tambah Permohonan Cuti
                                    </button>
                                    <!--end::Button-->
                                </div>
                                <!--end::Card toolbar-->
                            </div>
                            <!--end::Card header-->
                            <!--begin::Card body-->
                            <div class="card-body">
                                {{-- menampilkan error validasi --}}
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{!! $error !!}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <!--begin::Table-->
                                <div class="table-responsive rounded border">
                                    <table class="table align-middle table-striped fs-6 gy-5 mb-0"
                                           id="kt_permissions_table">
                                        <!--begin::Table head-->
                                        <thead>
                                        <!--begin::Table row-->
                                        <tr class="text-start text-white fw-bolder fs-7 text-uppercase gs-0"
                                            style="background-color:#02275d;">
                                            <th class="min-w-50px text-center">No</th>
                                            <th class="min-w-100px">Nomor Tiket</th>
                                            <th class="min-w-120px">Jenis Cuti</th>
                                            <th class="min-w-225px">Tanggal Cuti</th>
                                            <th class="min-w-100px">Tanggal Permohonan</th>
                                            <th class="w-200px">Status</th>
                                            <th class="text-center min-w-130px">Actions</th>
                                        </tr>
                                        <!--end::Table row-->
                                        </thead>
                                        <!--end::Table head-->
                                        <!--begin::Table body-->
                                        <tbody class="fw-bold text-gray-600">
                                        <?php $no = $numberOfElements; $cutiTahunanDiajukan = 0; $cutiMelahirkanDiajukan = 0; $_persalinanKe = 0 ?>
                                        @foreach ($permohonanCuti as $usulan)
                                                <?php
                                                $tanggalLCB = AppHelper::convertDateMDY($usulan->tanggalMulai) . ' - ' . AppHelper::convertDateMDY($usulan->tanggalSelesai);
                                                if (($usulan->status == 1 || $usulan->status == 0) && in_array($usulan->jnsCuti->id, array(6, 9, 10))) {
                                                    $cutiTahunanDiajukan++;
                                                }
                                                $persalinanKe = $usulan->persalinanKe ?? null;
                                                $tanggalPersetujuan = $usulan->tanggalPersetujuan ?? null;
                                                $approvalAtasanBerwenang = $usulan->approvalAtasanBerwenang ?? null;
                                                $approvalAtasanLangsung = $usulan->approvalAtasanLangsung ?? null;

                                                // 1=simpan draf
                                                // 3=tolak
                                                // persalinan ke increment
                                                if (in_array($usulan->status, [0, 1, 3]) && in_array($usulan->jnsCuti->jnsCuti, [3])) {
                                                    $cutiMelahirkanDiajukan++;
                                                }

                                                // 2=ajukan cuti
                                                // buat kondisi untuk menampilkan cuti melahirkan terakhir
                                                if (in_array($usulan->status, [2]) && in_array($usulan->jnsCuti->jnsCuti, [3])) {
                                                    $_persalinanKe = $persalinanKe;
                                                }

                                                $text = 'text-dark';
                                                if (2 == $usulan->status) {
                                                    $text = 'text-success';
                                                } elseif (preg_match('/4|5/i', $usulan->status)) {
                                                    $text = 'text-warning';
                                                } elseif (preg_match('/6/i', $usulan->status)) {
                                                    $text = 'text-danger';
                                                } elseif (preg_match('/1/i', $usulan->status)) {
                                                    $text = 'text-primary';
                                                }
                                                ?>
                                            <tr id="cuti{{ $usulan->id }}">
                                                <td class="text-center">{{ $no }}</td>
                                                <td>{{ $usulan->nomorTicket }}</td>
                                                <td>{{ $usulan->jnsCuti->namaCuti }}</td>
                                                <td>
                                                    {{ AppHelper::convertDateDMY($usulan->tanggalMulai) }}
                                                    &nbsp - &nbsp
                                                    {{ AppHelper::convertDateDMY($usulan->tanggalSelesai) }}
                                                    <hr/>
                                                    ({{ $usulan->lamaCuti }} hari)
                                                </td>
                                                <td>
                                                    {{ AppHelper::convertDateDMY($usulan->tanggalBuat) }}
                                                </td>
                                                <td>
                                                    <span class="{{ $text }}">
                                                        {!! $usulan->ketStatus !!}
                                                        @if (in_array($usulan->status, [1]))
                                                            @if(null === $tanggalPersetujuan && 1 == $approvalAtasanBerwenang)
                                                                <span class="text-info">
                                                                    <br/>(Proses Approval UPK)
                                                                </span>
                                                            @elseif(null === $approvalAtasanLangsung)
                                                                <span class="text-info">
                                                                    <br/>(Proses Approval Atasan Langsung)
                                                                </span>
                                                            @elseif(null === $approvalAtasanBerwenang)
                                                                <span class="text-info">
                                                                    <br/>(Proses Approval Atasan Berwenang)
                                                                </span>
                                                            @endif
                                                        @endif
                                                        <br/>
                                                        @if (isset($usulan->catatan) && in_array($usulan->status, [0, 1, 6]))
                                                            <span class="text-info addReadMore showlesscontent">
                                                                <br/>({{ $usulan->catatan }})
                                                            </span>
                                                        @endif
                                                    </span>
                                                </td>
                                                <td class="text-center">
                                                    @if(1 == $hakcutitambahan && 6 == $usulan->jnsCuti->jnsCuti || 1 == $hakcutitambahan && 10 == $usulan->jnsCuti->jnsCuti )
                                                        <button
                                                            class="btn btn-sm btn-icon btn-info btnCutiTambahan mt-1"
                                                            data-id="{{ $usulan->id }}"
                                                            data-nomorticket="{{ $usulan->nomorTicket }}"
                                                            data-url="{{url('cuti/administrasi/showFormEditUsulan')}}"
                                                            data-id-permohonan="{{ $usulan->id }}"
                                                            data-tanggal-cuti="{{ $tanggalLCB }}"
                                                            data-tanggal-mulai="{{ $usulan->tanggalMulai }}"
                                                            data-tanggal-selesai="{{ $usulan->tanggalSelesai }}"
                                                            data-lama-cuti="{{ $usulan->lamaCuti }}"
                                                            data-jns-cuti="{{ $usulan->jnsCuti->id}}"
                                                            data-alasan="{{ $usulan->alasan }}"
                                                            data-phone="{{ $usulan->phone }}"
                                                            data-alamat-sementara="{{ $usulan->alamatSementara }}"
                                                            data-tanggal-buat="{{ $usulan->tanggalBuat }}"
                                                            data-ctt="{{ $usulan->permohonanCutiTambahan->id ?? "" }}"
                                                            data-bs-toggle="tooltip" data-bs-placement="top"
                                                            title="Permohonan Cuti Tambahan">
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                    @endif
                                                    @if (0 == $usulan->status)
                                                        <button
                                                            class="btn btn-sm btn-icon btn-warning btnEditUsulan mt-1"
                                                            data-id="{{ $usulan->id }}"
                                                            data-url="{{ url('cuti/administrasi/showFormEditUsulan') }}"
                                                            data-id-permohonan="{{ $usulan->id }}"
                                                            data-tanggal-cuti="{{ $tanggalLCB }}"
                                                            data-lama-cuti="{{ $usulan->lamaCuti }}"
                                                            data-jns-cuti="{{ $usulan->jnsCuti->jnsCuti }}"
                                                            data-alasan="{{ $usulan->alasan }}"
                                                            data-phone="{{ $usulan->phone }}"
                                                            data-alamat-sementara="{{ $usulan->alamatSementara }}"
                                                            data-persalinan-ke="{{ $persalinanKe }}"
                                                            data-bs-toggle="tooltip" data-bs-placement="top"
                                                            title="Edit">
                                                            <i class="fa fa-edit"></i>
                                                        </button>

                                                        <button
                                                            class="btn btn-sm btn-icon btn-danger btnHapusUsulan mt-1"
                                                            data-url="{{url('cuti/deletePermohonan?string='.$_GET['string'])}}"
                                                            data-id="{{ $usulan->id }}"
                                                            data-tahun="{{ $usulan->tahun }}"
                                                            data-lama-cuti="{{ $usulan->lamaCuti }}"
                                                            data-bs-toggle="tooltip"
                                                            data-bs-placement="top" title="Hapus">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                        <br/>
                                                        <button class="btn btn-sm btn-primary btnKirimUsulan mt-1"
                                                                data-id="{{ $usulan->id }}"
                                                                data-url="{{url('cuti/kirimPermohonan?string='.$_GET['string'])}}"
                                                                data-bs-toggle="tooltip" data-bs-placement="top"
                                                                title="Kirim">
                                                            <i class="fa fa-paper-plane"></i> Kirim
                                                        </button>
                                                    @endif
                                                    <a href="{{url('cuti/print?string='.$_GET['string'].'&key='.$usulan->id)}}"
                                                       target="_blank" class="btn btn-sm btn-success btn-icon">
                                                        <i class="bi bi-printer-fill text-white fs-2"></i>
                                                    </a>
                                                    <br/>
                                                    @if (strpos(implode(', ', $userRole),'UPK_PUSAT') && empty($usulan->cutiPegawai->id))
                                                        <button class="btn btn-sm btn-primary btnSesuaikanData mt-1"
                                                                data-id="{{ $usulan->id }}"
                                                                data-url="{{url('cuti/sesuaikanPermohonan?string='.$_GET['string'])}}"
                                                                data-jns-cuti="{{ $usulan->jnsCuti->jnsCuti }}"
                                                                data-bs-toggle="tooltip" data-bs-placement="top"
                                                                title="Sesuaikan Data">
                                                            <i class="bi bi-wrench-adjustable-circle-fill text-white fs-2"></i>
                                                            Sesuaikan Data
                                                        </button>
                                                    @endif
                                                </td>
                                            </tr>
                                                <?php $no++; ?>
                                        @endforeach
                                        </tbody>
                                        <!--end::Table body-->
                                    </table>
                                </div>

                                <!--end::Table-->
                                @if ($totalItems != 0)
                                    <!--begin::pagination-->
                                    <div class="d-flex flex-stack flex-wrap pt-5 p-3">
                                        <div class="fs-6 fw-bold text-gray-700"></div>

                                        @php
                                            $disabled = '';
                                            $nextPage= '';
                                            $disablednext = '';
                                            $hidden = '';
                                            $pagination = '';

                                            // tombol pagination
                                            if ($totalItems == 0) {
                                                // $pagination = 'none';
                                            }

                                            // tombol back
                                            if($currentPage == 1){
                                                $disabled = 'disabled';
                                            }

                                            // tombol next
                                            if($currentPage != $totalPages) {
                                                $nextPage = $currentPage + 1;
                                            }

                                            // tombol pagination
                                            if($currentPage == $totalPages || $totalPages == 0){
                                                $disablednext = 'disabled';
                                                $hidden = 'hidden';
                                            }

                                        @endphp

                                        <ul class="pagination" style="display: {{ $pagination }}">
                                            <li class="page-item disabled"><a href="#"
                                                                              class="page-link">Halaman {{ $currentPage }}
                                                    dari {{ $totalPages }}</a></li>

                                            <li class="page-item previous {{ $disabled }}"><a
                                                    href="{{ url()->current() . '?page=' . ($currentPage - 1) }}"
                                                    class="page-link"><i class="previous"></i></a></li>
                                            <li class="page-item active"><a
                                                    href="{{ url()->current() . '?page=' . $currentPage }}"
                                                    class="page-link">{{ $currentPage }}</a></li>
                                            <li class="page-item" {{ $hidden }}><a
                                                    href="{{ url()->current() . '?page=' . $nextPage }}"
                                                    class="page-link">{{ $nextPage }}</a></li>
                                            <li class="page-item next {{ $disablednext }}"><a
                                                    href="{{ url()->current() . '?page=' . $nextPage }}"
                                                    class="page-link"><i class="next"></i></a></li>
                                        </ul>

                                    </div>
                                @endif
                                <!-- end::Pagination -->
                            </div>
                            <!--end::Card body-->
                        </div>
                        <!--end::Card-->
                        <!--begin::Modal - Add permohonan-->
                        <div class="modal fade" id="kt_modal_permohonan" tabindex="-1" aria-hidden="true">
                            <!--begin::Modal dialog-->
                            <div class="modal-dialog modal-dialog-centered mw-1000px">
                                <!--begin::Modal content-->
                                <div class="modal-content">
                                    <!--begin::Modal header-->
                                    <div class="modal-header" style="background-color:#1d428a;">
                                        <!--begin::Modal title-->
                                        <h2 class="fw-bolder text-white">Form Permohonan Cuti Pegawai </h2>
                                        <!--end::Modal title-->
                                        <!--begin::Close-->
                                        <div class="btn btn-icon btn-sm btn-active-icon-primary"
                                             data-bs-dismiss="modal">
                                            <!--begin::Svg Icon | path: icons/duotone/Navigation/Close.svg-->
                                            <span class="svg-icon svg-icon-1">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                                                   fill="#000000">
                                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                                    <rect fill="#000000" opacity="0.5"
                                                          transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                                          x="0" y="7" width="16" height="2" rx="1"/>
                                                </g>
                                            </svg>
                                        </span>
                                            <!--end::Svg Icon-->
                                        </div>
                                        <!--end::Close-->
                                    </div>
                                    <!--end::Modal header-->
                                    <!--begin::Modal body-->
                                    <div class="modal-body scroll-y mx-5 mx-xl-15 my-5">
                                        <!--begin::Form-->
                                        <form class="form w-lg-500px mx-auto" id="kt_basic_form"
                                              action="{{ url('/cuti/createPermohonan?string='.$_GET['string']) }}"
                                              method="post">
                                            @csrf

                                            {{-- menampilkan error validasi --}}
                                            @if (count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif

                                            <!--begin::Input group-->
                                            <div class="d-flex flex-column mb-5 fv-row">
                                                <!--begin::Label-->
                                                <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                    <span class="required">Jenis Cuti</span>
                                                </label>
                                                <!--end::Label-->
                                                <!--begin::Select-->
                                                <select class="form-select" data-control="select2"
                                                        data-placeholder="Select an option" data-allow-clear="true"
                                                        data-dropdown-parent="#kt_modal_permohonan" name="jnsCuti"
                                                        id="jnsCuti">
                                                    <option></option>
                                                    @foreach ($jenisCuti as $jenis)
                                                        <option data-id="{{ $jenis->id }}"
                                                                value="{{ $jenis->jnsCuti }}" {{ (('6' == $jenis->jnsCuti)? "selected" : "") }}>{{ $jenis->namaCuti }}</option>
                                                    @endforeach
                                                </select>
                                                <input type="hidden" class="form-control form-control-solid"
                                                       name="idJnsCuti" id="idJnsCuti" placeholder="" value=""/>
                                                <!--end::Select-->
                                            </div>
                                            <!--end::Input group-->

                                            <!--begin::Input group-->
                                            <div class="d-flex flex-column mb-5 fv-row fieldTipeCutiHalf">
                                                <!--begin::Label-->
                                                <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                    <span class="required">Jenis Cuti 1/2 Hari</span>
                                                </label>
                                                <!--end::Label-->

                                                <!--begin::Select-->
                                                <select class="form-select" data-control="select2"
                                                        data-placeholder="Select an option" data-allow-clear="true"
                                                        data-dropdown-parent="#kt_modal_permohonan" name="tipeCutiHalf"
                                                        id="tipeCutiHalf">
                                                    <option value="0">Pagi</option>
                                                    <option value="1">Sore</option>
                                                </select>
                                                <!--end::Select-->
                                            </div>
                                            <!--end::Input group-->

                                            <!--begin::Input group-->
                                            <div class="fv-row mb-8" id="CutiSalinMuncul" hidden>
                                                <!--begin::Label-->
                                                <label class="required fs-6 fw-bold mb-2">Persalinan ke</label>
                                                <!--end::Label-->
                                                <!--begin::Input-->
                                                <input type="text" class="form-control form-control-solid"
                                                       name="persalinanKe" id="persalinanKe" readonly/>
                                                <!--end::Input-->
                                            </div>
                                            <!--end::Input group-->

                                            <!--begin::Input group-->
                                            <div class="fv-row mb-10">
                                                <!--begin::Label-->
                                                <label class="required fs-6 fw-bold mb-2">Tanggal Cuti</label>
                                                <!--end::Label-->
                                                <!--begin::Wrapper-->
                                                <div class="position-relative d-flex align-items-center">
                                                    <!--begin::Icon-->
                                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                        <span class="symbol-label bg-secondary">
                                                            <!--begin::Svg Icon | path: icons/duotone/Layout/Layout-grid.svg-->
                                                            <span class="svg-icon">
                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                     width="24px"
                                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                                    <g stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd">
                                                                        <rect x="0" y="0" width="24" height="24"/>
                                                                        <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                              width="4" height="4" rx="1"/>
                                                                        <path
                                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                            fill="#000000"/>
                                                                    </g>
                                                                </svg>
                                                            </span>
                                                            <!--end::Svg Icon-->
                                                        </span>
                                                    </div>
                                                    <!--end::Icon-->
                                                    <!--begin::Input-->
                                                    <input class="form-control form-control-solid ps-12"
                                                           placeholder="Pick date rage" id="tanggalCuti"
                                                           name="tanggalCuti"
                                                           value=""/>
                                                    <input type="hidden" class="form-control form-control-solid"
                                                           name="idPermohonan" id="idPermohonan" placeholder=""
                                                           value=""/>
                                                    <input type="hidden" class="form-control form-control-solid"
                                                           name="idRekapCutiYmin1" id="idRekapCutiYmin1" placeholder=""
                                                           value=""/>
                                                    <!--end::Input-->
                                                </div>
                                                <!--end::Wrapper-->
                                            </div>
                                            <!--end::Input group-->

                                            <!--begin::Input group-->
                                            <div class="fv-row mb-10 ">
                                                <!--begin::Label-->
                                                <label class="form-label">Lama Cuti</label>
                                                <!--end::Label-->

                                                <!--begin::Input-->
                                                <input type="text" class="form-control form-control-solid"
                                                       name="lamaCuti" id="lamaCuti" readonly="readonly"/>
                                                <!--end::Input-->
                                            </div>
                                            <!--end::Input group-->

                                            <!--begin::Input group-->
                                            <div class="fv-row mb-8 fieldAlasanCuti" id="Alasan" hidden>
                                                <!--begin::Label-->
                                                <label class="required fs-6 fw-bold mb-2">Alasan Cuti</label>
                                                <!--end::Label-->
                                                <!--begin::Input-->
                                                <textarea class="form-control form-control-solid" rows="3"
                                                          placeholder="Mohon isi penjelasan" id="alasan"
                                                          name="alasan">{{ old('alasan') }}</textarea>
                                                <!--end::Input-->
                                            </div>
                                            <!--end::Input group-->

                                            <!--begin::Input group-->
                                            <div class="fv-row mb-8">
                                                <!--begin::Label-->
                                                <label class="required fs-6 fw-bold mb-2">Alamat Selama Cuti</label>
                                                <!--end::Label-->
                                                <!--begin::Input-->
                                                <textarea class="form-control form-control-solid" rows="3"
                                                          placeholder="Mohon isi alamat selama cuti berlangsung"
                                                          id="alamatSementara"
                                                          name="alamatSementara">{{ old('alamatSementara') }}</textarea>
                                                <!--end::Input-->
                                            </div>
                                            <!--end::Input group-->

                                            <!--begin::Input group-->
                                            <div class="fv-row mb-10">
                                                <!--begin::Label-->
                                                <label class="form-label">Nomor Telepon Yang Dapat Dihubungi</label>
                                                <!--end::Label-->

                                                <!--begin::Input-->
                                                <input type="number" class="form-control form-control-solid"
                                                       name="phone" id="phone" placeholder=""/>
                                                <!--end::Input-->
                                            </div>
                                            <!--end::Input group-->

                                            <!--begin::Input group-->
                                            <div class="fv-row mb-8">
                                                <button type="button" class="btn btn-primary" data-action="submit"
                                                        id="btn-submit">
                                                <span class="indicator-label">
                                                    Submit
                                                </span>
                                                    <span class="indicator-progress">
                                                    Please wait... <span
                                                            class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                                </span>
                                                </button>
                                            </div>
                                            <!--end::Input group-->
                                        </form>
                                    </div>
                                    <!--end::Modal body-->
                                </div>
                                <!--end::Modal content-->
                            </div>
                            <!--end::Modal dialog-->
                        </div>
                        <!--end::Modal - Add permohonan-->
                        <!--begin::Modal - Add permohonan cuti tambahan-->
                        <div class="modal fade" data-backdrop-limit="1" tabindex="-1" id="kt_modal_1">
                            <!--begin::Modal dialog-->
                            <div class="modal-dialog modal-dialog-centered mw-1000px">
                                <!--begin::Modal content-->
                                <div class="modal-content">
                                    <!--begin::Modal header-->
                                    <div class="modal-header">
                                        <!--begin::Modal title-->
                                        <h2 class="fw-bolder text-white">Form Permohonan Cuti Tambahan Pegawai </h2>
                                        <!--end::Modal title-->
                                        <!--begin::Close-->
                                        <div class="btn btn-icon btn-sm btn-active-icon-primary"
                                             data-bs-dismiss="modal">
                                            <!--begin::Svg Icon | path: icons/duotone/Navigation/Close.svg-->
                                            <span class="svg-icon svg-icon-1">
                                           <svg xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                viewBox="0 0 24 24" version="1.1">
                                               <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                                                  fill="#000000">
                                                   <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                                   <rect fill="#000000" opacity="0.5"
                                                         transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                                         x="0" y="7" width="16" height="2" rx="1"/>
                                               </g>
                                           </svg>
                                       </span>
                                            <!--end::Svg Icon-->
                                        </div>
                                        <!--end::Close-->
                                    </div>
                                    <!--end::Modal header-->
                                    <!--begin::Modal body-->
                                    <div class="modal-body scroll-y mx-5 mx-xl-15 my-5">
                                        <!--begin::Stepper-->
                                        <div class="stepper stepper-pills" id="kt_stepper_example_basic">
                                            <!--begin::Nav-->
                                            <div class="stepper-nav flex-center flex-wrap mb-10">
                                                <!--begin::Step 1-->
                                                <div class="stepper-item mx-2 my-4 current"
                                                     data-kt-stepper-element="nav">
                                                    <!--begin::Line-->
                                                    <div class="stepper-line w-40px"></div>
                                                    <!--end::Line-->

                                                    <!--begin::Icon-->
                                                    <div class="stepper-icon w-40px h-40px">
                                                        <i class="stepper-check fas fa-check"></i>
                                                        <span class="stepper-number">1</span>
                                                    </div>
                                                    <!--end::Icon-->

                                                    <!--begin::Label-->
                                                    <div class="stepper-label">
                                                        <h3 class="stepper-title">
                                                            Step 1
                                                        </h3>

                                                        <div class="stepper-desc">
                                                            <medium>Cek Cuti Tahunan</medium>
                                                        </div>
                                                    </div>
                                                    <!--end::Label-->
                                                </div>
                                                <!--end::Step 1-->

                                                <!--begin::Step 2-->
                                                <div class="stepper-item mx-2 my-4" data-kt-stepper-element="nav">
                                                    <!--begin::Line-->
                                                    <div class="stepper-line w-40px"></div>
                                                    <!--end::Line-->

                                                    <!--begin::Icon-->
                                                    <div class="stepper-icon w-40px h-40px">
                                                        <i class="stepper-check fas fa-check"></i>
                                                        <span class="stepper-number">2</span>
                                                    </div>
                                                    <!--begin::Icon-->

                                                    <!--begin::Label-->
                                                    <div class="stepper-label">
                                                        <h3 class="stepper-title">
                                                            Step 2
                                                        </h3>

                                                        <div class="stepper-desc">
                                                            <medium>Input Cuti Tambahan</medium>
                                                        </div>
                                                    </div>
                                                    <!--end::Label-->
                                                </div>
                                                <!--end::Step 2-->

                                                <!--begin::Step 2-->
                                                <div class="stepper-item mx-2 my-4" data-kt-stepper-element="nav">
                                                    <!--begin::Line-->
                                                    <div class="stepper-line w-40px"></div>
                                                    <!--end::Line-->

                                                    <!--begin::Icon-->
                                                    <div class="stepper-icon w-40px h-40px">
                                                        <i class="stepper-check fas fa-check"></i>
                                                        <span class="stepper-number">3</span>
                                                    </div>
                                                    <!--begin::Icon-->

                                                    <!--begin::Label-->
                                                    <div class="stepper-label">
                                                        <h3 class="stepper-title">
                                                            Step 3
                                                        </h3>

                                                        <div class="stepper-desc">
                                                            <medium>Completed</medium>
                                                        </div>
                                                    </div>
                                                    <!--end::Label-->
                                                </div>
                                                <!--end::Step 2-->

                                            </div>
                                            <!--end::Nav-->

                                            <!--begin::Form-->
                                            <form class="form w-lg-500px mx-auto" novalidate="novalidate"
                                                  id="kt_stepper_example_basic_form"
                                                  action="{{ url('/cuti/createPermohonanCutiTambahan?string='.$_GET['string']) }}"
                                                  method="post">
                                                @csrf

                                                {{-- menampilkan error validasi --}}
                                                @if (count($errors) > 0)
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                                <!--begin::Group-->
                                                <div class="mb-5">
                                                    <!--begin::Step 1-->
                                                    <div class="flex-column current" data-kt-stepper-element="content">
                                                        <!--begin::Input group-->
                                                        <div class="fv-row mb-10">
                                                            <!--begin::Label-->
                                                            <label class="form-label">Nomor Ticket</label>
                                                            <!--end::Label-->

                                                            <!--begin::Input-->
                                                            <input type="text" class="form-control form-control-solid"
                                                                   name="nomotTicket" id="nomorTicket"
                                                                   readonly="readonly"/>
                                                            <!--end::Input-->
                                                        </div>
                                                        <!--end::Input group-->
                                                        <!--begin::Input group-->
                                                        <div class="fv-row mb-10">
                                                            <!--begin::Label-->
                                                            <label class="required fs-6 fw-bold mb-2">Tanggal
                                                                Cuti</label>
                                                            <!--end::Label-->
                                                            <!--begin::Wrapper-->
                                                            <div class="position-relative d-flex align-items-center">
                                                                <!--begin::Icon-->
                                                                <div
                                                                    class="symbol symbol-20px me-4 position-absolute ms-4">
                                                                <span class="symbol-label bg-secondary">
                                                                    <!--begin::Svg Icon | path: icons/duotone/Layout/Layout-grid.svg-->
                                                                    <span class="svg-icon">
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                             width="24px" height="24px"
                                                                             viewBox="0 0 24 24" version="1.1">
                                                                            <g stroke="none" stroke-width="1"
                                                                               fill="none" fill-rule="evenodd">
                                                                                <rect x="0" y="0" width="24"
                                                                                      height="24"/>
                                                                                <rect fill="#000000" opacity="0.3" x="4"
                                                                                      y="4" width="4" height="4"
                                                                                      rx="1"/>
                                                                                <path
                                                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                                    fill="#000000"/>
                                                                            </g>
                                                                        </svg>
                                                                    </span>
                                                                    <!--end::Svg Icon-->
                                                                </span>
                                                                </div>
                                                                <!--end::Icon-->
                                                                <!--begin::Input-->
                                                                <input class="form-control form-control-solid ps-12"
                                                                       placeholder="Pick date rage" id="getTanggalCuti"
                                                                       name="getTanggalCuti" value=""
                                                                       readonly="readonly"/>
                                                                <input type="hidden"
                                                                       class="form-control form-control-solid"
                                                                       name="idPermohonan" id="idPermohonan"
                                                                       placeholder="" value=""/>
                                                                <input type="hidden"
                                                                       class="form-control form-control-solid"
                                                                       name="idCutiTambahan" id="idCutiTambahan"
                                                                       placeholder="" value=""/>
                                                                <!--end::Input-->
                                                            </div>
                                                            <!--end::Wrapper-->
                                                        </div>
                                                        <!--end::Input group-->

                                                        <!--begin::Input group-->
                                                        <div class="fv-row mb-10 ">
                                                            <!--begin::Label-->
                                                            <label class="form-label">Lama Cuti</label>
                                                            <!--end::Label-->

                                                            <!--begin::Input-->
                                                            <input type="text" class="form-control form-control-solid"
                                                                   name="getLamaCuti" id="getLamaCuti"
                                                                   readonly="readonly"/>
                                                            <!--end::Input-->
                                                        </div>
                                                        <!--end::Input group-->

                                                        <!--begin::Input group-->
                                                        <div class="fv-row mb-8">
                                                            <!--begin::Label-->
                                                            <label class="required fs-6 fw-bold mb-2">Alasan
                                                                Cuti</label>
                                                            <!--end::Label-->
                                                            <!--begin::Input-->
                                                            <textarea class="form-control form-control-solid" rows="3"
                                                                      placeholder="Mohon isi penjelasan" id="getAlasan"
                                                                      name="getAlasan" readonly="readonly"></textarea>
                                                            <!--end::Input-->
                                                        </div>
                                                        <!--end::Input group-->

                                                        <!--begin::Input group-->
                                                        <div class="fv-row mb-8">
                                                            <!--begin::Label-->
                                                            <label class="required fs-6 fw-bold mb-2">Alamat Selama
                                                                Cuti</label>
                                                            <!--end::Label-->
                                                            <!--begin::Input-->
                                                            <textarea class="form-control form-control-solid" rows="3"
                                                                      placeholder="Mohon isi alamat selama cuti berlangsung"
                                                                      id="getAlamatSementara"
                                                                      name="getAlamatSementara"></textarea>
                                                            <!--end::Input-->
                                                        </div>
                                                        <!--end::Input group-->

                                                        <!--begin::Input group-->
                                                        <div class="fv-row mb-10">
                                                            <!--begin::Label-->
                                                            <label class="form-label">Nomor Telepon Yang Dapat
                                                                Dihubungi</label>
                                                            <!--end::Label-->

                                                            <!--begin::Input-->
                                                            <input type="text" class="form-control form-control-solid"
                                                                   name="getPhone" id="getPhone" placeholder=""/>
                                                            <!--end::Input-->
                                                        </div>
                                                        <!--end::Input group-->
                                                    </div>
                                                    <!--begin::Step 1-->

                                                    <!--begin::Step 2-->
                                                    <div class="flex-column" data-kt-stepper-element="content">
                                                        <!--begin::Input group-->
                                                        <div class="d-flex flex-column mb-5 fv-row">
                                                            <!--begin::Label-->
                                                            <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                                <span class="required">Jenis Cuti Tambahan</span>
                                                            </label>
                                                            <input type="hidden" class="form-control form-control-solid"
                                                                   name="idCutiTambahan" id="idCutiTambahan"
                                                                   placeholder="" value=""/>
                                                            <!--end::Label-->
                                                            <!--begin::Select-->
                                                            <select class="form-select" data-control="select2"
                                                                    data-placeholder="Select an option"
                                                                    data-allow-clear="true"
                                                                    data-dropdown-parent="#kt_stepper_example_basic"
                                                                    name="jenis" id="jenis">
                                                                <option></option>
                                                                @foreach ($jenisCutiTambahan as $jenis)
                                                                    <option
                                                                        value="{{ $jenis->id }}" {{ (('' == $jenis->jenis)? "selected" : "") }}>{{ $jenis->nama }}</option>
                                                                @endforeach
                                                            </select>
                                                            <!--end::Select-->
                                                        </div>

                                                        <div class="fv-row mb-10 fieldTanggalCutiTambahan">
                                                            <!--begin::Label-->
                                                            <label class="required fs-6 fw-bold mb-2">Tanggal Cuti
                                                                Tambahan</label>
                                                            <!--end::Label-->
                                                            <!--begin::Wrapper-->
                                                            <div class="position-relative d-flex align-items-center">
                                                                <!--begin::Icon-->
                                                                <div
                                                                    class="symbol symbol-20px me-4 position-absolute ms-4">
                                                                <span class="symbol-label bg-secondary">
                                                                    <!--begin::Svg Icon | path: icons/duotone/Layout/Layout-grid.svg-->
                                                                    <span class="svg-icon">
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                             width="24px" height="24px"
                                                                             viewBox="0 0 24 24" version="1.1">
                                                                            <g stroke="none" stroke-width="1"
                                                                               fill="none" fill-rule="evenodd">
                                                                                <rect x="0" y="0" width="24"
                                                                                      height="24"/>
                                                                                <rect fill="#000000" opacity="0.3" x="4"
                                                                                      y="4" width="4" height="4"
                                                                                      rx="1"/>
                                                                                <path
                                                                                    d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                                    fill="#000000"/>
                                                                            </g>
                                                                        </svg>
                                                                    </span>
                                                                    <!--end::Svg Icon-->
                                                                </span>
                                                                </div>
                                                                <!--end::Icon-->
                                                                <!--begin::Input-->
                                                                <input class="form-control form-control-solid ps-12"
                                                                       placeholder="Pick date rage"
                                                                       id="tanggalCutiTambahan"
                                                                       name="tanggalCutiTambahan" value=""/>
                                                                <input type="hidden"
                                                                       class="form-control form-control-solid"
                                                                       name="getIdPermohonan" id="getIdPermohonan"
                                                                       placeholder="" value=""/>
                                                                <input type="hidden"
                                                                       class="form-control form-control-solid"
                                                                       name="idCutiTambahan" id="idCutiTambahan"
                                                                       placeholder="" value=""/>
                                                                <input type="hidden"
                                                                       class="form-control form-control-solid"
                                                                       name="ctt" id="ctt" placeholder="" value=""/>
                                                                <!--end::Input-->
                                                            </div>
                                                            <!--end::Wrapper-->
                                                        </div>
                                                        <!--end::Input group-->
                                                        <!--begin::Input group-->
                                                        <div class="fv-row mb-10 ">
                                                            <!--begin::Label-->
                                                            <label class="form-label">Lama Cuti Tambahan</label>
                                                            <!--end::Label-->

                                                            <!--begin::Input-->
                                                            <input type="text" class="form-control form-control-solid"
                                                                   name="lamaCutiTambahan" id="lamaCutiTambahan"
                                                                   readonly="readonly"/>
                                                            <!--end::Input-->
                                                        </div>
                                                        <!--end::Input group-->

                                                        <!--begin::Input group-->
                                                        <div class="fv-row mb-10">
                                                            <!--begin::Label-->
                                                            <label class="form-label">Alasan Cuti Tambahan</label>
                                                            <!--end::Label-->

                                                            <!--begin::Input-->
                                                            <label
                                                                class="form-check form-check-custom form-check-solid mb-2">
                                                                <input class="form-check-input" type="checkbox"
                                                                       @if (isset($cutiTambahan))
                                                                           {{ (('Mengunjungi Orang Tua' == $cutiTambahan->alasanCutiTambahan)? "checked" : "") }}
                                                                       @endif
                                                                       name="alasanCutiTambahan" id="alasanCutiTambahan"
                                                                       value="Mengunjungi Orang Tua"/>
                                                                <span class="form-check-label">
                                                                <medium>Mengunjungi Orang Tua</medium>
                                                            </span>
                                                            </label>
                                                            <!--end::Input-->
                                                            <!--begin::Input-->
                                                            <label
                                                                class="form-check form-check-custom form-check-solid mb-2">
                                                                <input class="form-check-input" type="checkbox"
                                                                       @if (isset($cutiTambahan))
                                                                           {{ (('Mengunjungi Suami/Isteri' == $cutiTambahan->alasanCutiTambahan)? "checked" : "") }}
                                                                       @endif
                                                                       name="alasanCutiTambahan" id="alasanCutiTambahan"
                                                                       value="Mengunjungi Suami/Isteri"/>

                                                                <span class="form-check-label">
                                                                <medium>Mengunjungi Suami/Isteri</medium>
                                                            </span>
                                                            </label>
                                                            <!--end::Input-->
                                                            <!--begin::Input-->
                                                            <label
                                                                class="form-check form-check-custom form-check-solid mb-2">
                                                                <input class="form-check-input" type="checkbox"
                                                                       @if (isset($cutiTambahan))
                                                                           {{ (('Mengunjungi Anak' == $cutiTambahan->alasanCutiTambahan)? "checked" : "") }}
                                                                       @endif
                                                                       name="alasanCutiTambahan" id="alasanCutiTambahan"
                                                                       value="Mengunjungi Anak"/>

                                                                <span class="form-check-label">
                                                                <medium>Mengunjungi Anak</medium>
                                                            </span>
                                                            </label>
                                                            <!--end::Input-->
                                                        </div>
                                                        <!--end::Input group-->
                                                    </div>
                                                    <!--begin::Step 1-->

                                                    <!--begin::Step 1-->
                                                    <div class="flex-column" data-kt-stepper-element="content">
                                                        <!--begin::Wrapper-->
                                                        <div class="w-100">
                                                            <!--begin::Heading-->
                                                            <div class="pb-12 text-center">
                                                                <!--begin::Title-->
                                                                <h1 class="fw-bolder text-dark">Permohonan Selesai
                                                                    Dibuat!</h1>
                                                                <!--end::Title-->
                                                                <!--begin::Description-->
                                                                <div class="text-muted fw-bold fs-4">Silahkan klik
                                                                    submit untuk mengirim permohonan cuti tambahan
                                                                    Bapak/Ibu
                                                                </div>
                                                                <!--end::Description-->
                                                            </div>
                                                            <!--end::Heading-->
                                                        </div>
                                                        <!--end::Wrapper-->
                                                    </div>
                                                    <!--begin::Step 1-->
                                                </div>
                                                <!--end::Group-->

                                                <!--begin::Actions-->
                                                <div class="d-flex flex-stack">
                                                    <!--begin::Wrapper-->
                                                    <div class="me-2">
                                                        <button type="button"
                                                                class="btn btn-light btn-active-light-primary"
                                                                data-kt-stepper-action="previous">
                                                            Back
                                                        </button>
                                                    </div>
                                                    <!--end::Wrapper-->

                                                    <!--begin::Wrapper-->
                                                    <div>
                                                        <button type="button" class="btn btn-primary" id="btn-submit"
                                                                data-kt-stepper-action="submit">
                                                        <span class="indicator-label">
                                                            Submit
                                                        </span>
                                                            <span class="indicator-progress">
                                                            Please wait... <span
                                                                    class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                                        </span>
                                                        </button>

                                                        <button type="button" class="btn btn-primary" id="btn-continue"
                                                                data-kt-stepper-action="next">
                                                            Continue
                                                        </button>
                                                    </div>
                                                    <!--end::Wrapper-->
                                                </div>
                                                <!--end::Actions-->
                                            </form>
                                            <!--end::Form-->
                                        </div>
                                        <!--end::Stepper-->
                                    </div>
                                    <!--end::Modal body-->
                                </div>
                                <!--end::Modal content-->
                            </div>
                            <!--end::Modal dialog-->
                        </div>
                        <!--end::Modal - Add permohonan cuti tambahan-->
                    </div>
                    <div class="tab-pane fade" id="kt_permohonan_ctt" role="tabpanel">
                        <!--begin::Card-->
                        <div class="card card-flush">
                            <!--begin::Card header-->
                            <div class="card-header mt-6">
                                <!--begin::Card title-->
                                <div class="card-title">
                                    <!--begin::Search-->
                                    <div class="d-flex align-items-center position-relative my-1 me-5">

                                    </div>
                                    <!--end::Search-->
                                </div>
                                <!--end::Card title-->
                                <!--begin::Card toolbar-->
                                <div class="card-toolbar">

                                </div>
                                <!--end::Card toolbar-->
                            </div>
                            <!--end::Card header-->
                            <!--begin::Card body-->
                            <div class="card-body pt-0">
                                @if(isset($alert_message))
                                    {!! $alert_message !!}
                                @else
                                    <!--begin::Table-->
                                    <div class="table-responsive rounded border">
                                        <table
                                            class="table align-middle table-striped fs-6 gy-5 mb-0 rounded border border-gray-300"
                                            id="tabel_detail_cuti_tambahan">

                                            <thead class="fw-bold text-gray-600">
                                            <tr class="text-start text-white fw-bolder fs-7 text-uppercase gs-0"
                                                style="background-color:#02275d;">
                                                <th class="text-center min-w-50px">No</th>
                                                <th class="text-center min-w-100px">Nomor Ticket</th>
                                                <th class="text-center min-w-120px">Jenis Cuti Tambahan</th>
                                                <th class="text-center min-w-225px">Tanggal Cuti Tambahan</th>
                                                <th class="text-center min-w-260px">Status</th>
                                                <th class="text-center min-w-100px">Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody class="fw-bold text-gray-600">
                                                <?php $no = $numberOfElements; $cutiTambahanDiajukan = 0; ?>
                                            @foreach ($permohonanCutiTambahan as $cutiTambahan)
                                                    <?php $batal = (isset($cutiTambahan->batal)) ? $cutiTambahan->batal : ''; ?>
                                                <tr id="cuti{{ $cutiTambahan->id }}"
                                                    class="{{ ('' != $batal) ? "text-danger":"" }}">
                                                    @if (isset($cutiTambahan->jenis))
                                                            <?php
                                                            $tanggalLCBTambahan = AppHelper::convertDateMDY($cutiTambahan->tanggalMulai) . ' - ' . AppHelper::convertDateMDY($cutiTambahan->tanggalSelesai);
                                                            if (($cutiTambahan->status == 1 || $cutiTambahan->status == 0)) {
                                                                $cutiTambahanDiajukan++;
                                                            }
                                                            $tanggalPersetujuan = $cutiTambahan->tanggalPersetujuan ?? null;
                                                            $approvalAtasanBerwenang = $cutiTambahan->approvalAtasanBerwenang ?? null;
                                                            $approvalAtasanLangsung = $cutiTambahan->approvalAtasanLangsung ?? null;

                                                            $text = 'text-dark';
                                                            if (2 == $cutiTambahan->status) {
                                                                $text = 'text-success';
                                                            } else if (preg_match('/3|5/i', $cutiTambahan->status)) {
                                                                $text = 'text-warning';
                                                            } else if (preg_match('/4|6/i', $cutiTambahan->status)) {
                                                                $text = 'text-danger';
                                                            } else if (preg_match('/1/i', $cutiTambahan->status)) {
                                                                $text = 'text-primary';
                                                            }
                                                            ?>
                                                        <td class="text-center">{{ $no }}</td>
                                                        <td class="text-center">{{$cutiTambahan->permohonanCuti->nomorTicket ?? ""}}</td>
                                                        <td class="text-center">{{$cutiTambahan->jenis->nama ?? ""}}</td>
                                                        <td>
                                                            {{AppHelper::convertDateDMY($cutiTambahan->tanggalMulai ?? "") }}
                                                            &nbsp - &nbsp
                                                            {{AppHelper::convertDateDMY($cutiTambahan->tanggalSelesai ?? "") }}
                                                            <hr/>
                                                            ({{ $cutiTambahan->lamaCutiTambahan ?? "" }} hari)
                                                        </td>
                                                        <td>
                                                    <span class="{{ $text }}">
                                                        {!! $cutiTambahan->ketStatus !!}
                                                        @if (in_array($cutiTambahan->status, [1]))
                                                            @if(null === $tanggalPersetujuan && 1 == $approvalAtasanBerwenang)
                                                                <span class="text-info">
                                                                    <br/>(Proses Approval UPK)
                                                                </span>
                                                            @elseif(null === $approvalAtasanLangsung)
                                                                <span class="text-info">
                                                                    <br/>(Proses Approval Atasan Langsung)
                                                                </span>
                                                            @elseif(null === $approvalAtasanBerwenang)
                                                                <span class="text-info">
                                                                    <br/>(Proses Approval Atasan Berwenang)
                                                                </span>
                                                            @endif
                                                        @endif
                                                        <br/>
                                                        @if (isset($cutiTambahan->catatan) && in_array($cutiTambahan->status, [0, 1, 6]))
                                                            <span class="text-info addReadMore showlesscontent">
                                                                <br/>({{ $cutiTambahan->catatan }})
                                                            </span>
                                                        @endif
                                                    </span>
                                                        </td>
                                                        <td class="text-center">
                                                            @if((0 == $cutiTambahan->status))
                                                                <button
                                                                    class="btn btn-sm btn-icon btn-warning btnEditCutiTambahan mt-1"
                                                                    data-id="{{ $cutiTambahan->permohonanCuti->id }}"
                                                                    data-nomorticket="{{ $cutiTambahan->permohonanCuti->nomorTicket }}"
                                                                    data-url="{{url('cuti/administrasi/showFormEditUsulan')}}"
                                                                    data-id-permohonan="{{ $cutiTambahan->permohonanCuti->id }}"
                                                                    data-tanggal-cuti-tambahan="{{ $tanggalLCBTambahan}}"
                                                                    data-tanggal-mulai="{{ $cutiTambahan->permohonanCuti->tanggalMulai }}"
                                                                    data-tanggal-selesai="{{ $cutiTambahan->permohonanCuti->tanggalSelesai }}"
                                                                    data-lama-cuti="{{ $cutiTambahan->permohonanCuti->lamaCuti }}"
                                                                    data-alasan="{{ $cutiTambahan->permohonanCuti->alasan }}"
                                                                    data-phone="{{ $cutiTambahan->permohonanCuti->phone }}"
                                                                    data-alamat-sementara="{{ $cutiTambahan->permohonanCuti->alamatSementara }}"
                                                                    data-tanggal-buat="{{ $cutiTambahan->permohonanCuti->tanggalBuat }}"
                                                                    data-id-cuti-tambahan="{{ $cutiTambahan->id ?? ""}}"
                                                                    data-jenis-cuti-tambahan="{{$cutiTambahan->jenis->id ?? ""}}"
                                                                    data-tanggalmulai-cuti-tambahan="{{ $cutiTambahan->tanggalMulai ?? ""}}"
                                                                    data-tanggalselesai-cuti-tambahan="{{ $cutiTambahan->tanggalSelesai ?? "" }}"
                                                                    data-lama-cuti-tambahan="{{$cutiTambahan->lamaCutiTambahan ?? ""}}"
                                                                    data-alasan-cuti-tambahan="{{$cutiTambahan->alasanCutiTambahan ?? ""}}"
                                                                    data-bs-toggle="tooltip" data-bs-placement="top"
                                                                    title="Edit Cuti Tambahan">
                                                                    <i class="fa fa-edit"></i>
                                                                </button>
                                                                <button
                                                                    class="btn btn-sm btn-icon btn-danger btnHapusCutiTambahan mt-1"
                                                                    data-url="{{url('cuti/deletePermohonanCutiTambahan?string='.$_GET['string'])}}"
                                                                    data-id="{{ $cutiTambahan->id }}"
                                                                    data-bs-toggle="tooltip" data-bs-placement="top"
                                                                    title="Hapus">
                                                                    <i class="fa fa-trash"></i>
                                                                </button>
                                                                <button
                                                                    class="btn btn-sm btn-primary btnKirimCutiTambahan mt-1"
                                                                    data-id="{{ $cutiTambahan->id }}"
                                                                    data-url="{{url('cuti/kirimPermohonanCutiTambahan?string='.$_GET['string'])}}"
                                                                    data-bs-toggle="tooltip" data-bs-placement="top"
                                                                    title="Kirim">
                                                                    <i class="fa fa-paper-plane"></i> Kirim
                                                                </button>
                                                                <br>
                                                                @if (strpos(implode(', ', $userRole),'UPK_PUSAT') && (2 != $cutiTambahan->status))
                                                                    <button
                                                                        class="btn btn-sm btn-primary btnSesuaikanDataTambahan mt-1"
                                                                        data-id-tambahan="{{ $cutiTambahan->id }}"
                                                                        data-url-tambahan="{{url('cuti/sesuaikanPermohonanCutiTambahan?string='.$_GET['string'])}}"
                                                                        data-jns-cuti=tambahan="{{ $cutiTambahan->jenis->id }}"
                                                                        data-bs-toggle="tooltip" data-bs-placement="top"
                                                                        title="Sesuaikan Data">
                                                                        <i class="bi bi-wrench-adjustable-circle-fill text-white fs-2"></i>
                                                                        Sesuaikan Data
                                                                    </button>
                                                                @endif
                                                            @endif
                                                            @if(2 == $cutiTambahan->status)
                                                                <button
                                                                    class="btn btn-sm btn-icon btn-danger btnBatalTambahan mt-1"
                                                                    data-id="{{ $cutiTambahan->id }}"
                                                                    data-url="{{url('cuti/permohonanPembatalanCutiTambahan')}}"
                                                                    data-id-permohonan-tambahan="{{ $cutiTambahan->permohonanCuti->id }}"
                                                                    data-bs-toggle="tooltip" data-bs-placement="top"
                                                                    title="Batal">
                                                                    <i class="bi bi-x-octagon text-white fs-2x"></i>
                                                                </button>
                                                                <button
                                                                    class="btn btn-sm btn-icon btn-warning btnPanggilanDinasTambahan mt-1"
                                                                    data-id="{{ $cutiTambahan->id }}"
                                                                    data-url="{{url('cuti/panggilanDinasTambahan')}}"
                                                                    data-id-permohonan-tambahan="{{ $cutiTambahan->permohonanCuti->id }}"
                                                                    data-bs-toggle="tooltip" data-bs-placement="top"
                                                                    title="Panggilan Dinas">
                                                                    <i class="bi bi-telephone-inbound-fill text-white fs-2x"></i>
                                                                </button>
                                                                <button
                                                                    class="btn btn-sm btn-icon btn-info btnKembaliPersetujuanTambahan mt-1"
                                                                    data-id="{{ $cutiTambahan->id }}"
                                                                    data-url="{{url('cuti/kembaliKePersetujuanTambahan')}}"
                                                                    data-id-permohonan-tambahan="{{ $cutiTambahan->permohonanCuti->id }}"
                                                                    data-bs-toggle="tooltip" data-bs-placement="top"
                                                                    title="Kembalikan Ke Persetujuan">
                                                                    <i class="bi bi-arrow-clockwise text-white fs-2x"></i>
                                                                </button>
                                                            @endif
                                                            @if(1 == $batal)
                                                                <button
                                                                    class="btn btn-sm btn-icon btn-info btnKembaliPersetujuanTambahan mt-1"
                                                                    data-id="{{ $cutiTambahan->id }}"
                                                                    data-url="{{url('cuti/kembaliKePersetujuanTambahan')}}"
                                                                    data-id-permohonan-tambahan="{{ $cutiTambahan->permohonanCuti->id }}"
                                                                    data-bs-toggle="tooltip" data-bs-placement="top"
                                                                    title="Kembalikan Ke Persetujuan">
                                                                    <i class="bi bi-arrow-clockwise text-white fs-2x"></i>
                                                                </button>
                                                            @endif
                                                        </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>



                                    <!--end::Table-->
                                    @if ($totalItems != 0)
                                        <!--begin::pagination-->
                                        <div class="d-flex flex-stack flex-wrap pt-5 p-3">
                                            <div class="fs-6 fw-bold text-gray-700"></div>

                                            @php
                                                $disabled = '';
                                                $nextPage= '';
                                                $disablednext = '';
                                                $hidden = '';
                                                $pagination = '';

                                                // tombol pagination
                                                if ($totalRiwayats == 0) {
                                                    // $pagination = 'none';
                                                }

                                                // tombol back
                                                if($currentPage == 1){
                                                    $disabled = 'disabled';
                                                }

                                                // tombol next
                                                if($currentPage != $totalPagesRiwayats) {
                                                    $nextPage = $currentPage + 1;
                                                }

                                                // tombol pagination
                                                if($currentPage == $totalPagesRiwayats || $totalPagesRiwayats == 0){
                                                    $disablednext = 'disabled';
                                                    $hidden = 'hidden';
                                                }

                                            @endphp

                                            <ul class="pagination" style="display: {{ $pagination }}">
                                                <li class="page-item disabled"><a href="#"
                                                                                  class="page-link">Halaman {{ $currentPage }}
                                                        dari {{ $totalPagesRiwayats }}</a></li>

                                                <li class="page-item previous {{ $disabled }}"><a
                                                        href="{{ url()->current() . '?page=' . ($currentPage - 1) }}"
                                                        class="page-link"><i class="previous"></i></a></li>
                                                <li class="page-item active"><a
                                                        href="{{ url()->current() . '?page=' . $currentPage }}"
                                                        class="page-link">{{ $currentPage }}</a></li>
                                                <li class="page-item" {{ $hidden }}><a
                                                        href="{{ url()->current() . '?page=' . $nextPage }}"
                                                        class="page-link">{{ $nextPage }}</a></li>
                                                <li class="page-item next {{ $disablednext }}"><a
                                                        href="{{ url()->current() . '?page=' . $nextPage }}"
                                                        class="page-link"><i class="next"></i></a></li>
                                            </ul>

                                        </div>
                                    @endif
                                    <!-- end::Pagination -->
                                @endif
                            </div>
                            <!--end::Card body-->
                        </div>
                        <!--end::Card-->

                        <!--begin::Modal - Add permohonan Pembatalan-->
                        <div class="modal fade" id="kt_modal_permohonan_batal" tabindex="-1" aria-hidden="true">
                            <!--begin::Modal dialog-->
                            <div class="modal-dialog modal-dialog-centered mw-1000px">
                                <!--begin::Modal content-->
                                <div class="modal-content">
                                    <!--begin::Modal header-->
                                    <div class="modal-header">
                                        <!--begin::Modal title-->
                                        <h2 class="fw-bolder text-white">Form Permohonan Pembatalan Cuti </h2>
                                        <!--end::Modal title-->
                                        <!--begin::Close-->
                                        <div class="btn btn-icon btn-sm btn-active-icon-primary"
                                             data-bs-dismiss="modal">
                                            <!--begin::Svg Icon | path: icons/duotone/Navigation/Close.svg-->
                                            <span class="svg-icon svg-icon-1">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                                                   fill="#000000">
                                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                                    <rect fill="#000000" opacity="0.5"
                                                          transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                                          x="0" y="7" width="16" height="2" rx="1"/>
                                                </g>
                                            </svg>
                                        </span>
                                            <!--end::Svg Icon-->
                                        </div>
                                        <!--end::Close-->
                                    </div>
                                    <!--end::Modal header-->
                                    <!--begin::Modal body-->
                                    <div class="modal-body scroll-y mx-5 mx-xl-15 my-5">
                                        <!--begin::Form-->
                                        <form class="form w-lg-500px mx-auto" id="kt_batal_form" method="post">
                                            @csrf

                                            {{-- menampilkan error validasi --}}
                                            @if (count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif

                                            <!--begin::Input group-->
                                            <div class="fv-row mb-8">
                                                <!--begin::Label-->
                                                <label class="required fs-6 fw-bold mb-2">Alasan Pembatalan</label>
                                                <!--end::Label-->
                                                <!--begin::Input-->
                                                <textarea class="form-control form-control-solid" rows="3"
                                                          placeholder="Mohon isi penjelasan" id="alasanPembatalan"
                                                          name="alasanPembatalan"
                                                          required="required">{{ old('alasanPembatalan') }}</textarea>
                                                <!--end::Input-->
                                            </div>
                                            <!--end::Input group-->

                                            <!--begin::Input group-->
                                            <div class="fv-row mb-8">
                                                <button type="button" class="btn btn-primary" data-action="submit"
                                                        id="btn-submit-batal">
                                                <span class="indicator-label">
                                                    Submit
                                                </span>
                                                    <span class="indicator-progress">
                                                    Please wait... <span
                                                            class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                                </span>
                                                </button>
                                            </div>
                                            <!--end::Input group-->

                                        </form>
                                    </div>
                                    <!--end::Modal body-->
                                </div>
                                <!--end::Modal content-->
                            </div>
                            <!--end::Modal dialog-->
                        </div>
                        <!--end::Modal - Add permohonan Pembatalan-->

                        <!--begin::Modal - Add permohonan Pembatalan Tambahan-->
                        <div class="modal fade" id="kt_modal_permohonan_batal_tambahan" tabindex="-1"
                             aria-hidden="true">
                            <!--begin::Modal dialog-->
                            <div class="modal-dialog modal-dialog-centered mw-1000px">
                                <!--begin::Modal content-->
                                <div class="modal-content">
                                    <!--begin::Modal header-->
                                    <div class="modal-header">
                                        <!--begin::Modal title-->
                                        <h2 class="fw-bolder text-white">Form Permohonan Pembatalan Cuti Tambahan</h2>
                                        <!--end::Modal title-->
                                        <!--begin::Close-->
                                        <div class="btn btn-icon btn-sm btn-active-icon-primary"
                                             data-bs-dismiss="modal">
                                            <!--begin::Svg Icon | path: icons/duotone/Navigation/Close.svg-->
                                            <span class="svg-icon svg-icon-1">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                                                   fill="#000000">
                                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                                    <rect fill="#000000" opacity="0.5"
                                                          transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                                          x="0" y="7" width="16" height="2" rx="1"/>
                                                </g>
                                            </svg>
                                        </span>
                                            <!--end::Svg Icon-->
                                        </div>
                                        <!--end::Close-->
                                    </div>
                                    <!--end::Modal header-->
                                    <!--begin::Modal body-->
                                    <div class="modal-body scroll-y mx-5 mx-xl-15 my-5">
                                        <!--begin::Form-->
                                        <form class="form w-lg-500px mx-auto" id="kt_batal_form" method="post">
                                            @csrf

                                            {{-- menampilkan error validasi --}}
                                            @if (count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif

                                            <!--begin::Input group-->
                                            <div class="fv-row mb-8">
                                                <!--begin::Label-->
                                                <label class="required fs-6 fw-bold mb-2">Alasan Pembatalan</label>
                                                <!--end::Label-->
                                                <!--begin::Input-->
                                                <textarea class="form-control form-control-solid" rows="3"
                                                          placeholder="Mohon isi penjelasan"
                                                          id="alasanPembatalanTambahan"
                                                          name="alasanPembatalanTambahan"
                                                          required="required">{{ old('alasanPembatalanTambahan') }}</textarea>
                                                <!--end::Input-->
                                            </div>
                                            <!--end::Input group-->

                                            <!--begin::Input group-->
                                            <div class="fv-row mb-8">
                                                <button type="button" class="btn btn-primary" data-action="submit"
                                                        id="btn-submit-batal-tambahan">
                                                <span class="indicator-label">
                                                    Submit
                                                </span>
                                                    <span class="indicator-progress">
                                                    Please wait... <span
                                                            class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                                </span>
                                                </button>
                                            </div>
                                            <!--end::Input group-->

                                        </form>
                                    </div>
                                    <!--end::Modal body-->
                                </div>
                                <!--end::Modal content-->
                            </div>
                            <!--end::Modal dialog-->
                        </div>
                        <!--end::Modal - Add permohonan Pembatalan Tambahan-->
                    </div>
                    <div class="tab-pane fade" id="kt_riwayat" role="tabpanel">
                        <!--begin::Card-->
                        <div class="card card-flush">
                            <!--begin::Card header-->
                            <div class="card-header mt-6">
                                <!--begin::Card title-->
                                <div class="card-title">
                                    <!--begin::Search-->
                                    <div class="d-flex align-items-center position-relative my-1 me-5">

                                    </div>
                                    <!--end::Search-->
                                </div>
                                <!--end::Card title-->
                                <!--begin::Card toolbar-->
                                <div class="card-toolbar">

                                </div>
                                <!--end::Card toolbar-->
                            </div>
                            <!--end::Card header-->
                            <!--begin::Card body-->
                            <div class="card-body pt-0">
                                @if(isset($alert_message))
                                    {!! $alert_message !!}
                                @else
                                    <!--begin::Table-->
                                    <div class="table-responsive rounded border">
                                        <table class="table align-middle table-striped fs-6 gy-5 mb-0"
                                               id="kt_permissions_table">
                                            <!--begin::Table head-->
                                            <thead>
                                            <!--begin::Table row-->
                                            <tr class="text-start text-white fw-bolder fs-7 text-uppercase gs-0"
                                                style="background-color:#02275d;">
                                                <th class="min-w-50px text-center">No</th>
                                                <th class="min-w-100px">Nama Pegawai</th>
                                                <th class="min-w-120px">Jenis Cuti</th>
                                                <th class="min-w-1505px">Tanggal Cuti</th>
                                                <th class="min-w-120px">Tahun</th>
                                                <th class="min-w-150px">Alasan</th>
                                                <th class="text-center min-w-100px">Actions</th>
                                            </tr>
                                            <!--end::Table row-->
                                            </thead>
                                            <!--end::Table head-->
                                            <!--begin::Table body-->
                                            <tbody class="fw-bold text-gray-600">
                                                <?php $no = $numberOfElements; ?>
                                            @foreach ($riwayatCuti as $riwayat)
                                                    <?php
                                                    if (8 != $riwayat->jnsCuti->jnsCuti) {
                                                        $tanggalCuti = AppHelper::convertDateMDY($riwayat->tglAwalCutiPeg) . ' - ' . AppHelper::convertDateMDY($riwayat->tglAkhirCutiPeg);
                                                        $tglAwalCutiPeg = AppHelper::convertDateDMY($riwayat->tglAwalCutiPeg);
                                                        $tglAkhirCutiPeg = AppHelper::convertDateDMY($riwayat->tglAkhirCutiPeg);
                                                    } else {
                                                        $tanggalCuti = '';
                                                        $tglAwalCutiPeg = '';
                                                        $tglAkhirCutiPeg = '';
                                                    }
                                                    $batal = (isset($riwayat->batal)) ? $riwayat->batal : '';
                                                    $idPermohonan = explode("/", $riwayat->permohonanCuti);
                                                    ?>
                                                <tr id="cuti{{$riwayat->id}}"
                                                    class="{{ ('' != $batal) ? "text-danger":"" }}">
                                                    <td class="text-center">{{ $no }}</td>
                                                    <td>
                                                        {{ $riwayat->nip9 }}/{{ $usulan->nama }}
                                                        <hr/>
                                                        ({{ $riwayat->nomorTicket}})
                                                    </td>
                                                    <td>{{ $riwayat->jnsCuti->namaCuti }}</td>
                                                    <td>
                                                        {{ $tglAwalCutiPeg }}
                                                        &nbsp - &nbsp
                                                        {{ $tglAkhirCutiPeg }}
                                                        <hr/>
                                                        ({{ $riwayat->jumlahCuti ?? '' }} hari)
                                                    </td>
                                                    <td>
                                                        {{ $riwayat->tahun }}
                                                    </td>
                                                    <td>
                                                        {!! ('' != $batal) ? "<span class='text-danger'>(Cuti Batal)</span>" : "" !!}
                                                        <br/>
                                                        {!! (isset($riwayat->alasanPembatalan))? $riwayat->alasanPembatalan:'' !!}
                                                    </td>
                                                    <td class="text-center">
                                                        @if(date('Y') == $riwayat->tahun)
                                                            @if('' == $batal)
                                                                <button
                                                                    class="btn btn-sm btn-icon btn-danger btnBatal mt-1"
                                                                    data-id="{{ $riwayat->id }}"
                                                                    data-url="{{url('cuti/permohonanPembatalanCuti')}}"
                                                                    data-id-permohonan="{{ $idPermohonan[2] }}"
                                                                    data-bs-toggle="tooltip" data-bs-placement="top"
                                                                    title="Batal">
                                                                    <i class="bi bi-x-octagon text-white fs-2x"></i>
                                                                </button>
                                                                &nbsp;
                                                                <button
                                                                    class="btn btn-sm btn-icon btn-warning btnPanggilanDinas mt-1"
                                                                    data-id="{{ $riwayat->id }}"
                                                                    data-url="{{url('cuti/panggilanDinas')}}"
                                                                    data-id-permohonan="{{ $idPermohonan[2] }}"
                                                                    data-bs-toggle="tooltip" data-bs-placement="top"
                                                                    title="Panggilan Dinas">
                                                                    <i class="bi bi-telephone-inbound-fill text-white fs-2x"></i>
                                                                </button>
                                                            @endif
                                                            &nbsp;
                                                            <button
                                                                class="btn btn-sm btn-icon btn-info btnKembaliPersetujuan mt-1"
                                                                data-id="{{ $riwayat->id }}"
                                                                data-url="{{url('cuti/kembaliKePersetujuan')}}"
                                                                data-id-permohonan="{{ $idPermohonan[2] }}"
                                                                data-bs-toggle="tooltip" data-bs-placement="top"
                                                                title="Kembalikan Ke Persetujuan">
                                                                <i class="bi bi-arrow-clockwise text-white fs-2x"></i>
                                                            </button>
                                                        @endif
                                                    </td>
                                                </tr>
                                                    <?php $no++; ?>
                                            @endforeach
                                            </tbody>
                                            <!--end::Table body-->
                                        </table>
                                    </div>

                                    <!--end::Table-->
                                    @if ($totalItems != 0)
                                        <!--begin::pagination-->
                                        <div class="d-flex flex-stack flex-wrap pt-5 p-3">
                                            <div class="fs-6 fw-bold text-gray-700"></div>

                                            @php
                                                $disabled = '';
                                                $nextPage= '';
                                                $disablednext = '';
                                                $hidden = '';
                                                $pagination = '';

                                                // tombol pagination
                                                if ($totalRiwayats == 0) {
                                                    // $pagination = 'none';
                                                }

                                                // tombol back
                                                if($currentPage == 1){
                                                    $disabled = 'disabled';
                                                }

                                                // tombol next
                                                if($currentPage != $totalPagesRiwayats) {
                                                    $nextPage = $currentPage + 1;
                                                }

                                                // tombol pagination
                                                if($currentPage == $totalPagesRiwayats || $totalPagesRiwayats == 0){
                                                    $disablednext = 'disabled';
                                                    $hidden = 'hidden';
                                                }

                                            @endphp

                                            <ul class="pagination" style="display: {{ $pagination }}">
                                                <li class="page-item disabled"><a href="#"
                                                                                  class="page-link">Halaman {{ $currentPage }}
                                                        dari {{ $totalPagesRiwayats }}</a></li>

                                                <li class="page-item previous {{ $disabled }}"><a
                                                        href="{{ url()->current() . '?page=' . ($currentPage - 1) }}"
                                                        class="page-link"><i class="previous"></i></a></li>
                                                <li class="page-item active"><a
                                                        href="{{ url()->current() . '?page=' . $currentPage }}"
                                                        class="page-link">{{ $currentPage }}</a></li>
                                                <li class="page-item" {{ $hidden }}><a
                                                        href="{{ url()->current() . '?page=' . $nextPage }}"
                                                        class="page-link">{{ $nextPage }}</a></li>
                                                <li class="page-item next {{ $disablednext }}"><a
                                                        href="{{ url()->current() . '?page=' . $nextPage }}"
                                                        class="page-link"><i class="next"></i></a></li>
                                            </ul>

                                        </div>
                                    @endif
                                    <!-- end::Pagination -->
                                @endif
                            </div>
                            <!--end::Card body-->
                        </div>
                        <!--end::Card-->

                        <!--begin::Modal - Add permohonan Pembatalan-->
                        <div class="modal fade" id="kt_modal_permohonan_batal" tabindex="-1" aria-hidden="true">
                            <!--begin::Modal dialog-->
                            <div class="modal-dialog modal-dialog-centered mw-1000px">
                                <!--begin::Modal content-->
                                <div class="modal-content">
                                    <!--begin::Modal header-->
                                    <div class="modal-header">
                                        <!--begin::Modal title-->
                                        <h2 class="fw-bolder">Form Permohonan Pembatalan Cuti </h2>
                                        <!--end::Modal title-->
                                        <!--begin::Close-->
                                        <div class="btn btn-icon btn-sm btn-active-icon-primary"
                                             data-bs-dismiss="modal">
                                            <!--begin::Svg Icon | path: icons/duotone/Navigation/Close.svg-->
                                            <span class="svg-icon svg-icon-1">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                                                   fill="#000000">
                                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                                    <rect fill="#000000" opacity="0.5"
                                                          transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                                          x="0" y="7" width="16" height="2" rx="1"/>
                                                </g>
                                            </svg>
                                        </span>
                                            <!--end::Svg Icon-->
                                        </div>
                                        <!--end::Close-->
                                    </div>
                                    <!--end::Modal header-->
                                    <!--begin::Modal body-->
                                    <div class="modal-body scroll-y mx-5 mx-xl-15 my-5">
                                        <!--begin::Form-->
                                        <form class="form w-lg-500px mx-auto" id="kt_batal_form" method="post">
                                            @csrf

                                            {{-- menampilkan error validasi --}}
                                            @if (count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif

                                            <!--begin::Input group-->
                                            <div class="fv-row mb-8">
                                                <!--begin::Label-->
                                                <label class="required fs-6 fw-bold mb-2">Alasan Pembatalan</label>
                                                <!--end::Label-->
                                                <!--begin::Input-->
                                                <textarea class="form-control form-control-solid" rows="3"
                                                          placeholder="Mohon isi penjelasan" id="alasanPembatalan"
                                                          name="alasanPembatalan"
                                                          required="required">{{ old('alasanPembatalan') }}</textarea>
                                                <!--end::Input-->
                                            </div>
                                            <!--end::Input group-->

                                            <!--begin::Input group-->
                                            <div class="fv-row mb-8">
                                                <button type="button" class="btn btn-primary" data-action="submit"
                                                        id="btn-submit-batal">
                                                <span class="indicator-label">
                                                    Submit
                                                </span>
                                                    <span class="indicator-progress">
                                                    Please wait... <span
                                                            class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                                </span>
                                                </button>
                                            </div>
                                            <!--end::Input group-->

                                        </form>
                                    </div>
                                    <!--end::Modal body-->
                                </div>
                                <!--end::Modal content-->
                            </div>
                            <!--end::Modal dialog-->
                        </div>
                        <!--end::Modal - Add permohonan Pembatalan-->
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>

        // set status alasan penolakan atasan langsung dan pyb
        function AddReadMore() {
            //This limit you can set after how much characters you want to show Read More.
            var carLmt = 80;
            // Text to show when text is collapsed
            var readMoreTxt = " ... Read More";
            // Text to show when text is expanded
            var readLessTxt = " Read Less";


            //Traverse all selectors with this class and manupulate HTML part to show Read More
            $(".addReadMore").each(function () {
                if ($(this).find(".firstSec").length)
                    return;

                var allstr = $(this).text();
                if (allstr.length > carLmt) {
                    var firstSet = allstr.substring(0, carLmt);
                    var secdHalf = allstr.substring(carLmt, allstr.length);
                    var strtoadd = firstSet + "<span class='SecSec'>" + secdHalf + "</span><span class='readMore' >" + readMoreTxt + "</span><span class='readLess' >" + readLessTxt + "</span>";
                    $(this).html(strtoadd);
                }

                // open readMore and readLess
                // if (carLmt <= 80) {
                // readMoreTxt + readLessTxt = "";
                // } else {
                // readMoreTxt + readLessTxt = "active";
                // }

            });
            //Read More and Read Less Click Event binding
            $(document).on("click", ".readMore,.readLess", function () {
                $(this).closest(".addReadMore").toggleClass("showlesscontent showmorecontent");
            });
        }

        $(function () {
            //Calling function after Page Load
            AddReadMore();
        });

        if (0 < {{ count($hariLiburs) }}) {
            var liburs = @json($hariLiburs);
        } else {
            var liburs = [];
        }

        const cutiTahunan = ['6', '10'];
        const cutiLintasTahun = ['9'];
        const elementCutiMelahirkan = ['3'];
        const listNotCalcCuti = ['10'];

        var r = document.querySelector('[data-action="submit"]');
        // form element
        var formSub = document.querySelector('#kt_basic_form');

        var target = document.querySelector("#kt_post");

        var blockUI = new KTBlockUI(target, {
            message: '<div class="blockui-message"><span class="spinner-border text-primary"></span> Loading...</div>',
        });

        var blockModal = new KTBlockUI(document.querySelector('#kt_modal_permohonan'), {
            message: '<div class="blockui-message"><span class="spinner-border text-primary"></span> Loading...</div>',
        });

        // date format
        function getDateDiffDays(start, end) {
            var d1 = new Date(start);
            var d2 = new Date(end);
            var diff = d2.getTime() - d1.getTime();
            var result = Math.ceil(diff / (1000 * 60 * 60 * 24));

            return result;
        }

        $('#jnsCuti').on('change', function () {
            var jnsCuti = $('#jnsCuti').val();
            var idPermohonan = $('#idPermohonan').val();
            var selected = $(this).find('option:selected');
            var idJnsCuti = selected.data('id');
            $('#idJnsCuti').val(idJnsCuti);
            $('#lamaCuti').val('');
            $('#tanggalCuti').val('');
            /*if(cutiTahunan.includes(jnsCuti) && {{ $cutiTahunanDiajukan }} > 0 && '' == idPermohonan){
                alert('Mohon maaf pengajuan permohonan cuti tahunan tidak dapat dilakukan karena masih terdapat permohonan cuti tahunan yang belum disetujui');
                $('select[name=jnsCuti]').val('').change();
            }*/

            if (cutiTahunan.includes(jnsCuti) && {{ $kuotaCuti }} == 0 && '' == idPermohonan) {
                Swal.fire({
                    text: "Mohon maaf pengajuan permohonan cuti tahunan tidak dapat dilakukan karena kuota cuti Anda 0",
                    icon: "warning",
                    buttonsStyling: !1,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-warning"
                    }
                });
                $('select[name=jnsCuti]').val('').change();
            }

            // Alert permohonan cuti melahirkan harus disetujui dahulu
            if (elementCutiMelahirkan.includes(jnsCuti) && {{ $cutiMelahirkanDiajukan }} > 0 && '' == idPermohonan) {
                Swal.fire({
                    text: "Mohon maaf pengajuan permohonan cuti melahirkan tidak dapat dilakukan karena masih terdapat permohonan cuti melahirkan yang belum disetujui",
                    icon: "warning",
                    buttonsStyling: !1,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-warning"
                    }
                });
                $('select[name=jnsCuti]').val('').change();
                document.getElementById("btn-submit").disabled = true;
            }

            // hide field alasan cuti on cuti melahirkan form
            if (false == elementCutiMelahirkan.includes(jnsCuti)) {
                formSub.querySelector('.fieldAlasanCuti').style.setProperty("display", "block", "important");
            } else {
                formSub.querySelector('.fieldAlasanCuti').style.setProperty("display", "none", "important");
            }

            // hide field jensi cuti 1/2 hari
            if ('10' == jnsCuti) {
                formSub.querySelector('.fieldTipeCutiHalf').style.setProperty("display", "block", "important");
            } else {
                formSub.querySelector('.fieldTipeCutiHalf').style.setProperty("display", "none", "important");
            }

            // open card persalinanKe
            if (jnsCuti == '3') {
                $("#CutiSalinMuncul").attr("hidden", false);
                $("#Alasan").attr("hidden", true);
            } else {
                $("#CutiSalinMuncul").attr("hidden", true);
                $("#Alasan").attr("hidden", false);
            }

            // Validasi gender cuti melahirkan
            if (elementCutiMelahirkan.includes(jnsCuti) && 'Laki-laki' == "{{ $jenisKelamin }}") {
                Swal.fire({
                    text: "Mohon maaf pengajuan permohonan cuti melahirkan tidak dapat dilakukan karena anda tidak berhak",
                    icon: "warning",
                    buttonsStyling: !1,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-warning"
                    }
                });
                $('select[name=jnsCuti]').val('').change();
                document.getElementById("btn-submit").disabled = true;
            }

            // jika jenis cuti melahirkan maka tidak boleh lebih 3x melahirkan anak
            if (elementCutiMelahirkan.includes(jnsCuti) && parseInt('{{ $_persalinanKe + 1 }}') >= 4 && '' == idPermohonan) {
                Swal.fire({
                    text: "Mohon maaf pengajuan permohonan cuti melahirkan tidak dapat dilakukan karena permohonan cuti melahirkan lebih dari kuota hak cuti melahirkan",
                    icon: "warning",
                    buttonsStyling: !1,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-warning"
                    }
                });
                var token = $("meta[name='csrf-token']").attr("content");
                document.getElementById("btn-submit").disabled = true;
                $('select[name=jnsCuti]').val('').change();
            } else if ('' == idPermohonan) {
                //Increment cuti melahirkan
                $('#persalinanKe').val('{{ $_persalinanKe + 1 }}');
            }

            if (elementCutiMelahirkan.includes(jnsCuti)) {
                // tanggal cuti melahirkan
                $("#tanggalCuti").daterangepicker({
                    opens: 'left',
                    dateLimit: {
                        'months': 3,
                        'days': -1
                    },
                    // maxSpan: {days: 30},
                    isCustomDate: function (e) {
                        var dataCell = moment(e._d).format("YYYY-MM-DD");
                        //if ( dataCell == "2022-03-03" ) {
                        const d = new Date(dataCell);
                        let day = d.getDay()
                        if (liburs.includes(dataCell) || 0 == day || 6 == day) {
                            return 'holiday';
                        }
                    }
                }, function (start, end, label) {
                    var lamaCutiMelahirkan = getDateDiffDays(start.format('YYYY-MM-DD'), end.format(
                        'YYYY-MM-DD'));
                    $('#lamaCuti').val(lamaCutiMelahirkan);
                });
            } else {
                $("#tanggalCuti").daterangepicker({
                    opens: 'left',
                    //autoApply: true,
                    isCustomDate: function (e) {
                        var dataCell = moment(e._d).format("YYYY-MM-DD");
                        //if ( dataCell == "2022-03-03" ) {
                        const d = new Date(dataCell);
                        let day = d.getDay()
                        if (liburs.includes(dataCell) || 0 == day || 6 == day) {
                            return 'holiday';
                        }
                    }
                }, function (start, end, label) {
                    var token = $("meta[name='csrf-token']").attr("content");
                    var jnsCuti = $('#jnsCuti').val();
                    document.getElementById("btn-submit").disabled = true;

                    // untuk cuti yang membutuhkan hitung jumlah hari kerja
                    if (false === listNotCalcCuti.includes(jnsCuti)) {
                        const d = new Date();
                        // untuk cuti non lintas tahun pengecekan permohonan tahun lalu
                        if (cutiLintasTahun.includes(jnsCuti) && (start.format('YYYY') < d.getFullYear())) {
                            Swal.fire({
                                text: "Pengajuan Cuti Lintas Tahun hanya dapat untuk cuti pada tahun ini atau tahun depan !",
                                icon: "warning",
                                buttonsStyling: !1,
                                confirmButtonText: "Ok, got it!",
                                customClass: {
                                    confirmButton: "btn btn-warning"
                                }
                            });
                            $('#lamaCuti').val('');
                            $('#tanggalCuti').val('');
                            this.element.trigger('cancel.daterangepicker', this);
                        } else if (cutiTahunan.includes(jnsCuti) || cutiLintasTahun.includes(jnsCuti)) {
                            (async function () {
                                await getHariKerja(token, start.format("YYYY-MM-DD"), end.format("YYYY-MM-DD"), '{{ $_GET['string'] }}', function (results) {
                                    if (results['status'] === 1) {
                                        $('#lamaCuti').val(results['lama']);

                                        if (cutiTahunan.includes(jnsCuti)) {
                                            if (parseFloat(results['lama']) > parseFloat({{ $kuotaCuti }})) {
                                                Swal.fire({
                                                    text: "Lama cuti yang diajukan (" + results['lama'] + ") melebihi kuota cuti maksimal yaitu {{ $kuotaCuti }} ! ",
                                                    icon: "warning",
                                                    buttonsStyling: !1,
                                                    confirmButtonText: "Ok, got it!",
                                                    customClass: {
                                                        confirmButton: "btn btn-warning"
                                                    }
                                                });
                                                $('#lamaCuti').val('');
                                                $('#tanggalCuti').val('');
                                            }

                                            if (start.format('YYYY') < d.getFullYear()) {
                                                if (parseFloat(results['lama']) > parseFloat({{ $sisaCutiSebelumnyaNonMax }})) {
                                                    Swal.fire({
                                                        text: "Lama cuti yang diajukan (" + results['lama'] + ") melebihi kuota cuti maksimal Tahun " + start.format('YYYY')
                                                            + " yaitu {{ $sisaCutiSebelumnyaNonMax }} !",
                                                        icon: "warning",
                                                        buttonsStyling: !1,
                                                        confirmButtonText: "Ok, got it!",
                                                        customClass: {
                                                            confirmButton: "btn btn-warning"
                                                        }
                                                    });
                                                    $('#lamaCuti').val('');
                                                    $('#tanggalCuti').val('');
                                                }

                                                $('#idRekapCutiYmin1').val('{{ $IdRekapSisaYmin1 }}');
                                            }
                                        }
                                        document.getElementById("btn-submit").disabled = false;
                                    } else {
                                        console.log('Gagal get data hari kerja ' + results)
                                    }
                                });
                            })();
                        }
                    } else {
                        document.getElementById("btn-submit").disabled = false;
                    }
                });

                $('#tanggalCuti').on('apply.daterangepicker', function (ev, picker) {
                    var jnsCuti = $('#jnsCuti').val();
                    //untuk cek tanggal cuti setengah hari
                    if ('10' == jnsCuti) {
                        if (picker.startDate.format('YYYY-MM-DD') != picker.endDate.format('YYYY-MM-DD')) {
                            Swal.fire({
                                text: "Tanggal Cuti Setengah Hari harus sama antara tanggal mulai dan tanggal selesai !",
                                icon: "warning",
                                buttonsStyling: !1,
                                confirmButtonText: "Ok, got it!",
                                customClass: {
                                    confirmButton: "btn btn-warning"
                                }
                            });
                            $('#tanggalCuti').val('');
                            $(this).val('');
                            $('#lamaCuti').val('').change();
                        } else {
                            $('#lamaCuti').val(0.5);
                        }
                    }
                });
            }
        });

        $('#lamaCuti').on('change', function () {
            var lamaCuti = $('#lamaCuti').val();
            if (parseFloat(lamaCuti) > parseFloat({{ $kuotaCuti }}) && cutiTahunan.includes(jnsCuti)) {
                Swal.fire({
                    text: "Lama cuti yang diajukan (" + results['lama'] + ") melebihi kuota cuti maksimal yaitu {{ $kuotaCuti }} !",
                    icon: "warning",
                    buttonsStyling: !1,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-warning"
                    }
                });
                $('#lamaCuti').val('');
                $('#tanggalCuti').val('');
            }
        });

        // This is isn't declared as `async` because it already returns a promise
        function delay() {
            // `delay` returns a promise
            return new Promise(function (resolve, reject) {
                // Only `delay` is able to resolve or reject the promise
                setTimeout(function () {
                    resolve(42); // After 3 seconds, resolve the promise with value 42
                }, 3000);
            });
        }

        async function getHariKerja(token, start, end, pegawai, callback) {
            try {
                await delay();
                $.ajax({
                    type: 'post',
                    url: "{{ url('/cuti/getHariKerja?string=') }}" + pegawai,
                    data: {
                        _token: token,
                        start: start,
                        end: end
                    },
                    success: function (results) {
                        callback(results);
                    },
                    error: function (err) {
                        console.log(err)
                    }
                });
            } catch (error) {
                // If any of the awaited promises was rejected, this catch block
                // would catch the rejection reason
                console.log("error" + error);
            }
        }

        $('#alasan').on('change', function () {
            var jnsCuti = $('#jnsCuti').val();

            if ('' == $('#lamaCuti').val() && '' != $('#tanggalCuti').val()) {
                // alert("Silahkan ubah tanggal cuti agar trigger perhitungan lama cuti berjalan !");
                let tanggalCuti = $('#tanggalCuti').val();
                const arrayTanggal = tanggalCuti.split(' - ');
                const arrayStart = arrayTanggal[0].split('/');
                const arrayEnd = arrayTanggal[1].split('/');
                let start = arrayStart[2] + '-' + arrayStart[0] + '-' + arrayStart[1];
                let end = arrayEnd[2] + '-' + arrayEnd[0] + '-' + arrayEnd[1];

                if (elementCutiMelahirkan.includes(jnsCuti)) {
                    var lamaCutiMelahirkan = getDateDiffDays(start, end);
                    $('#lamaCuti').val(lamaCutiMelahirkan);
                } else {
                    var token = $("meta[name='csrf-token']").attr("content");
                    document.getElementById("btn-submit").disabled = true;

                    // untuk cuti yang tidak membutuhkan hitung jumlah hari kerja
                    if (false === listNotCalcCuti.includes(jnsCuti)) {
                        (async function () {
                            await getHariKerja(token, start, end, '{{ $_GET['string'] }}', function (results) {
                                const d = new Date();

                                if (results['status'] === 1) {
                                    $('#lamaCuti').val(results['lama']);

                                    if (cutiTahunan.includes(jnsCuti)) {
                                        if (parseFloat(results['lama']) > parseFloat({{ $kuotaCuti }})) {
                                            Swal.fire({
                                                text: "Lama cuti yang diajukan (" + results['lama'] + ") melebihi kuota cuti maksimal yaitu {{ $kuotaCuti }} !",
                                                icon: "warning",
                                                buttonsStyling: !1,
                                                confirmButtonText: "Ok, got it!",
                                                customClass: {
                                                    confirmButton: "btn btn-warning"
                                                }
                                            });
                                            $('#lamaCuti').val('');
                                            $('#tanggalCuti').val('');
                                        }

                                        if (arrayStart[2] < d.getFullYear()) {
                                            if (parseFloat(results['lama']) > parseFloat({{ $sisaCutiSebelumnyaNonMax }})) {
                                                Swal.fire({
                                                    text: "Lama cuti yang diajukan (" + results['lama'] + ") melebihi kuota cuti maksimal Tahun " + start.format('YYYY')
                                                        + " yaitu {{ $sisaCutiSebelumnyaNonMax }} !",
                                                    icon: "warning",
                                                    buttonsStyling: !1,
                                                    confirmButtonText: "Ok, got it!",
                                                    customClass: {
                                                        confirmButton: "btn btn-warning"
                                                    }
                                                });
                                                $('#lamaCuti').val('');
                                                $('#tanggalCuti').val('');
                                            }

                                            $('#idRekapCutiYmin1').val('{{ $IdRekapSisaYmin1 }}');
                                        }
                                    }
                                    document.getElementById("btn-submit").disabled = false;
                                } else {
                                    console.log('Gagal get data hari kerja ' + results)
                                }
                            });
                        })();
                    } else {
                        document.getElementById("btn-submit").disabled = false;
                    }
                }
            }
        });

        //submit form
        r.addEventListener("click", (function (e) {
            $('#kt_modal_permohonan').modal('hide');
            blockUI.block();
            r.disabled = !0, r.setAttribute("data-kt-indicator", "on"), setTimeout((function () {
                r.removeAttribute("data-kt-indicator"), r.disabled = !1, Swal.fire({
                    text: "Form will be submitted!",
                    icon: "success",
                    buttonsStyling: !1,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                }).then((function () {
                    blockUI.block();
                    var idPermohonan = $('#idPermohonan').val();

                    var cutiMelahirkanDitambahkan = document.createElement("INPUT");
                    cutiMelahirkanDitambahkan.setAttribute("type", 'hidden');
                    // persalinanKe = cutiPersalinanKe
                    cutiMelahirkanDitambahkan.setAttribute("name", 'cutiPersalinanKe');
                    if ('' == idPermohonan) {
                        cutiMelahirkanDitambahkan.setAttribute("value", '{{ $_persalinanKe  + 1 }}');
                    } else {
                        cutiMelahirkanDitambahkan.setAttribute("value", '{{ $_persalinanKe  + 1 }}');
                    }


                    var element = document.getElementById("kt_basic_form");
                    element.appendChild(cutiMelahirkanDitambahkan);
                    formSub.submit(function (e) {
                        e.preventDefault();
                        blockUI.release();
                    });
                }))
            }), 2e3)
        }));

        $(".btnTambahPermohonan").click(function () {
            $('#idPermohonan').val('').change();
            $('#tanggalCuti').val('');
            $('#lamaCuti').val('');
            $('#phone').val('');
            $('#alamatSementara').val('').change();
            $('#alasan').val('').change();
            $('select[name=jnsCuti]').val('').change();
        });

        // btn edit cuti melahirkan maka yang muncul hanya form isian cuti melahirkan
        $(".btnEditUsulan").click(function () {
            $('#kt_modal_permohonan').modal('show');
            var idPermohonan = $(this).data('id-permohonan');
            var tanggalCuti = $(this).data('tanggal-cuti');
            var tanggalBuat = $(this).data('tanggal-buat');
            var lamaCuti = $(this).data('lama-cuti');
            var phone = $(this).data('phone');
            var alasan = $(this).data('alasan');
            var alamatSementara = $(this).data('alamat-sementara');
            var jnsCuti = $(this).data('jns-cuti');
            var persalinanKe = $(this).data('persalinan-ke');


            $('#idPermohonan').val(idPermohonan).change();
            $('select[name=jnsCuti]').val(jnsCuti).change();
            $('#tanggalCuti').val(tanggalCuti);
            $('#lamaCuti').val(lamaCuti);
            $('#phone').val(phone);
            $('#alamatSementara').val(alamatSementara).change();
            $('#alasan').val(alasan).change();
            $('#persalinanKe').val(persalinanKe).change();
            $('#btn-submit').attr('data-tanggal-buat', tanggalBuat);
        });

        $(".btnHapusUsulan").click(function () {
            var id = $(this).data("id");
            var tahun = $(this).data("tahun");
            var lamaCuti = $(this).data("lama-cuti");
            var url = $(this).data("url");
            var token = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                title: 'Yakin akan menghapus data?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#C5584B',
                cancelButtonColor: '#B2B2B2',
                confirmButtonText: 'Hapus!',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    blockUI.block();
                    $.ajax({
                        type: 'post',
                        url: url,
                        data: {
                            _token: token,
                            tahun: tahun,
                            lamaCuti: lamaCuti,
                            idPermohonan: id
                        },
                        success: function (response) {
                            if (response['status'] === 1) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'data berhasil di hapus',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                $('#cuti' + id).remove();
                                blockUI.release();
                                location.reload();
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal menghapus data!',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                console.log(response);
                                blockUI.release();
                                location.reload();
                            }
                        },
                        error: function (err) {
                            console.log(err);
                            location.reload();
                        }
                    });
                }
            })
        });

        $(".btnKirimUsulan").click(function () {
            var id = $(this).data("id");
            var url = $(this).data("url");
            var token = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                title: 'Yakin akan mengirim permohonan?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009ef7',
                cancelButtonColor: '#C5584B',
                confirmButtonText: 'Kirim!',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    blockUI.block();
                    $.ajax({
                        type: 'post',
                        url: url + '?string=' + btoa(id),
                        data: {
                            _token: token,
                            idPermohonan: id
                        },
                        success: function (response) {
                            btoa
                            if (response['status'] === 1) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'permohonan cuti berhasil di kirim',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                blockUI.release();
                                window.location.href = document.URL;
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal mengirim permohonan cuti!',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                blockUI.release();
                            }
                        },
                        error: function (err) {
                            console.log(err);
                            blockUI.release();
                        }
                    });
                }
            })

        });

        $(".btnBatal").click(function () {
            $('#kt_modal_permohonan_batal').modal('show');

            var id = $(this).data("id");
            var idPermohonan = $(this).data("id-permohonan")
            var url = $(this).data("url");
            var token = $("meta[name='csrf-token']").attr("content");

            $('#alasanPembatalan').val('').change();

            $('#btn-submit-batal').attr('data-id', idPermohonan);
            $('#btn-submit-batal').attr('data-url', url);

        });

        $("#btn-submit-batal").on('click', function () {

            var id = $(this).data("id");
            var url = $(this).data("url");
            var token = $("meta[name='csrf-token']").attr("content");
            var alasanPembatalan = $('#alasanPembatalan').val();

            if ('' != alasanPembatalan) {
                Swal.fire({
                    title: 'Yakin akan membatalkan Cuti?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#009ef7',
                    cancelButtonColor: '#C5584B',
                    confirmButtonText: 'Kirim!',
                    cancelButtonText: 'Batal',
                    reverseButtons: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        $('#kt_modal_permohonan_batal').modal('hide');
                        blockUI.block();
                        $.ajax({
                            type: 'post',
                            url: url + '?string=' + btoa(id),
                            data: {
                                _token: token,
                                idPermohonan: id,
                                alasanPembatalan: alasanPembatalan
                            },
                            success: function (response) {
                                if (response['status'] === 1) {
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'success',
                                        title: 'Berhasil',
                                        text: 'Permohonan pembatalan cuti berhasil di kirim',
                                        showConfirmButton: false,
                                        timer: 1500
                                    });
                                    blockUI.release();
                                    window.location.href = document.URL;
                                } else {
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'error',
                                        title: 'Proses kirim permohonan pembatalan cuti gagal!',
                                        text: response.message,
                                        showConfirmButton: false,
                                        timer: 1500
                                    });
                                    blockUI.release();
                                    location.reload();
                                }
                            },
                            error: function (err) {
                                console.log(err);
                                blockUI.release();
                            }
                        });
                    }
                })
            } else {
                Swal.fire({
                    text: "Alasan pembatalan cuti belum diisi !",
                    icon: "warning",
                    buttonsStyling: !1,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-warning"
                    }
                });
            }
        });

        $(".btnBatalTambahan").click(function () {
            $('#kt_modal_permohonan_batal_tambahan').modal('show');

            var id = $(this).data("id");
            var idPermohonan = $(this).data("id-permohonan-tambahan")
            var url = $(this).data("url");
            var token = $("meta[name='csrf-token']").attr("content");

            $('#alasanPembatalanTambahan').val('').change();

            $('#btn-submit-batal-tambahan').attr('data-id', id);
            $('#btn-submit-batal-tambahan').attr('data-url', url);

        });

        $("#btn-submit-batal-tambahan").on('click', function () {

            var id = $(this).data("id");
            var url = $(this).data("url");
            var token = $("meta[name='csrf-token']").attr("content");
            var alasanPembatalanTambahan = $('#alasanPembatalanTambahan').val();

            if ('' != alasanPembatalanTambahan) {
                Swal.fire({
                    title: 'Yakin akan membatalkan Cuti Tambahan?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#009ef7',
                    cancelButtonColor: '#C5584B',
                    confirmButtonText: 'Kirim!',
                    cancelButtonText: 'Batal',
                    reverseButtons: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        $('#kt_modal_permohonan_batal_tambahan').modal('hide');
                        blockUI.block();
                        $.ajax({
                            type: 'post',
                            url: url + '?string=' + btoa(id),
                            data: {
                                _token: token,
                                idCutiTambahan: id,
                                alasanPembatalan: alasanPembatalanTambahan
                            },
                            success: function (response) {
                                if (response['status'] === 1) {
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'success',
                                        title: 'Berhasil',
                                        text: 'Permohonan pembatalan cuti berhasil di kirim',
                                        showConfirmButton: false,
                                        timer: 1500
                                    });
                                    blockUI.release();
                                    window.location.href = document.URL;
                                } else {
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'error',
                                        title: 'Proses kirim permohonan pembatalan cuti gagal!',
                                        text: response.message,
                                        showConfirmButton: false,
                                        timer: 1500
                                    });
                                    blockUI.release();
                                    location.reload();
                                }
                            },
                            error: function (err) {
                                console.log(err);
                                blockUI.release();
                            }
                        });
                    }
                })
            } else {
                Swal.fire({
                    text: "Alasan pembatalan cuti belum diisi !",
                    icon: "warning",
                    buttonsStyling: !1,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-warning"
                    }
                });
            }
        });

        //id-permohonan
        $(".btnKembaliPersetujuan").on('click', function () {

            var id = $(this).data("id");
            var idPermohonan = $(this).data("id-permohonan")
            var url = $(this).data("url");
            var token = $("meta[name='csrf-token']").attr("content");
            var alasanPembatalan = $('#alasanPembatalan').val();

            // if('' != alasanPembatalan){
            Swal.fire({
                title: 'Yakin akan mengembalikan status ke Permohonan?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009ef7',
                cancelButtonColor: '#C5584B',
                confirmButtonText: 'Kirim!',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    blockUI.block();
                    $.ajax({
                        type: 'post',
                        url: url,
                        data: {
                            _token: token,
                            idPermohonan: idPermohonan,
                            idCuti: id
                        },
                        success: function (response) {
                            if (response['status'] === 1) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'Perubahan Status ke Permohonan berhasil diproses',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                blockUI.release();
                                window.location.href = document.URL;
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Proses ubah status ke permohonan gagal!',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                blockUI.release();
                                location.reload();
                            }
                        },
                        error: function (err) {
                            console.log(err);
                            blockUI.release();
                        }
                    });
                }
            })
            // }else{
            //     alert('Alasan pembatalan cuti belum diisi !');
            // }
        });

        $(".btnKembaliPersetujuanTambahan").on('click', function () {

            var id = $(this).data("id");
            var idPermohonan = $(this).data("id-permohonan-tambahan")
            var url = $(this).data("url");
            var token = $("meta[name='csrf-token']").attr("content");
            var alasanPembatalan = $('#alasanPembatalan').val();

            // if('' != alasanPembatalan){
            Swal.fire({
                title: 'Yakin akan mengembalikan status ke Permohonan?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009ef7',
                cancelButtonColor: '#C5584B',
                confirmButtonText: 'Kirim!',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    blockUI.block();
                    $.ajax({
                        type: 'post',
                        url: url,
                        data: {
                            _token: token,
                            idCutiTambahan: id,
                            idCuti: id
                        },
                        success: function (response) {
                            if (response['status'] === 1) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'Perubahan Status ke Permohonan berhasil diproses',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                blockUI.release();
                                window.location.href = document.URL;
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Proses ubah status ke permohonan gagal!',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                blockUI.release();
                                location.reload();
                            }
                        },
                        error: function (err) {
                            console.log(err);
                            blockUI.release();
                        }
                    });
                }
            })
        });

        $(".btnSesuaikanData").click(function () {
            var id = $(this).data("id");
            var url = $(this).data("url");
            var jnsCuti = $(this).data('jns-cuti');
            var token = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                title: 'Yakin akan melakukan penyesuaian data permohonan?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009ef7',
                cancelButtonColor: '#C5584B',
                confirmButtonText: 'Sesuaikan!',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    blockUI.block();
                    $.ajax({
                        type: 'post',
                        url: url + '?string=' + btoa(id),
                        data: {
                            _token: token,
                            idPermohonan: id,
                            jnsCuti: jnsCuti,
                            pegawaiId: '{{ $_GET['string'] }}'
                        },
                        success: function (response) {
                            btoa
                            if (response['status'] === 1) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'permohonan cuti berhasil di sesuaikan',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                blockUI.release();
                                window.location.href = document.URL;
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal melakukan penyesuaian data permohonan cuti!',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                blockUI.release();
                            }
                        },
                        error: function (err) {
                            console.log(err);
                            blockUI.release();
                        }
                    });
                }
            })

        });

        $(".btnSesuaikanDataTambahan").click(function () {
            var id = $(this).data("id-tambahan");
            var url = $(this).data("url-tambahan");
            var jnsCuti = $(this).data('jns-cuti-tambahan');
            var token = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                title: 'Yakin akan melakukan penyesuaian data permohonan Cuti Tambahan?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009ef7',
                cancelButtonColor: '#C5584B',
                confirmButtonText: 'Sesuaikan!',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    blockUI.block();
                    $.ajax({
                        type: 'post',
                        url: url + '?string=' + btoa(id),
                        data: {
                            _token: token,
                            idCutiTambahan: id,
                            jenis: jnsCuti,
                            pegawaiId: '{{ $_GET['string'] }}'
                        },
                        success: function (response) {
                            btoa
                            if (response['status'] === 1) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'permohonan cuti tambahan berhasil di sesuaikan',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                blockUI.release();
                                window.location.href = document.URL;
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal melakukan penyesuaian data permohonan cuti tambahan!',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                blockUI.release();
                            }
                        },
                        error: function (err) {
                            console.log(err);
                            blockUI.release();
                        }
                    });
                }
            })

        });

        function extractTime(oldTime) {
            const time = oldTime.split("T");
            const newTime = time[1].split("+");

            return newTime[0];
        }

        $(".btnCutiTambahan").click(function () {
            $('#kt_modal_1').modal('show');
            var idPermohonan = $(this).data('id-permohonan');
            var tanggalCuti = $(this).data('tanggal-cuti');
            var tanggalMulai = $(this).data('tanggal-mulai');
            var tanggalSelesai = $(this).data('tanggal-selesai');
            var tanggalBuat = $(this).data('tanggal-buat');
            var lamaCuti = $(this).data('lama-cuti');
            var phone = $(this).data('phone');
            var alasan = $(this).data('alasan');
            var alamatSementara = $(this).data('alamat-sementara');
            var nomorTicket = $(this).data('nomorticket');
            var ctt = $(this).data('ctt');

            $('#getIdPermohonan').val(idPermohonan).change();
            $('select[name=jenis]').val('').change();
            $('#getTanggalCuti').val(tanggalCuti);
            $('#getLamaCuti').val(lamaCuti);
            $('#getPhone').val(phone);
            $('#getAlamatSementara').val(alamatSementara).change();
            $('#getAlasan').val(alasan).change();
            $('#btn-continue').attr('data-tanggal-mulai', tanggalMulai);
            $('#btn-continue').attr('data-tanggal-selesai', tanggalSelesai);
            $('#btn-continue').attr('data-lama-cuti', lamaCuti);
            $('#nomorTicket').val(nomorTicket);
            $('#ctt').val(ctt);
        });

        $('#jenis').on('change', function () {
            const cutiTambahan = ['1', '2', '3'];
            var jenis = $('#jenis').val();
            var idPermohonan = $('#idPermohonan').val();
            var idCutiTambahan = $('#idCutiTambahan').val();
            var ctt = $('#ctt').val();


            // hide field jensi cuti 1/2 hari
            if ('1' === jenis) {
                formSub1.querySelector('.fieldTanggalCutiTambahan').style.setProperty("display", "block", "important");
            } else if ('2' === jenis) {
                formSub1.querySelector('.fieldTanggalCutiTambahan').style.setProperty("display", "block", "important");
            } else if ('3' === jenis) {
                formSub1.querySelector('.fieldTanggalCutiTambahan').style.setProperty("display", "block", "important");
            } else {
                formSub1.querySelector('.fieldTanggalCutiTambahan').style.setProperty("display", "none", "important");
            }

            if (cutiTambahan.includes(jenis) && {{ $kuotaCutiTambahan }} == 0 && '' == idCutiTambahan) {
                Swal.fire({
                    text: "Mohon maaf pengajuan permohonan cuti tambahan tidak dapat dilakukan karena kuota cuti tambahan Anda 0",
                    icon: "warning",
                    buttonsStyling: !1,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-warning"
                    }
                });
                $('select[name=jenis]').val('').change();
            }

            if (cutiTambahan.includes(jenis) && '' !== ctt) {
                Swal.fire({
                    text: "Mohon maaf pengajuan permohonan cuti tambahan tidak dapat dilakukan karena sudah ada permohonan yang dibuat",
                    icon: "warning",
                    buttonsStyling: !1,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-warning"
                    }
                });
                $('select[name=jenis]').val('').change();
                document.getElementById("btn-submit").disabled = true;
            }
        });

        $(".btnEditCutiTambahan").click(function () {
            $('#kt_modal_1').modal('show');
            var idPermohonan = $(this).data('id-permohonan');
            var tanggalCuti = $(this).data('tanggal-cuti');
            var tanggalMulai = $(this).data('tanggal-mulai');
            var tanggalSelesai = $(this).data('tanggal-selesai');
            var tanggalBuat = $(this).data('tanggal-buat');
            var lamaCuti = $(this).data('lama-cuti');
            var phone = $(this).data('phone');
            var alasan = $(this).data('alasan');
            var alamatSementara = $(this).data('alamat-sementara');
            var jnsCuti = $(this).data('jns-cuti');
            var nomorTicket = $(this).data('nomorticket');
            var idCutiTambahan = $(this).data('id-cuti-tambahan');
            var tanggalCutiTambahan = $(this).data('tanggal-cuti-tambahan')
            var jenisCutiTambahan = $(this).data('jenis-cuti-tambahan');
            var tanggalMulaiCutiTambahan = $(this).data('tanggalmulai-cuti-tambahan');
            var tanggalSelesaiCutiTambahan = $(this).data('tanggalselesai-cuti-tambahan');
            var lamaCutiTambahan = $(this).data('lama-cuti-tambahan');
            var statusCutiTambahan = $(this).data('status-cuti-tambahan');
            var alasanCutiTambahan = $(this).data('alasan-cuti-tambahan');

            $('#getIdPermohonan').val(idPermohonan).change();
            $('#getTanggalCuti').val(tanggalCuti);
            $('#getLamaCuti').val(lamaCuti);
            $('#getPhone').val(phone);
            $('#getAlamatSementara').val(alamatSementara).change();
            $('#getAlasan').val(alasan).change();
            $('#btn-submit').attr('data-tanggal-buat', tanggalBuat);
            $('#btn-submit').attr('data-id-permohonan', idPermohonan);
            $('#nomorTicket').val(nomorTicket);
            $('#idCutiTambahan').val(idCutiTambahan).change();
            $('select[name=jenisCutiTambahan]').val(jenisCutiTambahan).change();
            $('#tanggalCutiTambahan').val(tanggalCutiTambahan);
            $('#tanggalMulaiCutiTambahan').val(tanggalMulaiCutiTambahan).change();
            $('#tanggalSelesaiCutiTambahan').val(tanggalSelesaiCutiTambahan).change();
            $('#lamaCutiTambahan').val(lamaCutiTambahan).change();
            $('#statusCutiTambahan').val(statusCutiTambahan).change();
            $('#alasanCutiTambahan').val(alasanCutiTambahan).change();
            $('#btn-continue').attr('data-tanggal-mulai', tanggalMulai);
            $('#btn-continue').attr('data-tanggal-selesai', tanggalSelesai);
            $('#btn-continue').attr('data-jenis-cuti-tambahan', jenisCutiTambahan);

        });

        $(".btnHapusCutiTambahan").click(function () {
            var id = $(this).data("id");
            var url = $(this).data("url");
            var token = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                title: 'Yakin akan menghapus data?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#C5584B',
                cancelButtonColor: '#B2B2B2',
                confirmButtonText: 'Hapus!',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    blockUI.block();
                    $.ajax({
                        type: 'post',
                        url: url,
                        data: {
                            _token: token,
                            idPermohonan: id
                        },
                        success: function (response) {
                            // alert(JSON.stringify(response));
                            if (response['status'] === 1) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'data berhasil di hapus',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                $('#cuti' + id).remove();
                                blockUI.release();
                                location.reload();
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal menghapus data!',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                console.log(response);
                                blockUI.release();
                                location.reload();
                            }
                        },
                        error: function (err) {
                            console.log(err);
                            location.reload();
                        }
                    });
                }
            })
        });

        $(".btnKirimCutiTambahan").click(function () {
            var id = $(this).data("id");
            var url = $(this).data("url");
            var token = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                title: 'Yakin akan mengirim permohonan?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009ef7',
                cancelButtonColor: '#C5584B',
                confirmButtonText: 'Kirim!',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    blockUI.block();
                    $.ajax({
                        type: 'post',
                        url: url + '?string=' + btoa(id),
                        data: {
                            _token: token,
                            idPermohonan: id
                        },
                        success: function (response) {
                            btoa
                            if (response['status'] === 1) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'permohonan cuti berhasil di kirim',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                blockUI.release();
                                window.location.href = document.URL;
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal mengirim permohonan cuti!',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                blockUI.release();
                            }
                        },
                        error: function (err) {
                            console.log(err);
                            blockUI.release();
                        }
                    });
                }
            })

        });

        // Stepper lement
        var element = document.querySelector("#kt_stepper_example_basic");

        // stepper submit element
        var r = element.querySelector('[data-kt-stepper-action="submit"]');

        // Initialize Stepper
        var stepper = new KTStepper(element);

        var formSub1 = document.querySelector('#kt_stepper_example_basic_form');

        r.addEventListener("click", (function (e) {
            $('#kt_modal_1').modal('hide');
            blockUI.block();
            r.disabled = !0, r.setAttribute("data-kt-indicator", "on"), setTimeout((function () {
                r.removeAttribute("data-kt-indicator"), r.disabled = !1, Swal.fire({
                    text: "Form has been successfully submitted!",
                    icon: "success",
                    buttonsStyling: !1,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                }).then((function () {
                    formSub1.submit();
                }))
            }), 2e3)
        }));

        // Handle next step
        stepper.on("kt.stepper.next", function (stepper) {
            stepper.goNext(); // go next step
        });

        // Handle previous step
        stepper.on("kt.stepper.previous", function (stepper) {
            stepper.goPrevious(); // go previous step
        });

        $(function () {
            $('#tanggalCutiTambahan').daterangepicker({
                isInvalidDate: function (date) {
                    var x = $('#btn-continue').data('tanggal-mulai').split('T');
                    var z = $('#btn-continue').data('tanggal-selesai').split('T');
                    var jenis = $('#jenis').val();

                    var formatted = date.format('YYYY-MM-DD');
                    if ('1' == jenis) {
                        if (formatted >= x[0]) {
                            return true;
                        }
                    } else if ('2' == jenis) {
                        if (formatted <= z[0]) {
                            return true;
                        }
                    } else if ('3' == jenis) {
                        if (formatted >= x[0] && formatted <= z[0]) {
                            return true;
                        }
                    }
                }
            });
            $('#tanggalCutiTambahan').on('apply.daterangepicker', function (ev, picker) {
                var x = $('#btn-continue').data('tanggal-mulai').split('T');
                var z = $('#btn-continue').data('tanggal-selesai').split('T');
                var lamaCuti = $('#btn-continue').data('lama-cuti');
                var jenis = $('#jenis').val();
                var end = picker.endDate.format('YYYY-MM-DD');
                var start = picker.startDate.format('YYYY-MM-DD');
                var mulai = moment(x[0]).format('DD-MM-YYYY');
                var selesai = moment(z[0]).format('DD-MM-YYYY');

                if ('1' == jenis) {
                    $('#lamaCutiTambahan').val(picker.endDate.diff(picker.startDate, "days") + 1);
                    if (end != moment(x[0]).subtract(1, 'day').format('YYYY-MM-DD')) {
                        Swal.fire({
                            text: "Mohon masukkan tanggal Cuti Tambahan berurutan dengan tanggal Cuti Tahunan",
                            icon: "warning",
                            buttonsStyling: !1,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-warning"
                            }
                        });
                        $('#lamaCutiTambahan').val('');
                        $('#tanggalCutiTambahan').val('');
                    }
                } else if ('2' == jenis) {
                    $('#lamaCutiTambahan').val(picker.endDate.diff(picker.startDate, "days") + 1);
                    if (start != moment(z[0]).add(1, 'day').format('YYYY-MM-DD')) {
                        Swal.fire({
                            text: "Mohon masukkan tanggal Cuti Tambahan berurutan dengan tanggal Cuti Tahunan",
                            icon: "warning",
                            buttonsStyling: !1,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-warning"
                            }
                        });
                        $('#lamaCutiTambahan').val('');
                        $('#tanggalCutiTambahan').val('');
                    }
                } else if ('3' == jenis) {
                    $('#lamaCutiTambahan').val(picker.endDate.diff(picker.startDate, "days") - 1);
                    if (end <= z) {
                        Swal.fire({
                            text: "Mohon masukkan tanggal Cuti Tambahan sebelum tanggal " + mulai + " dan setelah tanggal " + selesai,
                            icon: "warning",
                            buttonsStyling: !1,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-warning"
                            }
                        });
                        $('#lamaCutiTambahan').val('');
                        $('#tanggalCutiTambahan').val('');
                    } else if (start >= x) {
                        Swal.fire({
                            text: "Mohon masukkan tanggal Cuti Tambahan sebelum tanggal " + mulai + " dan setelah tanggal " + selesai,
                            icon: "warning",
                            buttonsStyling: !1,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-warning"
                            }
                        });
                        $('#lamaCutiTambahan').val('');
                        $('#tanggalCutiTambahan').val('');
                    }
                }


                if (parseFloat(picker.endDate.diff(picker.startDate, "days") + 1) > parseFloat({{ $kuotaCutiTambahan }})) {
                    Swal.fire({
                        text: "Lama cuti yang diajukan melebihi kuota cuti tambahan maksimal yaitu {{ $kuotaCutiTambahan }} !",
                        icon: "warning",
                        buttonsStyling: !1,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-warning"
                        }
                    });
                    $('#lamaCutiTambahan').val('');
                    $('#tanggalCutiTambahan').val('');
                }
            });
        });

        $(function () {
            $('.tanggalCutiTambahanEdit').daterangepicker({
                isInvalidDate: function (date) {
                    var x = $('#btn-continue-edit').data('tanggal-mulai').split('T');
                    var z = $('#btn-continue-edit').data('tanggal-selesai').split('T');
                    var jenis = $('#jenis').val();
                    // console.log(x);
                    var formatted = date.format('YYYY-MM-DD');
                    if ('1' == jenis) {
                        if (formatted >= x[0]) {
                            return true;
                        }
                    } else if ('2' == jenis) {
                        if (formatted <= z[0]) {
                            return true;
                        }
                    } else if ('3' == jenis) {
                        if (formatted >= x[0] && formatted <= z[0]) {
                            return true;
                        }
                    }
                }
            });
            $('.tanggalCutiTambahanEdit').on('apply.daterangepicker', function (ev, picker) {
                var x = $('#btn-continue-edit').data('tanggal-mulai').split('T');
                var z = $('#btn-continue-edit').data('tanggal-selesai').split('T');
                var lamaCuti = $('#btn-continue-edit').data('lama-cuti');
                var jenis = $('#jenis').val();
                var end = picker.endDate.format('YYYY-MM-DD');
                var start = picker.startDate.format('YYYY-MM-DD');
                var mulai = moment(x[0]).format('DD-MM-YYYY');
                var selesai = moment(z[0]).format('DD-MM-YYYY');

                if ('1' == jenis) {
                    $('#lamaCutiTambahan').val(picker.endDate.diff(picker.startDate, "days") + 1);

                } else if ('2' == jenis) {
                    $('#lamaCutiTambahan').val(picker.endDate.diff(picker.startDate, "days") + 1);
                } else if ('3' == jenis) {
                    $('#lamaCutiTambahan').val(picker.endDate.diff(picker.startDate, "days") - lamaCuti - 1);
                    if (end <= z) {
                        Swal.fire({
                            text: "Mohon masukkan tanggal Cuti Tambahan sebelum tanggal " + mulai + " dan setelah tanggal " + selesai,
                            icon: "warning",
                            buttonsStyling: !1,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-warning"
                            }
                        });
                        $('#lamaCutiTambahan').val('');
                        $('#tanggalCutiTambahan').val('');
                    } else if (start >= x) {
                        Swal.fire({
                            text: "Mohon masukkan tanggal Cuti Tambahan sebelum tanggal " + mulai + " dan setelah tanggal " + selesai,
                            icon: "warning",
                            buttonsStyling: !1,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-warning"
                            }
                        });
                        $('#lamaCutiTambahan').val('');
                        $('#tanggalCutiTambahan').val('');
                    }
                }


                if (parseFloat(picker.endDate.diff(picker.startDate, "days") + 1) > parseFloat({{ $kuotaCutiTambahan }})) {
                    Swal.fire({
                        text: "Lama cuti yang diajukan melebihi kuota cuti tambahan maksimal yaitu {{ $kuotaCutiTambahan }} !",
                        icon: "warning",
                        buttonsStyling: !1,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-warning"
                        }
                    });
                    $('#lamaCutiTambahan').val('');
                    $('#tanggalCutiTambahan').val('');
                }
            });
        });

        $("form").on("click", ":checkbox", function (event) {
            $(":checkbox:not(:checked)", this.form).prop("disabled", function () {
                return $(this.form).find(":checkbox:checked").length == 1;
            });
        });

        $("body").children().first().before($(".modal"));

        $('.modal-child').on('show.bs.modal', function () {
            var modalParent = $(this).attr('data-modal-parent');
            $(modalParent).css('opacity', 0);
        });

        $('.modal-child').on('hidden.bs.modal', function () {
            var modalParent = $(this).attr('data-modal-parent');
            $(modalParent).css('opacity', 1);
        });

    </script>
@endsection
