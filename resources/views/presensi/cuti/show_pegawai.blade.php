@extends('layout.master')

<?php
// dump(($pegawai));
?>

@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <!--begin::Card-->
            <div class="card card-flush">
                <!--begin::Card header-->
                <div class="card-header mt-6">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <!--begin::Search-->
                        <div class="d-flex align-items-center position-relative my-1 me-5">
                            <input type="text" data-kt-permissions-table-filter="search"
                                   class="form-control form-control-solid w-350px ps-15"
                                   placeholder="Isi Nama Pegawai / Nomor Ticket" id="searchData"
                                   value="{{ $searchValue }}"/>
                        </div>
                        <!--end::Search-->
                    </div>
                    <!--end::Card title-->
                    <!--begin::Card toolbar-->
                    <div class="card-toolbar">

                    </div>
                    <!--end::Card toolbar-->
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body pt-0">
                    @if(isset($alert_message))
                        {!! $alert_message !!}
                    @else
                        <!--begin::Table-->
                        <div class="table-responsive bg-gray-400 rounded border border-gray-300">
                            <table class="table table-striped table-row-bordered gy-5 gs-7" id="kt_permissions_table">
                                <!--begin::Table head-->
                                <thead>
                                <!--begin::Table row-->
                                <tr class="text-start text-white fw-bolder fs-7 text-uppercase gs-0"
                                    style="background-color:#1d428a;">
                                    <th class="min-w-50px text-center">No</th>
                                    <th class="min-w-100px">Nama Pegawai</th>
                                    <th class="min-w-120px">NIP</th>
                                    <th class="min-w-1505px">Pangkat</th>
                                    <th class="min-w-100px">Jabatan</th>
                                    <th class="min-w-100px">Unit Organisasi</th>
                                    <th class="min-w-150px">Kantor</th>
                                    <th class="text-center min-w-100px">Actions</th>
                                </tr>
                                <!--end::Table row-->
                                </thead>
                                <!--end::Table head-->
                                <!--begin::Table body-->
                                <tbody class="fw-bold text-gray-600">
                                    <?php $no = $numberOfElements; ?>
                                @foreach ($pegawai as $peg)
                                        <?php

                                        ?>
                                    <tr id="cuti{{$peg->pegawaiId}}">
                                        <td class="text-center">{{ $no }}</td>
                                        <td>
                                            {{ $peg->namaPegawai }}
                                        </td>
                                        <td>{{ $peg->nip18 }}</td>
                                        <td>
                                            {{ $peg->pangkat }}
                                        </td>
                                        <td>
                                            {{ $peg->jabatan }}
                                        </td>
                                        <td>
                                            {!! $peg->unit !!}
                                        </td>
                                        <td>
                                            {!! $peg->kantor !!}
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-sm btn-icon btn-primary btnSelect mt-1"
                                                    data-id="{{ $peg->pegawaiId }}"
                                                    data-bs-toggle="tooltip" data-bs-placement="top" title="Pilih">
                                                <i class="bi bi-arrow-right-circle-fill text-white fs-2x"></i>
                                            </button>
                                        </td>
                                    </tr>
                                        <?php $no++; ?>
                                @endforeach
                                </tbody>
                                <!--end::Table body-->
                            </table>
                        </div>
                        <!--end::Table-->
                        @if ($totalItems != 0)
                            <!--begin::pagination-->
                            <div class="d-flex flex-stack flex-wrap pt-5 p-3">
                                <div class="fs-6 fw-bold text-gray-700"></div>

                                @php
                                    $disabled = '';
                                    $nextPage= '';
                                    $disablednext = '';
                                    $hidden = '';
                                    $pagination = '';

                                    // tombol pagination
                                    if ($totalItems == 0) {
                                        // $pagination = 'none';
                                    }

                                    // tombol back
                                    if($currentPage == 1){
                                        $disabled = 'disabled';
                                    }

                                    // tombol next
                                    if($currentPage != $totalPages) {
                                        $nextPage = $currentPage + 1;
                                    }

                                    // tombol pagination
                                    if($currentPage == $totalPages || $totalPages == 0){
                                        $disablednext = 'disabled';
                                        $hidden = 'hidden';
                                    }

                                @endphp

                                <ul class="pagination" style="display: {{ $pagination }}">
                                    <li class="page-item disabled"><a href="#"
                                                                      class="page-link">Halaman {{ $currentPage }}
                                            dari {{ $totalPages }}</a></li>

                                    <li class="page-item previous {{ $disabled }}"><a
                                            href="{{ url()->current() . '?page=' . ($currentPage - 1) }}"
                                            class="page-link"><i class="previous"></i></a></li>
                                    <li class="page-item active"><a
                                            href="{{ url()->current() . '?page=' . $currentPage }}"
                                            class="page-link">{{ $currentPage }}</a></li>
                                    <li class="page-item" {{ $hidden }}><a
                                            href="{{ url()->current() . '?page=' . $nextPage }}"
                                            class="page-link">{{ $nextPage }}</a></li>
                                    <li class="page-item next {{ $disablednext }}"><a
                                            href="{{ url()->current() . '?page=' . $nextPage }}" class="page-link"><i
                                                class="next"></i></a></li>
                                </ul>

                            </div>
                        @endif
                        <!-- end::Pagination -->
                    @endif
                </div>
                <!--end::Card body-->
            </div>
            <!--end::Card-->

        </div>
    </div>
@endsection

@section('js')

    <script>

        var r = document.querySelector('[data-action="submit"]');
        // form element
        var formSub = document.querySelector('#kt_basic_form');

        var target = document.querySelector("#kt_post");

        var blockUI = new KTBlockUI(target, {
            message: '<div class="blockui-message"><span class="spinner-border text-primary"></span> Loading...</div>',
        });

        $(".btnSelect").on('click', function () {
            var id = $(this).data("id");
            var url = "{{url('cuti/administrasi')}}?string=" + id;

            //alert(url);
            window.location.href = url;
        });

        $("#searchData").keyup(function (event) {
            var search = $("#searchData").val();
            var url = '{{url("/cuti/pengelolaan_cuti?key=")}}' + search;

            if (event.keyCode === 13) {
                window.location.href = url;
            }
        });

    </script>

@endsection
