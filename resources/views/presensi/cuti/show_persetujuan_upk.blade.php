@extends('layout_new.master')
<?php
// dd(session()->get('login_info')['_role']);
$searchValue = $_GET['nama'] ?? '';
?>

@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <!--begin::Card-->
            <div class="card card-flush">
                <!--begin::Card header-->
                <div class="card-header mt-6">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <!--begin::Search-->
                        <div class="d-flex align-items-center position-relative my-1 me-5">
                            <input type="text" data-kt-permissions-table-filter="search"
                                   class="form-control form-control-solid w-250px ps-15" placeholder="Search Usulan"
                                   id="searchData" value="{{ $searchValue }}"/>
                        </div>
                        <!--end::Search-->
                    </div>
                    <!--end::Card title-->
                    <!--begin::Card toolbar-->
                    <div class="card-toolbar">

                    </div>
                    <!--end::Card toolbar-->
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body pt-0">
                    <div class="card card-stretch-50 card-bordered mb-5 p-10" id="card-header">
                        <strong>
                            <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x fs-6 ms-2">
                                <li class="nav-item">
                                    <a class="nav-link active text-white" data-bs-toggle="tab" href="#kt_lokal">Lokal</a>
                                </li>
                                @if(in_array('ROLE_UPK_PUSAT',session()->get('login_info')['_role_list']))
                                    <li class="nav-item">
                                        <a class="nav-link text-white" data-bs-toggle="tab" href="#kt_nasional">Nasional</a>
                                    </li>
                                @endif
                                <li class="nav-item">
                                    <a class="nav-link text-white" data-bs-toggle="tab" href="#kt_ctt">Cuti Tambahan</a>
                                </li>
                            </ul>
                        </strong>
                    </div>

                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="kt_lokal" role="tabpanel">
                            @if(isset($alert_message))
                                {!! $alert_message !!}
                            @else
                                <!--begin::Table-->
                                <table class="table align-middle table-striped fs-6 gy-5 mb-0"
                                       id="kt_permissions_table">
                                    <!--begin::Table head-->
                                    <thead>
                                    <!--begin::Table row-->
                                    <tr class="text-start text-white fw-bolder fs-7 text-uppercase gs-0"
                                        style="background-color:#02275d;">
                                        <th class="min-w-50px text-center">No</th>
                                        <th class="min-w-100px">Nama Pegawai</th>
                                        <th class="min-w-120px">Jenis Cuti</th>
                                        <th class="min-w-1505px">Tanggal Cuti</th>
                                        <th class="min-w-100px">Tanggal Permohonan</th>
                                        <th class="min-w-100px">Status</th>
                                        <th class="min-w-150px">Alasan</th>
                                        <th class="text-center min-w-100px">Actions</th>
                                    </tr>
                                    <!--end::Table row-->
                                    </thead>
                                    <!--end::Table head-->
                                    <!--begin::Table body-->
                                    <tbody class="fw-bold text-gray-600">
                                        <?php $no = $numberOfElements; ?>
                                    @foreach ($permohonanCuti as $usulan)
                                            <?php
                                            $tanggalCuti = AppHelper::convertDateMDY($usulan->tanggalMulai) . ' - ' . AppHelper::convertDateMDY($usulan->tanggalSelesai);
                                            $text = 'text-dark';
                                            if (2 == $usulan->status) {
                                                $text = 'text-success';
                                            } else if (preg_match('/3|4/i', $usulan->status)) {
                                                $text = 'text-warning';
                                            } else if (preg_match('/5|6/i', $usulan->status)) {
                                                $text = 'text-danger';
                                            } else if (preg_match('/1/i', $usulan->status)) {
                                                $text = 'text-primary';
                                            }

                                            $idCuti = '';
                                            $alasanPembatalan = '';
                                            if (isset($usulan->cutiPegawai->id)) {
                                                $idCuti = $usulan->cutiPegawai->id;
                                                $alasanPembatalan = $usulan->alasanPembatalan;
                                            }
                                            ?>
                                        <tr id="cuti{{$usulan->id}}">
                                            <td class="text-center">{{ $no }}</td>
                                            <td>
                                                {{ $usulan->nama }}
                                                <hr/>
                                                ({{ $usulan->nomorTicket }})
                                            </td>
                                            <td>{{ $usulan->jnsCuti->namaCuti }}</td>
                                            <td>
                                                {{ AppHelper::convertDateDMY($usulan->tanggalMulai) }}
                                                &nbsp - &nbsp
                                                {{ AppHelper::convertDateDMY($usulan->tanggalSelesai) }}
                                                <hr/>
                                                ({{ $usulan->lamaCuti }} hari)
                                            </td>
                                            <td>
                                                {{ AppHelper::convertDateDMY($usulan->tanggalBuat) }}
                                            </td>
                                            <td>
                                                <span class='{{ $text }}'>
                                                    {!! $usulan->ketStatus !!}
                                                </span>
                                            </td>
                                            <td>
                                                {!! $usulan->alasan !!}
                                            </td>
                                            <td class="text-center">
                                                @if((1 == $usulan->status || 3 == $usulan->status) &&
                                                        (1 == $usulan->approvalAtasanLangsung && 1 == $usulan->approvalAtasanBerwenang)

                                                )
                                                    <button class="btn btn-sm btn-icon btn-success btnApprove mt-1"
                                                            data-id="{{ $usulan->id }}"
                                                            data-url="{{url('cuti/approvePermohonan/upk')}}"
                                                            data-bs-toggle="tooltip" data-bs-placement="top"
                                                            data-status="{{ $usulan->status }}"
                                                            data-id-cuti="{{ $idCuti }}"
                                                            data-jns-cuti="{{$usulan->jnsCuti->jnsCuti}}"
                                                            data-alasan-pembatalan="{{ $alasanPembatalan }}"
                                                            title="Approve">
                                                        <i class="bi bi-calendar2-check text-white fs-2x"></i>
                                                    </button>

                                                    <button class="btn btn-sm btn-icon btn-danger btnTolak mt-1"
                                                            data-id="{{ $usulan->id }}"
                                                            data-id-tambahan="{{ $usulan->permohonanCutiTambahan->id ?? "" }}"
                                                            data-url="{{url('cuti/tolakPermohonan/upk')}}"
                                                            data-id-cuti="{{ $idCuti }}"
                                                            data-bs-toggle="tooltip" data-bs-placement="top"
                                                            data-status="{{ $usulan->status }}"
                                                            data-jns-cuti="{{$usulan->jnsCuti->jnsCuti}}" title="Tolak">
                                                        <i class="bi bi-calendar2-x text-white fs-2x"></i>
                                                    </button>
                                                @endif
                                            </td>
                                        </tr>
                                            <?php $no++; ?>
                                    @endforeach
                                    </tbody>
                                    <!--end::Table body-->
                                </table>

                                <!--end::Table-->
                                @if ($totalItems != 0)
                                    <!--begin::pagination-->
                                    <div class="d-flex flex-stack flex-wrap pt-5 p-3">
                                        <div class="fs-6 fw-bold text-gray-700"></div>

                                        @php
                                            $disabled = '';
                                            $nextPage= '';
                                            $disablednext = '';
                                            $hidden = '';
                                            $pagination = '';

                                            // tombol pagination
                                            if ($totalItems == 0) {
                                                // $pagination = 'none';
                                            }

                                            // tombol back
                                            if($currentPage == 1){
                                                $disabled = 'disabled';
                                            }

                                            // tombol next
                                            if($currentPage != $totalPages) {
                                                $nextPage = $currentPage + 1;
                                            }

                                            // tombol pagination
                                            if($currentPage == $totalPages || $totalPages == 0){
                                                $disablednext = 'disabled';
                                                $hidden = 'hidden';
                                            }

                                        @endphp

                                        <ul class="pagination" style="display: {{ $pagination }}">
                                            <li class="page-item disabled"><a href="#"
                                                                              class="page-link">Halaman {{ $currentPage }}
                                                    dari {{ $totalPages }}</a></li>

                                            <li class="page-item previous {{ $disabled }}"><a
                                                    href="{{ url()->current() . '?page=' . ($currentPage - 1) }}"
                                                    class="page-link"><i class="previous"></i></a></li>
                                            <li class="page-item active"><a
                                                    href="{{ url()->current() . '?page=' . $currentPage }}"
                                                    class="page-link">{{ $currentPage }}</a></li>
                                            <li class="page-item" {{ $hidden }}><a
                                                    href="{{ url()->current() . '?page=' . $nextPage }}"
                                                    class="page-link">{{ $nextPage }}</a></li>
                                            <li class="page-item next {{ $disablednext }}"><a
                                                    href="{{ url()->current() . '?page=' . $nextPage }}"
                                                    class="page-link"><i class="next"></i></a></li>
                                        </ul>

                                    </div>
                                @endif
                                <!-- end::Pagination -->
                            @endif
                        </div>
                        <div class="tab-pane fade show" id="kt_ctt" role="tabpanel">
                            @if(isset($alert_message))
                                {!! $alert_message !!}
                            @else
                                <!--begin::Table-->
                                <div class="table-responsive bg-gray-400 rounded border border-gray-300">
                                    <table class="table align-middle table-striped fs-6 gy-5 mb-0"
                                           id="tabel_detail_cuti_tambahan">

                                        <thead class="fw-bolder bg-secondary fs-6">
                                        <tr class="text-start text-white fw-bolder fs-7 text-uppercase gs-0"
                                            style="background-color:#02275d;">
                                            <th class="min-w-50px text-center">No</th>
                                            <th class="min-w-100px">Nama Pegawai</th>
                                            <th class="text-center min-w-100px">Jenis Cuti Tambahan</th>
                                            <th class="text-center min-w-100px">Tanggal Cuti Tambahan</th>
                                            <th class="text-center min-w-100px">Status</th>
                                            <th class="min-w-150px">Alasan</th>
                                            <th class="text-center min-w-100px">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody class="fw-bold text-gray-600">
                                            <?php $no = $numberOfElements; $cutiTambahanDiajukan = 0; ?>
                                        @foreach ($permohonanCutiTambahan as $cutiTambahan)
                                                <?php if (empty($cutiTambahan->approvalAtasanLangsung)) {
                                                continue;
                                            } ?>
                                                <?php
                                                $tanggalCuti = AppHelper::convertDateMDY($cutiTambahan->tanggalMulai) . ' - ' . AppHelper::convertDateMDY($cutiTambahan->tanggalSelesai);
                                                $text = 'text-dark';
                                                if (2 == $cutiTambahan->status) {
                                                    $text = 'text-success';
                                                } else if (preg_match('/3|4/i', $cutiTambahan->status)) {
                                                    $text = 'text-warning';
                                                } else if (preg_match('/5|6/i', $cutiTambahan->status)) {
                                                    $text = 'text-danger';
                                                } else if (preg_match('/1/i', $cutiTambahan->status)) {
                                                    $text = 'text-primary';
                                                }
                                                if ('' == $cutiTambahan->approvalAtasanLangsung && session()->get('user')['pegawaiId'] == $cutiTambahan->permohonanCuti->atasanLangsung) {
                                                    $step = '1.1';
                                                } else {
                                                    $step = '1.2';
                                                }

                                                $idCuti = '';
                                                $alasanPembatalan = '';




                                                if ($cutiTambahan->permohonanCuti->atasanLangsung == $cutiTambahan->permohonanCuti->atasanBerwenang) {
                                                    $aLeQualaBTambahan = 1;
                                                } else {
                                                    $aLeQualaBTambahan = 0;
                                                }
                                                ?>
                                            <tr>
                                                @if (isset($cutiTambahan->jenis))
                                                        <?php
                                                        $tanggalLCBTambahan = AppHelper::convertDateMDY($cutiTambahan->tanggalMulai) . ' - ' . AppHelper::convertDateMDY($cutiTambahan->tanggalSelesai);
                                                        if (($cutiTambahan->status == 1 || $cutiTambahan->status == 0)) {
                                                            $cutiTambahanDiajukan++;
                                                        }
                                                        ?>
                                                    <td class="text-center">{{ $no }}</td>
                                                    <td>
                                                        {{ $cutiTambahan->permohonanCuti->nama }}
                                                        <hr/>
                                                        ({{ $cutiTambahan->permohonanCuti->nomorTicket }})
                                                    </td>
                                                    <td class="text-center">{{$cutiTambahan->jenis->nama ?? ""}}</td>
                                                    <td>
                                                        {{AppHelper::convertDateDMY($cutiTambahan->tanggalMulai ?? "") }}
                                                        &nbsp - &nbsp
                                                        {{AppHelper::convertDateDMY($cutiTambahan->tanggalSelesai ?? "") }}
                                                        <hr/>
                                                        ({{ $cutiTambahan->lamaCutiTambahan ?? "" }} hari)
                                                    </td>

                                                    <td>
                                                        {!! $cutiTambahan->ketStatus !!}
                                                    </td>

                                                    <td class="text-center">{{ $cutiTambahan->alasanCutiTambahan ?? "" }}</td>
                                                    <td class="text-center">
                                                        @if((1 == $cutiTambahan->status || 3 == $cutiTambahan->status) &&
                                                            (1 == $cutiTambahan->approvalAtasanLangsung && 1 == $cutiTambahan->approvalAtasanBerwenang)

                                                            )
                                                            <button
                                                                class="btn btn-sm btn-icon btn-success btnApproveTambahan mt-1"
                                                                data-id-tambahan="{{ $cutiTambahan->id }}"
                                                                data-url="{{url('cuti/tambahan/approvePermohonan/upk')}}"
                                                                data-bs-toggle="tooltip" data-bs-placement="top"
                                                                data-status="{{ $cutiTambahan->status }}"
                                                                data-id-cuti="{{ $cutiTambahan->id }}"
                                                                data-alasan-pembatalan="{{ $alasanPembatalan }}"
                                                                title="Approve">
                                                                <i class="bi bi-calendar2-check text-white fs-2x"></i>
                                                            </button>

                                                            <button
                                                                class="btn btn-sm btn-icon btn-danger btnTolakTambahan mt-1"
                                                                data-id-tambahan="{{ $cutiTambahan->id }}"
                                                                data-url="{{url('cuti/tambahan/tolakPermohonan/upk')}}"
                                                                data-bs-toggle="tooltip" data-bs-placement="top"
                                                                data-status="{{ $cutiTambahan->status }}"
                                                                data-id-cuti="{{ $cutiTambahan->id }}"
                                                                title="Tolak">
                                                                <i class="bi bi-calendar2-x text-white fs-2x"></i>
                                                            </button>
                                                        @endif
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                <!--end::Table-->
                                @if ($totalItems != 0)
                                    <!--begin::pagination-->
                                    <div class="d-flex flex-stack flex-wrap pt-5 p-3">
                                        <div class="fs-6 fw-bold text-gray-700"></div>

                                        @php
                                            $disabled = '';
                                            $nextPage= '';
                                            $disablednext = '';
                                            $hidden = '';
                                            $pagination = '';

                                            // tombol pagination
                                            if ($totalItems == 0) {
                                                // $pagination = 'none';
                                            }

                                            // tombol back
                                            if($currentPage == 1){
                                                $disabled = 'disabled';
                                            }

                                            // tombol next
                                            if($currentPage != $totalPages) {
                                                $nextPage = $currentPage + 1;
                                            }

                                            // tombol pagination
                                            if($currentPage == $totalPages || $totalPages == 0){
                                                $disablednext = 'disabled';
                                                $hidden = 'hidden';
                                            }

                                        @endphp

                                        <ul class="pagination" style="display: {{ $pagination }}">
                                            <li class="page-item disabled"><a href="#"
                                                                              class="page-link">Halaman {{ $currentPage }}
                                                    dari {{ $totalPages }}</a></li>

                                            <li class="page-item previous {{ $disabled }}"><a
                                                    href="{{ url()->current() . '?page=' . ($currentPage - 1) }}"
                                                    class="page-link"><i class="previous"></i></a></li>
                                            <li class="page-item active"><a
                                                    href="{{ url()->current() . '?page=' . $currentPage }}"
                                                    class="page-link">{{ $currentPage }}</a></li>
                                            <li class="page-item" {{ $hidden }}><a
                                                    href="{{ url()->current() . '?page=' . $nextPage }}"
                                                    class="page-link">{{ $nextPage }}</a></li>
                                            <li class="page-item next {{ $disablednext }}"><a
                                                    href="{{ url()->current() . '?page=' . $nextPage }}"
                                                    class="page-link"><i class="next"></i></a></li>
                                        </ul>

                                    </div>
                                @endif
                                <!-- end::Pagination -->
                            @endif
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Card-->

                    <!--begin::Modal - Add permohonan-->
                    <div class="modal fade" tabindex="-1" id="kt_modal_permohonan_tambahan">
                        <!--begin::Modal dialog-->
                        <div class="modal-dialog modal-dialog-centered mw-1000px">
                            <!--begin::Modal content-->
                            <div class="modal-content">
                                <!--begin::Modal header-->
                                <div class="modal-header">
                                    <!--begin::Modal title-->
                                    <h2 class="fw-bolder text-white">Form Penolakan Cuti Tambahan</h2>
                                    <!--end::Modal title-->
                                    <!--begin::Close-->
                                    <div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal">
                                        <!--begin::Svg Icon | path: icons/duotone/Navigation/Close.svg-->
                                        <span class="svg-icon svg-icon-1">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                                       fill="#000000">
                                        <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                        <rect fill="#000000" opacity="0.5"
                                              transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                              x="0" y="7" width="16" height="2" rx="1"/>
                                    </g>
                                </svg>
                            </span>
                                        <!--end::Svg Icon-->
                                    </div>
                                    <!--end::Close-->
                                </div>
                                <!--end::Modal header-->
                                <!--begin::Modal body-->
                                <div class="modal-body scroll-y mx-5 mx-xl-15 my-5">
                                    <!--begin::Form-->
                                    <form class="form w-lg-500px mx-auto" id="kt_basic_form_tambahan" method="post">
                                        @csrf

                                        {{-- menampilkan error validasi --}}
                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif

                                        <!--begin::Input group-->
                                        <div class="fv-row mb-8">
                                            <!--begin::Label-->
                                            <label class="required fs-6 fw-bold mb-2">Alasan Di Tolak</label>
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            <textarea class="form-control form-control-solid" rows="3"
                                                      placeholder="Mohon isi penjelasan" id="catatan"
                                                      name="catatan">{{ old('catatan') }}</textarea>
                                            <!--end::Input-->
                                        </div>
                                        <!--end::Input group-->

                                        <!--begin::Input group-->
                                        <div class="fv-row mb-8">
                                            <button type="button" class="btn btn-primary" data-action="submit"
                                                    id="btn-submit-tambahan">
                                    <span class="indicator-label">
                                        Submit
                                    </span>
                                                <span class="indicator-progress">
                                        Please wait... <span
                                                        class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                    </span>
                                            </button>
                                        </div>
                                        <!--end::Input group-->

                                    </form>
                                </div>
                                <!--end::Modal body-->
                            </div>
                            <!--end::Modal content-->
                        </div>
                        <!--end::Modal dialog-->
                    </div>
                    <!--end::Modal - Add permohonan-->

                </div>
            </div>
            <!--begin::Modal - Add permohonan-->
            <div class="modal fade" id="kt_modal_permohonan" tabindex="-1" aria-hidden="true">
                <!--begin::Modal dialog-->
                <div class="modal-dialog modal-dialog-centered mw-1000px">
                    <!--begin::Modal content-->
                    <div class="modal-content">
                        <!--begin::Modal header-->
                        <div class="modal-header">
                            <!--begin::Modal title-->
                            <h2 class="fw-bolder text-white">Form Penolakan Cuti </h2>
                            <!--end::Modal title-->
                            <!--begin::Close-->
                            <div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal">
                                <!--begin::Svg Icon | path: icons/duotone/Navigation/Close.svg-->
                                <span class="svg-icon svg-icon-1">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                                       fill="#000000">
                                        <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"/>
                                        <rect fill="#000000" opacity="0.5"
                                              transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                              x="0" y="7" width="16" height="2" rx="1"/>
                                    </g>
                                </svg>
                            </span>
                                <!--end::Svg Icon-->
                            </div>
                            <!--end::Close-->
                        </div>
                        <!--end::Modal header-->
                        <!--begin::Modal body-->
                        <div class="modal-body scroll-y mx-5 mx-xl-15 my-5">
                            <!--begin::Form-->
                            <form class="form w-lg-500px mx-auto" id="kt_basic_form" method="post">
                                @csrf

                                {{-- menampilkan error validasi --}}
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <!--begin::Input group-->
                                <div class="fv-row mb-8">
                                    <!--begin::Label-->
                                    <label class="required fs-6 fw-bold mb-2">Alasan Di Tolak</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <textarea class="form-control form-control-solid" rows="3"
                                              placeholder="Mohon isi penjelasan" id="catatan"
                                              name="catatan">{{ old('catatan') }}</textarea>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->

                                <!--begin::Input group-->
                                <div class="fv-row mb-8">
                                    <button type="button" class="btn btn-primary" data-action="submit" id="btn-submit">
                                    <span class="indicator-label">
                                        Submit
                                    </span>
                                        <span class="indicator-progress">
                                        Please wait... <span
                                                class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                    </span>
                                    </button>
                                </div>
                                <!--end::Input group-->

                            </form>
                        </div>
                        <!--end::Modal body-->
                    </div>
                    <!--end::Modal content-->
                </div>
                <!--end::Modal dialog-->
            </div>
            <!--end::Modal - Add permohonan-->
        </div>
    </div>

@endsection

@section('js')

    <script>

        var r = document.querySelector('[data-action="submit"]');
        // form element
        var formSub = document.querySelector('#kt_basic_form');

        var target = document.querySelector("#kt_post");

        var blockUI = new KTBlockUI(target, {
            message: '<div class="blockui-message"><span class="spinner-border text-primary"></span> Loading...</div>',
        });

        $(".btnTolak").click(function () {
            $('#kt_modal_permohonan').modal('show');

            var id = $(this).data("id");
            var idtambahan = $(this).data("id-tambahan");
            var url = $(this).data("url");
            var token = $("meta[name='csrf-token']").attr("content");

            $('#btn-submit').attr('data-id', id);
            $('#btn-submit').attr('data-id-tambahan', idtambahan);
            $('#btn-submit').attr('data-url', url);

        });

        $("#btn-submit").on('click', function () {

            var id = $(this).data("id");
            var idtambahan = $(this).data("id-tambahan");
            var url = $(this).data("url");
            var token = $("meta[name='csrf-token']").attr("content");
            var status = $(this).data("status");
            var idCuti = $(this).data("id-cuti");
            var catatan = $('#catatan').val();

            Swal.fire({
                title: 'Yakin akan menolak permohonan?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009ef7',
                cancelButtonColor: '#C5584B',
                confirmButtonText: 'Kirim!',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#kt_modal_permohonan').modal('hide');
                    blockUI.block();
                    $.ajax({
                        type: 'post',
                        url: url + '?string=' + btoa(id),
                        data: {
                            _token: token,
                            idPermohonan: id,
                            idCutiTambahan: idtambahan,
                            status: status,
                            idCuti: idCuti,
                            catatan: catatan
                        },
                        success: function (response) {
                            //alert(JSON.stringify(response));
                            if (response['status'] === 1) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'permohonan cuti berhasil di tolak',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                blockUI.release();
                                window.location.href = document.URL;
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Proses Penolakan Cuti Gagal!',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                blockUI.release();
                                location.reload();
                            }
                        },
                        error: function (err) {
                            console.log(err);
                            blockUI.release();
                        }
                    });
                }
            })
        });

        $(".btnApprove").click(function () {
            var id = $(this).data("id");
            var url = $(this).data("url");
            var status = $(this).data("status");
            var idCuti = $(this).data("id-cuti");
            var jnsCuti = $(this).data("jns-cuti");
            var token = $("meta[name='csrf-token']").attr("content");

            blockUI.block();
            $.ajax({
                type: 'post',
                url: url + '?string=' + btoa(id),
                data: {
                    _token: token,
                    idPermohonan: id,
                    status: status,
                    idCuti: idCuti,
                    jnsCuti: jnsCuti
                },
                success: function (response) {
                    // alert(JSON.stringify(response));
                    if (response['status'] === 1) {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Berhasil',
                            text: 'permohonan cuti berhasil di setujui',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        blockUI.release();
                        window.location.href = document.URL;
                    } else {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Proses approval cuti pegawai gagal!',
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                        blockUI.release();
                        location.reload();
                    }
                },
                error: function (err) {
                    console.log(err);
                    blockUI.release();
                }
            });
        });

        var r = document.querySelector('[data-action="submit"]');
        // form element
        var formSub = document.querySelector('#kt_basic_form_tambahan');

        var target = document.querySelector("#kt_post_tambahan");

        var blockUI = new KTBlockUI(target, {
            message: '<div class="blockui-message"><span class="spinner-border text-primary"></span> Loading...</div>',
        });

        $(".btnTolakTambahan").click(function () {
            $('#kt_modal_permohonan_tambahan').modal('show');

            var id = $(this).data("id-tambahan");
            var url = $(this).data("url");
            var status = $(this).data("status");
            var token = $("meta[name='csrf-token']").attr("content");

            $('#btn-submit-tambahan').attr('data-id-tambahan', id);
            $('#btn-submit-tambahan').attr('data-url', url);
            $('#btn-submit-tambahan').attr('data-status', status);

        });

        $("#btn-submit-tambahan").on('click', function () {

            var id = $(this).data("id-tambahan");
            var url = $(this).data("url");
            var step = $(this).data("step");
            var alequalab = $(this).data("alequalab-tambahan");
            var token = $("meta[name='csrf-token']").attr("content");
            var catatan = $('#catatan').val();

            Swal.fire({
                title: 'Yakin akan menolak permohonan?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009ef7',
                cancelButtonColor: '#C5584B',
                confirmButtonText: 'Kirim!',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#kt_modal_permohonan_tambahan').modal('hide');
                    blockUI.block();
                    $.ajax({
                        type: 'post',
                        url: url + '?string=' + btoa(id),
                        data: {
                            _token: token,
                            idCutiTambahan: id,
                            step: step,
                            alequalab: alequalab,
                            catatan: catatan
                        },
                        success: function (response) {
                            //alert(JSON.stringify(response));
                            if (response['status'] === 1) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'permohonan cuti berhasil di tolak',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                blockUI.release();
                                window.location.href = document.URL;
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Proses Penolakan Cuti Gagal!',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                blockUI.release();
                                location.reload();
                            }
                        },
                        error: function (err) {
                            console.log(err);
                            blockUI.release();
                        }
                    });
                }
            })
        });
        $(".btnApproveTambahan").click(function () {
            var id = $(this).data("id-tambahan");
            var idCuti = $(this).data("id-cuti");
            var url = $(this).data("url");
            var status = $(this).data("status");
            var alasanPembatalan = $(this).data("alasan-pembatalan");
            var token = $("meta[name='csrf-token']").attr("content");

            blockUI.block();
            $.ajax({
                type: 'post',
                url: url + '?string=' + btoa(id),
                data: {
                    _token: token,
                    idCutiTambahan: id,
                    status: status,
                    alasanPembatalan: alasanPembatalan,
                    idCuti: idCuti
                },
                success: function (response) {
                    //alert(JSON.stringify(response));
                    if (response['status'] === 1) {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Berhasil',
                            text: 'permohonan berhasil di setujui',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        blockUI.release();
                        window.location.href = document.URL;
                    } else {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Proses approval permohonan gagal!',
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                        blockUI.release();
                        location.reload();
                    }
                },
                error: function (err) {
                    console.log(err);
                    blockUI.release();
                }
            });
        });

        $('.modal-child').on('show.bs.modal', function () {
            var modalParent = $(this).attr('data-modal-parent');
            $(modalParent).css('opacity', 0);
        });

        $('.modal-child').on('hidden.bs.modal', function () {
            var modalParent = $(this).attr('data-modal-parent');
            $(modalParent).css('opacity', 1);
        });

    </script>

@endsection
