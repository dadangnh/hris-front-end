@extends('layout_new.master')

@section('content')
    <?php
    //dd($usulanJKU);
    ?>
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            @if(isset($alert_message))
                {!! $alert_message !!}
            @else
                <!--begin::Card-->
                <div class="card card-flush">
                    <!--begin::Card header-->
                    <div id="card-header" class="card-header">
                        <div class="card-title fw-bold fs-4 text-white">
                            Administrasi Jam Kerja (Cuti)
                        </div>
                    </div>
                    <!--end::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body">
                        <div class="mb-12">
                        </div>
                        <!--begin::Table-->
                        <div class="table-responsive rounded border">
                            <table class="table align-middle table-striped fs-6 gy-5 mb-0" id="kt_permissions_table">
                                <!--begin::Table head-->
                                <thead>
                                <!--begin::Table row-->
                                <tr class="text-start text-white fw-bolder fs-7 text-uppercase gs-0"
                                    style="background-color:#02275d;">
                                    <th class="min-w-50px text-center">No</th>
                                    <th class="min-w-100px">Nomor Tiket</th>
                                    <th class="min-w-120px">Tanggal</th>
                                    <th class="min-w-125px">Jam Masuk (Cuti)</th>
                                    <th class="min-w-125px">Jam Pulang (Cuti)</th>
                                    <th class="min-w-125px">Jenis Cuti</th>
                                    <th class="min-w-225px">Keterangan</th>
                                    <th class="min-w-100px">Status</th>
                                    <th class="text-center min-w-100px">Actions</th>
                                </tr>
                                <!--end::Table row-->
                                </thead>
                                <!--end::Table head-->
                                <!--begin::Table body-->
                                <tbody class="fw-bold text-gray-600">
                                    <?php $no = $numberOfElements; ?>
                                @foreach ($usulanJKC as $usulan)
                                        <?php
                                        $tanggalJKU = AppHelper::convertDateMDY($usulan->tanggalMulai) . ' - ' . AppHelper::convertDateMDY($usulan->tanggalSelesai);
                                        ?>
                                    <tr id="libur{{$usulan->id}}">
                                        <td class="text-center">{{ $no }}</td>
                                        <td>{{ $usulan->nomorTicket }}</td>
                                        <td>
                                            {{ AppHelper::convertDateDMY($usulan->tanggalMulai) }}
                                            <hr/>
                                            {{ AppHelper::convertDateDMY($usulan->tanggalSelesai) }}
                                        </td>
                                        <td>
                                            {{ AppHelper::extractTime($usulan->jamMasukCuti) }}
                                        </td>
                                        <td>
                                            {{ AppHelper::extractTime($usulan->jamPulangCuti) }}
                                        </td>
                                        <td>
                                            {{ $usulan->jenisCuti->namaCuti }}
                                            @if(!empty($usulan->typeCuti))
                                                <br/>
                                                <span class="text-info">({{ (1 == $usulan->typeCuti) ? 'Pagi' : 'Siang' }})</span>
                                            @endif
                                        </td>
                                        <td>
                                            {{ $usulan->keterangan }}
                                        </td>
                                        <td>
                                            {!! (1 == $usulan->status)? "<span class=\"text-success\">Disetujui</span>" : "<span class=\"text-danger\">Proses Persetujuan</span>" !!}
                                        </td>
                                        <td class="text-center">
                                            @if(1 != $usulan->status)
                                                <button class="btn btn-sm btn-success btnKirimUsulan mt-1"
                                                        data-id="{{ $usulan->id }}"
                                                        data-url="{{url('jamkerjacuti/persetujuan/approveUsulan')}}"
                                                        data-bs-toggle="tooltip"
                                                        data-bs-placement="top" title="Approve">
                                                    <i class="fa fa-thumbs-up"></i> Approve
                                                </button>
                                            @endif
                                        </td>
                                    </tr>
                                        <?php $no++; ?>
                                @endforeach
                                </tbody>
                                <!--end::Table body-->
                            </table>
                        </div>
                        <!--end::Table-->
                        @if ($totalItems != 0)
                            <!--begin::pagination-->
                            <div class="d-flex flex-stack flex-wrap pt-5 p-3">
                                <div class="fs-6 fw-bold text-gray-700"></div>

                                @php
                                    $disabled = '';
                                    $nextPage= '';
                                    $disablednext = '';
                                    $hidden = '';
                                    $pagination = '';

                                    // tombol pagination
                                    if ($totalItems == 0) {
                                        // $pagination = 'none';
                                    }

                                    // tombol back
                                    if($currentPage == 1){
                                        $disabled = 'disabled';
                                    }

                                    // tombol next
                                    if($currentPage != $totalPages) {
                                        $nextPage = $currentPage + 1;
                                    }

                                    // tombol pagination
                                    if($currentPage == $totalPages || $totalPages == 0){
                                        $disablednext = 'disabled';
                                        $hidden = 'hidden';
                                    }

                                @endphp

                                <ul class="pagination" style="display: {{ $pagination }}">
                                    <li class="page-item disabled"><a href="#"
                                                                      class="page-link">Halaman {{ $currentPage }}
                                            dari {{ $totalPages }}</a></li>

                                    <li class="page-item previous {{ $disabled }}"><a
                                            href="{{ url()->current() . '?page=' . ($currentPage - 1) }}"
                                            class="page-link"><i class="previous"></i></a></li>
                                    <li class="page-item active"><a
                                            href="{{ url()->current() . '?page=' . $currentPage }}"
                                            class="page-link">{{ $currentPage }}</a></li>
                                    <li class="page-item" {{ $hidden }}><a
                                            href="{{ url()->current() . '?page=' . $nextPage }}"
                                            class="page-link">{{ $nextPage }}</a></li>
                                    <li class="page-item next {{ $disablednext }}"><a
                                            href="{{ url()->current() . '?page=' . $nextPage }}" class="page-link"><i
                                                class="next"></i></a></li>
                                </ul>

                            </div>
                        @endif
                        <!-- end::Pagination -->
                    </div>
                    <!--end::Card body-->
                </div>
                <!--end::Card-->
            @endif
        </div>
    </div>

@endsection

@section('js')
    <script>
        $(".btnKirimUsulan").click(function () {
            var id = $(this).data("id");
            var url = $(this).data("url");
            var token = $("meta[name='csrf-token']").attr("content");

            $.ajax({
                type: 'post',
                url: url,
                data: {
                    _token: token,
                    idUsulan: id
                },
                success: function (response) {
                    if (response['status'] === 1) {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Berhasil',
                            text: 'usulan berhasil di Approve',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        window.location.href = document.URL;
                    } else {
                        alert(response.message);
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Gagal approve usulan!',
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },
                error: function (err) {
                    console.log(err)
                }
            });
        });

        $("#btnTolakPermohonan").click(function () {
            $('#btnTolakUsulan').data('id'); //getter
            $('#btnTolakUsulan').attr('data-id-usulan', $(this).data("id")); //setter
        });

        $("#btnTolakUsulan").click(function () {
            var id = $(this).data("id-usulan");
            var url = $(this).data("url");
            var keterangan = $('#keteranganPenolakan').val();
            var token = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                title: 'Yakin akan menolak usulan ini?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#C5584B',
                cancelButtonColor: '#B2B2B2',
                confirmButtonText: 'Tolak!',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'post',
                        url: url,
                        data: {
                            _token: token,
                            idUsulan: id,
                            keterangan: keterangan,
                            statusUsulan: 2
                        },
                        success: function (response) {
                            //alert(JSON.stringify(response));
                            if (response['status'] === 1) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'usulan berhasil di tolak',
                                    showConfirmButton: false,
                                    timer: 1500
                                });

                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal menolak usulan!',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                console.log(response)
                            }
                            $('#kt_modal_tolak').modal('toggle');
                            window.location.href = document.URL;
                        },
                        error: function (err) {
                            //console.log(err)
                        }
                    });
                }
            })
        });

    </script>
@endsection
