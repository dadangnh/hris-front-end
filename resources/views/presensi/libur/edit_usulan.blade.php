@extends('layout_new.master')
<?php
//dd($selectedAgama);
?>

@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <!--begin::Card-->
            <div class="card card-flush">
                <!--begin::Card header-->
                <div class="card-header mt-6">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <h2><u>Form Edit Permohonan Usulan</u></h2>
                    </div>
                    <!--end::Card title-->
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body pt-0">
                    <!--begin::Stepper-->
                    <div class="stepper stepper-links d-flex flex-column" id="kt_stepper_example_basic">
                        <!--begin::Nav-->
                        <div class="stepper-nav py-5">
                            <!--begin::Step 1-->
                            <div class="stepper-item current" data-kt-stepper-element="nav">
                                <h3 class="stepper-title">1. Dasar Hukum Penetapan</h3>
                            </div>
                            <!--end::Step 1-->
                            <!--begin::Step 2-->
                            <div class="stepper-item" data-kt-stepper-element="nav">
                                <h3 class="stepper-title">2. Usul Hari Libur</h3>
                            </div>
                            <!--end::Step 2-->
                            <!--begin::Step 3-->
                            <div class="stepper-item" data-kt-stepper-element="nav">
                                <h3 class="stepper-title">3. Scope Detail</h3>
                            </div>
                            <!--end::Step 3-->
                            <!--begin::Step 4-->
                            <div class="stepper-item" data-kt-stepper-element="nav">
                                <h3 class="stepper-title">4. Completed</h3>
                            </div>
                            <!--end::Step 4-->
                        </div>
                        <!--end::Nav-->

                        <!--begin::Form-->
                        <form class="form w-lg-700px mx-auto" id="kt_stepper_example_basic_form"
                              action="{{ route('harilibur.createPermohonan') }}" method="post">
                            @csrf
                            <!--begin::Group-->
                            <div class="mb-5">
                                <!--begin::Step 1-->
                                <div class="flex-column current" data-kt-stepper-element="content">
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label">Dasar Hukum</label>
                                        <!--end::Label-->

                                        <!--begin::Input-->
                                        <input type="text" class="form-control form-control-solid"
                                               name="nomorSuratDasar" id="nomorSuratDasar" placeholder=""
                                               value="{{ $dataUsulan->nomorSuratDasar }}"/>
                                        <input type="hidden" class="form-control form-control-solid" name="idUsulan"
                                               id="idUsulan" placeholder="" value="{{ $dataUsulan->id }}"/>
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->

                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="required fs-6 fw-bold mb-2">Tanggal Dasar Hukum</label>
                                        <!--end::Label-->
                                        <!--begin::Wrapper-->
                                        <div class="position-relative d-flex align-items-center">
                                            <!--begin::Icon-->
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <!--begin::Svg Icon | path: icons/duotone/Layout/Layout-grid.svg-->
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                         xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                         height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none"
                                                           fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"/>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                  height="4" rx="1"/>
                                                            <path
                                                                d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                fill="#000000"/>
                                                        </g>
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->
                                            </span>
                                            </div>
                                            <!--end::Icon-->
                                            <!--begin::Input-->
                                            <input class="form-control form-control-solid ps-12" placeholder="Pick date"
                                                   name="tanggalSuratDasar" id="tanggalSuratDasar"
                                                   value="{{ $dataUsulan->tanggalSuratDasar }}"/>
                                            <!--end::Input-->
                                        </div>
                                        <!--end::Wrapper-->
                                    </div>
                                    <!--end::Input group-->

                                    <!--begin::Input group-->
                                    <div class="fv-row mb-8">
                                        <!--begin::Label-->
                                        <label class="required fs-6 fw-bold mb-2">Keterangan Dasar Hukum</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <textarea class="form-control form-control-solid" rows="3"
                                                  placeholder="Mohon isi penjelasan mengenai dasar hukum diatas"
                                                  id="keteranganSuratDasar"
                                                  name="keteranganSuratDasar">{{ $dataUsulan->keteranganSuratDasar }}</textarea>
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->

                                </div>
                                <!--begin::Step 1-->

                                <!--begin::Step 2-->
                                <div class="flex-column" data-kt-stepper-element="content">
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="required fs-6 fw-bold mb-2">Tanggal Hari Libur</label>
                                        <!--end::Label-->
                                        <!--begin::Wrapper-->
                                        <div class="position-relative d-flex align-items-center">
                                            <!--begin::Icon-->
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <!--begin::Svg Icon | path: icons/duotone/Layout/Layout-grid.svg-->
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                         xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                         height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none"
                                                           fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"/>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4"
                                                                  height="4" rx="1"/>
                                                            <path
                                                                d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                fill="#000000"/>
                                                        </g>
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->
                                            </span>
                                            </div>
                                            <!--end::Icon-->
                                            <!--begin::Input-->
                                            <input class="form-control form-control-solid ps-12"
                                                   placeholder="Pick date rage" id="tanggalHariLibur"
                                                   name="tanggalHariLibur"
                                                   value="{{ $dataUsulan->hariLibur->tanggalHariLibur }}"/>
                                            <input type="hidden" class="form-control form-control-solid"
                                                   name="idHariLibur" id="idHariLibur" placeholder=""
                                                   value="{{$dataUsulan->hariLibur->id}}"/>
                                            <!--end::Input-->
                                        </div>
                                        <!--end::Wrapper-->
                                    </div>
                                    <!--end::Input group-->

                                    <!--begin::Input group-->
                                    <div class="fv-row mb-8">
                                        <!--begin::Label-->
                                        <label class="required fs-6 fw-bold mb-2">Keterangan Hari Libur</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <textarea class="form-control form-control-solid" rows="3"
                                                  placeholder="Mohon isi penjelasan hari libur" id="keteranganHariLibur"
                                                  name="keterangan">{{ $dataUsulan->hariLibur->keterangan }}</textarea>
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->

                                    <!--begin::Input group-->
                                    <div class="d-flex flex-column mb-5 fv-row">
                                        <!--begin::Label-->
                                        <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                            <span class="required">Jenis Hari Libur</span>
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Select-->
                                        <select class="form-select form-select-solid" data-control="select2"
                                                data-placeholder="Select an option" data-allow-clear="true" name="jenis"
                                                id="jenis">
                                            @foreach ($jenisLibur as $jenis)
                                                <option
                                                    value="{{ $jenis->id }}" {{ (($dataUsulan->hariLibur->jenis->id == $jenis->id) ? 'selected' : '')  }}>{{ $jenis->nama }}</option>
                                            @endforeach
                                        </select>
                                        <!--end::Select-->
                                    </div>
                                    <!--end::Input group-->

                                    <!--begin::Input group-->
                                    <div class="d-flex flex-column mb-5 fv-row" id='div-scope'>
                                        <!--begin::Label-->
                                        <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                            <span>Scope Hari Libur</span>
                                            <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip"
                                               title="Harap diisi apabila jenis libur merupakan libur lokal/fakultatif"></i>
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Select-->
                                        <select class="form-select form-select-solid" data-control="select2"
                                                data-placeholder="Select an option" data-allow-clear="true" name="scope"
                                                id="scope">
                                            <option></option>
                                            @foreach ($scope as $key => $val)
                                                <option
                                                    value="{{ $key }}" {{ (($dataUsulan->hariLibur->scope == $key) ? 'selected' : '')  }}>{{ $val }}</option>
                                            @endforeach
                                        </select>
                                        <!--end::Select-->
                                    </div>
                                    <!--end::Input group-->
                                </div>
                                <!--begin::Step 2-->

                                <!--begin::Step 3-->
                                <div class="flex-column" data-kt-stepper-element="content">
                                    <!--begin::Input group-->
                                    <div class="d-flex flex-column mb-5 fv-row" id="div-provinsi">
                                        <!--begin::Label-->
                                        <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                            <span>Provinsi</span>
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Select-->
                                        <select class="form-select form-select-solid select-provinsi"
                                                data-placeholder="Select an option" data-allow-clear="true"
                                                name="provinsiIds[]" id="provinsiIds" multiple="multiple">
                                            @if (!empty($selectedProvinsi))
                                                @foreach ($selectedProvinsi as $p => $provinsi)
                                                    <option value="{{ $provinsi['id'] }}"
                                                            selected>{{ $provinsi['nama'] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <!--end::Select-->
                                    </div>
                                    <!--end::Input group-->

                                    <!--begin::Input group-->
                                    <div class="d-flex flex-column mb-5 fv-row" id="div-kota">
                                        <!--begin::Label-->
                                        <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                            <span>Kota</span>
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Select-->
                                        <select class="form-select form-select-solid select-kota"
                                                data-placeholder="Select an option" data-allow-clear="true"
                                                name="kotaiIds[]" id="kotaiIds" multiple="multiple">
                                            <option></option>
                                        </select>
                                        <!--end::Select-->
                                    </div>
                                    <!--end::Input group-->

                                    <!--begin::Input group-->
                                    <div class="d-flex flex-column mb-5 fv-row" id="div-agama">
                                        <!--begin::Label-->
                                        <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                            <span>Agama</span>
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Select-->
                                        <select class="form-select form-select-solid select-agama"
                                                data-placeholder="Select an option" data-allow-clear="true"
                                                name="agamaIds[]" id="agamaIds" multiple="multiple">
                                            @if (!empty($dataUsulan->hariLibur->agamaIds))
                                                @foreach ($selectedAgama as $agama)
                                                    <option value="{{ $agama['id'] }}"
                                                            selected>{{ $agama['nama'] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <!--end::Select-->
                                    </div>
                                    <!--end::Input group-->

                                    <!--begin::Input group-->
                                    <div class="d-flex flex-column mb-5 fv-row" id="div-kantor">
                                        <!--begin::Label-->
                                        <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                            <span>Kantor</span>
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Select-->
                                        <select class="form-select form-select-solid select-kantor"
                                                data-placeholder="Select an option" data-allow-clear="true"
                                                name="kantorIds[]" id="kantorIds" multiple="multiple">
                                            <option></option>
                                        </select>
                                        <!--end::Select-->
                                    </div>
                                    <!--end::Input group-->
                                </div>
                                <!--begin::Step 3-->

                                <!--begin::Step 4-->
                                <div class="flex-column" data-kt-stepper-element="content">
                                    <!--begin::Wrapper-->
                                    <div class="w-100">
                                        <!--begin::Heading-->
                                        <div class="pb-12 text-center">
                                            <!--begin::Title-->
                                            <h1 class="fw-bolder text-dark">Usulan Selesai Dibuat!</h1>
                                            <!--end::Title-->
                                            <!--begin::Description-->
                                            <div class="text-muted fw-bold fs-4">Silahkan klik submit untuk mengirim
                                                permohonan usulan hari libur Bapak/Ibu
                                            </div>
                                            <!--end::Description-->
                                        </div>
                                        <!--end::Heading-->
                                    </div>
                                    <!--end::Wrapper-->
                                </div>
                                <!--begin::Step 4-->
                            </div>
                            <!--end::Group-->

                            <!--begin::Actions-->
                            <div class="d-flex flex-stack">
                                <!--begin::Wrapper-->
                                <div class="me-2">
                                    <button type="button" class="btn btn-light btn-active-light-primary"
                                            data-kt-stepper-action="previous">
                                        Back
                                    </button>
                                </div>
                                <!--end::Wrapper-->

                                <!--begin::Wrapper-->
                                <div>
                                    <button type="button" class="btn btn-primary" data-kt-stepper-action="submit">
                                    <span class="indicator-label">
                                        Submit
                                    </span>
                                        <span class="indicator-progress">
                                        Please wait... <span
                                                class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                    </span>
                                    </button>

                                    <button type="button" class="btn btn-primary" data-kt-stepper-action="next">
                                        Continue
                                    </button>
                                </div>
                                <!--end::Wrapper-->
                            </div>
                            <!--end::Actions-->
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Stepper-->
                </div>
                <!--begin::Card Footer-->
                <div class="card-footer">
                    <button class="btn btn-sm btn-danger btnKembali mt-1" data-bs-toggle="tooltip"
                            data-bs-placement="top" title="Kembali">
                        <i class="fa fa-long-arrow-alt-left"></i> Kembali
                    </button>
                </div>
                <!--end::Card Footer-->
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        // Stepper lement
        var element = document.querySelector("#kt_stepper_example_basic");

        // stepper submit element
        var r = element.querySelector('[data-kt-stepper-action="submit"]');

        // Initialize Stepper
        var stepper = new KTStepper(element);

        // Handle next step
        stepper.on("kt.stepper.next", function (stepper) {
            currentStep = stepper.getCurrentStepIndex();
            jenis = $(formSub.querySelector('[name="jenis"]')).val();

            if (2 == currentStep && (1 == jenis || 4 == jenis)) {
                stepper.goTo(4);
            } else {
                stepper.goNext(); // go next step
            }
        });

        // Handle previous step
        stepper.on("kt.stepper.previous", function (stepper) {
            currentStep = stepper.getCurrentStepIndex();
            jenis = $(formSub.querySelector('[name="jenis"]')).val();

            if (4 == currentStep && (1 == jenis || 4 == jenis)) {
                stepper.goTo(2);
            } else {
                stepper.goPrevious(); // go previous step
            }
        });

        // form element
        var formSub = document.querySelector('#kt_stepper_example_basic_form');

        $(formSub.querySelector('[name="jenis"]')).on("change", (function () {
            var jenis = $(formSub.querySelector('[name="jenis"]'));

            //currentStep = stepper.getCurrentStepIndex();

            if (1 == jenis.val() || 4 == jenis.val()) {
                element.querySelector('#div-scope').style.setProperty("display", "none", "important");
                stepper.goTo(4);
            } else {
                element.querySelector('#div-scope').style.setProperty("display", "block", "important");
            }
        }));

        $(formSub.querySelector('[name="scope"]')).on("change", (function () {
            var scope = $(formSub.querySelector('[name="scope"]'));

            if (1 == scope.val()) {
                element.querySelector('#div-provinsi').style.setProperty("display", "block", "important");
                element.querySelector('#div-kota').style.setProperty("display", "none", "important");
                element.querySelector('#div-agama').style.setProperty("display", "none", "important");
                element.querySelector('#div-kantor').style.setProperty("display", "none", "important");
            } else if (2 == scope.val()) {
                element.querySelector('#div-provinsi').style.setProperty("display", "block", "important");
                element.querySelector('#div-kota').style.setProperty("display", "block", "important");
                element.querySelector('#div-agama').style.setProperty("display", "none", "important");
                element.querySelector('#div-kantor').style.setProperty("display", "none", "important");
            } else if (3 == scope.val()) {
                element.querySelector('#div-provinsi').style.setProperty("display", "none", "important");
                element.querySelector('#div-kota').style.setProperty("display", "none", "important");
                element.querySelector('#div-agama').style.setProperty("display", "none", "important");
                element.querySelector('#div-kantor').style.setProperty("display", "block", "important");
            } else if (4 == scope.val()) {
                element.querySelector('#div-provinsi').style.setProperty("display", "none", "important");
                element.querySelector('#div-kota').style.setProperty("display", "none", "important");
                element.querySelector('#div-agama').style.setProperty("display", "block", "important");
                element.querySelector('#div-kantor').style.setProperty("display", "none", "important");
            } else if (5 == scope.val()) {
                element.querySelector('#div-provinsi').style.setProperty("display", "block", "important");
                element.querySelector('#div-kota').style.setProperty("display", "none", "important");
                element.querySelector('#div-agama').style.setProperty("display", "block", "important");
                element.querySelector('#div-kantor').style.setProperty("display", "none", "important");
            } else {
                element.querySelector('#div-provinsi').style.setProperty("display", "block", "important");
                element.querySelector('#div-kota').style.setProperty("display", "block", "important");
                element.querySelector('#div-agama').style.setProperty("display", "block", "important");
                element.querySelector('#div-kantor').style.setProperty("display", "block", "important");
            }

        }));

        r.addEventListener("click", (function (e) {
            r.disabled = !0, r.setAttribute("data-kt-indicator", "on"), setTimeout((function () {
                r.removeAttribute("data-kt-indicator"), r.disabled = !1, Swal.fire({
                    text: "Form has been successfully submitted!",
                    icon: "success",
                    buttonsStyling: !1,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                }).then((function () {
                    formSub.submit();
                }))
            }), 2e3)
        }));

        $("#tanggalSuratDasar").daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1901,
                maxYear: parseInt(moment().format("YYYY"), 10)
            }/*, function(start, end, label) {
                var years = moment().diff(start, "years");
                alert("You are " + years + " years old!");
            }*/
        );

        $("#tanggalHariLibur").daterangepicker();

        $(".btnKembali").on('click', function () {
            window.location.href = "{{ url('/harilibur/administrasi') }}";
        });

        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //var scope = {{ $dataUsulan->hariLibur->scope }};
            var scope = $(formSub.querySelector('[name="scope"]')).val();

            if (1 == scope) {
                element.querySelector('#div-provinsi').style.setProperty("display", "block", "important");
                element.querySelector('#div-kota').style.setProperty("display", "none", "important");
                element.querySelector('#div-agama').style.setProperty("display", "none", "important");
                element.querySelector('#div-kantor').style.setProperty("display", "none", "important");
            } else if (2 == scope) {
                element.querySelector('#div-provinsi').style.setProperty("display", "block", "important");
                element.querySelector('#div-kota').style.setProperty("display", "block", "important");
                element.querySelector('#div-agama').style.setProperty("display", "none", "important");
                element.querySelector('#div-kantor').style.setProperty("display", "none", "important");
            } else if (3 == scope) {
                element.querySelector('#div-provinsi').style.setProperty("display", "none", "important");
                element.querySelector('#div-kota').style.setProperty("display", "none", "important");
                element.querySelector('#div-agama').style.setProperty("display", "none", "important");
                element.querySelector('#div-kantor').style.setProperty("display", "block", "important");
            } else if (4 == scope) {
                element.querySelector('#div-provinsi').style.setProperty("display", "none", "important");
                element.querySelector('#div-kota').style.setProperty("display", "none", "important");
                element.querySelector('#div-agama').style.setProperty("display", "block", "important");
                element.querySelector('#div-kantor').style.setProperty("display", "none", "important");
            } else if (5 == scope) {
                element.querySelector('#div-provinsi').style.setProperty("display", "block", "important");
                element.querySelector('#div-kota').style.setProperty("display", "none", "important");
                element.querySelector('#div-agama').style.setProperty("display", "block", "important");
                element.querySelector('#div-kantor').style.setProperty("display", "none", "important");
            } else {
                element.querySelector('#div-provinsi').style.setProperty("display", "block", "important");
                element.querySelector('#div-kota').style.setProperty("display", "block", "important");
                element.querySelector('#div-agama').style.setProperty("display", "block", "important");
                element.querySelector('#div-kantor').style.setProperty("display", "block", "important");
            }

            /*$(".select-provinsi").select2({
                ajax: {
                    url: "{{ url('/harilibur/getProvinsiList') }}",
                    dataType: 'json',
                    delay: 250,
                    allowClear: true,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        //console.log(JSON.stringify(data));
                        params.page = params.page || 1;
                        return {
                            results: data,
                            pagination: {
                            more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: 'Search for a Provinsi',
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });*/

            $(".select-provinsi").select2({
                minimumInputLength: 1,
                ajax: {
                    url: "{{ url('/harilibur/getProvinsiList') }}",
                    dataType: 'json',
                    delay: 250,
                    allowClear: true,
                    data: function (params) {
                        //alert(params.term);
                        return {
                            q: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    }
                }
            });

            $(".select-kota").select2({
                ajax: {
                    url: "{{ url('/harilibur/getKotaList') }}",
                    dataType: 'json',
                    delay: 250,
                    allowClear: true,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        return {
                            results: data,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: 'Search for a Kabupaten/Kota',
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });

            $(".select-kantor").select2({
                ajax: {
                    url: "{{ url('/harilibur/getKantorList') }}",
                    dataType: 'json',
                    delay: 250,
                    allowClear: true,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        return {
                            results: data.kantors,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: 'Search for a Kantor',
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });

            $(".select-agama").select2({
                minimumInputLength: 1,
                ajax: {
                    url: "{{ url('/harilibur/getAgamaList') }}",
                    dataType: 'json',
                    delay: 250,
                    allowClear: true,
                    data: function (params) {
                        //alert(params.term);
                        return {
                            search: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    }
                }
            });
        });

        function formatRepo(repo) {
            if (repo.loading) {
                return repo.nama;
            }

            var $container = $(
                "<div class='select2-result-text'>" + repo.nama + "</div>"
            );

            return $container;
        }

        function formatRepoSelection(repo) {
            return repo.nama;
        }

    </script>
@endsection
