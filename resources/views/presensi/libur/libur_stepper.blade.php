@extends('layout_new.master')

@section('content')
    <!--begin::Stepper-->
    <div class="stepper stepper-links d-flex flex-column" id="kt_stepper_example_basic">
        <!--begin::Nav-->
        <div class="stepper-nav py-5">
            <!--begin::Step 1-->
            <div class="stepper-item current" data-kt-stepper-element="nav">
                <h3 class="stepper-title">Dasar Hukum Penetapan</h3>
            </div>
            <!--end::Step 1-->
            <!--begin::Step 2-->
            <div class="stepper-item" data-kt-stepper-element="nav">
                <h3 class="stepper-title">Usul Hari Libur</h3>
            </div>
            <!--end::Step 2-->
            <!--begin::Step 3-->
            <div class="stepper-item" data-kt-stepper-element="nav">
                <h3 class="stepper-title">Konfirmasi Hari Libur</h3>
            </div>
            <!--end::Step 3-->
            <!--begin::Step 4-->
            <div class="stepper-item" data-kt-stepper-element="nav">
                <h3 class="stepper-title">Completed</h3>
            </div>
            <!--end::Step 4-->
        </div>
        <!--end::Nav-->

        <!--begin::Form-->
        <form class="form w-lg-500px mx-auto" id="kt_stepper_example_basic_form"
              action="{{ route('harilibur.createPermohonan') }}" method="post">
            @csrf
            <!--begin::Group-->
            <div class="mb-5">
                <!--begin::Step 1-->
                <div class="flex-column current" data-kt-stepper-element="content">
                    <!--begin::Input group-->
                    <div class="fv-row mb-10">
                        <!--begin::Label-->
                        <label class="form-label">Example Label 1</label>
                        <!--end::Label-->

                        <!--begin::Input-->
                        <input type="text" class="form-control form-control-solid" name="input1" placeholder=""
                               value=""/>
                        <!--end::Input-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="fv-row mb-10">
                        <!--begin::Label-->
                        <label class="form-label">Example Label 2</label>
                        <!--end::Label-->

                        <!--begin::Input-->
                        <input type="text" class="form-control form-control-solid" name="input2" placeholder=""
                               value=""/>
                        <!--end::Input-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="fv-row mb-10">
                        <!--begin::Label-->
                        <label class="form-label">Example Label 3</label>
                        <!--end::Label-->

                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input class="form-check-input" type="checkbox" checked="checked" value="1"/>
                            <span class="form-check-label">
                                Switch
                            </span>
                        </label>
                        <!--end::Switch-->
                    </div>
                    <!--end::Input group-->
                </div>
                <!--begin::Step 1-->

                <!--begin::Step 1-->
                <div class="flex-column" data-kt-stepper-element="content">
                    <!--begin::Input group-->
                    <div class="fv-row mb-10">
                        <!--begin::Label-->
                        <label class="form-label">Example Label 1</label>
                        <!--end::Label-->

                        <!--begin::Input-->
                        <input type="text" class="form-control form-control-solid" name="input1" placeholder=""
                               value=""/>
                        <!--end::Input-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="fv-row mb-10">
                        <!--begin::Label-->
                        <label class="form-label">Example Label 2</label>
                        <!--end::Label-->

                        <!--begin::Input-->
                        <textarea class="form-control form-control-solid" rows="3" name="input2"
                                  placeholder=""></textarea>
                        <!--end::Input-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="fv-row mb-10">
                        <!--begin::Label-->
                        <label class="form-label">Example Label 3</label>
                        <!--end::Label-->

                        <!--begin::Input-->
                        <label class="form-check form-check-custom form-check-solid">
                            <input class="form-check-input" checked="checked" type="checkbox" value="1"/>
                            <span class="form-check-label">
                                Checkbox
                            </span>
                        </label>
                        <!--end::Input-->
                    </div>
                    <!--end::Input group-->
                </div>
                <!--begin::Step 1-->

                <!--begin::Step 1-->
                <div class="flex-column" data-kt-stepper-element="content">
                    <!--begin::Input group-->
                    <div class="fv-row mb-10">
                        <!--begin::Label-->
                        <label class="form-label d-flex align-items-center">
                            <span class="required">Input 1</span>
                            <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip"
                               title="Example tooltip"></i>
                        </label>
                        <!--end::Label-->

                        <!--begin::Input-->
                        <input type="text" class="form-control form-control-solid" name="input1" placeholder=""
                               value=""/>
                        <!--end::Input-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="fv-row mb-10">
                        <!--begin::Label-->
                        <label class="form-label">
                            Input 2
                        </label>
                        <!--end::Label-->

                        <!--begin::Input-->
                        <input type="text" class="form-control form-control-solid" name="input2" placeholder=""
                               value=""/>
                        <!--end::Input-->
                    </div>
                    <!--end::Input group-->
                </div>
                <!--begin::Step 1-->

                <!--begin::Step 1-->
                <div class="flex-column" data-kt-stepper-element="content">
                    <!--begin::Input group-->
                    <div class="fv-row mb-10">
                        <!--begin::Label-->
                        <label class="form-label d-flex align-items-center">
                            <span class="required">Input 1</span>
                            <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip"
                               title="Example tooltip"></i>
                        </label>
                        <!--end::Label-->

                        <!--begin::Input-->
                        <input type="text" class="form-control form-control-solid" name="input1" placeholder=""
                               value=""/>
                        <!--end::Input-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="fv-row mb-10">
                        <!--begin::Label-->
                        <label class="form-label">
                            Input 2
                        </label>
                        <!--end::Label-->

                        <!--begin::Input-->
                        <input type="text" class="form-control form-control-solid" name="input2" placeholder=""
                               value=""/>
                        <!--end::Input-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="fv-row mb-10">
                        <!--begin::Label-->
                        <label class="form-label">
                            Input 3
                        </label>
                        <!--end::Label-->

                        <!--begin::Input-->
                        <input type="text" class="form-control form-control-solid" name="input3" placeholder=""
                               value=""/>
                        <!--end::Input-->
                    </div>
                    <!--end::Input group-->
                </div>
                <!--begin::Step 1-->
            </div>
            <!--end::Group-->

            <!--begin::Actions-->
            <div class="d-flex flex-stack">
                <!--begin::Wrapper-->
                <div class="me-2">
                    <button type="button" class="btn btn-light btn-active-light-primary"
                            data-kt-stepper-action="previous">
                        Back
                    </button>
                </div>
                <!--end::Wrapper-->

                <!--begin::Wrapper-->
                <div>
                    <button type="button" class="btn btn-primary" data-kt-stepper-action="submit">
                        <span class="indicator-label">
                            Submit
                        </span>
                        <span class="indicator-progress">
                            Please wait... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                        </span>
                    </button>

                    <button type="button" class="btn btn-primary" data-kt-stepper-action="next">
                        Continue
                    </button>
                </div>
                <!--end::Wrapper-->
            </div>
            <!--end::Actions-->
        </form>
        <!--end::Form-->
    </div>
    <!--end::Stepper-->
@endsection

@section('js')
    <script>
        // Stepper lement
        var element = document.querySelector("#kt_stepper_example_basic");

        // stepper submit element
        var r = element.querySelector('[data-kt-stepper-action="submit"]');

        // Initialize Stepper
        var stepper = new KTStepper(element);

        // Handle next step
        stepper.on("kt.stepper.next", function (stepper) {
            stepper.goNext(); // go next step
        });

        // Handle previous step
        stepper.on("kt.stepper.previous", function (stepper) {
            stepper.goPrevious(); // go previous step
        });

        // submitFormElement
        var formSub = document.querySelector('#kt_stepper_example_basic_form');

        r.addEventListener("click", (function (e) {
            r.disabled = !0, r.setAttribute("data-kt-indicator", "on"), setTimeout((function () {
                r.removeAttribute("data-kt-indicator"), r.disabled = !1, Swal.fire({
                    text: "Form has been successfully submitted!",
                    icon: "success",
                    buttonsStyling: !1,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                }).then((function () {
                    formSub.submit();
                }))
            }), 2e3)
        }));
    </script>
@endsection
