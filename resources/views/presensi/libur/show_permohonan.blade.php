@extends('layout_new.master')

@section('content')
    <?php
    //dump($usulanHariLibur);
    //dump($jumlahUsulan);
    ?>
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            @if(isset($alert_message))
                {!! $alert_message !!}
            @else
                <!--begin::Card-->
                <div class="card card-flush">
                    <!--begin::Card header-->
                    <div id="card-header" class="card-header">
                        <div class="card-title fw-bold fs-4 text-white">
                            Administrasi Hari Libur
                        </div>
                    </div>
                    <!--end::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body pt-0 mt-10">
                        <div class="mb-12">
                            <div class="row align-items-center">
                                <div class="col-lg-2 col-xl-2">
                                <span class="svg-icon svg-icon-1 position-absolute ms-6">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"/>
                                            <path
                                                d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z"
                                                fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                            <path
                                                d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z"
                                                fill="#000000" fill-rule="nonzero"/>
                                        </g>
                                    </svg>
                                </span>
                                    <!--end::Svg Icon-->
                                        <?php
                                        if (isset($_GET['nomorSuratDasar'])) {
                                            $searchValue = $_GET['nomorSuratDasar'];
                                        } else if (isset($_GET['nomorTicket'])) {
                                            $searchValue = $_GET['nomorTicket'];
                                        } else if (isset($_GET['keteranganSuratDasar'])) {
                                            $searchValue = $_GET['keteranganSuratDasar'];
                                        } else {
                                            $searchValue = "";
                                        }
                                        ?>

                                    <select class="form-select form-select-solid w-250px" data-control="select2"
                                            data-placeholder="Select an option" data-allow-clear="true"
                                            name="searchKategori" id="searchKategori">
                                        <option
                                            value="nomorSuratDasar" {{ (isset($_GET['nomorSuratDasar']) ? 'selected' : '') }}>
                                            Nomor Surat Dasar
                                        </option>
                                        <option
                                            value="nomorTicket" {{ (isset($_GET['nomorTicket']) ? 'selected' : '') }}>
                                            Nomor Ticket
                                        </option>
                                        <option
                                            value="keteranganSuratDasar" {{ (isset($_GET['keteranganSuratDasar']) ? 'selected' : '') }}>
                                            Keterangan Surat Dasar
                                        </option>
                                    </select>
                                </div>
                                <div class="col-lg-10 col-xl-10 mt-5 mt-lg-0">
                                    <input type="text" data-kt-permissions-table-filter="search"
                                           class="form-control form-control-solid w-250px ps-15"
                                           placeholder="Search Usulan" id="searchData" value="{{ $searchValue }}"/>
                                </div>
                            </div>
                        </div>
                        <!--begin::Table-->
                        <div class="table-responsive rounded border">
                            <table class="table align-middle table-striped fs-6 gy-5 mb-0" id="kt_permissions_table">
                                <!--begin::Table head-->
                                <thead>
                                <!--begin::Table row-->
                                <tr class="text-start text-white fw-bolder fs-7 text-uppercase gs-0"
                                    style="background-color:#02275d;">
                                    <th class="min-w-50px text-center">No</th>
                                    <th class="min-w-100px">No Dasar Hukum</th>
                                    <th class="min-w-120px">Keterangan Dasar Hukum</th>
                                    <th class="min-w-125px">Tanggal Hari Libur</th>
                                    <th class="min-w-125px">Jenis</th>
                                    <th class="min-w-125px">scope</th>
                                    <th class="text-center min-w-100px">Actions</th>
                                </tr>
                                <!--end::Table row-->
                                </thead>
                                <!--end::Table head-->
                                <!--begin::Table body-->
                                <tbody class="fw-bold text-gray-600">
                                    <?php $no = $numberOfElements; ?>
                                @foreach ($usulanHariLibur as $usulan)
                                    <tr id="libur{{$usulan->id}}">
                                        <td class="text-center">{{ $no }}</td>
                                        <td>{{ $usulan->nomorSuratDasar }}
                                            <hr/>{{ AppHelper::convertDateDMY($usulan->tanggalSuratDasar) }}</td>
                                        <td>{{ $usulan->keteranganSuratDasar }}</td>
                                        <td>
                                                <?php
                                                if (isset($usulan->hariLibur->id)) {
                                                    $rangeHariLibur = AppHelper::convertDateDMY($usulan->hariLibur->tanggalAwal) . " - " .
                                                        AppHelper::convertDateDMY($usulan->hariLibur->tanggalAkhir);
                                                    echo $rangeHariLibur . '<hr/><span class="text-info">(' .
                                                        $usulan->hariLibur->keterangan . ')</span>';

                                                    $idHariLibur = $usulan->hariLibur->id;

                                                    // if(null == $usulan->hariLibur->scope){
                                                    //     $usulan->hariLibur->scope = '';
                                                    // }
                                                } else {
                                                    $idHariLibur = '';
                                                }

                                                if (!isset($usulan->hariLibur->provinsiIds)) {
                                                    $usulan->hariLibur->provinsiIds = [];
                                                }

                                                ?>
                                        </td>
                                        <td>
                                            <span class="text-success">{{ $usulan->hariLibur->jenis->nama }}</span>
                                        </td>
                                        <td class="text-danger">
                                            <span>{{ (0 != $usulan->hariLibur->scope)? $scope[$usulan->hariLibur->scope] : ""}}</span>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-sm btn-danger mt-1" data-bs-toggle="modal"
                                                    data-bs-target="#kt_modal_tolak"
                                                    id="btnTolakPermohonan"
                                                    data-url="{{url('harilibur/persetujuan/tolakUsulan')}}"
                                                    data-id="{{ $usulan->id }}" title="Tolak">
                                                <i class="fa fa-thumbs-down"></i>Tolak
                                            </button>

                                            <button class="btn btn-sm btn-success btnKirimUsulan mt-1"
                                                    data-id="{{ $usulan->id }}" data-idharilibur="{{$idHariLibur}}"
                                                    data-url="{{url('harilibur/persetujuan/approveUsulan')}}"
                                                    data-status-usulan="{{$statusUsulan}}" data-bs-toggle="tooltip"
                                                    data-bs-placement="top" title="Approve">
                                                <i class="fa fa-thumbs-up"></i> Approve
                                            </button>
                                        </td>
                                    </tr>
                                        <?php $no++; ?>
                                @endforeach
                                </tbody>
                                <!--end::Table body-->
                            </table>
                        </div>
                        <!--end::Table-->
                        @if ($totalItems != 0)
                            <!--begin::pagination-->
                            <div class="d-flex flex-stack flex-wrap pt-5 p-3">
                                <div class="fs-6 fw-bold text-gray-700"></div>

                                @php
                                    $disabled = '';
                                    $nextPage= '';
                                    $disablednext = '';
                                    $hidden = '';
                                    $pagination = '';

                                    // tombol pagination
                                    if ($totalItems == 0) {
                                        // $pagination = 'none';
                                    }

                                    // tombol back
                                    if($currentPage == 1){
                                        $disabled = 'disabled';
                                    }

                                    // tombol next
                                    if($currentPage != $totalPages) {
                                        $nextPage = $currentPage + 1;
                                    }

                                    // tombol pagination
                                    if($currentPage == $totalPages || $totalPages == 0){
                                        $disablednext = 'disabled';
                                        $hidden = 'hidden';
                                    }

                                @endphp

                                <ul class="pagination" style="display: {{ $pagination }}">
                                    <li class="page-item disabled"><a href="#"
                                                                      class="page-link">Halaman {{ $currentPage }}
                                            dari {{ $totalPages }}</a></li>

                                    <li class="page-item previous {{ $disabled }}"><a
                                            href="{{ url()->current() . '?page=' . ($currentPage - 1) }}"
                                            class="page-link"><i class="previous"></i></a></li>
                                    <li class="page-item active"><a
                                            href="{{ url()->current() . '?page=' . $currentPage }}"
                                            class="page-link">{{ $currentPage }}</a></li>
                                    <li class="page-item" {{ $hidden }}><a
                                            href="{{ url()->current() . '?page=' . $nextPage }}"
                                            class="page-link">{{ $nextPage }}</a></li>
                                    <li class="page-item next {{ $disablednext }}"><a
                                            href="{{ url()->current() . '?page=' . $nextPage }}" class="page-link"><i
                                                class="next"></i></a></li>
                                </ul>

                            </div>
                        @endif
                        <!-- end::Pagination -->
                    </div>
                    <div class="modal fade" tabindex="-1" id="kt_modal_tolak">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Form Tolak Usulan</h5>

                                    <!--begin::Close-->
                                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                         data-bs-dismiss="modal" aria-label="Close">
                                        <span class="svg-icon svg-icon-2x"></span>
                                    </div>
                                    <!--end::Close-->
                                </div>

                                <div class="modal-body">
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-8">
                                        <!--begin::Label-->
                                        <label class="required fs-6 fw-bold mb-2">Alasan Penolakan</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <textarea class="form-control form-control-solid" rows="3"
                                                  placeholder="Mohon isi alasan penolakan" id="keteranganPenolakan"
                                                  name="keteranganPenolakan">{{ old('keteranganPenolakan') }}</textarea>
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                    <button class="btn btn-sm btn-danger btnTolakUsulan mt-1"
                                            data-url="{{url('harilibur/persetujuan/tolakUsulan')}}"
                                            id="btnTolakUsulan" data-id-usulan=""
                                            data-bs-toggle="tooltip" data-bs-placement="top" title="Tolak">
                                        <i class="fa fa-thumbs-down"></i>Tolak
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Card body-->
                </div>
                <!--end::Card-->
            @endif
        </div>
        <!--end::Container-->
    </div>

@endsection

@section('js')
    <script>
        $(".btnKirimUsulan").click(function () {
            var id = $(this).data("id");
            var idharilibur = $(this).data("idharilibur");
            var url = $(this).data("url");
            var statusUsulan = $(this).data('status-usulan');
            var token = $("meta[name='csrf-token']").attr("content");

            $.ajax({
                type: 'post',
                url: url,
                data: {
                    _token: token,
                    idUsulan: id,
                    idHariLibur: idharilibur,
                    statusUsulan: statusUsulan
                },
                success: function (response) {
                    if (response['status'] === 1) {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Berhasil',
                            text: 'usulan berhasil di Approve',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        window.location.href = document.URL;
                    } else {
                        alert(response.message);
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Gagal approve usulan!',
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },
                error: function (err) {
                    console.log(err)
                }
            });
        });

        $("#btnTolakPermohonan").click(function () {
            $('#btnTolakUsulan').data('id'); //getter
            $('#btnTolakUsulan').attr('data-id-usulan', $(this).data("id")); //setter
        });

        $("#btnTolakUsulan").click(function () {
            var id = $(this).data("id-usulan");
            var url = $(this).data("url");
            var keterangan = $('#keteranganPenolakan').val();
            var token = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                title: 'Yakin akan menolak usulan ini?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#C5584B',
                cancelButtonColor: '#B2B2B2',
                confirmButtonText: 'Tolak!',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'post',
                        url: url,
                        data: {
                            _token: token,
                            idUsulan: id,
                            keterangan: keterangan,
                            statusUsulan: 2
                        },
                        success: function (response) {
                            //alert(JSON.stringify(response));
                            if (response['status'] === 1) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'usulan berhasil di tolak',
                                    showConfirmButton: false,
                                    timer: 1500
                                });

                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal menolak usulan!',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                console.log(response)
                            }
                            $('#kt_modal_tolak').modal('toggle');
                            window.location.href = document.URL;
                        },
                        error: function (err) {
                            //console.log(err)
                        }
                    });
                }
            })
        });

        $("#searchData").keyup(function (event) {
            var kategori = $("#searchKategori").val();
            var search = $("#searchData").val();
            var url = '{{url("/harilibur/persetujuan?")}}' + kategori + '=' + search;

            if (event.keyCode === 13) {
                window.location.href = url;
            }
        });
    </script>
@endsection
