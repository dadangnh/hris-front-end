@foreach ($menu['menu'] as $list)

                {{-- sub menu --}}
                @if (count($list['sub']) > 0)

                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion" id="menu-here-{{ $list[0] }}">
                        <span class="menu-link">
                            <span class="menu-icon">
                                <span class="svg-icon svg-icon-2">
                                    <i class="{{ $list[3] }} fs-3"></i>
                                </span>
                            </span>
                            <span class="menu-title">{{ $list[1] }}</span>
                            <span class="menu-arrow"></span>
                        </span>
                        @foreach ($list['sub'] as $list_sub2)
                            @if (count($list_sub2['sub']) > 0)
                                <div class="menu-sub menu-sub-accordion" id="menu-acc-{{ $list_sub2[0] }}">
                                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion"
                                         id="menu-here-{{ $list_sub2[0] }}">
                                        <span class="menu-link">
                                            <span class="menu-bullet">
                                                <span class="bullet bullet-dot"></span>
                                            </span>
                                            <span class="menu-title">{{ $list_sub2[1] }}</span>
                                            <span class="menu-arrow"></span>
                                        </span>
                                        @foreach ($list_sub2['sub'] as $list_sub3)
                                            <div class="menu-sub menu-sub-accordion" id="menu-acc2-{{ $list_sub2[0] }}">
                                                <div class="menu-item">
                                                    <a class="menu-link" id="menu-a-{{ $list_sub3[0] }}"
                                                       href="{{ url($list_sub3[2]) }}">
                                                        <span class="menu-bullet">
                                                            <span class="bullet bullet-dot"></span>
                                                        </span>
                                                        <span class="menu-title">{{ $list_sub3[1] }}</span>
                                                    </a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @else
                                <div class="menu-sub menu-sub-accordion" id="menu-acc-{{ $list_sub2[0] }}">
                                    <div class="menu-item">
                                        <a class="menu-link" id="menu-a-{{ $list_sub2[0] }}"
                                           href="{{ url($list_sub2[2]) }}">
                                            <span class="menu-bullet">
                                                <span class="bullet bullet-dot"></span>
                                            </span>
                                            <span class="menu-title">{{ $list_sub2[1] }}</span>
                                        </a>
                                    </div>
                                </div>
                            @endif
                        @endforeach

                    </div>

                @else

                    {{-- non sub menu --}}
                    <div class="menu-item">
                        <a class="menu-link" id="menu-a-{{ $list[0] }}" href="{{ url($list[2]) }}">
                            <span class="menu-icon">
                                <span class="svg-icon svg-icon-2">
                                    <i class="{{ $list[3] }} fs-3"></i>
                                </span>
                            </span>
                            <span class="menu-title">{{ $list[1] }}</span>
                        </a>
                    </div>
                @endif
            @endforeach
