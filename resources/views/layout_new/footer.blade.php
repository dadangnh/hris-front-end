<!--begin::Javascript-->
<script>var hostUrl = "assets/";</script>
<!--begin::Global Javascript Bundle(used by all pages)-->
{{--<script src="{{ url('assets/dist') }}/assets_new/plugins/global/plugins.bundle.js"></script>--}}
<script src="{{ url('assets/dist') }}/assets/plugins/global/plugins.bundle.js"></script>
<script src="{{ url('assets/dist') }}/assets_new/js/scripts.bundle.js"></script>
<script src="{{ url('assets/dist') }}/assets_new/js/custom/widgets.js"></script>
<script src="{{ url('assets/dist') }}/assets_new/plugins/custom/datatables/datatables.bundle.js"></script>
<script src="{{ url('assets/dist') }}/assets_new/plugins/custom/formrepeater/formrepeater.bundle.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" defer></script>
<script src="{{ url('assets/dist') }}/assets_new/plugins/custom/fslightbox/fslightbox.bundle.js"></script>

{{--<script src="{{ url('vendor') }}/jquery-validation/dist/jquery.validate.js"></script>--}}
{{--<script src="{{ url('vendor') }}/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"--}}
{{--        type="text/javascript"></script>--}}
{{--<script src="{{ url('assets/') }}/js/global.js"></script>--}}
<!--end::Page Custom Javascript-->

<!--start::Custom Javascript-->
@yield('js')
<!--end::Custom Javascript-->
<script>
    let app_url = '{{ env('APP_URL') }}'

    var blockUI = new KTBlockUI(document.querySelector("#kt_content"), {
        message: '<div class="blockui-message"><span class="spinner-border text-primary"></span> Loading...</div>',
    });

    function swalError(message) {
        Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Gagal',
            text: message,
            showConfirmButton: false,
            timer: 1800
        });
    }

    function swalSuccess(message) {
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Berhasil',
            text: message,
            showConfirmButton: false,
            timer: 1800
        });
    }

    jQuery(document).ready(function () {
        var arrMenu = <?= $menu['current'] ?>;
        var i;
        for (i = 0; i < arrMenu.length; i++) {
            $("#menu-a-" + arrMenu[i]).addClass("active");
            $("#menu-acc-" + arrMenu[i]).addClass("menu-active-bg")
            $("#menu-acc2-" + arrMenu[i]).addClass("menu-active-bg");
            $("#menu-here-" + arrMenu[i]).addClass("here show");
        }

    });



</script>


