@extends('layout.master')

@section('content')
    @include('sweetalert::alert')
    <div class="post d-flex flex-column" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12" id="loadingtarget">
                <div class="card mb-3 border-warning shadow-sm">
                    <div id="card-header" class="card-header">
                        <div class="card-title fw-bold fs-4 text-white">
                            Tambah Pengusulan Permohonan Pensiun UPK Lokal
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="card">
                            <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                                <div class="p-2">
                                    <div class="row mb-3">
                                        <div class="form-group col-md-1">
                                            <input type="text" class="form-control-plaintext" disabled
                                                   value="NIP Pegawai :">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <input class="form-control formatInput" type="text" name="nip9"
                                                   id="niprekam" placeholder="NIP pendek">
                                        </div>
                                        <div class="form-group col-md-2">
                                            <button id="btnGetPegawai" class="btn btn-primary loadingBtn">
                                                <i class="fa fa-search"></i> Cari
                                            </button>
                                            <button class="btn btn-primary btn-sm fs-6" type="button" hidden disabled>
                                                Loading
                                                <span class="spinner-border spinner-border-sm" role="status"
                                                      aria-hidden="true"></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form id="formAdd" action="{{ url('/pengusulan-pensiun/form-tambah-permohonan-upk') }}"
                              method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="pegawaiId"/>
                            <input type="hidden" name="pangkatId"/>
                            <input type="hidden" name="jabatanId"/>
                            <input type="hidden" name="unitOrgId"/>
                            <input type="hidden" name="namaUnitOrg"/>
                            <input type="hidden" name="kantorId"/>
                            <input type="hidden" name="pegawaiInputId"/>
                            <input type="hidden" name="kdKpp"/>
                            <input type="hidden" name="kdKanwil"/>
                            <input type="hidden" name="kdKantor"/>
                            <div class="block">

                                <div class="card">
                                    <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                                        <div class="p-2">
                                            <!--begin::Table-->
                                            <div class="row mb-3">
                                                <div class="form-group col-md-6">
                                                    <label for="nama">Nama :</label>
                                                    <div class="col-12">
                                                        <input type="text" class="form-control form-control-solid"
                                                               placeholder="Nama Pegawai"
                                                               name="nama"
                                                               value="" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="nip">NIP :</label>
                                                    <div class="col-12">
                                                        <input type="text" class="form-control form-control-solid"
                                                               placeholder="NIP Panjang"
                                                               name="nip"
                                                               value="" readonly>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row mb-3">
                                                <div class="form-group col-md-6">
                                                    <label for="jabatan">Jabatan :</label>
                                                    <div class="col-12">
                                                        <input type="text" class="form-control form-control-solid"
                                                               placeholder="Jabatan"
                                                               name="jabatan"
                                                               value="" readonly>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label for="unitorganisasi">Unit Organisasi :</label>
                                                    <div class="col-12">
                                                        <input type="text" class="form-control form-control-solid"
                                                               placeholder="Unit Organisasi"
                                                               name="unitorganisasi"
                                                               value="" readonly>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row mb-3">
                                                <div class="form-group col-md-6">
                                                    <label for="pangkat">Pangkat :</label>
                                                    <div class="col-12 mb-2">
                                                        <input type="text" class="form-control form-control-solid"
                                                               placeholder="Pangkat"
                                                               name="pangkat"
                                                               value="" readonly>
                                                    </div>

                                                    <label for="masapersiapanpensiun">Akhir Bulan :</label>
                                                    <div class="col-12">

                                                        <div class="input-group">
                                                            <div
                                                                class="position-relative d-flex align-items-center col-md-12">
                                                                <div
                                                                    class="symbol symbol-20px me-4 position-absolute ms-4">
                                                                        <span class="symbol-label bg-secondary">
                                                                            <span class="svg-icon">
                                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                     width="24px" height="24px"
                                                                                     viewBox="0 0 24 24"
                                                                                     version="1.1">
                                                                                    <g stroke="none" stroke-width="1"
                                                                                       fill="none"
                                                                                       fill-rule="evenodd">
                                                                                        <rect x="0" y="0" width="24"
                                                                                              height="24">
                                                                                        </rect>
                                                                                        <rect fill="#000000"
                                                                                              opacity="0.3" x="4"
                                                                                              y="4" width="4" height="4"
                                                                                              rx="1">
                                                                                        </rect>
                                                                                        <path
                                                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                                            fill="#000000"></path>
                                                                                    </g>
                                                                                </svg>
                                                                            </span>
                                                                        </span>
                                                                </div>
                                                                <input class="form-control ps-12 flatpickr-input"
                                                                       id="datepicker"
                                                                       placeholder="Tahun - Bulan" name="akhirbulan"
                                                                       type="text"
                                                                       value="" required="">
                                                            </div>
                                                            <div class="col-md-12">
                                                                    <span class="form-text text-muted">*Tanggal akan otomatis akhir
                                                                        bulan</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="alamat">Alamat :</label>
                                                    <div class="col-12">
                                                            <textarea type="text"
                                                                      class="form-control form-control-solid h-120px"
                                                                      placeholder="Alamat Pegawai" name="alamat"
                                                                      readonly rows="4"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <div class="col-md-12">
                                                    <label for="jenispensiun">Jenis Pensiun :</label>
                                                    <div class="col-12">
                                                        <select class="form-select" name="jenispensiun" id="selectJenis"
                                                                required>
                                                            <option value="">--Pilih Jenis--</option>
                                                            @foreach($jenissPP as $key => $value)
                                                                <option
                                                                    value="{{$value->idJnsPensiun}}">{{$value->nama}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-3">

                                                <div class="col-md-12">
                                                    <label for="alasan">Alasan :</label>
                                                    <div class="col-12">
                                                            <textarea type="text" class="form-control h-120px"
                                                                      placeholder="Alasan"
                                                                      name="alasan" required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="card block">
                                <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                                    <div class="row">
                                        <div class="col-12">
                                            <td class="ps-4 text-dark  mb-1 fs-6">
                                                <h3>Upload Berkas Kelengkapan</h3>
                                            </td>
                                        </div>
                                        <div class="my-5">
                                            <table id="routeTable"
                                                   class="table table-rounded fs-5 table-row-bordered border gy-4 gs-4">
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-center">
                            <a href="{{ url('/pengusulan-pensiun/permohonan/upk') }}"
                               class="btn btn-sm btn-secondary my-1"><i class="fa fa-reply"></i> Kembali</a>
                            <button type="submit" class="btn btn-sm btn-success my-1">
                                <i class="fa fa-save"></i> Simpan
                            </button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>

@endsection

@section('js')
    <script src="{{ url('assets') }}/js/pensiun/pensiunpengusulan/pp.js" type="text/javascript"></script>
    <script type="text/javascript">
        let app_url = "{{env('APP_URL')}}";
    </script>
@endsection
