@extends('layout.master')

@section('content')
    @include('sweetalert::alert')
    <div class="post d-flex flex-column" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12" id="loadingtarget">
                <div class="card mb-3 border-warning shadow-sm">
                    <div id="card-header" class="card-header">
                        <div class="card-title fw-bold fs-4 text-white">
                            Detil Penilitian Permohonan Pensiun
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="card">
                            <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                                <div class="p-2">
                                    <div class="row mb-3">
                                        <div class="form-group col-md-6">
                                            <label for="nama">Nama :</label>
                                            <div class="col-12">
                                                <input type="text" class="form-control" placeholder="Nama Pegawai"
                                                       name="nama" value="{{ $dataPP -> nama }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="nip">NIP :</label>
                                            <div class="col-12">
                                                <input type="text" class="form-control" placeholder="NIP Panjang"
                                                       name="nip"
                                                       value="{{ $dataPP->nip }}" disabled>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row mb-3">
                                        <div class="form-group col-md-6">
                                            <label for="jabatan">Jabatan :</label>
                                            <div class="col-12">
                                                <input type="text" class="form-control" placeholder="Jabatan"
                                                       name="jabatan"
                                                       value="{{ $dataPP->jabatan }}" disabled>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="unitorganisasi">Unit Organisasi :</label>
                                            <div class="col-12">
                                                <input type="text" class="form-control" placeholder="Unit Organisasi"
                                                       name="unitorganisasi" value="{{ $dataPP->unitOrg }}" disabled>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row mb-3">
                                        <div class="form-group col-md-6">
                                            <label for="pangkat">Pangkat :</label>
                                            <div class="col-12 mb-2">
                                                <input type="text" class="form-control" placeholder="Pangkat"
                                                       name="pangkat"
                                                       value="{{ $dataPP->pangkat }}" disabled>
                                            </div>

                                            <label for="masapersiapanpensiun">Akhir Bulan :</label>
                                            <div class="col-12">

                                                <div class="input-group">
                                                    <div class="position-relative d-flex align-items-center col-md-12">
                                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                            <span class="symbol-label bg-secondary">
                                                                <span class="svg-icon">
                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                         width="24px" height="24px" viewBox="0 0 24 24"
                                                                         version="1.1">
                                                                        <g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24">
                                                                            </rect>
                                                                            <rect fill="#000000" opacity="0.3" x="4"
                                                                                  y="4"
                                                                                  width="4" height="4" rx="1">
                                                                            </rect>
                                                                            <path
                                                                                d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                                fill="#000000"></path>
                                                                        </g>
                                                                    </svg>
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <input class="form-control ps-12 flatpickr-input"
                                                               placeholder="Tahun - Bulan" name="akhirbulan" type="text"
                                                               value="{{ $dataPP->akBulan }}" required="" disabled>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <span class="form-text text-muted">*Tanggal akan otomatis akhir
                                                            bulan</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="alamat">Alamat :</label>
                                            <div class="col-12">
                                                <textarea type="text" class="form-control h-120px"
                                                          placeholder="Alamat Pegawai" name="alamat" rows="4"
                                                          disabled>{{ $dataPP->alamat }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-md-12">
                                            <label for="jenispensiun">Jenis Pensiun :</label>
                                            <input type="text" class="form-control"
                                                   value="{{$dataPP->idJnsPensiun->nama}}" disabled>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-md-12">
                                            <label for="alamat">Alasan :</label>
                                            <div class="col-12">
                                                <textarea type="text" class="form-control h-120px" placeholder="Alasan"
                                                          name="alasan" disabled>{{ $dataPP->alasan }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="accordion mb-5" id="kt_accordion_1">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="kt_accordion_1_header_1">
                                    <button class="accordion-button bg-secondary fs-4 fw-bold" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#kt_accordion_1_body_1"
                                            aria-expanded="true" aria-controls="kt_accordion_1_body_1">
                                        Berkas Kelengkapan
                                    </button>
                                </h2>
                                <div id="kt_accordion_1_body_1" class="accordion-collapse collapse show"
                                     aria-labelledby="kt_accordion_1_header_1" data-bs-parent="#kt_accordion_1">
                                    <div class="accordion-body">
                                        <!--begin::Stepper-->
                                        <div class="stepper stepper-pills" id="kt_stepper_example_basic">
                                            <!--begin::Nav-->
                                            <div class="mb-5">
                                                <!--begin::Step 1-->
                                                <div class="flex-column current" data-kt-stepper-element="content">
                                                    <table
                                                        class="table table-rounded fs-5 table-row-bordered border gy-4 gs-4">
                                                        <thead class="fw-bolder bg-secondary">
                                                        <tr
                                                            class="fw-bold text-gray-800 border-bottom-2 border-gray-200 align-middle">
                                                            <th width="60%">Keterangan</th>
                                                            <th width="20%">File</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($dokumensPP as $key => $items)
                                                            <tr>
                                                                <td class="align-middle"><label for="sp_berhenti_pns">
                                                                        {{ $items['namaDok'] }}</label></td>
                                                                <td>
                                                                    <!--Hasil file Upload-->
                                                                    <div>
                                                                        <a type="button"
                                                                           class="imageshow download_sp_{{ $items['idJnsDok'] }} ps-4 detilModal"
                                                                           data-url="{{ url('/pengusulan-pensiun/file/'.AppHelper::instance()->enkrip($items['namaFile'])) }}"
                                                                           data-file="{{ $items['namaDok'] }}"
                                                                           data-bs-toggle="modal"
                                                                           data-bs-target="#viewPDF">
                                                                            <img
                                                                                src="{{ url('assets') }}/src/media/svg/files/pdf.svg"
                                                                                style="max-height: 40px; max-width: 40px;"
                                                                                alt=""></a>
                                                                    </div>
                                                                    <span id="sp_{{ $items['idJnsDok'] }}_text"
                                                                          class="form-text text-muted fw-bold text-muted d-block fs-7 sp_{{ $items['idJnsDok'] }}_text">{{ $items['namaFile'] }}
                                                                            </span>
                                                                    <!-- EndHasil file Upload-->
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!--end::Group-->
                                        </div>
                                        <!--end::Stepper-->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                                <div class="p-2">
                                    <div class="row">
                                        <div class="col-3 fw-bold fs-3">
                                            Konsep Surat Persetujuan Permohonan Pensiun
                                        </div>
                                        <div class="col-6 ps-4 text-dark  mb-1 fs-6">
                                            <!--begin::Trigger-->
                                            <button type="button" class="btn btn-sm btn-primary rotate"
                                                    data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start"
                                                    data-kt-menu-flip="top-start">
                                                Generate
                                            </button>
                                            <!--end::Trigger-->

                                            <!--begin::Menu-->
                                            <div
                                                class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-200px py-4"
                                                data-kt-menu="true">
                                                <!--begin::Menu item-->
                                                <div class="menu-item px-3">
                                                    <a id="reloadTolak"
                                                       data-url="/pengusulan-pensiun/penelitian/tolak/'{{ AppHelper::instance()->enkrip($dataPP->idPp) }}"
                                                       class="menu-link tolak px-3">
                                                        Tolak
                                                    </a>
                                                </div>
                                                <!--end::Menu item-->


                                                <!--begin::Menu item-->
                                                <div class="menu-item px-3">
                                                    <a id="reloadTangguhkan"
                                                       data-url="/pengusulan-pensiun/penelitian/tangguhkan/'{{ AppHelper::instance()->enkrip($dataPP->idPp) }}"
                                                       class="menu-link tangguhkan px-3">
                                                        Tangguhkan
                                                    </a>
                                                </div>
                                                <!--end::Menu item-->

                                                <!--begin::Menu item-->
                                                <div class="menu-item px-3">
                                                    <a id="reloadSetuju"
                                                       data-url="/pengusulan-pensiun/penelitian/setujui/'{{ AppHelper::instance()->enkrip($dataPP->idPp) }}"
                                                       class="menu-link setujui px-3">
                                                        Setujui
                                                    </a>
                                                </div>
                                                <!--end::Menu item-->
                                            </div>
                                            <!--end::Menu-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--begin::Modal-->
                    <div class="modal fade" id="viewPDF" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header card-header">
                                    <h3 class="modal-title text-white namafile" id="exampleModalLabel"></h3>
                                </div>
                                <div class="modal-body embed-responsive">
                                </div>
                                <div class="text-center mb-8">
                                    <button type="button" class="btn btn-sm btn-secondary my-1"
                                            data-bs-dismiss="modal"><i class="fa fa-times"></i> Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end modal -->

                    <div class="card-footer">
                        <div class="text-center">
                            <a href="{{ url('/pengusulan-pensiun/penelitian') }}"
                               class="btn btn-sm btn-secondary my-1"><i class="fa fa-reply"></i> Kembali</a>
                            {{-- @if($dataPP->idJnsPensiun->idJnsPensiun == 2)
                            <button type="submit" class="btn btn-sm btn-success"><i
                                    class="fa fa-check"></i> Simpan Draft</button>
                            @endif --}}
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
@endsection
@section('js')
    <script src="{{ url('assets') }}/js/pensiun/pensiunpengusulan/pp.js" type="text/javascript"></script>
@endsection
