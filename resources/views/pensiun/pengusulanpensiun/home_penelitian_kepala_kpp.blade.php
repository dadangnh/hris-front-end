@extends('layout.master')

@section('content')
    <div class="block">
        <div class="post d-flex flex-column" id="kt_post">
            <!--begin::Container-->
            <div id="kt_content_container" class="container-fluid">
                <div class="col-lg-12">
                    <div class="card mb-3 border-warning shadow-sm">
                        <div id="card-header" class="card-header">
                            <div class="card-title fw-bold fs-2 text-white">
                                Daftar Pengusulan Permohonan Pensiun
                            </div>
                        </div>
                        {{-- End Card Header --}}
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6 mb-15">
                                    <div class="form-group form-sm float-right col-4 inner-addon left-addon">
                                        <!--begin::Svg Icon | path: icons/duotone/General/Search.svg-->
                                        <span
                                            class="svg-icon svg-icon-3 svg-icon-gray-500 position-absolute top-50 translate-middle ms-6">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <path
                                                    d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z"
                                                    fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                <path
                                                    d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z"
                                                    fill="#000000" fill-rule="nonzero"></path>
                                            </g>
                                        </svg>
                                    </span>
                                        <input type="text" id="searchData"
                                               class="form-control form-control-lg form-control-solid px-15"
                                               placeholder="filter">
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive bg-gray-400 rounded shadow-sm">
                                <!--begin::Table-->
                                <table class="table table-striped align-middle">
                                    <!--begin::Thead-->
                                    <thead class="fw-bolder bg-secondary fs-6 align-middle">
                                    <tr class="text-center">
                                        <th class="ps-4">No</th>
                                        <th>No Permohonan <span class="text-muted fw-bold text-muted d-block fs-7">Tanggal
                                                Permohonan</span></th>
                                        <th>Nama <span class="text-muted fw-bold text-muted d-block fs-7">NIP</span>
                                        </th>
                                        <th class="pe-4">Aksi</th>
                                    </tr>
                                    </thead>
                                    <!--end::Thead-->
                                    <!--begin::Tbody-->

                                    <tbody id="dataPermohonan">
                                    @if($totalItems != '0')
                                        @foreach($dataPPS as $key => $value)
                                            <tr class="text-center">
                                                <td class="ps-4 text-dark  mb-1 fs-6">
                                                    {{ $key+1 }}
                                                </td>
                                                <td class="text-dark  mb-1 fs-6">
                                                    {{ $value->notiket }}
                                                    <span
                                                        class="text-muted fw-bold text-muted d-block fs-7">{{ AppHelper::instance()->indonesian_date(strtotime($value->wktCreatePensiun),'j F Y','') }}</span>
                                                </td>
                                                <td class="text-dark  mb-1 fs-6">
                                                    {{ $value->nama }}
                                                    <span
                                                        class="text-muted fw-bold text-muted d-block fs-7">{{ $value->nip }}</span>
                                                </td>
                                                <td class="text-dark mb-1 fs-6 pe-4">
                                                    <span data-bs-toggle="tooltip" data-bs-placement="top"
                                                          title="Kelola Permohonan">
                                                        <a href="{{ url('/pengusulan-pensiun/penelitian/detil/'.AppHelper::instance()->enkrip($value->idPp)) }}"
                                                           type="button"
                                                           class="btn btn-primary btn-bold btn-icon btn-sm">
                                                            <span class="fas fa-tasks text-white"
                                                                  aria-hidden="true"></span>
                                                        </a>
                                                    </span>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr class="text-center">
                                            <td class="text-dark  mb-1 fs-6" colspan="8">Tidak terdapat data</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                    <!--end::Tbody-->
                                </table>
                                <!--end::Table-->

                                <!--begin::pagination-->
                                <div class="d-flex flex-stack flex-wrap pt-5 p-3">
                                    <div class="fs-6 fw-bold text-gray-700"></div>
                                    @php
                                        $disabled = '';
                                        if($currentPage==1){
                                        $disabled = 'disabled';
                                        }

                                        $nextPage= '';
                                        if($currentPage != $totalPages){
                                        $nextPage = $currentPage+1;
                                        }

                                        $disablednext = '';
                                        $hidden = '';
                                        if($currentPage==$totalPages){
                                        $disablednext = 'disabled';
                                        $hidden='hidden';
                                        }

                                        $hiddenpagination ='';
                                        if($totalItems == 0){
                                            $hiddenpagination = "none";
                                            $hidden='hidden';
                                        }
                                    @endphp
                                        <!--begin::Pages-->
                                    <ul class="pagination" style="display: {{ $hiddenpagination }}">
                                        <li class="page-item disabled"><a href="#"
                                                                          class="page-link">Halaman {{ $currentPage }}
                                                dari
                                                {{ $totalPages }}</a></li>
                                        <li class="page-item previous {{ $disabled }}"><a
                                                href="{{ url('/pengusulan-pensiun/penelitian?page='.($currentPage-1)) }}"
                                                class="page-link"><i class="previous"></i></a></li>
                                        <li class="page-item active"><a
                                                href="{{ url('/pengusulan-pensiun/penelitian?page='.$currentPage) }}"
                                                class="page-link">{{ $currentPage }}</a></li>
                                        <li class="page-item" {{ $hidden }}><a
                                                href="{{ url('/pengusulan-pensiun/penelitian?page='.$nextPage) }}"
                                                class="page-link">{{ $nextPage }}</a></li>
                                        <li class="page-item next {{ $disablednext }}"><a
                                                href="{{ url('/pengusulan-pensiun/penelitian?page='.$nextPage) }}"
                                                class="page-link"><i class="next"></i></a></li>
                                    </ul>
                                    <!--end::Pages-->
                                </div>
                                <!--end::paging-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ url('assets') }}/js/pensiun/pensiunpengusulan/pp.js" type="text/javascript"></script>
@endsection
