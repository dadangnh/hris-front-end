@extends('layout.master')

@section('content')
    <div id="block">
        <div class="post d-flex flex-column" id="kt_post">
            <!--begin::Container-->
            <div id="kt_content_container" class="container-fluid">
                <div class="col-lg-12">
                    <div class="card mb-3 border-warning shadow-sm">
                        <div id="card-header" class="card-header">
                            <div class="card-title fw-bold fs-2 text-white">
                                Daftar Review Masa Persiapan Pensiun
                            </div>
                        </div>
                        {{-- End Card Header --}}
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6 mb-15">
                                    <div class="form-group form-sm float-right col-4 inner-addon left-addon">
                                        <!--begin::Svg Icon | path: icons/duotone/General/Search.svg-->
                                        <span
                                            class="svg-icon svg-icon-3 svg-icon-gray-500 position-absolute top-50 translate-middle ms-6">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path
                                                        d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z"
                                                        fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                    <path
                                                        d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z"
                                                        fill="#000000" fill-rule="nonzero"></path>
                                                </g>
                                            </svg>
                                        </span>
                                        <input type="text" id="searchData"
                                               class="form-control form-control-lg form-control-solid px-15"
                                               placeholder="filter">

                                    </div>
                                </div>

                            </div>
                            <div class="table-responsive bg-gray-400 rounded shadow-sm">
                                <!--begin::Table-->
                                <table class="table table-striped align-middle">
                                    <!--begin::Thead-->
                                    <thead class="fw-bolder bg-secondary fs-6 align-middle">
                                    <tr class="text-center">
                                        <th class="ps-4">No</th>
                                        <th>No Permohonan / Tgl Permohonan</th>
                                        <th>Nama / NIP</th>
                                        <th>Unit</th>
                                        <th>Status</th>
                                        <th class="pe-4">Aksi</th>
                                    </tr>
                                    </thead>
                                    <!--end::Thead-->
                                    <!--begin::Tbody-->
                                    <tbody id="dataPermohonan">
                                    @if ($totalItems != '0')
                                        @foreach($dataMpp as $key => $value)
                                            <tr class="text-center">
                                                <td class="ps-4 text-dark  mb-1 fs-6">
                                                    {{ (($page - 1) * 10) + 1 + $key }}
                                                </td>
                                                <td class="text-dark  mb-1 fs-6">
                                                    {{$value->idMpp->notiket}}
                                                    <span
                                                        class="text-muted fw-bold text-muted d-block fs-7">{{AppHelper::instance()->indonesian_date(strtotime($value->idMpp->wktCreateMohon),'j F Y','')}}</span>
                                                </td>
                                                <td class="text-dark  mb-1 fs-6">
                                                    {{$value->idMpp->nama}}
                                                    <span
                                                        class="text-muted fw-bold text-muted d-block fs-7">{{$value->idMpp->nip}}</span>
                                                </td>
                                                <td class="text-dark  mb-1 fs-6">
                                                    {{$value->idMpp->namaKantor}}
                                                </td>
                                                <td class="text-dark  mb-1 fs-6">
                                                    @if($value->idMpp->status->urutanCase==2)
                                                        <span
                                                            class="badge badge-light-warning fs-7 fw-bolder">{{$value->idMpp->status->uraianStatus}}</span>
                                                    @elseif($value->idMpp->status->urutanCase==0)
                                                        <span
                                                            class="badge badge-light-danger fs-7 fw-bolder">{{$value->idMpp->status->uraianStatus}}</span>
                                                    @elseif($value->idMpp->status->urutanCase==10)
                                                        <span
                                                            class="badge badge-light-success fs-7 fw-bolder">{{$value->idMpp->status->uraianStatus}}</span>
                                                    @elseif($value->idMpp->status->urutanCase==11)
                                                        <span
                                                            class="badge badge-light-info fs-7 fw-bolder">{{$value->idMpp->status->uraianStatus}}</span>
                                                    @elseif($value->idMpp->status->urutanCase==12)
                                                        <span
                                                            class="badge badge-light-warning fs-7 fw-bolder">{{$value->idMpp->status->uraianStatus}}</span>
                                                    @else
                                                        <span
                                                            class="badge badge-light-primary fs-7 fw-bolder">{{$value->idMpp->status->uraianStatus}}</span>
                                                    @endif
                                                </td>
                                                <td class="text-dark mb-1 fs-6 pe-4">
                                                    @if($value->idMpp->status->urutanCase==8 && $value->idMpp->fileSrtKonfirmasi ==null || $value->idMpp->fileSrtPermintaan == null)
                                                        <span data-bs-toggle="tooltip" data-bs-placement="top"
                                                              title="Kelola Penelitian">
                                                    <a href="{{url('/permohonan-masa-pensiun/review/disposisi/'.AppHelper::instance()->enkrip($value->idMpp->idMpp))}}"
                                                       type="button" class="btn btn-bold btn-icon btn-sm">
                                                        <span class="fas fa-cog text-primary"></span>
                                                    </a>
                                                </span>
                                                    @elseif($value->idMpp->status->urutanCase==8 && $value->idMpp->fileSrtKonfirmasi !=null && $value->idMpp->fileSrtPermintaan != null)
                                                        <span data-bs-toggle="tooltip" data-bs-placement="top"
                                                              title="Kelola Konfirmasi Hukdis">
                                                    <a href="{{url('/permohonan-masa-pensiun/review/disposisi/'.AppHelper::instance()->enkrip($value->idMpp->idMpp))}}"
                                                       type="button" class="btn btn-bold btn-icon btn-sm">
                                                        <span class="fas fa-cog text-primary"></span>
                                                    </a>
                                                </span>
                                                        <span data-bs-toggle="tooltip" data-bs-placement="top"
                                                              title="Simpan Penelitian">
                                                    <button
                                                        data-url="{{url('/permohonan-masa-pensiun/review/disposisi/penelitian/simpan/'.AppHelper::instance()->enkrip($value->idMpp->idMpp))}}"
                                                        type="button"
                                                        class="btn btn-bold btn-icon btn-sm btnSimpanFinal">
                                                        <span class="fas fa-save text-success fs-4"></span>
                                                    </button>
                                                </span>
                                                    @elseif($value->idMpp->status->urutanCase==9)
                                                        <span data-bs-toggle="tooltip" data-bs-placement="top"
                                                              title="Kelola Konsep SK">
                                                    <a href="{{url('/permohonan-masa-pensiun/review/konsep-sk/'.AppHelper::instance()->enkrip($value->idMpp->idMpp))}}"
                                                       type="button" class="btn btn-bold btn-icon btn-sm">
                                                        <span class="fas fa-cog text-primary"></span>
                                                    </a>
                                                </span>
                                                        <span data-bs-toggle="tooltip" data-bs-placement="top"
                                                              title="Kirim dan Attach File SK">
                                                    <button type="button" class="btn btn-sm btn-icon modalAttach"
                                                            data-mpp="{{ AppHelper::instance()->enkrip($value->idMpp->idMpp) }}"
                                                            data-tiket="{{ $value->idMpp->notiket }}"
                                                            data-nip="{{ $value->idMpp->nip }}"
                                                            data-nmpeg="{{ $value->idMpp->nama }}"
                                                            data-bs-toggle="modal" data-bs-target="#uploadSK">
                                                        <span class="fas fa-paperclip text-success"></span>
                                                    </button>
                                                </span>
                                                    @else

                                                    @endif

                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr class="text-center">
                                            <td class="text-dark  mb-1 fs-6" colspan="8">Tidak terdapat data</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                    <!--end::Tbody-->
                                </table>
                                <!--end::Table-->

                                <!--begin::pagination-->
                                <div class="d-flex flex-stack flex-wrap pt-5 p-3">
                                    <div class="fs-6 fw-bold text-gray-700"></div>
                                    @php
                                        $disabled = '';
                                        if($currentPage==1){
                                        $disabled = 'disabled';
                                        }

                                        $nextPage= '';
                                        if($currentPage != $totalPages){
                                        $nextPage = $currentPage+1;
                                        }

                                        $disablednext = '';
                                        $hidden = '';
                                        if($currentPage==$totalPages){
                                        $disablednext = 'disabled';
                                        $hidden='hidden';
                                        }

                                        $hiddenpagination ='';
                                        if($totalItems == 0){
                                            $hiddenpagination = "none";
                                            $hidden='hidden';
                                        }

                                    @endphp
                                        <!--begin::Pages-->
                                    <ul class="pagination" style="display: {{$hiddenpagination}}">
                                        <li class="page-item disabled"><a href="#"
                                                                          class="page-link">Halaman {{ $currentPage }}
                                                dari {{ $totalPages }}</a></li>
                                        <li class="page-item previous {{ $disabled }}"><a
                                                href="{{ url('/permohonan-masa-pensiun/review?page='.($currentPage-1)) }}"
                                                class="page-link"><i class="previous"></i></a></li>
                                        <li class="page-item active"><a
                                                href="{{ url('/permohonan-masa-pensiun/review?page='.$currentPage) }}"
                                                class="page-link">{{ $currentPage }}</a></li>
                                        <li class="page-item" {{ $hidden }}><a
                                                href="{{ url('/permohonan-masa-pensiun/review?page='.$nextPage) }}"
                                                class="page-link">{{ $nextPage }}</a></li>
                                        <li class="page-item next {{ $disablednext }}"><a
                                                href="{{ url('/permohonan-masa-pensiun/review?page='.$nextPage) }}"
                                                class="page-link"><i class="next"></i></a></li>
                                    </ul>
                                    <!--end::Pages-->
                                </div>
                                <!--end::paging-->

                                <!--begin::Modal-->
                                <div class="modal fade" id="uploadSK" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-md" role="document">
                                        <form id="kirimSK" novalidate
                                              data-url="{{url('/permohonan-masa-pensiun/review/upload-sk-mpp')}}"
                                              action="" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="modal-content">
                                                <div class="modal-header card-header">
                                                    <h3 class="modal-title text-white" id="exampleModalLabel">Attachment
                                                        File SK MPP</h3>
                                                </div>
                                                <div id="blockUploadSK">
                                                    <div class="modal-body">
                                                        <div class="d-flex flex-column mb-8 fv-row">
                                                            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                                                <span>Nomor Tiket</span>
                                                            </label>
                                                            <input type="hidden" id='idMpp' name="idMpp">
                                                            <input type="text" class="form-control form-control-solid"
                                                                   placeholder="Enter Target Title" name="tiket"
                                                                   id="tiket" disabled/>
                                                        </div>
                                                        <div class="row g-9 mb-8">
                                                            <div class="col-md-6 fv-row">
                                                                <label
                                                                    class="d-flex align-items-center fs-6 fw-bold mb-2">
                                                                    <span>Nama Pegawai</span>
                                                                </label>
                                                                <input type="text"
                                                                       class="form-control form-control-solid"
                                                                       placeholder="Enter Target Title"
                                                                       name="namapegawai" id="namapegawai" disabled/>
                                                            </div>
                                                            <div class="col-md-6 fv-row">
                                                                <label
                                                                    class="d-flex align-items-center fs-6 fw-bold mb-2">
                                                                    <span>NIP</span>
                                                                </label>
                                                                <input type="text"
                                                                       class="form-control form-control-solid"
                                                                       placeholder="Enter Target Title" name="nip"
                                                                       id="nip" disabled/>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex flex-column mb-8 fv-row">
                                                            <label class="required fs-6 fw-bold mb-2">Hasil
                                                                Penelitian</label>
                                                            <select class="form-select form-select-solid"
                                                                    data-control="select2" data-hide-search="true"
                                                                    data-placeholder="Pilih ..." name="hasilpenelitian">
                                                                <option value="">Pilih ...</option>
                                                                <option class="tolak" value="1">Tolak</option>
                                                                <option class="setujui" value="2">Setuju</option>
                                                                <option class="tangguhkan" value="3">Tangguhkan</option>
                                                            </select>
                                                        </div>
                                                        <div class="d-flex flex-column mb-8 fv-row">
                                                            <label for="uploadskMPP"
                                                                   class="btn btn-sm btn-primary me-2"><span
                                                                    class="fa fa-upload"></span> Upload File SK
                                                                MPP</label>
                                                            <span class="form-text text-muted">Max file size is 2MB and PDF filetype.</span>

                                                            <input type="file" class="form-control" name="skMPP"
                                                                   id="uploadskMPP" style="display: none;" required>
                                                            <div class="col-6">
                                                                <label for="skMPP" id="doneuploadskMPP"></label>
                                                                <a type="button" class="hapusdoneuploadskMPP"
                                                                   style="display:none;"
                                                                   data-bs-toggle="tooltip" data-bs-placement="top"
                                                                   title="Hapus"><i
                                                                        class="fa fa-trash text-secondary"></i></a>
                                                            </div>
                                                            <span id="skMPP_text"
                                                                  class="form-text text-muted fw-bold text-muted d-block fs-7">
                                                                </span>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer text-center mb-8">
                                                        <button type="button" class="btn btn-sm btn-secondary my-1"
                                                                data-bs-dismiss="modal"><i
                                                                class="fa fa-reply"></i> batal
                                                        </button>
                                                        <button type="submit"
                                                                class="btn btn-sm btn-primary my-1 loadingBtn">
                                                            <i class="fa fa-paper-plane"></i> Kirim
                                                        </button>
                                                        <button class="btn btn-primary btn-sm" type="button" hidden
                                                                disabled>
                                                            Loading
                                                            <span class="spinner-border spinner-border-sm" role="status"
                                                                  aria-hidden="true"></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- end modal -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ url('assets') }}/js/pensiun/pensiunmpp/permohonanmpp.js" type="text/javascript"></script>
@endsection
