@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card mb-3 border-warning shadow-sm">
                    <div id="card-header" class="card-header">
                        <div class="card-title fw-bold fs-4 text-white">
                            Edit Permohonan Masa Persiapan Pensiun
                        </div>
                    </div>

                    <form action="{{ url('/permohonan-masa-pensiun/update/simpan') }}"
                          method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="card">
                                <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                                    <div class="p-2">
                                        <div class="row g-9 mb-6">
                                            <input type="hidden" name="idMpp"
                                                   value="{{ AppHelper::instance()->enkrip($dataMpp->idMpp) }}"/>
                                            <div class="col-md-6 fv-row">
                                                <label><span>Nama Pegawai</span></label>
                                                <input type="text" class="form-control form-control-solid"
                                                       placeholder="Nama Pegawai" name="nama"
                                                       value="{{ $dataMpp->nama }}"
                                                       disabled/>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label fs-6 fw-bold mb-2">NIP</label>
                                                <input type="text" class="form-control form-control-solid"
                                                       placeholder="Nama Pegawai" name="nip" value="{{ $dataMpp->nip }}"
                                                       disabled/>
                                            </div>
                                        </div>
                                        <div class="row g-9 mb-6">
                                            <div class="col-md-6 fv-row">
                                                <label fs-6 fw-bold mb-2">Pangkat</label>
                                                <input type="text" class="form-control form-control-solid"
                                                       placeholder="Nama Pegawai" name="pangkat"
                                                       value="{{ $dataMpp->pangkat }}" disabled/>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label fs-6 fw-bold mb-2">Jabatan</label>
                                                <input type="text" class="form-control form-control-solid"
                                                       placeholder="Nama Pegawai" name="jabatan"
                                                       value="{{ $dataMpp->jabatan }}" disabled/>
                                            </div>
                                        </div>
                                        <div class="row g-9 mb-6">
                                            <div class="col-md-6 fv-row">
                                                <label fs-6 fw-bold mb-2">Unit Organisasi</label>
                                                <input type="text" class="form-control form-control-solid"
                                                       placeholder="Nama Pegawai" name="unitorganisasi"
                                                       value="{{ $dataMpp->unitOrg }}" disabled/>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label fs-6 fw-bold mb-2">BUP Pensiun</label>
                                                <input type="text" class="form-control form-control-solid"
                                                       placeholder="BUP Pensiun" name="buppensiun"
                                                       value="{{ $dataMpp->bup }}" required="required" disabled>
                                            </div>
                                        </div>
                                        <div class="d-flex flex-column mb-6 fv-row">
                                            <label><span>Alamat</span></label>
                                            <input type="text" class="form-control form-control-solid"
                                                   placeholder="Alamat Pegawai" name="alamat" disabled
                                                   value="{{ $dataMpp->alamat }}"/>
                                        </div>
                                        <div class="row g-9 mb-6">
                                            <div class="col-md-6 fv-row">
                                                <label fs-6 fw-bold mb-2">SK CPNS</label>
                                                <input type="text" class="form-control form-control-solid"
                                                       placeholder="Tanggal Lahir" name="skcpns"
                                                       value="" disabled/>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label fs-6 fw-bold mb-2">SK Pangkat</label>
                                                <input type="text" class="form-control form-control-solid"
                                                       placeholder="SK Pangkat" name="skpangkat"
                                                       value="{{session()->get('user')['pangkat'] }}"
                                                       required="required" disabled>
                                            </div>
                                        </div>
                                        <div class="row g-9 mb-6">
                                            <div class="col-md-6 fv-row">
                                                <label class="required fs-6 fw-bold mb-2">Masa Persiapan Pensiun</label>
                                                <div class="input-group">
                                                    <div class="position-relative d-flex align-items-center col-5">
                                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                        <span class="symbol-label bg-secondary">
                                                            <span class="svg-icon">
                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                     width="24px" height="24px" viewBox="0 0 24 24"
                                                                     version="1.1">
                                                                    <g stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd">
                                                                        <rect x="0" y="0" width="24" height="24">
                                                                        </rect>
                                                                        <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                              width="4" height="4" rx="1">
                                                                        </rect>
                                                                        <path
                                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                            fill="#000000"></path>
                                                                    </g>
                                                                </svg>
                                                            </span>
                                                        </span>
                                                        </div>
                                                        <input class="form-control ps-12 flatpickr-input masaawal"
                                                               id="datemonth" placeholder="Tahun - Bulan"
                                                               name="masaawal"
                                                               type="text" value="{{ $masaAwal }}">
                                                    </div>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text text-gray-500">s.d.</span>
                                                    </div>
                                                    <div class="position-relative d-flex align-items-center col-6">
                                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                        <span class="symbol-label bg-secondary">
                                                            <span class="svg-icon">
                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                     width="24px" height="24px" viewBox="0 0 24 24"
                                                                     version="1.1">
                                                                    <g stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd">
                                                                        <rect x="0" y="0" width="24" height="24">
                                                                        </rect>
                                                                        <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                              width="4" height="4" rx="1">
                                                                        </rect>
                                                                        <path
                                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                            fill="#000000"></path>
                                                                    </g>
                                                                </svg>
                                                            </span>
                                                        </span>
                                                        </div>
                                                        <input class="form-control ps-12 flatpickr-input"
                                                               id="datemonth1"
                                                               placeholder="Tahun - Bulan" name="masaakhir" type="text"
                                                               value="{{ $masaAkhir }}" required="" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label class="fs-6 fw-bold mb-2">Selisih Bulan</label>
                                                <div class="position-relative d-flex align-items-center col-6">
                                                    <input type="hidden" id="selisihbln"
                                                           placeholder="Selisih Jumlah Bulan"
                                                           name="selisihbln" value="{{ $dataMpp->selisihMpp }}">
                                                    <input class="form-control flatpickr-input" id="selisihBulan"
                                                           placeholder="Selisih Jumlah Bulan" name="selisihBulan"
                                                           disabled
                                                           value="{{ $dataMpp->selisihMpp }}">
                                                    <div class="symbol symbol-45px me-4 position-left ms-4">
                                                    <span class="symbol-label bg-white">
                                                        <span class="text" width="24px" height="24px"
                                                              viewBox="0 0 24 24">
                                                            Bulan
                                                        </span>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                                    <div class="p-2">
                                        <div class="row">
                                            <div class="col-12">
                                                <td class="ps-4 text-dark  mb-1 fs-6">
                                                    <h3>SURAT KETERANGAN</h3>
                                                </td>
                                            </div>
                                            <div class="my-5">
                                                <table
                                                    class="table table-rounded fs-5 table-row-bordered border gy-4 gs-4">
                                                    <thead class="fw-bolder bg-secondary">
                                                    <tr
                                                        class="fw-bold text-gray-800 border-bottom-2 border-gray-200 align-middle">
                                                        <th width="60%">Keterangan</th>
                                                        <th width="20%">Aksi</th>
                                                        <th width="20%" class="text-center">File</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td class="align-middle"><label for="skpelanggarandisiplin">a.
                                                                Surat Keterangan tidak dalam
                                                                proses pemeriksaan pelanggaran disiplin</label></td>
                                                        <td><label for="uploadskpelanggarandisiplin"
                                                                   class="btn btn-sm btn-primary me-2"><span
                                                                    class="svg-icon  svg-icon-1x">
                                                                    <!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo13/dist/../src/media/svg/icons/Files/Upload.svg--><svg
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        width="24px" height="24px" viewBox="0 0 24 24"
                                                                        version="1.1">
                                                                        <g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24"/>
                                                                            <path
                                                                                d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z"
                                                                                fill="#000000" fill-rule="nonzero"
                                                                                opacity="0.3"/>
                                                                            <rect fill="#000000" opacity="0.3" x="11"
                                                                                  y="2" width="2" height="14" rx="1"/>
                                                                            <path
                                                                                d="M12.0362375,3.37797611 L7.70710678,7.70710678 C7.31658249,8.09763107 6.68341751,8.09763107 6.29289322,7.70710678 C5.90236893,7.31658249 5.90236893,6.68341751 6.29289322,6.29289322 L11.2928932,1.29289322 C11.6689749,0.916811528 12.2736364,0.900910387 12.6689647,1.25670585 L17.6689647,5.75670585 C18.0794748,6.12616487 18.1127532,6.75845471 17.7432941,7.16896473 C17.3738351,7.57947475 16.7415453,7.61275317 16.3310353,7.24329415 L12.0362375,3.37797611 Z"
                                                                                fill="#000000" fill-rule="nonzero"/>
                                                                        </g>
                                                                    </svg>
                                                                    <!--end::Svg Icon--></span> Upload File</label>
                                                            <div class="col-12">
                                                                <input type="file" class="form-control"
                                                                       name="skpelanggarandisiplin"
                                                                       id="uploadskpelanggarandisiplin"
                                                                       style="display: none;">
                                                                <span class="form-text text-muted">Max file size is
                                                                    2MB.</span>
                                                            </div>
                                                        </td>
                                                        <td class="text-center">
                                                            <label for="skpelanggarandisiplin"
                                                                   id="doneuploadskpelanggarandisiplin"><a
                                                                    target="_blank"
                                                                    href="{{ url('/permohonan-masa-pensiun/file/'.AppHelper::instance()->enkrip($dataMpp->sketPeriksa)) }}"><span
                                                                        class="las la-file-pdf fs-3x"></span></a></label>
                                                            <span id="skpelanggarandisiplin_text"
                                                                  class="form-text text-muted fw-bold text-muted d-block fs-7">SKet
                                                                Periksa
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="align-middle"><label for="skperadilan">b. Surat
                                                                Keterangan tidak dalam proses
                                                                peradilan</label></td>
                                                        <td><label for="uploadskperadilan"
                                                                   class="btn btn-sm btn-primary me-2"><span
                                                                    class="svg-icon  svg-icon-1x">
                                                                    <!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo13/dist/../src/media/svg/icons/Files/Upload.svg--><svg
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        width="24px" height="24px" viewBox="0 0 24 24"
                                                                        version="1.1">
                                                                        <g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24"/>
                                                                            <path
                                                                                d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z"
                                                                                fill="#000000" fill-rule="nonzero"
                                                                                opacity="0.3"/>
                                                                            <rect fill="#000000" opacity="0.3" x="11"
                                                                                  y="2" width="2" height="14" rx="1"/>
                                                                            <path
                                                                                d="M12.0362375,3.37797611 L7.70710678,7.70710678 C7.31658249,8.09763107 6.68341751,8.09763107 6.29289322,7.70710678 C5.90236893,7.31658249 5.90236893,6.68341751 6.29289322,6.29289322 L11.2928932,1.29289322 C11.6689749,0.916811528 12.2736364,0.900910387 12.6689647,1.25670585 L17.6689647,5.75670585 C18.0794748,6.12616487 18.1127532,6.75845471 17.7432941,7.16896473 C17.3738351,7.57947475 16.7415453,7.61275317 16.3310353,7.24329415 L12.0362375,3.37797611 Z"
                                                                                fill="#000000" fill-rule="nonzero"/>
                                                                        </g>
                                                                    </svg>
                                                                    <!--end::Svg Icon--></span> Upload File</label>
                                                            <div class="col-12">
                                                                <input type="file" class="form-control"
                                                                       name="skperadilan" id="uploadskperadilan"
                                                                       style="display: none;">
                                                                <span class="form-text text-muted">Max file size is
                                                                    2MB.</span>
                                                            </div>

                                                        </td>
                                                        <td class="text-center">
                                                            <label for="skperadilan" id="doneuploadskperadilan"><a
                                                                    target="_blank"
                                                                    href="{{ url('/permohonan-masa-pensiun/file/'.AppHelper::instance()->enkrip($dataMpp->sketPengadilan)) }}"><span
                                                                        class="las la-file-pdf fs-3x"></span></a></label>
                                                            <span id="skperadilan_text"
                                                                  class="form-text text-muted fw-bold text-muted d-block fs-7">SKet
                                                                Peradilan
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="align-middle"><label for="skselesaipekerjaan">c.
                                                                Surat
                                                                Keterangan telah
                                                                menyelesaikan Pekerjaan</label></td>
                                                        <td><label for="uploadskselesaipekerjaan"
                                                                   class="btn btn-sm btn-primary me-2"><span
                                                                    class="svg-icon  svg-icon-1x">
                                                                    <!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo13/dist/../src/media/svg/icons/Files/Upload.svg--><svg
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        width="24px" height="24px" viewBox="0 0 24 24"
                                                                        version="1.1">
                                                                        <g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24"/>
                                                                            <path
                                                                                d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z"
                                                                                fill="#000000" fill-rule="nonzero"
                                                                                opacity="0.3"/>
                                                                            <rect fill="#000000" opacity="0.3" x="11"
                                                                                  y="2" width="2" height="14" rx="1"/>
                                                                            <path
                                                                                d="M12.0362375,3.37797611 L7.70710678,7.70710678 C7.31658249,8.09763107 6.68341751,8.09763107 6.29289322,7.70710678 C5.90236893,7.31658249 5.90236893,6.68341751 6.29289322,6.29289322 L11.2928932,1.29289322 C11.6689749,0.916811528 12.2736364,0.900910387 12.6689647,1.25670585 L17.6689647,5.75670585 C18.0794748,6.12616487 18.1127532,6.75845471 17.7432941,7.16896473 C17.3738351,7.57947475 16.7415453,7.61275317 16.3310353,7.24329415 L12.0362375,3.37797611 Z"
                                                                                fill="#000000" fill-rule="nonzero"/>
                                                                        </g>
                                                                    </svg>
                                                                    <!--end::Svg Icon--></span> Upload File</label>
                                                            <div class="col-12">
                                                                <input type="file" class="form-control"
                                                                       name="skselesaipekerjaan"
                                                                       id="uploadskselesaipekerjaan"
                                                                       style="display: none;">
                                                                <span class="form-text text-muted">Max file size is
                                                                    2MB.</span>
                                                            </div>

                                                        </td>
                                                        <td class="text-center">
                                                            <label for="skselesaipekerjaan"
                                                                   id="doneuploadskselesaipekerjaan"><a target="_blank"
                                                                                                        href="{{ url('/permohonan-masa-pensiun/file/'.AppHelper::instance()->enkrip($dataMpp->sketPekerjaan)) }}"><span
                                                                        class="las la-file-pdf fs-3x"></span></a></label>
                                                            <span id="skselesaipekerjaan_text"
                                                                  class="form-text text-muted fw-bold text-muted d-block fs-7">SKet
                                                                Pekerjaan
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="text-center">
                                    <a href="{{ url('/permohonan-masa-pensiun/permohonan') }}"
                                       class="btn btn-sm btn-secondary my-1"><i class="fa fa-reply"></i> Batal</a>
                                    <button type="submit" class="btn btn-sm btn-success my-1">
                                        <span class="fa fa-save"></span> Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--end::Container-->
        </div>


        @endsection

        @section('js')
            <script src="{{ url('assets') }}/js/pensiun/pensiunmpp/permohonanmpp.js" type="text/javascript"></script>
@endsection
