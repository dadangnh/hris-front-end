@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card mb-3 border-warning shadow-sm">
                    <div id="card-header" class="card-header">
                        <div class="card-title fw-bold fs-2 text-white">
                            Review Konsep SK Permohonan Masa Persiapan Pensiun
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="card">
                            <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                                <div class="p-2">
                                    <div class="row g-9 mb-6">
                                        <div class="col-md-6 fv-row">
                                            <label><span>Nama Pegawai</span></label>
                                            <input type="text" class="form-control form-control-solid"
                                                   placeholder="Nama Pegawai" name="nama" value="{{ $dataMpp->nama }}"
                                                   disabled/>
                                        </div>
                                        <div class="col-md-6 fv-row">
                                            <label fs-6 fw-bold mb-2">NIP</label>
                                            <input type="text" class="form-control form-control-solid"
                                                   placeholder="Nama Pegawai" name="nip" value="{{ $dataMpp->nip }}"
                                                   disabled/>
                                        </div>
                                    </div>
                                    <div class="row g-9 mb-6">
                                        <div class="col-md-6 fv-row">
                                            <label fs-6 fw-bold mb-2">Pangkat</label>
                                            <input type="text" class="form-control form-control-solid"
                                                   placeholder="Nama Pegawai" name="pangkat"
                                                   value="{{ $dataMpp->pangkat }}" disabled/>
                                        </div>
                                        <div class="col-md-6 fv-row">
                                            <label fs-6 fw-bold mb-2">Jabatan</label>
                                            <input type="text" class="form-control form-control-solid"
                                                   placeholder="Nama Pegawai" name="jabatan"
                                                   value="{{ $dataMpp->jabatan }}" disabled/>
                                        </div>
                                    </div>
                                    <div class="row g-9 mb-6">
                                        <div class="col-md-6 fv-row">
                                            <label fs-6 fw-bold mb-2">Unit Organisasi</label>
                                            <input type="text" class="form-control form-control-solid"
                                                   placeholder="Nama Pegawai" name="unitorganisasi"
                                                   value="{{ $dataMpp->unitOrg }}" disabled/>
                                        </div>
                                        <div class="col-md-6 fv-row">
                                            <label fs-6 fw-bold mb-2">BUP Pensiun</label>
                                            <input type="text" class="form-control form-control-solid"
                                                   placeholder="BUP Pensiun" name="buppensiun"
                                                   value="{{ $dataMpp->bup }}" required="required" disabled>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column mb-6 fv-row">
                                        <label><span>Alamat</span></label>
                                        <input type="text" class="form-control form-control-solid"
                                               placeholder="Alamat Pegawai" name="alamat" disabled
                                               value="{{ $dataMpp->alamat }}"/>
                                    </div>
                                    <div class="row g-9 mb-6">
                                        <div class="col-md-6 fv-row">
                                            <label fs-6 fw-bold mb-2">SK CPNS</label>
                                            <input type="text" class="form-control form-control-solid"
                                                   placeholder="Tanggal Lahir" name="skcpns"
                                                   value="" disabled/>
                                        </div>
                                        <div class="col-md-6 fv-row">
                                            <label fs-6 fw-bold mb-2">SK Pangkat</label>
                                            <input type="text" class="form-control form-control-solid"
                                                   placeholder="SK Pangkat" name="skpangkat"
                                                   required="required" disabled>
                                        </div>
                                    </div>
                                    <div class="row g-9 mb-6">
                                        <div class="col-md-6 fv-row">
                                            <label class="required fs-6 fw-bold mb-2">Masa Persiapan Pensiun</label>
                                            <div class="input-group">
                                                <div class="position-relative d-flex align-items-center col-5">
                                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                    <span class="symbol-label bg-secondary">
                                                        <span class="svg-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="24px" height="24px" viewBox="0 0 24 24"
                                                                 version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24">
                                                                    </rect>
                                                                    <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                          width="4" height="4" rx="1">
                                                                    </rect>
                                                                    <path
                                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                        fill="#000000"></path>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    </span>
                                                    </div>
                                                    <input class="form-control ps-12 flatpickr-input masaawal"
                                                           id="datemonth" placeholder="Tahun - Bulan" name="masaawal"
                                                           type="text" value="{{ $dataMpp->masaAwal }}" required
                                                           disabled>
                                                </div>
                                                <div class="input-group-append">
                                                    <span class="input-group-text text-gray-500">s.d.</span>
                                                </div>
                                                <div class="position-relative d-flex align-items-center col-5">
                                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                    <span class="symbol-label bg-secondary">
                                                        <span class="svg-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="24px" height="24px" viewBox="0 0 24 24"
                                                                 version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24">
                                                                    </rect>
                                                                    <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                          width="4" height="4" rx="1">
                                                                    </rect>
                                                                    <path
                                                                        d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                        fill="#000000"></path>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    </span>
                                                    </div>
                                                    <input class="form-control ps-12 flatpickr-input" id="datemonth1"
                                                           placeholder="Tahun - Bulan" name="masaakhir" type="text"
                                                           value="{{ $dataMpp->masaAkhir }}" required="" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 fv-row">
                                            <label class="fs-6 fw-bold mb-2">Selisih Bulan</label>
                                            <div class="position-relative d-flex align-items-center col-6">
                                                <input type="hidden" id="selisihbln" placeholder="Selisih Jumlah Bulan"
                                                       name="selisihbln" value="{{ $dataMpp->selisihMpp }}">
                                                <input class="form-control flatpickr-input" id="selisihBulan"
                                                       placeholder="Selisih Jumlah Bulan" name="selisihBulan" disabled
                                                       value="{{ $dataMpp->selisihMpp }}">
                                                <div class="symbol symbol-45px me-4 position-left ms-4">
                                                <span class="symbol-label bg-white">
                                                    <span class="text" width="24px" height="24px"
                                                          viewBox="0 0 24 24">
                                                        Bulan
                                                    </span>
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                                <div class="p-2">
                                    <div class="row">
                                        <div class="col-12">
                                            <td class="ps-4 text-dark fs-6">
                                                <h3>SURAT KETERANGAN</h3>
                                            </td>
                                        </div>
                                        <div class="my-5">
                                            <table class="table table-rounded fs-5 table-row-bordered border gy-4 gs-4">
                                                <thead class="fw-bolder bg-secondary">
                                                <tr
                                                    class="fw-bold text-gray-800 border-bottom-2 border-gray-200 align-middle">
                                                    <th width="80%">Keterangan</th>
                                                    <th class="text-center" width="20%">File</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td class="align-middle"><label for="skpelanggarandisiplin">a.
                                                            Surat Keterangan tidak dalam
                                                            proses pemeriksaan pelanggaran disiplin</label></td>
                                                    <td class="text-center">
                                                        <label for="skpelanggarandisiplin"
                                                               id="doneuploadskpelanggarandisiplin"><a target="_blank"
                                                                                                       href="{{ url('/permohonan-masa-pensiun/file/'.AppHelper::instance()->enkrip($dataMpp->sketPeriksa)) }}"><span
                                                                    class="las la-file-pdf fs-3x"></span></a></label>
                                                        <span id="skpelanggarandisiplin_text"
                                                              class="form-text text-muted fw-bold text-muted d-block fs-7">SKet
                                                            Periksa
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="align-middle"><label for="skperadilan">b. Surat
                                                            Keterangan tidak dalam proses
                                                            peradilan</label></td>
                                                    <td class="text-center">
                                                        <label for="skperadilan" id="doneuploadskperadilan"><a
                                                                target="_blank"
                                                                href="{{ url('/permohonan-masa-pensiun/file/'.AppHelper::instance()->enkrip($dataMpp->sketPengadilan)) }}"><span
                                                                    class="las la-file-pdf fs-3x"></span></a></label>
                                                        <span id="skperadilan_text"
                                                              class="form-text text-muted fw-bold text-muted d-block fs-7">SKet
                                                            Peradilan
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="align-middle"><label for="skselesaipekerjaan">c.
                                                            Surat
                                                            Keterangan telah
                                                            menyelesaikan Pekerjaan</label></td>
                                                    <td class="text-center">
                                                        <label for="skselesaipekerjaan"
                                                               id="doneuploadskselesaipekerjaan"><a target="_blank"
                                                                                                    href="{{ url('/permohonan-masa-pensiun/file/'.AppHelper::instance()->enkrip($dataMpp->sketPekerjaan)) }}"><span
                                                                    class="las la-file-pdf fs-3x"></span></a></label>
                                                        <span id="skselesaipekerjaan_text"
                                                              class="form-text text-muted fw-bold text-muted d-block fs-7">SKet
                                                            Pekerjaan
                                                        </span>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">
                                <div class="p-2">
                                    <form
                                        action="{{ url('/permohonan-masa-pensiun/review/disposisi/penelitian') }}"
                                        method="post" enctype="multipart/form-data">
                                        @csrf

                                        <input type="hidden" name="idMpp"
                                               value="{{ AppHelper::instance()->enkrip($dataMpp->idMpp) }}">
                                        <div class="row mt-10">
                                            <div class="row">
                                                <div class="col-12 fw-bold fs-3">
                                                    Nota Dinas Pengantar
                                                </div>
                                                <div class="col-3 ps-4 text-dark  mb-1 fs-6">
                                                    <label>
                                                        <input type="text" class="form-control-plaintext"
                                                               value="No Surat Pengantar :" disabled>
                                                    </label>
                                                </div>
                                                <div class="col-3 ps-4 text-dark  mb-1 fs-6">
                                                    <input class="form-control form-control-md form-control-solid"
                                                           type="text" name="suratpengantar"
                                                           value="{{ $dataMpp->ndPengantar }}">
                                                </div>
                                                <div class="col-3 ps-4 text-dark  mb-1 fs-6">
                                                    <label>
                                                        <input type="text" class="form-control-plaintext"
                                                               value="Tanggal ND Pengantar :" disabled>
                                                    </label>
                                                </div>
                                                <div class="col-3 ps-4 text-dark  mb-1 fs-6">
                                                    <div class="position-relative d-flex align-items-center">
                                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                        <span class="symbol-label bg-secondary">
                                                            <span class="svg-icon">
                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                     width="24px" height="24px" viewBox="0 0 24 24"
                                                                     version="1.1">
                                                                    <g stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd">
                                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                                        <rect fill="#000000" opacity="0.3" x="4" y="4"
                                                                              width="4" height="4" rx="1"></rect>
                                                                        <path
                                                                            d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z"
                                                                            fill="#000000"></path>
                                                                    </g>
                                                                </svg>
                                                            </span>
                                                        </span>
                                                        </div>
                                                        <input
                                                            class="form-control form-control-md form-control-solid ps-12 flatpickr-input"
                                                            id="datemonthyear" placeholder="yyyy-mm-dd"
                                                            name="tanggalsurat"
                                                            type="text"
                                                            value="{{ $dataMpp->tglNdPengantar !=null ? date("Y-m-d", strtotime($dataMpp->tglNdPengantar)) : '' }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-10">
                                            <div class="row">
                                                <div class="col-12 fw-bold fs-3">
                                                    KONFIRMASI HUKDIS
                                                </div>
                                                <div class="col-3 ps-4 text-dark  mb-1 fs-6">
                                                    <label>
                                                        <input type="text" class="form-control-plaintext"
                                                               value="No Surat Permintaan :" disabled>
                                                    </label>
                                                </div>
                                                <div class="col-3 ps-4 text-dark  mb-1 fs-6">
                                                    <input class="form-control form-control-md form-control-solid"
                                                           type="text" name="suratpermintaan"
                                                           value="{{ $dataMpp->noSrtPermintaan }}">
                                                </div>
                                                <div class="col-3 ps-4 text-dark  mb-1 fs-6">
                                                    <label>
                                                        <input type="text" class="form-control-plaintext"
                                                               value="No Surat Konfirmasi :" disabled>
                                                    </label>
                                                </div>
                                                <div class="col-3 ps-4 text-dark  mb-1 fs-6">
                                                    <input class="form-control form-control-md form-control-solid"
                                                           type="text" name="suratkonfirmasi"
                                                           value="{{ $dataMpp->noSrtKonfirmasi }}">
                                                </div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-3 ps-4 text-dark  mb-1 fs-6">
                                                    Upload ND File
                                                </div>
                                                <div class="col-3">
                                                    <label for="uploadndfilespermintaan"
                                                           class="btn btn-sm btn-primary me-2"><span
                                                            class="svg-icon  svg-icon-1x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo13/dist/../src/media/svg/icons/Files/Upload.svg--><svg
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                width="24px"
                                                                height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"/>
                                                                    <path
                                                                        d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z"
                                                                        fill="#000000" fill-rule="nonzero"
                                                                        opacity="0.3"/>
                                                                    <rect fill="#000000" opacity="0.3" x="11" y="2"
                                                                          width="2" height="14" rx="1"/>
                                                                    <path
                                                                        d="M12.0362375,3.37797611 L7.70710678,7.70710678 C7.31658249,8.09763107 6.68341751,8.09763107 6.29289322,7.70710678 C5.90236893,7.31658249 5.90236893,6.68341751 6.29289322,6.29289322 L11.2928932,1.29289322 C11.6689749,0.916811528 12.2736364,0.900910387 12.6689647,1.25670585 L17.6689647,5.75670585 C18.0794748,6.12616487 18.1127532,6.75845471 17.7432941,7.16896473 C17.3738351,7.57947475 16.7415453,7.61275317 16.3310353,7.24329415 L12.0362375,3.37797611 Z"
                                                                        fill="#000000" fill-rule="nonzero"/>
                                                                </g>
                                                            </svg><!--end::Svg Icon--></span>
                                                        Upload File</label>
                                                    @if($dataMpp->fileSrtPermintaan !=null)
                                                        <a target="_blank"
                                                           href="{{ url('/permohonan-masa-pensiun/file/'.AppHelper::instance()->enkrip($dataMpp->fileSrtPermintaan)) }}">
                                                         <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo13/dist/../src/media/svg/icons/Files/Download.svg--><svg
                                                                 xmlns="http://www.w3.org/2000/svg"
                                                                 width="24px"
                                                                 height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none"
                                                           fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"/>
                                                            <path
                                                                d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z"
                                                                fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                            <rect fill="#000000" opacity="0.3"
                                                                  transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) "
                                                                  x="11" y="1" width="2" height="14" rx="1"/>
                                                            <path
                                                                d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z"
                                                                fill="#000000" fill-rule="nonzero"
                                                                transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                        </g>
                                                    </svg><!--end::Svg Icon--></span> NDPermintaan
                                                        </a>
                                                    @endif
                                                    <div class="col-12">
                                                        <input type="file" name="ndfilespermintaan"
                                                               id="uploadndfilespermintaan" style="display: none;">
                                                        <span class="form-text text-muted">Max file size is 2MB.</span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ndfilespermintaan"
                                                               id="doneuploadndfilespermintaan"></label>
                                                        <a type="button" class="hapusdoneuploadndfilespermintaan"
                                                           style="display:none;" data-bs-toggle="tooltip"
                                                           data-bs-placement="top" title="Hapus"><span class="svg-icon"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo13/dist/../src/media/svg/icons/Home/Trash.svg--><svg
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    width="24px" height="24px" viewBox="0 0 24 24"
                                                                    version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"/>
                                                                    <path
                                                                        d="M6,8 L18,8 L17.106535,19.6150447 C17.04642,20.3965405 16.3947578,21 15.6109533,21 L8.38904671,21 C7.60524225,21 6.95358004,20.3965405 6.89346498,19.6150447 L6,8 Z M8,10 L8.45438229,14.0894406 L15.5517885,14.0339036 L16,10 L8,10 Z"
                                                                        fill="#000000" fill-rule="nonzero"/>
                                                                    <path
                                                                        d="M14,4.5 L14,3.5 C14,3.22385763 13.7761424,3 13.5,3 L10.5,3 C10.2238576,3 10,3.22385763 10,3.5 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z"
                                                                        fill="#000000" opacity="0.3"/>
                                                                </g>
                                                            </svg><!--end::Svg Icon--></span></a>
                                                    </div>
                                                </div>
                                                <div class="col-3 ps-4 text-dark  mb-1 fs-6">
                                                    Upload ND File
                                                </div>
                                                <div class="col-3">
                                                    <label for="uploadndskonfirmasi"
                                                           class="btn btn-sm btn-primary me-2"><span
                                                            class="svg-icon  svg-icon-1x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo13/dist/../src/media/svg/icons/Files/Upload.svg--><svg
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                width="24px"
                                                                height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"/>
                                                                    <path
                                                                        d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z"
                                                                        fill="#000000" fill-rule="nonzero"
                                                                        opacity="0.3"/>
                                                                    <rect fill="#000000" opacity="0.3" x="11" y="2"
                                                                          width="2" height="14" rx="1"/>
                                                                    <path
                                                                        d="M12.0362375,3.37797611 L7.70710678,7.70710678 C7.31658249,8.09763107 6.68341751,8.09763107 6.29289322,7.70710678 C5.90236893,7.31658249 5.90236893,6.68341751 6.29289322,6.29289322 L11.2928932,1.29289322 C11.6689749,0.916811528 12.2736364,0.900910387 12.6689647,1.25670585 L17.6689647,5.75670585 C18.0794748,6.12616487 18.1127532,6.75845471 17.7432941,7.16896473 C17.3738351,7.57947475 16.7415453,7.61275317 16.3310353,7.24329415 L12.0362375,3.37797611 Z"
                                                                        fill="#000000" fill-rule="nonzero"/>
                                                                </g>
                                                            </svg><!--end::Svg Icon--></span> Upload File</label>
                                                    @if($dataMpp->fileSrtKonfirmasi !=null)
                                                        <a target="_blank"
                                                           href="{{ url('/permohonan-masa-pensiun/file/'.AppHelper::instance()->enkrip($dataMpp->fileSrtKonfirmasi)) }}">
                                                         <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo13/dist/../src/media/svg/icons/Files/Download.svg--><svg
                                                                 xmlns="http://www.w3.org/2000/svg"
                                                                 width="24px"
                                                                 height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none"
                                                           fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"/>
                                                            <path
                                                                d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z"
                                                                fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                            <rect fill="#000000" opacity="0.3"
                                                                  transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) "
                                                                  x="11" y="1" width="2" height="14" rx="1"/>
                                                            <path
                                                                d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z"
                                                                fill="#000000" fill-rule="nonzero"
                                                                transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                        </g>
                                                    </svg><!--end::Svg Icon--></span> NDKonfirmasi
                                                        </a>
                                                    @endif
                                                    <div class="col-12">
                                                        <input type="file" name="ndskonfirmasi" id="uploadndskonfirmasi"
                                                               style="display: none;">
                                                        <span class="form-text text-muted">Max file size is 2MB.</span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ndskonfirmasi" id="doneuploadndskonfirmasi"></label>
                                                        <a type="button" class="hapusdoneuploadndskonfirmasi"
                                                           style="display:none;" data-bs-toggle="tooltip"
                                                           data-bs-placement="top" title="Hapus"><span
                                                                class="svg-icon "><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo13/dist/../src/media/svg/icons/Home/Trash.svg--><svg
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    width="24px" height="24px" viewBox="0 0 24 24"
                                                                    version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"/>
                                                                    <path
                                                                        d="M6,8 L18,8 L17.106535,19.6150447 C17.04642,20.3965405 16.3947578,21 15.6109533,21 L8.38904671,21 C7.60524225,21 6.95358004,20.3965405 6.89346498,19.6150447 L6,8 Z M8,10 L8.45438229,14.0894406 L15.5517885,14.0339036 L16,10 L8,10 Z"
                                                                        fill="#000000" fill-rule="nonzero"/>
                                                                    <path
                                                                        d="M14,4.5 L14,3.5 C14,3.22385763 13.7761424,3 13.5,3 L10.5,3 C10.2238576,3 10,3.22385763 10,3.5 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z"
                                                                        fill="#000000" opacity="0.3"/>
                                                                </g>
                                                            </svg><!--end::Svg Icon--></span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <div class="text-center">
                            <a href="{{ url('/permohonan-masa-pensiun/review') }}"
                               class="btn btn-sm btn-secondary my-1"><i class="fa fa-reply"></i> Kembali</a>
                            <button type="submit" class="btn btn-sm btn-success my-1">
                                <i class="fa fa-save"></i> Simpan Draft
                            </button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>

@endsection

@section('js')
    <script src="{{ url('assets') }}/js/pensiun/pensiunmpp/permohonanmpp.js" type="text/javascript"></script>
@endsection
