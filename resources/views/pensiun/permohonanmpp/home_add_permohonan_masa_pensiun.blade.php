@extends('layout.master')

@section('content')
    <div id="block">
        <div class="post d-flex flex-column" id="kt_post">
            <!--begin::Container-->
            <div id="kt_content_container" class="container-fluid">
                <div class="col-lg-12">
                    <div class="card mb-3 border-warning shadow-sm">
                        <div id="card-header" class="card-header">
                            <div class="card-title fw-bold fs-4 text-white">
                                Daftar Permohonan Masa Persiapan Pensiun
                            </div>
                        </div>
                        {{-- End Card Header --}}
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6 mb-4">
                                    <div class="form-group form-sm float-right col-4 inner-addon left-addon">
                                        {{-- <span class="svg-icon svg-icon-3 svg-icon-gray-500 position-absolute top-50 translate-middle ms-6">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path
                                                        d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z"
                                                        fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                    <path
                                                        d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z"
                                                        fill="#000000" fill-rule="nonzero"></path>
                                                </g>
                                            </svg>
                                        </span> --}}
                                        <input type="text" id="searchData" class="form-control form-control-solid ps-10"
                                               name="search" value="" placeholder="Filter">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <div class="text-end">
                                        <a href="{{ url('/permohonan-masa-pensiun/form-permohonan') }}"
                                           class="btn btn-sm btn-primary fs-6">
                                        <span class="fas fa-plus">
                                        </span>
                                            Tambah
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive bg-gray-400 rounded shadow-sm">
                                <!--begin::Table-->
                                <table class="table table-striped align-middle datatable">
                                    <!--begin::Thead-->
                                    <thead class="fw-bolder bg-secondary fs-6">
                                    <tr class="text-center">
                                        <th class="ps-2">No</th>
                                        <th>No Permohonan / Tgl Permohonan</th>
                                        <th>Status</th>
                                        <th class="pe-2">Aksi</th>
                                    </tr>
                                    </thead>
                                    <!--end::Thead-->
                                    <!--begin::Tbody-->
                                    <tbody id="dataPermohonan">
                                    @if (count($dataMpp) != 0)
                                        @foreach($dataMpp as $key => $items)
                                            <tr class="text-center">
                                                <td class="ps-4 text-dark  mb-1 fs-6">
                                                    {{ (($page - 1) * 10) + 1 + $key }}
                                                </td>
                                                <td class="text-dark  mb-1 fs-6">
                                                    <p class="text-dark fw-bolder text-hover-primary d-block fs-6"></p>{{ $items->notiket }}
                                                    <span
                                                        class="text-muted fw-bold text-muted d-block fs-7">{{AppHelper::instance()->indonesian_date(strtotime($items->wktCreateMohon),'j F Y','')}}</span>
                                                </td>
                                                <td class="text-center text-dark  mb-1 fs-6">
                                                    @if($items->status->urutanCase==2)
                                                        <span
                                                            class="badge badge-light-warning fs-7 fw-bolder">{{$items->status->uraianStatus}}</span>
                                                    @elseif($items->status->urutanCase==0)
                                                        <span
                                                            class="badge badge-light-danger fs-7 fw-bolder">{{$items->status->uraianStatus}}</span>
                                                    @elseif($items->status->urutanCase==10)
                                                        <span
                                                            class="badge badge-light-success fs-7 fw-bolder">{{$items->status->uraianStatus}}</span>
                                                    @elseif($items->status->urutanCase==11)
                                                        <span
                                                            class="badge badge-light-info fs-7 fw-bolder">{{$items->status->uraianStatus}}</span>
                                                    @elseif($items->status->urutanCase==12)
                                                        <span
                                                            class="badge badge-light-warning fs-7 fw-bolder">{{$items->status->uraianStatus}}</span>
                                                    @else
                                                        <span
                                                            class="badge badge-light-primary fs-7 fw-bolder">{{$items->status->uraianStatus}}</span>
                                                    @endif
                                                </td>
                                                <td class="text-dark mb-1 fs-6 pe-2">
                                                    @if($items->status->urutanCase==2)
                                                        <a href="{{ url('/permohonan-masa-pensiun/edit/'.AppHelper::instance()->enkrip($items->idMpp)) }}"
                                                           class="btn btn-sm btn-icon btn-warning text-center"
                                                           data-bs-toggle="tooltip" data-bs-placement="top"
                                                           title="Edit">
                                                            <span class="fas fa-edit"></span>
                                                        </a>
                                                        <button
                                                            id="btnAjukan"
                                                            data-url="{{ url('/permohonan-masa-pensiun/ajukan/'.AppHelper::instance()->enkrip($items->idMpp)) }}"
                                                            class="btn btn-sm btn-primary btn-icon btnAjukan"
                                                            data-bs-toggle="tooltip"
                                                            data-bs-placement="top" title="Ajukan">
                                                            <span class="fas fa-paper-plane"></span>
                                                        </button>
                                                        <button
                                                            id="btnHapus"
                                                            data-url="{{ url('/permohonan-masa-pensiun/delete/'.AppHelper::instance()->enkrip($items->idMpp)) }}"
                                                            class="btn btn-sm btn-danger btn-icon btnHapus"
                                                            data-bs-toggle="tooltip" data-bs-placement="top"
                                                            title="Hapus">
                                                            <span class="fas fa-trash"></span>
                                                        </button>
                                                    @elseif($items->status->urutanCase==0)
                                                        <span data-bs-toggle="tooltip" data-bs-placement="top"
                                                              title="Preview">
                                                        <button type="button"
                                                                class="btn btn-sm btn-icon btn-info detilModal"
                                                                data-mpp="{{ AppHelper::instance()->enkrip($items->idMpp) }}"
                                                                data-tiket="{{ $items->notiket }}"
                                                                data-tanggal="{{ date('Y-m-d', strtotime($items->wktCreateMohon)) }}"
                                                                data-keterangan="{{ $items->alasanTolakUpkLokal }}"
                                                                data-status="{{ $items->status->uraianStatus }}"
                                                                data-bs-toggle="modal" data-bs-target="#viewStatus">
                                                            <span class="fas fa-eye"></span>
                                                        </button>
                                                    </span>
                                                    @elseif($items->status->urutanCase==10|| $items->status->urutanCase==11||$items->status->urutanCase==12)
                                                        <a href="{{ '/permohonan-masa-pensiun/permohonan/download-sk/'.AppHelper::instance()->enkrip($items->skMpp)}}"
                                                           target="_blank"
                                                           class="btn btn-sm btn-icon btn-warning me-2"
                                                           data-bs-toggle="tooltip"
                                                           data-bs-placement="top" title="Download Konsep SK">
                                                            <span class="fas fa-download"></span>
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr class="text-center">
                                            <td class="text-dark  mb-1 fs-6" colspan="6">Tidak terdapat data</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                    <!--end::Tbody-->
                                </table>
                                <!--end::Table-->
                                <!--begin::pagination-->
                                <div class="d-flex flex-stack flex-wrap pt-5 p-3">
                                    <div class="fs-6 fw-bold text-gray-700"></div>
                                    @php
                                        $disabled = '';
                                        if($currentPage==1){
                                        $disabled = 'disabled';
                                        }

                                        $nextPage= '';
                                        if($currentPage!=$totalPages){
                                        $nextPage = $currentPage+1;
                                        }

                                        $disablednext = '';
                                        $hidden = '';
                                        if($currentPage==$totalPages){
                                        $disablednext = 'disabled';
                                        $hidden='hidden';
                                        }

                                        $hiddenpagination ='';
                                        if($totalItems == 0){
                                            $hiddenpagination = "none";
                                            $hidden='hidden';
                                        }

                                    @endphp
                                        <!--begin::Pages-->
                                    <ul class="pagination" style="display: {{$hiddenpagination}}">
                                        <li class="page-item disabled"><a href="#"
                                                                          class="page-link">Halaman {{ $currentPage }}
                                                dari {{ $totalPages }}</a></li>
                                        <li class="page-item previous {{ $disabled }}"><a
                                                href="{{ url('/permohonan-masa-pensiun/permohonan?page='.($currentPage-1)) }}"
                                                class="page-link"><i class="previous"></i></a></li>
                                        <li class="page-item active"><a
                                                href="{{ url('/permohonan-masa-pensiun/permohonan?page='.$currentPage) }}"
                                                class="page-link">{{ $currentPage }}</a></li>
                                        <li class="page-item" {{ $hidden }}><a
                                                href="{{ url('/permohonan-masa-pensiun/permohonan?page='.$nextPage) }}"
                                                class="page-link">{{ $nextPage }}</a></li>
                                        <li class="page-item next {{ $disablednext }}"><a
                                                href="{{ url('/permohonan-masa-pensiun/permohonan?page='.$nextPage) }}"
                                                class="page-link"><i class="next"></i></a></li>
                                    </ul>
                                    <!--end::Pages-->
                                </div>
                                <!--end::paging-->
                            </div>

                            <!-- Modal Status -->
                            <!--begin::Modal-->
                            <div class="modal fade" id="viewStatus" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header card-header">
                                            <h3 class="modal-title text-white" id="exampleModalLabel">Status
                                                Pengajuan</h3>
                                        </div>
                                        <div class="modal-body">
                                            <div class="card">
                                                <div class="rounded border border-gray-300 p-4 d-flex flex-column mb-6">

                                                    <!--begin::Input group-->
                                                    <div class="row g-9 mb-8">
                                                        <!--begin::Col-->
                                                        <div class="col-md-6 fv-row">
                                                            <label class="fs-6 fw-bold">Tiket Permohonan</label>
                                                            <input type="text" class="form-control form-control-solid"
                                                                   id="tiket" disabled/>
                                                        </div>
                                                        <!--end::Col-->
                                                        <!--begin::Col-->
                                                        <div class="col-md-6 fv-row">
                                                            <label class="fs-6 fw-bold">Tanggal Tiket Permohonan</label>
                                                            <!--begin::Input-->
                                                            <input type="text" class="form-control form-control-solid"
                                                                   id="tanggal" disabled/>
                                                            <!--end::Input-->
                                                        </div>
                                                        <!--end::Col-->
                                                    </div>
                                                    <div class="d-flex flex-column fv-row">
                                                        <!--begin::Label-->
                                                        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                                            <span>Keterangan</span>
                                                        </label>
                                                        <!--end::Label-->
                                                        <textarea type="text" class="form-control form-control-solid"
                                                                  id="keterangan" disabled></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="table-responsive bg-gray-400 rounded shadow-sm">

                                                <!--begin::Table-->
                                                <table id="example"
                                                       class="table table-striped align-middle text-center">
                                                </table>
                                                <!--end::Table-->
                                            </div>
                                        </div>
                                        <div class="text-center mb-8">
                                            <button class="btn btn-sm btn-secondary my-1" data-bs-dismiss="modal"><i
                                                    class="fa fa-reply"></i> Kembali
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end modal -->

                        </div>
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ url('assets') }}/js/pensiun/pensiunmpp/permohonanmpp.js" type="text/javascript"></script>
@endsection
