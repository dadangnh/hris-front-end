@extends('layout_new.master')
<?php
// dump($user);
?>

@section('content')

    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <div class="card shadow-lg mb-3">
                <div class="card-body text-center">
                        <!--begin::Underline-->
                        <span class="d-inline-block position-relative ms-2">
                            <!--begin::Label-->
                            <span class="d-inline-block mb-2 fs-1 fw-bold"style="color: #02275d">
                                Selamat Datang di Layanan Mandiri
                            </span>
                            <!--end::Label-->
                            <!--begin::Line-->
                            <span class="d-inline-block position-absolute h-3px bottom-0 end-0 start-0 bg-warning translate rounded"></span>
                            <!--end::Line-->
                        </span>
                        <!--end::Underline-->
                    <!--begin::Main wrapper-->
                    <div class="row" style="padding-top: 30px">
                        <div class="col-lg-10" style="margin: 0 auto">
                            <div
                                id="kt_docs_search_handler_basic"

                                data-kt-search-keypress="true"
                                data-kt-search-min-length="2"
                                data-kt-search-enter="true"
                                data-kt-search-layout="inline">

                                <!--begin::Input Form-->
                                <form data-kt-search-element="form" class="w-100 position-relative mb-5" autocomplete="off">
                                    <!--begin::Hidden input(Added to disable form autocomplete)-->
                                    <input type="hidden"/>
                                    <!--end::Hidden input-->

                                    <!--begin::Icon-->
                                    <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                                    <!--end::Icon-->

                                    <!--begin::Input-->
                                    <input type="text" class="form-control form-control-lg form-control-solid px-15"
                                           name="search"
                                           value=""
                                           placeholder="Search by username, full name or email..."
                                           data-kt-search-element="input"/>
                                    <!--end::Input-->

                                    <!--begin::Spinner-->
                                    <span class="position-absolute top-50 end-0 translate-middle-y lh-0 d-none me-5" data-kt-search-element="spinner">
            <span class="spinner-border h-15px w-15px align-middle text-gray-400"></span>
        </span>
                                    <!--end::Spinner-->

                                    <!--begin::Reset-->
                                    <span class="btn btn-flush btn-active-color-primary position-absolute top-50 end-0 translate-middle-y lh-0 me-5 d-none"
                                          data-kt-search-element="clear">

            <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
        </span>
                                    <!--end::Reset-->
                                </form>
                                <!--end::Form-->

                                <!--begin::Wrapper-->
                                {{--                        <div class="py-5">--}}
                                {{--                            <!--being::Search suggestion-->--}}
                                {{--                            <div data-kt-search-element="suggestions">--}}
                                {{--                                ...--}}
                                {{--                            </div>--}}
                                {{--                            <!--end::Suggestion wrapper-->--}}

                                {{--                            <!--begin::Search results-->--}}
                                {{--                            <div data-kt-search-element="results" class="d-none">--}}
                                {{--                                ...--}}
                                {{--                            </div>--}}
                                {{--                            <!--end::Search results-->--}}

                                {{--                            <!--begin::Empty search-->--}}
                                {{--                            <div data-kt-search-element="empty" class="text-center d-none">--}}
                                {{--                                ...--}}
                                {{--                            </div>--}}
                                {{--                            <!--end::Empty search-->--}}
                                {{--                        </div>--}}
                                {{--                        <!--end::Wrapper-->--}}
                            </div>
                        </div>
                    </div>

                    <!--end::Main wrapper-->
                </div>
            </div>
            <div class="py-5 mb-3">
                <div class="row">
                    <div class="col-md-4">
                        <a href="#" class="card hover-elevate-up shadow-sm parent-hover">
                            <div class="card-body d-flex align-items" style="border-top: 5px solid #ffc107">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen002.svg-->
                                <span class="svg-icon svg-icon-1"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path opacity="0.3" d="M4.05424 15.1982C8.34524 7.76818 13.5782 3.26318 20.9282 2.01418C21.0729 1.98837 21.2216 1.99789 21.3618 2.04193C21.502 2.08597 21.6294 2.16323 21.7333 2.26712C21.8372 2.37101 21.9144 2.49846 21.9585 2.63863C22.0025 2.7788 22.012 2.92754 21.9862 3.07218C20.7372 10.4222 16.2322 15.6552 8.80224 19.9462L4.05424 15.1982ZM3.81924 17.3372L2.63324 20.4482C2.58427 20.5765 2.5735 20.7163 2.6022 20.8507C2.63091 20.9851 2.69788 21.1082 2.79503 21.2054C2.89218 21.3025 3.01536 21.3695 3.14972 21.3982C3.28408 21.4269 3.42387 21.4161 3.55224 21.3672L6.66524 20.1802L3.81924 17.3372ZM16.5002 5.99818C16.2036 5.99818 15.9136 6.08615 15.6669 6.25097C15.4202 6.41579 15.228 6.65006 15.1144 6.92415C15.0009 7.19824 14.9712 7.49984 15.0291 7.79081C15.0869 8.08178 15.2298 8.34906 15.4396 8.55884C15.6494 8.76862 15.9166 8.91148 16.2076 8.96935C16.4986 9.02723 16.8002 8.99753 17.0743 8.884C17.3484 8.77046 17.5826 8.5782 17.7474 8.33153C17.9123 8.08486 18.0002 7.79485 18.0002 7.49818C18.0002 7.10035 17.8422 6.71882 17.5609 6.43752C17.2796 6.15621 16.8981 5.99818 16.5002 5.99818Z" fill="currentColor"></path>
                                <path d="M4.05423 15.1982L2.24723 13.3912C2.15505 13.299 2.08547 13.1867 2.04395 13.0632C2.00243 12.9396 1.9901 12.8081 2.00793 12.679C2.02575 12.5498 2.07325 12.4266 2.14669 12.3189C2.22013 12.2112 2.31752 12.1219 2.43123 12.0582L9.15323 8.28918C7.17353 10.3717 5.4607 12.6926 4.05423 15.1982ZM8.80023 19.9442L10.6072 21.7512C10.6994 21.8434 10.8117 21.9129 10.9352 21.9545C11.0588 21.996 11.1903 22.0083 11.3195 21.9905C11.4486 21.9727 11.5718 21.9252 11.6795 21.8517C11.7872 21.7783 11.8765 21.6809 11.9402 21.5672L15.7092 14.8442C13.6269 16.8245 11.3061 18.5377 8.80023 19.9442ZM7.04023 18.1832L12.5832 12.6402C12.7381 12.4759 12.8228 12.2577 12.8195 12.032C12.8161 11.8063 12.725 11.5907 12.5653 11.4311C12.4057 11.2714 12.1901 11.1803 11.9644 11.1769C11.7387 11.1736 11.5205 11.2583 11.3562 11.4132L5.81323 16.9562L7.04023 18.1832Z" fill="currentColor"></path>
                                </svg>
                                </span>
                                <!--end::Svg Icon-->
                                <span class="ms-3 text-gray-700 parent-hover-primary fs-4 fw-bold">
                                    Permohonan
                                </span>
                                <div class="d-flex flex-column align-items-end ms-2">
                                    <span class="badge badge-sm badge-circle badge-light-danger">2</span>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-md-4">
                        <a href="#" class="card hover-elevate-up shadow-sm parent-hover">
                            <div class="card-body d-flex align-items" style="border-top: 5px solid #ffc107">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen013.svg-->
                                <span class="svg-icon svg-icon-1"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path opacity="0.3" d="M20.9 12.9C20.3 12.9 19.9 12.5 19.9 11.9C19.9 11.3 20.3 10.9 20.9 10.9H21.8C21.3 6.2 17.6 2.4 12.9 2V2.9C12.9 3.5 12.5 3.9 11.9 3.9C11.3 3.9 10.9 3.5 10.9 2.9V2C6.19999 2.5 2.4 6.2 2 10.9H2.89999C3.49999 10.9 3.89999 11.3 3.89999 11.9C3.89999 12.5 3.49999 12.9 2.89999 12.9H2C2.5 17.6 6.19999 21.4 10.9 21.8V20.9C10.9 20.3 11.3 19.9 11.9 19.9C12.5 19.9 12.9 20.3 12.9 20.9V21.8C17.6 21.3 21.4 17.6 21.8 12.9H20.9Z" fill="currentColor"></path>
                                <path d="M16.9 10.9H13.6C13.4 10.6 13.2 10.4 12.9 10.2V5.90002C12.9 5.30002 12.5 4.90002 11.9 4.90002C11.3 4.90002 10.9 5.30002 10.9 5.90002V10.2C10.6 10.4 10.4 10.6 10.2 10.9H9.89999C9.29999 10.9 8.89999 11.3 8.89999 11.9C8.89999 12.5 9.29999 12.9 9.89999 12.9H10.2C10.4 13.2 10.6 13.4 10.9 13.6V13.9C10.9 14.5 11.3 14.9 11.9 14.9C12.5 14.9 12.9 14.5 12.9 13.9V13.6C13.2 13.4 13.4 13.2 13.6 12.9H16.9C17.5 12.9 17.9 12.5 17.9 11.9C17.9 11.3 17.5 10.9 16.9 10.9Z" fill="currentColor"></path>
                                </svg>
                                </span>
                                <!--end::Svg Icon-->
                                <span class="ms-3 text-gray-700 parent-hover-primary fs-4 fw-bold">
                                    Permohonan Disarankan
                                </span>
                                <div class="d-flex flex-column align-items-end ms-2">
                                    <span class="badge badge-sm badge-circle badge-light-danger">2</span>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-md-4">
                        <a href="#" class="card hover-elevate-up shadow-sm parent-hover">
                            <div class="card-body d-flex align-items" style="border-top: 5px solid #ffc107">
                                <!--begin::Svg Icon | path: icons/duotune/art/art007.svg-->
                                <span class="svg-icon svg-icon-1"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path opacity="0.3" d="M20.859 12.596L17.736 13.596L10.388 20.944C10.2915 21.0406 10.1769 21.1172 10.0508 21.1695C9.9247 21.2218 9.78953 21.2486 9.65302 21.2486C9.5165 21.2486 9.3813 21.2218 9.25519 21.1695C9.12907 21.1172 9.01449 21.0406 8.918 20.944L2.29999 14.3229C2.10543 14.1278 1.99619 13.8635 1.99619 13.588C1.99619 13.3124 2.10543 13.0481 2.29999 12.853L11.853 3.29999C11.9495 3.20341 12.0641 3.12679 12.1902 3.07452C12.3163 3.02225 12.4515 2.9953 12.588 2.9953C12.7245 2.9953 12.8597 3.02225 12.9858 3.07452C13.1119 3.12679 13.2265 3.20341 13.323 3.29999L21.199 11.176C21.3036 11.2791 21.3797 11.4075 21.4201 11.5486C21.4605 11.6898 21.4637 11.8391 21.4295 11.9819C21.3953 12.1247 21.3249 12.2562 21.2249 12.3638C21.125 12.4714 20.9989 12.5514 20.859 12.596Z" fill="currentColor"></path>
                                <path d="M14.8 10.184C14.7447 10.1843 14.6895 10.1796 14.635 10.1699L5.816 8.69997C5.55436 8.65634 5.32077 8.51055 5.16661 8.29469C5.01246 8.07884 4.95035 7.8106 4.99397 7.54897C5.0376 7.28733 5.18339 7.05371 5.39925 6.89955C5.6151 6.7454 5.88334 6.68332 6.14498 6.72694L14.963 8.19692C15.2112 8.23733 15.435 8.36982 15.59 8.56789C15.7449 8.76596 15.8195 9.01502 15.7989 9.26564C15.7784 9.51626 15.6642 9.75001 15.479 9.92018C15.2939 10.0904 15.0514 10.1846 14.8 10.184ZM17 18.6229C17 19.0281 17.0985 19.4272 17.287 19.7859C17.4755 20.1446 17.7484 20.4521 18.0821 20.6819C18.4158 20.9117 18.8004 21.0571 19.2027 21.1052C19.605 21.1534 20.0131 21.103 20.3916 20.9585C20.7702 20.814 21.1079 20.5797 21.3758 20.2757C21.6437 19.9716 21.8336 19.607 21.9293 19.2133C22.025 18.8195 22.0235 18.4085 21.925 18.0154C21.8266 17.6223 21.634 17.259 21.364 16.9569L19.843 15.257C19.7999 15.2085 19.7471 15.1697 19.688 15.1432C19.6289 15.1167 19.5648 15.1029 19.5 15.1029C19.4352 15.1029 19.3711 15.1167 19.312 15.1432C19.2529 15.1697 19.2001 15.2085 19.157 15.257L17.636 16.9569C17.2254 17.4146 16.9988 18.0081 17 18.6229ZM10.388 20.9409L17.736 13.5929H1.99999C1.99921 13.7291 2.02532 13.8643 2.0768 13.9904C2.12828 14.1165 2.2041 14.2311 2.29997 14.3279L8.91399 20.9409C9.01055 21.0381 9.12539 21.1152 9.25188 21.1679C9.37836 21.2205 9.51399 21.2476 9.65099 21.2476C9.78798 21.2476 9.92361 21.2205 10.0501 21.1679C10.1766 21.1152 10.2914 21.0381 10.388 20.9409Z" fill="currentColor"></path>
                                </svg>
                                </span>
                                <!--end::Svg Icon-->
                                <span class="ms-3 text-gray-700 parent-hover-primary fs-4 fw-bold">
                                Status
                                </span>
                                <div class="d-flex flex-column align-items-end ms-2">
                                    <span class="badge badge-sm badge-circle badge-light-danger">2</span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 mb-3">
                <div class="mb-4 card border-warning shadow-sm">
                    <div class="card-header" id="card-header">
                        <h3 class="card-title">
                            <span class="fw-bold fs-2 text-white">Katalog Layanan</span>
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row mb-10">
                            <div class="col-lg-3 col-md-3">
                                <div class="text-center card border-warning shadow-sm">
                                    <div class="card-header" id="card-header" style="border-bottom: 5px solid #ffc107">
                                        <h3 class="card-title">
                                            <span class="fw-bold fs-2 text-white">Pribadi</span>
                                        </h3>
                                    </div>
                                    <div class="list-group list-group-flush">
                                        <a href="#link2" data-rb-event-key="#link2" class="list-group-item list-group-item-action">Update Data Pokok Pegawai
                                        </a>
                                        <a href="{{ url('cuti/administrasi?string='.$user['pegawaiId']) }}" data-rb-event-key="#link3" class="list-group-item list-group-item-action">Pengajuan Cuti
                                        </a>
                                        <a href="#link4" data-rb-event-key="#link4" class="list-group-item list-group-item-action">Monitoring Presensi</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="text-center card border-warning shadow-sm">
                                    <div class="card-header" id="card-header" style="border-bottom: 5px solid #ffc107">
                                        <h3 class="card-title">
                                            <span class="fw-bold fs-2 text-white">Favourite</span>
                                        </h3>
                                    </div>
                                    <div class="list-group list-group-flush"><a href="/layananmandiri">
                                            <button class="list-group-item list-group-item-action">Layanan Mandiri
                                            </button>
                                        </a><a href="#link2" data-rb-event-key="#link2" class="list-group-item list-group-item-action">Control Panel UPK</a><a href="#link3" data-rb-event-key="#link3" class="list-group-item list-group-item-action">Pengajuan
                                            Cuti</a><a href="#link4" data-rb-event-key="#link4" class="list-group-item list-group-item-action">Monitoring
                                            Presensi</a></div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="text-center card border-warning shadow-sm">
                                    <div class="card-header" id="card-header" style="border-bottom: 5px solid #ffc107">
                                        <h3 class="card-title">
                                            <span class="fw-bold fs-2 text-white">Favourite</span>
                                        </h3>
                                    </div>
                                    <div class="list-group list-group-flush"><a href="/layananmandiri">
                                            <button class="list-group-item list-group-item-action">Layanan Mandiri
                                            </button>
                                        </a><a href="#link2" data-rb-event-key="#link2" class="list-group-item list-group-item-action">Control Panel UPK</a><a href="#link3" data-rb-event-key="#link3" class="list-group-item list-group-item-action">Pengajuan
                                            Cuti</a><a href="#link4" data-rb-event-key="#link4" class="list-group-item list-group-item-action">Monitoring
                                            Presensi</a></div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="text-center card border-warning shadow-sm">
                                    <div class="card-header" id="card-header" style="border-bottom: 5px solid #ffc107">
                                        <h3 class="card-title">
                                            <span class="fw-bold fs-2 text-white">Favourite</span>
                                        </h3>
                                    </div>
                                    <div class="list-group list-group-flush"><a href="/layananmandiri">
                                            <button class="list-group-item list-group-item-action">Layanan Mandiri
                                            </button>
                                        </a><a href="#link2" data-rb-event-key="#link2" class="list-group-item list-group-item-action">Control Panel UPK</a><a href="#link3" data-rb-event-key="#link3" class="list-group-item list-group-item-action">Pengajuan
                                            Cuti</a><a href="#link4" data-rb-event-key="#link4" class="list-group-item list-group-item-action">Monitoring
                                            Presensi</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-10">
                            <div class="col-lg-3 col-md-3">
                                <div class="text-center card border-warning shadow-sm">
                                    <div class="card-header" id="card-header" style="border-bottom: 5px solid #ffc107">
                                        <h3 class="card-title">
                                            <span class="fw-bold fs-2 text-white">Favourite</span>
                                        </h3>
                                    </div>
                                    <div class="list-group list-group-flush"><a href="/layananmandiri">
                                            <button class="list-group-item list-group-item-action">Layanan Mandiri
                                            </button>
                                        </a><a href="#link2" data-rb-event-key="#link2" class="list-group-item list-group-item-action">Control Panel UPK</a><a href="#link3" data-rb-event-key="#link3" class="list-group-item list-group-item-action">Pengajuan
                                            Cuti</a><a href="#link4" data-rb-event-key="#link4" class="list-group-item list-group-item-action">Monitoring
                                            Presensi</a></div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="text-center card border-warning shadow-sm">
                                    <div class="card-header" id="card-header" style="border-bottom: 5px solid #ffc107">
                                        <h3 class="card-title">
                                            <span class="fw-bold fs-2 text-white">Favourite</span>
                                        </h3>
                                    </div>
                                    <div class="list-group list-group-flush"><a href="/layananmandiri">
                                            <button class="list-group-item list-group-item-action">Layanan Mandiri
                                            </button>
                                        </a><a href="#link2" data-rb-event-key="#link2" class="list-group-item list-group-item-action">Control Panel UPK</a><a href="#link3" data-rb-event-key="#link3" class="list-group-item list-group-item-action">Pengajuan
                                            Cuti</a><a href="#link4" data-rb-event-key="#link4" class="list-group-item list-group-item-action">Monitoring
                                            Presensi</a></div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="text-center card border-warning shadow-sm">
                                    <div class="card-header" id="card-header" style="border-bottom: 5px solid #ffc107">
                                        <h3 class="card-title">
                                            <span class="fw-bold fs-2 text-white">Favourite</span>
                                        </h3>
                                    </div>
                                    <div class="list-group list-group-flush"><a href="/layananmandiri">
                                            <button class="list-group-item list-group-item-action">Layanan Mandiri
                                            </button>
                                        </a><a href="#link2" data-rb-event-key="#link2" class="list-group-item list-group-item-action">Control Panel UPK</a><a href="#link3" data-rb-event-key="#link3" class="list-group-item list-group-item-action">Pengajuan
                                            Cuti</a><a href="#link4" data-rb-event-key="#link4" class="list-group-item list-group-item-action">Monitoring
                                            Presensi</a></div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="text-center card border-warning shadow-sm">
                                    <div class="card-header" id="card-header" style="border-bottom: 5px solid #ffc107">
                                        <h3 class="card-title">
                                            <span class="fw-bold fs-2 text-white">Favourite</span>
                                        </h3>
                                    </div>
                                    <div class="list-group list-group-flush"><a href="/layananmandiri">
                                            <button class="list-group-item list-group-item-action">Layanan Mandiri
                                            </button>
                                        </a><a href="#link2" data-rb-event-key="#link2" class="list-group-item list-group-item-action">Control Panel UPK</a><a href="#link3" data-rb-event-key="#link3" class="list-group-item list-group-item-action">Pengajuan
                                            Cuti</a><a href="#link4" data-rb-event-key="#link4" class="list-group-item list-group-item-action">Monitoring
                                            Presensi</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="notice d-flex bg-light-primary rounded border-primary border border-dashed  p-6 mb-3">
                            <!--begin::Icon-->
                            <!--begin::Svg Icon | path: icons/duotune/general/gen044.svg-->
                            <span class="svg-icon svg-icon-2tx svg-icon-primary me-4"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor"></rect>
<rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="currentColor"></rect>
<rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="currentColor"></rect>
</svg>
</span>
                            <!--end::Svg Icon-->        <!--end::Icon-->

                            <!--begin::Wrapper-->
                            <div class="d-flex flex-stack flex-grow-1 ">
                                <!--begin::Content-->
                                <div class=" fw-semibold">
                                    <h4 class="text-gray-900 fw-bold">We need your attention!</h4>

                                    <div class="fs-6 text-gray-700 ">Your payment was declined. To start using tools, please <a class="fw-bold" href="/metronic8/demo1/../demo1/account/billing.html">Add Payment Method</a>.</div>
                                </div>
                                <!--end::Content-->

                            </div>
                            <!--end::Wrapper-->
                        </div>
                    </div>

                </div>

            </div>


        </div>
    </div>



@endsection

@section('js')
    <script>
        var processs = function(search) {
            var timeout = setTimeout(function() {
                var number = KTUtil.getRandomInt(1, 6);

                // Hide recently viewed
                suggestionsElement.classList.add("d-none");

                if (number === 3) {
                    // Hide results
                    resultsElement.classList.add("d-none");
                    // Show empty message
                    emptyElement.classList.remove("d-none");
                } else {
                    // Show results
                    resultsElement.classList.remove("d-none");
                    // Hide empty message
                    emptyElement.classList.add("d-none");
                }

                // Complete search
                search.complete();
            }, 1500);
        }

        var clear = function(search) {
            // Show recently viewed
            suggestionsElement.classList.remove("d-none");
            // Hide results
            resultsElement.classList.add("d-none");
            // Hide empty message
            emptyElement.classList.add("d-none");
        }

        // Input handler
        const handleInput = () => {
            // Select input field
            const inputField = element.querySelector("[data-kt-search-element=input]")

            // Handle keyboard press event
            inputField.addEventListener("keydown", e => {
                // Only apply action to Enter key press
                if(e.key === "Enter"){
                    e.preventDefault(); // Stop form from submitting
                }
            });
        }

        // Elements
        element = document.querySelector('#kt_docs_search_handler_basic');

        // if (!element) {
        //     return;
        // }

        wrapperElement = element.querySelector("[data-kt-search-element=wrapper]");
        suggestionsElement = element.querySelector("[data-kt-search-element=suggestions]");
        resultsElement = element.querySelector("[data-kt-search-element=results]");
        emptyElement = element.querySelector("[data-kt-search-element=empty]");

        // Initialize search handler
        searchObject = new KTSearch(element);

        // Search handler
        searchObject.on("kt.search.process", processs);

        // Clear handler
        searchObject.on("kt.search.clear", clear);

        // Handle select
        KTUtil.on(element, "[data-kt-search-element=customer]", "click", function() {
            //modal.hide();
        });

        // Handle input enter keypress
        handleInput();
    </script>
@endsection
