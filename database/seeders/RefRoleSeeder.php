<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RefRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('r_role')->insert(
            [
                [
                    'id'             => 1,
                    'role'           => 'ROLE_USER',
                    'keterangan'     => 'User',
                    'aktif'          => 1,
                    'created_at'     => null,
                    'updated_at'     => null,
                    'bisa_otorisasi' => null
                ],
                [
                    'id'             => 2,
                    'role'           => 'ROLE_SUPER_ADMIN',
                    'keterangan'     => 'Super Admin',
                    'aktif'          => 1,
                    'created_at'     => null,
                    'updated_at'     => null,
                    'bisa_otorisasi' => null
                ],
                [
                    'id'             => 3,
                    'role'           => 'ROLE_UPK_PUSAT',
                    'keterangan'     => 'UPK Pusat',
                    'aktif'          => 1,
                    'created_at'     => null,
                    'updated_at'     => null,
                    'bisa_otorisasi' => 1
                ],
                [
                    'id'             => 4,
                    'role'           => 'ROLE_INACTIVE',
                    'keterangan'     => 'Inactive',
                    'aktif'          => 1,
                    'created_at'     => null,
                    'updated_at'     => null,
                    'bisa_otorisasi' => null
                ],
                [
                    'id'             => 5,
                    'role'           => 'ROLE_RETIRED',
                    'keterangan'     => 'Retired',
                    'aktif'          => 1,
                    'created_at'     => null,
                    'updated_at'     => null,
                    'bisa_otorisasi' => null
                ],
                [
                    'id'             => 6,
                    'role'           => 'ROLE_ADMIN',
                    'keterangan'     => 'Admin',
                    'aktif'          => 1,
                    'created_at'     => null,
                    'updated_at'     => null,
                    'bisa_otorisasi' => 1
                ],
                [
                    'id'             => 7,
                    'role'           => 'ROLE_PEMBUATSURAT_SISUKA',
                    'keterangan'     => 'null',
                    'aktif'          => 1,
                    'created_at'     => null,
                    'updated_at'     => null,
                    'bisa_otorisasi' => null
                ],
                [
                    'id'             => 8,
                    'role'           => 'ROLE_PENGAWASAN',
                    'keterangan'     => 'Pengawasan',
                    'aktif'          => 1,
                    'created_at'     => null,
                    'updated_at'     => null,
                    'bisa_otorisasi' => null
                ],
                [
                    'id'             => 9,
                    'role'           => 'ROLE_UPK_LOKAL',
                    'keterangan'     => 'UPK Lokal',
                    'aktif'          => 1,
                    'created_at'     => null,
                    'updated_at'     => null,
                    'bisa_otorisasi' => 1
                ],
                [
                    'id'             => 10,
                    'role'           => 'ROLE_UPK_WILAYAH',
                    'keterangan'     => 'UPK Wilayah',
                    'aktif'          => 1,
                    'created_at'     => null,
                    'updated_at'     => null,
                    'bisa_otorisasi' => 1
                ],
                [
                    'id'             => 12,
                    'role'           => 'ROLE_PELAKSANA_KEPANGKATAN',
                    'keterangan'     => 'Pelaksana Kepangkatan',
                    'aktif'          => 1,
                    'created_at'     => null,
                    'updated_at'     => null,
                    'bisa_otorisasi' => null
                ],
                [
                    'id'             => 11,
                    'role'           => 'ROLE_KASUBBAG_KEPANGKATAN',
                    'keterangan'     => 'Kabusag Kepangkatan',
                    'aktif'          => 1,
                    'created_at'     => null,
                    'updated_at'     => null,
                    'bisa_otorisasi' => 1
                ],
                [
                    'id'             => 13,
                    'role'           => 'ROLE_KASUBBAG_PEMBERHENTIAN_PEGAWAI',
                    'keterangan'     => 'Kasubbag PDPP',
                    'aktif'          => 1,
                    'created_at'     => null,
                    'updated_at'     => null,
                    'bisa_otorisasi' => null
                ],
                [
                    'id'             => 14,
                    'role'           => 'ROLE_PELAKSANA_SUBBAG_PEMBERHENTIAN',
                    'keterangan'     => 'pelaksana_pdpp',
                    'aktif'          => 1,
                    'created_at'     => null,
                    'updated_at'     => null,
                    'bisa_otorisasi' => null
                ],
                [
                    'id'             => 15,
                    'role'           => 'ROLE_KEPALA_KANWIL',
                    'keterangan'     => 'kepala_kanwil_all',
                    'aktif'          => 1,
                    'created_at'     => null,
                    'updated_at'     => null,
                    'bisa_otorisasi' => null
                ],
                [
                    'id'             => 16,
                    'role'           => 'ROLE_KEPALA_KPP',
                    'keterangan'     => 'kepala_kpp',
                    'aktif'          => 1,
                    'created_at'     => null,
                    'updated_at'     => null,
                    'bisa_otorisasi' => null
                ]
            ]
        );
    }
}
