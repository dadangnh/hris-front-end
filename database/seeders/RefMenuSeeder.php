<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RefMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('r_menu')->insert(
            [
                [
                    'id'         => 4,
                    'nm_menu'    => 'Monitoring Pegawai Tubel',
                    'route'      => 'tubel/monitoring',
                    'aktif'      => true,
                    'sub_dari'   => 2,
                    'icon'       => null,
                    'no_urut'    => 2,
                    'breadcumbs' => '["2", "4"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 43,
                    'nm_menu'    => 'Lapor Diri & Hasil Studi',
                    'route'      => null,
                    'aktif'      => true,
                    'sub_dari'   => 2,
                    'icon'       => null,
                    'no_urut'    => 13,
                    'breadcumbs' => '["2","43"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 51,
                    'nm_menu'    => 'Kelola Referensi',
                    'route'      => null,
                    'aktif'      => true,
                    'sub_dari'   => 50,
                    'icon'       => null,
                    'no_urut'    => 10,
                    'breadcumbs' => '["50","51"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 40,
                    'nm_menu'    => 'Administrasi',
                    'route'      => 'administrasi',
                    'aktif'      => true,
                    'sub_dari'   => 13,
                    'icon'       => null,
                    'no_urut'    => 3,
                    'breadcumbs' => '["13","40"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 52,
                    'nm_menu'    => 'Izin Pendidikan Anda',
                    'route'      => 'ibel/administrasi',
                    'aktif'      => true,
                    'sub_dari'   => 50,
                    'icon'       => null,
                    'no_urut'    => 1,
                    'breadcumbs' => '["50","52"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 65,
                    'nm_menu'    => 'Hasil Seleksi',
                    'route'      => 'ibel/hasil-seleksi',
                    'aktif'      => true,
                    'sub_dari'   => 50,
                    'icon'       => null,
                    'no_urut'    => 4,
                    'breadcumbs' => '["50","65"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 54,
                    'nm_menu'    => 'KEP Seleksi',
                    'route'      => 'ibel/kep-seleksi',
                    'aktif'      => true,
                    'sub_dari'   => 50,
                    'icon'       => null,
                    'no_urut'    => 6,
                    'breadcumbs' => '["50","54"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 57,
                    'nm_menu'    => 'Penerbitan Izin',
                    'route'      => 'ibel/penerbitan-izin',
                    'aktif'      => true,
                    'sub_dari'   => 50,
                    'icon'       => null,
                    'no_urut'    => 8,
                    'breadcumbs' => '["50","57"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 53,
                    'nm_menu'    => 'Pemeriksaan Permohonan',
                    'route'      => 'ibel/periksa',
                    'aktif'      => true,
                    'sub_dari'   => 50,
                    'icon'       => null,
                    'no_urut'    => 3,
                    'breadcumbs' => '["50","53"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 56,
                    'nm_menu'    => 'Permintaan Dokumen',
                    'route'      => 'ibel/permintaan-dokumen',
                    'aktif'      => true,
                    'sub_dari'   => 50,
                    'icon'       => null,
                    'no_urut'    => 2,
                    'breadcumbs' => '["50","56"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 55,
                    'nm_menu'    => 'Persetujuan Hasil Seleksi',
                    'route'      => 'ibel/persetujuan-hasil-seleksi',
                    'aktif'      => true,
                    'sub_dari'   => 50,
                    'icon'       => null,
                    'no_urut'    => 5,
                    'breadcumbs' => '["50","55"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 60,
                    'nm_menu'    => 'Referensi Lokasi Pendidikan',
                    'route'      => 'ibel/referensi/lokasi',
                    'aktif'      => true,
                    'sub_dari'   => 51,
                    'icon'       => null,
                    'no_urut'    => 3,
                    'breadcumbs' => '["50","51","60"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 61,
                    'nm_menu'    => 'Referensi Otorisasi Izin Belajar',
                    'route'      => 'ibel/referensi/otorisasi',
                    'aktif'      => true,
                    'sub_dari'   => 51,
                    'icon'       => null,
                    'no_urut'    => 4,
                    'breadcumbs' => '["50","51","61"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 59,
                    'nm_menu'    => 'Referensi Program Studi',
                    'route'      => 'ibel/referensi/prodi',
                    'aktif'      => true,
                    'sub_dari'   => 51,
                    'icon'       => null,
                    'no_urut'    => 2,
                    'breadcumbs' => '["50","51","59"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 62,
                    'nm_menu'    => 'Referensi Akreditasi Program Studi',
                    'route'      => 'ibel/referensi/prodi-akreditasi',
                    'aktif'      => true,
                    'sub_dari'   => 51,
                    'icon'       => null,
                    'no_urut'    => 5,
                    'breadcumbs' => '["50","51","62"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 58,
                    'nm_menu'    => 'Referensi Perguruan Tinggi',
                    'route'      => 'ibel/referensi/pt',
                    'aktif'      => true,
                    'sub_dari'   => 51,
                    'icon'       => null,
                    'no_urut'    => 1,
                    'breadcumbs' => '["50","51","58"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 63,
                    'nm_menu'    => 'Tim Otorisasi',
                    'route'      => 'ibel/referensi/tim-otorisasi',
                    'aktif'      => true,
                    'sub_dari'   => 51,
                    'icon'       => null,
                    'no_urut'    => 6,
                    'breadcumbs' => '["50","51","63"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 64,
                    'nm_menu'    => 'Tindak Lanjut',
                    'route'      => 'ibel/tindak-lanjut',
                    'aktif'      => true,
                    'sub_dari'   => 50,
                    'icon'       => null,
                    'no_urut'    => 9,
                    'breadcumbs' => '["50","64"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 49,
                    'nm_menu'    => 'Monitoring',
                    'route'      => 'monitoring',
                    'aktif'      => true,
                    'sub_dari'   => 13,
                    'icon'       => null,
                    'no_urut'    => 4,
                    'breadcumbs' => '["13","49"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 31,
                    'nm_menu'    => 'Penelitian',
                    'route'      => 'pengusulan-pensiun/penelitian',
                    'aktif'      => true,
                    'sub_dari'   => 29,
                    'icon'       => null,
                    'no_urut'    => 3,
                    'breadcumbs' => '["13","29","31"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 30,
                    'nm_menu'    => 'Pengusulan',
                    'route'      => 'pengusulan-pensiun/permohonan',
                    'aktif'      => true,
                    'sub_dari'   => 29,
                    'icon'       => null,
                    'no_urut'    => 1,
                    'breadcumbs' => '["13","29","30"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 38,
                    'nm_menu'    => 'Disposisi',
                    'route'      => 'pengusulan-pensiun/permohonan/disposisi',
                    'aktif'      => true,
                    'sub_dari'   => 29,
                    'icon'       => null,
                    'no_urut'    => 4,
                    'breadcumbs' => '["13","29","38"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 33,
                    'nm_menu'    => 'Pengusulan UPK',
                    'route'      => 'pengusulan-pensiun/permohonan/upk',
                    'aktif'      => true,
                    'sub_dari'   => 29,
                    'icon'       => null,
                    'no_urut'    => 2,
                    'breadcumbs' => '["13","29","33"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 37,
                    'nm_menu'    => 'Disposisi Permohonan',
                    'route'      => 'permohonan-masa-pensiun/disposisi',
                    'aktif'      => true,
                    'sub_dari'   => 14,
                    'icon'       => null,
                    'no_urut'    => 3,
                    'breadcumbs' => '["13","14","37"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 16,
                    'nm_menu'    => 'Permohonan',
                    'route'      => 'permohonan-masa-pensiun/permohonan',
                    'aktif'      => true,
                    'sub_dari'   => 14,
                    'icon'       => null,
                    'no_urut'    => 1,
                    'breadcumbs' => '["13","14","16"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 17,
                    'nm_menu'    => 'Review',
                    'route'      => 'permohonan-masa-pensiun/review',
                    'aktif'      => true,
                    'sub_dari'   => 14,
                    'icon'       => null,
                    'no_urut'    => 2,
                    'breadcumbs' => '["13","14","17"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 41,
                    'nm_menu'    => 'Kelola Referensi',
                    'route'      => 'tubel/referensi',
                    'aktif'      => true,
                    'sub_dari'   => 2,
                    'icon'       => null,
                    'no_urut'    => 21,
                    'breadcumbs' => '["2","41"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 42,
                    'nm_menu'    => 'Referensi',
                    'route'      => 'referensi/negara',
                    'aktif'      => FALSE,
                    'sub_dari'   => 41,
                    'icon'       => null,
                    'no_urut'    => 1,
                    'breadcumbs' => '["41","42"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 3,
                    'nm_menu'    => 'Administrasi',
                    'route'      => 'tubel/administrasi',
                    'aktif'      => true,
                    'sub_dari'   => 2,
                    'icon'       => null,
                    'no_urut'    => 1,
                    'breadcumbs' => '["2", "3"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 25,
                    'nm_menu'    => 'KEP Pembebasan',
                    'route'      => 'tubel/kep-pembebasan',
                    'aktif'      => true,
                    'sub_dari'   => 2,
                    'icon'       => null,
                    'no_urut'    => 9,
                    'breadcumbs' => '["2", "25"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 47,
                    'nm_menu'    => 'KEP Penempatan Tubel',
                    'route'      => 'tubel/kep-penempatan',
                    'aktif'      => true,
                    'sub_dari'   => 2,
                    'icon'       => null,
                    'no_urut'    => 14,
                    'breadcumbs' => '["2", "47"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 18,
                    'nm_menu'    => 'KEP Penempatan Cuti',
                    'route'      => 'tubel/kep-penempatan-cuti',
                    'aktif'      => true,
                    'sub_dari'   => 2,
                    'icon'       => null,
                    'no_urut'    => 6,
                    'breadcumbs' => '["2", "18"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 8,
                    'nm_menu'    => 'Laporan Perkembangan Studi',
                    'route'      => 'tubel/lps',
                    'aktif'      => true,
                    'sub_dari'   => 2,
                    'icon'       => null,
                    'no_urut'    => 3,
                    'breadcumbs' => '["2", "8"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 45,
                    'nm_menu'    => 'Hasil Akhir Studi',
                    'route'      => 'tubel/pelaporan/hasil-akhir',
                    'aktif'      => true,
                    'sub_dari'   => 43,
                    'icon'       => null,
                    'no_urut'    => 3,
                    'breadcumbs' => '["2","43","45"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 46,
                    'nm_menu'    => 'Berhenti Lain-lain',
                    'route'      => 'tubel/pelaporan/lain-lain',
                    'aktif'      => true,
                    'sub_dari'   => 43,
                    'icon'       => null,
                    'no_urut'    => 4,
                    'breadcumbs' => '["2","43","46"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 44,
                    'nm_menu'    => 'Lapor Diri',
                    'route'      => 'tubel/pelaporan/lapor-diri',
                    'aktif'      => true,
                    'sub_dari'   => 43,
                    'icon'       => null,
                    'no_urut'    => 1,
                    'breadcumbs' => '["2","43","44"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 48,
                    'nm_menu'    => 'Verifikasi Ijazah',
                    'route'      => 'tubel/pelaporan/verifikasi-ijazah',
                    'aktif'      => true,
                    'sub_dari'   => 43,
                    'icon'       => null,
                    'no_urut'    => 2,
                    'breadcumbs' => '["2","43","48"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 19,
                    'nm_menu'    => 'Pengaktifan Kembali',
                    'route'      => 'tubel/pengaktifan-kembali',
                    'aktif'      => true,
                    'sub_dari'   => 2,
                    'icon'       => null,
                    'no_urut'    => 7,
                    'breadcumbs' => '["2", "19"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 20,
                    'nm_menu'    => 'Perpanjangan Cuti',
                    'route'      => 'tubel/perpanjangan-cuti',
                    'aktif'      => true,
                    'sub_dari'   => 2,
                    'icon'       => null,
                    'no_urut'    => 5,
                    'breadcumbs' => '["2","20"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 26,
                    'nm_menu'    => 'Perpanjangan ST Non STAN',
                    'route'      => 'tubel/perpanjangan-st-nonstan',
                    'aktif'      => true,
                    'sub_dari'   => 2,
                    'icon'       => null,
                    'no_urut'    => 10,
                    'breadcumbs' => '["2", "26"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 27,
                    'nm_menu'    => 'Perpanjangan ST STAN',
                    'route'      => 'tubel/perpanjangan-st-stan',
                    'aktif'      => true,
                    'sub_dari'   => 2,
                    'icon'       => null,
                    'no_urut'    => 11,
                    'breadcumbs' => '["2", "27"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 35,
                    'nm_menu'    => 'Kelola Referensi',
                    'route'      => 'tubel/riset/kelola-referensi',
                    'aktif'      => true,
                    'sub_dari'   => 34,
                    'icon'       => null,
                    'no_urut'    => 1,
                    'breadcumbs' => '["2","34","35"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 36,
                    'nm_menu'    => 'Usulan Topik Riset',
                    'route'      => 'tubel/riset/usulan-topik-riset',
                    'aktif'      => true,
                    'sub_dari'   => 34,
                    'icon'       => null,
                    'no_urut'    => 2,
                    'breadcumbs' => '["2","34","36"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 24,
                    'nm_menu'    => 'Surat Tugas Aktif Kembali',
                    'route'      => 'tubel/st-aktif-kembali',
                    'aktif'      => true,
                    'sub_dari'   => 2,
                    'icon'       => null,
                    'no_urut'    => 8,
                    'breadcumbs' => '["2", "24"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 32,
                    'nm_menu'    => 'Tes Menu',
                    'route'      => 'tubel/tesMenu',
                    'aktif'      => FALSE,
                    'sub_dari'   => 2,
                    'icon'       => null,
                    'no_urut'    => 20,
                    'breadcumbs' => '["2","32"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 34,
                    'nm_menu'    => 'Topik Riset',
                    'route'      => null,
                    'aktif'      => true,
                    'sub_dari'   => 2,
                    'icon'       => 'fa fa-book',
                    'no_urut'    => 12,
                    'breadcumbs' => '["2","34"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 15,
                    'nm_menu'    => 'Cuti Belajar',
                    'route'      => 'tubel/cuti',
                    'aktif'      => true,
                    'sub_dari'   => 2,
                    'icon'       => 'fa fa-book',
                    'no_urut'    => 4,
                    'breadcumbs' => '["2", "15"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 14,
                    'nm_menu'    => 'Permohonan Masa Persiapan Pensiun',
                    'route'      => null,
                    'aktif'      => true,
                    'sub_dari'   => 13,
                    'icon'       => 'fa fa-business-time',
                    'no_urut'    => 1,
                    'breadcumbs' => null,
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 29,
                    'nm_menu'    => 'Pengusulan Permohonan Pensiun',
                    'route'      => null,
                    'aktif'      => true,
                    'sub_dari'   => 13,
                    'icon'       => 'fa fa-business-time',
                    'no_urut'    => 2,
                    'breadcumbs' => null,
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 13,
                    'nm_menu'    => 'Pengelolaan Pensiun',
                    'route'      => null,
                    'aktif'      => true,
                    'sub_dari'   => 0,
                    'icon'       => 'fa fa-business-time',
                    'no_urut'    => 3,
                    'breadcumbs' => null,
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 2,
                    'nm_menu'    => 'Pegawai Tugas Belajar',
                    'route'      => null,
                    'aktif'      => true,
                    'sub_dari'   => 0,
                    'icon'       => 'fa fa-graduation-cap',
                    'no_urut'    => 2,
                    'breadcumbs' => null,
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 50,
                    'nm_menu'    => 'Izin Pendidikan',
                    'route'      => null,
                    'aktif'      => true,
                    'sub_dari'   => 0,
                    'icon'       => 'fa fa-graduation-cap',
                    'no_urut'    => 5,
                    'breadcumbs' => null,
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 1,
                    'nm_menu'    => 'Beranda',
                    'route'      => 'beranda',
                    'aktif'      => true,
                    'sub_dari'   => 0,
                    'icon'       => 'fa fa-home',
                    'no_urut'    => 1,
                    'breadcumbs' => '["1"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 66,
                    'nm_menu'    => 'Pengelolaan Unsur Pembentuk Presensi',
                    'route'      => null,
                    'aktif'      => true,
                    'sub_dari'   => 0,
                    'icon'       => 'fa fa-graduation-cap',
                    'no_urut'    => 6,
                    'breadcumbs' => null,
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 67,
                    'nm_menu'    => 'Pengelolaan Hari Libur',
                    'route'      => null,
                    'aktif'      => true,
                    'sub_dari'   => 66,
                    'icon'       => 'fa fa-graduation-cap',
                    'no_urut'    => 1,
                    'breadcumbs' => '["66","67"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 68,
                    'nm_menu'    => 'Permohonan Hari Libur',
                    'route'      => 'harilibur/administrasi',
                    'aktif'      => true,
                    'sub_dari'   => 67,
                    'icon'       => null,
                    'no_urut'    => 1,
                    'breadcumbs' => '["66","67","68"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 69,
                    'nm_menu'    => 'Persetujuan Hari Libur',
                    'route'      => 'harilibur/persetujuan',
                    'aktif'      => true,
                    'sub_dari'   => 67,
                    'icon'       => null,
                    'no_urut'    => 1,
                    'breadcumbs' => '["66","67","69"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 70,
                    'nm_menu'    => 'Monitoring Usulan',
                    'route'      => 'harilibur/monitoring',
                    'aktif'      => true,
                    'sub_dari'   => 67,
                    'icon'       => null,
                    'no_urut'    => 1,
                    'breadcumbs' => '["66","67","70"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 71,
                    'nm_menu'    => 'Pengelolaan Jam Kerja',
                    'route'      => null,
                    'aktif'      => true,
                    'sub_dari'   => 66,
                    'icon'       => 'fa fa-graduation-cap',
                    'no_urut'    => 1,
                    'breadcumbs' => '["66","71"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 72,
                    'nm_menu'    => 'Pengelolaan Jam Kerja Umum',
                    'route'      => 'jamkerjaumum/administrasi',
                    'aktif'      => true,
                    'sub_dari'   => 71,
                    'icon'       => null,
                    'no_urut'    => 1,
                    'breadcumbs' => '["66","71","72"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 73,
                    'nm_menu'    => 'Persetujuan Jam Kerja Umum',
                    'route'      => 'jamkerjaumum/persetujuan',
                    'aktif'      => true,
                    'sub_dari'   => 71,
                    'icon'       => null,
                    'no_urut'    => 1,
                    'breadcumbs' => '["66","71","73"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 74,
                    'nm_menu'    => 'Pengelolaan Jam Kerja (Cuti)',
                    'route'      => 'jamkerjacuti/administrasi',
                    'aktif'      => true,
                    'sub_dari'   => 71,
                    'icon'       => null,
                    'no_urut'    => 3,
                    'breadcumbs' => '["66","71","74"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 75,
                    'nm_menu'    => 'Persetujuan Permohonan Jam Kerja (cuti)',
                    'route'      => 'jamkerjacuti/persetujuan',
                    'aktif'      => true,
                    'sub_dari'   => 71,
                    'icon'       => null,
                    'no_urut'    => 4,
                    'breadcumbs' => '["66","71","75"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 76,
                    'nm_menu'    => 'Pengelolaan Cuti',
                    'route'      => null,
                    'aktif'      => true,
                    'sub_dari'   => 66,
                    'icon'       => 'fa fa-graduation-cap',
                    'no_urut'    => 1,
                    'breadcumbs' => '["66","76"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 77,
                    'nm_menu'    => 'Pengelolaan Cuti Bersama',
                    'route'      => 'cutibersama/administrasi',
                    'aktif'      => true,
                    'sub_dari'   => 76,
                    'icon'       => null,
                    'no_urut'    => 1,
                    'breadcumbs' => '["66","76","77"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 78,
                    'nm_menu'    => 'Persetujuan Cuti Bersama',
                    'route'      => 'cutibersama/persetujuan',
                    'aktif'      => true,
                    'sub_dari'   => 76,
                    'icon'       => null,
                    'no_urut'    => 1,
                    'breadcumbs' => '["66","76","78"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 79,
                    'nm_menu'    => 'Pengelolaan Cuti Pegawai',
                    'route'      => 'cuti/administrasi',
                    'aktif'      => false,
                    'sub_dari'   => 76,
                    'icon'       => null,
                    'no_urut'    => 1,
                    'breadcumbs' => '["66","76","79"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 80,
                    'nm_menu'    => 'Persetujuan Cuti Pegawai',
                    'route'      => 'cuti/persetujuan',
                    'aktif'      => true,
                    'sub_dari'   => 76,
                    'icon'       => null,
                    'no_urut'    => 1,
                    'breadcumbs' => '["66","76","80"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 81,
                    'nm_menu'    => 'Persetujuan Cuti Pegawai (UPK)',
                    'route'      => 'cuti/persetujuan/upk',
                    'aktif'      => true,
                    'sub_dari'   => 76,
                    'icon'       => null,
                    'no_urut'    => 1,
                    'breadcumbs' => '["66","76","81"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 82,
                    'nm_menu'    => 'Pengelolaan (UPK)',
                    'route'      => 'cuti/pengelolaan',
                    'aktif'      => true,
                    'sub_dari'   => 76,
                    'icon'       => null,
                    'no_urut'    => 1,
                    'breadcumbs' => '["66","76","82"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 83,
                    'nm_menu'    => 'Pengelolaan (UPK)',
                    'route'      => 'cuti/pengelolaan',
                    'aktif'      => true,
                    'sub_dari'   => 76,
                    'icon'       => null,
                    'no_urut'    => 1,
                    'breadcumbs' => '["66","76","82"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 84,
                    'nm_menu'    => 'UK3TSP',
                    'route'      => null,
                    'aktif'      => true,
                    'sub_dari'   => 0,
                    'icon'       => 'fa fa-home',
                    'no_urut'    => 7,
                    'breadcumbs' => null,
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 85,
                    'nm_menu'    => 'Permohonan UK3TSP',
                    'route'      => 'uk3tsp/administrasi',
                    'aktif'      => true,
                    'sub_dari'   => 84,
                    'icon'       => null,
                    'no_urut'    => 1,
                    'breadcumbs' => '["84","85"]',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id'         => 86,
                    'nm_menu'    => 'Persetujuan UK3TSP',
                    'route'      => 'uk3tsp/persetujuan',
                    'aktif'      => true,
                    'sub_dari'   => 84,
                    'icon'       => 0,
                    'no_urut'    => 2,
                    'breadcumbs' => '["84","86"]',
                    'created_at' => null,
                    'updated_at' => null
                ]
            ]
        );
    }
}
