<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RefRoleMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('r_role_menu')->insert(
            [
                [
                    'id_role' => 1,
                    'id_menu' => 1
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 2
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 53
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 2
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 53
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 54
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 54
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 34
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 34
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 13
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 14
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 16
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 17
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 21
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 21
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 22
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 22
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 23
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 23
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 28
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 29
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 30
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 31
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 32
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 32
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 33
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 38
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 40
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 42
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 42
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 49
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 50
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 50
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 41
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 41
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 4
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 4
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 3
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 3
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 8
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 8
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 15
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 15
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 18
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 18
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 19
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 19
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 20
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 20
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 24
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 24
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 25
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 25
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 26
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 26
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 27
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 27
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 35
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 35
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 36
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 36
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 44
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 44
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 45
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 45
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 46
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 46
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 47
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 47
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 48
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 48
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 3
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 52
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 51
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 51
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 56
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 56
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 13
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 14
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 29
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 40
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 49
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 16
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 17
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 37
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 30
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 31
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 33
                ],
                [
                    'id_role' => 2,
                    'id_menu' => 38
                ],
                [
                    'id_role' => 9,
                    'id_menu' => 13
                ],
                [
                    'id_role' => 9,
                    'id_menu' => 14
                ],
                [
                    'id_role' => 9,
                    'id_menu' => 29
                ],
                [
                    'id_role' => 9,
                    'id_menu' => 49
                ],
                [
                    'id_role' => 9,
                    'id_menu' => 17
                ],
                [
                    'id_role' => 9,
                    'id_menu' => 31
                ],
                [
                    'id_role' => 9,
                    'id_menu' => 33
                ],
                [
                    'id_role' => 10,
                    'id_menu' => 13
                ],
                [
                    'id_role' => 10,
                    'id_menu' => 14
                ],
                [
                    'id_role' => 10,
                    'id_menu' => 29
                ],
                [
                    'id_role' => 10,
                    'id_menu' => 40
                ],
                [
                    'id_role' => 10,
                    'id_menu' => 49
                ],
                [
                    'id_role' => 10,
                    'id_menu' => 17
                ],
                [
                    'id_role' => 10,
                    'id_menu' => 31
                ],
                [
                    'id_role' => 10,
                    'id_menu' => 33
                ],
                [
                    'id_role' => 13,
                    'id_menu' => 13
                ],
                [
                    'id_role' => 13,
                    'id_menu' => 14
                ],
                [
                    'id_role' => 13,
                    'id_menu' => 29
                ],
                [
                    'id_role' => 13,
                    'id_menu' => 49
                ],
                [
                    'id_role' => 13,
                    'id_menu' => 37
                ],
                [
                    'id_role' => 13,
                    'id_menu' => 38
                ],
                [
                    'id_role' => 14,
                    'id_menu' => 13
                ],
                [
                    'id_role' => 14,
                    'id_menu' => 14
                ],
                [
                    'id_role' => 14,
                    'id_menu' => 29
                ],
                [
                    'id_role' => 14,
                    'id_menu' => 17
                ],
                [
                    'id_role' => 14,
                    'id_menu' => 31
                ],
                [
                    'id_role' => 14,
                    'id_menu' => 49
                ],
                [
                    'id_role' => 15,
                    'id_menu' => 13
                ],
                [
                    'id_role' => 15,
                    'id_menu' => 14
                ],
                [
                    'id_role' => 15,
                    'id_menu' => 29
                ],
                [
                    'id_role' => 15,
                    'id_menu' => 17
                ],
                [
                    'id_role' => 15,
                    'id_menu' => 31
                ],
                [
                    'id_role' => 16,
                    'id_menu' => 13
                ],
                [
                    'id_role' => 16,
                    'id_menu' => 14
                ],
                [
                    'id_role' => 16,
                    'id_menu' => 29
                ],
                [
                    'id_role' => 16,
                    'id_menu' => 17
                ],
                [
                    'id_role' => 16,
                    'id_menu' => 31
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 13
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 14
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 29
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 16
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 30
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 57
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 57
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 55
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 55
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 58
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 58
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 59
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 59
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 60
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 60
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 61
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 61
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 62
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 62
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 63
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 63
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 64
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 64
                ],
                [
                    'id_role' => 11,
                    'id_menu' => 65
                ],
                [
                    'id_role' => 12,
                    'id_menu' => 65
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 55
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 61
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 51
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 66
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 67
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 68
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 69
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 70
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 71
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 72
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 73
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 74
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 75
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 76
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 77
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 78
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 79
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 80
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 81
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 82
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 83
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 84
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 85
                ],
                [
                    'id_role' => 1,
                    'id_menu' => 86
                ]
            ]
        );
    }
}
