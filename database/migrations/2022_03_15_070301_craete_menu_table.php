<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_menu', function (Blueprint $table) {
            $table->id();
            $table->string('nm_menu');
            $table->string('route')->nullable();
            $table->boolean('aktif')->nullable();
            $table->bigInteger('sub_dari');
            $table->string('icon')->nullable();
            $table->integer('no_urut');
            $table->json('breadcumbs')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_menu');
    }
};
