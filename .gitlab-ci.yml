image: php:8.2

stages:
  - build
  - test
  - deploy
  - review
  - dast
  - staging
  - canary
  - production
  - incremental rollout 10%
  - incremental rollout 25%
  - incremental rollout 50%
  - incremental rollout 100%
  - performance
  - cleanup
  - release

variables:
  POSTGRES_USER: db_user
  POSTGRES_PASSWORD: db_pass
  POSTGRES_DB: db_name
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""
  CONTAINER_TIMEOUT_SECONDS: 36000
  TIMEOUT_SECONDS: 36000
  ENGINE_MEMORY_LIMIT_BYTES: 10240000000

services:
  - name: postgres:15-alpine
    alias: database
  - name: redis:latest
    alias: redis

cache:
  paths:
    - vendor/
    - node_modules/

build_backend:
  image: docker:dind
  stage: build
  variables:
    IMAGE_TAG_FRONTEND: $CI_REGISTRY_IMAGE/app:$CI_COMMIT_REF_SLUG
    IMAGE_TAG_LATEST: $CI_REGISTRY_IMAGE/app:latest
    DOCKER_TLS_CERTDIR: ''
  services:
    - name: 'docker:20.10.21-dind'
      command: [ '--tls=false', '--host=tcp://0.0.0.0:2375' ]
      alias: docker
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build --no-cache -t frontend .
    - docker tag frontend $IMAGE_TAG_FRONTEND
    - docker tag frontend $IMAGE_TAG_LATEST
    - docker push $IMAGE_TAG_FRONTEND
    - docker push $IMAGE_TAG_LATEST

test:
  image: php:8.2
  variables:
    POSTGRES_USER: db_user
    POSTGRES_PASSWORD: db_pass
    POSTGRES_DB: db_name
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
    CONTAINER_TIMEOUT_SECONDS: 3600
  services:
    - name: postgres:15-alpine
      alias: database
    - name: redis:latest
      alias: redis
  cache:
    paths:
      - vendor/
      - node_modules/
  before_script:
    - apt update -yqq
    - apt install gnupg -yqq
    - curl -sL https://deb.nodesource.com/setup_18.x | bash -
    - apt install git nodejs libcurl4-gnutls-dev libicu-dev libmcrypt-dev libvpx-dev libjpeg-dev libpng-dev libxpm-dev zlib1g-dev libfreetype6-dev libxml2-dev libexpat1-dev libbz2-dev libgmp3-dev libldap2-dev unixodbc-dev libpq-dev libsqlite3-dev libaspell-dev libsnmp-dev libpcre3-dev libtidy-dev libonig-dev libzip-dev -yqq
    - docker-php-ext-install mbstring pdo_pgsql pdo_mysql curl intl gd xml zip bz2 opcache
    - pecl install xdebug
    - docker-php-ext-enable xdebug
    - curl -sS https://getcomposer.org/installer | php
    - php composer.phar install
    - npm install
    - cp .env.testing .env
    - npm run dev
    - php artisan key:generate
    - php artisan config:cache
    - php artisan migrate
    - php artisan db:seed
  script:
    - php vendor/bin/phpunit --coverage-text --colors=never

sast:
  stage: test

deploy_simulation:
  image: ubuntu:latest
  stage: staging
  before_script:
    - command -v ssh-agent || ( apt-get update -y && apt-get install openssh-client curl
      -y )
    - eval $(ssh-agent -s)
    - cat ${SSH_PRIVATE_KEY} | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
    - touch ~/.ssh/known_hosts
    - echo "$KNOWN_HOST" > ~/.ssh/known_hosts
  script:
    - 'curl --header "Job-Token: $CI_JOB_TOKEN" --data branch=master "${CI_API_V4_URL}/projects/$CI_PROJECT_ID/packages/composer"'
    - ssh $SERVER ". ~/.asdf/asdf.sh; sh deploy.sh; exit;"
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_SERVER_HOST == "gitlab.com"'
      allow_failure: true
  cache:
    key: cacheDeploy
    paths:
      - vendor/
      - node_modules/

deploy_to_registries:
  image: docker:dind
  stage: deploy
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    DOCKER_TLS_CERTDIR: ''
    DOCKER_BUILDKIT: 1
  services:
    - name: 'docker:20.10.21-dind'
      command: ['--tls=false', '--host=tcp://0.0.0.0:2375']
      alias: dockercompose
  before_script:
    - docker build . -t app:latest
  script:
    - docker login -u "$DOCKER_REGISTRY_USER" -p "$DOCKER_REGISTRY_PASSWORD"
    - docker image ls
    - docker tag app:latest dadang/hris_front_end:$CI_COMMIT_TAG
    - docker tag app:latest dadang/hris_front_end:latest
    - docker push dadang/hris_front_end:$CI_COMMIT_TAG
    - docker push dadang/hris_front_end:latest
    - docker logout
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker tag app:latest $CI_REGISTRY_IMAGE/hris_front_end:$CI_COMMIT_TAG
    - docker tag app:latest $CI_REGISTRY_IMAGE/hris_front_end:latest
    - docker tag app:latest $CI_REGISTRY_IMAGE/app:$CI_COMMIT_TAG
    - docker tag app:latest $CI_REGISTRY_IMAGE/app:latest
    - docker push $CI_REGISTRY_IMAGE/hris_front_end:$CI_COMMIT_TAG
    - docker push $CI_REGISTRY_IMAGE/hris_front_end:latest
    - docker push $CI_REGISTRY_IMAGE/app:$CI_COMMIT_TAG
    - docker push $CI_REGISTRY_IMAGE/app:latest
  rules:
    - if: $CI_COMMIT_TAG
      allow_failure: true
  cache:
    key: cacheBuildDocker
    paths:
      - node_modules/

browser_performance:
  variables:
    URL: "$DAST_WEBSITE"

release_job:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo 'running release_job'
  release:
    name: 'Release $CI_COMMIT_TAG'
    description: 'Created using the release-cli $EXTRA_DESCRIPTION'
    tag_name: '$CI_COMMIT_TAG'
    ref: '$CI_COMMIT_TAG'

include:
  - template: Jobs/Build.gitlab-ci.yml
  - template: Jobs/Test.gitlab-ci.yml
  - template: Jobs/Code-Quality.gitlab-ci.yml
  - template: Jobs/Code-Intelligence.gitlab-ci.yml
  - template: Jobs/DAST-Default-Branch-Deploy.gitlab-ci.yml
  - template: Verify/Browser-Performance.gitlab-ci.yml
  - template: Security/DAST.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml
