<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use function array_push;

class MenuModel extends Model
{
    use HasFactory;

    protected $table = "r_role_menu";

    public static function getMenu($role, $route = NULL)
    {

        # route
        if ($route == NULL) {
            $route = Route::current()->uri;
        }

        # prepare array menu
        $arrMenu = [];
        $arrMenuPost = [];

        # get role
        $role = "'" . implode("','", session()->get('login_info')['_role_list']) . "'";

        # statement
        $menu = DB::select("select
                                x.*,
                                y.breadcumbs
                            from
                                (select
                                    distinct b.id,
                                    c.role,
                                    b.nm_menu,
                                    b.route,
                                    b.icon,
                                    b.sub_dari,
                                    b.no_urut
                                from
                                    r_role_menu a
                                join r_menu b on
                                    b.id = a.id_menu
                                left join r_role c on
                                    a.id_role = c.id
                                where
                                    c.role in (" . $role . ")
                                    and b.aktif = '1') x
                            join r_menu y
                                on x.id = y.id
                            order by
                                x.no_urut asc");

        $currentMenu = self::select('r_menu.breadcumbs', 'r_menu.nm_menu', 'r_menu.id')
            ->join('r_menu', 'r_menu.id', '=', 'r_role_menu.id_menu')
            ->where('r_menu.route', '=', $route)
            ->first();

        $breadArr = json_decode($currentMenu->breadcumbs);

        $bread = self::distinct()
            ->select('r_menu.id', 'r_menu.nm_menu', 'r_menu.route')
            ->join('r_menu', 'r_menu.id', '=', 'r_role_menu.id_menu')
            ->whereIn('r_menu.id', $breadArr)
            ->orderBy('r_menu.id', 'asc')
            ->get();


        $breadcumbs = [];

        foreach ($bread as $value) {
            array_push($breadcumbs, ['menu' => $value['nm_menu'], 'route' => $value['route']]);
        }

        foreach ($menu as $menu) {
            if (!array_key_exists($menu->sub_dari, $arrMenu)) {
            }
            $arrMenu[$menu->sub_dari][$menu->id] = array($menu->id, $menu->nm_menu, $menu->route, $menu->icon, $menu->sub_dari, $menu->no_urut, $menu->breadcumbs);
        }

        $arrMenuPost['menu'] = $arrMenu[0];

        foreach ($arrMenuPost['menu'] as $key => $value) {
            if (array_key_exists($key, $arrMenu)) {
                $arrMenuPost['menu'][$key]['sub'] = $arrMenu[$key];

                foreach ($arrMenuPost['menu'][$key]['sub'] as $key2 => $value2) {
                    if (array_key_exists($key2, $arrMenu)) {
                        $arrMenuPost['menu'][$key]['sub'][$key2]['sub'] = $arrMenu[$key2];
                    } else {
                        $arrMenuPost['menu'][$key]['sub'][$key2]['sub'] = [];
                    }
                }
            } else {
                $arrMenuPost['menu'][$key]['sub'] = [];
            }
        }

        $arrMenuPost['current'] = $currentMenu->breadcumbs;
        $arrMenuPost['title'] = $currentMenu->nm_menu;
        $arrMenuPost['breadcumbs'] = $breadcumbs;

        return $arrMenuPost;
    }

    public static function searchMenu($route)
    {

        # count path
        $arrRoute = explode("/", $route);
        $c = count($arrRoute) - 1;
        $currRoute = '';

        # loop path
        for ($i = $c; $i >= 0; $i--) {

            for ($i2 = 0; $i2 <= $i; $i2++) {
                $currRoute .= $arrRoute[$i2] . '/';
            }

            $currRoute = substr($currRoute, 0, -1);
            $menu = DB::table('r_menu')->where([['route', $currRoute], ['aktif', '1']])->first();

            if ($menu !== NULL) {
                return $menu->id;
            }

            $currRoute = '';
        }

        return abort(404);
    }

    public static function MenuAccess($id_menu, $role)
    {

        $cek = self::whereIn('id_role', $role)
            ->where([
                ['id_menu', $id_menu],
            ])
            ->count();

        return $cek;
    }
}
