<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class MenuHelper
{

    // public static function getMenu($role, $route = NULL, $addBreadcumbs = []){
    public static function getMenuAccess($role = null)
    {

        //route
        // if($route == NULL){
        //     $route = Route::current()->uri;
        // }

        // // $menu = ApiHelper::post('menu/getmenu',['role' => $role, 'route' => $route, 'bread' => $addBreadcumbs]);
        // $menu = ApiHelper::post('menu/getmenu',['route' => $route, 'bread' => $addBreadcumbs]);

        // if($menu['status'] == 0){
        //     return abort(403);
        // }

        // dd($menu);

        // return $menu['data'];

        $arrMenu = [];
        $menu = DB::select("select
                                distinct b.id,
                                b.route,
                                b.no_urut
                            from
                                r_role_menu a
                            inner join r_menu b on
                                a.id_menu = b.id
                            where
                                a.id_role in (1)
                                and b.aktif = '1'
                                and b.route is not null
                            order by
                                b.no_urut");

        foreach ($menu as $menu) {
            $arrMenu[$menu->id] = $menu->route;
        }

        return $arrMenu;

    }

    public static function MenuAccess($route, $idRole)
    {

        $cek = ApiHelper::post('menu/menuaccess', ['route' => $route, 'id_role' => $idRole]);

        if ($cek['status'] == 0) {
            return abort(403);
        }

        return $cek['data'];

    }

    public static function CekMenuAccess($route, $arrMenu)
    {
        # count path
        $arrRoute = explode("/", $route);
        $c = count($arrRoute);
        $currRoute = '';
        $cc = 0;

        // dd($route);

        # loop path
        for ($i = $c; $i > 0; $i--) {

            // for ($i2=0; $i2 <= $i ; $i2++) {
            //     $currRoute .= $arrRoute[$i2].'/';
            // }

            $currRoute .= $cc != 0 ? '/' . $arrRoute[$cc] : $arrRoute[$cc];

            // $currRoute = substr($currRoute,0,-1);

            if (in_array($currRoute, $arrMenu))
                return true;

            $cc++;

            // $currRoute = '';
        }

        // dd($arrMenu);

        return false;
    }

}
