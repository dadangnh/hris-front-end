<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class MenuHelper
{
    public static function getMenuAccess($roleList = null)
    {
        #buat array kosong
        $arrMenu = [];

        # role list
        $role = "'" . implode("','", $roleList) . "'";

        #query menu
        $menu = DB::select("select
                                distinct b.id,
                                b.route,
                                b.no_urut,
                                c.role,
                                c.aktif
                            from
                                r_role_menu a
                            inner join r_menu b on
                                a.id_menu = b.id
                            left join r_role c
                                on a.id_role = c.id
                            where
                                c.role in (" . $role . ")
                                and b.aktif = '1'
                                and b.route is not null
                                and c.aktif = '1'
                            order by
                                b.no_urut");

        #looping data menu
        foreach ($menu as $menu) {
            $arrMenu[$menu->id] = $menu->route;
        }

        return $arrMenu;
    }

    public static function MenuAccess($route, $idRole)
    {
        #cek akses menu
        $cek = ApiHelper::post('menu/menuaccess', ['route' => $route, 'id_role' => $idRole]);

        #jika tidak ada akses
        if ($cek['status'] == 0) {
            return abort(403);
        }

        return $cek['data'];
    }

    public static function CekMenuAccess($route, $arrMenu)
    {
        # count path
        $arrRoute = explode("/", $route);
        $c = count($arrRoute);
        $currRoute = '';
        $cc = 0;

        # loop path
        for ($i = $c; $i > 0; $i--) {
            $currRoute .= $cc != 0 ? '/' . $arrRoute[$cc] : $arrRoute[$cc];

            if (in_array($currRoute, $arrMenu))
                return true;

            $cc++;
        }

        return false;
    }
}
