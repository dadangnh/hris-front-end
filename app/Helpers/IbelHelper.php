<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;

class IbelHelper
{

    public static function instance()
    {
        return new IbelHelper();
    }

    public static function getRef(string $route)
    {
        $request = Http::get(env('API_URL_SDM0365') . $route);

        # cek status servis
        if (!$request->successful()) {
            return ['status' => 0, 'message' => 'Error Web Service!'];
        }

        $body = $request->getBody();

        return json_decode($body);
    }

    public static function get(string $route)
    {
        # set token
        $token = session()->get('login_info')['_token'];

        # request headers
        $request = Http::accept('application/json')
            ->withOptions([
                'headers' => [
                    'Token-IAM' => $token,
                ]
            ])
            ->get(env('API_URL_SDM0365') . $route);

        # cek status servis
        if ("OK" != $request->getReasonPhrase()) {

            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $response = $request->getBody();

        return json_decode($response);
    }

    public static function upload($file, $size = 2097152, array $mime)
    {
        #get properti file
        $pathFile = $file->getRealPath();
        $namaFile = $file->getClientOriginalName();
        $mimeFile = $file->getMimeType();
        $ukuranFile = $file->getSize();

        #validasi properti file
        if (!in_array($mimeFile, $mime)) {
            return ['status' => 0, 'message' => 'Format file tidak sesuai'];
        } elseif ($ukuranFile > $size) {
            return ['status' => 0, 'message' => 'Ukuran file melebihi batas'];
        } else {
            #send file
            $response = Http::attach('file', file_get_contents($pathFile), $namaFile)
                ->put(env('API_URL_SDM0365') . '/files');

            #validasi error
            if (!$response->successful()) {
                $message = $response->body();

                $response_upload = ['status' => 0, 'message' => $message];
            } else {
                $response_upload = ['status' => 1, 'file' => $response->body()];
            }
        }

        return $response_upload;
    }

    public static function put(string $route, array $body)
    {
        $request = Http::put(env('API_URL_SDM0365') . $route, $body);

        if (!$request->successful()) {
            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function uploadWithToken($file, $size = 2097152, array $mime)
    {
        # set token
        $token = session()->get('login_info')['_token'];

        #get properti file
        $pathFile = $file->getRealPath();
        $namaFile = $file->getClientOriginalName();
        $mimeFile = $file->getMimeType();
        $ukuranFile = $file->getSize();

        #validasi properti file
        if (!in_array($mimeFile, $mime)) {
            return ['status' => 0, 'message' => 'Format file tidak sesuai'];
        } elseif ($ukuranFile > $size) {
            return ['status' => 0, 'message' => 'Ukuran file melebihi batas'];
        } else {
            #send file
            $response = Http::attach('file', file_get_contents($pathFile), $namaFile)
                ->withOptions([
                    'headers' => [
                        'Token-IAM' => $token,
                    ]
                ])->put(env('API_URL_SDM0365') . '/files');

            #validasi error
            if (!$response->successful()) {
                $message = $response->body();

                $response_upload = ['status' => 0, 'message' => $message];
            } else {
                $response_upload = ['status' => 1, 'file' => $response->body()];
            }
        }
        // dd($response);
        return $response_upload;
    }

    public static function deleteFile(string $route)
    {
        $request = Http::delete(env('API_URL_SDM0365') . $route);

        # cek status servis
        if (!$request->successful()) {
            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function delete(string $route, array $body)
    {
        $request = Http::delete(env('API_URL_SDM0365') . $route, $body);

        # cek status servis
        if (!$request->successful()) {
            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function cekToken($time)
    {
        if ($time >= now()->getTimestamp()) {

            #refresh token jika expired
            $response = Http::post(env('API_URL_IAM') . '/api/token/refresh', ['refresh_token' => session()->get('login_info')['_refresh_token']]);

            # cek status servis
            if (!$response->successful()) {

                $message = 'Error Web Services';

                return ['status' => 0, 'message' => $message];
            }

            $data = json_decode($response);

            return $data;
        } else {

            #refresh token jika tidak expired
            $data = session()->get('login_info')['_refresh_token'];

            return $data;
        }
    }

    public static function post(string $route, array $body)
    {
        $request = Http::post(env('API_URL_SDM0365') . $route, $body);

        # cek status servis
        if (!$request->successful()) {

            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function refreshToken($refreshtoken)
    {
        $response = Http::post(env('API_URL_IAM') . '/api/token/refresh', ['refresh_token' => $refreshtoken]);

        # cek status servis
        if (!$response->successful()) {

            $message = 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $data = json_decode($response);

        return ['status' => 1, 'data' => $data];
    }

    public static function getNewSession($username)
    {
        $data = Http::accept('application/json')->post(env('API_URL_IAM') . 'authentication', [
            'username' => $username,
            'password' => 'Pajak123',
        ]);

        $body = $data->getBody();
        $authiam['data'] = json_decode($body);
        $token = $authiam['data']->token;
        // dd($token);

        return $token;
    }

    public static function generateBody($user, $isi = null)
    {
        $arrpeg = [
            'pegawaiId' => $user['pegawaiId'],
            'namaPegawai' => $user['namaPegawai'],
            'nip18' => $user['nip18'],
            'pangkatId' => $user['pangkatId'],
            'namaPangkat' => $user['pangkat'],
            'jabatanId' => $user['jabatanId'],
            'namaJabatan' => $user['jabatan'],
            'unitOrgId' => $user['pangkatId'],
            'namaUnitOrg' => $user['unit'],
            'kantorId' => $user['kantorId'],
            'namaKantor' => $user['kantor'],
            'pegawaiInputId' => $user['pegawaiId']
        ];
        if ($isi != null) {
            $arrisi = $isi;
        } else {
            $arrisi = [];
        }
        $body = array_merge($arrpeg, $arrisi);

        return $body;
    }

    public static function detectDelimiter($file)
    {
        $delimiters = [";" => 0, "," => 0, "\t" => 0, "|" => 0];

        $handle = fopen($file, "r");
        $firstLine = fgets($handle);
        fclose($handle);
        foreach ($delimiters as $delimiter => &$count) {
            $count = count(str_getcsv($firstLine, $delimiter));
        }

        if (array_sum($delimiters) <= count($delimiters)) {
            return false;
        } else {
            return array_search(max($delimiters), $delimiters);
        }
    }

    public static function csvToArray($tmp_file, $delimiter)
    {
        $header = null;
        $data = array();
        if (($handle = fopen($tmp_file, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }

    public static function getWithToken(string $route)
    {

        # set token
        $token = session()->get('login_info')['_token'];

        # request headers
        $request = Http::accept('application/json')
            ->withOptions([
                'headers' => [
                    'Token-IAM' => $token,
                ]
            ])
            ->get(env('API_URL_SDM0365') . $route);

        # cek status servis
        if ("OK" != $request->getReasonPhrase()) {

            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        return (array) json_decode($body);
    }

    public static function postWithToken(string $route, array $body = null)
    {
        # set token
        $token = session()->get('login_info')['_token'];

        $request = Http::accept('application/json')
            ->withOptions([
                'headers' => [
                    'Token-IAM' => $token,
                ]
            ])
            ->post(env('API_URL_SDM0365') . $route, $body);

        # cek status servis
        if ("OK" != $request->getReasonPhrase() && "Created" != $request->getReasonPhrase()) {

            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function patchWithToken(string $route, array $body)
    {
        # set token
        $token = session()->get('login_info')['_token'];

        $request = Http::accept('application/json')
            ->withOptions([
                'headers' => [
                    'Token-IAM' => $token,
                ]
            ])->patch(env('API_URL_SDM0365') . $route, $body);

        if ("OK" != $request->getReasonPhrase() && "Created" != $request->getReasonPhrase()) {

            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    #function patch with token

    public static function patch(string $route, array $body)
    {
        $request = Http::patch(env('API_URL_SDM0365') . $route, $body);

        if (!$request->successful()) {
            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    #function delete with token

    public static function deleteWithToken(string $route, array $body)
    {
        # set token
        $token = session()->get('login_info')['_token'];

        $request = Http::accept('application/json')
            ->withOptions([
                'headers' => [
                    'Token-IAM' => $token,
                ]
            ])->delete(env('API_URL_SDM0365') . $route, $body);

        // dd($request->getReasonPhrase());
        # cek status servis
        if ("OK" != $request->getReasonPhrase()) {

            $x = json_decode($request->getBody());
            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    // public static function generateFile(String $route)
    // {
    //     # set token
    //     $token = session()->get('login_info')['_token'];

    //     # get data
    //     $response =  Http::withHeaders([
    //         // 'accept' =>  'application/octet-stream',
    //         'Token-IAM' =>  $token,
    //     ])
    //         ->post(env('API_URL_SDM0365') . $route);

    //     if ("OK" != $response->getReasonPhrase() && "Created" != $response->getReasonPhrase()) {

    //         $x = json_decode($response->getBody());

    //         $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

    //         return ['status' => 0, 'message' => $message];
    //     }

    //     #get file name
    //     $content_dispotition = explode(';', $response->headers()['Content-Disposition'][0]);
    //     $file_name_raw = explode('=', $content_dispotition[1]);
    //     $file_name = str_replace('"', "", $file_name_raw[1]);

    //     # get content type
    //     $content_type = $response->headers()['Content-Type'][0];

    //     # get content length
    //     $content_length = $response->headers()['Content-Length'][0];

    //     $body = [
    //         'file' => $response,
    //         'filename' => $file_name,
    //         'type' => $content_type,
    //         'size' => $content_length
    //     ];

    //     return ['status' => 1, 'data' => $body];
    // }

    //   public static function uploadWithToken($file, $size = 2097152, array $mime)
    // {

    //     # set token
    //     $token = session()->get('login_info')['_token'];
    //     #get properti file
    //     $pathFile = $file->getRealPath();
    //     $namaFile = $file->getClientOriginalName();
    //     $mimeFile = $file->getMimeType();
    //     $ukuranFile = $file->getSize();

    //     #validasi properti file
    //     if (!in_array($mimeFile, $mime)) {
    //         return ['status' => 0, 'message' => 'Format file tidak sesuai'];
    //     } elseif ($ukuranFile > $size) {
    //         return ['status' => 0, 'message' => 'Ukuran file melebihi batas'];
    //     } else {
    //         #send file
    //         $response = Http::attach('file', file_get_contents($pathFile), $namaFile)
    //         ->withOptions([
    //             'headers'   => [
    //                 'Token-IAM' =>  $token,
    //             ]
    //         ])->put(env('API_URL') . '/files');

    //         #validasi error
    //         if (!$response->successful()) {
    //             $message = $response->body();

    //             $response_upload = ['status' => 0, 'message' => $message];
    //         } else {
    //             $response_upload = ['status' => 1, 'file' => $response->body()];
    //         }
    //     }

    //     return $response_upload;
    // }

    public static function generateFileWithToken(string $route)
    {
        # set token
        $token = session()->get('login_info')['_token'];

        # get data
        $response = Http::withHeaders([
            // 'accept' =>  'application/octet-stream',
            'Token-IAM' => $token,
        ])
            ->post(env('API_URL_SDM0365') . $route);

        if ("OK" != $response->getReasonPhrase() && "Created" != $response->getReasonPhrase()) {

            $x = json_decode($response->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        #get file name
        $content_dispotition = explode(';', $response->headers()['Content-Disposition'][0]);
        $file_name_raw = explode('=', $content_dispotition[1]);
        $file_name = str_replace('"', "", $file_name_raw[1]);

        # get content type
        $content_type = $response->headers()['Content-Type'][0];

        # get content length
        $content_length = $response->headers()['Content-Length'][0];

        $body = [
            'file' => $response,
            'filename' => $file_name,
            'type' => $content_type,
            'size' => $content_length
        ];

        return ['status' => 1, 'data' => $body];
    }

    #function delete with token
    public static function deleteFileWithToken(string $route)
    {
        $token = session()->get('login_info')['_token'];
        // $request =  Http::accept('application/json')
        //     ->withOptions([
        //         'headers'   => [
        //             'Token-IAM' =>  $token,
        //         ]
        //     ])->delete(env('API_URL') . $route);

        $request = Http::accept('application/json')
            ->withOptions([
                'headers' => [
                    'Token-IAM' => $token,
                ]
            ])->delete(env('API_URL_SDM0365') . $route);

        # cek status servis
        if ("No Content" != $request->getReasonPhrase()) {

            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function generateFile(string $route)
    {
        # set token
        $token = session()->get('login_info')['_token'];

        # get data
        $response = Http::withHeaders([
            // 'accept' =>  'application/octet-stream',
            'Token-IAM' => $token,
        ])
            ->post(env('API_URL_SDM0365') . $route);

        if ("OK" != $response->getReasonPhrase() && "Created" != $response->getReasonPhrase()) {

            $x = json_decode($response->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        #get file name
        $content_dispotition = explode(';', $response->headers()['Content-Disposition'][0]);
        $file_name_raw = explode('=', $content_dispotition[1]);
        $file_name = str_replace('"', "", $file_name_raw[1]);

        # get content type
        $content_type = $response->headers()['Content-Type'][0];

        # get content length
        $content_length = $response->headers()['Content-Length'][0];

        $body = [
            'file' => $response,
            'filename' => $file_name,
            'type' => $content_type,
            'size' => $content_length
        ];

        return ['status' => 1, 'data' => $body];
    }

    public static function getFile(string $file = null)
    {
        # set token
        $token = session()->get('login_info')['_token'];

        # get data
        $response = Http::withHeaders([
            'Token-IAM' => $token,
        ])
            ->get(env('API_URL_SDM0365') . '/files/' . $file);

        $tmp_direktori = tempnam(sys_get_temp_dir(), $file);

        file_put_contents($tmp_direktori, $response->body());

        return response()->file($tmp_direktori, [
            'Content-Type' => 'application/pdf'
        ]);
    }
}
