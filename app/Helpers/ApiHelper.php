<?php

namespace App\Helpers;

use App\Helpers\SidupakfilesClientHelper;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\FacadesStorage;

class ApiHelper
{

    public static function instance()
    {
        return new ApiHelper();
    }

    public static function postBaru(string $route, array $body)
    {
        $request = Http::post(env('API_URL') . $route, $body);

        # cek status servis
        if (!$request->successful()) {

            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();


        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function post(string $route, array $body)
    {
        // $request = Http::withToken(session()->get('token_jwt'))->post(env('SIDUPAK_API_URL').$route, $body);
        // $request = Http::withToken(session()->get('token_jwt'))->post('http://localhost:8000/newsikka/api/'.$route, $body);
        // $request = Http::post(env('API_URL') . $route, $body);
        $request = Http::post('http://localhost:8080' . $route, $body);

        # cek status servis
        if (!$request->successful()) {

            return ['status' => 0, 'message' => 'Error Web Service!'];
        }

        $body = $request->getBody();

        // return $request->json();
        return json_decode($body);
    }

    public static function patchBaru(string $route, array $body)
    {
        $request = Http::patch(env('API_URL') . $route, $body);

        if (!$request->successful()) {
            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function patch(string $route, array $body)
    {
        // $request = Http::withToken(session()->get('token_jwt'))->patch(env('SIDUPAK_API_URL').$route, $body);
        // $request = Http::withToken(session()->get('token_jwt'))->patch('http://localhost:8000/newsikka/api/'.$route, $body);
        // $request = Http::patch(env('API_URL') . $route, $body);
        $request = Http::patch('http://localhost:8080' . $route, $body);

        # cek status servis
        if (!$request->successful()) {
            return ['status' => 0, 'message' => 'Error Web Service!'];
        }

        $body = $request->getBody();

        // return $request->json();
        return json_decode($body);
    }

    public static function getBaru(string $route)
    {
        $request = Http::get(env('API_URL') . $route);

        //dd($request->getBody());

        # cek status servis
        if (!$request->successful()) {
            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function get(string $route)
    {
        // $request = Http::withToken(session()->get('token_jwt'))->get(env('SIDUPAK_API_URL').$route);
        // $request = Http::withToken(session()->get('token_jwt'))->get('http://localhost:8000/newsikka/api/'.$route);
        //$request = Http::get(env('API_URL') . $route);
        $request = Http::get('http://localhost:8080' . $route);

        # cek status servis
        if (!$request->successful()) {
            return ['status' => 0, 'message' => 'Error Web Service!'];
        }

        $body = $request->getBody();

        // return $request->json();
        return json_decode($body);
    }

    public static function getFile(string $route)
    {
        $request = Http::get(env('API_URL') . $route);

        //dd($request->getBody());

        # cek status servis
        if (!$request->successful()) {
            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        // dd($request);

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function deleteBaru(string $route, array $body)
    {
        $request = Http::delete(env('API_URL') . $route, $body);

        # cek status servis
        if (!$request->successful()) {
            $x = json_decode($request);


            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function delete(string $route, array $body)
    {
        // $request = Http::withToken(session()->get('token_jwt'))->get(env('SIDUPAK_API_URL').$route);
        // $request = Http::withToken(session()->get('token_jwt'))->get('http://localhost:8000/newsikka/api/'.$route);
        // $request = Http::delete(env('API_URL') . $route, $body);
        $request = Http::delete('http://localhost:8080' . $route, $body);

        # cek status servis
        if (!$request->successful()) {
            // return ['status' => 0, 'message' => 'Error Web Service!'];
            return json_decode($body = $request->getBody());
        }

        $body = $request->getBody();

        // return $request->json();
        return json_decode($body);
    }

    public static function getDataSDM012(string $route, string $token)
    {

        $request = Http::accept('application/json')
            ->withOptions([
                "verify" => false,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ]
            ])
            ->get(env('API_URL_SDM012') . $route);

        ///dd($request->getReasonPhrase().$request->getStatusCode());

        # cek status servis
        if ("OK" != $request->getReasonPhrase()) {

            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    #method untuk create Bearer Token IAM

    public static function getDataSDM012IdJson(string $route, string $token)
    {

        $request = Http::accept('application/ld+json')
            ->withOptions([
                "verify" => false,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ]
            ])
            ->get(env('API_URL_SDM012') . $route);

        ///dd($request->getReasonPhrase().$request->getStatusCode());

        # cek status servis
        if ("OK" != $request->getReasonPhrase()) {

            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        #dump($request);die();

        $data = (array) json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    #function get data SDM 0.1.2

    public static function postDataSDM012IdJson(string $route, string $token, array $body)
    {

        $request = Http::accept('application/ld+json')
            ->withOptions([
                "verify" => false,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ]
            ])
            ->post(env('API_URL_SDM012') . $route, $body);

        # cek status servis
        if ("OK" != $request->getReasonPhrase() && "Created" != $request->getReasonPhrase()) {

            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    #get data id+json format

    public static function postDataSDM012(string $route, string $token, array $body)
    {

        $request = Http::accept('application/json')
            ->withOptions([
                "verify" => false,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ]
            ])
            ->post(env('API_URL_SDM012') . $route, $body);

        #dump($request->getReasonPhrase().$request->getStatusCode());
        # cek status servis
        if ("OK" != $request->getReasonPhrase() && "Created" != $request->getReasonPhrase()) {

            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    #function post data SDM 0.1.2 Hydra Format

    public static function deleteDataSDM012(string $route, string $token)
    {
        $request = Http::accept('application/json')
            ->withOptions([
                "verify" => false,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ]
            ])
            ->delete(env('API_URL_SDM012') . $route);

        # dump($request->getReasonPhrase().'|'.env('API_URL_SDM012').$route.'|'.$request->getStatusCode());

        # cek status servis
        if ("No Content" != $request->getReasonPhrase()) {

            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    #function post data SDM 0.1.2

    public static function patchDataSDM012(string $route, string $token, array $body)
    {

        $request = Http::accept('application/json')
            ->withOptions([
                "verify" => false,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Content-Type' => 'application/merge-patch+json',
                ]
            ])->patch(
                env('API_URL_SDM012') . $route,
                $body
            );

        #dd($request->getReasonPhrase().$request->getStatusCode());
        # cek status servis

        if ("OK" != $request->getReasonPhrase() && "Created" != $request->getReasonPhrase()) {

            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    #function delete data SDM 0.1.2

    public static function cekTokenIAM()
    {
        if (session()->exists('bearerToken')) {
            $authiam = session()->get('bearerToken');

            // Cek validitas token
            list($tokenHeader, $payload, $signature) = explode('.', $authiam['data']->token);
            $decodedPayload = json_decode(base64_decode($payload), true);
            $tokenExpired = $decodedPayload['exp'];

            // kalau token sudah expired, refresh minta token baru
            if (time() > ($tokenExpired - 60)) {
                $authiam = self::getBearerIAM();
            }
        } else {
            $authiam = self::getBearerIAM();
        }

        return $authiam;
    }

    #function patch data SDM 0.1.2

    public static function getBearerIAM()
    {

        $userData = session()->get('user');

        $body = [
            'username' => 'root',
            'password' => 'toor'
        ];

        $request = Http::accept('application/json')
            ->withOptions(["verify" => false])
            ->post(env('API_URL_IAM') . 'authentication', [
                'username' => 'root',
                'password' => 'toor',
            ]);

        # cek status servis
        if ($request->getReasonPhrase() != "OK") {
            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $authiam['data'] = json_decode($body);

        session()->put('bearerToken', $authiam);

        return $authiam;
    }

    #function cek token iam

    public static function put(string $route, array $body)
    {
        $request = Http::put(env('API_URL') . $route, $body);

        if (!$request->successful()) {
            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    #function post data IAM

    public static function postDataIAM(string $route, string $token, array $body)
    {

        $request = Http::accept('application/json')
            ->withOptions([
                "verify" => false,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ]
            ])
            ->post(env('API_URL_IAM') . $route, $body);

        #dump($request->getReasonPhrase().$request->getStatusCode());
        # cek status servis
        if ("OK" != $request->getReasonPhrase() && "Created" != $request->getReasonPhrase()) {

            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    #function get data IAM
    public static function getDataIAM(string $route, string $token)
    {

        $request = Http::accept('application/json')
            ->withOptions([
                "verify" => false,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ]
            ])
            ->get(env('API_URL_IAM') . $route);

        #dump($request->getReasonPhrase().$request->getStatusCode());
        # cek status servis
        if ("OK" != $request->getReasonPhrase() && "Created" != $request->getReasonPhrase()) {

            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    //API_URL_SDM00
    public static function getDataSDM00IdJson(string $route, string $token)
    {

        $request = Http::accept('application/ld+json')
            ->withOptions([
                "verify" => false,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ]
            ])
            ->get(env('API_URL_SDM00') . $route);

        ///dd($request->getReasonPhrase().$request->getStatusCode());

        # cek status servis
        if ("OK" != $request->getReasonPhrase()) {

            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        #dump($request);die();

        $data = (array) json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function getDataSDM00(string $route, string $token): array
    {

        $request = Http::accept('application/json')
            ->withOptions([
                "verify" => false,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ]
            ])
            ->get(env('API_URL_SDM00') . $route);

        # cek status servis
        if ("OK" != $request->getReasonPhrase() && "Created" != $request->getReasonPhrase()) {

            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }
}
