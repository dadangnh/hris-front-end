<?php

namespace App\Helpers;

use App\Helpers\CommonVariable;
use App\Helpers\SidupakfilesClientHelper;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\FacadesStorage;

class PensiunApiHelper
{

    public static function instance()
    {
        return new PensiunApiHelper();
    }

    public static function post(string $route, array $body)
    {
        $request = Http::post(env('API_URL_SDM07') . $route, $body);

        # cek status servis
        if (!$request->successful()) {

            $x = json_decode($request);
            if (isset($x->apierror->message)) {
                $message = $x->apierror->message;
            } else {
                $message = "Error Web Service";
            }

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();
        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function patch(string $route, array $body)
    {
        $request = Http::patch(env('API_URL_SDM07') . $route, $body);

        if (!$request->successful()) {
            $x = json_decode($request);

            if (isset($x->apierror->message)) {
                $message = $x->apierror->message;
            } else {
                $message = "Error Web Service";
            }

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function get(string $route)
    {

        $request = Http::get(env('API_URL_SDM07') . $route);

        # cek status servis
        if (!$request->successful()) {
            $x = json_decode($request);

            if (isset($x->apierror->message)) {
                $message = $x->apierror->message;
            } else {
                $message = "Error Web Service";
            }

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function delete(string $route, array $body)
    {
        $request = Http::delete(env('API_URL_SDM07') . $route, $body);
        # cek status servis
        if (!$request->successful()) {
            $x = json_decode($request);
            if (isset($x->apierror->message)) {
                $message = $x->apierror->message;
            } else {
                $message = "Error Web Service";
            }
            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }
}
