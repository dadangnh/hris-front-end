<?php

namespace App\Helpers;

use Throwable;

class AppHelper
{
    public static function instance()
    {
        return new AppHelper();
    }

    public static function indonesian_date($timestamp = '', $date_format = 'l, j F Y | H:i', $suffix = 'WIB')
    {
        date_default_timezone_set('Asia/Jakarta');

        if (trim($timestamp) == '') {
            $timestamp = time();
        } elseif (!ctype_digit($timestamp)) {
            $timestamp = strtotime($timestamp);
        }

        # remove S (st,nd,rd,th) there are no such things in indonesia :p
        $date_format = preg_replace("/S/", "", $date_format);

        $pattern = array(
            '/Mon[^day]/', '/Tue[^sday]/', '/Wed[^nesday]/', '/Thu[^rsday]/',
            '/Fri[^day]/', '/Sat[^urday]/', '/Sun[^day]/', '/Monday/', '/Tuesday/',
            '/Wednesday/', '/Thursday/', '/Friday/', '/Saturday/', '/Sunday/',
            '/Jan[^uary]/', '/Feb[^ruary]/', '/Mar[^ch]/', '/Apr[^il]/', '/May/',
            '/Jun[^e]/', '/Jul[^y]/', '/Aug[^ust]/', '/Sep[^tember]/', '/Oct[^ober]/',
            '/Nov[^ember]/', '/Dec[^ember]/', '/January/', '/February/', '/March/',
            '/April/', '/June/', '/July/', '/August/', '/September/', '/October/',
            '/November/', '/December/',
        );

        $replace = array(
            'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab', 'Min',
            'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu',
            'Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des',
            'Januari', 'Februari', 'Maret', 'April', 'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember',
        );

        $date = date($date_format, $timestamp);
        $date = preg_replace($pattern, $replace, $date);
        $date = "{$date} {$suffix}";

        return $date;
    }

    public static function generateBody($user, $isi = null)
    {
        $arrpeg = [
            'pegawaiId' => $user['pegawaiId'],
            'namaPegawai' => $user['namaPegawai'],
            'nip18' => $user['nip18'],
            'pangkatId' => $user['pangkatId'],
            'namaPangkat' => $user['pangkat'],
            'jabatanId' => $user['jabatanId'],
            'namaJabatan' => $user['jabatan'],
            'unitOrgId' => $user['pangkatId'],
            'namaUnitOrg' => $user['unit'],
            'kantorId' => $user['kantorId'],
            'namaKantor' => $user['kantor'],
            'pegawaiInputId' => $user['pegawaiId']
        ];
        if ($isi != null) {
            $arrisi = $isi;
        } else {
            $arrisi = [];
        }
        $body = array_merge($arrpeg, $arrisi);

        return $body;
    }

    public static function getScopeLibur()
    {
        $arrayScope = array(
            1 => 'Provinsi',
            2 => 'Provinsi + Kota',
            3 => 'Kantor',
            4 => 'Agama',
            5 => 'Provinsi + Agama',
            6 => 'Provinsi + Kota + Agama'
        );

        return $arrayScope;
    }

    public static function convertDate(string $tanggal)
    {
        $arr_tanggal = explode('/', $tanggal);

        return $arr_tanggal[2] . '-' . $arr_tanggal[0] . '-' . $arr_tanggal[1];
    }

    #method untuk konversi tanggal ke format Y-m-d dari format m/d/y

    public static function convertDateDMY(string $tanggal)
    {
        $arr_tanggal_full = explode('T', $tanggal);
        $arr_tanggal = explode('-', $arr_tanggal_full[0]);

        return $arr_tanggal[2] . '/' . $arr_tanggal[1] . '/' . $arr_tanggal[0];
    }

    #method untuk konversi tanggal dari format Y-m-dTH:i:s dari format d/m/y

    public static function convertDateMDY(string $tanggal)
    {
        $arr_tanggal_full = explode('T', $tanggal);
        $arr_tanggal = explode('-', $arr_tanggal_full[0]);

        return $arr_tanggal[1] . '/' . $arr_tanggal[2] . '/' . $arr_tanggal[0];
    }

    #method untuk konversi format tanggal dari format Y-m-dTH:i:s dari format m/d/y

    public static function extractTime(string $outTime)
    {
        $arr_tanggal_full = explode('T', $outTime);
        $arr_time = explode('+', $arr_tanggal_full[1]);

        return $arr_time[0];
    }

    #method untuk konversi format time dari format Y-m-dTH:i:s ke H:i:s

    public static function normalisasiNamaJabatan($nm_jabatan)
    {

        $nm_jabatan = preg_replace("/[\s]+/", " ", $nm_jabatan);
        $nm_jabatan = preg_replace("/\//", " ", $nm_jabatan);
        $nm_jabatan = preg_replace("/seksi\sseksi/i", "Seksi", $nm_jabatan);
        $nm_jabatan = preg_replace("/subdirektorat\s/i", "", $nm_jabatan);
        $nm_jabatan = preg_replace("/direktorat\s/i", "", $nm_jabatan);
        $nm_jabatan = preg_replace("/jenderal jenderal/i", "Jenderal", $nm_jabatan);
        $nm_jabatan = preg_replace("/subbagian\ssubbagian/i", "", $nm_jabatan);
        $nm_jabatan = preg_replace("/subBagian\s/i", "", $nm_jabatan);
        $nm_jabatan = preg_replace("/sekretariat/i", "Direktorat", $nm_jabatan);
        $nm_jabatan = preg_replace("/bagian\sbagian/i", "Bagian", $nm_jabatan);
        $nm_jabatan = preg_replace("/bidang\sbidang/i", "Bidang", $nm_jabatan);
        $nm_jabatan = preg_replace("/pengkaji\spengkaji/i", "Pengkaji", $nm_jabatan);
        $nm_jabatan = preg_replace("/kantor\skantor/i", "Kantor", $nm_jabatan);
        $nm_jabatan = preg_replace("/subbag\ssub\sbagian/i", "Subbag", $nm_jabatan);
        $nm_jabatan = preg_replace("/subdit\ssub\sdirektorat/i", "Subdit", $nm_jabatan);
        $nm_jabatan = preg_replace("/kpp\spratama\skpp\spratama/i", "KPP Pratama", $nm_jabatan);
        $nm_jabatan = preg_replace("/kp2kp\skp2kp/i", "KP2KP", $nm_jabatan);
        $nm_jabatan = preg_replace("/kp2kp\skp2kp/i", "KP2KP", $nm_jabatan);
        $nm_jabatan = preg_replace("/pusat\spusat/i", "Pusat", $nm_jabatan);
        $nm_jabatan = preg_replace("/\skpp\swp\sbesar\smadya/i", "", $nm_jabatan);
        $nm_jabatan = preg_replace("/\skantor\swilayah\smodern/i", "", $nm_jabatan);
        $nm_jabatan = preg_replace("/\skantor\swilayah\swp\sbesar\skhusus/i", "", $nm_jabatan);
        $nm_jabatan = preg_replace("/pengaduan\sjenderal\spajak/i", "Pengaduan", $nm_jabatan);
        $nm_jabatan = preg_replace("/kantor\skpp/i", "KPP", $nm_jabatan);
        $nm_jabatan = preg_replace("/kanwil\skanwil/i", "Kanwil", $nm_jabatan);

        $nm_jabatan = preg_replace("/direktur jenderal kantor pusat djp/i", "Direktur Jenderal Pajak", $nm_jabatan);

        return $nm_jabatan;
    }

    #normalisasi jabatan

    public function number_to_indonesian_date($day)
    {
        switch ($day) {
            case '1':
                $hari = 'Senin';
                break;
            case '2':
                $hari = 'Selasa';
                break;
            case '3':
                $hari = 'Rabu';
                break;
            case '4':
                $hari = 'Kamis';
                break;
            case '5':
                $hari = 'Jumat';
                break;
            case '6':
                $hari = 'Sabtu';
                break;
            case '7':
                $hari = 'Minggu';
                break;
            default:
                $hari = '';
                break;
        }

        return $hari;
    }

    /**
     * untuk enkrip
     * return string atau halaman error
     */
    public function enkrip($string)
    {
        return encrypt($string);
    }

    /**
     * untuk halaman
     * return string atau halaman error
     */
    public function dekrip($string)
    {
        try {
            return decrypt($string);
        } catch (Throwable $th) {
            return abort(404);
        }
    }
}
