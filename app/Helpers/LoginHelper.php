<?php

namespace App\Helpers;

use App\Helpers\SidupakfilesClientHelper;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\FacadesStorage;

class LoginHelper
{

    public static function instance()
    {
        return new LoginHelper();
    }

    public static function patch(string $route, array $body, $header)
    {
        $client3 = new Client([
            // 'base_uri' => 'https://hris-ref.pajak.or.id/',
            'base_uri' => 'https://hris-ref.simulasikan.com',
        ]);

        $request = $client3->request(
            'patch',
            $route,
            $body,
            [
                'headers' =>
                    [
                        'Authorization' => "Bearer {$header}",
                        'Accept' => 'application/json',
                    ]
            ]
        );

        # cek status servis
        if ($request->getReasonPhrase() != "OK") {
            $x = json_decode($request->getBody());


            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }


        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function delete(string $route, array $body, $header)
    {
        $client3 = new Client([
            // 'base_uri' => 'https://hris-ref.pajak.or.id/',
            'base_uri' => 'https://hris-ref.simulasikan.com',
        ]);

        $request = $client3->request(
            'delete',
            $route,
            $body,
            [
                'headers' =>
                    [
                        'Authorization' => "Bearer {$header}",
                        'Accept' => 'application/json',
                    ]
            ]
        );

        # cek status servis
        if ($request->getReasonPhrase() != "OK") {
            $x = json_decode($request->getBody());


            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }


        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function iampost(string $route, array $body)
    {
        // $request = Http::post('https://iam.pajak.or.id/api/' . $route, $body);
        $request = Http::post('https://iam.simulasikan.com' . $route, $body);

        # cek status servis
        if (!$request->successful()) {

            $x = json_decode($request);


            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();
        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function post(string $route, array $body, $header)
    {
        $client3 = new Client([
            // 'base_uri' => 'https://hris-ref.pajak.or.id/',
            'base_uri' => 'https://hris-ref.simulasikan.com',
        ]);

        $request = $client3->request(
            'post',
            $route,
            $body,
            [
                'headers' =>
                    [
                        'Authorization' => "Bearer {$header}",
                        'Accept' => 'application/json',
                    ]
            ]
        );

        # cek status servis
        if ($request->getReasonPhrase() != "OK") {
            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }


        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function iampostbaru(string $route, array $body, $header)
    {
        // dd($route, $body, $header);
        $client3 = new Client([
            // 'base_uri' => 'https://iam.pajak.or.id',
            'base_uri' => 'https://iam.simulasikan.com',
            "verify" => false,
            'headers' => [
                'Authorization' => 'Bearer ' . $header,
                'Accept' => 'application/json',
            ]
        ]);

        $request = $client3->request('post', $route);

        # cek status servis
        if ($request->getReasonPhrase() != "OK") {
            $x = json_decode($request->getBody());


            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function iampatch(string $route, array $body, $header)
    {
        // dd($route, $body, $header);
        $client3 = new Client([
            // 'base_uri' => 'https://iam.pajak.or.id',
            'base_uri' => 'https://iam.simulasikan.com',
            'headers' => [
                'Authorization' => 'Bearer ' . $header,
                'Accept' => 'application/json',
            ]
        ]);

        $request = $client3->request('patch', $route, $body);

        # cek status servis
        if ($request->getReasonPhrase() != "OK") {
            $x = json_decode($request->getBody());


            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function iamget(string $route, $header)
    {
        // dd($route, $body, $header);
        $client3 = new Client([
            'base_uri' => 'https://iam.simulasikan.com',
            "verify" => false,
            'headers' => [
                'Authorization' => 'Bearer ' . $header,
                'Accept' => 'application/json',
            ]
        ]);

        $request = $client3->request('get', $route);

        # cek status servis
        if ($request->getReasonPhrase() != "OK") {
            $x = json_decode($request->getBody());


            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function iamdelete(string $route, array $body, $header)
    {
        // dd($route, $body, $header);
        $client3 = new Client([
            // 'base_uri' => 'https://iam.pajak.or.id',
            'base_uri' => 'https://iam.simulasikan.com',
            'headers' => [
                'Authorization' => 'Bearer ' . $header,
                'Accept' => 'application/json',
            ]
        ]);

        $request = $client3->request('delete', $route, $body);

        # cek status servis
        if ($request->getReasonPhrase() != "OK") {
            $x = json_decode($request->getBody());


            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function iamgetv2(string $route)
    {
        // dd($route);
        $token = IbelHelper::getNewSession(session()->get('login_info')['username']);
        $request = Http::accept('application/ld+json')
            ->withOptions([
                "verify" => false,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ]
            ])
            ->get(env('') . $route);

        // dd($request->getReasonPhrase().$request->getStatusCode());

        # cek status servis
        if ("OK" != $request->getReasonPhrase()) {

            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();
        #dump($request);die();

        $data = json_decode($body, true);
        // dd($data);


        return ['status' => 1, 'data' => $data];
    }

    public static function get(string $route, $header)
    {
        $client3 = new Client([
            // 'base_uri' => 'https://hris-ref.pajak.or.id/',
            'base_uri' => 'https://hris-ref.simulasikan.com',
        ]);

        $request = $client3->request(
            'GET',
            $route,
            [
                "verify" => false,
                'headers' =>
                    [
                        'Authorization' => "Bearer {$header}",
                        'Accept' => 'application/json',
                    ]
            ]
        );

        # cek status servis
        if ($request->getReasonPhrase() != "OK") {
            $x = json_decode($request->getBody());


            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }
}
