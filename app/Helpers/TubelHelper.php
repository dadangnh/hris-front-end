<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;

class TubelHelper
{

    public static function instance()
    {
        return new TubelHelper();
    }

    public static function getRef(string $route)
    {
        $request = Http::get(env('API_URL_SDM0362') . $route);
        // dd($request);
        # cek status servis
        if (!$request->successful()) {
            return ['status' => 0, 'message' => 'Error Web Service!'];
        }

        $body = $request->getBody();

        return json_decode($body);
    }

    public static function get(string $route)
    {
        $request = Http::get(env('API_URL_SDM0362') . $route);

        # cek status servis
        if (!$request->successful()) {
            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function postRef(string $route, array $body)
    {
        $request = Http::post(env('API_URL_SDM0362') . $route, $body);

        # cek status servis
        if (!$request->successful()) {

            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        return json_decode($body);
    }

    public static function post(string $route, array $body)
    {
        $request = Http::post(env('API_URL_SDM0362') . $route, $body);

        # cek status servis
        if (!$request->successful()) {

            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function patch(string $route, array $body)
    {
        $request = Http::patch(env('API_URL_SDM0362') . $route, $body);

        if (!$request->successful()) {
            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function upload($file, $size = 2097152, array $mime)
    {
        #get properti file
        $pathFile = $file->getRealPath();
        $namaFile = $file->getClientOriginalName();
        $mimeFile = $file->getMimeType();
        $ukuranFile = $file->getSize();

        #validasi properti file
        if (!in_array($mimeFile, $mime)) {
            return ['status' => 0, 'message' => 'Format file tidak sesuai'];
        } elseif ($ukuranFile > $size) {
            return ['status' => 0, 'message' => 'Ukuran file melebihi batas'];
        } else {
            #send file
            $response = Http::attach('file', file_get_contents($pathFile), $namaFile)
                ->put(env('API_URL_SDM0362') . '/files');

            #validasi error
            if (!$response->successful()) {
                $message = $response->body();

                $response_upload = ['status' => 0, 'message' => $message];
            } else {
                $response_upload = ['status' => 1, 'file' => $response->body()];
            }
        }

        return $response_upload;
    }

    public static function put(string $route, array $body)
    {
        $request = Http::put(env('API_URL_SDM0362') . $route, $body);

        if (!$request->successful()) {
            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function deleteFile(string $route)
    {
        $request = Http::delete(env('API_URL_SDM0362') . $route);

        # cek status servis
        if (!$request->successful()) {
            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function delete(string $route, array $body)
    {
        $request = Http::delete(env('API_URL_SDM0362') . $route, $body);

        # cek status servis
        if (!$request->successful()) {
            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function cekToken($time)
    {
        if ($time >= now()->getTimestamp()) {

            #refresh token jika expired
            $response = Http::post(env('API_URL_IAM') . 'token/refresh', ['refresh_token' => session()->get('login_info')['_refresh_token']]);

            # cek status servis
            if (!$response->successful()) {

                $message = 'Error Web Services';

                return ['status' => 0, 'message' => $message];
            }

            $data = json_decode($response);

            return $data;
        } else {

            #refresh token jika tidak expired
            $data = session()->get('login_info')['_refresh_token'];

            return $data;
        }
    }

    public static function refreshToken($refreshtoken)
    {
        $response = Http::post(env('API_URL_IAM') . 'token/refresh', ['refresh_token' => $refreshtoken]);

        # cek status servis
        if (!$response->successful()) {

            $message = 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $data = json_decode($response);

        return ['status' => 1, 'data' => $data];
    }

    public static function getNewSession($username)
    {
        $data = Http::accept('application/json')->post(env('API_URL_IAM') . 'authentication', [
            'username' => $username,
            'password' => 'Pajak123',
        ]);

        $body = $data->getBody();
        $body_decode = json_decode($body);

        $token = $body_decode->token;

        return $token;
    }

    public static function generateBody($user, $isi = null)
    {
        $arrpeg = [
            'pegawaiId' => $user['pegawaiId'],
            'namaPegawai' => $user['namaPegawai'],
            'nip18' => $user['nip18'],
            'pangkatId' => $user['pangkatId'],
            'namaPangkat' => $user['pangkat'],
            'jabatanId' => $user['jabatanId'],
            'namaJabatan' => $user['jabatan'],
            'unitOrgId' => $user['pangkatId'],
            'namaUnitOrg' => $user['unit'],
            'kantorId' => $user['kantorId'],
            'namaKantor' => $user['kantor'],
            'pegawaiInputId' => $user['pegawaiId']
        ];
        if ($isi != null) {
            $arrisi = $isi;
        } else {
            $arrisi = [];
        }
        $body = array_merge($arrpeg, $arrisi);

        return $body;
    }

    public static function detectDelimiter($file)
    {
        $delimiters = [";" => 0, "," => 0, "\t" => 0, "|" => 0];

        $handle = fopen($file, "r");
        $firstLine = fgets($handle);
        fclose($handle);
        foreach ($delimiters as $delimiter => &$count) {
            $count = count(str_getcsv($firstLine, $delimiter));
        }

        if (array_sum($delimiters) <= count($delimiters)) {
            return false;
        } else {
            return array_search(max($delimiters), $delimiters);
        }
    }

    public static function csvToArray($tmp_file, $delimiter)
    {
        $header = null;
        $data = array();
        if (($handle = fopen($tmp_file, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }
}
