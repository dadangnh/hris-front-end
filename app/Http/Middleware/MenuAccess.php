<?php

namespace App\Http\Middleware;

use App\Helpers\MenuHelper;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class MenuAccess
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $route = Route::current()->uri;

        # cek dapat menu apa
        $menu = session()->get('menu_access');

        # cek akses menu
        $cekAkses = MenuHelper::CekMenuAccess($route, $menu);

        # abort
        if ($cekAkses == false) {
            return abort(403);
        }

        return $next($request);
    }
}
