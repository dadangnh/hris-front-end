<?php

namespace App\Http\Controllers\SDM01\HariLibur;

use App\Helpers\ApiHelper;
use App\Helpers\AppHelper;
use App\Helpers\LoginHelper;
use App\Http\Controllers\Controller;
use App\Models\MenuModel;
use Illuminate\Http\Request;

class HariLiburController extends Controller
{
    public function index(Request $request)
    {
        $userData = session()->get('user');

        $data['menu'] = MenuModel::getMenu(session()->get('login_info')['_role_list'], 'harilibur/administrasi');

        $authiam = ApiHelper::cekTokenIAM();

        $itemsPerPage = 10;

        $propertyGet = '?unitOrg=' . $userData['unitId'] . '&status[]=0&status[]=2&itemsPerPage=' . $itemsPerPage;
        if (isset($_GET)) {
            foreach ($_GET as $key => $value) {

                $propertyGet .= '&' . $key . '=' . $value;

                if (null == $key) {
                    $propertyGet = '';
                }
            }
        }

        if (session()->exists('jenisLibur')) {
            $jenisLibur = session()->get('jenisLibur');
        } else {
            $jenisLibur = ApiHelper::getDataSDM012('jenis_liburs', $authiam['data']->token);

            session()->put('jenisLibur', $jenisLibur);
        }

        $usulanHariLibur = ApiHelper::getDataSDM012IdJson('usulan_hari_liburs' . $propertyGet, $authiam['data']->token);

        // dump($authiam['data']->token);
        // dd($jenisLibur);

        $data['jenisLibur'] = $jenisLibur['data'];

        $data['usulanHariLibur'] = $usulanHariLibur['data']['hydra:member'];

        $data['jumlahUsulan'] = $usulanHariLibur['data']['hydra:totalItems'];

        $data['scope'] = AppHelper::getScopeLibur();

        // pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        $data['totalItems'] = $data['jumlahUsulan'];
        $data['totalPages'] = ceil($data['jumlahUsulan'] / $itemsPerPage);
        $data['currentPage'] = $page;
        $data['numberOfElements'] = (($page - 1) * $itemsPerPage) + 1;
        //$data['size']               = $data['cuti']->size;

        return view('presensi.libur.index', $data);
    }

    public function createPermohonan(Request $request)
    {
        $userData = session()->get('user');

        $messages = [
            'required' => ':attribute wajib diisi !!!',
            'min' => ':attribute harus diisi minimal :min karakter !!!',
            'max' => ':attribute harus diisi maksimal :max karakter !!!',
        ];

        #setting validation field
        $dataCheck = [
            'nomorSuratDasar' => 'required',
            'tanggalSuratDasar' => 'required',
            'tanggalHariLibur' => 'required',
            'keteranganSuratDasar' => 'required',
            'keterangan' => 'required',
            'jenis' => 'required'
        ];

        if (1 != $request->jenis && 4 != $request->jenis) {
            $dataCheck['scope'] = 'required';

            if (1 == $request->scope) {
                $dataCheck['provinsiIds'] = 'required';
            } else if (2 == $request->scope) {
                $dataCheck['provinsiIds'] = 'required';
                $dataCheck['kotaIds'] = 'required';
            } else if (3 == $request->scope) {
                $dataCheck['kantorIds'] = 'required';
            } else if (4 == $request->scope) {
                $dataCheck['agamaIds'] = 'required';
            } else if (5 == $request->scope) {
                $dataCheck['provinsiIds'] = 'required';
                $dataCheck['agamaIds'] = 'required';
            } else if (6 == $request->scope) {
                $dataCheck['provinsiIds'] = 'required';
                $dataCheck['agamaIds'] = 'required';
                $dataCheck['kotaIds'] = 'required';
            }
        }
        #end setting

        $this->validate($request, $dataCheck, $messages);

        $arrHariLibur = explode(' - ', $request->tanggalHariLibur);
        $tanggalSuratDasar = AppHelper::convertDate($request->tanggalSuratDasar);
        $tanggalMulai = AppHelper::convertDate($arrHariLibur[0]);
        $tanggalSelesai = AppHelper::convertDate($arrHariLibur[1]);
        $scope = ((empty($request->scope)) ? 0 : $request->scope);

        //dump($request);die();

        $dataUsulan = [
            'nomorSuratDasar' => $request->nomorSuratDasar,
            'tanggalSuratDasar' => $tanggalSuratDasar . 'T00:00:00+07:00',
            'keteranganSuratDasar' => $request->keteranganSuratDasar,
            "idPembuat" => session('user')['pegawaiId'],
            "rolePembuat" => session('login_info')['_role'][0],
            "tanggalBuat" => date('Y-m-d') . 'T' . date('H:i:s') . "+07:00",
            "status" => 0,
            "unitOrg" => session('user')['unitId'],
            "kantor" => session('user')['kantorId'],
            "kantorInduk" => session('user')['kantorInduk']
        ];

        //dump($dataUsulan);

        $authiam = ApiHelper::cekTokenIAM();

        if (isset($request->idUsulan)) {
            $responseUsulan = ApiHelper::patchDataSDM012('usulan_hari_liburs/' . $request->idUsulan, $authiam['data']->token, $dataUsulan);
        } else {
            $responseUsulan = ApiHelper::postDataSDM012('usulan_hari_liburs', $authiam['data']->token, $dataUsulan);
        }

        if (1 == $responseUsulan['status']) {
            $idUsulan = $responseUsulan['data']->id;

            $dataHariLibur = [
                'usulan' => '/usulan_hari_liburs/' . $idUsulan,
                'tanggalAwal' => $tanggalMulai,
                'tanggalAkhir' => $tanggalSelesai,
                'keterangan' => $request->keterangan,
                "jenis" => '/jenis_liburs/' . $request->jenis,
                "scope" => (int) $scope,
                "active" => false
            ];

            if (0 != $scope) {
                if (1 == $scope) {
                    $dataHariLibur['provinsiIds'] = array_diff($request->provinsiIds, array(null));
                } else if (2 == $scope) {
                    $dataHariLibur['provinsiIds'] = array_diff($request->provinsiIds, array(null));
                    $dataHariLibur['kotaIds'] = array_diff($request->kotaIds, array(null));
                } else if (3 == $scope) {
                    $dataHariLibur['kantorIds'] = array_diff($request->kantorIds, array(null));
                } else if (4 == $scope) {
                    $dataHariLibur['agamaIds'] = array_diff($request->agamaIds, array(null));
                } else if (5 == $scope) {
                    $dataHariLibur['provinsiIds'] = array_diff($request->provinsiIds, array(null));
                    $dataHariLibur['agamaIds'] = array_diff($request->agamaIds, array(null));
                } else if (6 == $scope) {
                    $dataHariLibur['provinsiIds'] = array_diff($request->provinsiIds, array(null));
                    $dataHariLibur['agamaIds'] = array_diff($request->agamaIds, array(null));
                    $dataHariLibur['kotaIds'] = array_diff($request->kotaIds, array(null));
                }
            }

            //dump($dataHariLibur);

            if (isset($request->idHariLibur)) {
                $responseHariLibur = ApiHelper::patchDataSDM012('hari_liburs/' . $request->idHariLibur, $authiam['data']->token, $dataHariLibur);

                $message = 'Data Berhasil diubah!';

            } else {
                $responseHariLibur = ApiHelper::postDataSDM012('hari_liburs', $authiam['data']->token, $dataHariLibur);

                $message = 'Data Berhasil ditambah!';
            }

            if ($responseHariLibur['status'] == 1) {
                return redirect('harilibur/administrasi')->with('success', $message);
            } else {
                return redirect('harilibur/administrasi')->with('error', $responseHariLibur['message']);
            }

        } else {
            return redirect('harilibur/administrasi')->with('error', $responseUsulan['message']);
        }
    }

    public function deletePermohonan(Request $request)
    {
        $idUsulan = $request->idUsulan;
        //$idHariLibur= $request->idHariLibur;

        $authiam = ApiHelper::cekTokenIAM();

        $responseUsulan = ApiHelper::deleteDataSDM012('usulan_hari_liburs/' . $idUsulan, $authiam['data']->token);

        #dump($responseUsulan);

        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }

    public function getProvinsiList()
    {
        $authiam = ApiHelper::cekTokenIAM();

        $paramsURL = '';
        if (isset($_GET['q'])) {
            $paramsURL = '?nama=' . $_GET['q'];
        }

        $provinsi = LoginHelper::get('provinsis' . $paramsURL, $authiam['data']->token);

        foreach ($provinsi['data'] as $provinsi) {
            $provinsiList[] = [
                'id' => $provinsi->id,
                'text' => $provinsi->nama
            ];
        }

        return $provinsiList;
    }

    public function getProvinsi()
    {
        $authiam = ApiHelper::cekTokenIAM();

        $paramsURL = '';
        if (isset($_GET['provinsiId'])) {
            $paramsURL = '/' . $_GET['provinsiId'];
        }

        $provinsi = LoginHelper::get('provinsis' . $paramsURL, $authiam['data']->token);

        $data[] = [
            'id' => $provinsi['data']->id,
            'text' => $provinsi['data']->nama
        ];

        return json_encode($data);
        // return $provinsi['data'];
    }

    public function getKotaList()
    {
        $authiam = ApiHelper::cekTokenIAM();

        $paramsURL = '';
        if (isset($_GET['q'])) {
            $paramsURL = '?nama=' . $_GET['q'];
        }

        $kota = LoginHelper::get('kabupaten_kotas' . $paramsURL, $authiam['data']->token);

        foreach ($kota['data'] as $key => $val) {
            $data[] = [
                'id' => $val->id,
                'text' => $val->nama
            ];
        }

        return json_encode($data);
    }

    public function getKantorList()
    {
        $authiam = ApiHelper::cekTokenIAM();

        $paramsURL = '';
        if (isset($_GET['q'])) {
            $paramsURL = $_GET['q'];
        }

        $kantor = ApiHelper::getDataIAM('kantors/active/' . $paramsURL, $authiam['data']->token);

        foreach ($kantor['data']->kantors as $key => $val) {
            $data[] = [
                'id' => $val->id,
                'text' => $val->nama
            ];
        }

        return json_encode($data);
    }

    public function getAgamaList()
    {
        $authiam = ApiHelper::cekTokenIAM();

        $paramsURL = '';
        if (isset($_GET['search'])) {
            $paramsURL = '?nama=' . $_GET['search'];
        }

        $agamas = LoginHelper::get('agamas' . $paramsURL, $authiam['data']->token);

        foreach ($agamas['data'] as $key => $val) {
            $data[] = [
                'id' => $val->id,
                'text' => $val->nama
            ];
        }

        return json_encode($data);
    }

    public function getAgama()
    {
        $authiam = ApiHelper::cekTokenIAM();

        $paramsURL = '';
        if (isset($_GET['agamaId'])) {
            $paramsURL = '/' . $_GET['agamaId'];
        }

        $agama = LoginHelper::get('agamas' . $paramsURL, $authiam['data']->token);

        $data[] = [
            'id' => $agama['data']->id,
            'text' => $agama['data']->nama
        ];

        return json_encode($data);
    }

    public function showFormEditUsulan(Request $request)
    {

        $idUsulan = base64_decode($request->string);

        $data['menu'] = MenuModel::getMenu(session()->get('login_info')['_role_list'], 'harilibur/administrasi');

        $authiam = ApiHelper::cekTokenIAM();

        if (session()->exists('jenisLibur')) {
            $jenisLibur = session()->get('jenisLibur');
        } else {
            $jenisLibur = ApiHelper::getDataSDM012('jenis_liburs', $authiam['data']->token);

            session()->put('jenisLibur', $jenisLibur);
        }

        $dataUsulan = ApiHelper::getDataSDM012('usulan_hari_liburs/' . $idUsulan, $authiam['data']->token);

        $dataUsulan['data']->hariLibur->tanggalHariLibur = AppHelper::convertDateMDY($dataUsulan['data']->hariLibur->tanggalAwal) . ' - ' . AppHelper::convertDateMDY($dataUsulan['data']->hariLibur->tanggalAkhir);
        $dataUsulan['data']->tanggalSuratDasar = AppHelper::convertDateMDY($dataUsulan['data']->tanggalSuratDasar);

        if (!empty($dataUsulan['data']->hariLibur->provinsiIds)) {

            $paramsProvinsi = '';
            foreach ($dataUsulan['data']->hariLibur->provinsiIds as $provinsi) {
                if ('' == $paramsProvinsi) {
                    $paramsProvinsi .= '?id[]=' . $provinsi;
                } else {
                    $paramsProvinsi .= '&id[]=' . $provinsi;
                }
            }

            #consume endpoint get provinsis on hris-ref
            $getProvinsi = LoginHelper::get('provinsis' . $paramsProvinsi, $authiam['data']->token);

            foreach ($getProvinsi['data'] as $provinsi) {
                $selectedProvinsi[] = [
                    'id' => $provinsi->id,
                    'nama' => $provinsi->nama
                ];
            }
            $data['selectedProvinsi'] = $selectedProvinsi;
        }

        if (!empty($dataUsulan['data']->hariLibur->kotaIds)) {
            //dump($dataUsulan['data']->hariLibur->kotaIds);
        }

        if (!empty($dataUsulan['data']->hariLibur->kantorIds)) {
            //dump($dataUsulan['data']->hariLibur->kantorIds);
        }

        if (!empty($dataUsulan['data']->hariLibur->agamaIds)) {

            $paramsAgama = '';
            foreach ($dataUsulan['data']->hariLibur->agamaIds as $agama) {
                if ('' == $paramsAgama) {
                    $paramsAgama .= '?id[]=' . $agama;
                } else {
                    $paramsAgama .= '&id[]=' . $agama;
                }
            }

            $getAgama = LoginHelper::get('agamas' . $paramsAgama, $authiam['data']->token);
            foreach ($getAgama['data'] as $agama) {
                $selectedAgama[] = [
                    'id' => $agama->id,
                    'nama' => $agama->nama
                ];
            }
            $data['selectedAgama'] = $selectedAgama;
        }

        $data['jenisLibur'] = $jenisLibur['data'];

        $data['scope'] = AppHelper::getScopeLibur();

        $data['dataUsulan'] = $dataUsulan['data'];

        return view('presensi.libur.edit_usulan', $data);
    }

    public function kirimUsulan(Request $request)
    {
        //$idUsulan = base64_decode($request->string);
        $idUsulan = $request->idUsulan;

        $authiam = ApiHelper::cekTokenIAM();

        $body = [
            'status' => 1
        ];

        //dd($idUsulan);

        $responseUsulan = ApiHelper::patchDataSDM012('usulan_hari_liburs/' . $idUsulan, $authiam['data']->token, $body);

        //dd($responseUsulan);

        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error'];

        }

    }

    public function showPersetujuan()
    {

        $userData = session()->get('user');

        $userRole = session()->get('login_info')['_role_list'];

        $data['menu'] = MenuModel::getMenu(session()->get('login_info')['_role_list'], 'harilibur/persetujuan');

        $authiam = ApiHelper::cekTokenIAM();

        $itemsPerPage = 5;

        // dump($userRole);
        // dump($userJabatan);

        $propertyGet = '?itemsPerPage=' . $itemsPerPage;

        if (in_array('ROLE_UPK_PUSAT', $userRole)) {
            $propertyGet = '?status[]=4&status[]=1';
            $data['statusUsulan'] = 5;
        } else if (4 == $userData['levelJabatan']) {
            $propertyGet = '?unitOrg=' . $userData['unitId'] . '&status=1';
            $data['statusUsulan'] = 3;
        } else if (3 == $userData['levelJabatan']) {
            $propertyGet = '?kantor=' . $userData['kantorId'] . '&status=3';
            $data['statusUsulan'] = 4;
        }

        if (isset($_GET)) {
            foreach ($_GET as $key => $value) {

                $propertyGet .= '&' . $key . '=' . $value;

                if (null == $key) {
                    $propertyGet = '';
                }
            }
        }

        if (session()->exists('jenisLibur')) {
            $jenisLibur = session()->get('jenisLibur');
        } else {
            $jenisLibur = ApiHelper::getDataSDM012('jenis_liburs', $authiam['data']->token);

            session()->put('jenisLibur', $jenisLibur);
        }

        $usulanHariLibur = ApiHelper::getDataSDM012IdJson('usulan_hari_liburs' . $propertyGet, $authiam['data']->token);

        //dd($usulanHariLibur['data']);

        $data['jenisLibur'] = $jenisLibur['data'];

        $data['usulanHariLibur'] = $usulanHariLibur['data']['hydra:member'];

        $data['jumlahUsulan'] = $usulanHariLibur['data']['hydra:totalItems'];

        $data['scope'] = AppHelper::getScopeLibur();

        // pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        $data['totalItems'] = $data['jumlahUsulan'];
        $data['totalPages'] = ceil($data['jumlahUsulan'] / $itemsPerPage);
        $data['currentPage'] = $page;
        $data['numberOfElements'] = (($page - 1) * $itemsPerPage) + 1;
        //$data['size']               = $data['cuti']->size;

        // dump($data);die();

        #set alert
        if (6 == $userData['levelJabatan'] && false == in_array('ROLE_UPK_PUSAT', $userRole)) {
            $data['alert_message'] = '<div class="alert alert-custom alert-warning" role="alert">
                                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                        <div class="alert-text">
                                            <h4 class="alert-heading">Perhatian!</h4>
                                            <p>Menu ini hanya dapat diakses oleh level jabatan 4 keatas</p>
                                        </div>
                                    </div>';
        } else {
            $data['alert_message'] = null;
        }

        return view('presensi.libur.show_permohonan', $data);
    }

    public function approveUsulan(Request $request)
    {

        $idUsulan = $request->idUsulan;
        $idHariLibur = $request->idHariLibur;

        $authiam = ApiHelper::cekTokenIAM();

        $body = [
            'status' => (int) $request->statusUsulan
        ];

        if (3 == $request->statusUsulan) {
            $body['idAtasanApprove'] = session('user')['pegawaiId'];
            $body['tanggalAtasanApprove'] = date('Y-m-d') . 'T' . date('H:i:s') . "+07:00";
        } else if (4 == $request->statusUsulan) {
            $body['idKakapApprove'] = session('user')['pegawaiId'];
            $body['tanggalKakapApprove'] = date('Y-m-d') . 'T' . date('H:i:s') . "+07:00";
        } else if (5 == $request->statusUsulan) {
            $body['idUpkPusatApprove'] = session('user')['pegawaiId'];
            $body['tanggalUpkPusatApprove'] = date('Y-m-d') . 'T' . date('H:i:s') . "+07:00";
        }

        $responseUsulan = ApiHelper::patchDataSDM012('usulan_hari_liburs/' . $idUsulan, $authiam['data']->token, $body);

        if (1 == $responseUsulan['status']) {
            if (5 == $request->statusUsulan) {
                #update status active hari libur
                $responseHariLibur = ApiHelper::patchDataSDM012('hari_liburs/' . $idHariLibur, $authiam['data']->token, ['active' => true]);
            }

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error ' . json_encode($body)];

        }
    }

    public function tolakUsulan(Request $request)
    {

        $idUsulan = $request->idUsulan;

        $authiam = ApiHelper::cekTokenIAM();

        #setting validation field
        if (empty($request->keterangan)) {
            return ['status' => 0, 'message' => 'Alasan Penolakan Wajib Di Isi !!!'];
        }

        $body = [
            'status' => (int) $request->statusUsulan
        ];

        $responseUsulan = ApiHelper::patchDataSDM012('usulan_hari_liburs/' . $idUsulan, $authiam['data']->token, $body);

        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error ' . json_encode($body)];

        }
    }

    public function monitoring()
    {

        $userData = session()->get('user');

        $userRole = session()->get('login_info')['_role_list'];

        $data['menu'] = MenuModel::getMenu(session()->get('login_info')['_role_list'], 'harilibur/monitoring');

        $authiam = ApiHelper::cekTokenIAM();

        $itemsPerPage = 5;

        if (in_array('ROLE_UPK_PUSAT', $userRole)) {
            $propertyGet = $propertyGet = '?itemsPerPage=' . $itemsPerPage;
        } else {
            $propertyGet = $propertyGet = '?kantor=' . $userData['kantorId'] . '&itemsPerPage=' . $itemsPerPage;
        }


        if (isset($_GET)) {
            foreach ($_GET as $key => $value) {

                $propertyGet .= '&' . $key . '=' . $value;

                if (null == $key) {
                    $propertyGet = '';
                }
            }
        }

        if (session()->exists('jenisLibur')) {
            $jenisLibur = session()->get('jenisLibur');
        } else {
            $jenisLibur = ApiHelper::getDataSDM012('jenis_liburs', $authiam['data']->token);

            session()->put('jenisLibur', $jenisLibur);
        }

        $usulanHariLibur = ApiHelper::getDataSDM012IdJson('usulan_hari_liburs' . $propertyGet, $authiam['data']->token);

        $data['jenisLibur'] = $jenisLibur['data'];

        $data['usulanHariLibur'] = $usulanHariLibur['data']['hydra:member'];

        $data['jumlahUsulan'] = $usulanHariLibur['data']['hydra:totalItems'];

        $data['scope'] = AppHelper::getScopeLibur();

        for ($i = 0; $i < count($usulanHariLibur['data']['hydra:member']); $i++) {
            $data['usulanHariLibur'][$i]->ketStatus = self::viewStatus($data['usulanHariLibur'][$i]->status);
        }

        // pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        $data['totalItems'] = $data['jumlahUsulan'];
        $data['totalPages'] = ceil($data['jumlahUsulan'] / $itemsPerPage);
        $data['currentPage'] = $page;
        $data['numberOfElements'] = (($page - 1) * $itemsPerPage) + 1;
        //$data['size']               = $data['cuti']->size;

        return view('presensi.libur.monitoring', $data);
    }

    public static function viewStatus($status)
    {
        $statusList = [
            'Draft',
            'Kirim Usulan',
            'Usulan Di Tolak',
            'Persetujuan Atasan Langsung',
            'Persetujuan Atasan Atasan Langsung',
            'Persetujuan UPK',
        ];

        return $statusList[$status];
    }
}
