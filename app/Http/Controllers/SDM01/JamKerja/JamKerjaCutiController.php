<?php

namespace App\Http\Controllers\SDM01\JamKerja;

use App\Helpers\ApiHelper;
use App\Helpers\AppHelper;
use App\Http\Controllers\Controller;
use App\Models\MenuModel;
use Illuminate\Http\Request;

class JamKerjaCutiController extends Controller
{
    //
    public function index(Request $request)
    {

        $data['menu'] = MenuModel::getMenu(session()->get('login_info')['_role_list'], 'jamkerjacuti/administrasi');

        $authiam = ApiHelper::cekTokenIAM();

        $itemsPerPage = 10;

        $usulanJKC = ApiHelper::getDataSDM012IdJson('jam_kerja_cutis', $authiam['data']->token);

        $data['usulanJKC'] = $usulanJKC['data']['hydra:member'];

        $data['jumlahUsulan'] = $usulanJKC['data']['hydra:totalItems'];

        //dump($authiam['data']->token);

        if (session()->exists('jnsCuti')) {
            $jnsCuti = session()->get('jnsCuti');
        } else {
            $jnsCuti = ApiHelper::getDataSDM012('jns_cutis', $authiam['data']->token);

            session()->put('jnsCuti', $jnsCuti);
        }

        // pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        $data['totalItems'] = $data['jumlahUsulan'];
        $data['totalPages'] = ceil($data['jumlahUsulan'] / $itemsPerPage);
        $data['currentPage'] = $page;
        $data['numberOfElements'] = (($page - 1) * $itemsPerPage) + 1;
        //$data['size']               = $data['cuti']->size;

        $data['jnsCuti'] = $jnsCuti['data'];

        return view('presensi.jamkerjacuti.index', $data);
    }

    public function createPermohonan(Request $request)
    {
        $authiam = ApiHelper::cekTokenIAM();

        // dd($request);

        $messages = [
            'required' => ':attribute wajib diisi !!!',
            'min' => ':attribute harus diisi minimal :min karakter !!!',
            'max' => ':attribute harus diisi maksimal :max karakter !!!',
        ];

        #setting validation field
        $dataCheck = [
            'tanggalJamKerjaCuti' => 'required',
            'jnsCuti' => 'required',
            'jamMasukCuti' => 'required',
            'jamPulangCuti' => 'required',
            'keterangan' => 'required'
        ];

        $arrTanggal = explode(' - ', $request->tanggalJamKerjaCuti);
        $tanggalMulai = AppHelper::convertDate($arrTanggal[0]);
        $tanggalSelesai = AppHelper::convertDate($arrTanggal[1]);

        $dataUsulan = [
            'tanggalMulai' => $tanggalMulai,
            'tanggalSelesai' => $tanggalSelesai,
            'jenisCuti' => '/jns_cutis/' . $request->idJnsCuti,
            'jamMasukCuti' => $request->jamMasukCuti,
            'jamPulangCuti' => $request->jamPulangCuti,
            "dateCreated" => date('Y-m-d') . 'T' . date('H:i:s') . "+07:00",
            "createdBy" => session('user')['pegawaiId'],
            "status" => 0,
            "keterangan" => $request->keterangan
        ];

        if (10 == (int) $request->jnsCuti) {
            $data['typeCuti'] = 'required';
            $dataUsulan['typeCuti'] = (int) $request->typeCuti;
        }

        $this->validate($request, $dataCheck, $messages);

        //dd(json_encode($dataUsulan));

        if (isset($request->idUsulan)) {
            $responseUsulan = ApiHelper::patchDataSDM012('jam_kerja_cutis/' . $request->idUsulan, $authiam['data']->token, $dataUsulan);
            $message = 'Data Berhasil diubah!';
        } else {
            $responseUsulan = ApiHelper::postDataSDM012('jam_kerja_cutis', $authiam['data']->token, $dataUsulan);
            $message = 'Data Berhasil ditambah!';
        }

        if (1 == $responseUsulan['status']) {
            return redirect('jamkerjacuti/administrasi')->with('success', $message);
        } else {
            return redirect('jamkerjacuti/administrasi')->with('error', $responseUsulan['message']);
        }
    }

    public function deletePermohonan(Request $request)
    {
        $idUsulan = $request->idUsulan;

        $authiam = ApiHelper::cekTokenIAM();

        $responseUsulan = ApiHelper::deleteDataSDM012('jam_kerja_cutis/' . $idUsulan, $authiam['data']->token);

        #dump($responseUsulan);

        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }

    public function showPersetujuan()
    {
        $userData = session()->get('user');

        $userRole = session()->get('login_info')['_role_list'];

        $data['menu'] = MenuModel::getMenu(session()->get('login_info')['_role_list'], 'jamkerjacuti/persetujuan');

        $authiam = ApiHelper::cekTokenIAM();

        $itemsPerPage = 5;

        // dump($userRole);
        // dump($userJabatan);

        $propertyGet = '?itemsPerPage=' . $itemsPerPage . '&status=0';

        $usulanJKC = ApiHelper::getDataSDM012IdJson('jam_kerja_cutis' . $propertyGet, $authiam['data']->token);

        $data['usulanJKC'] = $usulanJKC['data']['hydra:member'];

        $data['jumlahUsulan'] = $usulanJKC['data']['hydra:totalItems'];

        // pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        $data['totalItems'] = $data['jumlahUsulan'];
        $data['totalPages'] = ceil($data['jumlahUsulan'] / $itemsPerPage);
        $data['currentPage'] = $page;
        $data['numberOfElements'] = (($page - 1) * $itemsPerPage) + 1;
        //$data['size']               = $data['cuti']->size;

        #set alert
        if (6 == $userData['levelJabatan'] && false == in_array('ROLE_UPK_PUSAT', $userRole)) {
            $data['alert_message'] = '<div class="alert alert-custom alert-warning" role="alert">
                                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                        <div class="alert-text">
                                            <h4 class="alert-heading">Perhatian!</h4>
                                            <p>Menu ini hanya dapat diakses oleh level jabatan 4 keatas</p>
                                        </div>
                                    </div>';
        } else {
            $data['alert_message'] = null;
        }
        return view('presensi.jamkerjacuti.show_permohonan', $data);
    }

    public function approveUsulan(Request $request)
    {

        $idUsulan = $request->idUsulan;

        $authiam = ApiHelper::cekTokenIAM();

        $body = [
            'status' => 1,
            'dateApproved' => date('Y-m-d') . 'T' . date('H:i:s') . "+07:00",
            'approvedBy' => session('user')['pegawaiId']
        ];

        $responseUsulan = ApiHelper::patchDataSDM012('jam_kerja_cutis/' . $idUsulan, $authiam['data']->token, $body);

        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error ' . json_encode($body)];

        }
    }
}
