<?php

namespace App\Http\Controllers\SDM01\JamKerja;

use App\Helpers\ApiHelper;
use App\Helpers\AppHelper;
use App\Http\Controllers\Controller;
use App\Models\MenuModel;
use Illuminate\Http\Request;

class JamKerjaUmumController extends Controller
{
    //
    public function index(Request $request)
    {
        $userData = session()->get('user');

        $data['menu'] = MenuModel::getMenu(session()->get('login_info')['_role_list'], 'jamkerjaumum/administrasi');

        $authiam = ApiHelper::cekTokenIAM();

        $itemsPerPage = 10;

        $usulanJKU = ApiHelper::getDataSDM012IdJson('jam_kerjas', $authiam['data']->token);

        $data['usulanJKU'] = $usulanJKU['data']['hydra:member'];

        $data['jumlahUsulan'] = $usulanJKU['data']['hydra:totalItems'];

        // pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        $data['totalItems'] = $data['jumlahUsulan'];
        $data['totalPages'] = ceil($data['jumlahUsulan'] / $itemsPerPage);
        $data['currentPage'] = $page;
        $data['numberOfElements'] = (($page - 1) * $itemsPerPage) + 1;
        //$data['size']               = $data['cuti']->size;


        return view('presensi.jamkerjaumum.index', $data);
    }

    public function createPermohonan(Request $request)
    {
        $userData = session()->get('user');
        $authiam = ApiHelper::cekTokenIAM();

        //dd($request);

        $messages = [
            'required' => ':attribute wajib diisi !!!',
            'min' => ':attribute harus diisi minimal :min karakter !!!',
            'max' => ':attribute harus diisi maksimal :max karakter !!!',
        ];

        #setting validation field
        $dataCheck = [
            'tanggalJamKerjaUtama' => 'required',
            'jamMasuk' => 'required',
            'jamPulang' => 'required',
            'keterangan' => 'required'
        ];

        $this->validate($request, $dataCheck, $messages);

        $arrTanggal = explode(' - ', $request->tanggalJamKerjaUtama);
        $tanggalMulai = AppHelper::convertDate($arrTanggal[0]);
        $tanggalSelesai = AppHelper::convertDate($arrTanggal[1]);

        $dataUsulan = [
            'tanggalMulai' => $tanggalMulai,
            'tanggalSelesai' => $tanggalSelesai,
            'jamMasuk' => $request->jamMasuk,
            'jamPulang' => $request->jamPulang,
            "dateCreated" => date('Y-m-d') . 'T' . date('H:i:s') . "+07:00",
            "createdBy" => session('user')['pegawaiId'],
            "status" => 0,
            "keterangan" => $request->keterangan
        ];

        if (isset($request->idUsulan)) {
            $responseUsulan = ApiHelper::patchDataSDM012('jam_kerjas/' . $request->idUsulan, $authiam['data']->token, $dataUsulan);
            $message = 'Data Berhasil diubah!';
        } else {
            $responseUsulan = ApiHelper::postDataSDM012('jam_kerjas', $authiam['data']->token, $dataUsulan);
            $message = 'Data Berhasil ditambah!';
        }

        if (1 == $responseUsulan['status']) {
            return redirect('jamkerjaumum/administrasi')->with('success', $message);
        } else {
            return redirect('jamkerjaumum/administrasi')->with('error', $responseUsulan['message']);
        }
    }

    public function deletePermohonan(Request $request)
    {
        $idUsulan = $request->idUsulan;

        $authiam = ApiHelper::cekTokenIAM();

        $responseUsulan = ApiHelper::deleteDataSDM012('jam_kerjas/' . $idUsulan, $authiam['data']->token);

        #dump($responseUsulan);

        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }

    public function showPersetujuan()
    {
        $userData = session()->get('user');

        $userRole = session()->get('login_info')['_role_list'];

        $data['menu'] = MenuModel::getMenu(session()->get('login_info')['_role_list'], 'jamkerjaumum/persetujuan');

        $authiam = ApiHelper::cekTokenIAM();

        $itemsPerPage = 5;

        // dump($userRole);
        // dump($userJabatan);

        $propertyGet = '?itemsPerPage=' . $itemsPerPage . '&status=0';

        $usulanJKU = ApiHelper::getDataSDM012IdJson('jam_kerjas' . $propertyGet, $authiam['data']->token);

        $data['usulanJKU'] = $usulanJKU['data']['hydra:member'];

        $data['jumlahUsulan'] = $usulanJKU['data']['hydra:totalItems'];

        // pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        $data['totalItems'] = $data['jumlahUsulan'];
        $data['totalPages'] = ceil($data['jumlahUsulan'] / $itemsPerPage);
        $data['currentPage'] = $page;
        $data['numberOfElements'] = (($page - 1) * $itemsPerPage) + 1;
        //$data['size']               = $data['cuti']->size;

        #set alert
        if (6 == $userData['levelJabatan'] && false == in_array('ROLE_UPK_PUSAT', $userRole)) {
            $data['alert_message'] = '<div class="alert alert-custom alert-warning" role="alert">
                                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                        <div class="alert-text">
                                            <h4 class="alert-heading">Perhatian!</h4>
                                            <p>Menu ini hanya dapat diakses oleh level jabatan 4 keatas</p>
                                        </div>
                                    </div>';
        } else {
            $data['alert_message'] = null;
        }
        return view('presensi.jamkerjaumum.show_permohonan', $data);
    }

    public function approveUsulan(Request $request)
    {

        $idUsulan = $request->idUsulan;

        $authiam = ApiHelper::cekTokenIAM();

        $body = [
            'status' => 1,
            'dateApproved' => date('Y-m-d') . 'T' . date('H:i:s') . "+07:00",
            'approvedBy' => session('user')['pegawaiId']
        ];

        $responseUsulan = ApiHelper::patchDataSDM012('jam_kerjas/' . $idUsulan, $authiam['data']->token, $body);

        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error ' . json_encode($body)];

        }
    }
}
