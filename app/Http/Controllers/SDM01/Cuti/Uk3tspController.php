<?php

namespace App\Http\Controllers\SDM01\Cuti;

use App\Helpers\ApiHelper;
use App\Helpers\AppHelper;
use App\Helpers\LoginHelper;
use App\Http\Controllers\Controller;
use App\Models\MenuModel;
use Illuminate\Http\Request;


class Uk3tspController extends Controller
{
    public function index(Request $request)
    {
        $userData = session()->get('user');

        $data['menu'] = MenuModel::getMenu(session()->get('login_info')['_role_list'], 'uk3tsp/administrasi');

        $authiam = ApiHelper::cekTokenIAM();

        $itemsPerPage = 10;

        $propertyGet = '?unitOrg=' . $userData['unitId'] . '&status[]=0&status[]=1&status[]=2&status[]=3&itemsPerPage=' . $itemsPerPage;
        if (isset($_GET)) {
            foreach ($_GET as $key => $value) {

                $propertyGet .= '&' . $key . '=' . $value;

                if (null == $key) {
                    $propertyGet = '';
                }
            }
        }

        $usulanJKU = ApiHelper::getDataSDM012IdJson('uk3tsps' . $propertyGet, $authiam['data']->token);

        // dd($usulanJKU);

        // $data = $usulanJKU['data']['hydra:member'];

        // $data2 = [];
        // foreach ($data as $key => $value) {
        //     $data[] = $key;
        // }

        // dd($data);

        $data['usulanJKU'] = $usulanJKU['data']['hydra:member'];

        $data['jumlahUsulan'] = $usulanJKU['data']['hydra:totalItems'];


        // pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        $data['totalItems'] = $data['jumlahUsulan'];
        $data['totalPages'] = ceil($data['jumlahUsulan'] / $itemsPerPage);
        $data['currentPage'] = $page;
        $data['numberOfElements'] = (($page - 1) * $itemsPerPage) + 1;
        //$data['size']               = $data['cuti']->size;


        return view('uk3tsp.index', $data);
    }

    public function createPermohonan(Request $request)
    {
        // dd($request->all());
        $userData = session()->get('user');
        $authiam = ApiHelper::cekTokenIAM();


        //dd($request);

        $messages = [
            'required' => ':attribute wajib diisi !!!',
            'min' => ':attribute harus diisi minimal :min karakter !!!',
            'max' => ':attribute harus diisi maksimal :max karakter !!!',
        ];

        #setting validation field
        $dataCheck = [
            'pmk' => 'required',
            'tanggal' => 'required',
            'keterangan' => 'required'
        ];

        $this->validate($request, $dataCheck, $messages);

        // $arrTanggal       = $request->tanggalPmk;
        $tanggal = AppHelper::convertDate($request->tanggal);
        $kantor = $request->kantorIds;
        // dd($tes);

        $dataUsulan = [
            'uk3tsp' => [
                'pmk' => $request->pmk,
                'tanggal' => $tanggal,
                'keterangan' => $request->keterangan,
                'status' => (int) 0,
            ],
        ];

        $dataUsulan['kantor'] = [];
        foreach ($kantor as $key => $val) {
            $kantors = explode('|', $val);
            array_push(
                $dataUsulan['kantor'],
                array(
                    'kantorId' => $kantors[0],
                    'nama' => $kantors[1]
                )
            );
        }
        // dd(json_encode($dataUsulan));
        // print_r($authiam['data']->token);
        // print_r(json_encode($dataUsulan));
        // dump($dataUsulan);
        // die;

        // $responseUsulan = ApiHelper::postDataSDM012('uk3tsps/permohonan_full', $authiam['data']->token, $dataUsulan);
        // dd($dataUsulan);
        // $message        = 'Data Berhasil ditambah!';
        // $responseUsulan = ApiHelper::patchDataSDM012('uk3tsps/'.$request->idUsulan, $authiam['data']->token, $dataUsulan);
        //     $message        = 'Data Berhasil diupdate!';

        if (isset($request->idUsulan)) {
            $responseUsulan = ApiHelper::patchDataSDM012('uk3tsps/permohonan_full/' . $request->idUsulan, $authiam['data']->token, $dataUsulan);
            $message = 'Data Berhasil diupdate!';
            // dd($responseUsulan);
        } else {
            $responseUsulan = ApiHelper::postDataSDM012('uk3tsps/permohonan_full', $authiam['data']->token, $dataUsulan);
            $message = 'Data Berhasil ditambah!';
            // dd($responseUsulan);


        }


        if ((int) 1 == $responseUsulan['status']) {
            return redirect('uk3tsp/administrasi')->with('success', $message);
        } else {
            return redirect('uk3tsp/administrasi')->with('error', $responseUsulan['message']);
        }
    }

    public function deletePermohonan(Request $request)
    {
        $idUsulan = $request->idUsulan;

        $authiam = ApiHelper::cekTokenIAM();

        $responseUsulan = ApiHelper::deleteDataSDM012('uk3tsps/' . $idUsulan, $authiam['data']->token);

        #dump($responseUsulan);

        if ((int) 1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }


    public function getKantorList()
    {
        $authiam = ApiHelper::cekTokenIAM();

        $paramsURL = '';
        if (isset($_GET['q'])) {
            $paramsURL = $_GET['q'];
        }

        $kantor = LoginHelper::iamget('api/kantors/active/' . $paramsURL, $authiam['data']->token);
        // dd($kantor);

        foreach ($kantor['data']->kantors as $key => $val) {
            $data[] = [
                'id' => $val->id . '|' . $val->nama,
                'text' => $val->nama
            ];
        }
        return json_encode($data);
    }

    public function showFormEditUsulan(Request $request)
    {

        $idUsulan = base64_decode($request->string);

        $data['menu'] = MenuModel::getMenu(session()->get('login_info')['_role_list'], 'uk3tsp/administrasi');

        $authiam = ApiHelper::cekTokenIAM();

        $dataUsulan = ApiHelper::getDataSDM012('uk3tsps/' . $idUsulan, $authiam['data']->token);

        // dd($dataUsulan);

        $dataUsulan['data']->tanggal = AppHelper::convertDateMDY($dataUsulan['data']->tanggal);

// dd($dataUsulan);

        // if(!empty($dataUsulan['data']->uk3tsp->kantorIds)){
        //     // dump($dataUsulan['data']->uk3tsp->kantorIds);
        // }
        if (!empty($dataUsulan['data']->kantor)) {

            $paramsKantor = '';
            foreach ($dataUsulan['data']->kantor as $item) {
                if ('' == $paramsKantor) {
                    $paramsKantor .= '?id[]=' . $item->kantorId;
                } else {
                    $paramsKantor .= '&id[]=' . $item->kantorId;
                }
            }

            $getKantor = LoginHelper::iamget('api/kantors/' . $paramsKantor, $authiam['data']->token);

            foreach ($getKantor['data'] as $kantor) {
                $selectedKantor[] = [
                    'id' => $kantor->id . '|' . $kantor->nama,
                    'nama' => $kantor->nama
                ];
            }
            $data['selectedKantor'] = $selectedKantor;
            // dump($selectedKantor);
        }

        // $data['jenisLibur']         = $jenisLibur['data'];

        // $data['scope']              = AppHelper::getScopeLibur();

        $data['dataUsulan'] = $dataUsulan['data'];
        // dd($dataUsulan['data']);

        return view('uk3tsp.edit_permohonan', $data);
    }


    public function kirimUsulan(Request $request)
    {

        $idUsulan = $request->idUsulan;

        $authiam = ApiHelper::cekTokenIAM();

        $body = [
            'status' => 1
            // 'dateApproved'  => date('Y-m-d').'T'.date('H:i:s')."+07:00",
            // 'approvedBy'    => session('user')['pegawaiId']
        ];

        $responseUsulan = ApiHelper::patchDataSDM012('uk3tsps/' . $idUsulan, $authiam['data']->token, $body);

        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error ' . json_encode($body)];

        }
    }

    public function showPersetujuan()
    {
        $userData = session()->get('user');

        $userRole = session()->get('login_info')['_role_list'];

        $data['menu'] = MenuModel::getMenu(session()->get('login_info')['_role_list'], 'uk3tsp/persetujuan');

        $authiam = ApiHelper::cekTokenIAM();

        $itemsPerPage = 5;

        // dump($userRole);
        // dump($userJabatan);

        $propertyGet = '?itemsPerPage=' . $itemsPerPage . '&status=1';

        $usulanJKU = ApiHelper::getDataSDM012IdJson('uk3tsps' . $propertyGet, $authiam['data']->token);

        $data['usulanJKU'] = $usulanJKU['data']['hydra:member'];

        $data['jumlahUsulan'] = $usulanJKU['data']['hydra:totalItems'];

        // pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        $data['totalItems'] = $data['jumlahUsulan'];
        $data['totalPages'] = ceil($data['jumlahUsulan'] / $itemsPerPage);
        $data['currentPage'] = $page;
        $data['numberOfElements'] = (($page - 1) * $itemsPerPage) + 1;
        //$data['size']               = $data['cuti']->size;

        #set alert
        if (6 == $userData['levelJabatan'] && false == in_array('ROLE_UPK_PUSAT', $userRole)) {
            $data['alert_message'] = '<div class="alert alert-custom alert-warning" role="alert">
                                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                        <div class="alert-text">
                                            <h4 class="alert-heading">Perhatian!</h4>
                                            <p>Menu ini hanya dapat diakses oleh level jabatan 4 keatas</p>
                                        </div>
                                    </div>';
        } else {
            $data['alert_message'] = null;
        }
        return view('uk3tsp.persetujuan', $data);
    }

    public function approveUsulan(Request $request)
    {

        $idUsulan = $request->idUsulan;

        $authiam = ApiHelper::cekTokenIAM();

        $body = [
            'status' => 2
            // 'dateApproved'  => date('Y-m-d').'T'.date('H:i:s')."+07:00",
            // 'approvedBy'    => session('user')['pegawaiId']
        ];

        $responseUsulan = ApiHelper::patchDataSDM012('uk3tsps/' . $idUsulan, $authiam['data']->token, $body);

        if (2 == $responseUsulan['status']) {

            return ['status' => 2, 'message' => 'success'];

        } else {

            return ['status' => 1, 'message' => 'error ' . json_encode($body)];

        }
    }

    public function tolakUsulan(Request $request)
    {

        $idUsulan = $request->idUsulan;

        $authiam = ApiHelper::cekTokenIAM();

        #setting validation field
        if (empty($request->keteranganTolak)) {
            return ['status' => 0, 'message' => 'Alasan Penolakan Wajib Di Isi !!!'];
        }

        $body = [
            'status' => (int) $request->statusUsulan,
            'keteranganTolak' => $request->keteranganTolak
        ];


        $responseUsulan = ApiHelper::patchDataSDM012('uk3tsps/' . $idUsulan, $authiam['data']->token, $body);

        // dd($responseUsulan);
        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error ' . json_encode($body)];

        }
    }


}
