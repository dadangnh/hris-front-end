<?php

namespace App\Http\Controllers\SDM01\Cuti;

use App\Helpers\ApiHelper;
use App\Helpers\AppHelper;
use App\Helpers\LoginHelper;
use App\Http\Controllers\Controller;
use App\Models\MenuModel;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PhpOffice\PhpWord\Style\Paper;
use PhpOffice\PhpWord\TemplateProcessor;

class CutiController extends Controller
{

    public function __construct()
    {

        $tokenIam = ApiHelper::cekTokenIAM();
        $this->token = $tokenIam['data']->token;
        $this->tahunIni = date('Y');
        $this->tahunLalu = date('Y') - 1;

    }

    public function index(Request $request)
    {
        $data['userRole'] = session()->get('login_info')['_role_list'];
        $data['menu'] = MenuModel::getMenu(session()->get('login_info')['_role_list'], 'cuti/administrasi');

        $userData = LoginHelper::get('master_pegawais?pegawaiId=' . $_GET['string'], $this->token);
        $userData['data'] = $userData['data'][0];
        $data['namaPegawai'] = $userData['data']->namaPegawai;
        $data['jenisKelamin'] = $userData['data']->jenisKelamin;

        $kantorId = (session()->get('user')['kantorId']);
        $uk3tsps = ApiHelper::getDataSDM012IdJson('uk3tsps/active/pmk', $this->token) ?? '';

        //get data kantor uk3tsp dan rubah menjadi array
        $arr_uk3tsp = [];
        if (null !== $uk3tsps['data']['uk3tsp']) {
            $dataUk3tsp = $uk3tsps['data']['uk3tsp']->kantor;
            foreach ($dataUk3tsp as $uk3tsp) {
                array_push($arr_uk3tsp, $uk3tsp->kantorId);
            }
        }

        //flag kantor yang mendapat cuti tambahan
        if (in_array($kantorId, $arr_uk3tsp)) {
            $data['hakcutitambahan'] = 1;
        } else {
            $data['hakcutitambahan'] = 0;
        }

        $hakCutiTambahan = $data['hakcutitambahan'];


        $userLogin = session()->get('user');
        $body = [
            'pegawaiId' => $userLogin['pegawaiId'],
        ];
        $cutiTambahanDipakai = ApiHelper::postDataSDM012('permohonan_cuti_tambahans/get_cuti_tambahan_dipakai', $this->token, $body);
        $data['cutiTambahanDipakai'] = $cutiTambahanDipakai['data']->cutiTambahanDipakai;


        $hariLiburs = ApiHelper::postDataSDM012(
            'hari_liburs/ambil_hari_libur',
            $this->token,
            [
                "agama" => $userData['data']->agamaId
            ]
        );

        $data['hariLiburs'] = $hariLiburs['data'];

        $itemsPerPage = 10;

        $propertyGet = '?itemsPerPage=' . $itemsPerPage . '&pegawaiId=' . $_GET['string'];
        if (isset($_GET)) {
            foreach ($_GET as $key => $value) {

                if ('string' != $key) {
                    $propertyGet .= '&' . $key . '=' . $value;

                    if (null == $key) {
                        $propertyGet = '';
                    }
                }
            }
        }

        #data permohonan cuti
        $permohonanCuti = ApiHelper::getDataSDM012IdJson('permohonan_cutis' . $propertyGet, $this->token);

        $data['permohonanCuti'] = $permohonanCuti['data']['hydra:member'];

        $data['jumlahUsulan'] = $permohonanCuti['data']['hydra:totalItems'];

        #data permohonan cuti Tambahan
        $permohonanCutiTambahan = ApiHelper::getDataSDM012IdJson('permohonan_cuti_tambahans' . $propertyGet, $this->token);

        $data['permohonanCutiTambahan'] = $permohonanCutiTambahan['data']['hydra:member'];

        $data['jumlahUsulan'] = $permohonanCutiTambahan['data']['hydra:totalItems'];

        for ($i = 0; $i < count($permohonanCuti['data']['hydra:member']); $i++) {
            $data['permohonanCuti'][$i]->ketStatus = self::viewStatus($data['permohonanCuti'][$i]->status);
        }

        for ($i = 0; $i < count($permohonanCutiTambahan['data']['hydra:member']); $i++) {
            $data['permohonanCutiTambahan'][$i]->ketStatus = self::viewStatus($data['permohonanCutiTambahan'][$i]->status);
        }

        #data riwayat cuti
        $riwayatCuti = ApiHelper::getDataSDM012IdJson('cuti_pegawais' . $propertyGet, $this->token);

        $data['riwayatCuti'] = $riwayatCuti['data']['hydra:member'];

        $data['jumlahRiwayat'] = $riwayatCuti['data']['hydra:totalItems'];

        $jenisCuti = ApiHelper::getDataSDM012('jns_cutis', $this->token);
        #data riwayat cuti
        $riwayatCuti = ApiHelper::getDataSDM012IdJson('cuti_pegawais' . $propertyGet, $this->token);

        $data['riwayatCuti'] = $riwayatCuti['data']['hydra:member'];

        $data['jumlahRiwayat'] = $riwayatCuti['data']['hydra:totalItems'];

        $jenisCuti = ApiHelper::getDataSDM012('jns_cutis', $this->token);
        $data['jenisCuti'] = $jenisCuti['data'];

        $jenisCutiTambahan = ApiHelper::getDataSDM012('jns_cuti_tambahans', $this->token);
        $data['jenisCutiTambahan'] = $jenisCutiTambahan['data'];

        $rekapCuti = ApiHelper::getDataSDM012('rekap_cutis' . $propertyGet . '&tahun=' . $this->tahunLalu, $this->token);

        if (!isset($rekapCuti['data'][0]->sisaCuti)) {
            $data['sisaCutiSebelumnya'] = 0;
            $data['sisaCutiSebelumnyaNonMax'] = 0;
        } else {
            $data['sisaCutiSebelumnya'] = $rekapCuti['data'][0]->sisaCuti ?? 0;
            $data['sisaCutiSebelumnyaNonMax'] = $rekapCuti['data'][0]->sisaCutiNonMax ?? 0;
        }

        $data['IdRekapSisaYmin1'] = $rekapCuti['data'][0]->id ?? '';

        $statusCutiPegawai = ApiHelper::getDataSDM012('cuti_pegawai_statuses' . $propertyGet, $this->token);

        if (!isset($statusCutiPegawai['data'][0])) {
            $data['cutiStatus'] = null;
        } else {
            $data['cutiStatus'] = $statusCutiPegawai['data'][0];
        }

        $dataPost = [
            "pegawaiId" => $_GET['string'],
            "year" => date('Y'),
            "agama" => $userData['data']->agamaId
        ];

        $cutiDipakai = ApiHelper::postDataSDM012('rekap_cutis/cuti_diambil', $this->token, $dataPost);
        $data['cutiDipakai'] = $cutiDipakai['data']->CutiTahunanDipakai;

        // pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        $data['totalItems'] = $data['jumlahUsulan'];
        $data['totalRiwayats'] = $data['jumlahRiwayat'];
        $data['totalPages'] = ceil($data['jumlahUsulan'] / $itemsPerPage);
        $data['totalPagesRiwayats'] = ceil($data['jumlahRiwayat'] / $itemsPerPage);
        $data['totalRiwayats'] = $data['jumlahRiwayat'];
        $data['totalPages'] = ceil($data['jumlahUsulan'] / $itemsPerPage);
        $data['totalPagesRiwayat'] = ceil($data['jumlahRiwayat'] / $itemsPerPage);
        $data['currentPage'] = $page;
        $data['numberOfElements'] = (($page - 1) * $itemsPerPage) + 1;

        return view('presensi.cuti.permohonan_cuti', $data);
    }

    public static function viewStatus($status)
    {
        $statusList = [
            'Draft',
            'Kirim Permohonan',
            'Disetujui',
            'Permohonan Pembatalan',
            'Cuti Batal',
            'Cuti Batal Panggilan Dinas',
            'Cuti Di tolak'
        ];

        return $statusList[$status];
    }

    public function cetak()
    {
        $permohonanCuti = ApiHelper::getDataSDM012('permohonan_cutis/' . $_GET['key'], $this->token);
        $data = $permohonanCuti['data'];

        $userData = LoginHelper::get('master_pegawais/' . $data->nip9 . '/data', $this->token);

        $statusCutiPegawai = ApiHelper::getDataSDM012('cuti_pegawai_statuses?pegawaiId=' . $_GET['string'], $this->token);
        $cutiStatus = $statusCutiPegawai['data'][0];

        $getAtasanLangsung = ApiHelper::postDataIAM('pegawais/atasan', $this->token, ["pegawaiId" => $_GET['string']]);
        if (in_array($data->jnsCuti->id, array(1, 2, 3, 4))) {
            $DataAtasanBerwenang = $getAtasanLangsung['data']->jabatanPegawais->pybCutiDiatur;
        } else {
            $DataAtasanBerwenang = $getAtasanLangsung['data']->jabatanPegawais->pyb;
        }

        $atasanBerwenang = $DataAtasanBerwenang->unitName;

        $jabatanPyb = AppHelper::normalisasiNamaJabatan($DataAtasanBerwenang->jabatanName . ' ' . $atasanBerwenang);

        //retype tanggal mulai untuk dijadikan sebagai tanggalAcuan
        $tglMulaiExp = explode('T', $data->tanggalMulai);

        $dataPost = [
            "pegawaiId" => $_GET['string'],
            "year" => $data->tahun,
            "agama" => $userData['data']->data->agamaId,
            "tanggalAcuan" => str_replace('-', '', $tglMulaiExp[0])
        ];

        $cutiDipakai = ApiHelper::postDataSDM012('rekap_cutis/sisa_cuti', $this->token, $dataPost);
        $sisaCuti = $cutiDipakai['data'];

        $n = $sisaCuti->kuotaTahunIni;
        $n1 = $sisaCuti->kuotaTahunLalu;
        $n2 = $sisaCuti->kuotaTahunLalu2;

        $ck_cb = $ck_cbs = $ck_ct = $ck_cltn = $ck_cap = $ck_cs = $ck_pal = $ck_pyb = '';
        $ctt_cs = $ctt_cltn = $ctt_cap = $ctt_cb = $ctt_cbs = '';

        $tempat = 'Jakarta';
        $tanggal_buat = AppHelper::indonesian_date($data->tanggalBuat, 'j F Y', '');
        $atasan_berwenang = $jabatanPyb;
        $tempat_pejabat = 'Jakarta';

        $nama = ucwords(strtolower($data->nama));
        $jabatan = $data->jabatan;
        $unit_organisasi = $data->unitOrganisasi;
        $nip = $data->nip18;
        $tanggalMulai = AppHelper::indonesian_date($data->tanggalMulai, 'j F Y', '');
        $tanggalSelesai = AppHelper::indonesian_date($data->tanggalSelesai, 'j F Y', '');
        $telp = $data->phone;
        $alamatSementara = $data->alamatSementara;
        $alasan = $data->alasan;
        $namaAtasanLangsung = $data->namaAtasanLangsung;
        $nipAtasanLangsung = $data->nipAtasanLangsung;
        $nipAtasanBerwenang = $data->nipAtasanBerwenang;
        $namaAtasanBerwenang = $data->namaAtasanBerwenang;
        $lamaCuti = $data->lamaCuti;
        $lamaCltn = $cutiStatus->lamaCltn ?? 0;

        //mencari masa kerja
        $tanggalBuat = new DateTime($data->tanggalBuat);
        $tmtCpns = new DateTime($cutiStatus->tmtCpns);
        $interval = $tmtCpns->diff($tanggalBuat);
        $mks = ($interval->y * 12) + $interval->m - $lamaCltn;

        $masa_kerja = floor($mks / 12) . ' Tahun ' . ($mks % 12) . ' Bulan';

        if ('1' == $data->jnsCuti->id) {
            $ck_cb = "&#10004;";
            if ('1' == $data->statusAlasanPenting) {
                $alasan = "Alasan keagamaan";
            } else if ('2' == $data->statusAlasanPenting) {
                $alasan = "melahirkan anak ke 4 dst";
            } elseif ('9' == $data->statusAlasanPenting) {
                $alasan = "melakukan ibadah haji pertama kali";
            } else {
                $alasan = "Masa kerja $masa_kerja";
            }
        } elseif ('3' == $data->jnsCuti->id) {
            $ck_cbs = "&#10004;";
            $alasan = "Melahirkan";
        } elseif ('2' == $data->jnsCuti->id) {
            $ck_cs = "&#10004;";
            if ('1' == $data->keguguran) {
                $alasan = "Keguguran";
            } else {
                $alasan = $data->alasan;
            }
        } elseif ('7' == $data->jnsCuti->id) {
            $ck_cs = "&#10004;";
            if ('1' == $data->keguguran) {
                $alasan = "Keguguran";
            } else {
                $alasan = $data->alasan;
            }
        } elseif ('4' == $data->jnsCuti->id) {
            $ck_cap = "&#10004;";
            switch ($data->statusAlasanPenting) {
                case '1':
                    $alasan = "orang tua/suami/istri/anak/kakak/adik meninggal dunia";
                    break;
                case '2':
                    $alasan = "mertua/menantu meninggal dunia";
                    break;
                case '3':
                    $alasan = "orang tua, mertua, istri/suami, anak, saudara kandung, menantu meninggal dunia";
                    break;
                case '4':
                    $alasan = "orang tua, mertua, istri/suami, anak, saudara kandung, menantu sakit keras";
                    break;
                case '5':
                    $alasan = "mengurus hak-hak dari salah satu anggota keluarga yang meninggal dunia";
                    break;
                case '6':
                    $alasan = "melangsungkan perkawinan";
                    break;
                case '7':
                    $alasan = "mengalami musibah kebakaran rumah atau bencana alam";
                    break;
                case '8':
                    $alasan = "menemani istri melahirkan secara normal ataupun operasi cessar";
                    break;
                default:
                    break;
            }
        } elseif ('9' == $data->jnsCuti->id) {
            $ck_ct = "&#10004;";
        } elseif ('6' == $data->jnsCuti->id || '10' == $data->jnsCuti->id) {
            $ck_ct = "&#10004;";
        } elseif ('8' == $data->jnsCuti->id) {
            $ck_ct = "&#10004;";
        }

        $template = new TemplateProcessor(storage_path('template/TEMPLATE_CUTI_LARAVEL.docx'));

        $paper = new Paper();
        $paper->setSize('Legal');

        $template->setValue('tempat', $tempat);
        $template->setValue('tanggal_buat', $tanggal_buat);
        $template->setValue('atasan_berwenang', $atasan_berwenang);
        $template->setValue('tempat_pejabat', $tempat_pejabat);
        $template->setValue('nama', $nama);
        $template->setValue('jabatan', $jabatan);
        $template->setValue('unit_organisasi', $unit_organisasi);
        $template->setValue('nip', $nip);
        $template->setValue('tanggal_mulai', $tanggalMulai);
        $template->setValue('tanggal_selesai', $tanggalSelesai);
        $template->setValue('alasan', $alasan);
        $template->setValue('alamat_sementara', $alamatSementara);
        $template->setValue('lama_cuti', $lamaCuti);
        $template->setValue('telp', $telp);
        $template->setValue('nip_atasan_langsung', $nipAtasanLangsung);
        $template->setValue('nama_atasan_langsung', $namaAtasanLangsung);
        $template->setValue('nip_atasan_berwenang', $nipAtasanBerwenang);
        $template->setValue('nama_atasan_berwenang', $namaAtasanBerwenang);
        $template->setValue('masa_kerja', $masa_kerja);
        $template->setValue('ck_cb', $ck_cb);
        $template->setValue('ck_ct', $ck_ct);
        $template->setValue('ck_cs', $ck_cs);
        $template->setValue('ck_cap', $ck_cap);
        $template->setValue('ck_cbs', $ck_cbs);
        $template->setValue('ck_cltn', $ck_cltn);
        $template->setValue('ck_pal', $ck_pal);
        $template->setValue('ck_pyb', $ck_pyb);
        $template->setValue('n', $n);
        $template->setValue('n1', $n1);
        $template->setValue('n2', $n2);
        $template->setValue('ctt_cb', $ctt_cb);
        $template->setValue('ctt_cbs', $ctt_cbs);
        $template->setValue('ctt_cap', $ctt_cap);
        $template->setValue('ctt_cs', $ctt_cs);
        $template->setValue('ctt_cltn', $ctt_cltn);

        $filename = 'permohonan_cuti.docx';

        header('Content-Type: application/octet-stream');
        header("Content-Disposition: attachment; filename=$filename");

        $template->saveAs('php://output');
    }

    public function getHariKerja(Request $request)
    {

        if (isset($request->start) && isset($request->end)) {
            $start = $request->start;
            $end = $request->end;
        }

        if (isset($request->tanggalCuti)) {
            $tanggalCuti = explode(' - ', $request->tanggalCuti);
            $tanggalMulai = explode('/', $tanggalCuti[0]);
            $tanggalSelesai = explode('/', $tanggalCuti[0]);

            $start = $tanggalMulai[2] . '-' . $tanggalMulai[0] . '-' . $tanggalMulai[1];
            $end = $tanggalSelesai[2] . '-' . $tanggalSelesai[0] . '-' . $tanggalSelesai[1];
        }

        $data = [
            "tanggalMulai" => $start,
            "tanggalSelesai" => $end,
            "provinsi" => '',
            "kota" => '',
            "agama" => session('user')['agamaId']
        ];

        $hariKerja = ApiHelper::postDataSDM012('rekap_cutis/lama_cuti', $this->token, $data);

        if (1 == $hariKerja['status']) {
            return ['status' => 1, 'message' => 'success', 'lama' => $hariKerja['data']->lamaHariKerja];
        } else {
            return ['status' => 0, 'message' => 'failed'];
        }
    }

    public function showTest()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('login_info')['_role_list'], 'cuti/administrasi');

        return view('presensi.tes', $data);
    }

    public function createPermohonan(Request $request)
    {
        //get role
        $userRole = session()->get('login_info')['_role_list'];

        #data href
        $user = LoginHelper::get('master_pegawais?pegawaiId=' . $_GET['string'], $this->token);
        $userData = $user['data'][0];

        $messages = [
            'required' => ':attribute wajib diisi !!!',
            'min' => ':attribute harus diisi minimal :min karakter !!!',
            'max' => ':attribute harus diisi maksimal :max karakter !!!',
        ];

        #setting validation field
        $dataCheck = [
            'tanggalCuti' => 'required',
            'alamatSementara' => 'required',
            'phone' => 'required',
            'lamaCuti' => 'required',
            'jnsCuti' => 'required'
        ];

        // alasan jenis cuti melahirkan
        if ('3' == $request->jnsCuti) {
            $request->alasan = "Melahirkan";
        }

        $validator = Validator::make($request->all(), $dataCheck, $messages);

        if ('8' != $request->jnsCuti) {
            $validator->validate();

            $tanggalCuti = explode(' - ', $request->tanggalCuti);
            $tglMulaiExp = explode('/', $tanggalCuti[0]);
            $tglSelesaiExp = explode('/', $tanggalCuti[1]);

            if (isset($request->idPermohonan)) {
                $permohonanCuti = ApiHelper::getDataSDM012('permohonan_cutis/' . $request->idPermohonan, $this->token);
                $tanggalBuat = explode('T', $permohonanCuti['data']->tanggalBuat);

                if (
                    (strtotime($tanggalBuat[0]) > strtotime($tglMulaiExp[2] . '-' . $tglMulaiExp[0] . '-' . $tglMulaiExp[1]))
                    &&
                    in_array($request->jnsCuti, array(1, 3, 6, 9, 10))
                ) {
                    if (false == in_array('ROLE_UPK_PUSAT', $userRole)) {
                        $validator->errors()->add(
                            'tanggalCuti', 'Pengajuan perubahan tanggal mulai cuti maksimal sama dengan tanggal buat yaitu ' . AppHelper::convertDateDMY($permohonanCuti['data']->tanggalBuat)
                        );
                    }
                }

            }

            if (
                (strtotime(date('Ymd')) > strtotime($tglMulaiExp[2] . $tglMulaiExp[0] . $tglMulaiExp[1]))
                &&
                in_array($request->jnsCuti, array(1, 3, 6, 9, 10)) && !isset($request->idPermohonan)
            ) {
                if (false == in_array('ROLE_UPK_PUSAT', $userRole)) {
                    $validator->errors()->add(
                        'tanggalCuti', 'Pengajuan permohonan cuti maksimal dibuat tanggal ' . AppHelper::convertDate($tanggalCuti[0])
                    );
                }
            }

            if (in_array($request->jnsCuti, array(6, 10)) && (date('Y') !== $tglSelesaiExp[2] || date('Y') !== $tglMulaiExp[2])) {
                if (false == in_array('ROLE_UPK_PUSAT', $userRole)) {
                    $validator->errors()->add(
                        'tahun', 'Pengajuan permohonan cuti tahunan non lintas tahun hanya bisa diajukan pada tahun yang sama dengan tahun pengajuan,
                        Silahkan menggunakan cuti lintas tahun untuk pengajuan cuti beda tahun'
                    );
                }
            }

            $paramsData = [
                "tanggalMulai" => $tglMulaiExp[2] . $tglMulaiExp[0] . $tglMulaiExp[1],
                "tanggalSelesai" => $tglSelesaiExp[2] . $tglSelesaiExp[0] . $tglSelesaiExp[1],
                "provinsi" => '',
                "kota" => '',
                "agama" => $userData->agamaId
            ];

            if ('9' == $request->jnsCuti) {
                $dataPost = [
                    "pegawaiId" => $_GET['string'],
                    "year" => date('Y'),
                    "agama" => $userData->agamaId,
                    "tanggalAcuan" => $tglMulaiExp[2] . $tglMulaiExp[0] . $tglMulaiExp[1]
                ];

                $hariKerja = ApiHelper::postDataSDM012('rekap_cutis/lama_cuti_clt', $this->token, $paramsData);
                $cutiDiambil = $hariKerja['data']->cutiDiambil;
                $cutiLT = $hariKerja['data']->cutiLT;
                $tahunLT = date('Y') + 1;

                $cutiDipakai = ApiHelper::postDataSDM012('rekap_cutis/sisa_cuti', $this->token, $dataPost);
                $sisaCuti = $cutiDipakai['data'];

                $jumlahCutiDitangguhkanLt = $sisaCuti->kuotaTahunIni + $sisaCuti->kuotaTahunLalu - $cutiDiambil;
                if ($jumlahCutiDitangguhkanLt > 12) {
                    $jumlahCutiDitangguhkanLt = 12;
                }

//                dump($dataPost);
//                dd($jumlahCutiDitangguhkanLt);

            } else {
                $hariKerja = ApiHelper::postDataSDM012('rekap_cutis/lama_cuti', $this->token, $paramsData);
                $cutiDiambil = $hariKerja['data']->lamaHariKerja;
            }

            if ($hariKerja['data']->lamaHariKerja != $request->lamaCuti && !in_array($request->jnsCuti, array(10, 3))) {
                $validator->errors()->add(
                    'lamaCuti', 'Lama Cuti yang anda masukkan <b>' . $request->lamaCuti . '</b> hari seharusnya adalah <b>' . $hariKerja['data']->lamaHariKerja . '</b>'
                );
            }


            $lamaCuti = $hariKerja['data']->lamaHariKerja;

            // validator persalinanKe
            if ($request->persalinanKe != $request->cutiPersalinanKe && '3' == $request->jnsCuti) {
                $validator->errors()->add(
                    'persalinanKe',
                    'ngapain bestie!'
                );
            }

            $validator->validated();
        }

        if (0 < count($validator->errors()->all())) {
            return redirect('cuti/administrasi?string=' . $_GET['string'])->withErrors($validator->errors()->all());
        }

        $getAtasanLangsung = ApiHelper::postDataIAM('pegawais/atasan', $this->token, ["pegawaiId" => $_GET['string']]);

        if (in_array($request->jnsCuti, array(1, 2, 3, 4))) {
            $atasanLangsung = $getAtasanLangsung['data']->jabatanPegawais->atasanCuti;
            $atasanBerwenang = $getAtasanLangsung['data']->jabatanPegawais->pybCutiDiatur;
        } else {
            $atasanLangsung = $getAtasanLangsung['data']->jabatanPegawais->atasan;
            $atasanBerwenang = $getAtasanLangsung['data']->jabatanPegawais->pyb;
        }

        $data = [
            'jnsCuti' => '/jns_cutis/' . $request->idJnsCuti,
            'pegawaiId' => $userData->pegawaiId,
            'nama' => $userData->namaPegawai,
            'nip9' => $userData->nip9,
            'nip18' => $userData->nip18,
            'lamaCuti' => (float) $lamaCuti,
            'kantor' => $userData->kantor,
            'unitOrganisasi' => $userData->unit,
            'alasan' => $request->alasan,
            'masaKerja' => $request->masaKerja,
            'alamatSementara' => $request->alamatSementara,
            'tahun' => date('Y'),
            'jabatan' => $userData->jabatan,
            'tanggalBuat' => date('Y-m-d') . 'T' . date('H:i:s'),
            'namaBuat' => session('user')['pegawaiId'],
            'atasanLangsung' => $atasanLangsung->pegawaiId,
            'nipAtasanLangsung' => $atasanLangsung->nip18,
            'namaAtasanLangsung' => $atasanLangsung->name,
            'atasanBerwenang' => $atasanBerwenang->pegawaiId,
            'nipAtasanBerwenang' => $atasanBerwenang->nip18,
            'namaAtasanBerwenang' => $atasanBerwenang->name,
            'status' => 0,
            'yth' => $atasanBerwenang->jabatanName,
            'kantorId' => $userData->kantorId,
            'phone' => $request->phone,
            'nm_pangkat' => $userData->pangkat
        ];

        // cuti melahirkan belum pernah diajukan
        if ('3' == $request->jnsCuti) {
            $data['persalinanKe'] = (int) $request->persalinanKe ?? '';
        }

        if ('8' != $request->jnsCuti) {
            $data['tanggalMulai'] = $tanggalCuti[0];
            $data['tanggalSelesai'] = $tanggalCuti[1];
        }

        if ('6' == $request->jnsCuti || '10' == $request->jnsCuti) {
            $data['cutiDiambil'] = (float) $cutiDiambil;
        }

        if ('10' == $request->jnsCuti) {
            $data['tipeCutiHalf'] = (int) $request->tipeCutiHalf;
        }

        if ('9' == $request->jnsCuti) {
            $data['cutiDiambil'] = (float) $cutiDiambil;
            $data['jumlahCutiLt'] = $cutiLT;
            $data['tahunLt'] = (string) $tahunLT;
            $data['jumlahCutiDitangguhkanLt'] = $jumlahCutiDitangguhkanLt;
        }
//        dd(json_encode($data));
        if (in_array($request->jnsCuti, array(6, 9, 10))) {
            if (strtotime(date('Y')) > strtotime($tglMulaiExp[2])) {
                $rekapCuti = ApiHelper::getDataSDM012('rekap_cutis?pegawaiId=' . $_GET['string'] . '&tahun=' . $tglMulaiExp[2], $this->token);
                $sisaCutiSebelumnya = $rekapCuti['data'][0]->sisaCutiNonMax;
                $data['tahun'] = $tglMulaiExp[2];
                $sisaCutiMax = $sisaCutiSebelumnya - (float) $request->lamaCuti;
                if (12 <= $sisaCutiMax) {
                    $sisaCuti = 12;
                } else {
                    $sisaCuti = $sisaCutiMax;
                }
                $dataRekap = [
                    'sisaCuti' => $sisaCuti,
                    'sisaCutiNonMax' => $sisaCutiMax
                ];
            }
        }

        if (isset($request->idPermohonan)) {
            $responseUsulan = ApiHelper::patchDataSDM012('permohonan_cutis/' . $request->idPermohonan, $this->token, $data);
        } else {
            $responseUsulan = ApiHelper::postDataSDM012('permohonan_cutis', $this->token, $data);
        }

        if (isset($request->idPermohonan)) {

            $message = 'Data Berhasil diubah!';

        } else {

            $message = 'Data Berhasil ditambah!';
        }

        if ($responseUsulan['status'] == 1) {
            if (in_array($request->jnsCuti, array(6, 9, 10))) {
                if (strtotime(date('Y')) > strtotime($tglMulaiExp[2])) {
                    $responseRekap = ApiHelper::patchDataSDM012('rekap_cutis/' . $rekapCuti['data'][0]->id, $this->token, $dataRekap);
                }
            }
            return redirect('cuti/administrasi?string=' . $_GET['string'])->with('success', $message);
        } else {
            return redirect('cuti/administrasi?string=' . $_GET['string'])->with('error', $responseUsulan['message']);
        }
    }

    public function createPermohonanCutiTambahan(Request $request)
    {
        $idPermohonan = $request->getIdPermohonan;
        $user = session()->get('user');

        #data href
        $pegawai = LoginHelper::iamget('api/pegawais/' . $_GET['string'], $this->token);
        $user = LoginHelper::get('master_pegawais/' . $pegawai['data']->nip9 . '/data', $this->token);
        $userData = $user['data']->data;

        $messages = [
            'required' => ':attribute wajib diisi !!!',
            'min' => ':attribute harus diisi minimal :min karakter !!!',
            'max' => ':attribute harus diisi maksimal :max karakter !!!',
        ];

        #setting validation field
        $dataCheck = [
            'alasanCutiTambahan' => 'required',
            'lamaCutiTambahan' => 'required',
            'jnsCutiTambahan' => 'required'
        ];

        $validator = Validator::make($request->all(), $dataCheck, $messages);

        $tanggalCutiTambahan = explode(' - ', $request->tanggalCutiTambahan);
        $tglMulaiExp = explode('/', $tanggalCutiTambahan[0]);
        $tglSelesaiExp = explode('/', $tanggalCutiTambahan[1]);

        $getAtasanLangsung = ApiHelper::postDataIAM('pegawais/atasan', $this->token, ["pegawaiId" => $_GET['string']]);
        $atasanLangsung = $getAtasanLangsung['data']->jabatanPegawais->atasan;

        $getAtasanBerwenang = ApiHelper::postDataIAM('kantors/kepala_kantor', $this->token, ["kantorId" => $userData->kantorId]);
        $atasanBerwenang = $getAtasanBerwenang['data']->kepala_kantor;

        $data = [
            'pegawaiId' => $userData->pegawaiId,
            'tanggalMulai' => substr($tanggalCutiTambahan[0], 6, 4) . '-' . substr($tanggalCutiTambahan[0], 0, 2) . '-' . substr($tanggalCutiTambahan[0], 3, 2),
            'tanggalSelesai' => substr($tanggalCutiTambahan[1], 6, 4) . '-' . substr($tanggalCutiTambahan[1], 0, 2) . '-' . substr($tanggalCutiTambahan[1], 3, 2),
            'lamaCutiTambahan' => (float) $request->lamaCutiTambahan,
            'alasanCutiTambahan' => $request->alasanCutiTambahan,
            'status' => 0,
            'permohonanCuti' => '/permohonan_cutis/' . $idPermohonan,
            'jenis' => '/jns_cuti_tambahans/' . $request->jenis,
            'atasanLangsung' => $atasanLangsung->pegawaiId,
            'nipAtasanLangsung' => $atasanLangsung->nip18,
            'namaAtasanLangsung' => $atasanLangsung->name,
            'atasanBerwenang' => $atasanBerwenang->pegawaiId,
            'nipAtasanBerwenang' => $atasanBerwenang->nip18,
            'namaAtasanBerwenang' => $atasanBerwenang->name,
        ];

        if (isset($request->idCutiTambahan)) {
            $responseUsulan = ApiHelper::patchDataSDM012('permohonan_cuti_tambahans/' . $request->idCutiTambahan, $this->token, $data);

        } else {
            $responseUsulan = ApiHelper::postDataSDM012('permohonan_cuti_tambahans', $this->token, $data);
        }

        if (isset($request->idCutiTambahan)) {

            $message = 'Data Berhasil diubah!';

        } else {

            $message = 'Data Berhasil ditambah!';
        }

        if ($responseUsulan['status'] == 1) {
            return redirect('cuti/administrasi?string=' . $_GET['string'])->with('success', $message);
        } else {
            return redirect('cuti/administrasi?string=' . $_GET['string'])->with('error', $responseUsulan['message']);
        }
    }

    public function deletePermohonan(Request $request)
    {
        $idPermohonan = $request->idPermohonan;
        $lamaCuti = $request->lamaCuti;
        $tahun = $request->tahun;

        $responseUsulan = ApiHelper::deleteDataSDM012('permohonan_cutis/' . $idPermohonan, $this->token);

        #dump($responseUsulan);
        if (strtotime(date('Y')) > strtotime($tahun)) {
            $rekapCuti = ApiHelper::getDataSDM012('rekap_cutis?pegawaiId=' . $_GET['string'] . '&tahun=' . $tahun, $this->token);
            $sisaCutiSebelumnya = $rekapCuti['data'][0]->sisaCutiNonMax;
            $sisaCutiMax = $sisaCutiSebelumnya + $lamaCuti;
            if (12 <= $sisaCutiMax) {
                $sisaCuti = 12;
            } else {
                $sisaCuti = $sisaCutiMax;
            }
            $dataRekap = [
                'sisaCuti' => $sisaCuti,
                'sisaCutiNonMax' => $sisaCutiMax
            ];
        }

        if (1 == $responseUsulan['status']) {
            $responseRekap = ApiHelper::patchDataSDM012('rekap_cutis/' . $rekapCuti['data'][0]->id, $this->token, $dataRekap);
            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }

    public function deletePermohonanCutiTambahan(Request $request)
    {
        $idPermohonan = $request->idPermohonan;


        $responseUsulan = ApiHelper::deleteDataSDM012('permohonan_cuti_tambahans/' . $idPermohonan, $this->token);


        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }

    public function kirimPermohonan(Request $request)
    {
        $idPermohonan = $request->idPermohonan;

        $body = [
            'status' => 1
        ];

        $responseUsulan = ApiHelper::patchDataSDM012('permohonan_cutis/' . $idPermohonan, $this->token, $body);

        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }

    public function kirimPermohonanCutiTambahan(Request $request)
    {
        $idPermohonan = $request->idPermohonan;

        $body = [
            'status' => 1
        ];

        $responseUsulan = ApiHelper::patchDataSDM012('permohonan_cuti_tambahans/' . $idPermohonan, $this->token, $body);

        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }

    public function showPersetujuan()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('login_info')['_role_list'], 'cuti/persetujuan');

        $userLogin = session()->get('user');

        $userRole = session()->get('login_info')['_role_list'];

        $itemsPerPage = 10;

        $propertyGet = '?itemsPerPage=' . $itemsPerPage;

        if (isset($_GET)) {
            foreach ($_GET as $key => $value) {

                $propertyGet .= '&' . $key . '=' . $value;

                if (null == $key) {
                    $propertyGet = '';
                }
            }
        }

        if (!isset($page)) {
            $page = 0;
        }

        $kantorId = (session()->get('user')['kantorId']);

        $uk3tsps = ApiHelper::getDataSDM012IdJson('uk3tsps/active/pmk', $this->token);

        //get data kantor uk3tsp dan rubah menjadi array
        $arr_uk3tsp = [];
        if (null !== $uk3tsps['data']['uk3tsp']) {
            $dataUk3tsp = $uk3tsps['data']['uk3tsp']->kantor;
            foreach ($dataUk3tsp as $uk3tsp) {
                array_push($arr_uk3tsp, $uk3tsp->kantorId);
            }
        }

        //flag kantor yang mendapat cuti tambahan
        if (in_array($kantorId, $arr_uk3tsp)) {
            $data['hakcutitambahan'] = 1;
        } else {
            $data['hakcutitambahan'] = 0;
        }

        $hakCutiTambahan = $data['hakcutitambahan'];

        $body = [
            'pegawaiId' => $userLogin['pegawaiId'],
            'offset' => $page,
            'limit' => $itemsPerPage
        ];
        $permohonanCuti = ApiHelper::postDataSDM012('permohonan_cutis/get_list_persetujuan_cuti', $this->token, $body);

        $data['permohonanCuti'] = $permohonanCuti['data']->list;

        #data permohonan cuti Tambahan
        $permohonanCutiTambahan = ApiHelper::postDataSDM012('permohonan_cuti_tambahans/get_list_persetujuan_cuti_tambahan', $this->token, $body);
        $data['permohonanCutiTambahan'] = $permohonanCutiTambahan['data']->list;

        for ($i = 0, $iMax = count($data['permohonanCuti']); $i < $iMax; $i++) {
            $data['permohonanCuti'][$i]->ketStatus = self::viewStatus($data['permohonanCuti'][$i]->status);
        }

        for ($i = 0; $i < count($data['permohonanCutiTambahan']); $i++) {
            $data['permohonanCutiTambahan'][$i]->ketStatus = self::viewStatus($data['permohonanCutiTambahan'][$i]->status);
        }


        if (null == $permohonanCuti['data']->count) {
            $data['jumlahPermohonan'] = 0;
        } else {
            $data['jumlahPermohonan'] = $permohonanCuti['data']->count;
        }

        // pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        $data['totalItems'] = $data['jumlahPermohonan'];
        $data['totalPages'] = ceil($data['jumlahPermohonan'] / $itemsPerPage);
        $data['currentPage'] = $page;
        $data['numberOfElements'] = (($page - 1) * $itemsPerPage) + 1;

        #set alert
        if (6 == $userLogin['levelJabatan'] && false == in_array('ROLE_UPK_PUSAT', $userRole)) {
            $data['alert_message'] = '<div class="alert alert-custom alert-warning" role="alert">
                                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                        <div class="alert-text">
                                            <h4 class="alert-heading">Perhatian!</h4>
                                            <p>Menu ini hanya dapat diakses oleh level jabatan 4 keatas</p>
                                        </div>
                                    </div>';
        } else {
            $data['alert_message'] = null;
        }

        return view('presensi.cuti.show_permohonan', $data);
    }

    public function showPersetujuanTambahan()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('login_info')['_role_list'], 'cuti/tambahan/persetujuan');

        $userLogin = session()->get('user');

        $userRole = session()->get('login_info')['_role_list'];

        $itemsPerPage = 10;

        $propertyGet = '?itemsPerPage=' . $itemsPerPage;

        if (isset($_GET)) {
            foreach ($_GET as $key => $value) {

                $propertyGet .= '&' . $key . '=' . $value;

                if (null == $key) {
                    $propertyGet = '';
                }
            }
        }

        if (!isset($page)) {
            $page = 0;
        }
        $kantorId = (session()->get('user')['kantorId']);

        $uk3tsps = ApiHelper::getDataSDM012IdJson('uk3tsps/active/pmk', $this->token);

        //get data kantor uk3tsp dan rubah menjadi array
        $arr_uk3tsp = [];
        if (null !== $uk3tsps['data']['uk3tsp']) {
            $dataUk3tsp = $uk3tsps['data']['uk3tsp']->kantor;
            foreach ($dataUk3tsp as $uk3tsp) {
                array_push($arr_uk3tsp, $uk3tsp->kantorId);
            }
        }

        //flag kantor yang mendapat cuti tambahan
        if (in_array($kantorId, $arr_uk3tsp)) {
            $data['hakcutitambahan'] = 1;
        } else {
            $data['hakcutitambahan'] = 0;
        }

        $hakCutiTambahan = $data['hakcutitambahan'];

        $body = [
            'pegawaiId' => $userLogin['pegawaiId'],
            'offset' => $page,
            'limit' => $itemsPerPage
        ];

        $permohonanCuti = ApiHelper::postDataSDM012('permohonan_cuti_tambahan/get_list_persetujuan_cuti', $this->token, $body);

        $data['permohonanCuti'] = $permohonanCuti['data']->list;

        #data permohonan cuti Tambahan
        $permohonanCutiTambahan = ApiHelper::getDataSDM012IdJson('permohonan_cuti_tambahans' . $propertyGet, $this->token);

        $data['permohonanCutiTambahan'] = $permohonanCuti['data']->list;

        for ($i = 0; $i < count($data['permohonanCutiTambahan']); $i++) {
            $data['permohonanCutiTambahan'][$i]->ketStatus = self::viewStatus($data['permohonanCutiTambahan'][$i]->status);
        }

        if (null == $permohonanCutiTambahan['data']->count) {
            $data['jumlahPermohonan'] = 0;
        } else {
            $data['jumlahPermohonan'] = $permohonanCutiTambahan['data']->count;
        }

        // pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        $data['totalItems'] = $data['jumlahPermohonan'];
        $data['totalPages'] = ceil($data['jumlahPermohonan'] / $itemsPerPage);
        $data['currentPage'] = $page;
        $data['numberOfElements'] = (($page - 1) * $itemsPerPage) + 1;

        #set alert
        if (6 == $userLogin['levelJabatan'] && false == in_array('ROLE_UPK_PUSAT', $userRole)) {
            $data['alert_message'] = '<div class="alert alert-custom alert-warning" role="alert">
                                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                        <div class="alert-text">
                                            <h4 class="alert-heading">Perhatian!</h4>
                                            <p>Menu ini hanya dapat diakses oleh level jabatan 4 keatas</p>
                                        </div>
                                    </div>';
        } else {
            $data['alert_message'] = null;
        }

        return view('presensi.cuti.show_permohonan', $data);
    }

    public function showPersetujuanUPK()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('login_info')['_role_list'], 'cuti/persetujuan/upk');

        $userLogin = session()->get('user');

        $userRole = session()->get('login_info')['_role_list'];

        $itemsPerPage = 10;

        $kantorId = (session()->get('user')['kantorId']);
        $uk3tsps = ApiHelper::getDataSDM012IdJson('uk3tsps/active/pmk', $this->token);

        //get data kantor uk3tsp dan rubah menjadi array
        $arr_uk3tsp = [];
        if (null !== $uk3tsps['data']['uk3tsp']) {
            $dataUk3tsp = $uk3tsps['data']['uk3tsp']->kantor;
            foreach ($dataUk3tsp as $uk3tsp) {
                array_push($arr_uk3tsp, $uk3tsp->kantorId);
            }
        }

        //flag kantor yang mendapat cuti tambahan
        if (in_array($kantorId, $arr_uk3tsp)) {
            $data['hakcutitambahan'] = 1;
        } else {
            $data['hakcutitambahan'] = 0;
        }

        $hakCutiTambahan = $data['hakcutitambahan'];

        $childs = [];
        if (strpos(implode(', ', $userRole), 'UPK_PUSAT')) {
            $sesditjen = ApiHelper::getDataIAM('kantors/active/sekretariat direktorat jenderal', $this->token);
            $getChilds = ApiHelper::getDataIAM('kantors/find_childs/by_id/' . $sesditjen['data']->kantors[0]->id, $this->token);
            $childs = $getChilds['data']->childs;
        }

        $strKantors = '';
        if (0 < count($childs)) {
            for ($i = 0; $i < count($childs); $i++) {
                $dataKantor = $childs[$i];
                $strKantors .= '&kantorId[]=' . $dataKantor->id;
            }
        }

        $propertyGet = '?itemsPerPage=' . $itemsPerPage . '&kantorId=' . $userLogin['kantorId'] . '&approvalAtasanLangsung=1&approvalPejabatBerwenang=1&status[]=1&status[]=3' . $strKantors;
        $propertyGetAll = '?itemsPerPage=' . $itemsPerPage . '&approvalAtasanLangsung=1&approvalPejabatBerwenang=1&status[]=1&status[]=3';

        if (isset($_GET)) {
            foreach ($_GET as $key => $value) {

                if ('target' != $key) {
                    $propertyGet .= '&' . $key . '=' . $value;
                    $propertyGetAll .= '&' . $key . '=' . $value;

                    if (null == $key) {
                        $propertyGet .= '';
                    }
                }
            }
        }

        // pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        #for get All Cuti UPK Pusat
        if (in_array('ROLE_UPK_PUSAT', $userRole)) {

            $permohonanCutiAll = ApiHelper::getDataSDM012IdJson('permohonan_cutis' . $propertyGetAll, $this->token);

            $data['permohonanCutiAll'] = $permohonanCutiAll['data']['hydra:member'];

            $data['jumlahPermohonanAll'] = $permohonanCutiAll['data']['hydra:totalItems'];

            $permohonanCutiTambahanAll = ApiHelper::getDataSDM012IdJson('permohonan_cuti_tambahans' . $propertyGetAll, $this->token);

            $data['permohonanCutiTambahanAll'] = $permohonanCutiTambahanAll['data']['hydra:member'];

            $data['jumlahPermohonan'] = $permohonanCutiTambahanAll['data']['hydra:totalItems'];

            for ($i = 0; $i < count($data['permohonanCutiAll']); $i++) {
                $data['permohonanCutiAll'][$i]->ketStatus = self::viewStatus($data['permohonanCutiAll'][$i]->status);
            }

            for ($i = 0; $i < count($data['permohonanCutiTambahanAll']); $i++) {
                $data['permohonanCutiTambahanAll'][$i]->ketStatus = self::viewStatus($data['permohonanCutiTambahanAll'][$i]->status);
            }

            $data['totalItemsAll'] = $data['jumlahPermohonanAll'];
            $data['totalPagesAll'] = ceil($data['jumlahPermohonanAll'] / $itemsPerPage);
            $data['currentPageAll'] = $page;
            $data['numberOfElementsAll'] = (($page - 1) * $itemsPerPage) + 1;
        } else {
            $data['permohonanCutiAll'] = [];
            $data['jumlahPermohonanAll'] = $data['totalItemsAll'] = $data['totalPagesAll'] = 0;
            $data['currentPageAll'] = $page;
            $data['numberOfElementsAll'] = (($page - 1) * $itemsPerPage) + 1;
        }

        $permohonanCuti = ApiHelper::getDataSDM012IdJson('permohonan_cutis' . $propertyGet, $this->token);

        $data['permohonanCuti'] = $permohonanCuti['data']['hydra:member'];

        $data['jumlahPermohonan'] = $permohonanCuti['data']['hydra:totalItems'];

        $permohonanCutiTambahan = ApiHelper::getDataSDM012IdJson('permohonan_cuti_tambahans' . $propertyGet, $this->token);

        $data['permohonanCutiTambahan'] = $permohonanCutiTambahan['data']['hydra:member'];

        $data['jumlahPermohonan'] = $permohonanCutiTambahan['data']['hydra:totalItems'];

        for ($i = 0; $i < count($data['permohonanCuti']); $i++) {
            $data['permohonanCuti'][$i]->ketStatus = self::viewStatus($data['permohonanCuti'][$i]->status);
        }

        for ($i = 0; $i < count($data['permohonanCutiTambahan']); $i++) {
            $data['permohonanCutiTambahan'][$i]->ketStatus = self::viewStatus($data['permohonanCutiTambahan'][$i]->status);
        }

        $data['totalItems'] = $data['jumlahPermohonan'];
        $data['totalPages'] = ceil($data['jumlahPermohonan'] / $itemsPerPage);
        $data['currentPage'] = $page;
        $data['numberOfElements'] = (($page - 1) * $itemsPerPage) + 1;

        if (isset($_GET['page']) && ($_GET['page'] * $itemsPerPage) > $data['totalItems']) {
            $data['jumlahPermohonan'] = $data['totalItems'] = $data['totalPages'] = 0;
        }

        #set alert
        if (0 == strpos(implode(', ', $userRole), 'UPK')) {
            $data['alert_message'] = '<div class="alert alert-custom alert-warning" role="alert">
                                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                        <div class="alert-text">
                                            <h4 class="alert-heading">Perhatian!</h4>
                                            <p>Menu ini hanya dapat diakses oleh UPK</p>
                                        </div>
                                    </div>';
        } else {
            $data['alert_message'] = null;
        }

        return view('presensi.cuti.show_persetujuan_upk', $data);
    }

    public function approvePermohonan(Request $request)
    {
        $idPermohonan = $request->idPermohonan;

        $body = [];

        if ('1.1' == $request->step && 0 == $request->alequalab) {
            $body['approvalAtasanLangsung'] = (int) 1;
        } else if ('1.1' == $request->step && 1 == $request->alequalab) {
            $body['approvalAtasanLangsung'] = (int) 1;
            $body['approvalAtasanBerwenang'] = (int) 1;
        } else {
            $body['approvalAtasanBerwenang'] = (int) 1;
        }

        $responseUsulan = ApiHelper::patchDataSDM012('permohonan_cutis/' . $idPermohonan, $this->token, $body);

        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }

    public function approvePermohonanTambahan(Request $request)
    {
        $idPermohonan = $request->idCutiTambahan;

        $body = [];

        if ('1.1' == $request->step && 0 == $request->alequalab) {
            $body['approvalAtasanLangsung'] = (int) 1;
        } else if ('1.1' == $request->step && 1 == $request->alequalab) {
            $body['approvalAtasanLangsung'] = (int) 1;
            $body['approvalAtasanBerwenang'] = (int) 1;
        } else {
            $body['approvalAtasanBerwenang'] = (int) 1;
        }

        $responseUsulan = ApiHelper::patchDataSDM012('permohonan_cuti_tambahans/' . $idPermohonan, $this->token, $body);

        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }

    public function approvePermohonanUPK(Request $request)
    {

        $idPermohonan = $request->idPermohonan;

        $date = str_replace(' ', 'T', date('Y-m-d H:i:s'));

        if (1 == $request->status) {
            $body = [
                'status' => (int) 2,
                'tanggalPersetujuan' => $date,
                'namaSetuju' => session()->get('user')['pegawaiId']
            ];
        } else {
            $body = [
                'status' => (int) 4,
                'status_cuti' => (int) 1,
                'alasanPembatalan' => $request->alasanPembatalan
            ];

            $bodyRiwayat = [
                'batal' => (int) 1,
                'alasanPembatalan' => $request->alasanPembatalan
            ];
        }


        $responseUsulan = ApiHelper::patchDataSDM012('permohonan_cutis/' . $idPermohonan, $this->token, $body);

        if (1 == $responseUsulan['status']) {

            if (1 == $request->status) {
                $permohonanCuti = ApiHelper::getDataSDM012('permohonan_cutis/' . $idPermohonan, $this->token);
                $data = $permohonanCuti['data'];

                $dataCuti = [
                    "pegawaiId" => $data->pegawaiId,
                    "nip9" => $data->nip9,
                    "tglAwalCutiPeg" => $data->tanggalMulai,
                    "tglAkhirCutiPeg" => $data->tanggalSelesai,
                    "jumlahCuti" => (int) $data->lamaCuti,
                    "tahun" => $data->tahun,
                    "namaBuat" => $data->namaBuat,
                    "tanggalBuat" => $data->tanggalBuat,
                    "tglSrtIzinCuti" => $data->tglSrtIzinCuti ?? null,
                    "noSrtIzinCuti" => $data->noSrtIzinCuti ?? null,
                    "nomorTicket" => $data->nomorTicket,
                    "permohonanCuti" => '/permohonan_cutis/' . $idPermohonan,
                    "jnsCuti" => '/jns_cutis/' . $data->jnsCuti->id
                ];

                if (9 == $data->jnsCuti->jnsCuti) {
                    $dataCuti["jumlahCutiDitangguhkanLt"] = (int) $data->jumlahCutiDitangguhkanLt;
                    $dataCuti["jumlahCutiLt"] = (int) $data->jumlahCutiLt;
                    $dataCuti["tahunLt"] = $data->tahunLt;

                    $jnsCuti = ApiHelper::getDataSDM012('jns_cutis?jnsCuti=8', $this->token);
                    $jnsCutiData = $jnsCuti['data'][0];

                    $dataPenundaan = [
                        "pegawaiId" => $data->pegawaiId,
                        "nip9" => $data->nip9,
                        "jumlahCuti" => (int) $data->jumlahCutiDitangguhkanLt,
                        "sisaCuti" => (int) $data->jumlahCutiDitangguhkanLt,
                        "tahun" => $data->tahun,
                        "namaBuat" => $data->namaBuat,
                        "tanggalBuat" => $data->tanggalBuat,
                        "tglSrtIzinCuti" => $data->tglSrtIzinCuti ?? null,
                        "noSrtIzinCuti" => $data->noSrtIzinCuti ?? null,
                        "nomorTicket" => $data->nomorTicket,
                        "permohonanCuti" => '/permohonan_cutis/' . $idPermohonan,
                        "jnsCuti" => '/jns_cutis/' . $jnsCutiData->id
                    ];

                    ApiHelper::postDataSDM012('cuti_pegawais', $this->token, $dataPenundaan);

                    //Hitung sisaCutiNonMax
                    $propertyGet = '?itemsPerPage=' . 10 . '&pegawaiId=' . $data->pegawaiId;

                    $rekapCuti = ApiHelper::getDataSDM012('rekap_cutis' . $propertyGet . '&tahun=' . ($data->tahun - 1), $this->token);
                    if (!isset($rekapCuti['data'][0]->sisaCuti)) {
                        $sisaCutiSebelumnya = 0;
                    } else {
                        $sisaCutiSebelumnya = $rekapCuti['data'][0]->sisaCuti ?? 0;
                    }

                    $statusCutiPegawai = ApiHelper::getDataSDM012('cuti_pegawai_statuses' . $propertyGet, $this->token);
                    if (!isset($statusCutiPegawai['data'][0])) {
                        $cutiStatus = null;
                    } else {
                        $cutiStatus = $statusCutiPegawai['data'][0];
                    }

                    $hakCuti = $cutiStatus->hakCuti ?? 0;

                    $dataPost = [
                        "pegawaiId" => $data->pegawaiId,
                        "year" => date('Y'),
                        "agama" => null
                    ];

                    $dataCutiDipakai = ApiHelper::postDataSDM012('rekap_cutis/cuti_diambil', $this->token, $dataPost);
                    $cutiDipakai = $dataCutiDipakai['data']->CutiTahunanDipakai;

                    $kuotaCuti = $hakCuti + $sisaCutiSebelumnya - $cutiDipakai;

                    $dataRekap = [
                        "pegawaiId" => $data->pegawaiId,
                        "tahun" => (int) $data->tahun,
                        "sisaCuti" => (int) $data->jumlahCutiDitangguhkanLt,
                        "dateCreated" => date('Y-m-d') . 'T' . date('H:i:s'),
                        "createdBy" => $data->namaBuat,
                        "sisaCutiNonMax" => (int) $kuotaCuti
                    ];

                    ApiHelper::postDataSDM012('rekap_cutis', $this->token, $dataRekap);
                }

                if (10 == $data->jnsCuti->jnsCuti) {
                    $dataCuti["tipeCutiHalf"] = (int) $data->tipeCutiHalf;
                }

                if (3 == $data->jnsCuti->jnsCuti) {
                    $dataCuti['persalinanKe'] = (int) $data->persalinanKe;
                }

                if (in_array($data->jnsCuti->jnsCuti, array(2, 7, 11))) {
                    $dataCuti['noSketDokter'] = $data->noSketDokter;
                    $dataCuti['statusPerawatan'] = $data->statusPerawatan;
                    $dataCuti['rumahSakit'] = $data->rumahSakit;
                    $dataCuti['keguguran'] = $data->keguguran;

                    if (1 == $data->statusPerawatan) {
                        $dataCuti["tanggalPerawatanMulai"] = $data->tanggalPerawatanMulai;
                        $dataCuti["tanggalPerawatanSelesai"] = $data->tanggalPerawatanSelesai;
                    }
                }

                if (in_array($data->jnsCuti->jnsCuti, array(1, 4))) {
                    $dataCuti["statusAlasanPenting"] = $data->statusAlasanPenting;

                    if (1 == $data->jnsCuti->jnsCuti && 9 == $data->statusAlasanPenting) {
                        $dataCuti["tglBerangkatHaji"] = $data->tglBerangkatHaji;
                        $dataCuti["tglKembaliHaji"] = $data->tglKembaliHaji;
                    }
                }

                $cutiPegawai = ApiHelper::postDataSDM012('cuti_pegawais', $this->token, $dataCuti);

            } else {

                $cutiPegawai = ApiHelper::patchDataSDM012('cuti_pegawais/' . $request->idCuti, $this->token, $bodyRiwayat);

            }

            if (1 == $cutiPegawai['status']) {

                return ['status' => 1, 'message' => 'success'];

            } else {

                return ['status' => 0, 'message' => 'error'];

            }

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }

    public function tolakPermohonan(Request $request)
    {
        $idPermohonan = $request->idPermohonan;

        $body = [
            "catatan" => $request->catatan
        ];
        $body['status'] = (int) 0;
        $body['approvalAtasanLangsung'] = null;

        $responseUsulan = ApiHelper::patchDataSDM012('permohonan_cutis/' . $idPermohonan, $this->token, $body);

        if (1 == $responseUsulan['status']) {

            $idPermohonan = $request->idCutiTambahan;

            $body = [
                "catatan" => $request->catatan
            ];
            $body['status'] = (int) 0;
            $body['approvalAtasanLangsung'] = null;

            $cutiTambahan = ApiHelper::patchDataSDM012('permohonan_cuti_tambahans/' . $idPermohonan, $this->token, $body);

            if (1 == $cutiTambahan['status']) {

                return ['status' => 1, 'message' => 'success'];

            } else {

                return ['status' => 0, 'message' => 'error'];
            }
        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }

    public function tolakPermohonanTambahan(Request $request)
    {
        $idPermohonan = $request->idCutiTambahan;

        $body = [
            "catatan" => $request->catatan
        ];

        $body['status'] = (int) 0;
        $body['approvalAtasanLangsung'] = null;

        $responseUsulan = ApiHelper::patchDataSDM012('permohonan_cuti_tambahans/' . $idPermohonan, $this->token, $body);

        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }

    public function tolakPermohonanUPK(Request $request)
    {
        $idPermohonan = $request->idPermohonan;

        $body = [
            "catatan" => $request->catatan
        ];

        $body['status'] = (int) 6;

        $responseUsulan = ApiHelper::patchDataSDM012('permohonan_cutis/' . $idPermohonan, $this->token, $body);

        if (1 == $responseUsulan['status']) {

            $idPermohonan = $request->idCutiTambahan;

            $body = [
                "catatan" => $request->catatan
            ];

            $body['status'] = (int) 6;

            $responseUsulan = ApiHelper::patchDataSDM012('permohonan_cuti_tambahans/' . $idPermohonan, $this->token, $body);

            if (1 == $responseUsulan['status']) {

                return ['status' => 1, 'message' => 'success'];

            } else {

                return ['status' => 0, 'message' => 'error'];

            }

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }

    public function permohonanPembatalanCuti(Request $request)
    {
        $idPermohonan = $request->idPermohonan;

        $body = [
            "alasanPembatalan" => $request->alasanPembatalan
        ];

        $body['status'] = (int) 3;

        $responseUsulan = ApiHelper::patchDataSDM012('permohonan_cutis/' . $idPermohonan, $this->token, $body);

        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }

    public function permohonanPembatalanCutiTambahan(Request $request)
    {
        $idPermohonan = $request->idCutiTambahan;

        $body = [
            "alasanPembatalan" => $request->alasanPembatalan
        ];

        $body['status'] = (int) 3;

        $responseUsulan = ApiHelper::patchDataSDM012('permohonan_cuti_tambahans/' . $idPermohonan, $this->token, $body);

        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }

    public function kembaliKePersetujuan(Request $request)
    {
        $idPermohonan = $request->idPermohonan;
        $idCuti = $request->idCuti;

        $body = [
            'approvalAtasanLangsung' => null,
            'approvalAtasanBerwenang' => null,
            'tanggalPersetujuan' => null,
            'namaSetuju' => null,
            'status' => (int) 0,
            'lamaCutiBatal' => (int) 0,
            'alasanPembatalan' => ''
        ];

        $responseUsulan = ApiHelper::patchDataSDM012('permohonan_cutis/' . $idPermohonan, $this->token, $body);

        if (1 == $responseUsulan['status']) {

            $cutiPegawai = ApiHelper::deleteDataSDM012('cuti_pegawais/delete_by_permohonan/' . $idPermohonan, $this->token);

            if (1 == $cutiPegawai['status']) {

                return ['status' => 1, 'message' => 'success'];

            } else {

                return ['status' => 0, 'message' => 'error'];

            }

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }

    public function kembaliKePersetujuanTambahan(Request $request)
    {
        $idPermohonan = $request->idCutiTambahan;
        $idCuti = $request->idCuti;

        $body = [
            'approvalAtasanLangsung' => null,
            'approvalAtasanBerwenang' => null,
            'tanggalPersetujuan' => null,
            'namaSetuju' => null,
            'status' => (int) 0,
            'batal' => null,
            'alasanPembatalan' => ''
        ];

        $responseUsulan = ApiHelper::patchDataSDM012('permohonan_cuti_tambahans/' . $idPermohonan, $this->token, $body);

        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }

    public function pengelolaanCuti()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('login_info')['_role_list'], 'cuti/pengelolaan_cuti');

        $userLogin = session()->get('user');

        $userRole = session()->get('login_info')['_role_list'];

        $itemsPerPage = 10;

        $propertyGet = '?itemsPerPage=' . $itemsPerPage . '&pensiun=false';

        if (0 == strpos(implode(', ', $userRole), 'UPK_PUSAT')) {
            $propertyGet .= '&kantorId=' . $userLogin['kantorId'];
        }

        $data['searchValue'] = '';
        if (isset($_GET['key'])) {
            $data['searchValue'] = $_GET['key'];
            $propertyGet .= '&namaPegawai=' . $_GET['key'];
        }

        if (isset($_GET)) {
            foreach ($_GET as $key => $value) {

                if ('key' != $key) {
                    $propertyGet .= '&' . $key . '=' . $value;
                }
            }
        }

        $pegawai = ApiHelper::getDataSDM00IdJson('master_pegawais' . $propertyGet, $this->token);

        $data['pegawai'] = $pegawai['data']['hydra:member'];

        $data['jumlahPegawai'] = $pegawai['data']['hydra:totalItems'];

        // pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        $data['totalItems'] = $data['jumlahPegawai'];
        $data['totalPages'] = ceil($data['jumlahPegawai'] / $itemsPerPage);
        $data['currentPage'] = $page;
        $data['numberOfElements'] = (($page - 1) * $itemsPerPage) + 1;

        #set alert
        if (0 == strpos(implode(', ', $userRole), 'UPK')) {
            $data['alert_message'] = '<div class="alert alert-custom alert-warning" role="alert">
                                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                        <div class="alert-text">
                                            <h4 class="alert-heading">Perhatian!</h4>
                                            <p>Menu ini hanya dapat diakses oleh level UPK</p>
                                        </div>
                                    </div>';
        } else {
            $data['alert_message'] = null;
        }

        return view('presensi.cuti.show_pegawai', $data);
    }

    public function approvePermohonanUPKTambahan(Request $request)
    {
        $idPermohonan = $request->idCutiTambahan;
        $idCuti = $request->idCuti;

        $date = str_replace(' ', 'T', date('Y-m-d H:i:s'));

        if (1 == $request->status) {
            $body = [
                'status' => (int) 2,
                'tanggalPersetujuan' => $date,
                'namaSetuju' => session()->get('user')['pegawaiId']
            ];
        } elseif (3 == $request->status) {
            $bodyRiwayat = [
                'status' => (int) 4,
                'batal' => (int) 1,
                'alasanPembatalan' => $request->alasanPembatalan
            ];
        } else {
            $body = [
                'status' => (int) 4
            ];


        }
        if (1 == $request->status) {
            $responseUsulan = ApiHelper::patchDataSDM012('permohonan_cuti_tambahans/' . $idPermohonan, $this->token, $body);
        } elseif (3 == $request->status) {
            $responseUsulan = ApiHelper::patchDataSDM012('permohonan_cuti_tambahans/' . $idCuti, $this->token, $bodyRiwayat);
        } else {
            $responseUsulan = ApiHelper::patchDataSDM012('permohonan_cuti_tambahans/' . $idPermohonan, $this->token, $body);
        }
        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }

    public function tolakPermohonanUPKTambahan(Request $request)
    {
        $idPermohonan = $request->idCutiTambahan;

        $body = [
            "catatan" => $request->catatan
        ];

        $body['status'] = (int) 6;

        $responseUsulan = ApiHelper::patchDataSDM012('permohonan_cuti_tambahans/' . $idPermohonan, $this->token, $body);

        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }

    public function sesuaikanPermohonan(Request $request)
    {
        $idPermohonan = $request->idPermohonan;
        $pegawaiId = $request->pegawaiId;
        $jnsCuti = $request->jnsCuti;

        $getAtasanLangsung = ApiHelper::postDataIAM('pegawais/atasan', $this->token, ["pegawaiId" => $pegawaiId]);

        if (in_array($jnsCuti, array(1, 2, 3, 4))) {
            $atasanLangsung = $getAtasanLangsung['data']->jabatanPegawais->atasanCuti;
            $atasanBerwenang = $getAtasanLangsung['data']->jabatanPegawais->pybCutiDiatur;
        } else {
            $atasanLangsung = $getAtasanLangsung['data']->jabatanPegawais->atasan;
            $atasanBerwenang = $getAtasanLangsung['data']->jabatanPegawais->pyb;
        }

        $body = [
            'kantor' => $getAtasanLangsung['data']->jabatanPegawais->kantorName,
            'unitOrganisasi' => $getAtasanLangsung['data']->jabatanPegawais->unitName,
            'atasanLangsung' => $atasanLangsung->pegawaiId,
            'nipAtasanLangsung' => $atasanLangsung->nip18,
            'namaAtasanLangsung' => $atasanLangsung->name,
            'atasanBerwenang' => $atasanBerwenang->pegawaiId,
            'nipAtasanBerwenang' => $atasanBerwenang->nip18,
            'namaAtasanBerwenang' => $atasanBerwenang->name,
            'kantorId' => $atasanLangsung->kantorId,
            'approvalAtasanLangsung' => null,
            'approvalAtasanBerwenang' => null,
            'tanggalPersetujuan' => null,
            'namaSetuju' => null,
            'status' => (int) 0,
            'lamaCutiBatal' => (int) 0,
            'alasanPembatalan' => ''
        ];

        $responseUsulan = ApiHelper::patchDataSDM012('permohonan_cutis/' . $idPermohonan, $this->token, $body);

        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }

    public function sesuaikanPermohonanTambahan(Request $request)
    {
        $idPermohonan = $request->idCutiTambahan;
        $pegawaiId = $request->pegawaiId;
        $jnsCuti = $request->jenis;

        $getAtasanLangsung = ApiHelper::postDataIAM('pegawais/atasan', $this->token, ["pegawaiId" => $pegawaiId]);

        if (in_array($jnsCuti, array(1, 2, 3, 4))) {
            $atasanLangsung = $getAtasanLangsung['data']->jabatanPegawais->atasanCuti;
            $atasanBerwenang = $getAtasanLangsung['data']->jabatanPegawais->pybCutiDiatur;
        } else {
            $atasanLangsung = $getAtasanLangsung['data']->jabatanPegawais->atasan;
            $atasanBerwenang = $getAtasanLangsung['data']->jabatanPegawais->pyb;
        }

        $body = [
            'kantor' => $getAtasanLangsung['data']->jabatanPegawais->kantorName,
            'unitOrganisasi' => $getAtasanLangsung['data']->jabatanPegawais->unitName,
            'atasanLangsung' => $atasanLangsung->pegawaiId,
            'nipAtasanLangsung' => $atasanLangsung->nip18,
            'namaAtasanLangsung' => $atasanLangsung->name,
            'atasanBerwenang' => $atasanBerwenang->pegawaiId,
            'nipAtasanBerwenang' => $atasanBerwenang->nip18,
            'namaAtasanBerwenang' => $atasanBerwenang->name,
            'kantorId' => $atasanLangsung->kantorId,
            'approvalAtasanLangsung' => null,
            'approvalAtasanBerwenang' => null,
            'tanggalPersetujuan' => null,
            'namaSetuju' => null,
            'status' => (int) 0,
            'alasanPembatalan' => ''
        ];

        $responseUsulan = ApiHelper::patchDataSDM012('permohonan_cuti_tambahans/' . $idPermohonan, $this->token, $body);

        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }
}
