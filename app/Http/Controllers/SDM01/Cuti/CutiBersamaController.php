<?php

namespace App\Http\Controllers\SDM01\Cuti;

use App\Helpers\ApiHelper;
use App\Helpers\AppHelper;
use App\Http\Controllers\Controller;
use App\Models\MenuModel;
use Illuminate\Http\Request;

class CutiBersamaController extends Controller
{
    //
    public function index(Request $request)
    {

        $data['menu'] = MenuModel::getMenu(session()->get('login_info')['_role_list'], 'cutibersama/administrasi');

        $authiam = ApiHelper::cekTokenIAM();

        $itemsPerPage = 10;

        $usulanLCB = ApiHelper::getDataSDM012IdJson('cuti_bersamas', $authiam['data']->token);

        $data['usulanLCB'] = $usulanLCB['data']['hydra:member'];

        $data['jumlahUsulan'] = $usulanLCB['data']['hydra:totalItems'];

        //dump($authiam['data']->token);

        // pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        $data['totalItems'] = $data['jumlahUsulan'];
        $data['totalPages'] = ceil($data['jumlahUsulan'] / $itemsPerPage);
        $data['currentPage'] = $page;
        $data['numberOfElements'] = (($page - 1) * $itemsPerPage) + 1;
        //$data['size']               = $data['cuti']->size;

        return view('presensi.cutibersama.index', $data);
    }

    public function createPermohonan(Request $request)
    {
        $authiam = ApiHelper::cekTokenIAM();

        // dd($request);

        $messages = [
            'required' => ':attribute wajib diisi !!!',
            'min' => ':attribute harus diisi minimal :min karakter !!!',
            'max' => ':attribute harus diisi maksimal :max karakter !!!',
        ];

        #setting validation field
        $dataCheck = [
            'tanggalCutiBersama' => 'required',
            'keterangan' => 'required'
        ];

        $arrTanggal = explode(' - ', $request->tanggalCutiBersama);
        $tanggalMulai = AppHelper::convertDate($arrTanggal[0]);
        $tanggalSelesai = AppHelper::convertDate($arrTanggal[1]);

        $dataUsulan = [
            'tanggalMulai' => $tanggalMulai,
            'tanggalSelesai' => $tanggalSelesai,
            "dateCreated" => date('Y-m-d') . 'T' . date('H:i:s') . "+07:00",
            "createdBy" => session('user')['pegawaiId'],
            "status" => 0,
            "keterangan" => $request->keterangan
        ];

        $this->validate($request, $dataCheck, $messages);

        //dd(json_encode($dataUsulan));

        if (isset($request->idUsulan)) {
            $responseUsulan = ApiHelper::patchDataSDM012('cuti_bersamas/' . $request->idUsulan, $authiam['data']->token, $dataUsulan);
            $message = 'Data Berhasil diubah!';
        } else {
            $responseUsulan = ApiHelper::postDataSDM012('cuti_bersamas', $authiam['data']->token, $dataUsulan);
            $message = 'Data Berhasil ditambah!';
        }

        if (1 == $responseUsulan['status']) {
            return redirect('cutibersama/administrasi')->with('success', $message);
        } else {
            return redirect('cutibersama/administrasi')->with('error', $responseUsulan['message']);
        }
    }

    public function deletePermohonan(Request $request)
    {
        $idUsulan = $request->idUsulan;

        $authiam = ApiHelper::cekTokenIAM();

        $responseUsulan = ApiHelper::deleteDataSDM012('cuti_bersamas/' . $idUsulan, $authiam['data']->token);

        #dump($responseUsulan);

        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error'];

        }
    }

    public function showPersetujuan()
    {
        $userData = session()->get('user');

        $userRole = session()->get('login_info')['_role_list'];

        $data['menu'] = MenuModel::getMenu(session()->get('login_info')['_role_list'], 'cutibersama/persetujuan');

        $authiam = ApiHelper::cekTokenIAM();

        $itemsPerPage = 5;

        // dump($userRole);
        // dump($userJabatan);

        $propertyGet = '?itemsPerPage=' . $itemsPerPage . '&status=0';

        $usulanLCB = ApiHelper::getDataSDM012IdJson('cuti_bersamas' . $propertyGet, $authiam['data']->token);

        $data['usulanLCB'] = $usulanLCB['data']['hydra:member'];

        $data['jumlahUsulan'] = $usulanLCB['data']['hydra:totalItems'];

        // pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        $data['totalItems'] = $data['jumlahUsulan'];
        $data['totalPages'] = ceil($data['jumlahUsulan'] / $itemsPerPage);
        $data['currentPage'] = $page;
        $data['numberOfElements'] = (($page - 1) * $itemsPerPage) + 1;
        //$data['size']               = $data['cuti']->size;

        #set alert
        if (6 == $userData['levelJabatan'] || false == in_array('ROLE_UPK_PUSAT', $userRole)) {
            $data['alert_message'] = '<div class="alert alert-custom alert-warning" role="alert">
                                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                        <div class="alert-text">
                                            <h4 class="alert-heading">Perhatian!</h4>
                                            <p>Menu ini hanya dapat diakses oleh level jabatan 4 keatas</p>
                                        </div>
                                    </div>';
        } else {
            $data['alert_message'] = null;
        }
        return view('presensi.cutibersama.show_permohonan', $data);
    }

    public function approveUsulan(Request $request)
    {

        $idUsulan = $request->idUsulan;

        $authiam = ApiHelper::cekTokenIAM();

        $body = [
            'status' => 1,
            'dateApproved' => date('Y-m-d') . 'T' . date('H:i:s') . "+07:00",
            'approvedBy' => session('user')['pegawaiId']
        ];

        $responseUsulan = ApiHelper::patchDataSDM012('cuti_bersamas/' . $idUsulan, $authiam['data']->token, $body);

        if (1 == $responseUsulan['status']) {

            return ['status' => 1, 'message' => 'success'];

        } else {

            return ['status' => 0, 'message' => 'error ' . json_encode($body)];

        }
    }

}
