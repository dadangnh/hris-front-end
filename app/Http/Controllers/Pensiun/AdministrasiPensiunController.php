<?php

namespace App\Http\Controllers\Pensiun;

use App\Helpers\AppHelper;
use App\Helpers\CommonVariable;
use App\Helpers\PensiunApiHelper;
use App\HelpersAppHelper;
use App\Models\MenuModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;


class AdministrasiPensiunController extends Controller
{

    public function homeAdministrasi()
    {
        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'administrasi');

        #set parameter
        $kdKanwil = session()->get('user')['kdkanwil'];

        #pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $size = 5;

        #response
        $response = PensiunApiHelper::get('/api/v1/pps/administrasi?kdKanwil=' . $kdKanwil . '&page=' . $page . '&size=' . $size);

        #data
        $data['dataPPS'] = $response['data']->data;
        $data['totalItems'] = $response['data']->totalItems;
        $data['totalPages'] = $response['data']->totalPages;
        $data['currentPage'] = $response['data']->currentPage;
        $data['numberOfElements'] = $response['data']->numberOfElements;
        $data['size'] = $response['data']->size;
        $data['page'] = $page;
        // dd($data);
        // #data get validation
        if ($response['status'] == 1) {
            return view('pensiun.administrasi.home_administrasi', $data);
        } else {
            return abort(500);
        }
    }

    public function detilAdministrasi($id)
    {
        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'administrasi');


        #set parameter id PP
        $idDekrip = AppHelper::instance()->dekrip($id);

        #endpoint get data pp by id
        $response = PensiunApiHelper::get('/api/v1/pps/' . $idDekrip);
        $data['dataPP'] = $response['data'];

        #loop data dokumen
        foreach ($response['data'] as $key) {
            $datakey = $key;
        }
        $data['dokumensPP'] = $datakey;
        // dd($data);

        #response return
        if ($response['status'] == 1) {
            return view('pensiun.administrasi.detil_administrasi', $data);
        } else {
            return redirect()->back()->with('error', 'Gagal Mengambil Data');
        }
    }

    public function kirimSKDraft(Request $request)
    {
        // dd($request->all());

        #Dari form input
        $idDekrip = AppHelper::instance()->dekrip($request->input('idPp'));
        $filesk = $request->files->get('SKPensiun');

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #upload file
        $upload_sk = $this->upload($filesk);
        if ($upload_sk['status'] == 1) {

            #file sk mpp
            $skpensiun = $upload_sk['file'];

            #all data
            $databody = [
                "pegawaiId" => session()->get('user')['pegawaiId'],
                "namaPegawai" => session()->get('user')['namaPegawai'],
                "nip18" => session()->get('user')['nip18'],
                "pangkatId" => session()->get('user')['pangkatId'],
                "namaPangkat" => session()->get('user')['pangkat'],
                "jabatanId" => session()->get('user')['jabatanId'],
                "namaJabatan" => session()->get('user')['jabatan'],
                "unitOrgId" => session()->get('user')['unitId'],
                "namaUnitOrg" => session()->get('user')['unit'],
                "kantorId" => session()->get('user')['kantorId'],
                "namaKantor" => session()->get('user')['kantor'],
                "pegawaiInputId" => session()->get('user')['pegawaiId'],
                "kdKpp" => $kdkpp,
                "kdKanwil" => $kdkanwil,
                "kdKantor" => session()->get('user')['kantorLegacyKode'],
                "skPensiun" => $skpensiun,
                "noSkPensiun" => $request->input('noSKPensiun'),
                "pangkatTerbaru" => $request->input('pangkat'),
                "tmtPangkatTerbaru" => $request->input('tglpangkat'),
                "tmtPensiun" => $request->input('tmtpensiun'),
                "keterangan" => 'SK Pensiun Di Upload oleh ' . session()->get('user')['namaPegawai']
            ];

            // dd(json_encode($databody));
            #response
            $url = env('API_URL_SDM07') . '/api/v1/pps/upload/sk_pensiun?id=' . $idDekrip;
            $response = Http::patch($url, $databody);
            // dd($response);


            #cekbalikan
            # cek status servis
            if (!$response->successful()) {

                $x = json_decode($response);

                return response() - redirect()->back()->with('error', 'Gagal Menyimpan Data');

            } else {
                #get data body
                $body = $response->getBody();
                $data = json_decode($body);
                return redirect()->back()->with('success', 'Berhasil Menyimpan Data');
            }

            return redirect()->back()->with('success', 'Berhasil Menyimpan Data');

        } else {
            return response()->redirect()->back()->with('error', 'Gagal Menyimpan Data');
        }

    }

    private function upload($file)
    {
        $filepath = $file->getRealPath();
        $nama = $file->getClientOriginalName();
        $mime = $file->getMimeType();
        $ukuran = $file->getSize();
        // dd($filepath,$nama,$mime,$ukuran);
        # validasi mime file
        if ($mime != "application/pdf") {
            $retUpload = ['status' => 0, 'message' => 'Format file bukan .pdf'];
        } elseif ($ukuran > 2097152) {
            $retUpload = ['status' => 0, 'message' => 'Ukuran file lebih dari 2 MB'];
        } else {
            $response = Http::attach('file', file_get_contents($filepath), $nama)
                ->put(env('API_URL_SDM07') . '/api/v1/files');

            if (!$response->successful()) {
                $message = $response->body();

                $retUpload = ['status' => 0, 'message' => 'Gagal Melakukan Upload File'];

            } else {
                $retUpload = ['status' => 1, 'file' => $response->body()];
            }
        }

        return $retUpload;
    }

    public function kirimSKFinal(Request $request, $id)
    {
        #id Dekrip param ajukan
        $idDekrip = AppHelper::instance()->dekrip($id);

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];


        #body
        #all data
        $data = [
            "pegawaiId" => session()->get('user')['pegawaiId'],
            "namaPegawai" => session()->get('user')['namaPegawai'],
            "nip18" => session()->get('user')['nip18'],
            "pangkatId" => session()->get('user')['pangkatId'],
            "namaPangkat" => session()->get('user')['pangkat'],
            "jabatanId" => session()->get('user')['jabatanId'],
            "namaJabatan" => session()->get('user')['jabatan'],
            "unitOrgId" => session()->get('user')['unitId'],
            "namaUnitOrg" => session()->get('user')['unit'],
            "kantorId" => session()->get('user')['kantorId'],
            "namaKantor" => session()->get('user')['kantor'],
            "pegawaiInputId" => session()->get('user')['pegawaiId'],
            "kdKpp" => $kdkpp,
            "kdKanwil" => $kdkanwil,
            "kdKantor" => session()->get('user')['kantorLegacyKode'],
            "keterangan" => "Permohonan diajukan",
        ];

        $response = PensiunApiHelper::patch('/api/v1/pps/upload/sk_pensiun/final?id=' . $idDekrip, ($data));
        return ($response);

    }


}

?>
