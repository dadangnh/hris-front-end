<?php

namespace App\Http\Controllers\Pensiun;

use App\Helpers\PensiunApiHelper;
use App\Models\MenuModel;
use AppHelper;
use Illuminate\Routing\Controller;


class MonitoringPPController extends Controller
{

    public function homeMonitoring()
    {
        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'monitoring');

        #set parameter
        $kdKantor = session()->get('user')['kantorLegacyKode'];
        // dd($kdKantor);
        #pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $size = 5;

        #response
        $response = PensiunApiHelper::get('/api/v1/pps/monitoring?kdKantor=' . $kdKantor . '&page=' . $page . '&size=' . $size);

        #data
        $data['dataPPS'] = $response['data']->data;
        $data['totalItems'] = $response['data']->totalItems;
        $data['totalPages'] = $response['data']->totalPages;
        $data['currentPage'] = $response['data']->currentPage;
        $data['numberOfElements'] = $response['data']->numberOfElements;
        $data['size'] = $response['data']->size;
        $data['page'] = $page;
        // dd($data);
        // #data get validation
        if ($response['status'] == 1) {
            return view('pensiun.monitoring.home', $data);
        } else {
            return abort(500);
        }
    }

    public function detilMonitoring($id)
    {
        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'monitoring');


        #set parameter id PP
        $idDekrip = AppHelper::instance()->dekrip($id);

        #endpoint get data pp by id
        $response = PensiunApiHelper::get('/api/v1/pps/' . $idDekrip);
        $data['dataPP'] = $response['data'];

        #loop data dokumen
        foreach ($response['data'] as $key) {
            $datakey = $key;
        }
        $data['dokumensPP'] = $datakey;
        // dd($data);

        #response return
        if ($response['status'] == 1) {
            return view('pensiun.monitoring.detil', $data);
        } else {
            return redirect()->back()->with('error', 'Gagal Mengambil Data');
        }
    }


}

?>
