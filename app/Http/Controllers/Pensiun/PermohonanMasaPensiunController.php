<?php

namespace App\Http\Controllers\Pensiun;

use App\Helpers\AppHelper;
use App\Helpers\CommonVariable;
use App\Helpers\PensiunApiHelper;
use App\HelpersAppHelper;
use App\Models\MenuModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;

class PermohonanMasaPensiunController extends Controller
{

    public function index()
    {

        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'permohonan-masa-pensiun/permohonan');

        #get data by nip
        $nip = session()->get('user')['nip18'];

        #pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $size = 10;

        #response
        $response = PensiunApiHelper::get('/api/v1/mpps/drafts?page=' . $page . '&size=' . $size . '&nip=' . $nip);
        $data['dataMpp'] = $response['data']->data;
        $data['totalItems'] = $response['data']->totalItems;
        $data['totalPages'] = $response['data']->totalPages;
        $data['currentPage'] = $response['data']->currentPage;
        $data['numberOfElements'] = $response['data']->numberOfElements;
        $data['size'] = $response['data']->size;
        $data['page'] = $page;
        // dd($data);
        if ($response['status'] == 1) {
            return view('pensiun.permohonanmpp.home_add_permohonan_masa_pensiun', $data);
        } else {
            return view('errors.error');
        }
    }

    public function logstatusmpp()
    {
        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'permohonan-masa-pensiun/permohonan');

        #get data by nip
        $idMppDekrip = AppHelper::instance()->dekrip($_GET['idMpp']);

        #pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $size = 10;

        #response
        $response = PensiunApiHelper::get('/api/v1/mpps/logs?idMpp=' . $idMppDekrip);
        // dd($response);
        $data['logMpp'] = $response['data']->data;
        $data['totalItems'] = $response['data']->totalItems;
        $data['totalPages'] = $response['data']->totalPages;
        $data['currentPage'] = $response['data']->currentPage;
        $data['numberOfElements'] = $response['data']->numberOfElements;
        $data['size'] = $response['data']->size;
        $data['page'] = $page;
        // dd($data);
        if ($response['status'] == 1) {
            return ($data);
        } else {
            return view('errors.error');
        }
    }

    public function addPermohonan()
    {
        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'permohonan-masa-pensiun/permohonan');

        #get data by nip
        $nip = session()->get('user')['nip18'];

        #hitung bup servis
        if (session()->get('user')['levelJabatan'] == '1' || session()->get('user')['levelJabatan'] == '2' || session()->get('user')['kd_jabatan_id'] == '0906' || session()->get('user')['kd_jabatan_id'] == '0237' || session()->get('user')['kd_jabatan_id'] == '0424' || session()->get('user')['kd_jabatan_id'] == '0128') {
            $bup = '60';
        } else {
            $bup = '58';
        }

        #create tgl lahir
        $thn = substr($nip, 0, 4);
        $bln = substr($nip, 4, 2);
        $tgl = substr($nip, 6, 2);

        $tglUltahPensiun = $thn + $bup . '-' . $bln;

        $data['tglpensiun'] = $tglUltahPensiun;
        $data['tgllahir'] = $thn . '-' . $bln . '-' . $tgl;
        $data['bup'] = $bup;

        return view('pensiun.permohonanmpp.add_permohonan_masa_pensiun', $data);
    }

    public function generateSK($id)
    {
        #get konsep sket
        if ($id == 'skpelanggarandisiplin') {

            $url = env('API_URL_SDM07') . '/api/v1/mpps/generate/sket_periksa?nipPendek=' . session()->get('user')['nip9'];
            $response = Http::accept('application/octet-stream')->post($url, ['']);

            #validasi balikan
            if (!$response->successful()) {
                return redirect()->back()->with('error', 'Gagal Generate File Surat Keterangan');
            }

            #create download file
            $namafile = session()->get('user')['nip9'] . 'sket_periksas.docx';
            $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);
            file_put_contents($tmp_direktori, $response->body());
            $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

            #return data
            $data = response()->download($tmp_direktori, $namafile, $headers);
            return ($data);

        } elseif ($id == 'skperadilan') {

            $url = env('API_URL_SDM07') . '/api/v1/mpps/generate/sket_pengadilan?nipPendek=' . session()->get('user')['nip9'];
            $response = Http::accept('application/octet-stream')->post($url, ['']);

            #cek balikan
            if (!$response->successful()) {
                return redirect()->back()->with('error', 'Gagal Generate File Surat Keterangan');
            }

            #create download file
            $namafile = session()->get('user')['nip9'] . 'sket_pengadilans.docx';
            $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);
            file_put_contents($tmp_direktori, $response->body());
            $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

            #return data
            $data = response()->download($tmp_direktori, $namafile, $headers);
            return ($data);

        } elseif ($id == 'skselesaipekerjaan') {

            $url = env('API_URL_SDM07') . '/api/v1/mpps/generate/sket_pekerjaan?nipPendek=' . session()->get('user')['nip9'];
            $response = Http::accept('application/octet-stream')->post($url, ['']);

            #cek balikan
            if (!$response->successful()) {
                return redirect()->back()->with('error', 'Gagal Generate File Surat Keterangan');
            }

            #create download file
            $namafile = session()->get('user')['nip9'] . 'sket_pekerjaans.docx';
            $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);
            file_put_contents($tmp_direktori, $response->body());
            $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

            #return
            $data = response()->download($tmp_direktori, $namafile, $headers);
            return ($data);
        }
    }

    public function simpanPermohonan(Request $request)
    {

        if ($request->files->get('skpelanggarandisiplin') == null || $request->files->get('skperadilan') == null || $request->files->get('skselesaipekerjaan') == null) {
            return redirect()->back()->with('error', 'File Surat Keterangan Belum Lengkap');
        }

        #hitung bup servis
        if (session()->get('user')['levelJabatan'] == '1' || session()->get('user')['levelJabatan'] == '2' || session()->get('user')['kd_jabatan_id'] == '0906' || session()->get('user')['kd_jabatan_id'] == '0237' || session()->get('user')['kd_jabatan_id'] == '0424' || session()->get('user')['kd_jabatan_id'] == '0128') {
            $bup = '60';
        } else {
            $bup = '58';
        }

        #get data by nip
        $nip = session()->get('user')['nip18'];

        #create tgl lahir
        $thn = substr($nip, 0, 4);
        $bln = substr($nip, 4, 2);
        $tglawal = '01';
        $tglakhir = substr($this->lastOfMonth($thn + $bup, $bln), 8, 2);
        $masaawal = $request->input('masaawal') . '-' . $tglawal;
        $masaakhir = $thn + $bup . '-' . $bln . '-' . $tglakhir;

        #selisih masa
        $selisihmasa = $request->input('selisihbln');

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #requ file
        $sket_periksas = $request->files->get('skpelanggarandisiplin');
        $sket_pengadilans = $request->files->get('skperadilan');
        $sket_pekerjaans = $request->files->get('skselesaipekerjaan');

        #upload file
        $upload_sket_periksas = $this->upload($sket_periksas);
        if ($upload_sket_periksas['status'] == 1) {
            #file sket_periksas
            $file_sket_periksas = $upload_sket_periksas['file'];
            $upload_sket_pengadilans = $this->upload($sket_pengadilans);
            if ($upload_sket_periksas['status'] == 1) {
                #file sket_pengadilans
                $file_sket_pengadilans = $upload_sket_pengadilans['file'];
                $upload_sket_pekerjaans = $this->upload($sket_pekerjaans);

                if ($upload_sket_pekerjaans['status'] == 1) {
                    #file sket_pekerjaans
                    $file_sket_pekerjaans = $upload_sket_pekerjaans['file'];
                } else {
                    return redirect()->back()->with('error', $upload_sket_pekerjaans['message'] . ' Sket Pekerjaan');
                }
            } else {
                return redirect()->back()->with('error', $upload_sket_pengadilans['message'] . ' Sket Pengadilan');
            }
        } else {
            return redirect()->back()->with('error', $upload_sket_periksas['message'] . ' Sket Periksa');
        }

        #all data
        $data = [
            "pegawaiId" => session()->get('user')['pegawaiId'],
            "namaPegawai" => session()->get('user')['namaPegawai'],
            "nip18" => session()->get('user')['nip18'],
            "pangkatId" => session()->get('user')['pangkatId'],
            "namaPangkat" => session()->get('user')['pangkat'],
            "jabatanId" => session()->get('user')['jabatanId'],
            "namaJabatan" => session()->get('user')['jabatan'],
            "unitOrgId" => session()->get('user')['unitId'],
            "namaUnitOrg" => session()->get('user')['unit'],
            "kantorId" => session()->get('user')['kantorId'],
            "namaKantor" => session()->get('user')['kantor'],
            "pegawaiInputId" => session()->get('user')['pegawaiId'],
            "kdKpp" => $kdkpp,
            "kdKanwil" => $kdkanwil,
            "kdKantor" => session()->get('user')['kantorLegacyKode'],
            "alamat" => session()->get('user')['alamatJalan'] . ',' . session()->get('user')['kelurahan'] . ',' . session()->get('user')['kecamatan'] . ',' . session()->get('user')['kabupaten'] . ',' . session()->get('user')['provinsi'],
            "bup" => $bup,
            "masaAwal" => $masaawal,
            "masaAkhir" => $masaakhir,
            "sketPeriksa" => $file_sket_periksas,
            "sketPengadilan" => $file_sket_pengadilans,
            "sketPekerjaan" => $file_sket_pekerjaans,
            "selisihMpp" => $selisihmasa,
        ];

        $response = PensiunApiHelper::post('/api/v1/mpps', ($data));
        if ($response['status'] == 1) {
            return redirect('permohonan-masa-pensiun/permohonan')->with('success', 'Data berhasil di simpan');
        } else {
            return redirect()->back()->with('error', 'Data gagal di simpan');
        }
    }

    private function lastOfMonth($year, $month)
    {
        return date("Y-m-d", strtotime('-1 second', strtotime('+1 month', strtotime($month . '/01/' . $year . ' 00:00:00'))));
    }

    private function upload($file)
    {
        $filepath = $file->getRealPath();
        $nama = $file->getClientOriginalName();
        $mime = $file->getMimeType();
        $ukuran = $file->getSize();
        # validasi mime file
        if ($mime != "application/pdf") {
            $retUpload = ['status' => 0, 'message' => 'Format file bukan .pdf'];
        } elseif ($ukuran > 2097152) {
            $retUpload = ['status' => 0, 'message' => 'Ukuran file lebih dari 2 MB'];
        } else {
            $response = Http::attach('file', file_get_contents($filepath), $nama)
                ->put(env('API_URL_SDM07') . '/api/v1/files');

            if (!$response->successful()) {
                $message = $response->body();

                $retUpload = ['status' => 0, 'message' => 'Gagal Melakukan Upload File'];

            } else {
                $retUpload = ['status' => 1, 'file' => $response->body()];
            }
        }

        return $retUpload;
    }

    public function editPermohonan($id)
    {
        #id
        $idDekrip = AppHelper::instance()->dekrip($id);

        #get data by nip
        $nip = session()->get('user')['nip18'];

        #hitung bup servis
        if (session()->get('user')['levelJabatan'] == '1' || session()->get('user')['levelJabatan'] == '2' || session()->get('user')['kd_jabatan_id'] == '0906' || session()->get('user')['kd_jabatan_id'] == '0237' || session()->get('user')['kd_jabatan_id'] == '0424' || session()->get('user')['kd_jabatan_id'] == '0128') {
            $bup = '60';
        } else {
            $bup = '58';
        }

        #create tgl lahir
        $thn = substr($nip, 0, 4);
        $bln = substr($nip, 4, 2);

        $tglUltahPensiun = $thn + $bup . '-' . $bln;
        $data['masaAkhir'] = $tglUltahPensiun;

        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'permohonan-masa-pensiun/permohonan');

        #response
        $response = PensiunApiHelper::get('/api/v1/mpps/' . $idDekrip);
        $data['dataMpp'] = $response['data'];
        $data['masaAwal'] = substr($response['data']->masaAwal, 0, 7);
        #response return
        if ($response['status'] == 1) {
            return view('pensiun.permohonanmpp.edit_permohonan_masa_pensiun', $data);
        } else {
            return redirect()->back()->with('error', 'Gagal Mengambil Data');
        }

    }

    public function updatePermohonan(Request $request)
    {

        // dd($request->input('idMpp'));
        $idDekrip = AppHelper::instance()->dekrip($request->input('idMpp'));
        #hitung bup servis
        if (session()->get('user')['levelJabatan'] == '1' || session()->get('user')['levelJabatan'] == '2' || session()->get('user')['kd_jabatan_id'] == '0906' || session()->get('user')['kd_jabatan_id'] == '0237' || session()->get('user')['kd_jabatan_id'] == '0424' || session()->get('user')['kd_jabatan_id'] == '0128') {
            $bup = '60';
        } else {
            $bup = '58';
        }

        #get data by nip
        $nip = session()->get('user')['nip18'];

        #create tgl lahir
        $thn = substr($nip, 0, 4);
        $bln = substr($nip, 4, 2);
        $tglawal = '01';
        $tglakhir = substr($this->lastOfMonth($thn + $bup, $bln), 8, 2);
        $masaawal = $request->input('masaawal') . '-' . $tglawal;
        $masaakhir = $thn + $bup . '-' . $bln . '-' . $tglakhir;

        #selisih masa
        $selisihmasa = $request->input('selisihbln');

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];


        #get data file sket
        $responseMpp = PensiunApiHelper::get('/api/v1/mpps/' . $idDekrip);
        if ($responseMpp['status'] == 1) {
            #data get
            $sketPeriksa = $responseMpp['data']->sketPeriksa;
            $sketPengadilan = $responseMpp['data']->sketPengadilan;
            $sketPekerjaan = $responseMpp['data']->sketPekerjaan;
        } else {
            return redirect()->back()->with('error', 'Gagal Mengambil Data Mpp');
        }

        #requ file
        $sket_periksas = $request->files->get('skpelanggarandisiplin');
        $sket_pengadilans = $request->files->get('skperadilan');
        $sket_pekerjaans = $request->files->get('skselesaipekerjaan');

        $file_sket_periksas = null;
        $file_sket_pengadilans = null;
        $file_sket_pekerjaans = null;
        #upload file
        if ($sket_periksas != null) {
            #delete filenya jika ada file baru
            $delete_sket_periksa = PensiunApiHelper::delete('/api/v1/files/' . $sketPeriksa, []);
            #cek kondisi delete
            if ($delete_sket_periksa['status'] == 1) {
                #proses upload
                $upload_sket_periksas = $this->upload($sket_periksas);
                #cekresponse upload
                if ($upload_sket_periksas['status'] == 1) {
                    #file sket_periksas
                    $file_sket_periksas = $upload_sket_periksas['file'];
                } else {
                    return redirect()->back()->with('error', $upload_sket_periksas['message']);
                }
            }
        } else {
            $file_sket_periksas = $sketPeriksa;
        }

        if ($sket_pengadilans != null) {
            #delete filenya jika ada file baru
            $delete_sket_pengadilans = PensiunApiHelper::delete('/api/v1/files/' . $sketPengadilan, []);
            // dd($delete_sket_pengadilans);
            #cek kondisi delete
            if ($delete_sket_pengadilans['status'] == 1) {
                #proses upload
                $upload_sket_pengadilans = $this->upload($sket_pengadilans);
                #cek response upload
                if ($upload_sket_pengadilans['status'] == 1) {
                    #file sket_pengadilans
                    $file_sket_pengadilans = $upload_sket_pengadilans['file'];
                } else {
                    return redirect()->back()->with('error', $upload_sket_pengadilans['message']);
                }
            }
        } else {
            $file_sket_pengadilans = $sketPengadilan;
        }

        if ($sket_pekerjaans != null) {
            #delete filenya jika ada file baru
            $delete_sket_pekerjaans = PensiunApiHelper::delete('/api/v1/files/' . $sketPekerjaan, []);
            #cek kondisi delete
            if ($delete_sket_pekerjaans['status'] == 1) {
                #proses upload
                $upload_sket_pekerjaans = $this->upload($sket_pekerjaans);
                #cek response upload
                if ($upload_sket_pekerjaans['status'] == 1) {
                    #file sket_pekerjaans
                    $file_sket_pekerjaans = $upload_sket_pekerjaans['file'];
                } else {
                    return redirect()->back()->with('error', $upload_sket_pekerjaans['message']);
                }
            }
        } else {
            $file_sket_pekerjaans = $sketPekerjaan;
        }

        $data = [
            "pegawaiId" => session()->get('user')['pegawaiId'],
            "namaPegawai" => session()->get('user')['namaPegawai'],
            "nip18" => session()->get('user')['nip18'],
            "pangkatId" => session()->get('user')['pangkatId'],
            "namaPangkat" => session()->get('user')['pangkat'],
            "jabatanId" => session()->get('user')['jabatanId'],
            "namaJabatan" => session()->get('user')['jabatan'],
            "unitOrgId" => session()->get('user')['unitId'],
            "namaUnitOrg" => session()->get('user')['unit'],
            "kantorId" => session()->get('user')['kantorId'],
            "namaKantor" => session()->get('user')['kantor'],
            "pegawaiInputId" => session()->get('user')['pegawaiId'],
            "kdKpp" => $kdkpp,
            "kdKanwil" => $kdkanwil,
            "kdKantor" => session()->get('user')['kantorLegacyKode'],
            "alamat" => session()->get('user')['alamatJalan'] . ',' . session()->get('user')['kelurahan'] . ',' . session()->get('user')['kecamatan'] . ',' . session()->get('user')['kabupaten'] . ',' . session()->get('user')['provinsi'],
            "bup" => $bup,
            "masaAwal" => $masaawal,
            "masaAkhir" => $masaakhir,
            "sketPeriksa" => $file_sket_periksas,
            "sketPengadilan" => $file_sket_pengadilans,
            "sketPekerjaan" => $file_sket_pekerjaans,
            "selisihMpp" => $selisihmasa,
        ];

        $response = Http::put(env('API_URL_SDM07') . '/api/v1/mpps?idMpp=' . $idDekrip, ($data));
        if (!$response->successful()) {
            return redirect()->back()->with('error', 'Data gagal di simpan');
        } else {
            return redirect()->back()->with('success', 'Data berhasil di simpan');
        }

    }

    public function getFilePermohonan($id)
    {
        #id
        $idDekrip = AppHelper::instance()->dekrip($id);
        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'permohonan-masa-pensiun/permohonan');

        #file
        $tmp_direktori = tempnam(sys_get_temp_dir(), $idDekrip);
        $response = Http::get(env('API_URL_SDM07') . '/api/v1/files/' . $idDekrip);
        file_put_contents($tmp_direktori, $response->body());
        return response()->file($tmp_direktori, [
            'Content-Type' => 'application/pdf'
        ]);

    }

    public function ajukanPermohonan($id)
    {
        #idMpp
        $idDekrip = AppHelper::instance()->dekrip($id);

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #all data
        $data = [
            "pegawaiId" => session()->get('user')['pegawaiId'],
            "namaPegawai" => session()->get('user')['namaPegawai'],
            "nip18" => session()->get('user')['nip18'],
            "pangkatId" => session()->get('user')['pangkatId'],
            "namaPangkat" => session()->get('user')['pangkat'],
            "jabatanId" => session()->get('user')['jabatanId'],
            "namaJabatan" => session()->get('user')['jabatan'],
            "unitOrgId" => session()->get('user')['unitId'],
            "namaUnitOrg" => session()->get('user')['unit'],
            "kantorId" => session()->get('user')['kantorId'],
            "namaKantor" => session()->get('user')['kantor'],
            "pegawaiInputId" => session()->get('user')['pegawaiId'],
            "kdKpp" => $kdkpp,
            "kdKanwil" => $kdkanwil,
            "kdKantor" => session()->get('user')['kantorLegacyKode'],
            "keterangan" => "Draft MPP diajukan",
        ];

        // dd(json_encode($data), $idDekrip);

        #endpoint
        $url = env('API_URL_SDM07') . '/api/v1/mpps/proses?id=' . $idDekrip;
        $response = Http::patch($url, $data);
        // dd(json_decode($response));

        # cek status servis
        if (!$response->successful()) {

            $x = json_decode($response);
            if (isset($x->apierror->message)) {
                $message = 'Error Web Services';
            }
            $return = ['status' => 0, 'message' => $message];

        } else {
            #get data body
            $body = $response->getBody();
            $data = json_decode($body);

            $return = ['status' => 1, 'data' => $data];
        }
        return response()->json($return);

    }

    public function deletePermohonan($id)
    {
        #idMpp
        $idDekrip = AppHelper::instance()->dekrip($id);

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #get data file sket
        $responseMpp = PensiunApiHelper::get('/api/v1/mpps/' . $idDekrip);
        if ($responseMpp['status'] == 1) {
            #delete sketPeriksa
            $sketPeriksa = $responseMpp['data']->sketPeriksa;
            $delete_sket_periksa = PensiunApiHelper::delete('/api/v1/files/' . $sketPeriksa, []);
            // dd($delete_sket_periksa);
            if ($delete_sket_periksa['status'] == 1) {
                #delete sketPengadilan
                $sketPengadilan = $responseMpp['data']->sketPengadilan;
                $delete_sket_pengadilans = PensiunApiHelper::delete('/api/v1/files/' . $sketPengadilan, []);
                if ($delete_sket_pengadilans['status'] == 1) {
                    #delete sketPekerjaan
                    $sketPekerjaan = $responseMpp['data']->sketPekerjaan;
                    $delete_sket_pekerjaans = PensiunApiHelper::delete('/api/v1/files/' . $sketPekerjaan, []);
                }
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Gagal Mengambil Data Mpp']);
        }

        #all data
        $data = [
            "pegawaiId" => session()->get('user')['pegawaiId'],
            "namaPegawai" => session()->get('user')['namaPegawai'],
            "nip18" => session()->get('user')['nip18'],
            "pangkatId" => session()->get('user')['pangkatId'],
            "namaPangkat" => session()->get('user')['pangkat'],
            "jabatanId" => session()->get('user')['jabatanId'],
            "namaJabatan" => session()->get('user')['jabatan'],
            "unitOrgId" => session()->get('user')['unitId'],
            "namaUnitOrg" => session()->get('user')['unit'],
            "kantorId" => session()->get('user')['kantorId'],
            "namaKantor" => session()->get('user')['kantor'],
            "pegawaiInputId" => session()->get('user')['pegawaiId'],
            "kdKpp" => $kdkpp,
            "kdKanwil" => $kdkanwil,
            "kdKantor" => session()->get('user')['kantorLegacyKode'],
        ];

        $response = PensiunApiHelper::delete('/api/v1/mpps/' . $idDekrip, ($data));
        return response()->json($response);
    }

    public function reviewPermohonan()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'permohonan-masa-pensiun/review');

        #get data by nip
        $nip = session()->get('user')['nip18'];
        $nip9 = session()->get('user')['nip9'];
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $size = 10;

        if (session()->get('user')['unitLegacyKode'] == '0424071300' && session()->get('user')['kd_jabatan_id'] == '403') { #upk kpp
            #response
            $response = PensiunApiHelper::get('/api/v1/mpps/upks_kpp?page=' . $page . '&size=' . $size . '&kdKpp=' . $kdkpp);
            $data['dataMpp'] = $response['data'];
            $data['totalItems'] = $response['data']->totalItems;
            $data['totalPages'] = $response['data']->totalPages;
            $data['currentPage'] = $response['data']->currentPage;
            $data['numberOfElements'] = $response['data']->numberOfElements;
            $data['size'] = $response['data']->size;
            $data['page'] = $page;
            $status = $response['status'];

            #cek balikan
            if ($status == 1) {
                return view('pensiun.permohonanmpp.home_review_upk_lokal_permohonan_masa_pensiun', $data);
            } else {
                return redirect()->back()->with('error', 'Gagal Mengambil Data');
            }
        } elseif (session()->get('user')['kd_jabatan_id'] == '304' && $kdkanwil != '0' && $kdkanwil < 900) { #kepala kpp
            #response
            $response = PensiunApiHelper::get('/api/v1/mpps/kepalas_kpp?page=' . $page . '&size=' . $size . '&kdKpp=' . $kdkpp);
            $data['dataMpp'] = $response['data'];
            $data['totalItems'] = $response['data']->totalItems;
            $data['totalPages'] = $response['data']->totalPages;
            $data['currentPage'] = $response['data']->currentPage;
            $data['numberOfElements'] = $response['data']->numberOfElements;
            $data['size'] = $response['data']->size;
            $data['page'] = $page;
            $status = $response['status'];

            #cek balikan
            if ($status == 1) {
                return view('pensiun.permohonanmpp.home_review_upk_lokal_permohonan_masa_pensiun', $data);
            } else {
                return redirect()->back()->with('error', 'Gagal Mengambil Data');
            }
        } elseif (session()->get('user')['unitLegacyKode'] == '0423010100' && session()->get('user')['kd_jabatan_id'] == '403' || session()->get('user')['unitLegacyKode'] == '0421010100' && session()->get('user')['kd_jabatan_id'] == '403' || $kdkanwil == '0' && session()->get('user')['kd_jabatan_id'] == '403' || $kdkanwil > 900 && session()->get('user')['kd_jabatan_id'] == '403') { #upk kanwil / UPT / PPDDP / Es 2
            #upk wilayah
            if ($kdkanwil == null) {
                $response = PensiunApiHelper::get('/api/v1/mpps/upks_wilayah?page=' . $page . '&size=' . $size);
            } else {
                $response = PensiunApiHelper::get('/api/v1/mpps/upks_wilayah?page=' . $page . '&size=' . $size . '&kdKanwil=' . $kdkanwil);
            }

            $data['dataMpp'] = $response['data'];
            $data['totalItems'] = $response['data']->totalItems;
            $data['totalPages'] = $response['data']->totalPages;
            $data['currentPage'] = $response['data']->currentPage;
            $data['numberOfElements'] = $response['data']->numberOfElements;
            $data['size'] = $response['data']->size;
            $data['page'] = $page;
            $status = $response['status'];

            #cek balikan
            if ($status == 1) {
                return view('pensiun.permohonanmpp.home_review_upk_kanwil_permohonan_masa_pensiun', $data);
            } else {
                return redirect()->back()->with('error', 'Gagal Mengambil Data');
            }
        } elseif (session()->get('user')['kd_jabatan_id'] == '203' || $kdkanwil == '0' && session()->get('user')['kd_jabatan_id'] == '304' || $kdkanwil > 900 && session()->get('user')['kd_jabatan_id'] == '204' || $kdkanwil > 900 && session()->get('user')['kd_jabatan_id'] == 202) { #kakanwil / upt / ppddp  / es 2

            $response = PensiunApiHelper::get('/api/v1/mpps/kepalas_wilayah?page=' . $page . '&size=' . $size . '&kdKanwil=' . $kdkanwil);
            $data['dataMpp'] = $response['data'];
            $data['totalItems'] = $response['data']->totalItems;
            $data['totalPages'] = $response['data']->totalPages;
            $data['currentPage'] = $response['data']->currentPage;
            $data['numberOfElements'] = $response['data']->numberOfElements;
            $data['size'] = $response['data']->size;
            $data['page'] = $page;
            $status = $response['status'];

            #cek balikan
            if ($status == 1) {
                return view('pensiun.permohonanmpp.home_review_upk_kanwil_permohonan_masa_pensiun', $data);
            } else {
                return redirect()->back()->with('error', 'Gagal Mengambil Data');
            }
        } elseif (session()->get('user')['unitLegacyKode'] == '0402060400') {
            $response = PensiunApiHelper::get('/api/v1/mpps/pegawais_pdpp?page=' . $page . '&size=' . $size . '&nipDisposisi=' . $nip9);
            $data['dataMpp'] = $response['data']->data;
            $data['totalItems'] = $response['data']->totalItems;
            $data['totalPages'] = $response['data']->totalPages;
            $data['currentPage'] = $response['data']->currentPage;
            $data['numberOfElements'] = $response['data']->numberOfElements;
            $data['size'] = $response['data']->size;
            $data['page'] = $page;
            $status = $response['status'];

            #cek balikan
            if ($status == 1) {
                return view('pensiun.permohonanmpp.home_review_pelaksanapdpp_permohonan_masa_pensiun', $data);
            } else {
                return redirect()->back()->with('error', 'Gagal Mengambil Data');
            }
        } else {
            #sementara
            return redirect()->back()->with('error', 'Anda Tidak Memiliki Akses ke Halaman Ini');
        }
    }

    public function reviewPermohonanDetil($id)
    {
        #id
        $idDekrip = AppHelper::instance()->dekrip($id);

        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'permohonan-masa-pensiun/review');

        #response
        $response = PensiunApiHelper::get('/api/v1/mpps/' . $idDekrip);
        $data['dataMpp'] = $response['data'];
        // dd($data);
        #cekbalikan
        if ($response['status'] == 1) {
            return view('pensiun.permohonanmpp.review_upk_kepala_permohonan_masa_pensiun', $data);
        } else {
            return redirect()->back()->with('error', 'Gagal Mengambil Data');
        }
    }

    public function setujuiPermohonan($id)
    {
        #idMpp
        $idDekrip = AppHelper::instance()->dekrip($id);

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #all data
        $data = [
            "pegawaiId" => session()->get('user')['pegawaiId'],
            "namaPegawai" => session()->get('user')['namaPegawai'],
            "nip18" => session()->get('user')['nip18'],
            "pangkatId" => session()->get('user')['pangkatId'],
            "namaPangkat" => session()->get('user')['pangkat'],
            "jabatanId" => session()->get('user')['jabatanId'],
            "namaJabatan" => session()->get('user')['jabatan'],
            "unitOrgId" => session()->get('user')['unitId'],
            "namaUnitOrg" => session()->get('user')['unit'],
            "kantorId" => session()->get('user')['kantorId'],
            "namaKantor" => session()->get('user')['kantor'],
            "pegawaiInputId" => session()->get('user')['pegawaiId'],
            "kdKpp" => $kdkpp,
            "kdKanwil" => $kdkanwil,
            "kdKantor" => session()->get('user')['kantorLegacyKode'],
            "keterangan" => "Disetujui oleh " . session()->get('user')['namaPegawai']
        ];

        #endpoint
        $url = env('API_URL_SDM07') . '/api/v1/mpps/setujui?id=' . $idDekrip;
        $response = Http::patch($url, $data);

        # cek status servis
        if (!$response->successful()) {

            $x = json_decode($response);
            if (isset($x->apierror->message)) {
                $message = 'Error Web Services';
            }
            $return = ['status' => 0, 'message' => $message];

        } else {
            #get data body
            $body = $response->getBody();
            $data = json_decode($body);
            $return = ['status' => 1, 'data' => $data];
        }

        return response()->json($return);

    }

    public function tolakPermohonan(Request $request)
    {
        #idMpp
        $idDekrip = AppHelper::instance()->dekrip($request->input('id'));

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #all data
        $data = [
            "pegawaiId" => session()->get('user')['pegawaiId'],
            "namaPegawai" => session()->get('user')['namaPegawai'],
            "nip18" => session()->get('user')['nip18'],
            "pangkatId" => session()->get('user')['pangkatId'],
            "namaPangkat" => session()->get('user')['pangkat'],
            "jabatanId" => session()->get('user')['jabatanId'],
            "namaJabatan" => session()->get('user')['jabatan'],
            "unitOrgId" => session()->get('user')['unitId'],
            "namaUnitOrg" => session()->get('user')['unit'],
            "kantorId" => session()->get('user')['kantorId'],
            "namaKantor" => session()->get('user')['kantor'],
            "pegawaiInputId" => session()->get('user')['pegawaiId'],
            "kdKpp" => $kdkpp,
            "kdKanwil" => $kdkanwil,
            "kdKantor" => session()->get('user')['kantorLegacyKode'],
            "keterangan" => $request->input('keterangan')
        ];

        #endpoint
        $response = PensiunApiHelper::patch('/api/v1/mpps/tolak?id=' . $idDekrip, $data);
        return response()->json($response);
    }

    public function getDisposisi()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'permohonan-masa-pensiun/disposisi');

        #get data by nip
        $nip = session()->get('user')['nip18'];
        $nip9 = session()->get('user')['nip9'];
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $size = 10;

        if (session()->get('user')['unitLegacyKode'] == '0402060400' && session()->get('user')['kd_jabatan_id'] == '403') {
            #pdpp
            $responsePdpp = PensiunApiHelper::get('/api/v1/mpps/kasubbags_pdpp?page=' . $page . '&size=' . $size);
            $data['dataMppPdpp'] = $responsePdpp['data'];
            $data['totalItems'] = $responsePdpp['data']->totalItems;
            $data['totalPages'] = $responsePdpp['data']->totalPages;
            $data['currentPage'] = $responsePdpp['data']->currentPage;
            $data['numberOfElements'] = $responsePdpp['data']->numberOfElements;
            $data['size'] = $responsePdpp['data']->size;
            $data['page'] = $page;
            $statusPdpp = $responsePdpp['status'];

            #cek balikan
            if ($statusPdpp == 1) {
                return view('pensiun.permohonanmpp.home_review_disposisi_permohonan_masa_pensiun', $data);
            } else {
                return redirect()->back()->with('error', 'Gagal Mengambil Data');
            }
        } else {
            #sementara
            return redirect()->back()->with('error', 'Anda Tidak Memiliki Akses ke Halaman Ini');
        }
    }

    public function detilDisposisiPermohonan($id)
    {
        #id
        $idDekrip = AppHelper::instance()->dekrip($id);

        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'permohonan-masa-pensiun/review');

        #response data mpp
        $responsempp = PensiunApiHelper::get('/api/v1/mpps/' . $idDekrip);
        $data['dataMpp'] = $responsempp['data'];

        #response data disposisiPermohonan
        $responsedisposisi = PensiunApiHelper::get('/api/v1/mpps/disposisi/pegawais?kdUnit=' . session()->get('user')['kantorLegacyKode'] . '&kdUnitOrg=' . session()->get('user')['unitLegacyKode']);
        $data['dataDisposisi'] = $responsedisposisi['data'];
        //  dd($data);
        #cekbalikan
        if ($responsempp['status'] == 1 && $responsedisposisi['status'] == 1) {
            return view('pensiun.permohonanmpp.review_disposisi_pdpp_permohonan_masa_pensiun', $data);
        } else {
            return redirect()->back()->with('error', 'Gagal Mengambil Data');
        }
    }

    public function simpanDisposisiPermohonan(Request $request)
    {

        if ($request->input('namaNip') == null) {
            return response()->json(['status' => 0, 'message' => 'Pegawai belum dipilih!']);
        }

        #making array
        foreach ($request->input('namaNip') as $key => $value) {

            $data = explode("|", $value['nip']);
            $dataArr[] = [
                "nipDisposisi" => $data[0],
                "namaDisposisi" => $data[1],
            ];
        }

        #dataDisposisi
        $idDekrip = AppHelper::instance()->dekrip($request->input('id'));

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #response
        $response = PensiunApiHelper::patch('/api/v1/mpps/disposisi?idMpp=' . $idDekrip, [
            'pegawai' => [
                "pegawaiId" => session()->get('user')['pegawaiId'],
                "namaPegawai" => session()->get('user')['namaPegawai'],
                "nip18" => session()->get('user')['nip18'],
                "pangkatId" => session()->get('user')['pangkatId'],
                "namaPangkat" => session()->get('user')['pangkat'],
                "jabatanId" => session()->get('user')['jabatanId'],
                "namaJabatan" => session()->get('user')['jabatan'],
                "unitOrgId" => session()->get('user')['unitId'],
                "namaUnitOrg" => session()->get('user')['unit'],
                "kantorId" => session()->get('user')['kantorId'],
                "namaKantor" => session()->get('user')['kantor'],
                "pegawaiInputId" => session()->get('user')['pegawaiId'],
                "kdKpp" => $kdkpp,
                "kdKanwil" => $kdkanwil,
                "kdKantor" => session()->get('user')['kantorLegacyKode'],
                'keterangan' => 'Disposisi oleh ' . session()->get('user')['namaPegawai']
            ], 'dispoPegawai' => $dataArr
        ]);

        if ($response['status'] == 1) {
            return response()->json($response);

        } else {
            return response()->json(['status' => 0, 'message' => 'Disposisi gagal!']);
        }
    }

    public function detilReviewDisposisiPermohonan($id)
    {
        #id
        $idDekrip = AppHelper::instance()->dekrip($id);

        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'permohonan-masa-pensiun/review');

        #response data mpp
        $responsempp = PensiunApiHelper::get('/api/v1/mpps/' . $idDekrip);
        $data['dataMpp'] = $responsempp['data'];
        // dd($data);
        #cekbalikan
        if ($responsempp['status'] == 1) {
            return view('pensiun.permohonanmpp.review_penelitian_permohonan_masa_pensiun', $data);
        } else {
            return redirect()->back()->with('error', 'Gagal Mengambil Data');
        }

    }

    public function penilitianDisposisiPermohonan(Request $request)
    {
        $idDekrip = AppHelper::instance()->dekrip($request->input('idMpp'));

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #get data file sket
        $responseMpp = PensiunApiHelper::get('/api/v1/mpps/' . $idDekrip);
        if ($responseMpp['status'] == 1) {
            #data get
            $noSrtPermintaan = $responseMpp['data']->noSrtPermintaan;
            $fileSrtPermintaan = $responseMpp['data']->fileSrtPermintaan;
            $noSrtKonfirmasi = $responseMpp['data']->noSrtKonfirmasi;
            $fileSrtKonfirmasi = $responseMpp['data']->fileSrtKonfirmasi;
            $noNdPengantar = $responseMpp['data']->ndPengantar;
            $tgl_NdPengantar = $responseMpp['data']->tglNdPengantar;

        } else {
            return redirect()->back()->with('error', 'Gagal Mengambil Data Mpp');
        }

        #cek file current konfirm hukdis
        if ($noSrtPermintaan == null && $fileSrtPermintaan == null) {
            if ($request->input('suratpermintaan') == null && $request->files->get('ndfilespermintaan') == null && $request->input('suratpengantar') == null && $request->input('tanggalsurat') == null) {
                return redirect()->back()->with('error', 'Surat Permintaan atau Surat Pengantar Belum di Input');
            } else {
                $no_SrtPermintaan = $request->input('suratpermintaan');
                $file_SrtPermintaan = $request->files->get('ndfilespermintaan');
                $nm_file_SrtPermintaan = $file_SrtPermintaan;
                $no_SrtKonfirmasi = $request->input('suratkonfirmasi');
                $file_SrtKonfirmasi = $request->files->get('ndskonfirmasi');
                $nm_file_SrtKonfirmasi = $file_SrtKonfirmasi;
                $nd_pengantar = $request->input('suratpengantar');
                $tanggal_nd_pengantar = $request->input('tanggalsurat');
            }
        } else {
            $no_SrtPermintaan = $request->input('suratpermintaan');
            $file_SrtPermintaan = $request->files->get('ndfilespermintaan');
            $nm_file_SrtPermintaan = $fileSrtPermintaan;
            $no_SrtKonfirmasi = $request->input('suratkonfirmasi');
            $file_SrtKonfirmasi = $request->files->get('ndskonfirmasi');
            $nm_file_SrtKonfirmasi = $fileSrtKonfirmasi;
            $nd_pengantar = $request->input('suratpengantar');
            $tanggal_nd_pengantar = $request->input('tanggalsurat');
        }

        #upload file permintaan dan konfirmasi hukdis
        if ($file_SrtPermintaan != null) {
            $upload_fileSrtPermintaan = $this->upload($file_SrtPermintaan);
            if ($upload_fileSrtPermintaan['status'] == 1) {
                #nm file s permintaan
                $nm_file_SrtPermintaan = $upload_fileSrtPermintaan['file'];

            } else {
                return redirect()->back()->with('error', $upload_fileSrtPermintaan['message'] . ' ND Permintaan');
            }
        }

        if ($file_SrtKonfirmasi != null) {
            $upload_fileSrtKonfirmasi = $this->upload($file_SrtKonfirmasi);
            if ($upload_fileSrtKonfirmasi['status'] == 1) {
                #nm_file s konfirmasi
                $nm_file_SrtKonfirmasi = $upload_fileSrtKonfirmasi['file'];
            } else {
                return redirect()->back()->with('error', $upload_fileSrtKonfirmasi['message'] . ' ND Konfirmasi');
            }
        }

        #edit file
        if ($file_SrtPermintaan != null && $fileSrtPermintaan != null) {

            #delete file lama file
            $delete_file_SrtPermintaan = PensiunApiHelper::delete('/api/v1/files/' . $fileSrtPermintaan, []);
            if ($delete_file_SrtPermintaan['status'] == 1) {
                #proses upload ulang
                $upload_fileSrtPermintaan = $this->upload($file_SrtPermintaan);
                if ($upload_fileSrtPermintaan['status'] == 1) {
                    #nm file s permintaan
                    $nm_file_SrtPermintaan = $upload_fileSrtPermintaan['file'];
                }
            }

        } elseif ($file_SrtKonfirmasi != null && $fileSrtKonfirmasi != null) {

            #delete file lama file
            $delete_file_SrtKonfirmasi = PensiunApiHelper::delete('/api/v1/files/' . $fileSrtKonfirmasi, []);
            if ($delete_file_SrtKonfirmasi['status'] == 1) {
                #proses upload ulang
                $upload_fileSrtKonfirmasi = $this->upload($file_SrtKonfirmasi);
                if ($upload_fileSrtKonfirmasi['status'] == 1) {
                    #nm file s permintaan
                    $nm_file_SrtKonfirmasi = $upload_fileSrtKonfirmasi['file'];
                }
            }
        }

        #all data
        $data = [
            // 'pegawai' => [
            //     "pegawaiId"=> session()->get('user')['pegawaiId'],
            //     "namaPegawai"=> session()->get('user')['namaPegawai'],
            //     "nip18"=> session()->get('user')['nip18'],
            //     "pangkatId"=> session()->get('user')['pangkatId'],
            //     "namaPangkat"=> session()->get('user')['pangkat'],
            //     "jabatanId"=> session()->get('user')['jabatanId'],
            //     "namaJabatan"=> session()->get('user')['jabatan'],
            //     "unitOrgId"=> session()->get('user')['unitId'],
            //     "namaUnitOrg"=> session()->get('user')['unit'],
            //     "kantorId"=> session()->get('user')['kantorId'],
            //     "namaKantor"=> session()->get('user')['kantor'],
            //     "pegawaiInputId"=> session()->get('user')['pegawaiId'],
            //     "kdKpp"=>  $kdkpp,
            //     "kdKanwil"=> $kdkanwil,
            //     "kdKantor"=> session()->get('user')['kantorLegacyKode'],
            //     'keterangan' => ''
            // ],
            "noSrtPermintaanHukdis" => $no_SrtPermintaan,
            "fileSrtPermintaanHukdis" => $nm_file_SrtPermintaan,
            "noSrtKonfirmasiHukdis" => $no_SrtKonfirmasi,
            "fileSrtKonfirmasiHukdis" => $nm_file_SrtKonfirmasi,
            "ndPengantar" => $nd_pengantar,
            "tglNdPengantar" => $tanggal_nd_pengantar
        ];

        // dd($data);

        $response = PensiunApiHelper::patch('/api/v1/mpps/penelitian/draft?idMpp=' . $idDekrip, $data);
        // dd($response);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Draft berhasil di simpan');
        } else {
            return redirect()->back()->with('error', 'Draft gagal di simpan');
        }
    }

    public function simpanFinalPenelitianPermohonan($id)
    {
        #idMpp
        $idDekrip = AppHelper::instance()->dekrip($id);

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #get data file sket
        $responseMpp = PensiunApiHelper::get('/api/v1/mpps/' . $idDekrip);
        if ($responseMpp['status'] == 1) {
            #data get
            $noSrtPermintaan = $responseMpp['data']->noSrtPermintaan;
            $fileSrtPermintaan = $responseMpp['data']->fileSrtPermintaan;
            $noSrtKonfirmasi = $responseMpp['data']->noSrtKonfirmasi;
            $fileSrtKonfirmasi = $responseMpp['data']->fileSrtKonfirmasi;
            $noNdPengantar = $responseMpp['data']->ndPengantar;
            $tgl_NdPengantar = $responseMpp['data']->tglNdPengantar;

        } else {
            return redirect()->back()->with('error', 'Gagal Mengambil Data Mpp');
        }

        if ($noSrtPermintaan == null || $fileSrtPermintaan == null || $noSrtKonfirmasi == null || $fileSrtKonfirmasi == null || $noNdPengantar == null || $tgl_NdPengantar == null) {
            return redirect()->back()->with('error', 'Gagal Menyimpan Final Hasil Penelitian Mpp');
        }

        #all data
        $data = [
            "pegawaiId" => session()->get('user')['pegawaiId'],
            "namaPegawai" => session()->get('user')['namaPegawai'],
            "nip18" => session()->get('user')['nip18'],
            "pangkatId" => session()->get('user')['pangkatId'],
            "namaPangkat" => session()->get('user')['pangkat'],
            "jabatanId" => session()->get('user')['jabatanId'],
            "namaJabatan" => session()->get('user')['jabatan'],
            "unitOrgId" => session()->get('user')['unitId'],
            "namaUnitOrg" => session()->get('user')['unit'],
            "kantorId" => session()->get('user')['kantorId'],
            "namaKantor" => session()->get('user')['kantor'],
            "pegawaiInputId" => session()->get('user')['pegawaiId'],
            "kdKpp" => $kdkpp,
            "kdKanwil" => $kdkanwil,
            "kdKantor" => session()->get('user')['kantorLegacyKode']
        ];

        #endpoint
        $response = PensiunApiHelper::patch('/api/v1/mpps/penelitian/final?idMpp=' . $idDekrip, $data);

        #cek balikan
        if (!$response['status'] == 1) {
            return redirect()->back()->with('error', 'Gagal Menyimpan Hasil Penelitian');
        } else {
            return redirect()->back()->with('success', 'Berhasil Menyimpan Hasil Penelitian');
        }
    }

    public function konsepSKPermohonan($id)
    {
        #id
        $idDekrip = AppHelper::instance()->dekrip($id);

        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'permohonan-masa-pensiun/review');

        #response
        $response = PensiunApiHelper::get('/api/v1/mpps/' . $idDekrip);
        $data['dataMpp'] = $response['data'];

        #cekbalikan
        if ($response['status'] == 1) {
            return view('pensiun.permohonanmpp.review_konsep_sk_permohonan_masa_pensiun', $data);
        } else {
            return redirect()->back()->with('error', 'Gagal Mengambil Data');
        }
    }

    public function tangguhkanKonsepSKMpp($id)
    {
        #idMpp
        $idDekrip = AppHelper::instance()->dekrip($id);

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #all data
        $databody = [
            "pegawaiId" => session()->get('user')['pegawaiId'],
            "namaPegawai" => session()->get('user')['namaPegawai'],
            "nip18" => session()->get('user')['nip18'],
            "pangkatId" => session()->get('user')['pangkatId'],
            "namaPangkat" => session()->get('user')['pangkat'],
            "jabatanId" => session()->get('user')['jabatanId'],
            "namaJabatan" => session()->get('user')['jabatan'],
            "unitOrgId" => session()->get('user')['unitId'],
            "namaUnitOrg" => session()->get('user')['unit'],
            "kantorId" => session()->get('user')['kantorId'],
            "namaKantor" => session()->get('user')['kantor'],
            "pegawaiInputId" => session()->get('user')['pegawaiId'],
            "kdKpp" => $kdkpp,
            "kdKanwil" => $kdkanwil,
            "kdKantor" => session()->get('user')['kantorLegacyKode'],
            "keterangan" => 'SK Disetujui oleh ' . session()->get('user')['namaPegawai']
        ];

        $url = env('API_URL_SDM07') . '/api/v1/mpps/penelitian/tangguhkan?id=' . $idDekrip;
        $response = Http::accept('application/octet-stream')->patch($url, $databody);
        #cek balikan
        if (!$response->successful()) {
            return redirect()->back()->with('error', 'Gagal Membuat File SK');
        }

        #create download file
        $namafile = session()->get('user')['nip9'] . '_sk_Ditangguhkan.docx';
        $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);
        file_put_contents($tmp_direktori, $response->body());
        $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

        #return data
        $data = response()->download($tmp_direktori, $namafile, $headers);
        return ($data);

    }

    public function tolakKonsepSKMpp($id)
    {
        #idMpp
        $idDekrip = AppHelper::instance()->dekrip($id);

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #all data
        $databody = [
            "pegawaiId" => session()->get('user')['pegawaiId'],
            "namaPegawai" => session()->get('user')['namaPegawai'],
            "nip18" => session()->get('user')['nip18'],
            "pangkatId" => session()->get('user')['pangkatId'],
            "namaPangkat" => session()->get('user')['pangkat'],
            "jabatanId" => session()->get('user')['jabatanId'],
            "namaJabatan" => session()->get('user')['jabatan'],
            "unitOrgId" => session()->get('user')['unitId'],
            "namaUnitOrg" => session()->get('user')['unit'],
            "kantorId" => session()->get('user')['kantorId'],
            "namaKantor" => session()->get('user')['kantor'],
            "pegawaiInputId" => session()->get('user')['pegawaiId'],
            "kdKpp" => $kdkpp,
            "kdKanwil" => $kdkanwil,
            "kdKantor" => session()->get('user')['kantorLegacyKode'],
            "keterangan" => 'SK Disetujui oleh ' . session()->get('user')['namaPegawai']
        ];

        $url = env('API_URL_SDM07') . '/api/v1/mpps/penelitian/tolak?id=' . $idDekrip;
        $response = Http::accept('application/octet-stream')->patch($url, $databody);
        #cek balikan
        if (!$response->successful()) {
            return redirect()->back()->with('error', 'Gagal Membuat File SK');
        }

        #create download file
        $namafile = session()->get('user')['nip9'] . '_sk_DiTolak.docx';
        $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);
        file_put_contents($tmp_direktori, $response->body());
        $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

        #return data
        $data = response()->download($tmp_direktori, $namafile, $headers);
        return ($data);
    }

    public function setujuiKonsepSKMpp($id)
    {
        #idMpp
        $idDekrip = AppHelper::instance()->dekrip($id);

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #all data
        $databody = [
            "pegawaiId" => session()->get('user')['pegawaiId'],
            "namaPegawai" => session()->get('user')['namaPegawai'],
            "nip18" => session()->get('user')['nip18'],
            "pangkatId" => session()->get('user')['pangkatId'],
            "namaPangkat" => session()->get('user')['pangkat'],
            "jabatanId" => session()->get('user')['jabatanId'],
            "namaJabatan" => session()->get('user')['jabatan'],
            "unitOrgId" => session()->get('user')['unitId'],
            "namaUnitOrg" => session()->get('user')['unit'],
            "kantorId" => session()->get('user')['kantorId'],
            "namaKantor" => session()->get('user')['kantor'],
            "pegawaiInputId" => session()->get('user')['pegawaiId'],
            "kdKpp" => $kdkpp,
            "kdKanwil" => $kdkanwil,
            "kdKantor" => session()->get('user')['kantorLegacyKode'],
            "keterangan" => 'SK Disetujui oleh ' . session()->get('user')['namaPegawai']
        ];

        $url = env('API_URL_SDM07') . '/api/v1/mpps/penelitian/setujui?id=' . $idDekrip;
        $response = Http::accept('application/octet-stream')->patch($url, $databody);
        #cek balikan
        if (!$response->successful()) {
            return redirect()->back()->with('error', 'Gagal Membuat File SK');
        }

        #create download file
        $namafile = session()->get('user')['nip9'] . '_sk_DiSetujui.docx';
        $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);
        file_put_contents($tmp_direktori, $response->body());
        $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

        #return data
        $data = response()->download($tmp_direktori, $namafile, $headers);
        return ($data);

    }

    public function uploadSKMpp(Request $request)
    {
        if ($request->input('hasilpenelitian') == null) {
            return response()->json(['status' => 0, 'message' => 'Form Hasil Penelitian Tidak Boleh Kosong']);
        }

        #Dari form input
        $idDekrip = AppHelper::instance()->dekrip($request->input('idMpp'));
        $fileskMpp = $request->files->get('skMPP');
        $penelitian = $request->input('hasilpenelitian');

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #all data
        $databody = [
            "pegawaiId" => session()->get('user')['pegawaiId'],
            "namaPegawai" => session()->get('user')['namaPegawai'],
            "nip18" => session()->get('user')['nip18'],
            "pangkatId" => session()->get('user')['pangkatId'],
            "namaPangkat" => session()->get('user')['pangkat'],
            "jabatanId" => session()->get('user')['jabatanId'],
            "namaJabatan" => session()->get('user')['jabatan'],
            "unitOrgId" => session()->get('user')['unitId'],
            "namaUnitOrg" => session()->get('user')['unit'],
            "kantorId" => session()->get('user')['kantorId'],
            "namaKantor" => session()->get('user')['kantor'],
            "pegawaiInputId" => session()->get('user')['pegawaiId'],
            "kdKpp" => $kdkpp,
            "kdKanwil" => $kdkanwil,
            "kdKantor" => session()->get('user')['kantorLegacyKode'],
            "keterangan" => 'SK Di Upload oleh ' . session()->get('user')['namaPegawai']
        ];


        #upload file
        $upload_sk_mpp = $this->upload($fileskMpp);
        if ($upload_sk_mpp['status'] == 1) {

            #file sk mpp
            $skMpp = $upload_sk_mpp['file'];

            #response
            $url = env('API_URL_SDM07') . '/api/v1/mpps/upload/sk?id=' . $idDekrip . '&skMpp=' . $skMpp . '&status=' . $penelitian;
            $response = Http::patch($url, $databody);
            #cekbalikan
            # cek status servis
            if (!$response->successful()) {

                $x = json_decode($response);
                if (isset($x->apierror->message)) {
                    $message = 'Error Web Services';
                }
                $return = ['status' => 0, 'message' => $message];

            } else {
                #get data body
                $body = $response->getBody();
                $data = json_decode($body);
                $return = ['status' => 1, 'data' => $data];
            }

            return response()->json($return);

        } else {
            return response()->json(['status' => 0, 'message' => 'Gagal Melakukan Upload File SK']);
        }

    }

    public function getSKMpp($id)
    {
        #nama file
        $namaFile = AppHelper::instance()->dekrip($id);

        #file
        $tmp_direktori = tempnam(sys_get_temp_dir(), $namaFile);
        $response = Http::get(env('API_URL_SDM07') . '/api/v1/files/' . $namaFile);
        file_put_contents($tmp_direktori, $response->body());
        return response()->file($tmp_direktori, [
            'Content-Type' => 'application/pdf'
        ]);


    }

    public function cekToken()
    {

        #refresh
        if (session()->get('login_info')['_expired'] <= now()->getTimestamp()) {

            #refresh token
            $refreshtoken = $this->refreshToken(session()->get('login_info')['_refresh_token']);
            #response mu jika dia expired

        } else {
            #responmu jika dia not expired

        }

    }

    private function refreshToken($refreshtoken)
    {
        $response = Http::post(env('IAM_SIMULASIKAN_URL') . 'api/token/refresh', ['refresh_token' => $refreshtoken]);

        # cek status servis
        if (!$response->successful()) {

            $message = 'Error Web Services';

            return ['status' => 0, 'message' => $message];

        }

        $data = json_decode($response);

        return ['status' => 1, 'data' => $data];
    }

    #blm di gunakan

    public function getHomeAdd(Request $request)
    {

        #nip
        $nip = session()->get('user')['nip18'];

        #Read value
        $draw = $request->input('draw');

        #pagination
        if (!empty($request->input('start'))) {
            $page = $request->input('start');
            $start = ($page / 10) + 1;
        } else {
            $start = 1;
        }

        $rowperpage = $request->input("length");

        $response = PensiunApiHelper::get('/api/v1/mpps/drafts?page=' . $start . '&size=' . $rowperpage . '&nip=' . $nip);
        $totalRecords = $totalRecordswithFilter = $response['data']->totalItems;
        $record = $response['data']->data;
        $data_arr = array();

        $number = 1;
        foreach ($record as $records) {
            $no = $number++;
            $nopermohonan = $records->notiket;
            $tglpermohonan = AppHelper::instance()->indonesian_date(strtotime($records->wktCreateMohon), 'j F Y', '');

            if ($records->status->urutanCase == 2) {
                $urlEdit = "/permohonan-masa-pensiun/" . AppHelper::instance()->enkrip($records->idMpp) . "/edit";
                $urlAjukan = "/permohonan-masa-pensiun/ajukan/" . AppHelper::instance()->enkrip($records->idMpp);
                $urlHapus = "/permohonan-masa-pensiun/delete/" . AppHelper::instance()->enkrip($records->idMpp);

                #status
                $status = '<span
                class="badge badge-light-warning fs-7 fw-bolder">' . $records->status->uraianStatus . '</span>';

                #aksi
                $aksi = '<a href="' . $urlEdit . '"
                       class="btn btn-sm btn-icon btn-warning text-center mb-3"
                       data-bs-toggle="tooltip" data-bs-placement="top" title="Edit">
                       <span class="las la-edit fs-1"></span>
                   </a>
                   <a href="' . $urlAjukan . '"
                       class="btn btn-sm btn-icon btn-primary mb-3" data-bs-toggle="tooltip"
                       data-bs-placement="top" title="Ajukan">
                       <span class="fa fa-paper-plane"></span>
                   </a>
                   <a href="' . $urlHapus . '" class="btn btn-sm btn-icon btn-danger mb-3"
                       data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus">
                       <span class="fa fa-trash"></span>
                   </a>';
            } elseif ($records->status->urutanCase == 0 || $records->status->urutanCase == 12) {
                $status = '<span class="badge badge-light-danger fs-7 fw-bolder mb-3">' . $records->status->uraianStatus . '</span>';
                $aksi = '<span data-bs-toggle="tooltip" data-bs-placement="top" title="Preview">
                        <button type="button"
                            class="btn btn-bold btn-icon btn-info btn-sm detilModal"
                            data-mpp="' . AppHelper::instance()->enkrip($records->idMpp) . '"
                            data-tiket="' . $records->notiket . '"
                            data-tanggal="' . date("Y-m-d", strtotime($records->wktCreateMohon)) . '"
                            data-keterangan="' . $records->alasanTolakUpkLokal . '"
                            data-status="' . $records->status->uraianStatus . '"
                            data-bs-toggle="modal" data-bs-target="#viewStatus">
                            <span class="fa fa-eye"></span>
                        </button>
                    </span>';
            } else {
                $status = '<span class="badge badge-light-success fs-7 fw-bolder">' . $records->status->uraianStatus . '</span>';
                $aksi = '';
            }

            $data_arr[] = array(
                "no" => $no,
                "nopermohonan" => $nopermohonan,
                "tglpermohonan" => $tglpermohonan,
                "status" => $status,
                "aksi" => $aksi
            );
        }

        $respons = array(
            "draw" => intval($draw),
            "recordsTotal" => $totalRecords,
            "recordsFiltered" => $totalRecordswithFilter,
            "data" => $data_arr
        );

        echo json_encode($respons);
    }

    private function downloadFile($namafile)
    {
        $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);

        $response = Http::get(env('API_URL') . '/files/' . $namafile);

        file_put_contents($tmp_direktori, $response->body());

        $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

        return response()->download($tmp_direktori, $namafile, $headers);
    }

    #tidak di gunakan ( serverside datatable optional)

    private function getFile($namafile)
    {
        $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);

        $response = Http::get(env('API_URL_SDM07') . '/api/v1/files/' . $namafile);

        file_put_contents($tmp_direktori, $response->body());

        return response()->file($tmp_direktori, [
            'Content-Type' => 'application/pdf'
        ]);
    }


}
