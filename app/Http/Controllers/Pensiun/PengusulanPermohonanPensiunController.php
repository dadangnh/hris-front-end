<?php

namespace App\Http\Controllers\Pensiun;

use App\Helpers\AppHelper;
use App\Helpers\CommonVariable;
use App\Helpers\LoginHelper;
use App\Helpers\PensiunApiHelper;
use App\HelpersAppHelper;
use App\Models\MenuModel;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;


class PengusulanPermohonanPensiunController extends Controller
{

    public function homePermohonan()
    {
        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'pengusulan-pensiun/permohonan');

        #set parameter
        $nip = session()->get('user')['nip18'];

        #pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $size = 5;

        #response
        $response = PensiunApiHelper::get('/api/v1/pps/drafts?page=' . $page . '&size=' . $size . '&nip=' . $nip);
        #data
        $data['dataPPS'] = $response['data']->data;
        $data['totalItems'] = $response['data']->totalItems;
        $data['totalPages'] = $response['data']->totalPages;
        $data['currentPage'] = $response['data']->currentPage;
        $data['numberOfElements'] = $response['data']->numberOfElements;
        $data['size'] = $response['data']->size;
        $data['page'] = $page;

        // dd($data);
        #data get validation
        if ($response['status'] == 1) {
            return view('pensiun.pengusulanpensiun.home_add_pengusulan_permohonan_pensiun', $data);
        } else {
            return abort(500);
        }
    }

    public function logstatuspp()
    {
        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'pengusulan-pensiun/permohonan');

        #parameter
        $idPPDekrip = AppHelper::instance()->dekrip($_GET['idPp']);

        #pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $size = 10;

        #get data log by nip
        $response = PensiunApiHelper::get('/api/v1/pps/logs?idPp=' . $idPPDekrip);
        $data['logPp'] = $response['data']->data;
        $data['totalItems'] = $response['data']->totalItems;
        $data['totalPages'] = $response['data']->totalPages;
        $data['currentPage'] = $response['data']->currentPage;
        $data['numberOfElements'] = $response['data']->numberOfElements;
        $data['size'] = $response['data']->size;
        $data['page'] = $page;

        if ($response['status'] == 1) {
            return ($data);
        } else {
            return (["status" => 0, "message" => 'Gagal Mengambil Data']);
        }
    }

    public function generateSurat($id)
    {
        #parameter
        $nip9 = session()->get('user')['nip9'];
        $kdKanwil = session()->get('user')['kdkanwil'];

        #Surat Permohonan Perampingan Organisasi
        if ($id == '1') {
        } #Surat Keterangan Tim Penguji Kesehatan
        elseif ($id == '2') {
        } #Surat Ketarangan  Tidak Melaporkan Diri setelah menjalani cuti diluar tanggungan negara
        elseif ($id == '3') {
        } #Data Perorangan Calon Penerima Pensiun
        elseif ($id == '4') {

            $url = env('API_URL_SDM07') . '/api/v1/pps/generate/dpcp?nipPendek=' . $nip9;
            $response = Http::accept('application/octet-stream')->post($url, ['']);

            #validasi balikan
            if (!$response->successful()) {
                return redirect()->back()->with('error', 'Gagal Generate File Surat Keterangan');
            }

            #create download file
            $namafile = 'DPCP.docx';
            $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);
            file_put_contents($tmp_direktori, $response->body());
            $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

            #return data
            $data = response()->download($tmp_direktori, $namafile, $headers);
            return ($data);
        } #PPKPNS
        elseif ($id == '5') {
        } #Surat Pernyataan Hukdis
        elseif ($id == '6') {

            $url = env('API_URL_SDM07') . '/api/v1/pps/generate/sper_hukdis_es_2?nipPendek=' . $nip9 . '&kdKanwil=' . $kdKanwil;
            $response = Http::accept('application/octet-stream')->post($url, ['']);

            #validasi balikan
            if (!$response->successful()) {
                return redirect()->back()->with('error', 'Gagal Generate File Surat Keterangan');
            }

            #create download file
            $namafile = 'Surat_Pernyataan_Hukdis_(Es_2).docx';
            $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);
            file_put_contents($tmp_direktori, $response->body());
            $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

            #return data
            $data = response()->download($tmp_direktori, $namafile, $headers);
            return ($data);
        } #Surat Pernyataan Pidana (Kanwil)
        elseif ($id == '7') {

            $url = env('API_URL_SDM07') . '/api/v1/pps/generate/sper_pidana_es_2?nipPendek=' . $nip9 . '&kdKanwil=' . $kdKanwil;
            $response = Http::accept('application/octet-stream')->post($url, ['']);

            #validasi balikan
            if (!$response->successful()) {
                return redirect()->back()->with('error', 'Gagal Generate File Surat Keterangan');
            }

            #create download file
            $namafile = 'Surat_Pernyataan_Pidana_(Es_2).docx';
            $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);
            file_put_contents($tmp_direktori, $response->body());
            $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

            #return data
            $data = response()->download($tmp_direktori, $namafile, $headers);
            return ($data);
        } #Surat Permohonan Berhenti Sebagai PNS
        elseif ($id == '8') {
            $url = env('API_URL_SDM07') . '/api/v1/pps/generate/sper_berhenti_pns?nipPendek=' . $nip9;
            $response = Http::accept('application/octet-stream')->post($url, ['']);

            #validasi balikan
            if (!$response->successful()) {
                return redirect()->back()->with('error', 'Gagal Generate File Surat Pernyataan');
            }

            #create download file
            $namafile = 'Surat_Pernyataan_Berhenti_PNS.docx';
            $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);
            file_put_contents($tmp_direktori, $response->body());
            $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

            #return data
            $data = response()->download($tmp_direktori, $namafile, $headers);
            return ($data);
        } #Surat Keterangan Kematian
        elseif ($id == '9') {
            $url = env('API_URL_SDM07') . '/api/v1/pps/generate/sket_meniggal_dunia?nipPendek=' . $nip9;
            $response = Http::accept('application/octet-stream')->post($url, ['']);

            #validasi balikan
            if (!$response->successful()) {
                return redirect()->back()->with('error', 'Gagal Generate File Surat Keterangan');
            }

            #create download file
            $namafile = 'Surat_Keterangan_Kematian.docx';
            $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);
            file_put_contents($tmp_direktori, $response->body());
            $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

            #return data
            $data = response()->download($tmp_direktori, $namafile, $headers);
            return ($data);
        } #Keputusan Penetapan Tewas
        elseif ($id == '10') {
        } #Surat Pernyataan Hilang
        elseif ($id == '11') {
            $url = env('API_URL_SDM07') . '/api/v1/pps/generate/sper_pns_hilang?nipPendek=' . $nip9;
            $response = Http::accept('application/octet-stream')->post($url, ['']);

            #validasi balikan
            if (!$response->successful()) {
                return redirect()->back()->with('error', 'Gagal Generate File Surat Pernyataan');
            }

            #create download file
            $namafile = 'Surat_Pernyataan_Hilang.docx';
            $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);
            file_put_contents($tmp_direktori, $response->body());
            $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

            #return data
            $data = response()->download($tmp_direktori, $namafile, $headers);
            return ($data);
        } #Surat Konfirmasi Hukdis
        elseif ($id == '12') {
        } #Surat Konfirmasi Ikatan Dinas
        elseif ($id == '13') {
        } #Surat Konfirmasi Sedang dalam Pemeriksaan
        elseif ($id == '14') {
        } #Surat Permintaan Konfirmasi Hukdis
        elseif ($id == '15') {
        } #Surat Permintaan Konfirmasi Ikatan Dinas
        elseif ($id == '16') {
        } #Surat Permintaan Konfirmasi Sedang dalam Pemeriksaan
        elseif ($id == '17') {
        } #Surat Pernyataan Tidak Menguasai Rumah Dinas
        elseif ($id == '18') {
            $url = env('API_URL_SDM07') . '/api/v1/pps/generate/sper_rumah_dinas?nipPendek=' . $nip9 . '&kdKanwil=' . $kdKanwil;
            $response = Http::accept('application/octet-stream')->post($url, ['']);

            #validasi balikan
            if (!$response->successful()) {
                return redirect()->back()->with('error', 'Gagal Generate File Surat Keterangan');
            }

            #create download file
            $namafile = 'Surat_Pernyataan_Tidak_Menguasai_Rumah_Dinas.docx';
            $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);
            file_put_contents($tmp_direktori, $response->body());
            $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

            #return data
            $data = response()->download($tmp_direktori, $namafile, $headers);
            return ($data);
        } #Surat Pernyataan Tidak Menguasai Barang Milik Negara
        elseif ($id == '19') {

            $url = env('API_URL_SDM07') . '/api/v1/pps/generate/bmn?nipPendek=' . $nip9 . '&kdKanwil=' . $kdKanwil;
            $response = Http::accept('application/octet-stream')->post($url, ['']);

            #validasi balikan
            if (!$response->successful()) {
                return redirect()->back()->with('error', 'Gagal Generate File Surat Keterangan');
            }

            #create download file
            $namafile = 'Surat_Pernyataan_Tidak_Menguasai_BMN.docx';
            $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);
            file_put_contents($tmp_direktori, $response->body());
            $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

            #return data
            $data = response()->download($tmp_direktori, $namafile, $headers);
            return ($data);
        } #Surat Pernyataan Pidana (Pegawai)
        elseif ($id == '20') {

            $url = env('API_URL_SDM07') . '/api/v1/pps/generate/sper_pidana_pegawai?nipPendek=' . $nip9;
            $response = Http::accept('application/octet-stream')->post($url, ['']);

            #validasi balikan
            if (!$response->successful()) {
                return redirect()->back()->with('error', 'Gagal Generate File Surat Keterangan');
            }

            #create download file
            $namafile = '_Surat_Pernyataan_Pidana_(Pegawai).docx';
            $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);
            file_put_contents($tmp_direktori, $response->body());
            $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

            #return data
            $data = response()->download($tmp_direktori, $namafile, $headers);
            return ($data);
        }


    }

    public function generateDitolak($id)
    {
        $idDekrip = $idDekrip = AppHelper::instance()->dekrip($id);
        #parameter
        $nip9 = session()->get('user')['nip9'];
        $kdKanwil = session()->get('user')['kdkanwil'];

        // dd($idDekrip);

        $url = env('API_URL_SDM07') . '/api/v1/pps/generate/konsep_surat_persetujuan/tolak?id=' . $idDekrip . '&nipPendek=' . $nip9 . '&kdKanwil=' . $kdKanwil;
        // dd($url);
        $response = Http::accept('application/octet-stream')->post($url, ['']);
        // dd($response);

        #validasi balikan
        if (!$response->successful()) {
            return redirect()->back()->with('error', 'Gagal Generate File Surat Keterangan');
        }

        #create download file
        $namafile = 'Surat_Persetujuan_Di_tolak.docx';
        $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);
        file_put_contents($tmp_direktori, $response->body());
        $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

        #return data
        $data = response()->download($tmp_direktori, $namafile, $headers);
        return ($data);


    }

    public function generateDitangguhkan($id)
    {
        $idDekrip = $idDekrip = AppHelper::instance()->dekrip($id);
        #parameter
        $nip9 = session()->get('user')['nip9'];
        $kdKanwil = session()->get('user')['kdkanwil'];

        $url = env('API_URL_SDM07') . '/api/v1/pps/generate/konsep_surat_persetujuan/tangguhkan?id=' . $idDekrip . '&nipPendek=' . $nip9 . '&kdKanwil=' . $kdKanwil;
        $response = Http::accept('application/octet-stream')->post($url, ['']);

        #validasi balikan
        if (!$response->successful()) {
            return redirect()->back()->with('error', 'Gagal Generate File Surat Keterangan');
        }

        #create download file
        $namafile = 'Surat_Persetujuan_Di_tangguhkan.docx';
        $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);
        file_put_contents($tmp_direktori, $response->body());
        $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

        #return data
        $data = response()->download($tmp_direktori, $namafile, $headers);
        return ($data);


    }

    public function generateDisetujui($id)
    {
        $idDekrip = $idDekrip = AppHelper::instance()->dekrip($id);
        #parameter
        $nip9 = session()->get('user')['nip9'];
        $kdKanwil = session()->get('user')['kdkanwil'];

        $url = env('API_URL_SDM07') . '/api/v1/pps/generate/konsep_surat_persetujuan/setujui?id=' . $idDekrip . '&nipPendek=' . $nip9 . '&kdKanwil=' . $kdKanwil;

        $response = Http::accept('application/octet-stream')->post($url, ['']);

        #validasi balikan
        if (!$response->successful()) {
            return redirect()->back()->with('error', 'Gagal Generate File Surat Keterangan');
        }

        #create download file
        $namafile = 'Surat_Persetujuan_Di_setujui.docx';
        $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);
        file_put_contents($tmp_direktori, $response->body());
        $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

        #return data
        $data = response()->download($tmp_direktori, $namafile, $headers);
        return ($data);

    }

    public function addPermohonan()
    {
        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'pengusulan-pensiun/permohonan');

        #masa kerja
        $tahundiangkat = substr(session()->get('user')['nip18'], 8, 4);
        $bulandiangkat = substr(session()->get('user')['nip18'], 12, 2);
        $tmt = new DateTime($tahundiangkat . "-" . $bulandiangkat . "-01");

        #now time
        $currentDate = now();
        $currYearMont = new DateTime($currentDate->format('Ym'));

        #cari diff masa kerja
        $diffmasakerja = ($currYearMont->diff($tmt));
        $diffmonths = ($diffmasakerja->y * 12) + $diffmasakerja->m;

        #cari usia
        $tahunlahir = substr(session()->get('user')['nip18'], 0, 4);
        $currYear = $currentDate->format('Y');
        $usia = $currYear - $tahunlahir;

        if ($diffmonths >= '20' && $usia >= '50') {
            #response
            $response = PensiunApiHelper::get('/api/v1/pps/pensiuns/dokumens?idJnsPensiun=2');

            #data
            $data['dokumensPP'] = $response['data'];

            if ($response['status'] == 1) {
                return view('pensiun.pengusulanpensiun.add_pengusulan_permohonan_pensiun', $data);
            } else {
                return abort(500);
            }
        } else {
            return redirect()->back()->with('error', 'Usia dan Masa Kerja belum memenuhi persyaratan');
        }

    }

    public function simpanPermohonan(Request $request)
    {

        #looping inputan
        foreach ($request->all('surat')['surat'] as $key => $value) {

            #proses upload file
            $uploadfile = $this->upload($value['file']);

            #validsi upload file
            if ($uploadfile['status'] != 1) {
                return redirect()->back()->with('error', $uploadfile['message']);
            } else {
                $filesk = $uploadfile['file'];
            }

            #making array file
            $dataArr[] = [
                "idDokPensiun" => 0,
                "idJnsDok" => $key,
                "namaFile" => $filesk,
                "noSurat" => $value['nomorSurat']
            ];
        }

        #Set last date of month
        $akbulan = $request->input('akhirbulan');

        #set body
        $data = [
            "pegawaiId" => session()->get('user')['pegawaiId'],
            "namaPegawai" => session()->get('user')['namaPegawai'],
            "nip18" => session()->get('user')['nip18'],
            "pangkatId" => session()->get('user')['pangkatId'],
            "namaPangkat" => session()->get('user')['pangkat'],
            "jabatanId" => session()->get('user')['jabatanId'],
            "namaJabatan" => session()->get('user')['jabatan'],
            "unitOrgId" => session()->get('user')['unitId'],
            "namaUnitOrg" => session()->get('user')['unit'],
            "kantorId" => session()->get('user')['kantorId'],
            "namaKantor" => session()->get('user')['kantor'],
            "pegawaiInputId" => session()->get('user')['pegawaiId'],
            "kdKpp" => session()->get('user')['kdkpp'],
            "kdKanwil" => session()->get('user')['kdkanwil'],
            "kdKantor" => session()->get('user')['kantorLegacyKode'],
            "idJnsPensiun" => 2,
            "alamat" => session()->get('user')['alamatJalan'] . ',' . session()->get('user')['kelurahan'] . ',' . session()->get('user')['kecamatan'] . ',' . session()->get('user')['kabupaten'] . ',' . session()->get('user')['provinsi'],
            "akBulan" => $akbulan,
            "alasan" => $request->input('alasan'),
            "idDokSketBerhentiPns" => 0,
            "sketBerhentiPns" => "tesets",
            "dokPensiun" => $dataArr
        ];

        #start endpoint pp
        $response = PensiunApiHelper::post('/api/v1/pps', ($data));
        // dd($response);
        #validasi
        if ($response['status'] == 1) {
            return redirect('pengusulan-pensiun/permohonan')->with('success', 'Data berhasil di simpan');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    private function upload($file)
    {
        $filepath = $file->getRealPath();
        $nama = $file->getClientOriginalName();
        $mime = $file->getMimeType();
        $ukuran = $file->getSize();
        // dd($filepath,$nama,$mime,$ukuran);
        # validasi mime file
        if ($mime != "application/pdf") {
            $retUpload = ['status' => 0, 'message' => 'Format file bukan .pdf'];
        } elseif ($ukuran > 2097152) {
            $retUpload = ['status' => 0, 'message' => 'Ukuran file lebih dari 2 MB'];
        } else {
            $response = Http::attach('file', file_get_contents($filepath), $nama)
                ->put(env('API_URL_SDM07') . '/api/v1/files');

            if (!$response->successful()) {
                $message = $response->body();

                $retUpload = ['status' => 0, 'message' => 'Gagal Melakukan Upload File'];

            } else {
                $retUpload = ['status' => 1, 'file' => $response->body()];
            }
        }

        return $retUpload;
    }

    public function editPermohonan($id)
    {
        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'pengusulan-pensiun/permohonan');

        #set parameter id PP
        $idDekrip = AppHelper::instance()->dekrip($id);

        #endpoint get data pp by id
        $response = PensiunApiHelper::get('/api/v1/pps/' . $idDekrip);
        $data['dataPP'] = $response['data'];

        #loop data dokumen
        foreach ($response['data'] as $key) {
            $datakey = $key;
        }
        $data['dokumensPP'] = $datakey;

        #response return
        if ($response['status'] == 1) {
            return view('pensiun.pengusulanpensiun.edit_pengusulan_permohonan_pensiun', $data);
        } else {
            return redirect()->back()->with('error', 'Gagal Mengambil Data Permohonan Pensiun');
        }
    }

    public function setEditPermohonan(Request $request, $id)
    {

        #id
        $idDekrip = AppHelper::instance()->dekrip($id);

        #loop data surat
        foreach ($request->all('surat')['surat'] as $key => $value) {
            $dataArr = [
                "idDokPensiun" => AppHelper::instance()->dekrip($value['idDokPensiun']),
                "idJnsDok" => AppHelper::instance()->dekrip($value['idJnsDok']),
                "namaFile" => AppHelper::instance()->dekrip($value['filelama']),
                "noSurat" => $value['nomorSurat']
            ];

            if (isset($value['file'])) {

                #delete file
                $delete_file = PensiunApiHelper::delete('/api/v1/files/' . $value['filelama'], []);

                #proses upload file
                $uploadfile = $this->upload($value['file']);

                #validsi upload file
                if ($uploadfile['status'] != 1) {
                    return redirect()->back()->with('error', $uploadfile['message']);
                } else {
                    $filesk = $uploadfile['file'];
                }

                #replace data
                $dataArr['namaFile'] = $filesk;
            }
            $arrData[] = $dataArr;
        }

        #akBulan set tanggal akhir bulan
        $akbulan = $request->input('akhirbulan');

        #set body untuk post ke end point post pp
        $data = [
            "pegawaiId" => session()->get('user')['pegawaiId'],
            "namaPegawai" => session()->get('user')['namaPegawai'],
            "nip18" => session()->get('user')['nip18'],
            "pangkatId" => session()->get('user')['pangkatId'],
            "namaPangkat" => session()->get('user')['pangkat'],
            "jabatanId" => session()->get('user')['pegawaiId'],
            "namaJabatan" => session()->get('user')['jabatan'],
            "unitOrgId" => session()->get('user')['unitId'],
            "namaUnitOrg" => session()->get('user')['unit'],
            "kantorId" => session()->get('user')['kantorId'],
            "namaKantor" => session()->get('user')['kantor'],
            "pegawaiInputId" => session()->get('user')['pegawaiId'],
            "kdKpp" => session()->get('user')['kdkpp'],
            "kdKanwil" => session()->get('user')['kdkanwil'],
            "kdKantor" => session()->get('user')['unitLegacyKode'],
            "idJnsPensiun" => 2,
            "alamat" => session()->get('user')['alamatJalan'] . ',' . session()->get('user')['kelurahan'] . ',' . session()->get('user')['kecamatan'] . ',' . session()->get('user')['kabupaten'] . ',' . session()->get('user')['provinsi'],
            "akBulan" => $akbulan,
            "alasan" => $request->input('alasan'),
            "idDokSketBerhentiPns" => 0,
            "sketBerhentiPns" => "tesets",
            "dokPensiun" => $arrData
        ];

        $response = Http::put(env('API_URL_SDM07') . '/api/v1/pps?idPp=' . $idDekrip, ($data));

        if (!$response->successful()) {
            return redirect()->back()->with('error', 'Data gagal di simpan');
        } else {
            return redirect()->back()->with('success', 'Data berhasil di simpan');
        }
    }

    public function deletePermohonan($id)
    {
        #set parameter id PP
        $idDekrip = AppHelper::instance()->dekrip($id);

        #endpoint get data pp by id pp
        $response = PensiunApiHelper::get('/api/v1/pps/' . $idDekrip);

        #validation
        if ($response['status'] != 1) {
            return response()->json(["status" => 0, "message" => 'Gagal Mengambil Data Permohonan Pensiun']);
        } else {
            #looping data file
            foreach ($response['data'] as $key) {
                $datakey = $key;
            }
            $dokumensPP = $datakey;

            #looping delete file
            foreach ($dokumensPP as $keys => $items) {
                #delete file sotrage
                $deletefile = PensiunApiHelper::delete('/api/v1/files/' . $items->namaFile, []);
                // if()
            }
            // dd($deletefile);
        }

        #set body
        $data = [
            "pegawaiId" => session()->get('user')['pegawaiId'],
            "namaPegawai" => session()->get('user')['namaPegawai'],
            "nip18" => session()->get('user')['nip18'],
            "pangkatId" => session()->get('user')['pangkatId'],
            "namaPangkat" => session()->get('user')['pangkat'],
            "jabatanId" => session()->get('user')['pegawaiId'],
            "namaJabatan" => session()->get('user')['jabatan'],
            "unitOrgId" => session()->get('user')['unitId'],
            "namaUnitOrg" => session()->get('user')['unit'],
            "kantorId" => session()->get('user')['kantorId'],
            "namaKantor" => session()->get('user')['kantor'],
            "pegawaiInputId" => session()->get('user')['pegawaiId'],
            "kdKpp" => session()->get('user')['kdkpp'],
            "kdKanwil" => session()->get('user')['kdkanwil'],
            "kdKantor" => session()->get('user')['unitLegacyKode'],
        ];

        $response = PensiunApiHelper::delete('/api/v1/pps/' . $idDekrip, ($data));
        return response()->json($response);
    }

    public function ajukanPermohonan($id)
    {
        #idMpp
        $idDekrip = AppHelper::instance()->dekrip($id);

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #all data
        $data = [
            "pegawaiId" => session()->get('user')['pegawaiId'],
            "namaPegawai" => session()->get('user')['namaPegawai'],
            "nip18" => session()->get('user')['nip18'],
            "pangkatId" => session()->get('user')['pangkatId'],
            "namaPangkat" => session()->get('user')['pangkat'],
            "jabatanId" => session()->get('user')['jabatanId'],
            "namaJabatan" => session()->get('user')['jabatan'],
            "unitOrgId" => session()->get('user')['unitId'],
            "namaUnitOrg" => session()->get('user')['unit'],
            "kantorId" => session()->get('user')['kantorId'],
            "namaKantor" => session()->get('user')['kantor'],
            "pegawaiInputId" => session()->get('user')['pegawaiId'],
            "kdKpp" => $kdkpp,
            "kdKanwil" => $kdkanwil,
            "kdKantor" => session()->get('user')['kantorLegacyKode'],
            "keterangan" => "Permohonan diajukan",
        ];

        // dd(json_encode($data), $idDekrip);

        #endpoint
        $url = env('API_URL_SDM07') . '/api/v1/pps/proses?id=' . $idDekrip;
        $response = Http::patch($url, $data);
        // dd($response);

        # cek status servis
        if (!$response->successful()) {

            $x = json_decode($response);
            $message = 'Error Web Services';
            $return = ['status' => 0, 'message' => $message];

        } else {
            #get data body
            $body = $response->getBody();
            $data = json_decode($body);

            $return = ['status' => 1, 'data' => $data];
        }
        return response()->json($return);

    }

    public function tolakPermohonan($id)
    {
        #idMpp
        $idDekrip = AppHelper::instance()->dekrip($id);

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #all data
        $data = [
            "pegawaiId" => session()->get('user')['pegawaiId'],
            "namaPegawai" => session()->get('user')['namaPegawai'],
            "nip18" => session()->get('user')['nip18'],
            "pangkatId" => session()->get('user')['pangkatId'],
            "namaPangkat" => session()->get('user')['pangkat'],
            "jabatanId" => session()->get('user')['jabatanId'],
            "namaJabatan" => session()->get('user')['jabatan'],
            "unitOrgId" => session()->get('user')['unitId'],
            "namaUnitOrg" => session()->get('user')['unit'],
            "kantorId" => session()->get('user')['kantorId'],
            "namaKantor" => session()->get('user')['kantor'],
            "pegawaiInputId" => session()->get('user')['pegawaiId'],
            "kdKpp" => $kdkpp,
            "kdKanwil" => $kdkanwil,
            "kdKantor" => session()->get('user')['kantorLegacyKode'],
            "keterangan" => "Permohonan Ditolak",
        ];

        // dd(json_encode($data), $idDekrip);

        #endpoint
        $url = env('API_URL_SDM07') . '/api/v1/pps/tolak?id=' . $idDekrip;
        $response = Http::patch($url, $data);
        // dd(json_decode($response));

        # cek status servis
        if (!$response->successful()) {

            $x = json_decode($response);
            if (isset($x->apierror->message)) {
                $message = 'Error Web Services';
            }
            $return = ['status' => 0, 'message' => $message];

        } else {
            #get data body
            $body = $response->getBody();
            $data = json_decode($body);

            $return = ['status' => 1, 'data' => $data];
        }
        return response()->json($return);

    }

    public function setujuiPermohonan($id)
    {
        #idMpp
        $idDekrip = AppHelper::instance()->dekrip($id);

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #set body
        $data = [
            "pegawaiId" => session()->get('user')['pegawaiId'],
            "namaPegawai" => session()->get('user')['namaPegawai'],
            "nip18" => session()->get('user')['nip18'],
            "pangkatId" => session()->get('user')['pangkatId'],
            "namaPangkat" => session()->get('user')['pangkat'],
            "jabatanId" => session()->get('user')['jabatanId'],
            "namaJabatan" => session()->get('user')['jabatan'],
            "unitOrgId" => session()->get('user')['unitId'],
            "namaUnitOrg" => session()->get('user')['unit'],
            "kantorId" => session()->get('user')['kantorId'],
            "namaKantor" => session()->get('user')['kantor'],
            "pegawaiInputId" => session()->get('user')['pegawaiId'],
            "kdKpp" => $kdkpp,
            "kdKanwil" => $kdkanwil,
            "kdKantor" => session()->get('user')['kantorLegacyKode'],
            "keterangan" => "Disetujui oleh " . session()->get('user')['namaPegawai']
        ];

        #endpoint
        $url = env('API_URL_SDM07') . '/api/v1/pps/setujui?id=' . $idDekrip;
        $response = Http::patch($url, $data);

        # cek status servis
        if (!$response->successful()) {

            $x = json_decode($response);
            if (isset($x->apierror->message)) {
                $message = 'Error Web Services';
            }
            $return = ['status' => 0, 'message' => $message];

        } else {
            #get data body
            $body = $response->getBody();
            $data = json_decode($body);
            $return = ['status' => 1, 'data' => $data];
        }

        return response()->json($return);

    }

    public function homePermohonanUpk()
    {
        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'pengusulan-pensiun/permohonan/upk');

        #get data by nip
        $nip = session()->get('user')['nip18'];

        #pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $size = 5;

        #response
        $response = PensiunApiHelper::get('/api/v1/pps/drafts/upks?page=' . $page . '&size=' . $size . '&nip=' . $nip);

        #data
        $data['dataPPS'] = $response['data']->data;
        $data['totalItems'] = $response['data']->totalItems;
        $data['totalPages'] = $response['data']->totalPages;
        $data['currentPage'] = $response['data']->currentPage;
        $data['numberOfElements'] = $response['data']->numberOfElements;
        $data['size'] = $response['data']->size;
        $data['page'] = $page;

        #validation
        if ($response['status'] == 1) {
            return view('pensiun.pengusulanpensiun.home_add_pengusulan_permohonan_pensiun_upk', $data);
        } else {
            return abort(500);
        }
    }

    public function getFilePermohonan($id)
    {
        #id
        $idDekrip = AppHelper::instance()->dekrip($id);

        #file
        $tmp_direktori = tempnam(sys_get_temp_dir(), $idDekrip);
        $response = Http::get(env('API_URL_SDM07') . '/api/v1/files/' . $idDekrip);
        file_put_contents($tmp_direktori, $response->body());
        return response()->file($tmp_direktori, [
            'Content-Type' => 'application/pdf'
        ]);
    }

    public function getDataPegawai($nip)
    {
        #refresh token when it expired
        if (session()->get('login_info')['_expired'] <= now()->getTimestamp()) {

            #refresh token
            $refreshtoken = $this->refreshToken(session()->get('login_info')['_refresh_token']);

            #response mu jika dia expired
            $dataHref = LoginHelper::get('master_pegawais/' . $nip . '/data', $refreshtoken['data']->token);
            if ($dataHref['status'] != 1) {
                return response()->json(['status' => 0, 'message' => 'Gagal Mengambil Data']);
            }

            #golongan dan pangkat
            $pangkatId = $dataHref['data']->data->pangkatId;
            $pangkat = LoginHelper::get('pangkats/' . $pangkatId, $refreshtoken['data']->token);
            $golongan = LoginHelper::get($pangkat['data']->golongan, $refreshtoken['data']->token);

            $jabatan['golongan'] = $golongan['data']->nama;

            #data jabatans iam
            $dataJabatan = LoginHelper::iamget('/api/jabatans/' . $dataHref['data']->data->jabatanId, $refreshtoken['data']->token);
            $jabatan['kelasJabatan'] = $dataJabatan['data']->nama;
            $jabatan['levelJabatan'] = $dataJabatan['data']->level;
            $jabatan['kd_jabatan_id'] = $dataJabatan['data']->legacyKode;

            #data kantor iam
            $dataKantors = LoginHelper::iamget('/api/kantors?page=1&legacyKode=' . $dataHref['data']->data->kantorLegacyKode, $refreshtoken['data']->token);
            $jeniskantor = LoginHelper::iamget($dataKantors['data'][0]->jenisKantor, $refreshtoken['data']->token);
            $kanwil = LoginHelper::iamget($dataKantors['data'][0]->parent, $refreshtoken['data']->token);

            if ($jeniskantor['data']->tipe != 'UPT') {
                $kdKpp = $dataKantors['data'][0]->legacyKodeKpp;
                $kdKanwil = $dataKantors['data'][0]->legacyKodeKanwil;
                $nmKanwil = $kanwil['data']->nama;
                $tipeKantor = '';
            } else {
                $kdKpp = '0';
                $kdKanwil = '0';
                $nmKanwil = $kanwil['data']->nama;
                $tipeKantor = $jeniskantor['data']->tipe;
            }

            #data collection
            if ($kdKpp == session()->get('user')['kdkpp']) {
                #masa kerja
                $tahundiangkat = substr($dataHref['data']->data->nip18, 8, 4);
                $bulandiangkat = substr($dataHref['data']->data->nip18, 12, 2);
                $tmt = new DateTime($tahundiangkat . "-" . $bulandiangkat . "-01");

                #now time
                $currentDate = now();
                $currYearMont = new DateTime($currentDate->format('Ym'));

                #validasi usia dan masa kerja
                foreach ($dataHref['data']->data as $key) {
                    $dataUser = [
                        "id" => $dataHref['data']->data->id,
                        "pegawaiId" => $dataHref['data']->data->pegawaiId,
                        "namaPegawai" => $dataHref['data']->data->namaPegawai,
                        "nip9" => $dataHref['data']->data->nip9,
                        "nip18" => $dataHref['data']->data->nip18,
                        // "nik" => $dataHref['data']->data->nik,
                        "pangkatId" => $dataHref['data']->data->pangkatId,
                        "pangkat" => $dataHref['data']->data->pangkat,
                        "jabatanId" => $dataHref['data']->data->jabatanId,
                        "jabatan" => $dataHref['data']->data->jabatan,
                        "unitId" => $dataHref['data']->data->unitId,
                        "unit" => $dataHref['data']->data->unit,
                        "unitLegacyKode" => $dataHref['data']->data->unitLegacyKode,
                        "kantorId" => $dataHref['data']->data->kantorId,
                        "kantor" => $dataHref['data']->data->kantor,
                        "kantorLegacyKode" => $dataHref['data']->data->kantorLegacyKode,
                        "golongan" => $golongan['data']->nama,
                        // "levelJabatan"=>$dataJabatan['data']->level,
                        // "kd_jabatan_id"=>$dataJabatan['data']->legacyKode,
                        "kdkpp" => $kdKpp,
                        "kdkanwil" => $kdKanwil,
                        "nmkanwil" => $nmKanwil,
                        // "tipekantor"=>$tipeKantor,
                        // "agamaId" => $dataHref['data']->data->agamaId,
                        // "agama" => $dataHref['data']->data->agama,
                        // "jenisKelaminId" => $dataHref['data']->data->jenisKelaminId,
                        // "jenisKelamin" => $dataHref['data']->data->jenisKelamin,
                        "alamatJalan" => $dataHref['data']->data->alamatJalan,
                        // "kelurahanId" => $dataHref['data']->data->kelurahanId,
                        "kelurahan" => $dataHref['data']->data->kelurahan,
                        // "kecamatanId" => $dataHref['data']->data->kecamatanId,
                        "kecamatan" => $dataHref['data']->data->kecamatan,
                        // "kabupatenId" => $dataHref['data']->data->kabupatenId,
                        "kabupaten" => $dataHref['data']->data->kabupaten,
                        // "provinsiId" => $dataHref['data']->data->provinsiId,
                        "provinsi" => $dataHref['data']->data->provinsi,
                        "tanggalLahir" => $dataHref['data']->data->tanggalLahir,
                        "tempatLahir" => $dataHref['data']->data->tempatLahir,
                        // "pensiun" => $dataHref['data']->data->pensiun,

                    ];
                }
            } else {
                return response()->json(['status' => 0, 'message' => "NIP yang di input berada diluar wewenang"]);
            }

        } else {
            #responmu jika dia not expired
            #data href
            $dataHref = LoginHelper::get('master_pegawais/' . $nip . '/data', session()->get('login_info')['_token']);
            if ($dataHref['status'] != 1) {
                return response()->json(['status' => 0, 'message' => 'Gagal Mengambil Data']);
            }


            #golongan dan pangkat
            $pangkatId = $dataHref['data']->data->pangkatId;
            $pangkat = LoginHelper::get('pangkats/' . $pangkatId, session()->get('login_info')['_token']);
            $golongan = LoginHelper::get($pangkat['data']->golongan, session()->get('login_info')['_token']);

            $jabatan['golongan'] = $golongan['data']->nama;

            #data jabatans iam
            $dataJabatan = LoginHelper::iamget('/api/jabatans/' . $dataHref['data']->data->jabatanId, session()->get('login_info')['_token']);
            $jabatan['kelasJabatan'] = $dataJabatan['data']->nama;
            $jabatan['levelJabatan'] = $dataJabatan['data']->level;
            $jabatan['kd_jabatan_id'] = $dataJabatan['data']->legacyKode;

            #data kantor iam
            $dataKantors = LoginHelper::iamget('/api/kantors?page=1&legacyKode=' . $dataHref['data']->data->kantorLegacyKode, session()->get('login_info')['_token']);
            $jeniskantor = LoginHelper::iamget($dataKantors['data'][0]->jenisKantor, session()->get('login_info')['_token']);
            $kanwil = LoginHelper::iamget($dataKantors['data'][0]->parent, session()->get('login_info')['_token']);

            if ($jeniskantor['data']->tipe != 'UPT') {
                $kdKpp = $dataKantors['data'][0]->legacyKodeKpp;
                $kdKanwil = $dataKantors['data'][0]->legacyKodeKanwil;
                $nmKanwil = $kanwil['data']->nama;
                $tipeKantor = '';
            } else {
                $kdKpp = '0';
                $kdKanwil = '0';
                $nmKanwil = $kanwil['data']->nama;
                $tipeKantor = $jeniskantor['data']->tipe;
            }

            #data collection
            if ($kdKpp == session()->get('user')['kdkpp']) {

                #masa kerja
                $tahundiangkat = substr($dataHref['data']->data->nip18, 8, 4);
                $bulandiangkat = substr($dataHref['data']->data->nip18, 12, 2);
                $tmt = new DateTime($tahundiangkat . "-" . $bulandiangkat . "-01");

                #now time
                $currentDate = now();
                $currYearMont = new DateTime($currentDate->format('Ym'));

                #validasi usia dan masa kerja
                foreach ($dataHref['data']->data as $key) {
                    $dataUser = [
                        "id" => $dataHref['data']->data->id,
                        "pegawaiId" => $dataHref['data']->data->pegawaiId,
                        "namaPegawai" => $dataHref['data']->data->namaPegawai,
                        "nip9" => $dataHref['data']->data->nip9,
                        "nip18" => $dataHref['data']->data->nip18,
                        // "nik" => $dataHref['data']->data->nik,
                        "pangkatId" => $dataHref['data']->data->pangkatId,
                        "pangkat" => $dataHref['data']->data->pangkat,
                        "jabatanId" => $dataHref['data']->data->jabatanId,
                        "jabatan" => $dataHref['data']->data->jabatan,
                        "unitId" => $dataHref['data']->data->unitId,
                        "unit" => $dataHref['data']->data->unit,
                        "unitLegacyKode" => $dataHref['data']->data->unitLegacyKode,
                        "kantorId" => $dataHref['data']->data->kantorId,
                        "kantor" => $dataHref['data']->data->kantor,
                        "kantorLegacyKode" => $dataHref['data']->data->kantorLegacyKode,
                        "golongan" => $golongan['data']->nama,
                        // "levelJabatan"=>$dataJabatan['data']->level,
                        // "kd_jabatan_id"=>$dataJabatan['data']->legacyKode,
                        "kdkpp" => $kdKpp,
                        "kdkanwil" => $kdKanwil,
                        "nmkanwil" => $nmKanwil,
                        // "tipekantor"=>$tipeKantor,
                        // "agamaId" => $dataHref['data']->data->agamaId,
                        // "agama" => $dataHref['data']->data->agama,
                        // "jenisKelaminId" => $dataHref['data']->data->jenisKelaminId,
                        // "jenisKelamin" => $dataHref['data']->data->jenisKelamin,
                        "alamatJalan" => $dataHref['data']->data->alamatJalan,
                        // "kelurahanId" => $dataHref['data']->data->kelurahanId,
                        "kelurahan" => $dataHref['data']->data->kelurahan,
                        // "kecamatanId" => $dataHref['data']->data->kecamatanId,
                        "kecamatan" => $dataHref['data']->data->kecamatan,
                        // "kabupatenId" => $dataHref['data']->data->kabupatenId,
                        "kabupaten" => $dataHref['data']->data->kabupaten,
                        // "provinsiId" => $dataHref['data']->data->provinsiId,
                        "provinsi" => $dataHref['data']->data->provinsi,
                        "tanggalLahir" => $dataHref['data']->data->tanggalLahir,
                        "tempatLahir" => $dataHref['data']->data->tempatLahir,
                        // "pensiun" => $dataHref['data']->data->pensiun,

                    ];
                }
            } else {
                return response()->json(['status' => 0, 'message' => "NIP yang di input berada diluar wewenang"]);
            }
        }

        # return
        return response()->json(['status' => 1, 'data' => $dataUser]);

    }

    private function refreshToken($refreshtoken)
    {
        $response = Http::post(env('IAM_SIMULASIKAN_URL') . 'api/token/refresh', ['refresh_token' => $refreshtoken]);

        # cek status servis
        if (!$response->successful()) {

            $message = 'Error Web Services';

            return ['status' => 0, 'message' => $message];

        }

        $data = json_decode($response);

        return ['status' => 1, 'data' => $data];
    }

    public function addPermohonanUpk()
    {
        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'pengusulan-pensiun/permohonan/upk');

        #response
        $responsejenis = PensiunApiHelper::get('/api/v1/pps/pensiuns');

        #data
        $data['jenissPP'] = $responsejenis['data'];

        #validasi balikan
        if ($responsejenis['status'] == 1) {
            return view('pensiun.pengusulanpensiun.add_pengusulan_permohonan_pensiun_upk', $data);
        } else {
            return view('errors.error');
        }
    }

    public function getBerkasPensiun($id, $idRcase)
    {
        #get data jenis
        $responsejenis = PensiunApiHelper::get('/api/v1/pps/pensiuns/dokumens?idJnsPensiun=' . $id . '&$idRcase=' . $idRcase);
        $data = $responsejenis['data'];
        if ($responsejenis['status'] == 1) {
            return response()->json(['status' => 1, 'data' => $data]);
        } else {
            return response()->json(['status' => 0, 'message' => 'Gagal Mengambil Data']);
        }
    }

    public function simpanPermohonanUpk(Request $request)
    {
        #looping inputan
        foreach ($request->all('surat')['surat'] as $key => $value) {

            #proses upload file
            $uploadfile = $this->upload($value['file']);

            #validsi upload file
            if ($uploadfile['status'] != 1) {
                return redirect()->back()->with('error', $uploadfile['message']);
            } else {
                $filesk = $uploadfile['file'];
            }

            #making array file
            $dataArr[] = [
                "idDokPensiun" => 0,
                "idJnsDok" => $key,
                "namaFile" => $filesk,
                "noSurat" => $value['nomorSurat']
            ];
        }

        #Set last date of month
        $akbulan = $request->input('akhirbulan');
        $tgl_terakhir = date('Y-m-t', strtotime($akbulan));

        #set body
        $data = [
            "pegawaiId" => $request->input('pegawaiId'),
            "namaPegawai" => $request->input('nama'),
            "nip18" => $request->input('nip'),
            "pangkatId" => $request->input('pangkatId'),
            "namaPangkat" => $request->input('pangkat'),
            "jabatanId" => $request->input('jabatanId'),
            "namaJabatan" => $request->input('jabatan'),
            "unitOrgId" => $request->input('unitOrgId'),
            "namaUnitOrg" => $request->input('namaUnitOrg'),
            "kantorId" => $request->input('kantorId'),
            "namaKantor" => $request->input('unitorganisasi'),
            "pegawaiInputId" => $request->input('pegawaiInputId'),
            "kdKpp" => $request->input('kdKpp'),
            "kdKanwil" => $request->input('kdKanwil'),
            "kdKantor" => $request->input('kdKantor'),
            "idJnsPensiun" => (int) $request->input('jenispensiun'),
            "alamat" => $request->input('alamat'),
            "akBulan" => $tgl_terakhir,
            "alasan" => $request->input('alasan'),
            "idDokSketBerhentiPns" => 0,
            "sketBerhentiPns" => "null",
            "dokPensiun" => $dataArr
        ];

        // dd(json_encode($data));

        #start endpoint pp
        $response = PensiunApiHelper::post('/api/v1/pps', ($data));
        // dd($response);
        #validasi
        if ($response['status'] == 1) {
            return redirect('pengusulan-pensiun/permohonan/upk')->with('success', 'Data berhasil di simpan');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }

    }

    public function editPermohonanUpk($id)
    {
        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'pengusulan-pensiun/permohonan/upk');

        #set parameter id PP
        $idDekrip = AppHelper::instance()->dekrip($id);

        #endpoint get data pp by id
        $response = PensiunApiHelper::get('/api/v1/pps/' . $idDekrip);
        $data['dataPP'] = $response['data'];

        foreach ($response['data'] as $key) {
            $datakey = $key;
        }
        $data['dokumensPP'] = $datakey;

        #response
        $responsejenis = PensiunApiHelper::get('/api/v1/pps/pensiuns');

        #data
        $data['jenissPP'] = $responsejenis['data'];

        #response return
        if ($response['status'] == 1 && $responsejenis['status'] == 1) {
            return view('pensiun.pengusulanpensiun.edit_pengusulan_permohonan_pensiun_upk', $data);
        } else {
            return redirect()->back()->with('error', 'Gagal Mengambil Data Permohonan Pensiun');
        }
    }

    public function setEditPermohonanUpk(Request $request, $id)
    {
        #id
        $idDekrip = AppHelper::instance()->dekrip($id);
        $idJnsPensiun = AppHelper::instance()->dekrip($request->input('jenispensiun'));

        #loop data surat
        foreach ($request->all('surat')['surat'] as $key => $value) {
            $dataArr = [
                "idDokPensiun" => AppHelper::instance()->dekrip($value['idDokPensiun']),
                "idJnsDok" => AppHelper::instance()->dekrip($value['idJnsDok']),
                "namaFile" => AppHelper::instance()->dekrip($value['filelama']),
                "noSurat" => $value['nomorSurat']
            ];

            if (isset($value['file'])) {

                #delete file
                $delete_file = PensiunApiHelper::delete('/api/v1/files/' . $value['filelama'], []);

                #proses upload file
                $uploadfile = $this->upload($value['file']);

                #validsi upload file
                if ($uploadfile['status'] != 1) {
                    return redirect()->back()->with('error', $uploadfile['message']);
                } else {
                    $filesk = $uploadfile['file'];
                }

                #replace data
                $dataArr['namaFile'] = $filesk;
            }
            $arrData[] = $dataArr;
        }

        #akBulan set tanggal akhir bulan
        $akbulan = $request->input('akhirbulan');

        #set body untuk post ke end point post pp
        $data = [
            "pegawaiId" => session()->get('user')['pegawaiId'],
            "namaPegawai" => session()->get('user')['namaPegawai'],
            "nip18" => session()->get('user')['nip18'],
            "pangkatId" => session()->get('user')['pangkatId'],
            "namaPangkat" => session()->get('user')['pangkat'],
            "jabatanId" => session()->get('user')['pegawaiId'],
            "namaJabatan" => session()->get('user')['jabatan'],
            "unitOrgId" => session()->get('user')['unitId'],
            "namaUnitOrg" => session()->get('user')['unit'],
            "kantorId" => session()->get('user')['kantorId'],
            "namaKantor" => session()->get('user')['kantor'],
            "pegawaiInputId" => session()->get('user')['pegawaiId'],
            "kdKpp" => session()->get('user')['kdkpp'],
            "kdKanwil" => session()->get('user')['kdkanwil'],
            "kdKantor" => session()->get('user')['unitLegacyKode'],
            "idJnsPensiun" => $idJnsPensiun,
            "alamat" => session()->get('user')['alamatJalan'] . ',' . session()->get('user')['kelurahan'] . ',' . session()->get('user')['kecamatan'] . ',' . session()->get('user')['kabupaten'] . ',' . session()->get('user')['provinsi'],
            "akBulan" => $akbulan,
            "alasan" => $request->input('alasan'),
            "idDokSketBerhentiPns" => 0,
            "sketBerhentiPns" => "tesets",
            "dokPensiun" => $arrData
        ];

        $response = Http::put(env('API_URL_SDM07') . '/api/v1/pps?idPp=' . $idDekrip, ($data));

        if (!$response->successful()) {
            return redirect()->back()->with('error', 'Data gagal di simpan');
        } else {
            return redirect()->back()->with('success', 'Data berhasil di simpan');
        }
    }

    public function penelitianPermohonan()
    {
        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'pengusulan-pensiun/penelitian');

        #get data by nip
        $nip = session()->get('user')['nip18'];
        $nip9 = session()->get('user')['nip9'];
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $size = 5;

        #proses pengambilan data
        if (session()->get('user')['unitLegacyKode'] == '0424071300' && session()->get('user')['kd_jabatan_id'] == '403') { #upk kpp
            #response
            $response = PensiunApiHelper::get('/api/v1/pps/upks_kpp?page=' . $page . '&size=' . $size . '&kdKpp=' . $kdkpp);

            #data
            #get data upload jenis 4 untuk parameter
            $dokUPK = [];
            foreach ($response['data']->data as $key => $vals) {
                foreach ($vals->dokPensiunList as $key => $items) {
                    if ($items->idJnsDok->idJnsDok == 4) {
                        $dokUPK[] = [
                            "idPps" => $vals->idPp,
                            "idJenis" => $items->idJnsDok->idJnsDok,
                            "namaDoc" => $items->idJnsDok->namaDok,
                        ];
                    }

                }
            }
            $data['dataPPS'] = $response['data']->data;
            $data['dokUPK'] = $dokUPK;
            $data['totalItems'] = $response['data']->totalItems;
            $data['totalPages'] = $response['data']->totalPages;
            $data['currentPage'] = $response['data']->currentPage;
            $data['numberOfElements'] = $response['data']->numberOfElements;
            $data['size'] = $response['data']->size;
            $data['page'] = $page;
            return view('pensiun.pengusulanpensiun.home_penelitian_lokal', $data);

        } elseif (session()->get('user')['kd_jabatan_id'] == '304' && $kdkanwil != '0' && $kdkanwil < 900) { #kepala kpp

            #response
            $response = PensiunApiHelper::get('/api/v1/pps/kepalas_kpp?page=' . $page . '&size=' . $size . '&kdKpp=' . $kdkpp);

            if ($response['status'] == 1) {
                #data
                $data['dataPPS'] = $response['data']->data;
                $data['totalItems'] = $response['data']->totalItems;
                $data['totalPages'] = $response['data']->totalPages;
                $data['currentPage'] = $response['data']->currentPage;
                $data['numberOfElements'] = $response['data']->numberOfElements;
                $data['size'] = $response['data']->size;
                $data['page'] = $page;

                return view('pensiun.pengusulanpensiun.home_penelitian_kepala_kpp', $data);

            } else {
                return redirect()->back()->with('error', 'Gagal Mengambil Data');
            }

        } elseif (session()->get('user')['unitLegacyKode'] == '0423010100' && session()->get('user')['kd_jabatan_id'] == '403' || session()->get('user')['unitLegacyKode'] == '0421010100' && session()->get('user')['kd_jabatan_id'] == '403' || $kdkanwil == '0' && session()->get('user')['kd_jabatan_id'] == '403' || $kdkanwil > 900 && session()->get('user')['kd_jabatan_id'] == '403') { #upk kanwil / UPT / PPDDP / Es 2
            #response
            $response = PensiunApiHelper::get('/api/v1/pps/upks_wilayah?page=' . $page . '&size=' . $size . '&kdKanwil=' . $kdkanwil);

            #set default array data
            $arrayData = [];
            $dataArrUploadUpk = [];

            #jika balikan sukses
            if ($response['status'] == 1) {

                #looping untuk membuat validasi
                foreach ($response['data']->data as $key => $value) {
                    #get jenis dok
                    $responseJenisDoc = PensiunApiHelper::get('/api/v1/pps/pensiuns/dokumens?idJnsPensiun=' . $value->idJnsPensiun->idJnsPensiun . '&idRcase=' . $value->status->id);

                    #validasi respon return
                    if ($responseJenisDoc['status'] != 1) {
                        return redirect()->back()->with('error', 'Gagal Mengambil Data Dokumen Pensiun');
                    }


                    #set data arr
                    $dataArr[$value->idPp] = [];
                    foreach ($value->dokPensiunList as $key => $items) {
                        if ($items->idJnsDok->idJnsDok == 6 || $items->idJnsDok->idJnsDok == 7 || $items->idJnsDok->idJnsDok == 18 || $items->idJnsDok->idJnsDok == 19) {
                            $dataArr[$value->idPp][] =
                                [
                                    "idJnsDok" => (int) $items->idJnsDok->idJnsDok,
                                    "namaDok" => $items->idJnsDok->namaDok,
                                ];
                            unset($value->dokPensiunList[$items->idJnsDok->idJnsDok]);
                            continue;
                        }
                    }

                    foreach ($responseJenisDoc['data'] as $key => $items) {
                        $dataArrUploadUpk[$value->idPp][] =
                            ["idDokPensiun" => 0,
                                "idJnsDok" => (int) $items->idJnsDok->idJnsDok,
                                "namaDok" => $items->idJnsDok->namaDok,
                            ];
                    }

                    $arrayData [] = [
                        "dataPP" => $value,
                        "draft" => count($dataArrUploadUpk[$value->idPp]),
                        "final" => count($dataArr[$value->idPp])
                    ];
                }

                #data
                $data['dataPPS'] = $arrayData;
                $data['totalItems'] = $response['data']->totalItems;
                $data['totalPages'] = $response['data']->totalPages;
                $data['currentPage'] = $response['data']->currentPage;
                $data['numberOfElements'] = $response['data']->numberOfElements;
                $data['size'] = $response['data']->size;
                $data['page'] = $page;
                // dd($data);
                return view('pensiun.pengusulanpensiun.home_penelitian_upk_kanwil', $data);
            } else {
                return redirect()->back()->with('error', 'Gagal Mengambil Data');
            }

        } elseif (session()->get('user')['kd_jabatan_id'] == '203' || $kdkanwil == '0' && session()->get('user')['kd_jabatan_id'] == '304' || $kdkanwil > 900 && session()->get('user')['kd_jabatan_id'] == '204' || $kdkanwil > 900 && session()->get('user')['kd_jabatan_id'] == 202) { #kakanwil / upt / ppddp  / es 2
            #response
            $response = PensiunApiHelper::get('/api/v1/pps/kepalas_wilayah?page=' . $page . '&size=' . $size . '&kdKanwil=' . $kdkanwil);

            if ($response['status'] == 1) {
                #data
                $data['dataPPS'] = $response['data']->data;
                $data['totalItems'] = $response['data']->totalItems;
                $data['totalPages'] = $response['data']->totalPages;
                $data['currentPage'] = $response['data']->currentPage;
                $data['numberOfElements'] = $response['data']->numberOfElements;
                $data['size'] = $response['data']->size;
                $data['page'] = $page;

                return view('pensiun.pengusulanpensiun.home_penelitian_kepala_kanwil', $data);
            } else {
                return redirect()->back()->with('error', 'Gagal Mengambil Data');
            }

        } elseif (session()->get('user')['unitLegacyKode'] == '0402060400') {
            #response
            $response = PensiunApiHelper::get('/api/v1/pps/pegawais_pdpp?page=' . $page . '&size=' . $size . '&nipDisposisi=' . $nip9);

            #default array data
            $arrayData = [];
            $dataArr = [];
            $dataArrUploadUpk = [];

            #jika respon berhasil
            if ($response['status'] == 1) {

                #looping untuk membuat validasi
                foreach ($response['data']->data as $key => $value) {
                    #get jenis dok
                    $responseJenisDoc = PensiunApiHelper::get('/api/v1/pps/pensiuns/dokumens?idJnsPensiun=' . $value->idPp->idJnsPensiun->idJnsPensiun . '&idRcase=' . $value->idPp->status->id);

                    #validasi respon return
                    if ($responseJenisDoc['status'] != 1) {
                        return redirect()->back()->with('error', 'Gagal Mengambil Data Dokumen Pensiun');
                    }

                    #cek data arr dokumen yang sudah di upload di pelaksana pdpp
                    foreach ($value->idPp->dokPensiunList as $key => $items) {
                        if ($items->idJnsDok->idJnsDok == 12 || $items->idJnsDok->idJnsDok == 13 || $items->idJnsDok->idJnsDok == 14 || $items->idJnsDok->idJnsDok == 15 || $items->idJnsDok->idJnsDok == 16 || $items->idJnsDok->idJnsDok == 17) {
                            $dataArr[$value->idPp->idPp][] =
                                [
                                    "idJnsDok" => (int) $items->idJnsDok->idJnsDok,
                                    "namaDok" => $items->idJnsDok->namaDok,
                                ];
                        } else {
                            $dataArr[$value->idPp->idPp] = [];
                        }
                    }

                    #semua dokumen yang wajib di upload di pelaksana pdpp
                    if (count($responseJenisDoc['data']) != null) {
                        foreach ($responseJenisDoc['data'] as $key => $items) {
                            $dataArrUploadUpk[$value->idPp->idPp][] =
                                ["idDokPensiun" => 0,
                                    "idJnsDok" => (int) $items->idJnsDok->idJnsDok,
                                    "namaDok" => $items->idJnsDok->namaDok,
                                ];
                        }
                    } else {
                        $dataArrUploadUpk[$value->idPp->idPp] = [];
                    }

                    $arrayData [] = [
                        "dataPP" => $value,
                        "draft" => count($dataArrUploadUpk[$value->idPp->idPp]),
                        "final" => count($dataArr[$value->idPp->idPp])
                    ];
                }

                #data
                $data['dataPPS'] = $arrayData;
                $data['totalItems'] = $response['data']->totalItems;
                $data['totalPages'] = $response['data']->totalPages;
                $data['currentPage'] = $response['data']->currentPage;
                $data['numberOfElements'] = $response['data']->numberOfElements;
                $data['size'] = $response['data']->size;
                $data['page'] = $page;
                // dd($data);
                return view('pensiun.pengusulanpensiun.home_penelitian_pdpp', $data);
            } else {
                return redirect()->back()->with('error', 'Gagal Mengambil Data');
            }

        } else {
            #sementara
            return redirect()->back()->with('error', 'Anda Tidak Memiliki Akses ke Halaman Ini');
        }
    }

    public function detilPenelitianPermohonan($id)
    {
        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'pengusulan-pensiun/penelitian');

        #set parameter id PP
        $idDekrip = AppHelper::instance()->dekrip($id);

        #proses pengambilan data
        if (session()->get('user')['unitLegacyKode'] == '0424071300' && session()->get('user')['kd_jabatan_id'] == '403') { #upk kpp
            #data pp by id
            $response = PensiunApiHelper::get('/api/v1/pps/' . $idDekrip);

            #response return
            if ($response['status'] != 1) {
                return redirect()->back()->with('error', 'Gagal Mengambil Data Permohonan Pensiun');
            }

            #data dokumen pp upload upk
            $idJnsPen = $response['data']->idJnsPensiun->idJnsPensiun;
            $idRcase = $response['data']->status->id;

            #respon jenis dokumen yang di upload pada saat upk lokal
            $responseJenisDoc = PensiunApiHelper::get('/api/v1/pps/pensiuns/dokumens?idJnsPensiun=' . $idJnsPen . '&idRcase=' . $idRcase);

            #validasi
            if ($responseJenisDoc['status'] != 1) {
                return redirect()->back()->with('error', 'Gagal Mengambil Data Permohonan Pensiun');
            }

            #loop data doc
            foreach ($response['data'] as $key) {
                $datakey = $key;
            }

            #default data array
            $dataArr = [];
            $dataArrUpk = [];

            #set data arr
            foreach ($datakey as $key => $value) {
                #mengambil semua data dokumen yang sudah di upload kecuali jenis dok 4
                if ($value->idJnsDok->idJnsDok != 4) {
                    $dataArr[] =
                        ["idDokPensiun" => $value->idDokPensiun,
                            "idJnsDok" => (int) $value->idJnsDok->idJnsDok,
                            "namaDok" => $value->idJnsDok->namaDok,
                            "namaFile" => $value->namaFile,
                            "noSurat" => $value->noSurat
                        ];
                }

                #mengambil semua data yang sudah
                foreach ($responseJenisDoc['data'] as $key => $items) {
                    if ($value->idJnsDok->idJnsDok == $items->idJnsDok->idJnsDok) {
                        $dataArrUpk =
                            ["idDokPensiun" => $value->idDokPensiun,
                                "idJnsDok" => (int) $items->idJnsDok->idJnsDok,
                                "namaDok" => $items->idJnsDok->namaDok,
                                "namaFile" => $value->namaFile,
                                "noSurat" => $value->noSurat,
                            ];
                    }
                }
            }

            #mengambil data yang wajib di upload di upk lokal
            foreach ($responseJenisDoc['data'] as $key => $items) {
                $dataArrUploadUpk =
                    ["idDokPensiun" => 0,
                        "idJnsDok" => (int) $items->idJnsDok->idJnsDok,
                        "namaDok" => $items->idJnsDok->namaDok,
                    ];
            }

            #data prep
            $data['dataPP'] = $response['data'];
            $data['docPPUpk'] = $dataArrUpk;
            $data['dokumensPP'] = $dataArr;
            $data['docPP'] = $dataArrUploadUpk;
            // dd($data);
            #return
            if ($response['status'] == 1) {
                return view('pensiun.pengusulanpensiun.detil_penelitian_pengusulan_permohonan_pensiun_upk_lokal', $data);
            } else {
                return redirect()->back()->with('error', 'Gagal Mengambil Data Permohonan Pensiun');
            }

        } elseif (session()->get('user')['kd_jabatan_id'] == '304' && session()->get('user')['kdkanwil'] != '0' && session()->get('user')['kdkanwil'] < 900) { #kepala kpp

            #endpoint get data pp by id & get doc for upload
            $response = PensiunApiHelper::get('/api/v1/pps/' . $idDekrip);

            #loop data doc
            foreach ($response['data'] as $key) {
                $datakey = $key;
            }

            #data prep
            $data['dataPP'] = $response['data'];
            $data['dokumensPP'] = $datakey;
            // dd($data);

            #return
            if ($response['status'] == 1) {
                return view('pensiun.pengusulanpensiun.detil_penelitian_pengusulan_permohonan_pensiun_kepala_kantor', $data);
            } else {
                return redirect()->back()->with('error', 'Gagal Mengambil Data Permohonan Pensiun');
            }
        } elseif (session()->get('user')['unitLegacyKode'] == '0423010100' && session()->get('user')['kd_jabatan_id'] == '403' || session()->get('user')['unitLegacyKode'] == '0421010100' && session()->get('user')['kd_jabatan_id'] == '403' || session()->get('user')['kdkanwil'] == '0' && session()->get('user')['kd_jabatan_id'] == '403' || session()->get('user')['kdkanwil'] > 900 && session()->get('user')['kd_jabatan_id'] == '403') { #upk kanwil / UPT / PPDDP / Es 2
            #data pp by id
            $response = PensiunApiHelper::get('/api/v1/pps/' . $idDekrip);

            #response return
            if ($response['status'] != 1) {
                return redirect()->back()->with('error', 'Gagal Mengambil Data Permohonan Pensiun');
            }

            #data dokumen pp upload upk
            $idJnsPen = $response['data']->idJnsPensiun->idJnsPensiun;
            $idRcase = $response['data']->status->id;

            #response jenis dokumen yang wajib di upload pada upk wilayah
            $responseJenisDoc = PensiunApiHelper::get('/api/v1/pps/pensiuns/dokumens?idJnsPensiun=' . $idJnsPen . '&idRcase=' . $idRcase);

            #validasi
            if ($responseJenisDoc['status'] != 1) {
                return redirect()->back()->with('error', 'Gagal Mengambil Data Permohonan Pensiun');
            }

            #loop data doc
            foreach ($response['data'] as $key) {
                $datakey = $key;
            }

            #default data array
            $dataArr = [];
            $dataArrUpk = [];

            #set data arr
            foreach ($datakey as $key => $value) {
                foreach ($responseJenisDoc['data'] as $key => $items) {
                    #Array Data Yang Sudah Upload karena simpan draft
                    if ($value->idJnsDok->idJnsDok == $items->idJnsDok->idJnsDok) {
                        $dataArrUpk[] =
                            ["idDokPensiun" => $value->idDokPensiun,
                                "idJnsDok" => $items->idJnsDok->idJnsDok,
                                "namaDok" => $items->idJnsDok->namaDok,
                                "namaFile" => $value->namaFile,
                                "noSurat" => $value->noSurat,
                            ];
                    }
                }

                #semua data upload berkas alur sebelumnya
                if ($value->idJnsDok->idJnsDok != 6 && $value->idJnsDok->idJnsDok != 7 && $value->idJnsDok->idJnsDok != 18 && $value->idJnsDok->idJnsDok != 19) {

                    $dataArr[] = ["idDokPensiun" => $value->idDokPensiun,
                        "idJnsDok" => $value->idJnsDok->idJnsDok,
                        "namaDok" => $value->idJnsDok->namaDok,
                        "namaFile" => $value->namaFile,
                        "noSurat" => $value->noSurat
                    ];
                }
            }

            #seluruh data wajib upload
            foreach ($responseJenisDoc['data'] as $key => $items) {
                $array[$items->idJnsDok->idJnsDok] =
                    [
                        "idDokPensiun" => 0,
                        "idJnsDok" => $items->idJnsDok->idJnsDok,
                        "namaDok" => $items->idJnsDok->namaDok,
                        "namaFile" => "",
                        "noSurat" => "",
                    ];

                #cek jika sudah ada file atau data yang di upload sebelumnya
                foreach ($datakey as $key => $value) {
                    if ($value->idJnsDok->idJnsDok == $items->idJnsDok->idJnsDok) {
                        $array[$items->idJnsDok->idJnsDok]['idDokPensiun'] = $value->idDokPensiun;
                        $array[$items->idJnsDok->idJnsDok]['namaFile'] = $value->namaFile;
                        $array[$items->idJnsDok->idJnsDok]['noSurat'] = $value->noSurat;
                        unset($datakey[$key]);
                        continue;
                    }
                }
            }

            #data prep
            $data['dataPP'] = $response['data'];
            $data['dokumensPP'] = $dataArr;
            $data['docPP'] = $array;
            $data['docPPUpk'] = $dataArrUpk;
            // dd($data);
            #return
            if ($response['status'] == 1) {
                return view('pensiun.pengusulanpensiun.detil_penelitian_pengusulan_permohonan_pensiun_upk_kanwil', $data);
            } else {
                return redirect()->back()->with('error', 'Gagal Mengambil Data Permohonan Pensiun');
            }
        } elseif (session()->get('user')['kd_jabatan_id'] == '203' || session()->get('user')['kdkanwil'] == '0' && session()->get('user')['kd_jabatan_id'] == '304' || session()->get('user')['kdkanwil'] > 900 && session()->get('user')['kd_jabatan_id'] == '204' || session()->get('user')['kdkanwil'] > 900 && session()->get('user')['kd_jabatan_id'] == 202) { #kakanwil / upt / ppddp  / es 2
            #endpoint get data pp by id & get doc for upload
            $response = PensiunApiHelper::get('/api/v1/pps/' . $idDekrip);

            #loop data doc
            foreach ($response['data'] as $key) {
                $datakey = $key;
            }

            #data prep
            $data['dataPP'] = $response['data'];
            $data['dokumensPP'] = $datakey;
            // dd($data);

            #return
            if ($response['status'] == 1) {
                return view('pensiun.pengusulanpensiun.detil_penelitian_pengusulan_permohonan_pensiun_kepala_kanwil', $data);
            } else {
                return redirect()->back()->with('error', 'Gagal Mengambil Data Permohonan Pensiun');
            }
        } elseif (session()->get('user')['unitLegacyKode'] == '0402060400') {
            #data pp by id
            $response = PensiunApiHelper::get('/api/v1/pps/' . $idDekrip);

            #response return
            if ($response['status'] != 1) {
                return redirect()->back()->with('error', 'Gagal Mengambil Data Permohonan Pensiun');
            }

            #data dokumen pp upload upk
            $idJnsPen = $response['data']->idJnsPensiun->idJnsPensiun;
            $idRcase = $response['data']->status->id;

            #response jenis dokumen yang wajib di upload pada alur pelaksana pdpp
            $responseJenisDoc = PensiunApiHelper::get('/api/v1/pps/pensiuns/dokumens?idJnsPensiun=' . $idJnsPen . '&idRcase=' . $idRcase);

            #validasi
            if ($responseJenisDoc['status'] != 1) {
                return redirect()->back()->with('error', 'Gagal Mengambil Data Permohonan Pensiun');
            }

            #loop data doc
            foreach ($response['data'] as $key) {
                $datakey = $key;
            }

            #default data array
            $dataArr = [];
            $dataArrUpk = [];
            $array = [];

            #set data arr
            foreach ($datakey as $key => $value) {
                foreach ($responseJenisDoc['data'] as $key => $items) {
                    #Array Data Yang Sudah Upload karena simpan draft
                    if ($value->idJnsDok->idJnsDok == $items->idJnsDok->idJnsDok) {
                        $dataArrUpk[] =
                            ["idDokPensiun" => $value->idDokPensiun,
                                "idJnsDok" => $items->idJnsDok->idJnsDok,
                                "namaDok" => $items->idJnsDok->namaDok,
                                "namaFile" => $value->namaFile,
                                "noSurat" => $value->noSurat,
                            ];
                    }
                }

                #semua data upload berkas alur sebelumnya
                if ($value->idJnsDok->idJnsDok != 12 || $value->idJnsDok->idJnsDok != 13 || $value->idJnsDok->idJnsDok != 14 || $value->idJnsDok->idJnsDok != 15 || $value->idJnsDok->idJnsDok != 16 || $value->idJnsDok->idJnsDok != 17) {

                    $dataArr[] = ["idDokPensiun" => $value->idDokPensiun,
                        "idJnsDok" => $value->idJnsDok->idJnsDok,
                        "namaDok" => $value->idJnsDok->namaDok,
                        "namaFile" => $value->namaFile,
                        "noSurat" => $value->noSurat
                    ];
                }
            }

            #seluruh data wajib upload
            foreach ($responseJenisDoc['data'] as $key => $items) {
                $array[$items->idJnsDok->idJnsDok] =
                    [
                        "idDokPensiun" => 0,
                        "idJnsDok" => $items->idJnsDok->idJnsDok,
                        "namaDok" => $items->idJnsDok->namaDok,
                        "namaFile" => "",
                        "noSurat" => ""
                    ];
                #cek file jika sudah ada yang di upload
                foreach ($datakey as $key => $value) {
                    if ($value->idJnsDok->idJnsDok == $items->idJnsDok->idJnsDok) {
                        $array[$items->idJnsDok->idJnsDok]['idDokPensiun'] = $value->idDokPensiun;
                        $array[$items->idJnsDok->idJnsDok]['namaFile'] = $value->namaFile;
                        $array[$items->idJnsDok->idJnsDok]['noSurat'] = $value->noSurat;
                        unset($datakey[$key]);
                        continue;
                    }
                }
            }

            #data prep
            $data['dataPP'] = $response['data'];
            $data['dokumensPP'] = $dataArr;
            $data['docPP'] = $array;
            $data['docPPUpk'] = $dataArrUpk;
            // dd($data);
            #return
            if ($response['status'] == 1) {
                if (Route::getFacadeRoot()->current()->uri() == 'pengusulan-pensiun/penelitian/detil/final/{id}') {
                    return view('pensiun.pengusulanpensiun.detil_penelitian_pengusulan_permohonan_pensiun_pelaksana_pdpp_final', $data);
                } else {
                    return view('pensiun.pengusulanpensiun.detil_penelitian_pengusulan_permohonan_pensiun_pelaksana_pdpp', $data);
                }
            } else {
                return redirect()->back()->with('error', 'Gagal Mengambil Data Permohonan Pensiun');
            }
        } else {
            #sementara
            return redirect()->back()->with('error', 'Anda Tidak Memiliki Akses ke Halaman Ini');
        }
    }

    public function setDraftPenelitian(Request $request)
    {
        // dd($request->all());

        #preparation data from request
        $idPpDekrip = AppHelper::instance()->dekrip($request->input('idPp'));
        $arrData = [];
        foreach ($request->all('surat')['surat'] as $key => $value) {

            #jika file lama ada harus dibawa
            if ($value['filelama'] != null) {
                $namaFile = AppHelper::instance()->dekrip($value['filelama']);
            } else {
                $namaFile = $value['filelama'];
            }

            #set null default iddokpensiun
            if ($value['idDokPensiun'] != '0') {
                $idDokPensiun = AppHelper::instance()->dekrip($value['idDokPensiun']);
            } else {
                $idDokPensiun = null;
            }

            #drop key null
            if (($value['noSurat'] != null && !empty($namaFile)) || ($value['noSurat'] != null && !empty($value['file']))) {
                $dataArr = [
                    "idDokPensiun" => $idDokPensiun,
                    "idJnsDok" => AppHelper::instance()->dekrip($value['idJnsDok']),
                    "namaFile" => $namaFile,
                    "noSurat" => $value['noSurat']
                ];

                // dd($dataArr);

                if (isset($value['file'])) {

                    #delete file
                    if ($value['filelama'] != null) {
                        $delete_file = PensiunApiHelper::delete('/api/v1/files/' . $value['filelama'], []);
                    }

                    #proses upload file
                    $uploadfile = $this->upload($value['file']);
                    // dd($uploadfile);
                    #validsi upload file
                    if ($uploadfile['status'] != 1) {
                        return redirect()->back()->with('error', $uploadfile['message']);
                    } else {
                        $filesk = $uploadfile['file'];
                    }

                    #replace data
                    $dataArr['namaFile'] = $filesk;
                }
                $arrData[] = $dataArr;
            } else {
                // dd($key);
                unset($key);
                continue;
            }

        }
        // dd($arrData);

        # response draft
        $response = PensiunApiHelper::patch('/api/v1/pps/penelitian/draft?idPp=' . $idPpDekrip, ($arrData));
        // dd($response);
        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasil di simpan');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function setFinalPenelitian(Request $request, $id)
    {

        #id Dekrip param ajukan
        $idDekrip = AppHelper::instance()->dekrip($id);

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];


        #body
        #all data
        $data = [
            "pegawaiId" => session()->get('user')['pegawaiId'],
            "namaPegawai" => session()->get('user')['namaPegawai'],
            "nip18" => session()->get('user')['nip18'],
            "pangkatId" => session()->get('user')['pangkatId'],
            "namaPangkat" => session()->get('user')['pangkat'],
            "jabatanId" => session()->get('user')['jabatanId'],
            "namaJabatan" => session()->get('user')['jabatan'],
            "unitOrgId" => session()->get('user')['unitId'],
            "namaUnitOrg" => session()->get('user')['unit'],
            "kantorId" => session()->get('user')['kantorId'],
            "namaKantor" => session()->get('user')['kantor'],
            "pegawaiInputId" => session()->get('user')['pegawaiId'],
            "kdKpp" => $kdkpp,
            "kdKanwil" => $kdkanwil,
            "kdKantor" => session()->get('user')['kantorLegacyKode'],
            "keterangan" => "Permohonan diajukan",
        ];

        $response = PensiunApiHelper::patch('/api/v1/pps/penelitian/final?idPp=' . $idDekrip, ($data));
        return ($response);
    }

    public function getDisposisi()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'pengusulan-pensiun/permohonan/disposisi');

        #get data by nip
        $nip = session()->get('user')['nip18'];
        $nip9 = session()->get('user')['nip9'];
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #pagination
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $size = 5;

        if (session()->get('user')['unitLegacyKode'] == '0402060400' && session()->get('user')['kd_jabatan_id'] == '403') {
            #pdpp
            $responsePdpp = PensiunApiHelper::get('/api/v1/pps/kasubbags_pdpp?page=' . $page . '&size=' . $size);
            // $responsePdpp = PensiunApiHelper::get('/api/v1/pps/upks_kpp?page='.$page.'&size='.$size.'&kdKpp=315');

            $data['dataPPS'] = $responsePdpp['data']->data;
            $data['totalItems'] = $responsePdpp['data']->totalItems;
            $data['totalPages'] = $responsePdpp['data']->totalPages;
            $data['currentPage'] = $responsePdpp['data']->currentPage;
            $data['numberOfElements'] = $responsePdpp['data']->numberOfElements;
            $data['size'] = $responsePdpp['data']->size;
            $data['page'] = $page;
            $statusPdpp = $responsePdpp['status'];
            // dd($data);
            #cek balikan
            if ($statusPdpp == 1) {
                return view('pensiun.pengusulanpensiun.home_disposisi_pdpp', $data);
            } else {
                return redirect()->back()->with('error', 'Gagal Mengambil Data');
            }
        } else {
            #sementara
            return redirect()->back()->with('error', 'Anda Tidak Memiliki Akses ke Halaman Ini');
        }
    }

    public function getDetilDisposisi($id)
    {
        #id
        $idDekrip = AppHelper::instance()->dekrip($id);

        # menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'pengusulan-pensiun/permohonan/disposisi');

        #response data mpp
        $responsepp = PensiunApiHelper::get('/api/v1/pps/' . $idDekrip);
        #loop data doc
        foreach ($responsepp['data'] as $key) {
            $datakey = $key;
        }

        #data prep
        $data['dataPP'] = $responsepp['data'];
        $data['dokumensPP'] = $datakey;

        #response data disposisiPermohonan
        $responsedisposisi = PensiunApiHelper::get('/api/v1/pps/disposisi/pegawais?kdUnit=' . session()->get('user')['kantorLegacyKode'] . '&kdUnitOrg=' . session()->get('user')['unitLegacyKode']);
        $data['dataPPDisposisi'] = $responsedisposisi['data'];
        //  dd($data);
        #cekbalikan
        if ($responsepp['status'] == 1 && $responsedisposisi['status'] == 1) {
            return view('pensiun.pengusulanpensiun.detil_penelitian_pengusulan_permohonan_pensiun_disposisi', $data);
        } else {
            return redirect()->back()->with('error', 'Gagal Mengambil Data');
        }
    }

    public function setDisposisi(Request $request)
    {

        // dd($request->all());

        if ($request->input('namaNip') == null) {
            return response()->json(['status' => 0, 'message' => 'Pegawai belum dipilih!']);
        }

        #making array
        foreach ($request->input('namaNip') as $key => $value) {

            $data = explode("|", $value['nip']);
            $dataArr[] = [
                "nipDisposisi" => $data[0],
                "namaDisposisi" => $data[1],
            ];
        }

        #dataDisposisi
        $idDekrip = AppHelper::instance()->dekrip($request->input('id'));

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #response
        $response = PensiunApiHelper::patch('/api/v1/pps/disposisi?idPp=' . $idDekrip, [
            'pegawai' => [
                "pegawaiId" => session()->get('user')['pegawaiId'],
                "namaPegawai" => session()->get('user')['namaPegawai'],
                "nip18" => session()->get('user')['nip18'],
                "pangkatId" => session()->get('user')['pangkatId'],
                "namaPangkat" => session()->get('user')['pangkat'],
                "jabatanId" => session()->get('user')['jabatanId'],
                "namaJabatan" => session()->get('user')['jabatan'],
                "unitOrgId" => session()->get('user')['unitId'],
                "namaUnitOrg" => session()->get('user')['unit'],
                "kantorId" => session()->get('user')['kantorId'],
                "namaKantor" => session()->get('user')['kantor'],
                "pegawaiInputId" => session()->get('user')['pegawaiId'],
                "kdKpp" => $kdkpp,
                "kdKanwil" => $kdkanwil,
                "kdKantor" => session()->get('user')['kantorLegacyKode'],
                'keterangan' => 'Disposisi oleh ' . session()->get('user')['namaPegawai']
            ], 'dispoPegawai' => $dataArr
        ]);

        if ($response['status'] == 1) {
            return response()->json($response);

        } else {
            return response()->json(['status' => 0, 'message' => 'Disposisi gagal!']);
        }
    }

    public function uploadSuratPersetujuan(Request $request)
    {
        if ($request->input('hasilpenelitian') == null) {
            return response()->json(['status' => 0, 'message' => 'Form Hasil Penelitian Tidak Boleh Kosong']);
        }

        #Dari form input
        $idDekrip = AppHelper::instance()->dekrip($request->input('idPp'));
        $fileskpp = $request->files->get('skPP');
        $penelitian = $request->input('hasilpenelitian');

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #upload file
        $upload_sk_pp = $this->upload($fileskpp);
        if ($upload_sk_pp['status'] == 1) {

            #file sk mpp
            $skpp = $upload_sk_pp['file'];

            #all data
            $databody = [
                "pegawaiId" => session()->get('user')['pegawaiId'],
                "namaPegawai" => session()->get('user')['namaPegawai'],
                "nip18" => session()->get('user')['nip18'],
                "pangkatId" => session()->get('user')['pangkatId'],
                "namaPangkat" => session()->get('user')['pangkat'],
                "jabatanId" => session()->get('user')['jabatanId'],
                "namaJabatan" => session()->get('user')['jabatan'],
                "unitOrgId" => session()->get('user')['unitId'],
                "namaUnitOrg" => session()->get('user')['unit'],
                "kantorId" => session()->get('user')['kantorId'],
                "namaKantor" => session()->get('user')['kantor'],
                "pegawaiInputId" => session()->get('user')['pegawaiId'],
                "kdKpp" => $kdkpp,
                "kdKanwil" => $kdkanwil,
                "kdKantor" => session()->get('user')['kantorLegacyKode'],
                "suratPersetujuanPp" => $skpp,
                "noSuratPersetujuanPp" => $request->input('noSK'),
                "statusPersetujuanPp" => $penelitian,
                "keterangan" => 'Surat Persetujuan Di Upload oleh ' . session()->get('user')['namaPegawai']
            ];

            // dd(json_encode($databody));
            #response
            $url = env('API_URL_SDM07') . '/api/v1/pps/upload/surat_persetujuan_pp?id=' . $idDekrip;
            $response = Http::patch($url, $databody);
            // dd($response);


            #cekbalikan
            # cek status servis
            if (!$response->successful()) {

                $x = json_decode($response);

                $return = ['status' => 0, 'message' => 'Data Gagal Disimpan'];

            } else {
                #get data body
                $body = $response->getBody();
                $data = json_decode($body);
                $return = ['status' => 1, 'data' => $data];
            }

            return response()->json($return);

        } else {
            return response()->json(['status' => 0, 'message' => 'Gagal Melakukan Upload File SK']);
        }

    }

    public function uploadSKPp(Request $request)
    {
        #Dari form input
        $idDekrip = AppHelper::instance()->dekrip($request->input('idPp'));
        $fileskpp = $request->files->get('skPP');

        #kantor
        $kdkpp = session()->get('user')['kdkpp'];
        $kdkanwil = session()->get('user')['kdkanwil'];

        #upload file
        $upload_sk_pp = $this->upload($fileskpp);
        if ($upload_sk_pp['status'] == 1) {

            #file sk mpp
            $skpp = $upload_sk_pp['file'];

            #all data
            $databody = [
                "pegawaiId" => session()->get('user')['pegawaiId'],
                "namaPegawai" => session()->get('user')['namaPegawai'],
                "nip18" => session()->get('user')['nip18'],
                "pangkatId" => session()->get('user')['pangkatId'],
                "namaPangkat" => session()->get('user')['pangkat'],
                "jabatanId" => session()->get('user')['jabatanId'],
                "namaJabatan" => session()->get('user')['jabatan'],
                "unitOrgId" => session()->get('user')['unitId'],
                "namaUnitOrg" => session()->get('user')['unit'],
                "kantorId" => session()->get('user')['kantorId'],
                "namaKantor" => session()->get('user')['kantor'],
                "pegawaiInputId" => session()->get('user')['pegawaiId'],
                "kdKpp" => $kdkpp,
                "kdKanwil" => $kdkanwil,
                "kdKantor" => session()->get('user')['kantorLegacyKode'],
                "suratUsulanPp" => $skpp,
                "noSuratUsulanPp" => $request->input('noSK'),
                "keterangan" => 'Surat Persetujuan Di Upload oleh ' . session()->get('user')['namaPegawai']
            ];

            // dd($databody);
            #response
            $url = env('API_URL_SDM07') . '/api/v1/pps/upload/surat_usulan_pp?id=' . $idDekrip;
            $response = Http::patch($url, $databody);
            // dd($response);


            #cekbalikan
            # cek status servis
            if (!$response->successful()) {

                $x = json_decode($response);

                $return = ['status' => 0, 'message' => 'Data Gagal Disimpan'];

            } else {
                #get data body
                $body = $response->getBody();
                $data = json_decode($body);
                $return = ['status' => 1, 'data' => $data];
            }

            return response()->json($return);

        } else {
            return response()->json(['status' => 0, 'message' => 'Gagal Melakukan Upload File SK']);
        }

    }

    public function getSKPp($id)
    {
        #nama file
        $namaFile = AppHelper::instance()->dekrip($id);

        #file
        $tmp_direktori = tempnam(sys_get_temp_dir(), $namaFile);
        $response = Http::get(env('API_URL_SDM07') . '/api/v1/files/' . $namaFile);
        file_put_contents($tmp_direktori, $response->body());
        return response()->file($tmp_direktori, [
            'Content-Type' => 'application/pdf'
        ]);


    }

    public function generateKonsepSK($id)
    {
        $idDekrip = $idDekrip = AppHelper::instance()->dekrip($id);
        #parameter
        $nip9 = session()->get('user')['nip9'];
        $kdKanwil = session()->get('user')['kdkanwil'];

        $url = env('API_URL_SDM07') . '/api/v1/pps/generate/konsep_surat_usulan?id=' . $idDekrip . '&nipPendek=' . $nip9 . '&kdKanwil=' . $kdKanwil;

        $response = Http::accept('application/octet-stream')->post($url, ['']);
        // dd($response);
        #validasi balikan
        if (!$response->successful()) {
            return redirect()->back()->with('error', 'Gagal Generate File Surat');
        }

        #create download file
        $namafile = 'Surat_Usulan_PP.docx';
        $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);
        file_put_contents($tmp_direktori, $response->body());
        $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

        #return data
        $data = response()->download($tmp_direktori, $namafile, $headers);
        return ($data);
    }

    private function lastOfMonth($year, $month)
    {
        return date("Y-m-d", strtotime('-1 second', strtotime('+1 month', strtotime($month . '/01/' . $year . ' 00:00:00'))));
    }

    private function downloadFile($namafile)
    {
        $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);

        $response = Http::get(env('API_URL') . '/files/' . $namafile);

        file_put_contents($tmp_direktori, $response->body());

        $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

        return response()->download($tmp_direktori, $namafile, $headers);
    }

    private function getFile($namafile)
    {
        $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);

        $response = Http::get(env('API_URL_SDM07') . '/api/v1/files/' . $namafile);

        file_put_contents($tmp_direktori, $response->body());

        return response()->file($tmp_direktori, [
            'Content-Type' => 'application/pdf'
        ]);
    }


}

?>
