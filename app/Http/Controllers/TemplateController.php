<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class TemplateController extends Controller
{
    public function form()
    {
        $menu = DB::table('r_menu')->get();
        $data['menu'] = $menu;

        return view('template.form', $data);
    }

    public function notifikasi()
    {
        $menu = DB::table('r_menu')->get();
        $data['menu'] = $menu;

        return view('template.notifikasi', $data);
    }

    public function tabel()
    {
        $menu = DB::table('r_menu')->get();
        $data['menu'] = $menu;

        return view('template.tabel', $data);
    }

    public function cari()
    {
        $menu = DB::table('r_menu')->get();
        $data['menu'] = $menu;

        return view('template.cari', $data);
    }

}
