<?php

namespace App\Http\Controllers;

use App\Models\MenuModel;

class HomeController extends Controller
{
    public function index()
    {
        $menu = MenuModel::getMenu(session()->get('role'), 'beranda');

        $data['menu'] = $menu;

        return view('beranda', $data);
    }

    public function layananMandiri()
    {
        $menu = MenuModel::getMenu(session()->get('role'), 'beranda');

        $data['menu'] = $menu;

        $data['user'] = session()->get('user');

        return view('layanan_mandiri', $data);
    }
}
