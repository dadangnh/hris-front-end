<?php

namespace App\Http\Controllers\Ibel;

use App\Helpers\IbelHelper;
use App\Models\MenuModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PeriksaPermohonanIbelController extends Controller
{
    public function index(Request $request)
    {
        #list route
        $route = ['menunggu-pemeriksaan', 'ditolak', 'diteruskan'];

        #validate route
        if ($request->route == '' || in_array($request->route, $route)) {

            #get menu, tab
            $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/periksa');
            $data['tab'] = $request->route ? $request->route : 'menunggu-pemeriksaan';

            #set parameters
            $size = 10;
            $page = $request->page;
            $nama = $request->nama;
            $nip = $request->nip;
            $tiket = $request->tiket;

            #set api
            switch ($data['tab']) {
                case 'menunggu-pemeriksaan':
                    $response = IbelHelper::getWithToken('/izin_belajar/permohonan/monitoring/persetujuan?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&tiket=' . $tiket);
                    break;

                case 'ditolak':
                    $response = IbelHelper::getWithToken('/izin_belajar/permohonan/monitoring/tolak?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&tiket=' . $tiket);
                    break;

                case 'diteruskan':
                    $response = IbelHelper::getWithToken('/izin_belajar/permohonan/monitoring/diteruskan?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&tiket=' . $tiket);
                    break;
            }

            #data
            $data['ibel'] = $response;

            #jadwal
            if (!empty($response['data'])) {

                # loop
                foreach ($response['data'] as $r) {
                    $listJadwal = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/jadwal/' . $r->id);

                    # set jadwal array
                    foreach ($listJadwal as $d) {
                        if (!empty($d->jamMulai)) {
                            $data['ibel_jadwal'][$d->permohonanIzinBelajar][] = [
                                'jamMulai' => $d->jamMulai,
                                'hari' => $d->hari
                            ];
                        }
                    }
                }
            }

            #referensi
            $data['jenjang'] = IbelHelper::getWithToken('/jenjang_pendidikan');

            return view('ibel.pemeriksaan.pemeriksaan_index', $data);
        } else {
            return abort(404);
        }
    }

    public function periksaPermohonan($id)
    {
        #get menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/periksa');
        $data['action'] = true;

        # data ibel
        $response = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/' . $id);

        # data
        $data['ibel'] = $response;
        $data['ibel_jadwal'] = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/jadwal/' . $response['id']);

        return view('ibel.pemeriksaan.pemeriksaan_detail', $data);
    }

    public function lihatPermohonan($id)
    {
        #get menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/periksa');
        $data['action'] = false;

        # data
        $response = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/' . $id);

        #data
        $data['ibel'] = $response;

        #jadwal
        if (!empty($response)) {

            # get data
            $listJadwal = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/jadwal/' . $response['id']);

            # set jadwal array
            foreach ($listJadwal as $d) {
                $data['ibel_jadwal'][] = (object) [
                    'jamMulai' => $d->jamMulai,
                    'hari' => $d->hari
                ];
            }
        }

        return view('ibel.pemeriksaan.pemeriksaan_detail', $data);
    }

    public function setujuPermohonan($id)
    {
        # send api
        $isi = ['keterangan' => null];
        $body = IbelHelper::generateBody(session()->get('user'), $isi);

        $response = ibelHelper::postWithToken('/izin_belajar/permohonan/admin/' . $id . '/terima', $body);

        if ($response['status'] == 1) {
            return redirect('ibel/periksa/')->with('success', 'Permohonan berhasil di setujui');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function tolakPermohonan(Request $request)
    {
        #body
        $isi = ['keterangan' => $request->keterangan];
        $body = IbelHelper::generateBody(session()->get('user'), $isi);

        # send api
        $response = ibelHelper::postWithToken('/izin_belajar/permohonan/admin/' . $request->id . '/tolak', $body);

        return response()->json($response);
    }
}
