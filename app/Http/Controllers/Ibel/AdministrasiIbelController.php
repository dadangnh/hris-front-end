<?php

namespace App\Http\Controllers\Ibel;

use App\Helpers\AppHelper;
use App\Helpers\IbelHelper;
use App\Models\MenuModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;

class AdministrasiIbelController extends Controller
{
#index Dashboard
    public function index()
    {
        #get menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/administrasi');

        #get data ibel
        $idpegawai = session()->get('user')['pegawaiId'];
        $response = IbelHelper::getWithToken('/izinbelajar/monitoring/upk/draft?pegawaiId=' . $idpegawai);
        $data['ibel'] = [];
        #data
        if (!empty($response['data'])) {

            # ibel
            foreach ($response as $key => $value) {
                if ($key == 'data') {
                    foreach ($value as $v) {
                        # get ibel
                        $data['ibel'][$key][$v->id]['ibel'] = $v;

                        # get jadwal
                        $jadwal = IbelHelper::getWithToken('/izinbelajar/jadwal_kuliah/' . $v->id);

                        foreach ($jadwal as $j) {
                            $data['ibel'][$key][$v->id]['jadwal'][] = $j;
                        }

                        # get kep
                        $kep = IbelHelper::get('/izin_belajar/permohonan/permohonan/' . $v->id . '/kep_seleksi');
                        $data['ibel'][$key][$v->id]['kep_seleksi'] = $kep;

                        # get penolakan
                        $penolakan = IbelHelper::get('/izin_belajar/permohonan/permohonan/' . $v->id . '/penolakan');
                        $data['ibel'][$key][$v->id]['surat_penolakan'] = $penolakan;

                        # get surat izin
                        $surat_izin = IbelHelper::get('/izin_belajar/permohonan/permohonan/' . $v->id . '/surat_izin_belajar');
                        $data['ibel'][$key][$v->id]['surat_izin'] = $surat_izin;
                    }
                } else {
                    $data['ibel'][$key] = $value;
                }
            }
        }
        #tampil jadwal
        // if (!empty($response['data'])) {

        //     # loop
        //     foreach ($response['data'] as $r) {
        //         $listJadwal = IbelHelper::getWithToken('/izinbelajar/jadwal_kuliah/' . $r->id);
        //         # set jadwal array
        //         foreach ($listJadwal as $d) {
        //             if (!empty($d->jamMulai)) {
        //                 $data['ibel_jadwal'][$d->permohonanIzinBelajar][] = [
        //                     'jamMulai' => $d->jamMulai,
        //                     'hari' => $d->hari
        //                 ];
        //             }
        //         }
        //     }
        // }

        // dd($data);
        return view('ibel.administrasi.index', $data);
    }

    //lihat draft ibel
    public function lihatPermohonanDraft($id)
    {
        #get menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/administrasi');

        # data
        $response = IbelHelper::get('/izinbelajar/id/' . $id);
        $data['ibel'] = $response['0'];
        $data['ibel_jadwal'] = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/jadwal/' . $id);

        return view('ibel.administrasi.permohonan_ibel_detail', $data);
    }

    // Delete Ibel
    public function deleteIbel(Request $request)
    {
        $input = [
            'pegawaiInputId' => session()->get('user')['pegawaiId']
        ];
        $response = IbelHelper::deleteWithToken('/izinbelajar/' . $request->id, $input);
        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data Berhasil Dihapus!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    //tiket ibel
    public function getTiket($tiket)
    {
        #get data
        $response = IbelHelper::getWithToken('/probis/log/' . $tiket);

        #fetch data
        foreach ($response as $key => $item) {
            $data[] = [
                'createdDate' => AppHelper::instance()->indonesian_date($item->createdDate, 'j F Y', ''),
                'output' => $item->output,
                'nomorTiket' => $item->nomorTiket,
                'keterangan' => $item->keterangan,
            ];
        }

        return $data;
    }

    //kirim ibel
    public function kirimIbel(Request $request)
    {
        $id = $request->id;

        # cek kelengkapan lampiran
        $getdatadokumen = IbelHelper::getWithToken('/izinbelajar/id/' . $id);
        $response = $getdatadokumen['0'];
        if (
            $response->pathTidakUpkp == null || $response->pathKebutuhanProdi == null ||
            $response->pathSkorsing == null || $response->pathHukdis == null ||
            $response->pathJadwal == null || $response->pathBrosur == null ||
            $response->pathAkreditasi == null || $response->pathIjazah == null ||
            $response->pathJadwal == null || $response->pathBrosur == null ||
            $response->pathTranskrip == null || $response->pathSehat == null ||
            $response->pathCpns == null || $response->pathPns == null ||
            $response->pathPangkat == null || $response->pathPpkpns1 == null
            || $response->path_ppkpns2 == null
        ) {
            return response()->json(['status' => 0, 'message' => 'Ada Dokumen belum diupload']);
        }

        $isi = [
            'keterangan' => null,
        ];

        $body = IbelHelper::generateBody(session()->get('user'), $isi);

        $response = IbelHelper::postWithToken('/izinbelajar/upk/' . $id . '/kirimdraf', $body);

        return response()->json($response);
    }

#index Langkah 1
    public function indexIzinBelajarLangkahSatu()
    {
        #get menu
        $data['tab'] = '1';
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/administrasi');

        $data['ref_jenjang'] = IbelHelper::getWithToken('/jenjang_pendidikan');
        $data['ref_pt'] = IbelHelper::getWithToken('/perguruan_tinggi_ibel');
        $data['ref_prodi_akreditasi'] = IbelHelper::getWithToken('/ptprodiakreditasi');
        $data['ref_lokasi_kuliah'] = IbelHelper::getWithToken('/lokasi_perkuliahan');
        if (Session::has('PTIdNama')) {
            $data['ptidnama'] = Session::get('PTIdNama');
        }
        if (Session::has('prodiNama')) {
            $data['prodiNama'] = Session::get('prodiNama');
        }
        if (Session::has('lokasikampus')) {
            $data['lokasikampus'] = Session::get('lokasikampus');
        }
        return view('ibel.buat_ibel.buat_ibel_index', $data);
    }

    public function indexIzinBelajarStepSatuEdit($id)
    {
        #get menu
        $data['tab'] = 'edit1';
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/administrasi');

        $data['ref_jenjang'] = IbelHelper::getWithToken('/jenjang_pendidikan');
        $data['ref_pt'] = IbelHelper::getWithToken('/perguruan_tinggi_ibel');
        $data['ref_prodi_akreditasi'] = IbelHelper::getWithToken('/ptprodiakreditasi');
        $data['ref_lokasi_kuliah'] = IbelHelper::getWithToken('/lokasi_perkuliahan');
        $data['id'] = $id;
        $data['ibel'] = IbelHelper::getWithToken('/izinbelajar/id/' . $id);

        return view('ibel.buat_ibel.buat_ibel_index', $data);
    }

    //Get data Perguruan Tinggi
    public function getDataPT(Request $request)
    {
        $response = IbelHelper::getWithToken('/izinbelajar/perguruan_tinggi?idJenjang=' . $request->pt_jenjang);

        foreach ($response as $key => $item) {
            $data[] = [
                'id' => $item->id . '|' . $item->namaPerguruanTinggi,
                'text' => $item->namaPerguruanTinggi
            ];
        }

        return json_encode($data);
    }

    //Get data Prodi dan Akreditasi
    public function getDataProdi(Request $request)
    {
        #split data
        $prodiAkreditasi = $request->pt_id;
        $resultProdiAkreditasi = explode('|', $prodiAkreditasi);
        $prodiIdAkhir = $resultProdiAkreditasi[0];

        $response = IbelHelper::getWithToken('/izinbelajar/program_studi?idPt=' . $prodiIdAkhir . '&idJenjang=' . $request->pt_jenjang);

        foreach ($response as $key => $item) {
            $data[] = [
                'id' => $item->id . ',' . $item->programStudiIbelId->namaProgramStudi . ',' . $item->akreditasiId->akreditasi,
                'text' => $item->programStudiIbelId->namaProgramStudi
            ];
        }

        return json_encode($data);
    }

    //Get data Kampus
    public function getDataLokasiIbel(Request $request)
    {
        #split data
        $ambilIdpt = $request->pt_id;
        $resultIdPT = explode('|', $ambilIdpt);
        $getIdAkhir = $resultIdPT[0];

        $response = IbelHelper::getWithToken('/izinbelajar/lokasi?idPt=' . $getIdAkhir);

        foreach ($response as $key => $item) {
            $data[] = [
                'id' => $item->id . ',' . $item->namaKampus . ',' . $item->alamat,
                'text' => $item->namaKampus
            ];
        }

        return json_encode($data);
    }

#index Langkah 2
    public function indexIzinBelajarLangkahDua($id)
    {
        #get menu
        $data['tab'] = '2';
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/administrasi');

        $data['flagstep'] = IbelHelper::getWithToken('/izinbelajar/id/' . $id);
        $data['id'] = $id;
        // dd($data);
        return view('ibel.buat_ibel.buat_ibel_index', $data);
    }

    public function indexIzinBelajarLangkahDuaEdit($id)
    {
        #get menu
        $data['tab'] = 'edit2';
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/administrasi');
        $data['id'] = $id;

        $data['ibelJadwal'] = IbelHelper::getWithToken('/izinbelajar/jadwal_kuliah/' . $id);

        return view('ibel.buat_ibel.buat_ibel_index', $data);
    }

#index langkah 3
    public function indexIzinBelajarLangkahTiga($id)
    {
        #get menu
        $data['tab'] = '3';
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/administrasi');
        $data['id'] = $id;
        $data['file_lama'] = IbelHelper::getWithToken('/izinbelajar/id/' . $id);
        return view('ibel.buat_ibel.buat_ibel_index', $data);
    }

#index Izin Belajar Karena Mutasi
    public function cekMutasiStatusIbel($id)
    {
        #get menu
        $body = IbelHelper::generateBody(session()->get('user'));
        $response = IbelHelper::postWithToken('/izinbelajar/mutasi/3/' . $id, $body);

        if ($response['status'] == 1 && $response['data']->flagStep == 4) {
            $idIbelBaru = $response['data']->id;
            return redirect('ibel/administrasi/konfirmasi-tatap-muka/' . $idIbelBaru);
        } elseif ($response['status'] == 1 && $response['data']->flagStep == 7) {
            $idIbelBaru = $response['data']->id;
            return redirect('ibel/administrasi/langkah-empat-mutasi/' . $idIbelBaru);
        } else {
            return redirect()->back()
                ->with('error', $response['message']);
        }
    }

    public function indexKonfirmasiIbel($id)
    {
        #get menu
        $data['tab'] = 'konfirmasi';
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/administrasi');
        $data['idIbelBaru'] = $id;

        return view('ibel.buat_ibel.buat_ibel_index', $data);
    }

    public function indexIzinBelajarLangkahDuaMutasi($id)
    {
        #get menu
        $data['tab'] = 'mutasi-2';
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/administrasi');

        $data['ref_jenjang'] = IbelHelper::getWithToken('/jenjang_pendidikan');
        $data['ref_pt'] = IbelHelper::getWithToken('/perguruan_tinggi_ibel');
        $data['ref_prodi_akreditasi'] = IbelHelper::getWithToken('/ptprodiakreditasi');
        $data['ref_lokasi_kuliah'] = IbelHelper::getWithToken('/lokasi_perkuliahan');
        $data['ibel'] = IbelHelper::getWithToken('/izinbelajar/id/' . $id);

        return view('ibel.buat_ibel_mutasi.buat_ibel_index', $data);
    }

    public function indexIzinBelajarLangkahTigaMutasi($id)
    {
        #get menu
        $data['tab'] = 'mutasi-3';
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/administrasi');
        $data['id'] = $id;
        $data['ibelJadwal'] = IbelHelper::getWithToken('/izinbelajar/jadwal_kuliah/' . $id);

        return view('ibel.buat_ibel_mutasi.buat_ibel_index', $data);
    }

    public function indexIzinBelajarLangkahEmpatMutasi($id)
    {
        #get menu
        $data['tab'] = 'mutasi-4';
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/administrasi');
        $data['id'] = $id;
        $data['file_lama'] = IbelHelper::getWithToken('/izinbelajar/id/' . $id);
        return view('ibel.buat_ibel_mutasi.buat_ibel_index', $data);
    }
#----------------------------Post Langkah--------------------------------------
    // langkah pertama
    public function addIzinBelajarLangkahSatu(Request $request)
    {
        // pecah PT id
        $ptAsal = $request->perguruanTinggiId;
        $resultPTAsal = explode('|', $ptAsal);
        $PTIdAsal = $resultPTAsal[0];
        $PTIdNama = $resultPTAsal[1];

        // pecah Akreditasi id
        $akreditasiProdi = $request->prodiId;
        $resultAkreditasiAsal = explode(',', $akreditasiProdi);
        $akreditasiProdiIdAsal = $resultAkreditasiAsal[0];
        $prodiNama = $resultAkreditasiAsal[1];

        // pecah Lokasi id
        $getLokasi = $request->lokasiKuliah;
        $resultLokasiAsal = explode(',', $getLokasi);
        $lokasiIdAsal = $resultLokasiAsal[0];
        $lokasiKampus = $resultLokasiAsal[1];


        //  UPLOAD FILE Pencantuman Gelar
        if ($request->jenjangPendidikanIdGelar != null) {
            if ($request->docPencantumanGelar !== null) {
                $file_pencantuman_gelar = $request->docPencantumanGelar; #file

                $size = 2097152; #2MB
                $mime = ['application/pdf']; #pdf
                $file_file_pencantuman_gelar_hasil = IbelHelper::uploadWithToken($file_pencantuman_gelar, $size, $mime);

                if ($file_file_pencantuman_gelar_hasil['status'] == 0) {
                    return redirect()->back()->with('error', $file_file_pencantuman_gelar_hasil['message']);
                }
            } else {
                return redirect()->back()->with('error', 'Dokumen Pencantuman Gelar Belum Diupload!')->withInput()
                    ->with(['PTIdNama' => $PTIdNama])
                    ->with(['prodiNama' => $prodiNama])
                    ->with(['lokasikampus' => $lokasiKampus]);
            }
        }

        //  UPLOAD FILE Penetapan PJJ
        if ($request->pjjId == 1) {
            if ($request->penetepanProdiPJJ !== null) {
                $file_penetapan_pjj = $request->penetepanProdiPJJ; #file
                $size = 2097152; #2MB
                $mime = ['application/pdf']; #pdf
                $file_penetapan_pjj_hasil = IbelHelper::uploadWithToken($file_penetapan_pjj, $size, $mime);

                if ($file_penetapan_pjj_hasil['status'] == 0) {
                    return redirect()->back()->with('error', $file_penetapan_pjj_hasil['message'])->withInput()
                        ->with(['PTIdNama' => $PTIdNama])
                        ->with(['prodiNama' => $prodiNama])
                        ->with(['lokasikampus' => $lokasiKampus]);
                }
            } else {
                return redirect()->back()->with('error', 'Dokumen Penetapan PJJ Belum Diupload!')->withInput()
                    ->with(['PTIdNama' => $PTIdNama])
                    ->with(['prodiNama' => $prodiNama])
                    ->with(['lokasikampus' => $lokasiKampus]);
            }
        }

        //flag pencantuman gelar
        if (empty($request->jenjangPendidikanIdGelar)) {
            $flagjenjangPendidikan = 0;
        } else {
            $flagjenjangPendidikan = 1;
        }

        $step = 1;
        $input = [
            'jenjangPendidikan' => $request->jenjangPendidikanId,
            'flagPencantumanGelar' => $flagjenjangPendidikan,
            'pathPencantumanGelar' => $file_file_pencantuman_gelar_hasil['file'] ?? null,
            'periodePendaftaranAwal' => $request->periodePendmulai,
            'periodePendaftaranAkhir' => $request->periodePendselesai,
            'flagPjj' => $request->pjjId,
            'pathPenetapanPjj' => $file_penetapan_pjj_hasil['file'] ?? null,
            'perguruanTinggiIbel' => $PTIdAsal,
            'ptProdiAkreditasi' => $akreditasiProdiIdAsal,
            'flagStep' => $step,
            'rlokasiPerkuliahan' => $lokasiIdAsal,
            'jenjangPendidikanPgId' => $request->jenjangPendidikanIdGelar
        ];
        $body = IbelHelper::generateBody(session()->get('user'), $input);

        $response = IbelHelper::postWithToken('/izinbelajar/pegawaiset', $body);

        if ($response['status'] == 1) {
            $idtubel = $response['data']->id;
            return redirect('ibel/administrasi/isi-jadwal-perkuliahan/' . $idtubel)->with('success', 'Langkah Pertama berhasil!');
        } else {
            return redirect()->back()
                ->with('error', $response['message'])
                ->withInput()
                ->with(['PTIdNama' => $PTIdNama])
                ->with(['prodiNama' => $prodiNama])
                ->with(['lokasikampus' => $lokasiKampus]);
        }
    }

    public function editIzinBelajarLangkahSatu(Request $request, $id)
    {
        // pecah PT id
        $ptAsal = $request->perguruanTinggiId;
        $resultPTAsal = explode('|', $ptAsal);
        $PTIdAsal = $resultPTAsal[0];

        // pecah PT id
        $akreditasiProdi = $request->prodiId;
        $resultAkreditasiAsal = explode(',', $akreditasiProdi);
        $akreditasiProdiIdAsal = $resultAkreditasiAsal[0];

        // pecah Lokasi id
        $getLokasi = $request->lokasiKuliah;
        $resultLokasiAsal = explode(',', $getLokasi);
        $lokasiIdAsal = $resultLokasiAsal[0];

        //  UPLOAD FILE Pencantuman Gelar Edit
        if ($request->jenjangPendidikanIdGelar != null) {
            if ($request->docPencantumanGelar !== null) {

                IbelHelper::deleteFileWithToken('/files/' . $request->docPencantumanGelar_lama);

                $file_pencantuman_gelar = $request->docPencantumanGelar; #file
                $size = 2097152; #2MB
                $mime = ['application/pdf']; #pdf
                $file_pencantuman_gelar_hasil_edit = IbelHelper::uploadWithToken($file_pencantuman_gelar, $size, $mime);

                if ($file_pencantuman_gelar_hasil_edit['status'] == 0) {
                    return redirect()->back()->with('error', $file_pencantuman_gelar_hasil_edit['message']);
                }
            } elseif ($request->docPencantumanGelar_lama !== null) {
                $file_pencantuman_gelar_hasil_edit['file'] = $request->docPencantumanGelar_lama; #file
            } else {
                return redirect()->back()->with('error', 'Dokumen Pencantuman Gelar Belum Diupload!');
            }
        }

        //  UPLOAD FILE Penetapan PJJ Edit
        if ($request->pjjId == 1) {
            if ($request->penetepanProdiPJJ !== null) {
                IbelHelper::deleteFileWithToken('/files/' . $request->docPencantumanGelar_lama);

                $file_penetapan_pjj = $request->penetepanProdiPJJ; #file
                $token = session()->get('login_info')['_token'];
                $size = 2097152; #2MB
                $mime = ['application/pdf']; #pdf
                $file_penetapan_pjj_hasil_edit = IbelHelper::uploadWithToken($file_penetapan_pjj, $size, $mime);

                if ($file_penetapan_pjj_hasil_edit['status'] == 0) {
                    return redirect()->back()->with('error', $file_penetapan_pjj_hasil_edit['message']);
                }
            } elseif ($request->docpenetepanProdiPJJ_lama !== null) {
                $file_penetapan_pjj_hasil_edit['file'] = $request->docpenetepanProdiPJJ_lama; #file
            } else {
                return redirect()->back()->with('error', 'Dokumen Penetapan PJJ Belum Diupload!');
            }
        }

        //flag pencantuman gelar
        if (empty($request->jenjangPendidikanIdGelar)) {
            $flagjenjangPendidikan = 0;
        } else {
            $flagjenjangPendidikan = 1;
        }

        if ($request->stepFlag == 5) {
            $flagStep = 6;
        } else {
            $flagStep = $request->stepFlag;
        }

        $input = [
            'jenjangPendidikan' => $request->jenjangPendidikanId,
            'flagPencantumanGelar' => $flagjenjangPendidikan,
            'pathPencantumanGelar' => $file_pencantuman_gelar_hasil_edit['file'] ?? null,
            'periodePendaftaranAwal' => $request->periodePendmulai,
            'periodePendaftaranAkhir' => $request->periodePendselesai,
            'flagPjj' => $request->pjjId,
            'pathPenetapanPjj' => $file_penetapan_pjj_hasil_edit['file'] ?? null,
            'perguruanTinggiIbel' => $PTIdAsal,
            'ptProdiAkreditasi' => $akreditasiProdiIdAsal,
            'rlokasiPerkuliahan' => $lokasiIdAsal,
            'jenjangPendidikanPgId' => $request->jenjangPendidikanIdGelar,
            'flagStep' => $flagStep,
        ];

        $body = IbelHelper::generateBody(session()->get('user'), $input);
        $response = IbelHelper::patchWithToken('/izinbelajar/pegawai/' . $id, $body);

        if ($request->stepFlag == 1 || $request->stepFlag == 5) {
            if ($response['status'] == 1) {
                return redirect('ibel/administrasi/isi-jadwal-perkuliahan/' . $id)->with('success', 'Langkah Pertama berhasil Diisi!');
            } else {
                return redirect()->back()->with('error', $response['message']);
            }
        } else {
            if ($response['status'] == 1) {
                return redirect('ibel/administrasi/isi-jadwal-perkuliahan/edit/' . $id)->with('success', 'Langkah Pertama berhasil Diupdate!');
            } else {
                return redirect()->back()->with('error', $response['message']);
            }
        }
    }

    // langkah kedua
    public function nextBuatIzinBelajar(Request $request, $id)
    {

        $jadwal = $request->data;
        $idIbel = $id;
        $step = 2;
        #set array
        $data = [];


        #looping
        foreach ($jadwal as $key => $value) {
            $data[] = [
                'permohonanIzinBelajarId' => $idIbel,
                'hari' => $key,
                'jamMulai' => $value,
                'flagStep' => $step
            ];
        }
        $token = session()->get('login_info')['_token'];
        #send request
        $idpegawai = session()->get('user')['pegawaiId'];
        $response = IbelHelper::postWithToken('/izinbelajar/jamkuliah/?idPegawai=' . $idpegawai, $data);

        if ($response['status'] == 1) {
            return redirect('ibel/administrasi/upload-dokumen-persyaratan/' . $idIbel)->with('success', 'Data Langkah kedua berhasil ditambahkan!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function editIzinBelajarLangkahDua(Request $request, $id)
    {

        $jamEdit = $request->data;
        $step = 2;

        #set array
        $data = [];

        #looping
        foreach ($jamEdit as $key => $value) {
            $data[] = [
                'id' => $value['id'],
                'permohonanIzinBelajarId' => $id,
                'hari' => $key,
                'jamMulai' => $value['jam']
            ];
        }
        $idpegawai = session()->get('user')['pegawaiId'];

        // dd($data);
        $response = IbelHelper::patchWithToken('/izinbelajar/jamkuliah?idPegawai=' . $idpegawai, $data);


        if ($response['status'] == 1) {
            return redirect('ibel/administrasi/upload-dokumen-persyaratan/' . $id)->with('success', 'Data Langkah kedua berhasil diupdate!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    //langkah ketiga
    public function generateFileProdiSesuai($id)
    {
        # get file
        $response = IbelHelper::generateFileWithToken('/izinbelajar/upk/generate/sprodisesuai?id_laporan=' . $id);

        #tmp direktori
        $tmp_direktori = tempnam(sys_get_temp_dir(), $id);

        #custom nama file
        $namafile = 'IBEL_' . session('user')['nip9'] . '_SUPER_Prodi Sesuai Kebutuhan Organisasi.docx';

        #put content
        file_put_contents($tmp_direktori, $response['data']['file']);

        return response()->download($tmp_direktori, $namafile);
    }

    public function generateFileTidakMenuntut($id)
    {
        # get file
        $response = IbelHelper::generateFileWithToken('/izinbelajar/upk/generate/skebutuhan?id_laporan=' . $id);

        #tmp direktori
        $tmp_direktori = tempnam(sys_get_temp_dir(), $id);

        #custom nama file
        $namafile = 'IBEL_' . session('user')['nip9'] . '_SUPER_Tidak menuntut UPKP.docx';

        #put content
        file_put_contents($tmp_direktori, $response['data']['file']);

        return response()->download($tmp_direktori, $namafile);
    }

    public function uploadFileIzinbelajar(Request $request)
    {
        $file_tidak_menuntut_upkp = $request->file('file_tidak_menuntut_upkp'); #file
        $file_prodi_sesuai_kebutuhan = $request->file('file_prodi_sesuai_kebutuhan'); #file
        $file_tidak_skorsing = $request->file('file_tidak_skorsing'); #file
        $file_tidak_hukdis = $request->file('file_tidak_hukdis'); #file
        $file_jarak_lokasi = $request->file('file_jarak_lokasi'); #file
        $file_luar_jam_kerja = $request->file('file_luar_jam_kerja'); #file
        $file_brosur_pmb = $request->file('file_brosur_pmb'); #file
        $file_sertifikat_prodi = $request->file('file_sertifikat_prodi'); #file
        $ijazah_kedinasan = $request->file('ijazah_kedinasan'); #file
        $file_transkrip_nilai = $request->file('file_transkrip_nilai'); #file
        $file_sket_sehat = $request->file('file_sket_sehat'); #file
        $file_sk_cpns = $request->file('file_sk_cpns'); #file
        $file_sk_pns = $request->file('file_sk_pns'); #file
        $file_sk_pangkat = $request->file('file_sk_pangkat'); #file
        $file_ppkpns_satu = $request->file('file_ppkpns_satu'); #file
        $file_ppkpns_dua = $request->file('file_ppkpns_dua'); #file
        $size = 2097152; #2MB
        $mime = ['application/pdf']; #pdf

        if ($file_tidak_menuntut_upkp != null) {

            #upload data
            $response = IbelHelper::uploadWithToken($file_tidak_menuntut_upkp, $size, $mime);
            return response()->json($response);
        } elseif ($file_prodi_sesuai_kebutuhan != null) {

            #upload data
            $response = IbelHelper::uploadWithToken($file_prodi_sesuai_kebutuhan, $size, $mime);
            return response()->json($response);
        } elseif ($file_tidak_skorsing != null) {

            #upload data
            $response = IbelHelper::uploadWithToken($file_tidak_skorsing, $size, $mime);
            return response()->json($response);
        } elseif ($file_tidak_hukdis != null) {

            #upload data
            $response = IbelHelper::uploadWithToken($file_tidak_hukdis, $size, $mime);
            return response()->json($response);
        } elseif ($file_jarak_lokasi != null) {

            #upload data
            $response = IbelHelper::uploadWithToken($file_jarak_lokasi, $size, $mime);
            return response()->json($response);
        } elseif ($file_luar_jam_kerja != null) {

            #upload data
            $response = IbelHelper::uploadWithToken($file_luar_jam_kerja, $size, $mime);
            return response()->json($response);
        } elseif ($file_brosur_pmb != null) {

            #upload data
            $response = IbelHelper::uploadWithToken($file_brosur_pmb, $size, $mime);
            return response()->json($response);
        } elseif ($file_sertifikat_prodi != null) {

            #upload data
            $response = IbelHelper::uploadWithToken($file_sertifikat_prodi, $size, $mime);
            return response()->json($response);
        } elseif ($ijazah_kedinasan != null) {

            #upload data
            $response = IbelHelper::uploadWithToken($ijazah_kedinasan, $size, $mime);
            return response()->json($response);
        } elseif ($file_transkrip_nilai != null) {

            #upload data
            $response = IbelHelper::uploadWithToken($file_transkrip_nilai, $size, $mime);
            return response()->json($response);
        } elseif ($file_sket_sehat != null) {

            #upload data
            $response = IbelHelper::uploadWithToken($file_sket_sehat, $size, $mime);
            return response()->json($response);
        } elseif ($file_sk_cpns != null) {

            #upload data
            $response = IbelHelper::uploadWithToken($file_sk_cpns, $size, $mime);
            return response()->json($response);
        } elseif ($file_sk_pns != null) {

            #upload data
            $response = IbelHelper::uploadWithToken($file_sk_pns, $size, $mime);
            return response()->json($response);
        } elseif ($file_sk_pangkat != null) {

            #upload data
            $response = IbelHelper::uploadWithToken($file_sk_pangkat, $size, $mime);
            return response()->json($response);
        } elseif ($file_ppkpns_satu != null) {

            #upload data
            $response = IbelHelper::uploadWithToken($file_ppkpns_satu, $size, $mime);
            return response()->json($response);
        } elseif ($file_ppkpns_dua != null) {

            #upload data
            $response = IbelHelper::uploadWithToken($file_ppkpns_dua, $size, $mime);
            return response()->json($response);
        }
    }

    public function postAfterUploadFile(Request $request)
    {
        #cek jika ada file sebelumnya

        if (!empty($request->file_tidak_menuntut_upkp_lama)) {
            IbelHelper::deleteFileWithToken('/files/' . $request->file_tidak_menuntut_upkp_lama);
        } elseif (!empty($request->file_prodi_sesuai_kebutuhan_lama)) {
            IbelHelper::deleteFileWithToken('/files/' . $request->file_prodi_sesuai_kebutuhan_lama);
        } elseif (!empty($request->file_tidak_skorsing_lama)) {
            IbelHelper::deleteFileWithToken('/files/' . $request->file_tidak_skorsing_lama);
        } elseif (!empty($request->file_tidak_hukdis_lama)) {
            IbelHelper::deleteFileWithToken('/files/' . $request->file_tidak_hukdis_lama);
        } elseif (!empty($request->file_jarak_lokasi_lama)) {
            IbelHelper::deleteFileWithToken('/files/' . $request->file_jarak_lokasi_lama);
        } elseif (!empty($request->file_luar_jam_kerja_lama)) {
            IbelHelper::deleteFileWithToken('/files/' . $request->file_luar_jam_kerja_lama);
        } elseif (!empty($request->file_brosur_pmb_lama)) {
            IbelHelper::deleteFileWithToken('/files/' . $request->file_brosur_pmb_lama);
        } elseif (!empty($request->file_lama_sert_akreditasi)) {
            IbelHelper::deleteFileWithToken('/files/' . $request->file_lama_sert_akreditasi);
        } elseif (!empty($request->file_lama_ijazah_kedinasan)) {
            IbelHelper::deleteFileWithToken('/files/' . $request->file_lama_ijazah_kedinasan);
        } elseif (!empty($request->file_transkrip_nilai_lama)) {
            IbelHelper::deleteFileWithToken('/files/' . $request->file_transkrip_nilai_lama);
        } elseif (!empty($request->file_sket_sehat_lama)) {
            IbelHelper::deleteFileWithToken('/files/' . $request->file_sket_sehat_lama);
        } elseif (!empty($request->file_sk_cpns_lama)) {
            IbelHelper::deleteFileWithToken('/files/' . $request->file_sk_cpns_lama);
        } elseif (!empty($request->file_sk_pns_lama)) {
            IbelHelper::deleteFileWithToken('/files/' . $request->file_sk_pns_lama);
        } elseif (!empty($request->file_sk_pangkat_lama)) {
            IbelHelper::deleteFileWithToken('/files/' . $request->file_sk_pangkat_lama);
        } elseif (!empty($request->file_ppkpns_satu_lama)) {
            IbelHelper::deleteFileWithToken('/files/' . $request->file_ppkpns_satu_lama);
        } elseif (!empty($request->file_ppkpns_dua_lama)) {
            IbelHelper::deleteFileWithToken('/files/' . $request->file_ppkpns_dua_lama);
        }


        $step = 3;
        $idIbel = $request->idIbel;

        $file_tidak_menuntut_upkp_baru = ['pathTidakUpkp' => $request->file_tidak_menuntut_upkp_baru];
        $file_prodi_sesuai_kebutuhan_baru = ['pathKebutuhanProdi' => $request->file_prodi_sesuai_kebutuhan_baru];
        $file_tidak_skorsing = ['pathSkorsing' => $request->file_tidak_skorsing_baru];
        $file_tidak_hukdis = ['pathHukdis' => $request->file_tidak_hukdis_baru];
        $file_jarak_lokasi = ['pathJarak' => $request->file_jarak_lokasi_baru];
        $file_luar_jam_kerja = ['pathJadwal' => $request->file_luar_jam_kerja_baru];
        $file_brosur_pmb = ['pathBrosur' => $request->file_brosur_pmb_baru];
        $Akreditasi = ['pathAkreditasi' => $request->file_baru_sert_akreditasi];
        $ijazah = ['pathIjazah' => $request->file_baru_ijazah_kedinasan];
        $file_transkrip_nilai = ['pathTranskrip' => $request->file_transkrip_nilai_baru];
        $file_sket_sehat = ['pathSehat' => $request->file_sket_sehat_baru];
        $file_sk_cpns = ['pathCpns' => $request->file_sk_cpns_baru];
        $file_sk_pns = ['pathPns' => $request->file_sk_pns_baru];
        $file_sk_pangkat = ['pathPangkat' => $request->file_sk_pangkat_baru];
        $file_ppkpns_satu = ['pathPpkpns1' => $request->file_ppkpns_satu_baru];
        $file_ppkpns_dua = ['path_ppkpns2' => $request->file_ppkpns_dua_baru];

        if (!empty($request->file_tidak_menuntut_upkp_baru)) {
            $response = IbelHelper::patchWithToken('/izinbelajar/dokumen/' . $idIbel, $file_tidak_menuntut_upkp_baru);
        } elseif (!empty($request->file_prodi_sesuai_kebutuhan_baru)) {
            $response = IbelHelper::patchWithToken('/izinbelajar/dokumen/' . $idIbel, $file_prodi_sesuai_kebutuhan_baru);
        } elseif (!empty($request->file_tidak_skorsing_baru)) {
            $response = IbelHelper::patchWithToken('/izinbelajar/dokumen/' . $idIbel, $file_tidak_skorsing);
        } elseif (!empty($request->file_tidak_hukdis_baru)) {
            $response = IbelHelper::patchWithToken('/izinbelajar/dokumen/' . $idIbel, $file_tidak_hukdis);
        } elseif (!empty($request->file_jarak_lokasi_baru)) {
            $response = IbelHelper::patchWithToken('/izinbelajar/dokumen/' . $idIbel, $file_jarak_lokasi);
        } elseif (!empty($request->file_luar_jam_kerja_baru)) {
            $response = IbelHelper::patchWithToken('/izinbelajar/dokumen/' . $idIbel, $file_luar_jam_kerja);
        } elseif (!empty($request->file_brosur_pmb_baru)) {
            $response = IbelHelper::patchWithToken('/izinbelajar/dokumen/' . $idIbel, $file_brosur_pmb);
        } elseif (!empty($request->file_baru_sert_akreditasi)) {
            $response = IbelHelper::patchWithToken('/izinbelajar/dokumen/' . $idIbel, $Akreditasi);
        } elseif (!empty($request->file_baru_ijazah_kedinasan)) {
            $response = IbelHelper::patchWithToken('/izinbelajar/dokumen/' . $idIbel, $ijazah);
        } elseif (!empty($request->file_transkrip_nilai_baru)) {
            $response = IbelHelper::patchWithToken('/izinbelajar/dokumen/' . $idIbel, $file_transkrip_nilai);
        } elseif (!empty($request->file_sket_sehat_baru)) {
            $response = IbelHelper::patchWithToken('/izinbelajar/dokumen/' . $idIbel, $file_sket_sehat);
        } elseif (!empty($request->file_sk_cpns_baru)) {
            $response = IbelHelper::patchWithToken('/izinbelajar/dokumen/' . $idIbel, $file_sk_cpns);
        } elseif (!empty($request->file_sk_pns_baru)) {
            $response = IbelHelper::patchWithToken('/izinbelajar/dokumen/' . $idIbel, $file_sk_pns);
        } elseif (!empty($request->file_sk_pangkat_baru)) {
            $response = IbelHelper::patchWithToken('/izinbelajar/dokumen/' . $idIbel, $file_sk_pangkat);
        } elseif (!empty($request->file_ppkpns_satu_baru)) {
            $response = IbelHelper::patchWithToken('/izinbelajar/dokumen/' . $idIbel, $file_ppkpns_satu);
        } elseif (!empty($request->file_ppkpns_dua_baru)) {
            $response = IbelHelper::patchWithToken('/izinbelajar/dokumen/' . $idIbel, $file_ppkpns_dua);
        }

        return response()->json($response);
    }

    public function finishIzinBelajarTiga(Request $request, $id)
    {

        $step = 3;
        $input = [
            'flagStep' => $step
        ];

        $response = IbelHelper::patchWithToken('/izinbelajar/dokumen/' . $id, $input);

        if ($response['status'] == 1) {
            return redirect('ibel/administrasi')->with('success', 'Langkah Ketiga Selesai!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    //konfirmasi
    public function inputKonfirmasiIbel(Request $request, $id)
    {
        if (!empty($request->pjjId)) {
            $flag = $request->pjjId;
        } else {
            $flag = 0;
        }

        $flagTatapMuka = $flag;
        $pathTatapMuka = $request->dokKeterangan;
        $body = IbelHelper::generateBody(session()->get('user'));

        $response = IbelHelper::postWithToken('/izinbelajar/mutasi/4/' . $id .
            '?flagPjj=' . $flagTatapMuka .
            '&pathPenetapanPjj=' . $pathTatapMuka, $body);

        if ($response['status'] == 1 && $response['data']->flagStep == 5) {
            $idIbelBaru = $response['data']->id;
            return redirect('ibel/administrasi/isi-data-utama/edit/' . $idIbelBaru);
        } elseif ($response['status'] == 1 && $response['data']->flagStep == 7) {
            $idIbelBaru = $response['data']->id;
            return redirect('ibel/administrasi/langkah-empat-mutasi/' . $idIbelBaru);
        } else {
            return redirect()->back()
                ->with('error', $response['message']);
        }
    }
    // public function uploadUpkp(Request $request)
    // {
    //     // dd($request->file("file_tidak_menuntut_upkp"), $request->all());

    //     if ($request->file("file_tidak_menuntut_upkp") !== null) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }
}
