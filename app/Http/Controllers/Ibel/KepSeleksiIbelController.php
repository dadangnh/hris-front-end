<?php

namespace App\Http\Controllers\Ibel;

use App\Helpers\CommonVariable;
use App\Helpers\IbelHelper;
use App\Models\MenuModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;

class KepSeleksiIbelController extends Controller
{
    public function index(Request $request)
    {
        #get menu, tab
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/kep-seleksi');
        $data['tab'] = $request->route ? $request->route : 'perekaman';

        #set parameters
        $size = 10;
        $page = $request->page;
        $nama = $request->nama;
        $nip = $request->nip;
        $tiket = $request->tiket;

        #set api
        $response = IbelHelper::getWithToken('/izin_belajar/permohonan/monitoring/perekaman_kep_hasil_seleksi?page=' . $page . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&tiket=' . $tiket);

        #data
        $data['ibel'] = $response;

        # get jadwal & kep seleksi
        if (!empty($response['data'])) {

            # jadwal
            foreach ($response['data'] as $r) {
                $listJadwal = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/jadwal/' . $r->id);

                # set jadwal array
                foreach ($listJadwal as $d) {
                    if (!empty($d->jamMulai)) {
                        $data['ibel_jadwal'][$d->permohonanIzinBelajar][] = [
                            'jamMulai' => $d->jamMulai,
                            'hari' => $d->hari
                        ];
                    }
                }

                # kep seleksi
                $listKep[] = IbelHelper::get('/izin_belajar/permohonan/permohonan/' . $r->id . '/kep_seleksi');

                # set kep array
                foreach ($listKep as $k) {
                    if (!empty($k)) {
                        $data['ibel_kep'][$k->permohonanIzinBelajar->id] = [
                            'nomorKep' => $k->nomorKep,
                            'tglKep' => $k->tglKep,
                            'flagDiterima' => $k->flagDiterima,
                            'alasanDitolak' => $k->alasanDitolak,
                            'pathKeputusanSeleksi' => $k->pathKeputusanSeleksi,
                            'jangkaWaktuTahun' => $k->jangkaWaktuTahun,
                            'jangkaWaktuBulan' => $k->jangkaWaktuBulan
                        ];
                    }
                }
            }
        }

        #referensi
        $data['jenjang'] = IbelHelper::getWithToken('/jenjang_pendidikan');

        return view('ibel.kep_seleksi.kep_seleksi_index', $data);
    }

    public function rekamPermohonan(Request $request)
    {
        # file
        $file_kep = $request->file('file_kep'); #file
        $size = 2097152; #2MB
        $mime = ['application/pdf']; #pdf

        # upload file
        $response_file = IbelHelper::uploadWithToken($file_kep, $size, $mime);

        # set body
        $body = IbelHelper::generateBody(session()->get('user'), [
            'nomorKep' => $request->nomor_kep,
            'tglKep' => $request->tgl_kep,
            'pathKep' => $response_file['file']
        ]);

        # send to api
        $response = IbelHelper::postWithToken('/izin_belajar/permohonan/admin/' . $request->id_permohonan . '/kep_hasil_seleksi', $body);

        # return
        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasil disimpan');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function prosesPermohonan($id)
    {

        # validasi
        $validate = IbelHelper::get('/izin_belajar/permohonan/permohonan/' . $id . '/kep_seleksi');

        if (empty($validate->nomorKep) || empty($validate->tglKep) || empty($validate->pathKeputusanSeleksi)) {
            return redirect()->back()->with('error', 'Keputusan Hasil Seleksi belum lengkap!');
        }

        # send to api
        $body = IbelHelper::generateBody(session()->get('user'), [
            'keterangan' => null
        ]);

        # send to api
        $response = IbelHelper::postWithToken('/izin_belajar/permohonan/admin/' . $id . '/proses_kep_hasil_seleksi', $body);

        # return
        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasil disimpan');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function generateKepKetuaTim($id)
    {
        # set token
        $token = session()->get('login_info')['_token'];

        $tmp_direktori = tempnam(sys_get_temp_dir(), $id);

        $body = IbelHelper::generateBody(session()->get('user'), ['keterangan' => null]);

        # get data
        $response = Http::withHeaders([
            'Token-IAM' => $token,
        ])
            ->post(env('API_URL_SDM0365') . '/izin_belajar/permohonan/upk/generate/kep_ketua_tim?id_permohonan=' . $id, $body);
        // dd($response);
        #put content
        file_put_contents($tmp_direktori, $response);

        #get file name
        $content_dispotition = explode(';', $response->headers()['Content-Disposition'][0]);
        $file_name_raw = explode('=', $content_dispotition[1]);
        $file_name = str_replace('"', "", $file_name_raw[1]);

        return response()->download($tmp_direktori, $file_name);
    }

    public function detailPermohonan($id)
    {
        #get menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/kep-seleksi');

        # data
        $data['ibel'] = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/' . $id);
        $data['ibel_jadwal'] = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/jadwal/' . $id);

        return view('ibel.kep_seleksi.kep_seleksi_detail', $data);
    }
}
