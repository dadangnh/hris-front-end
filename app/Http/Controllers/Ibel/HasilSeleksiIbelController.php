<?php

namespace App\Http\Controllers\Ibel;

use App\Helpers\IbelHelper;
use App\Models\MenuModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class HasilSeleksiIbelController extends Controller
{
    public function index(Request $request)
    {
        #list route
        $route = ['perekaman', 'menunggu-persetujuan'];

        #validate route
        if ($request->route == '' || in_array($request->route, $route)) {

            #get menu, tab
            $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/hasil-seleksi');
            $data['tab'] = $request->route ? $request->route : 'perekaman';

            #set parameters
            $size = 10;
            $page = $request->page;
            $nama = $request->nama;
            $nip = $request->nip;
            $tiket = $request->tiket;
            $idPegawaitim = session()->get('user')['pegawaiId'];

            #set api
            switch ($data['tab']) {
                case 'perekaman':
                    $response = IbelHelper::getWithToken('/izin_belajar/permohonan/monitoring/diteruskan?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&tiket=' . $tiket);
                    break;

                case 'menunggu-persetujuan':
                    $response = IbelHelper::getWithToken('/izinbelajar/upk/tim/' . $idPegawaitim . '/monitoringIzinBelajarByDetaiTim?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&tiket=' . $tiket);
                    break;
            }

            #data
            $data['ibel'] = $response;
            # get jadwal & kep seleksi
            if (!empty($response['data'])) {

                # jadwal
                foreach ($response['data'] as $r) {
                    $listJadwal = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/jadwal/' . $r->id);

                    # set jadwal array
                    foreach ($listJadwal as $d) {
                        if (!empty($d->jamMulai)) {
                            $data['ibel_jadwal'][$d->permohonanIzinBelajar][] = [
                                'jamMulai' => $d->jamMulai,
                                'hari' => $d->hari
                            ];
                        }
                    }

                    # kep seleksi
                    $listKep[] = IbelHelper::get('/izin_belajar/permohonan/permohonan/' . $r->id . '/kep_seleksi');

                    # set kep array
                    foreach ($listKep as $k) {
                        if (!empty($k)) {
                            $data['ibel_kep'][$k->permohonanIzinBelajar->id] = [
                                'nomorKep' => $k->nomorKep,
                                'tglKep' => $k->tglKep,
                                'flagDiterima' => $k->flagDiterima,
                                'alasanDitolak' => $k->alasanDitolak,
                                'pathKeputusanSeleksi' => $k->pathKeputusanSeleksi,
                                'jangkaWaktuTahun' => $k->jangkaWaktuTahun,
                                'jangkaWaktuBulan' => $k->jangkaWaktuBulan
                            ];
                        }
                    }
                }

            }

            #referensi
            $data['jenjang'] = IbelHelper::getWithToken('/jenjang_pendidikan');
            // $data['lokasi'] = IbelHelper::getWithToken('/lokasi_pendidikan');
            return view('ibel.hasil_seleksi.hasil_seleksi_index', $data);
        } else {
            return abort(404);
        }
    }

    public function lihatPermohonan($id)
    {
        #get menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/hasil-seleksi');
        $data['action'] = false;

        # data
        $response = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/' . $id);

        #data
        $data['ibel'] = $response;
        $data['action'] = false;

        #jadwal
        if (!empty($response)) {

            # get data
            $listJadwal = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/jadwal/' . $response['id']);

            # set jadwal array
            foreach ($listJadwal as $d) {
                $data['ibel_jadwal'][] = (object) [
                    'jamMulai' => $d->jamMulai,
                    'hari' => $d->hari
                ];
            }
        }

        return view('ibel.hasil_seleksi.hasil_seleksi_detail', $data);
    }

    public function hasilSeleksi($id)
    {
        #get menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/hasil-seleksi');
        $data['action'] = true;

        # data
        $response = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/' . $id);

        #data
        $data['ibel'] = $response;

        #jadwal
        if (!empty($response)) {

            # get data
            $listJadwal = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/jadwal/' . $response['id']);

            # set jadwal array
            foreach ($listJadwal as $d) {
                $data['ibel_jadwal'][] = (object) [
                    'jamMulai' => $d->jamMulai,
                    'hari' => $d->hari
                ];
            }
        }

        return view('ibel.hasil_seleksi.hasil_seleksi_detail', $data);
    }

    public function rekamHasilSeleksi(Request $request)
    {
        // dd($request->all());

        $tim = explode("|", $request->tim_seleksi)[0];

        if ($request->keputusan == 1) { #diterima
            $isi = [
                'flagDiterima' => $request->keputusan,
                'alasanDitolak' => null,
                'jangkaWaktuTahun' => $request->tahun,
                'jangkaWaktuBulan' => $request->bulan,
                'timSeleksiId' => $tim
            ];
        } elseif ($request->keputusan == 0) { #ditolak
            $isi = [
                'flagDiterima' => $request->keputusan,
                'alasanDitolak' => $request->alasan_penolakan,
                'jangkaWaktuTahun' => null,
                'jangkaWaktuBulan' => null,
                'timSeleksiId' => $tim
            ];
        }

        # send api
        $body = IbelHelper::generateBody(session()->get('user'), $isi);
        $response = ibelHelper::postWithToken('/izin_belajar/permohonan/upk/' . $request->id_permohonan . '/hasil_seleksi', $body);

        if ($response['status'] == 1) {
            return redirect('ibel/hasil-seleksi/')->with('success', 'Hasil seleksi berhasil direkam');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }
}
