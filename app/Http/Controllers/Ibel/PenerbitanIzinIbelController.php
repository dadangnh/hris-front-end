<?php

namespace App\Http\Controllers\Ibel;

use App\Helpers\CommonVariable;
use App\Helpers\IbelHelper;
use App\Http\Controllers\Controller;
use App\Models\MenuModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PenerbitanIzinIbelController extends Controller
{
    public function index(Request $request)
    {
        #list route
        $route = ['penerbitan', 'telah-diterbitkan'];

        #validate route
        if ($request->route == '' || in_array($request->route, $route)) {

            #get menu, tab
            $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/penerbitan-izin');
            $data['tab'] = $request->route ? $request->route : 'penerbitan';

            #set parameters
            $size = 10;
            $page = $request->page;
            $nama = $request->nama;
            $nip = $request->nip;
            $tiket = $request->tiket;

            #set api
            switch ($data['tab']) {
                case 'penerbitan':
                    $response = IbelHelper::getWithToken('/izin_belajar/permohonan/monitoring/penerbitan_ibel?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&tiket=' . $tiket);
                    break;

                case 'telah-diterbitkan':
                    $response = IbelHelper::getWithToken('/izin_belajar/permohonan/monitoring/tindak_lanjut?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&tiket=' . $tiket);
                    break;
            }

            #data
            $data['ibel'] = $response;

            #jadwal
            if (!empty($response['data'])) {

                # jadwal
                foreach ($response['data'] as $r) {
                    $listJadwal = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/jadwal/' . $r->id);

                    # set jadwal array
                    foreach ($listJadwal as $d) {
                        if (!empty($d->jamMulai)) {
                            $data['ibel_jadwal'][$d->permohonanIzinBelajar][] = [
                                'jamMulai' => $d->jamMulai,
                                'hari' => $d->hari
                            ];
                        }
                    }

                    # kep seleksi
                    $listKep[] = IbelHelper::get('/izin_belajar/permohonan/permohonan/' . $r->id . '/kep_seleksi');

                    # set kep array
                    foreach ($listKep as $k) {
                        if (!empty($k)) {
                            $data['ibel_kep'][$k->permohonanIzinBelajar->id] = [
                                'nomorKep' => $k->nomorKep,
                                'tglKep' => $k->tglKep,
                                'flagDiterima' => $k->flagDiterima,
                                'alasanDitolak' => $k->alasanDitolak,
                                'pathKeputusanSeleksi' => $k->pathKeputusanSeleksi,
                                'jangkaWaktuTahun' => $k->jangkaWaktuTahun,
                                'jangkaWaktuBulan' => $k->jangkaWaktuBulan
                            ];
                        }
                    }
                }
            }

            #referensi
            $data['jenjang'] = IbelHelper::getWithToken('/jenjang_pendidikan');

            return view('ibel.penerbitan_izin.penerbitan_izin_index', $data);
        } else {
            return abort(404);
        }
    }

    public function dsPermohonan(Request $request)
    {

        // get data current
        $id = explode(",", $request->id_permohonan_ibel);
        $nik = session()->get('user')['nik'];

        # send api
        $isi = [
            'nik' => $nik,
            'passphrase' => $request->passphrase
        ];

        $body = IbelHelper::generateBody(session()->get('user'), $isi);

        foreach ($id as $i) {
            $response = ibelHelper::postWithToken('/izin_belajar/permohonan/admin/' . $i . '/si_belajar/tetapkan', $body);
        }

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Permohonan berhasil di DS');
        } else {
            return redirect()->back()->with('success', 'Permohonan berhasil di DS');
        }
    }

    public function lihatPermohonan($id)
    {
        #get menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/penerbitan-izin');

        # data
        $data['ibel'] = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/' . $id);
        $data['ibel_jadwal'] = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/jadwal/' . $id);

        return view('ibel.penerbitan_izin.penerbitan_izin_detail', $data);
    }

    // BELUM DIPAKAI (BALIKAN ENDPOINT MASIH PAKAI MS WORD)
    public function previewPermohonan($id)
    {
        # set token
        $token = session()->get('login_info')['_token'];

        $tmp_direktori = tempnam(sys_get_temp_dir(), $id);

        $isi = [
            'keterangan' => null
        ];

        $body = IbelHelper::generateBody(session()->get('user'), $isi);

        # get data
        $response = Http::withHeaders([
            'Token-IAM' => $token,
        ])
            ->post(env('API_URL_SDM0365') . '/izin_belajar/permohonan/upk/generate/kep_ketua_tim?id_permohonan=' . $id, $body);

        #put content
        file_put_contents($tmp_direktori, $response);

        return response()->file($tmp_direktori, [
            'Content-Type' => 'application/pdf'
        ]);
    }

    public function generateFileKepKetuaTim($id)
    {
        # set token
        $token = session()->get('login_info')['_token'];

        $tmp_direktori = tempnam(sys_get_temp_dir(), $id);

        $body = IbelHelper::generateBody(session()->get('user'), ['keterangan' => null]);

        # get data
        $response = Http::withHeaders([
            'Token-IAM' => $token,
        ])
            ->post(env('API_URL_SDM0365') . '/izin_belajar/permohonan/upk/generate/surat_ibel?id_permohonan=' . $id, $body);

        #put content
        file_put_contents($tmp_direktori, $response);

        #get file name
        $content_dispotition = explode(';', $response->headers()['Content-Disposition'][0]);
        $file_name_raw = explode('=', $content_dispotition[1]);
        $file_name = str_replace('"', "", $file_name_raw[1]);

        return response()->download($tmp_direktori, $file_name);
    }
}
