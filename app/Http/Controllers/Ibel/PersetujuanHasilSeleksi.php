<?php

namespace App\Http\Controllers\Ibel;

use App\Helpers\IbelHelper;
use App\Models\MenuModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PersetujuanHasilSeleksi extends Controller
{
    public function index(Request $request)
    {
        #list route
        $route = ['menunggu-persetujuan', 'disetujui'];

        #validate route
        if ($request->route == '' || in_array($request->route, $route)) {

            #get menu, tab
            $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/persetujuan-hasil-seleksi');
            $data['tab'] = $request->route ? $request->route : 'menunggu-persetujuan';
            #set parameters
            $size = 10;
            $page = $request->page;
            $nama = $request->nama;
            $nip = $request->nip;
            $tiket = $request->tiket;

            // sementara
            $idPegawaitim = session()->get('user')['pegawaiId'];
            #set api
            switch ($data['tab']) {
                case 'menunggu-persetujuan':
                    $response = IbelHelper::getWithToken('/izinbelajar/upk/tim/' . $idPegawaitim . '/monitoringIzinBelajarByDetaiTim?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&tiket=' . $tiket);
                    // dd($response);
                    break;

                case 'disetujui':
                    $response = IbelHelper::getWithToken('/izin_belajar/permohonan/monitoring/perekaman_kep_hasil_seleksi?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&tiket=' . $tiket);
                    // dd($response);
                    break;
            }

            #data
            $data['ibel'] = $response;

            #jadwal
            if (!empty($response['data'])) {

                # loop
                foreach ($response['data'] as $r) {
                    $listJadwal = IbelHelper::getWithToken('/izinbelajar/jadwal_kuliah/' . $r->id);

                    # set jadwal array
                    foreach ($listJadwal as $d) {
                        if (!empty($d->jamMulai)) {
                            $data['ibel_jadwal'][$d->permohonanIzinBelajar][] = [
                                'jamMulai' => $d->jamMulai,
                                'hari' => $d->hari
                            ];
                        }
                    }

                    # kep seleksi
                    $listKep[] = IbelHelper::get('/izin_belajar/permohonan/permohonan/' . $r->id . '/kep_seleksi');

                    # set kep array
                    foreach ($listKep as $k) {
                        if (!empty($k)) {
                            $data['ibel_kep'][$k->permohonanIzinBelajar->id] = [
                                'nomorKep' => $k->nomorKep,
                                'tglKep' => $k->tglKep,
                                'flagDiterima' => $k->flagDiterima,
                                'alasanDitolak' => $k->alasanDitolak,
                                'pathKeputusanSeleksi' => $k->pathKeputusanSeleksi,
                                'jangkaWaktuTahun' => $k->jangkaWaktuTahun,
                                'jangkaWaktuBulan' => $k->jangkaWaktuBulan
                            ];
                        }
                    }
                }
            }

            #referensi
            $data['jenjang'] = IbelHelper::getWithToken('/jenjang_pendidikan');

            return view('ibel.persetujuan_tim.persetujuan_index', $data);
        } else {
            return abort(404);
        }
    }

    public function periksaPersetujuan($id)
    {
        #get menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/persetujuan-hasil-seleksi');
        $data['action'] = true;

        # data ibel
        $getdata = IbelHelper::getWithToken('/izinbelajar/upk/tim/monitoringStatus/' . $id);
        $response = $getdata['permohonanIzinBelajar'];

        # data
        $data['ibel'] = $response;
        $data['kep_persetujuan'] = $getdata;
        $data['ibel_jadwal'] = IbelHelper::getWithToken('/izinbelajar/jadwal_kuliah/' . $response->id);

        return view('ibel.persetujuan_tim.persetujuan_detail', $data);
    }

    public function timSetuju(Request $request)
    {
        if ($request->keputusan == 1) { #diterima
            $isi = [
                'flagDiterima' => $request->keputusan,
                'keterangan' => null,
                'jangkaWaktuTahun' => $request->tahun,
                'jangkaWaktuBulan' => $request->bulan,
            ];
        } elseif ($request->keputusan == 0) { #ditolak
            $isi = [
                'flagDiterima' => $request->keputusan,
                'keterangan' => $request->alasan_penolakan,
                'jangkaWaktuTahun' => null,
                'jangkaWaktuBulan' => null,
            ];
        }

        # send api
        $body = IbelHelper::generateBody(session()->get('user'), $isi);
        $response = ibelHelper::postWithToken('/izinbelajar/upk/tim/' . $request->id_permohonan . '/persetujuanAnggota', $body);

        if ($response['status'] == 1) {
            return redirect('ibel/persetujuan-hasil-seleksi')->with('success', 'Permohonan berhasil di setujui');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function lihatPermohonan($id)
    {
        #get menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/persetujuan-hasil-seleksi');
        $data['action'] = false;

        # data
        $data['ibel'] = IbelHelper::get('/izin_belajar/permohonan/admin/' . $id);
        $data['ibel_jadwal'] = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/jadwal/' . $id);
        return view('ibel.persetujuan_tim.persetujuan_detail', $data);
    }
}
