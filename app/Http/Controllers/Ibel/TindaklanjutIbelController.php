<?php

namespace App\Http\Controllers\Ibel;

use App\Helpers\CommonVariable;
use App\Helpers\IbelHelper;
use App\Models\MenuModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;

class TindaklanjutIbelController extends Controller
{
    public function index(Request $request)
    {
        #get menu, tab
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/tindak-lanjut');

        #set parameters
        $size = 10;
        $page = $request->page;
        $nama = $request->nama;
        $nip = $request->nip;
        $tiket = $request->tiket;

        #set api
        $response = IbelHelper::getWithToken('/izin_belajar/permohonan/monitoring/tindak_lanjut?page=' . $page . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&tiket=' . $tiket);

        $data['ibel'] = [];

        #data
        if (!empty($response['data'])) {

            # ibel
            foreach ($response as $key => $value) {
                if ($key == 'data') {
                    foreach ($value as $v) {
                        # get ibel
                        $data['ibel'][$key][$v->id]['ibel'] = $v;

                        # get jadwal
                        $jadwal = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/jadwal/' . $v->id);

                        foreach ($jadwal as $j) {
                            $data['ibel'][$key][$v->id]['jadwal'][] = $j;
                        }

                        # get kep
                        $kep = IbelHelper::get('/izin_belajar/permohonan/permohonan/' . $v->id . '/kep_seleksi');
                        $data['ibel'][$key][$v->id]['kep_seleksi'] = $kep;

                        # get penolakan
                        $penolakan = IbelHelper::get('/izin_belajar/permohonan/permohonan/' . $v->id . '/penolakan');
                        $data['ibel'][$key][$v->id]['surat_penolakan'] = $penolakan;

                        # get surat izin
                        $surat_izin = IbelHelper::get('/izin_belajar/permohonan/permohonan/' . $v->id . '/surat_izin_belajar');
                        $data['ibel'][$key][$v->id]['surat_izin'] = $surat_izin;
                    }
                } else {
                    $data['ibel'][$key] = $value;
                }
            }
        }

        #referensi
        $data['jenjang'] = IbelHelper::getWithToken('/jenjang_pendidikan');

        // dd($data);

        return view('ibel.tindak_lanjut.tindak_lanjut_index', $data);
    }

    public function rekamTindakLanjut(Request $request)
    {
        # file
        $file_surat_penolakan = $request->file('file_surat_penolakan'); #file
        $file_nd = $request->file('file_nd'); #file
        $size = 2097152; #2MB
        $mime = ['application/pdf']; #pdf

        # upload & validasi file
        $response_file_surat_penolakan = IbelHelper::uploadWithToken($file_surat_penolakan, $size, $mime);

        // if ($response_file_surat_penolakan['status'] == 0) {
        //     return redirect()->back()->with('error', $response_file_surat_penolakan['message']);
        // }

        $response_file_nd = IbelHelper::uploadWithToken($file_nd, $size, $mime);

        // if ($response_file_nd['status'] == 0) {
        //     return redirect()->back()->with('error', $response_file_nd['message']);
        // }

        # set body
        $body = IbelHelper::generateBody(session()->get('user'), [
            'nomorSuratPenolakan' => $request->nomor_surat_penolakan,
            'tglSuratPenolakan' => $request->tgl_surat_penolakan,
            'pathSuratPenolakan' => $response_file_surat_penolakan['file'],
            'nomorSuratPemberitahuan' => $request->nomor_nd,
            'tglSuratPemberitahuan' => $request->tgl_nd,
            'pathSuratPemberitahuan' => $response_file_nd['file']
        ]);

        # send to api
        $response = IbelHelper::postWithToken('/izin_belajar/permohonan/upk/' . $request->id_permohonan . '/tindak_lanjut', $body);

        # return
        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasil disimpan');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function generateKepKetuaTim($id)
    {
        # set token
        $token = session()->get('login_info')['_token'];

        $tmp_direktori = tempnam(sys_get_temp_dir(), $id);

        $body = IbelHelper::generateBody(session()->get('user'), ['keterangan' => null]);

        # get data
        $response = Http::withHeaders([
            'Token-IAM' => $token,
        ])
            ->post(env('API_URL_SDM0365') . '/izin_belajar/permohonan/upk/generate/kep_ketua_tim?id_permohonan=' . $id, $body);

        #put content
        file_put_contents($tmp_direktori, $response);

        #get file name
        $content_dispotition = explode(';', $response->headers()['Content-Disposition'][0]);
        $file_name_raw = explode('=', $content_dispotition[1]);
        $file_name = str_replace('"', "", $file_name_raw[1]);

        return response()->download($tmp_direktori, $file_name);
    }

    public function generateKepSeleksi($id)
    {
        # set token
        $token = session()->get('login_info')['_token'];

        $tmp_direktori = tempnam(sys_get_temp_dir(), $id);

        $body = IbelHelper::generateBody(session()->get('user'), ['keterangan' => null]);

        # get data
        $response = Http::withHeaders([
            'Token-IAM' => $token,
        ])
            ->post(env('API_URL_SDM0365') . '/izin_belajar/permohonan/upk/generate/kep_ketua_tim?id_permohonan=' . $id, $body);

        #put content
        file_put_contents($tmp_direktori, $response);

        #get file name
        $content_dispotition = explode(';', $response->headers()['Content-Disposition'][0]);
        $file_name_raw = explode('=', $content_dispotition[1]);
        $file_name = str_replace('"', "", $file_name_raw[1]);

        return response()->download($tmp_direktori, $file_name);
    }

    public function generateTemplateSuratPenolakan($id)
    {
        # set token
        $token = session()->get('login_info')['_token'];

        $tmp_direktori = tempnam(sys_get_temp_dir(), $id);

        # get data
        $response = Http::withHeaders([
            'Token-IAM' => $token,
        ])
            ->get(env('API_URL_SDM0365') . '/izin_belajar/permohonan/upk/generate/surat_penolakan?id_permohonan=' . $id);

        #put content
        file_put_contents($tmp_direktori, $response);

        #get file name
        $content_dispotition = explode(';', $response->headers()['Content-Disposition'][0]);
        $file_name_raw = explode('=', $content_dispotition[1]);
        $file_name = str_replace('"', "", $file_name_raw[1]);

        return response()->download($tmp_direktori, $file_name);
    }

    public function generateSuratIzin($id)
    {
        # set token
        $token = session()->get('login_info')['_token'];

        $tmp_direktori = tempnam(sys_get_temp_dir(), $id);

        $body = IbelHelper::generateBody(session()->get('user'), ['keterangan' => null]);

        # get data
        $response = Http::withHeaders([
            'Token-IAM' => $token,
        ])
            ->post(env('API_URL_SDM0365') . '/izin_belajar/permohonan/upk/generate/surat_ibel?id_permohonan=' . $id, $body);

        #put content
        file_put_contents($tmp_direktori, $response);

        #get file name
        $content_dispotition = explode(';', $response->headers()['Content-Disposition'][0]);
        $file_name_raw = explode('=', $content_dispotition[1]);
        $file_name = str_replace('"', "", $file_name_raw[1]);

        return response()->download($tmp_direktori, $file_name);
    }

    public function generateTemplateNdPemberitahuanPenolakan($id)
    {
        # set token
        $token = session()->get('login_info')['_token'];

        $tmp_direktori = tempnam(sys_get_temp_dir(), $id);

        # get data
        $response = Http::withHeaders([
            'Token-IAM' => $token,
        ])
            ->get(env('API_URL_SDM0365') . '/izin_belajar/permohonan/upk/generate/surat_pemberitahuan_penolakan?id_permohonan=' . $id);

        #put content
        file_put_contents($tmp_direktori, $response);

        #get file name
        $content_dispotition = explode(';', $response->headers()['Content-Disposition'][0]);
        $file_name_raw = explode('=', $content_dispotition[1]);
        $file_name = str_replace('"', "", $file_name_raw[1]);

        return response()->download($tmp_direktori, $file_name);
    }

    public function generateNdPenyampaian($id)
    {
        # set token
        $token = session()->get('login_info')['_token'];

        $tmp_direktori = tempnam(sys_get_temp_dir(), $id);

        # get data
        $response = Http::withHeaders([
            'Token-IAM' => $token,
        ])
            ->get(env('API_URL_SDM0365') . '/izin_belajar/permohonan/upk/generate/nd_penyampaian?id_permohonan=' . $id);

        #put content
        file_put_contents($tmp_direktori, $response);

        #get file name
        $content_dispotition = explode(';', $response->headers()['Content-Disposition'][0]);
        $file_name_raw = explode('=', $content_dispotition[1]);
        $file_name = str_replace('"', "", $file_name_raw[1]);

        return response()->download($tmp_direktori, $file_name);
    }

    public function detailPermohonan($id)
    {
        #get menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/tindak-lanjut');

        # data
        $data['ibel'] = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/' . $id);
        $data['ibel_jadwal'] = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/jadwal/' . $id);

        return view('ibel.tindak_lanjut.tindak_lanjut_detail', $data);
    }

    public function tetapkanIzinBelajar($id)
    {
        # validasi
        $validate = IbelHelper::get('/izin_belajar/permohonan/permohonan/' . $id . '/penolakan');

        if (empty($validate->nomorSuratPenolakan) || empty($validate->tglSuratPenolakan) || empty($validate->pathSuratPenolakan) || empty($validate->nomorSuratPemberitahuan) || empty($validate->tglSuratPemberitahuan) || empty($validate->pathSuratPemberitahuan)) {
            return redirect()->back()->with('error', 'Surat Penolakan belum lengkap!');
        }

        # send to api
        $body = IbelHelper::generateBody(session()->get('user'), [
            'keterangan' => null
        ]);

        # send to api
        $response = IbelHelper::postWithToken('/izin_belajar/permohonan/upk/' . $id . '/tindak_lanjut/proses', $body);

        # return
        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasil disimpan');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }
}
