<?php

namespace App\Http\Controllers\Ibel;

use App\Helpers\AppHelper;
use App\Helpers\CommonVariable;
use App\Helpers\IbelHelper;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class IbelController extends Controller
{
    public function getTiket($tiket)
    {
        #get data
        $response = IbelHelper::getWithToken('/probis/log/' . $tiket);

        #fetch data
        foreach ($response as $key => $item) {
            $data[] = [
                'createdDate' => AppHelper::instance()->indonesian_date($item->createdDate, 'j F Y', ''),
                'output' => $item->output,
                'nomorTiket' => $item->nomorTiket,
                'keterangan' => $item->keterangan,
            ];
        }

        return $data;
    }

    public function getFile($file)
    {
        $response = IbelHelper::getFile($file);

        return $response;
    }

    public function kepSeleksiPermohonanIbel($id)
    {
        $response = IbelHelper::get('/izin_belajar/permohonan/permohonan/' . $id . '/kep_seleksi');

        return response()->json($response);
    }

    public function getTImSeleksi(Request $request)
    {
        $idKantor = session()->get('user')['kantorId'];

        //get lama
        $response = IbelHelper::get('/izin_belajar/upk/timseleksi/search?NamaTim=' . $request->search . '&page=1&size=5');

        //get by kantor
        // $response = IbelHelper::get('/izin_belajar/upk/timseleksi/' . $idKantor . '?page=1&size=5');

        foreach ($response->data as $key => $item) {
            $data[] = [
                'id' => $item->id . '|' . $item->namaTim,
                'text' => $item->namaTim
            ];
        }

        return json_encode($data);
    }

    public function tindaklanjutPermohonanIbel($id)
    {
        $response = IbelHelper::get('/izin_belajar/permohonan/permohonan/' . $id . '/kep_seleksi');

        return response()->json($response);
    }
}
