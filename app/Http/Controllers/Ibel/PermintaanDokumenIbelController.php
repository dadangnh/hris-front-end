<?php

namespace App\Http\Controllers\Ibel;

use App\Helpers\CommonVariable;
use App\Helpers\IbelHelper;
use App\Http\Controllers\Controller;
use App\Models\MenuModel;
use Illuminate\Http\Request;

class PermintaanDokumenIbelController extends Controller
{
    public function index(Request $request)
    {
        #get menu, tab
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/permintaan-dokumen');

        #set parameters
        $size = 10;
        $page = $request->page;
        $nama = $request->nama;
        $nip = $request->nip;
        $tiket = $request->tiket;

        #set api
        $response = IbelHelper::getWithToken('/izin_belajar/permohonan/monitoring/persetujuan?page=' . $page . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&tiket=' . $tiket);

        #data
        $data['ibel'] = $response;

        #jadwal
        if (!empty($response['data'])) {

            # loop
            foreach ($response['data'] as $r) {
                $listJadwal = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/jadwal/' . $r->id);

                # set jadwal array
                foreach ($listJadwal as $d) {
                    if (!empty($d->jamMulai)) {
                        $data['ibel_jadwal'][$d->permohonanIzinBelajar][] = [
                            'jamMulai' => $d->jamMulai,
                            'hari' => $d->hari
                        ];
                    }
                }
            }
        }

        #referensi
        $data['jenjang'] = IbelHelper::getWithToken('/jenjang_pendidikan');

        return view('ibel.permintaan_dokumen.permintaan_dokumen_index', $data);
    }

    public function lihatPermohonan($id)
    {
        #get menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/permintaan-dokumen');

        # data ibel
        $data['ibel'] = IbelHelper::getWithToken('/izin_belajar/permohonan/admin/' . $id);

        # data

        return view('ibel.permintaan_dokumen.permintaan_dokumen_detail', $data);
    }

    public function generateFileSkorsing($id)
    {
        # get file
        $response = IbelHelper::generateFile('/izin_belajar/permohonan/upk/generate/skorsing?id_laporan=' . $id);

        #tmp direktori
        $tmp_direktori = tempnam(sys_get_temp_dir(), $id);

        #custom nama file
        $namafile = 'IBEL_' . session('user')['nip9'] . '_SKET_Tidak Sedang Skorsing.docx';

        #put content
        file_put_contents($tmp_direktori, $response['data']['file']);

        // return response()->download($tmp_direktori, $namafile);
        return response()->download($tmp_direktori, $response['data']['filename']);
    }

    public function generateFileHukdis($id)
    {
        # get file
        $response = IbelHelper::generateFile('/izin_belajar/permohonan/upk/generate/hukdis?id_laporan=' . $id);

        #tmp direktori
        $tmp_direktori = tempnam(sys_get_temp_dir(), $id);

        #custom nama file
        $namafile = 'IBEL_' . session('user')['nip9'] . '_SKET_Tidak Sedang Hukdis.docx';

        #put content
        file_put_contents($tmp_direktori, $response['data']['file']);

        // return response()->download($tmp_direktori, $namafile);
        return response()->download($tmp_direktori, $response['data']['filename']);
    }

    public function generateFileJarak($id)
    {
        # get file
        $response = IbelHelper::generateFile('/izin_belajar/permohonan/upk/generate/lokasi_jarak?id_laporan=' . $id);

        #tmp direktori
        $tmp_direktori = tempnam(sys_get_temp_dir(), $id);

        #custom nama file
        $namafile = 'IBEL_' . session('user')['nip9'] . '_SKET_Jarak.docx';

        #put content
        file_put_contents($tmp_direktori, $response['data']['file']);

        // return response()->download($tmp_direktori, $namafile);
        return response()->download($tmp_direktori, $response['data']['filename']);
    }

    public function generateFileWaktu($id)
    {
        # get file
        $response = IbelHelper::generateFile('/izin_belajar/permohonan/upk/generate/luar_jam_kerja?id_laporan=' . $id);

        #tmp direktori
        $tmp_direktori = tempnam(sys_get_temp_dir(), $id);

        #custom nama file
        $namafile = 'IBEL_' . session('user')['nip9'] . '_SKET_Luar Jam Kerja.docx';

        #put content
        file_put_contents($tmp_direktori, $response['data']['file']);

        // return response()->download($tmp_direktori, $namafile);
        return response()->download($tmp_direktori, $response['data']['filename']);
    }
}
