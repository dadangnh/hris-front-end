<?php

namespace App\Http\Controllers\Ibel;

use App\Helpers\IbelHelper;
use App\Helpers\LoginHelper;
use App\Models\MenuModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;


class ReferensiController extends Controller
{
#-----------------------------------------REFERENSI PERGURUAN TINGGI----------------------------------------

    // GET With Helper
    public function indexReferensiPT1()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/referensi/pt');
        $data['route'] = 'ibel/referensi/pt';

        #tab
        $data['tab'] = 'referensi_pt';

        #param
        $CariNama = isset($_GET['cari']) ? $_GET['cari'] : null;
        $size = 5;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;

        # tampilkan semua data dari hasil cari
        $response = IbelHelper::getWithToken('/perguruan_tinggi_ibel/search?page=' . $page
            . '&size=' . $size
            . '&nama=' . $CariNama);
        $data['pt'] = $response['data'];

        #pagination
        $data['totalItems'] = $response['totalItems'];
        $data['totalPages'] = $response['totalPages'];
        $data['currentPage'] = $response['currentPage'];
        $data['numberOfElements'] = $response['numberOfElements'];
        $data['size'] = $response['size'];

        return view('ibel.referensi.referensi_ibel_index', $data);
    }

    // Post With Helper
    public function addReferensiPT(Request $request)
    {
        $input = [
            'namaPerguruanTinggi' => $request->nama_ref,
            'alamat' => $request->alamat_ref,
            'createdBy' => session()->get('user')['pegawaiId']
        ];

        $response = IbelHelper::postWithToken('/perguruan_tinggi_ibel', $input);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Perguruan Tinggi berhasil ditambahkan!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    // Patch With Helper
    public function updateReferensiPT(Request $request)
    {
        $input = [
            'namaPerguruanTinggi' => $request->nama_ref_edit,
            'alamat' => $request->alamat_ref_edit,
            'updatedBy' => session()->get('user')['pegawaiId']
        ];

        $response = IbelHelper::patchWithToken('/perguruan_tinggi_ibel/' . $request->id_ref_edit, $input);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Nama Perguruan Tinggi berhasil diubah!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    // Delete With Helper
    public function deleteReferensiPT(Request $request)
    {
        $input = [
            'pegawaiInputId' => session()->get('user')['pegawaiId']
        ];

        $response = IbelHelper::deleteWithToken('/perguruan_tinggi_ibel/' . $request->id, $input);

        return response()->json($response);
    }

#-----------------------------------------REFERENSI PROGRAM STUDI----------------------------------------------------------
    // GET With Helper
    public function indexReferensiProdi()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/referensi/prodi');
        $data['route'] = 'ibel/referensi/prodi';

        #tab
        $data['tab'] = 'referensi_prodi';

        #param
        $CariNama = isset($_GET['cari']) ? $_GET['cari'] : null;
        $size = 5;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;

        # tampilkan semua data dari hasil cari
        $response = IbelHelper::getWithToken('/program_studi/search?page=' . $page
            . '&size=' . $size
            . '&nama=' . $CariNama);

        $data['prodi'] = $response['data'];
        $data['ref_prodi'] = IbelHelper::getWithToken('/jenjang_pendidikan');

        #pagination
        $data['totalItems'] = $response['totalItems'];
        $data['totalPages'] = $response['totalPages'];
        $data['currentPage'] = $response['currentPage'];
        $data['numberOfElements'] = $response['numberOfElements'];
        $data['size'] = $response['size'];

        return view('ibel.referensi.referensi_ibel_index', $data);
    }

    // Post With Helper
    public function addReferensiProdi(Request $request)
    {
        $input = [
            'namaProgramStudi' => $request->nama_ref,
            'jenjangPendidikanId' => $request->jenjangPendidikanId,
            'createdBy' => session()->get('user')['pegawaiId']
        ];

        $response = IbelHelper::postWithToken('/program_studi/', $input);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Program Studi berhasil ditambahkan!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    // Patch With Helper
    public function updateReferensiProdi(Request $request)
    {
        $input = [
            'namaProgramStudi' => $request->nama_ref_edit,
            'jenjangPendidikanId' => $request->jenjangPendidikanId,
            'upadateBy' => session()->get('user')['pegawaiId']
        ];

        $response = IbelHelper::patchWithToken('/program_studi/' . $request->id_ref_edit, $input);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Program Studi berhasil diubah!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    // Delete With Helper
    public function deleteReferensiProdi(Request $request)
    {
        $input = [
            'pegawaiInputId' => session()->get('user')['pegawaiId']
        ];

        $response = IbelHelper::deleteWithToken('/program_studi/' . $request->id, $input);

        return response()->json($response);
    }

#-----------------------------------------REFERENSI LOKASI PENDIDIKAN----------------------------------------------------------
    // GET With Helper
    public function indexReferensiLokasi()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/referensi/lokasi');
        $data['route'] = 'ibel/referensi/lokasi';

        #tab
        $data['tab'] = 'referensi_lokasi';

        #param
        $CariNama = isset($_GET['cari']) ? $_GET['cari'] : null;
        $size = 5;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;

        # tampilkan semua data dari hasil cari
        $response = IbelHelper::getWithToken('/lokasi_perkuliahan/search?page=' . $page
            . '&size=' . $size
            . '&nama=' . $CariNama);

        $data['lokasi'] = $response['data'];
        $data['ref_lokasi'] = IbelHelper::getWithToken('/perguruan_tinggi_ibel');

        #pagination
        $data['totalItems'] = $response['totalItems'];
        $data['totalPages'] = $response['totalPages'];
        $data['currentPage'] = $response['currentPage'];
        $data['numberOfElements'] = $response['numberOfElements'];
        $data['size'] = $response['size'];

        return view('ibel.referensi.referensi_ibel_index', $data);
    }

    // Post With Helper
    public function addReferensiLokasi(Request $request)
    {
        $input = [
            'namaKampus' => $request->nama_kampus,
            'perguruan_tinggi_id' => $request->ptId,
            'alamat' => $request->lokasi,
            'longAt' => $request->longi,
            'latAt' => $request->lati,
            'createdBy' => session()->get('user')['pegawaiId']
        ];

        $response = IbelHelper::postWithToken('/lokasi_perkuliahan', $input);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Lokasi Pendidikan berhasil ditambahkan!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    // Patch With Helper
    public function updateReferensiLokasi(Request $request)
    {
        $input = [
            'namaKampus' => $request->nama_kampus,
            'perguruan_tinggi_id' => $request->idPt,
            'alamat' => $request->lokasi,
            'longAt' => $request->long,
            'latAt' => $request->lat,
            'upadateBy' => session()->get('user')['pegawaiId']
        ];

        $response = IbelHelper::patchWithToken('/lokasi_perkuliahan/perguruan_tinggi_ibel/' . $request->id_ref_edit, $input);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Lokasi Pendidikan berhasil diubah!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    // Delete With Helper
    public function deleteReferensiLokasi(Request $request)
    {
        $input = [
            'pegawaiInputId' => session()->get('user')['pegawaiId']
        ];

        $response = IbelHelper::deleteWithToken('/lokasi_perkuliahan/perguruan_tinggi_ibel/' . $request->id, $input);

        return response()->json($response);
    }
#-----------------------------------------REFERENSI OTORISASI IZIN BELAJAR----------------------------------------------------------

    // GET With Helper
    public function indexReferensiOtorisasi()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/referensi/otorisasi');
        $data['route'] = 'ibel/referensi/otorisasi';

        #tab
        $data['tab'] = 'referensi_otorisasi';

        #param
        $CariNama = isset($_GET['cari']) ? $_GET['cari'] : null;
        $size = 5;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;

        # tampilkan semua data dari hasil cari
        $response = IbelHelper::getWithToken('/otoriasi_izin_belajar/search?page=' . $page
            . '&size=' . $size
            . '&nama=' . $CariNama);

        $data['otorisasi'] = $response['data'];
        $data['ref_otorisasi'] = IbelHelper::getWithToken('/tipe_otoritas');
        $idkantor = session()->get('user')['kantorId'];
        $data['namaKantor'] = session()->get('user')['kantor'];

        if ($idkantor == '8a1690ca-1d75-4f6e-9ae8-4a09e2db573c') {
            $data['mupang'] = true;
        } else {
            $data['mupang'] = false;
            $data['namaKantorNonMupang'] = session()->get('user')['kantor'];
            $data['idKantorNonMupang'] = $idkantor;
        }

        #pagination
        $data['totalItems'] = $response['totalItems'];
        $data['totalPages'] = $response['totalPages'];
        $data['currentPage'] = $response['currentPage'];
        $data['numberOfElements'] = $response['numberOfElements'];
        $data['size'] = $response['size'];

        return view('ibel.referensi.referensi_ibel_index', $data);
    }

    //Get data pegawai
    public function getDataPegawaiReferensi(Request $request)
    {
        #ambil id kantor
        if (empty($request->kantor_id)) {
            $kantorIdAsal = $request->kantor_id_non_mupang;
        } else {
            $kantorAsal = $request->kantor_id;
            $resultKantorAsal = explode('|', $kantorAsal);
            $kantorIdAsal = $resultKantorAsal[0];
        }

        $response = LoginHelper::iamgetv2(env('HRIS_URL') . '/master_pegawais?namaPegawai=' . $request->search
            . '&kantorId=' . $kantorIdAsal);

        foreach ($response['data']['hydra:member'] as $key => $item) {
            if (!empty($item['unitId'])) {
                $data[] = [
                    'id' => $item['pegawaiId'] . '|' . $item['namaPegawai'] .
                        '|' . $item['nip18'] . '|' . $item['jabatanId'] .
                        '|' . $item['jabatan'] . '|' . $item['unitId'] .
                        '|' . $item['unit'],
                    'text' => $item['namaPegawai']
                ];
            } else {
                $data[] = [
                    'id' => $item['pegawaiId'] . '|' . $item['namaPegawai'] .
                        '|' . $item['nip18'] . '|' . $item['jabatanId'] .
                        '|' . $item['jabatan'] . '|' . null .
                        '|' . $item['unit'],
                    'text' => $item['namaPegawai']
                ];
            }
        }

        return json_encode($data);
    }

    //Get data Kantor
    public function getDataKantorReferensi(Request $request)
    {
        $token = IbelHelper::getNewSession(session()->get('login_info')['username']);

        $response = LoginHelper::iamget(env('API_URL_IAM') . 'kantors/active/' . $request->search, $token);

        foreach ($response['data']->kantors as $key => $item) {
            $data[] = [
                'id' => $item->id . '|' . $item->nama,
                'text' => $item->nama
            ];
        }

        return json_encode($data);
    }

    // Post With Helper
    public function addReferensiOtorisasi(Request $request)
    {
        if (empty($request->nama_kantor_otorisasi2)) {
            #split data kantor
            $kantorAsal = $request->nama_kantor_otorisasi;
            $resultKantorAsal = explode('|', $kantorAsal);

            #get id, kantor
            $kantorIdAsal = $resultKantorAsal[0];
            $namaKantorAsal = $resultKantorAsal[1];
        } else {
            $namaKantorAsal = $request->nama_kantor_otorisasi2;
            $kantorIdAsal = $request->idKantorNonMupang;
        }

        #split data pegawai
        $pegawaiAsal = $request->nama_pegawai;
        $resultpegawaiAsal = explode('|', $pegawaiAsal);

        #get id, pegawai
        $pegawaiIdAsal = $resultpegawaiAsal[0];
        $namapegawaiAsal = $resultpegawaiAsal[1];
        $nippegawaiAsal = $resultpegawaiAsal[2];
        $jabatan_id = $resultpegawaiAsal[3];
        $nama_jabatan = $resultpegawaiAsal[4];
        $unitId = $resultpegawaiAsal[5];
        $nama_unit = $resultpegawaiAsal[6];


        $input = [
            'tipeOtorisasiId' => $request->tipeOtorisasiId,
            'pegawaiId' => $pegawaiIdAsal,
            'namaPegawai' => $namapegawaiAsal,
            'nip18' => $nippegawaiAsal,
            'kantorId' => $kantorIdAsal,
            'namaKantor' => $namaKantorAsal,
            'jabatanId' => $jabatan_id,
            'namaJabatan' => $nama_jabatan,
            'unitOrgId' => $unitId,
            'namaUnitOrg' => $nama_unit,
            'kantorPerekamId' => session()->get('user')['kantorId'],
            'createdBy' => session()->get('user')['pegawaiId']
        ];

        $response = IbelHelper::postWithToken('/otoriasi_izin_belajar', $input);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Otorisasi berhasil ditambahkan!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    // Patch With Helper
    public function updateReferensiOtorisasi(Request $request)
    {
        $input = [
            'tipeOtorisasiId' => $request->OtorsisasiId,
            'namaPegawai' => $request->nama_pegawai_oto_edit,
            'pegawaiId' => $request->nama_pegawai_oto_edit_id,
            'nip18' => $request->nip_oto_edit,
            'kantorId' => $request->nama_kantor_oto_edit_id,
            'namaKantor' => $request->nama_kantor_oto_edit,
            'updatedBy' => session()->get('user')['pegawaiId']
        ];

        $response = IbelHelper::patchWithToken('/otoriasi_izin_belajar/' . $request->id_ref_edit, $input);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Otorisasi berhasil diubah!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    // Delete With Helper
    public function deleteReferensiOtorisasi(Request $request)
    {
        $input = [
            'pegawaiInputId' => session()->get('user')['pegawaiId']
        ];
        $response = IbelHelper::deleteWithToken('/otoriasi_izin_belajar/' . $request->id, $input);

        return response()->json($response);
    }

#-----------------------------------------REFERENSI Akreditasi Prodi----------------------------------------------------------
    // GET With Helper
    public function indexReferensiAkreditasi()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/referensi/prodi-akreditasi');
        $data['route'] = 'ibel/referensi/prodi-akreditasi';

        #tab
        $data['tab'] = 'referensi_akreditasi';

        #param
        $CariNama = isset($_GET['cari']) ? $_GET['cari'] : null;
        $size = 5;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;

        # tampilkan semua data dari hasil cari
        $response = IbelHelper::getWithToken('/ptprodiakreditasi/page/?page=' . $page
            . '&size=' . $size
            . '&nama=' . $CariNama);

        $data['akreditasi'] = $response['data'];
        $data['ref_akreditasi_pt'] = IbelHelper::getWithToken('/perguruan_tinggi_ibel');
        $data['ref_akreditasi_prodi'] = IbelHelper::getWithToken('/program_studi/');
        $data['ref_akreditasi_jenjang'] = IbelHelper::getWithToken('/jenjang_pendidikan');
        $data['ref_akreditasi_id'] = IbelHelper::getWithToken('/akreditasi');

        #pagination
        $data['totalItems'] = $response['totalItems'];
        $data['totalPages'] = $response['totalPages'];
        $data['currentPage'] = $response['currentPage'];
        $data['numberOfElements'] = $response['numberOfElements'];
        $data['size'] = $response['size'];

        return view('ibel.referensi.referensi_ibel_index', $data);
    }

    // Post With Helper
    public function addReferensiAkreditasi(Request $request)
    {
        #ambil id jenjang
        $jenjangPendidikan = $request->prodi_akre;
        $resultJenjang = explode('|', $jenjangPendidikan);
        $jenjangIdResult = $resultJenjang[0];

        $input = [
            'perguruanTinggiIbelId' => $request->pr_akre,
            'programStudiIbelId' => $jenjangIdResult,
            'akreditasiId' => $request->akreditasi,
            'tglBerakhir' => $request->tgl_berakhir,
            'createdBy' => session()->get('user')['pegawaiId']
        ];

        $response = IbelHelper::postWithToken('/ptprodiakreditasi', $input);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasil ditambahkan!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    // Patch With Helper
    public function updateReferensiAkreditasi(Request $request)
    {
        $input = [
            'perguruanTinggiIbelId' => $request->id_pt_akr,
            'programStudiIbelId' => $request->id_prodi_akr,
            'akreditasiId' => $request->id_akreditasi_edit,
            'tglBerakhir' => $request->tgl_berakhir_akr,
            'updatedBy' => session()->get('user')['pegawaiId']
        ];

        $response = IbelHelper::patchWithToken('/ptprodiakreditasi/' . $request->id_ref_edit, $input);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Akreditasi berhasil diubah!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    // Delete With Helper
    public function deleteReferensiAkreditasi(Request $request)
    {
        $input = [
            'pegawaiInputId' => session()->get('user')['pegawaiId']
        ];

        $response = IbelHelper::deleteWithToken('/ptprodiakreditasi/' . $request->id, $input);

        return response()->json($response);
    }


#-----------------------------------------REFERENSI TIM Otorisasi-------------------------------------------------
    public function indexReferensiTimOtorisasi()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/referensi/tim-otorisasi');
        $data['route'] = 'ibel/referensi/tim_otorisasi';

        #tab
        $data['tab'] = 'tim_seleksi';

        #param
        $CariNama = isset($_GET['cari']) ? $_GET['cari'] : null;
        $size = 5;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;

        $idKantor = session()->get('user')['kantorId'];

        # tampilkan semua data
        $response = IbelHelper::getWithToken('/izin_belajar/upk/timseleksi/' . $idKantor . '?page=' . $page
            . '&size=' . $size);

        $data['tim_seleksi'] = $response['data'];

        #pagination
        $data['totalItems'] = $response['totalItems'];
        $data['totalPages'] = $response['totalPages'];
        $data['currentPage'] = $response['currentPage'];
        $data['numberOfElements'] = $response['numberOfElements'];
        $data['size'] = $response['size'];

        return view('ibel.referensi.referensi_ibel_index', $data);
    }

    //index detail anggota tim
    public function indexAnggotaTim($id)
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'ibel/referensi/tim-otorisasi');
        $data['route'] = 'ibel/referensi/tim_otorisasi';

        #tab
        $data['tab'] = 'tim_seleksi';
        $data['action'] = true;
        $idKantor = session()->get('user')['kantorId'];

        #anggota tim
        $response['tim'] = IbelHelper::getWithToken('/izin_belajar/upk/anggotatimseleksi/' . $id);
        $data['anggota'] = $response['tim'];

        #referensi anggota by kantor
        $response['anggota'] = IbelHelper::getWithToken('/otoriasi_izin_belajar/search?kantorId=' . $idKantor);
        $data['dataAnggotaBykantor'] = $response['anggota']['data'];

        #nama tim
        $response['namaTim'] = IbelHelper::getWithToken('/izin_belajar/upk/timseleksi/search?id=' . $id);
        $data['tampilkanNamaTim'] = $response['namaTim']['data'];

        $data['namaKantor'] = session()->get('user')['kantor'];
        $data['idTim'] = $id;

        return view('ibel.referensi.tim_seleksi_detail', $data);
    }

    // Post With Helper
    public function addReferensiTimOtorisasi(Request $request)
    {
        $input = [
            'namaTim' => $request->nama_tim_otorisasi,
            'kantorId' => session()->get('user')['kantorId'],
            'createdBy' => session()->get('user')['pegawaiId']
        ];
        $response = IbelHelper::postWithToken('/izin_belajar/upk/timseleksi', $input);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Tim berhasil ditambahkan!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }
    // Patch With Helper
    // public function updateTimOtorisasi(Request $request)
    // {
    //     $input = [
    //         'id' => $request->id_ref_edit,
    //         'namaTim' => $request->nama_tim_otorisasi,
    //         'kantorId' => $request->id_kantor_tim_otorisasi_edit,
    //         'updatedBy' => session()->get('user')['pegawaiId']
    //     ];

    //     $response = IbelHelper::patchWithToken('/izin_belajar/upk/timseleksi', $input);

    //     if ($response['status'] == 1) {
    //         return redirect()->back()->with('success', 'Lokasi Pendidikan berhasil diubah!');
    //     } else {
    //         return redirect()->back()->with('error', $response['message']);
    //     }
    // }

    // Delete With Helper
    public function deleteTimOtorisasi(Request $request)
    {
        $input = [
            'pegawaiInputId' => session()->get('user')['pegawaiId']
        ];

        $pegawaiId = session()->get('user')['pegawaiId'];
        $response = IbelHelper::deleteWithToken('/izin_belajar/upk/timseleksi?timId=' . $request->id . '&pegawaiId=' . $pegawaiId, $input);

        return response()->json($response);
    }

    //cari Anggota
    public function cariAnggotaTim(Request $request)
    {
        $token = IbelHelper::getNewSession(session()->get('login_info')['username']);
        $response = IbelHelper::getWithToken('/otoriasi_izin_belajar/search?nama=' . $request->keyword);
        return response()->json($response);
    }

    //tambah Anggota
    public function tambahAnggotaTim(Request $request)
    {
        #set output
        $output_berhasil = 0;
        $output_gagal = 0;

        #jika tidak ada data
        if (!$request->id_anggota_tim_by_otorisasi) {
            return ['status' => 0, 'message' => 'Tidak ada pegawai yang dipilih'];
        }

        #jika data hanya 1
        if (count($request->id_anggota_tim_by_otorisasi) == 1) {
            #looping
            $id_tim = $request->id_tim_otorisasi;

            foreach ($request->id_anggota_tim_by_otorisasi as $id) {
                $input = [
                    'otorisasiIzinBelajarId' => $id,
                    'timSeleksiId' => $id_tim
                ];

                #send data
                $response = IbelHelper::postWithToken('/izin_belajar/upk/anggotatimseleksi/', $input);

                #output
                if ($response['status'] == 1) {
                    return ['status' => 1, 'message' => 'Data berhasil ditambahkan'];
                } else {
                    return ['status' => 0, 'message' => 'Database error (conflict)'];
                }
            }
        } elseif (count($request->id_anggota_tim_by_otorisasi) > 1) {
            #looping
            $id_tim = $request->id_tim_otorisasi;

            foreach ($request->id_anggota_tim_by_otorisasi as $id) {
                $input = [
                    'otorisasiIzinBelajarId' => $id,
                    'timSeleksiId' => $id_tim
                ];

                #send data
                $response = IbelHelper::postWithToken('/izin_belajar/upk/anggotatimseleksi/', $input);

                #output
                if ($response['status'] == 1) {
                    $output_berhasil = $output_berhasil + 1;
                } else {
                    #set output baru
                    $output_gagal = $output_gagal + 1;

                    #jika gagal semua
                    if (count($request->id_tubel) == $output_gagal) {
                        return ['status' => 0, 'message' => 'Database error (conflict)'];
                    }
                }
            }
        }

        #set to array
        $response['output_berhasil'] = $output_berhasil . ' data berhasil ditambahkan';
        $response['output_gagal'] = ', ' . $output_gagal . ' data gagal ditambahkan';

        return response()->json($response);
    }

    // Delete Anggota Tim
    public function hapusAnggotaTim(Request $request)
    {
        $input = [
            'pegawaiInputId' => session()->get('user')['pegawaiId']
        ];

        $response = IbelHelper::deleteWithToken('/izin_belajar/upk/anggotatimseleksi/{angotaId}?anggotaId=' . $request->id, $input);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Tim berhasil dihapus!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }
}
