<?php

namespace App\Http\Controllers\Tubel;

use App\Helpers\TubelHelper;
use App\Models\MenuModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class KelolaReferensiController extends Controller
{
// -------------------------------------REFERENSI NEGARA MULAI----------------------

    public function indexReferensiNegara()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/referensi');
        $data['route'] = 'tubel/referensi';

        #tab
        $data['tab'] = 'referensi_negara';

        #param
        $CariNama = isset($_GET['cari']) ? $_GET['cari'] : null;
        $size = 5;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;

        # tampilkan semua data negara dari hasil cari
        $data['negara'] = TubelHelper::getRef('/negara_perguruan_tinggi/search?page=' . $page
            . '&size=' . $size
            . '&nama=' . $CariNama);

        #pagination
        $data['totalItems'] = $data['negara']->totalItems;
        $data['totalPages'] = $data['negara']->totalPages;
        $data['currentPage'] = $data['negara']->currentPage;
        $data['numberOfElements'] = $data['negara']->numberOfElements;
        $data['size'] = $data['negara']->size;

        // dd($data);
        return view('tubel.referensi.tubel_referensi_index', $data);
    }

    public function addReferensiNegara(Request $request)
    {
        $negara = [
            'namaNegara' => $request->nama_ref,
            'createdBy' => session()->get('user')['pegawaiId']
        ];

        $response = TubelHelper::post('/negara_perguruan_tinggi/', $negara);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Nama Negara berhasil ditambahkan!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }

    }

    public function updateReferensiNegara(Request $request)
    {
        $body = [
            'id' => $request->id_ref_edit,
            'namaNegara' => $request->nama_ref_edit,
            'updatedBy' => session()->get('user')['pegawaiId']
        ];

        $response = TubelHelper::patch('/negara_perguruan_tinggi/' . $request->id_ref_edit, $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Nama Negara berhasil diupadate!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }

    }

    public function deleteReferensiNegara(Request $request)
    {
        $response = TubelHelper::delete('/negara_perguruan_tinggi/' . $request->id, [
                'pegawaiInputId' => session()->get('user')['pegawaiId']]
        );

        return response()->json($response);
    }
// --------------------------------REFERENSI NEGARA SELESAI-----------------------------

// ################################REFERENSI PERGURUAN TINGGI MULAI#####################
    public function indexReferensiPT()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/referensi');
        $data['route'] = 'tubel/referensi/pt';

        #tab
        $data['tab'] = 'referensi_pt';

        #param
        $namaPT = isset($_GET['cari']) ? $_GET['cari'] : null;
        $size = 5;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;

        # tampilkan semua data negara dari hasil cari
        $data['pt'] = TubelHelper::getRef('/perguruan_tinggi/search/{nama}?nama=' . $namaPT
            . '&page=' . $page
            . '&size=' . $size
        );

        #pagination
        $data['totalItems'] = $data['pt']->totalItems;
        $data['totalPages'] = $data['pt']->totalPages;
        $data['currentPage'] = $data['pt']->currentPage;
        $data['numberOfElements'] = $data['pt']->numberOfElements;
        $data['size'] = $data['pt']->size;

        // dd($data['pt']);
        return view('tubel.referensi.tubel_referensi_index', $data);
    }

    public function addReferensiPT(Request $request)
    {
        $body = [
            'namaPerguruanTinggi' => $request->nama_ref,
            'createdBy' => session()->get('user')['pegawaiId']
        ];
        $response = TubelHelper::post('/perguruan_tinggi/', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Nama Perguruan Tinggi berhasil ditambahkan!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function updateReferensiPT(Request $request)
    {
        $body = [
            'id' => $request->id_ref_edit,
            'namaPerguruanTinggi' => $request->nama_ref_edit,
            'updatedBy' => session()->get('user')['pegawaiId']
        ];

        $response = TubelHelper::patch('/perguruan_tinggi/' . $request->id_ref_edit, $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Nama Perguruan Tinggi berhasil diupdate!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }

    }

    public function deleteReferensiPT(Request $request)
    {
        $response = TubelHelper::delete('/perguruan_tinggi/' . $request->id, [
                'pegawaiInputId' => session()->get('user')['pegawaiId']]
        );

        return response()->json($response);
    }

// ################################REFERENSI PROGRAM STUDI MULAI#####################
    public function indexReferensiProdi()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/referensi');
        $data['route'] = 'tubel/referensi/prodi';

        #tab
        $data['tab'] = 'referensi_prodi';

        #param
        $namaProdi = isset($_GET['cari']) ? $_GET['cari'] : null;
        $size = 5;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;

        # tampilkan semua data negara dari hasil cari
        $data['prodi'] = TubelHelper::getRef('/program_studi/search?page=' . $page
            . '&size=' . $size
            . '&nama=' . $namaProdi);

        #pagination
        $data['totalItems'] = $data['prodi']->totalItems;
        $data['totalPages'] = $data['prodi']->totalPages;
        $data['currentPage'] = $data['prodi']->currentPage;
        $data['numberOfElements'] = $data['prodi']->numberOfElements;
        $data['size'] = $data['prodi']->size;

        return view('tubel.referensi.tubel_referensi_index', $data);
    }

    public function addReferensiProdi(Request $request)
    {
        $body = [
            'programStudi' => $request->nama_ref,
            'createdBy' => session()->get('user')['pegawaiId']
        ];

        $response = TubelHelper::post('/program_studi/', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Nama Program Studi berhasil ditambahkan!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function updateReferensiProdi(Request $request)
    {
        $body = [
            'id' => $request->id_ref_edit,
            'programStudi' => $request->nama_ref_edit,
            'updatedBy' => session()->get('user')['pegawaiId']
        ];

        $response = TubelHelper::patch('/program_studi/' . $request->id_ref_edit, $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Nama Program Studi berhasil diupdate!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }

    }

    public function deleteReferensiProdi(Request $request)
    {
        $response = TubelHelper::delete('/program_studi/' . $request->id, [
                'pegawaiInputId' => session()->get('user')['pegawaiId']]
        );

        return response()->json($response);
    }

// ################################REFERENSI JENJANG PENDIDIKAN MULAI#####################
    public function indexReferensiJenjang()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/referensi');
        $data['route'] = 'tubel/referensi/jenjang';

        #tab
        $data['tab'] = 'referensi_jenjang';

        #param
        $namaJenjang = isset($_GET['cari']) ? $_GET['cari'] : null;
        $size = 5;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;

        # tampilkan semua data dari hasil cari
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan/search?page=' . $page
            . '&size=' . $size
            . '&nama=' . $namaJenjang);

        #pagination
        $data['totalItems'] = $data['jenjang']->totalItems;
        $data['totalPages'] = $data['jenjang']->totalPages;
        $data['currentPage'] = $data['jenjang']->currentPage;
        $data['numberOfElements'] = $data['jenjang']->numberOfElements;
        $data['size'] = $data['jenjang']->size;

        // dd($data['prodi']);
        return view('tubel.referensi.tubel_referensi_index', $data);
    }

    public function addReferensiJenjang(Request $request)
    {
        $body = [
            'jenjangPendidikan' => $request->nama_ref,
            'createdBy' => session()->get('user')['pegawaiId']
        ];

        $response = TubelHelper::post('/jenjang_pendidikan/', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Nama Jenjang Pendidikan berhasil ditambahkan!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function updateReferensiJenjang(Request $request)
    {
        $body = [
            'id' => $request->id_ref_edit,
            'jenjangPendidikan' => $request->nama_ref_edit,
            'updatedBy' => session()->get('user')['pegawaiId']
        ];

        $response = TubelHelper::patch('/jenjang_pendidikan/' . $request->id_ref_edit, $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Nama Jenjang Pendidikan berhasil diupdate!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }

    }

    public function deleteReferensiJenjang(Request $request)
    {
        $response = TubelHelper::delete('/jenjang_pendidikan/' . $request->id, [
                'deletedBy' => session()->get('user')['pegawaiId']]
        );

        return response()->json($response);
    }

// ################################REFERENSI LOKASI PENDIDIKAN MULAI#####################
    public function indexReferensiLokasi()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/referensi');
        $data['route'] = 'tubel/referensi/lokasi';

        #tab
        $data['tab'] = 'referensi_lokasi';

        #param
        $namaLokasi = isset($_GET['cari']) ? $_GET['cari'] : null;
        $size = 5;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;

        # tampilkan semua data dari hasil cari
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan/search?page=' . $page
            . '&size=' . $size
            . '&nama=' . $namaLokasi);

        #pagination
        $data['totalItems'] = $data['lokasi']->totalItems;
        $data['totalPages'] = $data['lokasi']->totalPages;
        $data['currentPage'] = $data['lokasi']->currentPage;
        $data['numberOfElements'] = $data['lokasi']->numberOfElements;
        $data['size'] = $data['lokasi']->size;

        // dd($data['lokasi']);
        return view('tubel.referensi.tubel_referensi_index', $data);
    }

    public function addReferensiLokasi(Request $request)
    {
        $body = [
            'lokasi' => $request->nama_ref,
            'createdBy' => session()->get('user')['pegawaiId']
        ];

        $response = TubelHelper::post('/lokasi_pendidikan/', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Lokasi Pendidikan berhasil ditambahkan!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function updateReferensiLokasi(Request $request)
    {
        $body = [
            'id' => $request->id_ref_edit,
            'lokasi' => $request->nama_ref_edit,
            'updatedBy' => session()->get('user')['pegawaiId']
        ];

        $response = TubelHelper::patch('/lokasi_pendidikan/' . $request->id_ref_edit, $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Lokasi Pendidikan berhasil diupdate!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }

    }

    public function deleteReferensiLokasi(Request $request)
    {
        $response = TubelHelper::delete('/lokasi_pendidikan/' . $request->id, [
                'pegawaiInputId' => session()->get('user')['pegawaiId']]
        );

        return response()->json($response);
    }


}
