<?php

namespace App\Http\Controllers\Tubel;

use App\Helpers\TubelHelper;
use App\Models\MenuModel;
use Illuminate\Routing\Controller;


class MonitoringController extends Controller
{
    public function monitoringTubel()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/monitoring');
        $data['route'] = 'tubel/monitoring';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $status = isset($_GET['status']) ? $_GET['status'] : null;
        $tgl_mulai_dari = isset($_GET['tgl_mulai_dari']) ? $_GET['tgl_mulai_dari'] : null;
        $tgl_mulai_selesai = isset($_GET['tgl_mulai_selesai']) ? $_GET['tgl_mulai_selesai'] : null;
        $tgl_selesai_dari = isset($_GET['tgl_selesai_dari']) ? $_GET['tgl_selesai_dari'] : null;
        $tgl_selesai_selesai = isset($_GET['tgl_selesai_selesai']) ? $_GET['tgl_selesai_selesai'] : null;

        #get data tubal
        $data['tubel'] = TubelHelper::getRef('/tubel/search?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&status=' . $status
            . '&tanggal_mulai_start=' . $tgl_mulai_dari
            . '&tanggal_mulai_end=' . $tgl_mulai_selesai
            . '&tanggal_selesai_start=' . $tgl_selesai_dari
            . '&tanggal_selesai_end=' . $tgl_selesai_selesai);

        #looping data tubel
        $tubel = $data['tubel']->data;
        $perguruantinggi = [];
        foreach ($tubel as $key => $item) {

            #get data perguruan tinggi
            $perguruantinggis = TubelHelper::get('/perguruan_tinggi_peserta/tubel/' . $item->id);

            #looping perguruan tinggi
            $pt = [];
            foreach ($perguruantinggis['data'] as $key => $p) {
                $pt[] = $p->perguruanTinggiId->namaPerguruanTinggi;
            }

            #set dalam array
            $perguruantinggi[$item->id] = $pt;
        }

        $data['perguruantinggi'] = $perguruantinggi;

        #looping status
        $case = TubelHelper::getRef('/probis/tubel/cases');

        $status = [];
        foreach ($case as $c) {

            #looping
            foreach ($c->rcaseOutputList as $c => $value) {
                $status[] = [
                    'id' => $value->id,
                    'caseOutput' => $value->caseOutput,
                ];
            }
        }

        #pagination
        $data['totalItems'] = $data['tubel']->totalItems;
        $data['totalPages'] = $data['tubel']->totalPages;
        $data['currentPage'] = $data['tubel']->currentPage;
        $data['numberOfElements'] = $data['tubel']->numberOfElements;
        $data['size'] = $data['tubel']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');
        $data['status'] = $status;

        return view('tubel.monitoring.index', $data);
    }

    public function detailMonitoringTubel($id)
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/monitoring');

        #tab
        $tab = !empty($_GET['tab']) ? $_GET['tab'] : 'lps';
        $data['action'] = false;
        $data['tab'] = $tab;

        if ($tab == 'lps') {
            $data['lps'] = TubelHelper::getRef('/laporan/tubel/' . $id);
        } elseif ($tab == 'cuti') {
            $data['cuti'] = TubelHelper::getRef('/cuti_belajar/tubel/' . $id);
        } elseif ($tab == 'pengaktifan') {
            $data['pengaktifan_kembali'] = TubelHelper::getRef('/pengaktifan_kembali/tubel/' . $id);
        } elseif ($tab == 'perpanjangan') {
            $data['perpanjangan'] = TubelHelper::getRef('/perpanjangan_st/tubel/' . $id);
        }

        #get data tubel pegawai
        $data['tubel'] = TubelHelper::getRef('/tubel/' . $id);
        $data['perguruantinggi'] = TubelHelper::getRef('/perguruan_tinggi_peserta/tubel/' . $id);
        $data['countperguruantinggi'] = count(TubelHelper::getRef('/perguruan_tinggi_peserta/tubel/' . $id));

        #referensi
        $data['ref_lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');
        $data['ref_perguruantinggi'] = TubelHelper::getRef('/perguruan_tinggi');
        $data['ref_negara'] = TubelHelper::getRef('/negara_perguruan_tinggi');
        $data['ref_prodi'] = TubelHelper::getRef('/program_studi');

        $data['ptTubel'] = TubelHelper::getRef('/perguruan_tinggi_peserta/tubel/' . $id);

        return view('tubel.administrasi.detail', $data);
    }
}
