<?php

namespace App\Http\Controllers\Tubel;

use App\Helpers\LoginHelper;
use App\Helpers\TubelHelper;
use App\Models\MenuModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;

class CutiBelajarController extends Controller
{
    public function getCuti()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/cuti');
        $data['route'] = 'tubel/cuti';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $kep = isset($_GET['kep']) ? $_GET['kep'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $tgl_mulai = isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : null;
        $tgl_selesai = isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : null;

        $data['tab'] = 'menunggu_persetujuan';
        $data['cuti'] = TubelHelper::getRef('/cuti_belajar/proses/persetujuan?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&kep=' . $kep
            . '&tiket=' . $tiket
            . '&tanggal_selesai_cuti_awal=' . $tgl_mulai
            . '&tanggal_selesai_cuti_akhir=' . $tgl_selesai);

        #pagination
        $data['totalItems'] = $data['cuti']->totalItems;
        $data['totalPages'] = $data['cuti']->totalPages;
        $data['currentPage'] = $data['cuti']->currentPage;
        $data['numberOfElements'] = $data['cuti']->numberOfElements;
        $data['size'] = $data['cuti']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.cutibelajar.cuti_index', $data);
    }

    public function getCutiSetuju()
    {

        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/cuti');
        $data['route'] = 'tubel/cuti';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $kep = isset($_GET['kep']) ? $_GET['kep'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $tgl_mulai = isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : null;
        $tgl_selesai = isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : null;

        $data['tab'] = 'disetujui';
        $data['cuti'] = TubelHelper::getRef('/cuti_belajar/proses/menunggu_penempatan?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&kep=' . $kep
            . '&tiket=' . $tiket
            . '&tanggal_selesai_cuti_awal=' . $tgl_mulai
            . '&tanggal_selesai_cuti_akhir=' . $tgl_selesai);

        #pagination
        $data['totalItems'] = $data['cuti']->totalItems;
        $data['totalPages'] = $data['cuti']->totalPages;
        $data['currentPage'] = $data['cuti']->currentPage;
        $data['numberOfElements'] = $data['cuti']->numberOfElements;
        $data['size'] = $data['cuti']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.cutibelajar.cuti_index', $data);
    }

    public function getcutiTolak()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/cuti');
        $data['route'] = 'tubel/cuti';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $kep = isset($_GET['kep']) ? $_GET['kep'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $tgl_mulai = isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : null;
        $tgl_selesai = isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : null;

        $data['tab'] = 'ditolak';
        $data['cuti'] = TubelHelper::getRef('/cuti_belajar/proses/tolak?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&kep=' . $kep
            . '&tiket=' . $tiket
            . '&tanggal_selesai_cuti_awal=' . $tgl_mulai
            . '&tanggal_selesai_cuti_akhir=' . $tgl_selesai);

        #pagination
        $data['totalItems'] = $data['cuti']->totalItems;
        $data['totalPages'] = $data['cuti']->totalPages;
        $data['currentPage'] = $data['cuti']->currentPage;
        $data['numberOfElements'] = $data['cuti']->numberOfElements;
        $data['size'] = $data['cuti']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.cutibelajar.cuti_index', $data);
    }

    public function getCutiSelesai()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/cuti');
        $data['route'] = 'tubel/cuti';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $kep = isset($_GET['kep']) ? $_GET['kep'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $tgl_mulai = isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : null;
        $tgl_selesai = isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : null;

        $data['tab'] = 'selesai';
        $data['cuti'] = TubelHelper::getRef('/cuti_belajar/proses/ditempatkan?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&kep=' . $kep
            . '&tiket=' . $tiket
            . '&tanggal_selesai_cuti_awal=' . $tgl_mulai
            . '&tanggal_selesai_cuti_akhir=' . $tgl_selesai);

        #pagination
        $data['totalItems'] = $data['cuti']->totalItems;
        $data['totalPages'] = $data['cuti']->totalPages;
        $data['currentPage'] = $data['cuti']->currentPage;
        $data['numberOfElements'] = $data['cuti']->numberOfElements;
        $data['size'] = $data['cuti']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.cutibelajar.cuti_index', $data);
    }

    public function getCutiDashboard()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/cuti');
        $data['route'] = 'tubel/cuti';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;

        $data['tab'] = 'dashboard';
        $data['cuti'] = TubelHelper::getRef('/cuti_belajar/proses/persetujuan?page=' . $page . '&size=' . $size);

        #pagination
        $data['totalItems'] = $data['cuti']->totalItems;
        $data['totalPages'] = $data['cuti']->totalPages;
        $data['currentPage'] = $data['cuti']->currentPage;
        $data['numberOfElements'] = $data['cuti']->numberOfElements;
        $data['size'] = $data['cuti']->size;

        return view('tubel.cuti_index', $data);
    }

    #ini mau disesuaikan
    public function GetIndukTubelCuti(Request $request)
    {

        if ($request->nip == null) {
            return ['status' => 0, 'message' => 'nip tidak boleh kosong'];
        }

        $response = TubelHelper::get('/tubel/nip_nama/' . $request->nip);

        if ($response['data'] == null) {
            return ['status' => 0, 'message' => 'data tidak ditemukan'];
        } else {
            return response()->json($response['data'][0]);
        }
    }

    public function getKepPenempatanCuti()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/kep-penempatan-cuti');
        $data['route'] = 'tubel/kep-penempatan-cuti';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $kep = isset($_GET['kep']) ? $_GET['kep'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $kantor = isset($_GET['kantor']) ? $_GET['kantor'] : null;
        $jabatan = isset($_GET['jabatan']) ? $_GET['jabatan'] : null;
        $tgl_mulai_dari = isset($_GET['tgl_mulai_dari']) ? $_GET['tgl_mulai_dari'] : null;
        $tgl_mulai_selesai = isset($_GET['tgl_mulai_selesai']) ? $_GET['tgl_mulai_selesai'] : null;
        $tgl_selesai_dari = isset($_GET['tgl_selesai_dari']) ? $_GET['tgl_selesai_dari'] : null;
        $tgl_selesai_selesai = isset($_GET['tgl_selesai_selesai']) ? $_GET['tgl_selesai_selesai'] : null;

        $data['tab'] = 'rekam_kep';
        $data['penempatancuti'] = TubelHelper::getRef('/cuti_belajar/proses/menunggu_penempatan?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&kep_penempatan=' . $kep
            . '&tiket=' . $tiket
            . '&kantor_asal=' . $kantor
            . '&jabatan_asal=' . $jabatan
            . '&tanggal_mulai_cuti_start=' . $tgl_mulai_dari
            . '&tanggal_mulai_cuti_end=' . $tgl_mulai_selesai
            . '&tanggal_selesai_cuti_start=' . $tgl_selesai_dari
            . '&tanggal_selesai_cuti_end=' . $tgl_selesai_selesai);

        #pagination
        $data['totalItems'] = $data['penempatancuti']->totalItems;
        $data['totalPages'] = $data['penempatancuti']->totalPages;
        $data['currentPage'] = $data['penempatancuti']->currentPage;
        $data['numberOfElements'] = $data['penempatancuti']->numberOfElements;
        $data['size'] = $data['penempatancuti']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');


        return view('tubel.keppenempatancuti.keppenempatancuti_index', $data);
    }

    public function getKepPenempatanCutiSetuju()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/kep-penempatan-cuti');
        $data['route'] = 'tubel/kep-penempatan-cuti';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $kep = isset($_GET['kep']) ? $_GET['kep'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $kantor = isset($_GET['kantor']) ? $_GET['kantor'] : null;
        $jabatan = isset($_GET['jabatan']) ? $_GET['jabatan'] : null;
        $tgl_mulai_dari = isset($_GET['tgl_mulai_dari']) ? $_GET['tgl_mulai_dari'] : null;
        $tgl_mulai_selesai = isset($_GET['tgl_mulai_selesai']) ? $_GET['tgl_mulai_selesai'] : null;
        $tgl_selesai_dari = isset($_GET['tgl_selesai_dari']) ? $_GET['tgl_selesai_dari'] : null;
        $tgl_selesai_selesai = isset($_GET['tgl_selesai_selesai']) ? $_GET['tgl_selesai_selesai'] : null;

        $data['tab'] = 'setuju';
        $data['penempatancuti'] = TubelHelper::getRef('/cuti_belajar/proses/ditempatkan?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&kep_penempatan=' . $kep
            . '&tiket=' . $tiket
            . '&kantor_asal=' . $kantor
            . '&jabatan_asal=' . $jabatan
            . '&tanggal_mulai_cuti_start=' . $tgl_mulai_dari
            . '&tanggal_mulai_cuti_end=' . $tgl_mulai_selesai
            . '&tanggal_selesai_cuti_start=' . $tgl_selesai_dari
            . '&tanggal_selesai_cuti_end=' . $tgl_selesai_selesai);

        #pagination
        $data['totalItems'] = $data['penempatancuti']->totalItems;
        $data['totalPages'] = $data['penempatancuti']->totalPages;
        $data['currentPage'] = $data['penempatancuti']->currentPage;
        $data['numberOfElements'] = $data['penempatancuti']->numberOfElements;
        $data['size'] = $data['penempatancuti']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.keppenempatancuti.keppenempatancuti_index', $data);
    }

    public function getKepPenempatanCutiDitempatkan()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/kep-penempatan-cuti');
        $data['route'] = 'tubel/kep-penempatan-cuti';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $kep = isset($_GET['kep']) ? $_GET['kep'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $kantor = isset($_GET['kantor']) ? $_GET['kantor'] : null;
        $jabatan = isset($_GET['jabatan']) ? $_GET['jabatan'] : null;
        $tgl_mulai_dari = isset($_GET['tgl_mulai_dari']) ? $_GET['tgl_mulai_dari'] : null;
        $tgl_mulai_selesai = isset($_GET['tgl_mulai_selesai']) ? $_GET['tgl_mulai_selesai'] : null;
        $tgl_selesai_dari = isset($_GET['tgl_selesai_dari']) ? $_GET['tgl_selesai_dari'] : null;
        $tgl_selesai_selesai = isset($_GET['tgl_selesai_selesai']) ? $_GET['tgl_selesai_selesai'] : null;

        $data['tab'] = 'ditempatkan';
        $data['penempatancuti'] = TubelHelper::getRef('/cuti_belajar/proses/ditempatkan?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&kep_penempatan=' . $kep
            . '&tiket=' . $tiket
            . '&kantor_asal=' . $kantor
            . '&jabatan_asal=' . $jabatan
            . '&tanggal_mulai_cuti_start=' . $tgl_mulai_dari
            . '&tanggal_mulai_cuti_end=' . $tgl_mulai_selesai
            . '&tanggal_selesai_cuti_start=' . $tgl_selesai_dari
            . '&tanggal_selesai_cuti_end=' . $tgl_selesai_selesai);

        #pagination
        $data['totalItems'] = $data['penempatancuti']->totalItems;
        $data['totalPages'] = $data['penempatancuti']->totalPages;
        $data['currentPage'] = $data['penempatancuti']->currentPage;
        $data['numberOfElements'] = $data['penempatancuti']->numberOfElements;
        $data['size'] = $data['penempatancuti']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.keppenempatancuti.keppenempatancuti_index', $data);
    }

    public function getLokasiPenempatan(Request $request)
    {
        // create jwt token
        $token = LoginHelper::iampost('authentication', [
            'username' => 'ardi.ananta',
            'password' => 'Pajak123'
        ]);

        if ($token['status'] == 0) {
            return ['status' => 0, 'message' => 'Error get Auth'];
        }

        $header = $token['data']->token;

        // $response = null;
        $response = LoginHelper::iamget('/api/kantors/active/' . $request->lokasi, $header);

        if ($response['status'] == 0) {
            return ['status' => 0, 'message' => 'error get service lokasi'];
        }

        return response()->json($response['data']->kantors);
    }

    public function rekamKepPenempatanCuti(Request $request)
    {

        #split lokasi penempatan
        $lokasi_penempatan = $request->lokasi_penempatan_rekam;
        $result = explode('|', $lokasi_penempatan);

        #get lokasi di dan nama
        $lokasi_penempatan_id = $result[0];
        $lokasi_penempatan_nama = $result[1];

        $file_kep = $this->upload($request->file_kep_rekam);

        if ($file_kep['status'] == 0) {
            return redirect()->back()->with('error', $file_kep['message']);
        }

        $isi = [
            'nomorSuratKep' => $request->nomor_kep_rekam,
            'tglSuratKep' => $request->tgl_kep_rekam,
            'pathKepPenempatan' => $file_kep['file'],
            'tglTmtPenempatan' => $request->tmt_penempatan_rekam,
            'kantorIdPenempatan' => $lokasi_penempatan_id,
            'namaKantorPenempatan' => $lokasi_penempatan_nama
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/cuti_belajar/penempatan/' . $request->id_cuti, $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'KEP Penempatan Cuti berhasil direkam');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    private function upload($file)
    {

        $filepath = $file->getRealPath();
        $nama = $file->getClientOriginalName();
        $mime = $file->getMimeType();
        $ukuran = $file->getSize();

        // validasi mime file
        if ($mime != 'application/pdf') {
            return ['status' => 0, 'message' => 'Format file bukan .pdf'];
        }

        // validasi ukuran 2mb
        if ($ukuran > 2097152) {
            return ['status' => 0, 'message' => 'Ukuran file lebih dari 2 MB'];
        }

        $response = Http::attach('file', file_get_contents($filepath), $nama)
            ->put(env('API_URL') . '/files');

        if (!$response->successful()) {
            $message = $response->body();

            $retUpload = ['status' => 0, 'message' => $message];
        } else {
            $retUpload = ['status' => 1, 'file' => $response->body()];
        }

        return ($retUpload);
    }

    public function getAdvKantor(Request $request)
    {
        #token iam (baru)

        #validasi token (jika expired = refresh token)
        // $token = TubelHelper::cekToken(session()->get('login_info')['_expired']);

        $token = TubelHelper::getNewSession(session()->get('login_info')['username']);

        $response = LoginHelper::iamget(env('API_URL_IAM') . '/api/kantors/active/' . $request->search, $token);

        foreach ($response['data']->kantors as $key => $item) {
            $data[] = [
                'id' => $item->nama,
                'text' => $item->nama
            ];
        }

        return json_encode($data);
    }

    public function getAdvJabatan(Request $request)
    {
        #token iam (baru)
        $token = TubelHelper::getNewSession(session()->get('login_info')['username']);

        #get jabatan for advanced
        $response = LoginHelper::iamget(env('API_URL_IAM') . '/api/jabatans/active/' . $request->search, $token);

        #loop
        foreach ($response['data']->jabatans as $key => $item) {
            $data[] = [
                'id' => $item->nama,
                'text' => $item->nama
            ];
        }

        return json_encode($data);
    }

    public function getPerpanjanganCuti()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/perpanjangan-cuti');
        $data['route'] = 'tubel/perpanjangan-cuti';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $tgl_mulai = isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : null;
        $tgl_selesai = isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : null;

        $data['tab'] = 'menunggu_persetujuan';
        $data['perpanjangancuti'] = TubelHelper::getRef('/cuti_belajar_perpanjangan/proses/persetujuan?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&tiket=' . $tiket
            . '&tanggal_selesai_cuti_awal=' . $tgl_mulai
            . '&tanggal_selesai_cuti_akhir=' . $tgl_selesai);

        #pagination
        $data['totalItems'] = $data['perpanjangancuti']->totalItems;
        $data['totalPages'] = $data['perpanjangancuti']->totalPages;
        $data['currentPage'] = $data['perpanjangancuti']->currentPage;
        $data['numberOfElements'] = $data['perpanjangancuti']->numberOfElements;
        $data['size'] = $data['perpanjangancuti']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');


        return view('tubel.perpanjangancuti.perpanjangancuti_index', $data);
    }

    public function getPerpanjanganCutiSetuju()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/perpanjangan-cuti');
        $data['route'] = 'tubel/perpanjangan-cuti';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $tgl_mulai = isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : null;
        $tgl_selesai = isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : null;

        $data['tab'] = 'telah_disetujui';
        $data['perpanjangancuti'] = TubelHelper::getRef('/cuti_belajar_perpanjangan/proses/terima?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&tiket=' . $tiket
            . '&tanggal_selesai_cuti_awal=' . $tgl_mulai
            . '&tanggal_selesai_cuti_akhir=' . $tgl_selesai);

        #get perguruan tinggi
        // foreach ($data['perpanjangancuti'] as $key => $item) {
        //     $perguruantinggis = TubelHelper::getRef('/perguruan_tinggi_peserta/tubel/' . $item->indukTubel->id);

        //     $pt = [];
        //     if ($perguruantinggis != null) {
        //         foreach ($perguruantinggis as $perguruantinggi) {
        //             $pt[] = [
        //                 'idTubel' => $perguruantinggi->dataIndukTubelId->id,
        //                 'idPt' => $perguruantinggi->perguruanTinggiId->id,
        //                 'namaPt' => $perguruantinggi->perguruanTinggiId->namaPerguruanTinggi
        //             ];
        //         }
        //     }
        // };

        #pagination
        $data['totalItems'] = $data['perpanjangancuti']->totalItems;
        $data['totalPages'] = $data['perpanjangancuti']->totalPages;
        $data['currentPage'] = $data['perpanjangancuti']->currentPage;
        $data['numberOfElements'] = $data['perpanjangancuti']->numberOfElements;
        $data['size'] = $data['perpanjangancuti']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.perpanjangancuti.perpanjangancuti_index', $data);
    }

    public function getPerpanjanganCutiSetujuTolak()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/perpanjangan-cuti');
        $data['route'] = 'tubel/perpanjangan-cuti';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $tgl_mulai = isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : null;
        $tgl_selesai = isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : null;

        $data['tab'] = 'ditolak';
        $data['perpanjangancuti'] = TubelHelper::getRef('/cuti_belajar_perpanjangan/proses/tolak?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&tiket=' . $tiket
            . '&tanggal_selesai_cuti_awal=' . $tgl_mulai
            . '&tanggal_selesai_cuti_akhir=' . $tgl_selesai);

        #pagination
        $data['totalItems'] = $data['perpanjangancuti']->totalItems;
        $data['totalPages'] = $data['perpanjangancuti']->totalPages;
        $data['currentPage'] = $data['perpanjangancuti']->currentPage;
        $data['numberOfElements'] = $data['perpanjangancuti']->numberOfElements;
        $data['size'] = $data['perpanjangancuti']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.perpanjangancuti.perpanjangancuti_index', $data);
    }

    public function getDataPerpanjanganCuti(Request $request)
    {
        if ($request->tiket == null) {
            return ['status' => 0, 'message' => 'nomor tiket tidak boleh kosong'];
        }

        $response = TubelHelper::get('/cuti_belajar/tiket?all=1&tiket=' . $request->tiket);

        if ($response['status'] == 0) {
            return ['status' => 0, 'message' => 'data tidak ditemukan'];
        } else {
            return response()->json($response);
        }
    }

    public function addPerpanjanganCutiAdmin(Request $request)
    {
        $file_cuti = $this->upload($request->file_cuti_perpanjangan_rekam);

        if ($file_cuti['status'] == 0) {
            return redirect()->back()->with('error', $file_cuti['message']);
        }

        $file_sponsor = $this->upload($request->file_sponsors_perpanjangan_rekam);

        if ($file_sponsor['status'] == 0) {
            return redirect()->back()->with('error', $file_sponsor['message']);
        }

        $isi = [
            'alasanCuti' => $request->alasan_cuti_perpanjangan_rekam,
            'tglMulaiCuti' => $request->tgl_mulai_cuti_perpanjangan_rekam,
            'tglSelesaiCuti' => $request->tgl_selesai_cuti_perpanjangan_rekam,
            'nomorSuratKampus' => $request->surat_izin_cuti_kampus_perpanjangan_rekam,
            'tglSuratKampus' => $request->tgl_surat_kampus_perpanjangan_rekam,
            'nomorSuratSponsor' => $request->surat_izin_cuti_sponsor_perpanjangan_rekam,
            'tglSuratKepSponsor' => $request->tgl_surat_sponsor_perpanjangan_rekam,
            'pathSuratIjinCutiKampus' => $file_cuti['file'],
            'pathSuratIjinCutiSponsor' => $file_sponsor['file'],
            'dataIndukTubelId' => $request->idTubel
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/cuti_belajar_perpanjangan/admin/cuti_utama/' . $request->tiket, $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data Berhasil ditambah!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function updatePerpanjanganCutiAdmin(Request $request)
    {

        #file cuti kampus
        if ($request->file_cuti_perpanjangan_edit != null) {  #jika update file
            $file_cuti = $this->upload($request->file_cuti_perpanjangan_edit);

            #delete file lama
            TubelHelper::deleteFile('/files/' . $request->file_cuti_perpanjangan_edit_lama);

            #validasi
            if ($file_cuti['status'] == 0) {
                return redirect()->back()->with('error', 'Gagal upload file cuti kampus');
            }
        } else { #jika tidak
            $file_cuti['file'] = $request->file_cuti_perpanjangan_edit_lama;
        }

        #file cuti sponsor
        if ($request->file_sponsors_perpanjangan_edit != null) {  #jika update file
            $file_sponsor = $this->upload($request->file_sponsors_perpanjangan_edit);

            #delete file lama
            TubelHelper::deleteFile('/files/' . $request->file_sponsors_perpanjangan_edit_lama);

            #validasi
            if ($file_sponsor['status'] == 0) {
                return redirect()->back()->with('error', 'Gagal upload file cuti sponsor');
            }
        } else { #jika tidak
            $file_sponsor['file'] = $request->file_sponsors_perpanjangan_edit_lama;
        }

        $isi = [
            'alasanCuti' => $request->alasan_cuti_perpanjangan_edit,
            'tglMulaiCuti' => $request->tgl_mulai_cuti_perpanjangan_edit,
            'tglSelesaiCuti' => $request->tgl_selesai_cuti_perpanjangan_edit,
            'nomorSuratKampus' => $request->surat_izin_cuti_kampus_perpanjangan_edit,
            'tglSuratKampus' => $request->tgl_surat_kampus_perpanjangan_edit,
            'nomorSuratSponsor' => $request->surat_izin_cuti_sponsor_perpanjangan_edit,
            'tglSuratKepSponsor' => $request->tgl_surat_sponsor_perpanjangan_edit,
            'pathSuratIjinCutiKampus' => $file_cuti['file'],
            'pathSuratIjinCutiSponsor' => $file_sponsor['file'],
            'dataIndukTubelId' => $request->idTubel
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::patch('/cuti_belajar/' . $request->id_cuti, $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasil diupdate!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }
}
