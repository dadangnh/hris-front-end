<?php

namespace App\Http\Controllers\Tubel;

use App\Helpers\TubelHelper;
use App\Models\MenuModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;


class LaporDiriController extends Controller
{
    public function laporDiri(Request $request)
    {
        #list route
        $route = ['persetujuan', 'ditolak', 'kep-penempatan', 'dashboard'];

        #validate route
        if ($request->route == '' || in_array($request->route, $route)) {

            #get menu, tab
            $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/pelaporan/lapor-diri');
            $data['tab'] = $request->route ? $request->route : 'persetujuan';

            #set parameters
            $size = 10;
            $page = $request->page;
            $nama = $request->nama;
            $nip = $request->nip;
            $jenjang = $request->jenjang;
            $lokasi = $request->lokasi;
            $topik = $request->topik;
            $tiket = $request->tiket;

            #set api
            switch ($data['tab']) {
                case 'persetujuan':
                    $response = TubelHelper::get('/lapor_selesai/monitoring/persetujuan?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&jenjang=' . $jenjang
                        . '&lokasi=' . $lokasi
                        . '&topik=' . $topik
                        . '&tiket=' . $tiket);
                    break;

                case 'ditolak':
                    $response = TubelHelper::get('/lapor_selesai/monitoring/tolak?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&jenjang=' . $jenjang
                        . '&lokasi=' . $lokasi
                        . '&topik=' . $topik
                        . '&tiket=' . $tiket);
                    break;

                case 'kep-penempatan':
                    $response = TubelHelper::get('/lapor_selesai/monitoring/ditempatkan?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&jenjang=' . $jenjang
                        . '&lokasi=' . $lokasi
                        . '&topik=' . $topik
                        . '&tiket=' . $tiket);
                    break;
            }

            #data
            $data['lapordiri'] = $response['data']->data;

            $pt = [];

            # get perguruan tinggi (jika data lulus)
            if ($data['lapordiri'] != null) {

                foreach ($data['lapordiri'] as $key => $item) {

                    #get data perguruan tinggi
                    $perguruantinggis = TubelHelper::get('/perguruan_tinggi_peserta/tubel/' . $item->dataIndukTubelId->id);

                    if ($perguruantinggis['data'] != null) {
                        #looping
                        foreach ($perguruantinggis['data'] as $perguruantinggi) {
                            $pt[$item->dataIndukTubelId->id][] = [
                                'idTubel' => $perguruantinggi->dataIndukTubelId->id,
                                'ipk' => $perguruantinggi->ipk,
                                'namaPerguruanTinggi' => $perguruantinggi->perguruanTinggiId->namaPerguruanTinggi,
                                'nomorTranskripNilai' => $perguruantinggi->nomorTranskripNilai,
                                'tglTranskripNilai' => $perguruantinggi->tglTranskripNilai,
                                'pathTranskripNilai' => $perguruantinggi->pathTranskripNilai,
                                'nomorSkl' => $perguruantinggi->nomorSkl,
                                'tglSkl' => $perguruantinggi->tglSkl,
                                'pathSkl' => $perguruantinggi->pathSkl,
                                'nomorIjazah' => $perguruantinggi->nomorIjazah,
                                'tglIjazah' => $perguruantinggi->tglIjazah,
                                'pathIjazah' => $perguruantinggi->pathIjazah
                            ];
                        }
                    }
                }

                #data to variable
                $data['perguruantinggi'] = $pt;
            }

            #pagination
            $data['totalItems'] = $response['data']->totalItems;
            $data['totalPages'] = $response['data']->totalPages;
            $data['currentPage'] = $response['data']->currentPage;
            $data['numberOfElements'] = $response['data']->numberOfElements;
            $data['size'] = $response['data']->size;

            #referensi
            $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
            $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

            return view('tubel.lapordiri.lapordiri_index', $data);
        } else {
            return abort(404);
        }
    }

    public function setujuLaporDiri(Request $request)
    {
        $isi = [
            'keterangan' => null,
        ];

        # generate body
        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        # send to api
        $response = TubelHelper::post('/lapor_selesai/' . $request->id . '/terima', $body);

        return response()->json($response);
    }

    public function tolakLaporDiri(Request $request)
    {
        $isi = [
            'keterangan' => $request->keterangan,
        ];

        # generate body
        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        # send to api
        $response = TubelHelper::post('/lapor_selesai/' . $request->id . '/tolak', $body);

        return response()->json($response);
    }

    public function hasilAkhir(Request $request)
    {
        #list route
        $route = ['persetujuan', 'ditolak', 'diterima', 'dashboard'];

        #validate route
        if ($request->route == '' || in_array($request->route, $route)) {

            #get menu, tab
            $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/pelaporan/hasil-akhir');
            $data['tab'] = $request->route ? $request->route : 'persetujuan';

            #set parameters
            $size = 10;
            $page = $request->page;
            $nama = $request->nama;
            $nip = $request->nip;
            $jenjang = $request->jenjang;
            $lokasi = $request->lokasi;
            $topik = $request->topik;
            $tiket = $request->tiket;

            #set api
            switch ($data['tab']) {
                case 'persetujuan':
                    $response = TubelHelper::get('/hasil_studi/monitoring/studi/verifikasi?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&jenjang=' . $jenjang
                        . '&lokasi=' . $lokasi
                        . '&topik=' . $topik
                        . '&tiket=' . $tiket);
                    break;

                case 'ditolak':
                    $response = TubelHelper::get('/hasil_studi/monitoring/studi/tolak?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&jenjang=' . $jenjang
                        . '&lokasi=' . $lokasi
                        . '&topik=' . $topik
                        . '&tiket=' . $tiket);
                    break;

                case 'diterima':
                    $response = TubelHelper::get('/hasil_studi/monitoring/KP4?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&jenjang=' . $jenjang
                        . '&lokasi=' . $lokasi
                        . '&topik=' . $topik
                        . '&tiket=' . $tiket);
                    break;
            }

            #data
            $data['hasil_akhir'] = $response['data']->data;

            # get perguruan tinggi (jika data lulus)
            $pt = [];
            if ($data['hasil_akhir'] != null) {

                foreach ($data['hasil_akhir'] as $key => $item) {

                    #get data perguruan tinggi
                    $perguruantinggis = TubelHelper::get('/perguruan_tinggi_peserta/tubel/' . $item->statusLaporDiri->dataIndukTubelId->id);

                    if ($perguruantinggis['data'] != null) {
                        #looping
                        foreach ($perguruantinggis['data'] as $perguruantinggi) {
                            $pt[$item->statusLaporDiri->dataIndukTubelId->id][] = [
                                'idTubel' => $perguruantinggi->dataIndukTubelId->id,
                                'ipk' => $perguruantinggi->ipk,
                                'namaPerguruanTinggi' => $perguruantinggi->perguruanTinggiId->namaPerguruanTinggi,
                                'nomorTranskripNilai' => $perguruantinggi->nomorTranskripNilai,
                                'tglTranskripNilai' => $perguruantinggi->tglTranskripNilai,
                                'pathTranskripNilai' => $perguruantinggi->pathTranskripNilai,
                                'nomorSkl' => $perguruantinggi->nomorSkl,
                                'tglSkl' => $perguruantinggi->tglSkl,
                                'pathSkl' => $perguruantinggi->pathSkl,
                                'nomorIjazah' => $perguruantinggi->nomorIjazah,
                                'tglIjazah' => $perguruantinggi->tglIjazah,
                                'pathIjazah' => $perguruantinggi->pathIjazah
                            ];
                        }
                    }
                }

                #data to variable
                $data['perguruantinggi'] = $pt;
            }

            #pagination
            $data['totalItems'] = $response['data']->totalItems;
            $data['totalPages'] = $response['data']->totalPages;
            $data['currentPage'] = $response['data']->currentPage;
            $data['numberOfElements'] = $response['data']->numberOfElements;
            $data['size'] = $response['data']->size;

            #referensi
            $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
            $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

            return view('tubel.lapordiri.hasil_akhir_index', $data);
        } else {
            return abort(404);
        }
    }

    public function verifikasiIjazah(Request $request)
    {
        #list route
        $route = ['menunggu-verifikasi', 'ditolak', 'terverifikasi', 'dashboard'];

        #validate route
        if ($request->route == '' || in_array($request->route, $route)) {

            #get menu, tab
            $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/pelaporan/verifikasi-ijazah');
            $data['tab'] = $request->route ? $request->route : 'menunggu-verifikasi';

            #set parameters
            $size = 10;
            $page = $request->page;
            $nama = $request->nama;
            $nip = $request->nip;
            $jenjang = $request->jenjang;
            $lokasi = $request->lokasi;
            $topik = $request->topik;
            $tiket = $request->tiket;

            #set api
            switch ($data['tab']) {
                case 'menunggu-verifikasi':
                    $response = TubelHelper::get('/hasil_studi/monitoring/ijazah/verifikasi?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&jenjang=' . $jenjang
                        . '&lokasi=' . $lokasi
                        . '&topik=' . $topik
                        . '&tiket=' . $tiket);
                    break;

                case 'ditolak':
                    $response = TubelHelper::get('/hasil_studi/monitoring/ijazah/tolak?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&jenjang=' . $jenjang
                        . '&lokasi=' . $lokasi
                        . '&topik=' . $topik
                        . '&tiket=' . $tiket);
                    break;

                case 'terverifikasi':
                    $response = TubelHelper::get('/hasil_studi/monitoring/KP4?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&jenjang=' . $jenjang
                        . '&lokasi=' . $lokasi
                        . '&topik=' . $topik
                        . '&tiket=' . $tiket);
                    break;
            }

            #data
            $data['verifikasi'] = $response['data']->data;

            $pt = [];

            # get perguruan tinggi (jika data lulus)
            if ($data['verifikasi'] != null) {

                foreach ($data['verifikasi'] as $key => $item) {

                    #get data perguruan tinggi
                    $perguruantinggis = TubelHelper::get('/perguruan_tinggi_peserta/tubel/' . $item->statusLaporDiri->dataIndukTubelId->id);

                    if ($perguruantinggis['data'] != null) {
                        #looping
                        foreach ($perguruantinggis['data'] as $perguruantinggi) {
                            $pt[$item->statusLaporDiri->dataIndukTubelId->id][] = [
                                'idTubel' => $perguruantinggi->dataIndukTubelId->id,
                                'ipk' => $perguruantinggi->ipk,
                                'namaPerguruanTinggi' => $perguruantinggi->perguruanTinggiId->namaPerguruanTinggi,
                                'nomorTranskripNilai' => $perguruantinggi->nomorTranskripNilai,
                                'tglTranskripNilai' => $perguruantinggi->tglTranskripNilai,
                                'pathTranskripNilai' => $perguruantinggi->pathTranskripNilai,
                                'nomorSkl' => $perguruantinggi->nomorSkl,
                                'tglSkl' => $perguruantinggi->tglSkl,
                                'pathSkl' => $perguruantinggi->pathSkl,
                                'nomorIjazah' => $perguruantinggi->nomorIjazah,
                                'tglIjazah' => $perguruantinggi->tglIjazah,
                                'pathIjazah' => $perguruantinggi->pathIjazah
                            ];
                        }
                    }
                }

                #data to variable
                $data['perguruantinggi'] = $pt;
            }

            #pagination
            $data['totalItems'] = $response['data']->totalItems;
            $data['totalPages'] = $response['data']->totalPages;
            $data['currentPage'] = $response['data']->currentPage;
            $data['numberOfElements'] = $response['data']->numberOfElements;
            $data['size'] = $response['data']->size;

            #referensi
            $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
            $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

            return view('tubel.lapordiri.verifikasi_ijazah_index', $data);
        } else {
            return abort(404);
        }
    }

    public function setujuHasilAkhir(Request $request)
    {
        $isi = [
            'keterangan' => null,
        ];

        # generate body
        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        # send to api
        $response = TubelHelper::post('/hasil_studi/' . $request->id . '/studi/terima', $body);

        return response()->json($response);
    }

    public function tolakHasilAkhir(Request $request)
    {
        $isi = [
            'keterangan' => $request->keterangan,
        ];

        # generate body
        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        # send to api
        $response = TubelHelper::post('/hasil_studi/' . $request->id . '/studi/tolak', $body);

        return response()->json($response);
    }

    public function setujuverifikasiIjazah(Request $request)
    {
        $isi = [
            'keterangan' => null,
        ];

        # generate body
        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        # send to api
        $response = TubelHelper::post('/hasil_studi/' . $request->id . '/ijazah/terima', $body);

        return response()->json($response);
    }

    public function tolakverifikasiIjazah(Request $request)
    {
        $isi = [
            'keterangan' => $request->keterangan,
        ];

        # generate body
        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        # send to api
        $response = TubelHelper::post('/hasil_studi/' . $request->id . '/ijazah/tolak', $body);

        return response()->json($response);
    }
}
