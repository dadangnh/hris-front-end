<?php

namespace App\Http\Controllers\Tubel;

use App\Helpers\TubelHelper;
use App\Models\MenuModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;

class PerpanjanganStController extends Controller
{
    public function perpanjanganStNonStan()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/perpanjangan-st-nonstan');
        $data['route'] = 'tubel/perpanjangan-st-nonstan';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $nost = isset($_GET['nost']) ? $_GET['nost'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $tgl_mulai = isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : null;
        $tgl_selesai = isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : null;

        $data['tab'] = 'menunggu';
        $data['perpanjanganstnonstan'] = TubelHelper::getRef('/perpanjangan_st/monitoring/menunggu_reviu?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&surat_tugas_awal=' . $nost
            . '&tiket=' . $tiket
            . '&tanggal_selesai_awal=' . $tgl_mulai
            . '&tanggal_selesai_akhir=' . $tgl_selesai);

        #pagination
        $data['totalItems'] = $data['perpanjanganstnonstan']->totalItems;
        $data['totalPages'] = $data['perpanjanganstnonstan']->totalPages;
        $data['currentPage'] = $data['perpanjanganstnonstan']->currentPage;
        $data['numberOfElements'] = $data['perpanjanganstnonstan']->numberOfElements;
        $data['size'] = $data['perpanjanganstnonstan']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.perpanjanganstnonstan.perpanjanganstnonstan_index', $data);
    }

    public function perpanjanganstnonstansetuju()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/perpanjangan-st-nonstan');
        $data['route'] = 'tubel/perpanjangan-st-nonstan';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $nost = isset($_GET['nost']) ? $_GET['nost'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $tgl_mulai = isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : null;
        $tgl_selesai = isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : null;

        $data['tab'] = 'disetujui';
        $data['perpanjanganstnonstan'] = TubelHelper::getRef('/perpanjangan_st/monitoring/disetujui?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&surat_tugas_awal=' . $nost
            . '&tiket=' . $tiket
            . '&tanggal_selesai_awal=' . $tgl_mulai
            . '&tanggal_selesai_akhir=' . $tgl_selesai);

        #pagination
        $data['totalItems'] = $data['perpanjanganstnonstan']->totalItems;
        $data['totalPages'] = $data['perpanjanganstnonstan']->totalPages;
        $data['currentPage'] = $data['perpanjanganstnonstan']->currentPage;
        $data['numberOfElements'] = $data['perpanjanganstnonstan']->numberOfElements;
        $data['size'] = $data['perpanjanganstnonstan']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.perpanjanganstnonstan.perpanjanganstnonstan_index', $data);
    }

    public function perpanjanganstnonstankembalikan()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/perpanjangan-st-nonstan');
        $data['route'] = 'tubel/perpanjangan-st-nonstan';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $nost = isset($_GET['nost']) ? $_GET['nost'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $tgl_mulai = isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : null;
        $tgl_selesai = isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : null;

        $data['tab'] = 'dikembalikan';
        $data['perpanjanganstnonstan'] = TubelHelper::getRef('/perpanjangan_st/monitoring/dikembalikan?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&surat_tugas_awal=' . $nost
            . '&tiket=' . $tiket
            . '&tanggal_selesai_awal=' . $tgl_mulai
            . '&tanggal_selesai_akhir=' . $tgl_selesai);

        #pagination
        $data['totalItems'] = $data['perpanjanganstnonstan']->totalItems;
        $data['totalPages'] = $data['perpanjanganstnonstan']->totalPages;
        $data['currentPage'] = $data['perpanjanganstnonstan']->currentPage;
        $data['numberOfElements'] = $data['perpanjanganstnonstan']->numberOfElements;
        $data['size'] = $data['perpanjanganstnonstan']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.perpanjanganstnonstan.perpanjanganstnonstan_index', $data);
    }

    public function perpanjanganstnonstantolak()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/perpanjangan-st-nonstan');
        $data['route'] = 'tubel/perpanjangan-st-nonstan';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $nost = isset($_GET['nost']) ? $_GET['nost'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $tgl_mulai = isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : null;
        $tgl_selesai = isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : null;

        $data['tab'] = 'ditolak';
        $data['perpanjanganstnonstan'] = TubelHelper::getRef('/perpanjangan_st/monitoring/ditolak?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&surat_tugas_awal=' . $nost
            . '&tiket=' . $tiket
            . '&tanggal_selesai_awal=' . $tgl_mulai
            . '&tanggal_selesai_akhir=' . $tgl_selesai);

        #pagination
        $data['totalItems'] = $data['perpanjanganstnonstan']->totalItems;
        $data['totalPages'] = $data['perpanjanganstnonstan']->totalPages;
        $data['currentPage'] = $data['perpanjanganstnonstan']->currentPage;
        $data['numberOfElements'] = $data['perpanjanganstnonstan']->numberOfElements;
        $data['size'] = $data['perpanjanganstnonstan']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.perpanjanganstnonstan.perpanjanganstnonstan_index', $data);
    }

    public function perpanjanganstnonstanterbitst()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/perpanjangan-st-nonstan');
        $data['route'] = 'tubel/perpanjangan-st-nonstan';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $nost = isset($_GET['nost']) ? $_GET['nost'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $tgl_mulai = isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : null;
        $tgl_selesai = isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : null;

        $data['tab'] = 'terbitst';
        $data['perpanjanganstnonstan'] = TubelHelper::getRef('/perpanjangan_st/monitoring/terbit_st?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&surat_tugas_awal=' . $nost
            . '&tiket=' . $tiket
            . '&tanggal_selesai_awal=' . $tgl_mulai
            . '&tanggal_selesai_akhir=' . $tgl_selesai);

        #pagination
        $data['totalItems'] = $data['perpanjanganstnonstan']->totalItems;
        $data['totalPages'] = $data['perpanjanganstnonstan']->totalPages;
        $data['currentPage'] = $data['perpanjanganstnonstan']->currentPage;
        $data['numberOfElements'] = $data['perpanjanganstnonstan']->numberOfElements;
        $data['size'] = $data['perpanjanganstnonstan']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.perpanjanganstnonstan.perpanjanganstnonstan_index', $data);
    }

    public function perpanjanganstnonstandashboard()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/perpanjangan-st-nonstan');
        $data['route'] = 'tubel/perpanjangan-st-nonstan';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $nost = isset($_GET['nost']) ? $_GET['nost'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $tgl_mulai = isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : null;
        $tgl_selesai = isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : null;

        $data['tab'] = 'dashboard';
        $data['perpanjanganstnonstan'] = TubelHelper::getRef('/perpanjangan_st/monitoring/terbit_st?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&surat_tugas_awal=' . $nost
            . '&tiket=' . $tiket
            . '&tanggal_selesai_awal=' . $tgl_mulai
            . '&tanggal_selesai_akhir=' . $tgl_selesai);

        #pagination
        $data['totalItems'] = $data['perpanjanganstnonstan']->totalItems;
        $data['totalPages'] = $data['perpanjanganstnonstan']->totalPages;
        $data['currentPage'] = $data['perpanjanganstnonstan']->currentPage;
        $data['numberOfElements'] = $data['perpanjanganstnonstan']->numberOfElements;
        $data['size'] = $data['perpanjanganstnonstan']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.perpanjanganstnonstan_index', $data);
    }

    public function perpanjanganStNonStanActionSetuju(Request $request)
    {
        $isi = ['keterangan' => null];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/perpanjangan_st/' . $request->id . '/setuju', $body);

        return response()->json($response);
    }

    public function perpanjanganStNonStanActionTolak(Request $request)
    {

        $isi = ['keterangan' => $request->keterangan];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        $response = TubelHelper::post('/perpanjangan_st/' . $request->id . '/tolak', $body);

        return response()->json($response);
    }

    public function perpanjanganStNonStanActionKembalikan(Request $request)
    {
        $isi = ['keterangan' => $request->keterangan];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/perpanjangan_st/' . $request->id . '/kembalikan', $body);

        return response()->json($response);
    }

    public function perpanjanganStNonStanActionUploadSt(Request $request)
    {
        $file = $this->upload($request->file_upload_st);

        if ($file['status'] == 0) {
            return redirect()->back()->with('error', $file['message']);
        }

        $isi = [
            'nomorStPerpanjangan' => $request->nomor_st_upload_st,
            'tglStPerpanjangan' => $request->tgl_upload_st,
            'tglMulaiStPerpanjangan' => $request->tgl_mulai_upload_st,
            'tglSelesaiStPerpanjangan' => $request->tgl_selesai_upload_st,
            'pathStPerpanjangan' => $file['file']
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/perpanjangan_st/' . $request->id_permohonan_aktif_upload_st . '/penerbitan_st', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'ST berhasil direkam!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    private function upload($file)
    {

        $filepath = $file->getRealPath();
        $nama = $file->getClientOriginalName();
        $mime = $file->getMimeType();
        $ukuran = $file->getSize();

        // validasi mime file
        if ($mime != 'application/pdf') {
            return ['status' => 0, 'message' => 'Format file bukan .pdf'];
        }

        // validasi ukuran 2mb
        if ($ukuran > 2097152) {
            return ['status' => 0, 'message' => 'Ukuran file lebih dari 2 MB'];
        }

        $response = Http::attach('file', file_get_contents($filepath), $nama)
            ->put(env('API_URL') . '/files');

        if (!$response->successful()) {
            $message = $response->body();

            $retUpload = ['status' => 0, 'message' => $message];
        } else {
            $retUpload = ['status' => 1, 'file' => $response->body()];
        }

        return ($retUpload);
    }

    public function perpanjanganststan()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/perpanjangan-st-stan');
        $data['route'] = 'tubel/perpanjangan-st-stan';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $stawal = isset($_GET['stawal']) ? $_GET['stawal'] : null;
        $stperpanjang = isset($_GET['stperpanjang']) ? $_GET['stperpanjang'] : null;
        $tglselesai = isset($_GET['tglselesai']) ? $_GET['tglselesai'] : null;

        $data['tab'] = 'perpanjangan';
        $data['perpanjanganststan'] = TubelHelper::getRef('/perpanjangan_st_stan/monitoring/draft?page=' . $page
            . '&size=' . $size
            . '&surat_tugas_awal=' . $stawal
            . '&surat_tugas_perpanjangan=' . $stperpanjang
            . '&tanggal_selesai_perpanjangan=' . $tglselesai);

        #pagination
        $data['totalItems'] = $data['perpanjanganststan']->totalItems;
        $data['totalPages'] = $data['perpanjanganststan']->totalPages;
        $data['currentPage'] = $data['perpanjanganststan']->currentPage;
        $data['numberOfElements'] = $data['perpanjanganststan']->numberOfElements;
        $data['size'] = $data['perpanjanganststan']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.perpanjanganststan.perpanjanganststan_index', $data);
    }

    public function perpanjanganStStanUploadSt()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/perpanjangan-st-stan');
        $data['route'] = 'tubel/perpanjangan-st-stan';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $stawal = isset($_GET['stawal']) ? $_GET['stawal'] : null;
        $stperpanjang = isset($_GET['stperpanjang']) ? $_GET['stperpanjang'] : null;
        $tglselesai = isset($_GET['tglselesai']) ? $_GET['tglselesai'] : null;

        #tab
        $data['tab'] = 'telah_upload';

        #data from rest
        $data['perpanjanganststan'] = TubelHelper::getRef('/perpanjangan_st_stan/monitoring/terbit_st?page=' . $page
            . '&size=' . $size
            . '&surat_tugas_awal=' . $stawal
            . '&surat_tugas_perpanjangan=' . $stperpanjang
            . '&tanggal_selesai_perpanjangan=' . $tglselesai);

        #pagination
        $data['totalItems'] = $data['perpanjanganststan']->totalItems;
        $data['totalPages'] = $data['perpanjanganststan']->totalPages;
        $data['currentPage'] = $data['perpanjanganststan']->currentPage;
        $data['numberOfElements'] = $data['perpanjanganststan']->numberOfElements;
        $data['size'] = $data['perpanjanganststan']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.perpanjanganststan.perpanjanganststan_index', $data);
    }

    public function perpanjanganStStanAdd($id)
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/perpanjangan-st-stan');
        $data['route'] = 'tubel/perpanjangan-st-stan';
        $data['perpanjanganststan'] = TubelHelper::getref('/perpanjangan_st_stan/' . $id);
        $data['action'] = true;

        $data['lampiran'] = TubelHelper::getRef('/perpanjangan_st_stan/' . $id . '/lampiran');

        return view('tubel.perpanjanganststan.perpanjanganststan_detail', $data);
    }

    public function perpanjanganStStanDetail($id)
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/perpanjangan-st-stan');
        $data['route'] = 'tubel/perpanjangan-st-stan';
        $data['perpanjanganststan'] = TubelHelper::getRef('/perpanjangan_st_stan/' . $id);

        $data['action'] = false;

        $data['lampiran'] = TubelHelper::getRef('/perpanjangan_st_stan/' . $id . '/lampiran');

        return view('tubel.perpanjanganststan.perpanjanganststan_detail', $data);
    }

    public function perpanjanganStStanDetailLihat($id)
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/perpanjangan-st-stan');
        $data['route'] = 'tubel/perpanjangan-st-stan';
        $data['perpanjanganststan'] = TubelHelper::getRef('/perpanjangan_st_stan/' . $id);

        $data['lampiran'] = TubelHelper::getRef('/perpanjangan_st_stan/' . $id . '/lampiran');

        return view('tubel.perpanjanganststan.perpanjanganststan_detail_lihat', $data);
    }

    public function deletePerpanjanganStStan(Request $request)
    {

        $response = TubelHelper::delete('/perpanjangan_st_stan/' . $request->id, [
            'pegawaiInputId' => session()->get('user')['pegawaiId']
        ]);

        return response()->json($response);
    }

    public function addDasarStPerpanjanganStStan(Request $request)
    {

        $file_dokumen = $this->upload($request->file_dokumen);
        $file_dokumen_pendukung = $this->upload($request->file_dokumen_pendukung);

        if ($file_dokumen['status'] == 0) {
            return redirect()->back()->with('error', 'Dokumen gagal diupload');
        }

        if ($file_dokumen_pendukung['status'] == 0) {
            return redirect()->back()->with('error', 'Dokumen pendukung gagal diupload');
        }

        $isi = [
            'alasanPerpanjangan' => $request->alasan_perpanjangan_st,
            'tglSelesaiPerpanjangan' => $request->tgl_selesai_perpanjangan,
            'noStAwal' => $request->nomor_st_awal,
            'pathStAwal' => $file_dokumen['file'],
            'keteranganDokumenPendukung' => $request->keterangan,
            'pathDokumenPendukung' => $file_dokumen_pendukung['file']
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/perpanjangan_st_stan/', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Dasar Perpanjangan ST berhasil ditambahkan!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function updateDasarStPerpanjanganStStan(Request $request)
    {
        $file_dokumen = $this->upload($request->file_dokumen_edit);
        $file_dokumen_pendukung = $this->upload($request->file_dokumen_pendukung_edit);

        if ($file_dokumen['status'] == 0) {
            return redirect()->back()->with('error', 'Dokumen gagal diupload');
        }

        if ($file_dokumen_pendukung['status'] == 0) {
            return redirect()->back()->with('error', 'Dokumen pendukung gagal diupload');
        }

        $isi = [
            'alasanPerpanjangan' => $request->alasan_perpanjangan_st_edit,
            'tglSelesaiPerpanjangan' => $request->tgl_selesai_perpanjangan_edit,
            'noStAwal' => $request->nomor_st_awal_edit,
            'pathStAwal' => $file_dokumen['file'],
            'keteranganDokumenPendukung' => $request->keterangan_edit,
            'pathDokumenPendukung' => $file_dokumen_pendukung['file']
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::patch('/perpanjangan_st_stan/' . $request->id_perpanjangan_stan_edit, $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Dasar Perpanjangan ST berhasil diupdate!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function uploadStPerpanjanganStStan(Request $request)
    {
        $file_upload_st = $this->upload($request->file_upload_st);

        if ($file_upload_st['status'] == 0) {
            return redirect()->back()->with('error', 'Dokumen gagal diupload');
        }

        $isi = [
            'nomorStPerpanjangan' => $request->nomor_st_upload_st,
            'tglStPerpanjangan' => $request->tgl_st_upload_st,
            'tglMulaiStPerpanjangan' => $request->tgl_mulai_upload_st,
            'tglSelesaiStPerpanjangan' => $request->tgl_selesai_upload_st,
            'pathStPerpanjangan' => $file_upload_st['file'],
            'iamToken' => session('token_apps')
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/perpanjangan_st_stan/' . $request->id_perpanjangan_stan . '/penerbitan_st', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Dasar Perpanjangan ST berhasil diupdate!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function getNamaNip(Request $request)
    {

        $response = TubelHelper::get('/tubel/nip_nama/' . $request->keyword);

        return response()->json($response);
    }

    public function addNamaNipPerpanjanganStStan(Request $request)
    {
        dd($request->all());

        #set output
        $output_berhasil = 0;
        $output_gagal = 0;

        $isi = [
            'keterangan' => null,
        ];

        #get body
        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        #jika tidak ada data
        if (!$request->id_tubel) {
            return ['status' => 0, 'message' => 'Tidak ada pegawai yang dipilih'];
        }

        #jika data hanya 1
        if (count($request->id_tubel) == 1) {
            #looping
            foreach ($request->id_tubel as $id) {
                #send data
                $response = TubelHelper::post('/perpanjangan_st_stan/' . $request->id_perpanjangan . '/lampiran/tubel/' . $id, $body);

                #output
                if ($response['status'] == 1) {
                    return ['status' => 1, 'message' => 'Data berhasil ditambahkan'];
                } else {
                    return ['status' => 0, 'message' => 'Database error (conflict)'];
                }
            }
        } elseif (count($request->id_tubel) > 1) {
            #looping
            foreach ($request->id_tubel as $id) {
                #send data
                $response = TubelHelper::post('/perpanjangan_st_stan/' . $request->id_perpanjangan . '/lampiran/tubel/' . $id, $body);

                #output
                if ($response['status'] == 1) {
                    $output_berhasil = $output_berhasil + 1;
                } else {
                    #set output baru
                    $output_gagal = $output_gagal + 1;

                    #jika gagal semua
                    if (count($request->id_tubel) == $output_gagal) {
                        return ['status' => 0, 'message' => 'Database error (conflict)'];
                    }
                }
            }
        }


        #set to array
        $response['output_berhasil'] = $output_berhasil . ' data berhasil ditambahkan';
        $response['output_gagal'] = ', ' . $output_gagal . ' data gagal ditambahkan';

        return response()->json($response);
    }

    // public function perpanjanganStStanLampiranGenerate(Request $request)

    public function addPerpanjanganStStanTubel(Request $request)
    {

        $isi = [
            'keterangan' => null
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/perpanjangan_st_stan/' . $request->id_perpanjangan . '/lampiran/st/' . $request->nomor_st_awal, $body);

        if ($response['status'] == 1) {
            #return redirect()->back()->with('success', 'Dasar berhasil ditambahkan!');
            return redirect()->back()->with('success', $response['data']->message);
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function generateLampiranStStan($id)
    {
        $response = Http::post(env('API_URL') . '/perpanjangan_st_stan/generate/lampiran?id%20Perpanjangan%20Tubel%20STAN=' . $id);

        #tmp direktori
        $tmp_direktori = tempnam(sys_get_temp_dir(), $id);

        #get content header disposition
        $headers = ['accept: ' . $response->headers()['Content-Type'][0]];

        #get file name
        $fraw = explode(';', $response->headers()['Content-Disposition'][0]);
        $fraw2 = explode('=', $fraw[1]);
        $namafile = str_replace('"', "", $fraw2[1]);

        #custom nama file
        // $namafile = session('user')->nip9 . '_LPS.docx';

        #put content
        file_put_contents($tmp_direktori, $response->body());

        return response()->download($tmp_direktori, $namafile, $headers);
    }

    public function deleteLampiranStStan(Request $request)
    {
        $response = TubelHelper::delete('/perpanjangan_st_stan/' . $request->id . '/lampiran/' . $request->id_lampiran, [
            'pegawaiInputId' => session()->get('user')['pegawaiId']
        ]);

        return response()->json($response);
    }

    public function deleteAllLampiranStStan(Request $request)
    {
        $response = TubelHelper::delete('/perpanjangan_st_stan/' . $request->id . '/all/lampiran', [
            'pegawaiInputId' => session()->get('user')['pegawaiId']
        ]);

        return response()->json($response);
    }
}
