<?php

namespace App\Http\Controllers\Tubel;

use App\Helpers\LoginHelper;
use App\Helpers\TubelHelper;
use App\Models\MenuModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;

class AdministrasiController extends Controller
{
    public function index()
    {
        #get menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/administrasi');

        #get data tubel
        $tubel = TubelHelper::get('/tubel/pegawai/' . session()->get('user')['pegawaiId']);

        $data['tubel'] = $tubel['data'];
        #set array null
        $pt = [];

        #looping perguruan tinggi
        if ($data['tubel'] != null) {

            foreach ($data['tubel'] as $key => $item) {

                #get data perguruan tinggi
                $perguruantinggis = TubelHelper::get('/perguruan_tinggi_peserta/tubel/' . $item->id);

                if ($perguruantinggis['data'] != null) {
                    #looping
                    foreach ($perguruantinggis['data'] as $perguruantinggi) {
                        $pt[] = [
                            'idTubel' => $perguruantinggi->dataIndukTubelId->id,
                            'idPt' => $perguruantinggi->perguruanTinggiId->id,
                            'namaPt' => $perguruantinggi->perguruanTinggiId->namaPerguruanTinggi
                        ];
                    }
                }
            }

            # data to variable
            $data['perguruantinggi'] = $pt;
        }

        return view('tubel.administrasi.index', $data);
    }

    public function getDataTubel()
    {
        $isi = [
            'pegawaiInputId' => session()->get('user')['pegawaiId']
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/tubel/sikka/' . session()->get('user')['nip9'], $body);

        return response()->json($response);
    }

    public function getDataKantor(Request $request)
    {
        # token iam (baru)
        $token = TubelHelper::getNewSession(session()->get('login_info')['username']);

        $response = LoginHelper::iamget('/api/kantors/active/' . $request->search, $token);

        foreach ($response['data']->kantors as $key => $item) {
            $data[] = [
                'id' => $item->id . '|' . $item->nama,
                'text' => $item->nama
            ];
        }

        return json_encode($data);
    }

    public function getDataJabatan(Request $request)
    {

        # token iam (baru)
        $token = TubelHelper::getNewSession(session()->get('login_info')['username']);

        # get jabatan
        $response = LoginHelper::iamget('/api/jabatans/active/' . $request->search, $token);

        # loop
        foreach ($response['data']->jabatans as $key => $item) {
            $data[] = [
                'id' => $item->id . '|' . $item->nama,
                'text' => $item->nama
            ];
        }

        return json_encode($data);
    }

    public function getDataStan(Request $request)
    {

        # get data
        $response = TubelHelper::get('/perpanjangan_st_stan/' . $request->id);

        return json_encode($response['data']);
    }

    public function getDataPerguruanTinggi(Request $request)
    {
        # get param
        $page = 1;
        $size = 5;
        $nama = $request->search;

        # get data
        // $response = TubelHelper::getRef('/perguruan_tinggi/search/' . $request->search);
        $response = TubelHelper::getRef('/perguruan_tinggi/search/{nama}?nama=' . $request->search);

        #loop
        foreach ($response->data as $key => $item) {
            $data[] = [
                'id' => $item->id,
                'text' => $item->namaPerguruanTinggi
            ];
        }

        return json_encode($data);
    }

    public function getDataProdi(Request $request)
    {
        #get data
        $response = TubelHelper::getRef('/program_studi/search?nama=' . $request->search);

        #loop
        foreach ($response->data as $key => $item) {
            $data[] = [
                'id' => $item->id,
                'text' => $item->programStudi
            ];
        }

        return json_encode($data);
    }

    public function getDataNegara(Request $request)
    {
        #get data
        $response = TubelHelper::getRef('/negara_perguruan_tinggi/search?nama=' . $request->search);

        #loop
        foreach ($response->data as $key => $item) {
            $data[] = [
                'id' => $item->id,
                'text' => $item->namaNegara
            ];
        }

        return json_encode($data);
    }

    public function getTopikRiset(Request $request)
    {

        #get param
        $page = 1;
        $size = 10;
        $topik = $request->search;
        // $nomor_pengumuman = $request->nomor_pengumuman;

        #get data
        $response = TubelHelper::get('/topik_riset/search?nama_topik=' . $topik . '&page=' . $page . '&size=' . $size . '');

        #loop
        foreach ($response['data']->data as $key => $item) {
            $data[] = [
                'id' => $item->id,
                'text' => $item->namaTopik
            ];
        }

        return json_encode($data);
    }

    public function detailTubel($id)
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/administrasi');

        #tab
        $tab = !empty($_GET['tab']) ? $_GET['tab'] : 'lps';
        $data['action'] = true;
        $data['tab'] = $tab;

        #get data from tab
        switch ($tab) {
            case 'lps':
                $response = TubelHelper::get('/laporan/tubel/' . $id);
                $data['lps'] = $response['data'];
                break;

            case 'cuti':
                $response = TubelHelper::get('/cuti_belajar/tubel/' . $id);
                $data['cuti'] = $response['data'];
                break;

            case 'pengaktifan':
                $response = TubelHelper::get('/pengaktifan_kembali/tubel/' . $id);
                $data['pengaktifan_kembali'] = $response['data'];
                break;

            case 'perpanjangan':
                $response = TubelHelper::get('/perpanjangan_st/tubel/' . $id);
                $data['perpanjangan'] = $response['data'];
                break;

            case 'usulanriset':
                $response = TubelHelper::get('/usulan_topik_riset/tubel/' . $id);
                $data['usulanriset'] = $response['data'];

                # get selesai studi
                $data['selesai_studi'] = TubelHelper::getRef('/lapor_selesai/tubel/' . $id);

                #get hasil akhir
                if ($data['selesai_studi'] != null) {
                    $data['hasil_akhir'] = TubelHelper::getRef('/hasil_studi/lapor_diri/' . $data['selesai_studi']->id);
                }
                $data['ref_status_lapor_diri'] = TubelHelper::getRef('/status_lapor_diri');
                $data['ref_dokumen_penelitian'] = TubelHelper::getRef('/tipe_dok_penelitian');
                $data['ref_tahap_belum_lulus'] = TubelHelper::getRef('/tahap_belum_lulus');

                break;
        }

        #get data tubel pegawai
        $data['tubel'] = TubelHelper::getRef('/tubel/' . $id);
        $data['perguruantinggi'] = TubelHelper::getRef('/perguruan_tinggi_peserta/tubel/' . $id);
        $data['countperguruantinggi'] = count(TubelHelper::getRef('/perguruan_tinggi_peserta/tubel/' . $id));

        #get referensi
        $data['ref_lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');
        $data['ref_perguruantinggi'] = TubelHelper::getRef('/perguruan_tinggi');
        $data['ref_negara'] = TubelHelper::getRef('/negara_perguruan_tinggi');
        $data['ref_prodi'] = TubelHelper::getRef('/program_studi');

        #dddd
        // dd($data);

        return view('tubel.administrasi.detail', $data);
    }

    public function updateTubel($id)
    {
        #get menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/administrasi');

        #get data
        $response = TubelHelper::get('/tubel/' . $id);
        $data['tubel'] = $response['data'];

        #get referensi
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');

        return view('tubel.administrasi.detail_tubel_update', $data);
    }

    public function updateTubelAction(Request $request, $id)
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/administrasi');

        #split data kantor
        $kantorAsal = $request->kantorAsal;
        $resultKantorAsal = explode('|', $kantorAsal);

        #get id, kantor
        $kantorIdAsal = $resultKantorAsal[0];
        $namaKantorAsal = $resultKantorAsal[1];

        #split data kantor
        $jabatanAsal = $request->jabatanAsal;
        $resultJabatanAsal = explode('|', $jabatanAsal);

        #get id, jabatan
        $jabatanIdAsal = $resultJabatanAsal[0];
        $namaJabatanAsal = $resultJabatanAsal[1];

        $response = TubelHelper::patch('/tubel/' . $id, [
            "pegawaiInputId" => $request->pegawaiInputId,
            "lamaPendidikan" => $request->lamaPendidikan,
            "totalSemester" => $request->totalSemester,
            "nomorSt" => $request->nomorSt,
            "tglSt" => $request->tglSt,
            "tglMulaiSt" => $request->tglMulaiSt,
            "tglSelesaiSt" => $request->tglSelesaiSt,
            "nomorKepPembebasan" => $request->nomorKepPembebasan,
            "tglKepPembebasan" => $request->tglKepPembebasan,
            "tmtKepPembebasan" => $request->tmtKepPembebasan,
            "emailNonPajak" => $request->emailNonPajak,
            "noHandphone" => $request->noHandphone,
            "alamatTinggal" => $request->alamatTinggal,
            "lokasiPendidikanId" => $request->lokasiPendidikanId,
            "flagStan" => $request->fgstan,
            "programBeasiswa" => $request->programBeasiswa,
            "jenjangPendidikanId" => $request->jenjangPendidikanId,
            "kantorIdAsal" => $kantorIdAsal,
            "namaKantorAsal" => $namaKantorAsal,
            "jabatanIdAsal" => $jabatanIdAsal,
            "namaJabatanAsal" => $namaJabatanAsal
        ]);

        if ($response['status'] == 1) {
            return redirect('/tubel/administrasi/' . $id)->with('success', 'Data berhasil diupdate');
        } else {
            return redirect()->back()->with('error', 'Data gagal di tambahkan');
        }
    }

    public function generateLps($idlps)
    {
        $response = Http::post(env('API_URL_SDM0362') . '/laporan/generate/laporan_perkembangan_studi?id%20LPS=' . $idlps);

        #tmp direktori
        $tmp_direktori = tempnam(sys_get_temp_dir(), $idlps);

        #get content header disposition
        $headers = ['accept: ' . $response->headers()['Content-Type'][0]];

        #get file name
        $fraw = explode(';', $response->headers()['Content-Disposition'][0]);
        $fraw2 = explode('=', $fraw[1]);
        $namafile = str_replace('"', "", $fraw2[1]);

        #custom nama file
        // $namafile = session('user')->nip9 . '_LPS.docx';

        #put content
        file_put_contents($tmp_direktori, $response->body());

        return response()->download($tmp_direktori, $namafile, $headers);
    }

    public function generateFormLps($id)
    {
        $response = Http::get(env('API_URL_SDM0362') . '/lapor_selesai/surat/laporan?Id%20Laporan=' . $id);

        #tmp direktori
        $tmp_direktori = tempnam(sys_get_temp_dir(), $id);

        #get content header disposition
        $headers = ['accept: ' . $response->headers()['Content-Type'][0]];

        #get file name
        $fraw = explode(';', $response->headers()['Content-Disposition'][0]);
        $fraw2 = explode('=', $fraw[1]);
        $namafile = str_replace('"', "", $fraw2[1]);

        #custom nama file
        // $namafile = session('user')->nip9 . '_LPS.docx';

        #put content
        file_put_contents($tmp_direktori, $response->body());

        return response()->download($tmp_direktori, $namafile, $headers);
    }

    public function getPerguruanTinggi($id)
    {
        $response = TubelHelper::getRef('/perguruan_tinggi_peserta/' . $id);

        return response()->json($response);
    }

    public function getHasilAkhir(request $request)
    {

        $response = TubelHelper::getRef('/hasil_studi/lapor_diri/' . $request->id);

        return response()->json($response);
    }

    public function addPerguruanTinggi(Request $request, $idtubel)
    {
        $response = TubelHelper::post('/perguruan_tinggi_peserta/tubel/' . $idtubel, [
            'pegawaiInputId' => session()->get('user')['pegawaiId'],
            'dataIndukTubelId' => $idtubel,
            'perguruanTinggiId' => $request->pt,
            'tglMulai' => $request->tglMulai,
            'programStudiId' => $request->prodi,
            'negaraPtId' => $request->negara,
        ]);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasil di tambahkan');
        } else {
            return redirect()->back()->with('error', 'Data gagal di tambahkan');
        }
    }

    public function updatePerguruanTinggi(Request $request)
    {
        $response = TubelHelper::patch('/perguruan_tinggi_peserta/' . $request->idPerguruanTinggi, [
            'pegawaiInputId' => session()->get('user')['pegawaiId'],
            'dataIndukTubelId' => $request->idTubel,
            'perguruanTinggiId' => $request->perguruan_tinggi,
            'tglMulai' => $request->tgl_mulai_pendidikan,
            'programStudiId' => $request->prodi,
            'negaraPtId' => $request->negara
        ]);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasil di update');
        } else {
            return redirect()->back()->with('error', 'Data gagal di update');
        }
    }

    public function deletePerguruanTinggi($id)
    {
        $response = TubelHelper::delete('/perguruan_tinggi_peserta/' . $id, [
            'pegawaiInputId' => session()->get('user')['pegawaiId']
        ]);

        return response()->json($response['data']);
    }

    public function updateStatusLps(Request $request)
    {

        $caseOutputId = $request->caseOutputId;

        $id = $request->id;

        $isi = ['keterangan' => $request->keterangan];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        $response = TubelHelper::post('/laporan/' . $id . '/proses/' . $caseOutputId, $body);

        return response()->json($response);
    }

    public function addLps(Request $request, $id)
    {
        $isi = [
            'semesterKe' => $request->semesterke,
            'ipk' => $request->ipk,
            'jumlahSks' => $request->jumlah_sks,
            'flagSemester' => $request->semester,
            'pathKhs' => "",
            'pathLps' => "",
            'tahunAkademik' => $request->tahun,
            'ptPesertaTubelId' => $request->perguruantinggi
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/laporan/tubel/' . $id, $body);

        if ($response['status'] == 1) {
            return redirect('tubel/administrasi/' . $id)->with('success', 'Data Berhasil ditambah!');
        } else {
            return redirect('tubel/administrasi/' . $id)->with('error', $response['message']);
        }
    }

    public function updateLps(Request $request)
    {
        $isi = [
            'semesterKe' => (int) $request->semesterke,
            'ipk' => $request->ipk,
            'jumlahSks' => $request->jumlah_sks,
            'flagSemester' => $request->fg_semester,
            'tahunAkademik' => $request->tahun,
            'ptPesertaTubelId' => $request->perguruantinggi
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::patch('/laporan/' . $request->idLps, $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data Berhasil di update');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function deleteLps(Request $request)
    {
        #get lps
        $lps = TubelHelper::get('/laporan/' . $request->id);

        #delete file
        if ($lps['status'] == 1) {
            TubelHelper::deleteFile('/files/' . $lps['data']->pathKhs);
            TubelHelper::deleteFile('/files/' . $lps['data']->pathLps);
        } else {
            return ['status' => 0, 'message' => 'Gagal mendapatkan data'];
        }

        #delete data
        $response = TubelHelper::delete('/laporan/' . $request->id, [
            'pegawaiInputId' => session()->get('user')['pegawaiId']
        ]);

        return response()->json($response);
    }

    public function uploadKhs(Request $request)
    {
        #set validasi
        $file = $request->file('filekhs'); #file
        $size = 2097152; #2MB
        $mime = ['application/pdf']; #pdf

        #request
        if (!$request->file('filekhs')) {
            return ['status' => 0, 'message' => 'Tidak ada file yg diupload!'];
        } else {

            #upload data
            $response = TubelHelper::upload($file, $size, $mime);

            return response()->json($response);
        }
    }

    private function upload($file)
    {
        $filepath = $file->getRealPath();
        $nama = $file->getClientOriginalName();
        $mime = $file->getMimeType();
        $ukuran = $file->getSize();

        // validasi mime file
        if ($mime != 'application/pdf') {
            return ['status' => 0, 'message' => 'Format file bukan .pdf'];
        }

        // validasi ukuran 2mb
        if ($ukuran > 2097152) {
            return ['status' => 0, 'message' => 'Ukuran file lebih dari 2 MB'];
        }

        $response = Http::attach('file', file_get_contents($filepath), $nama)
            ->put(env('API_URL_SDM0362') . '/files');

        if (!$response->successful()) {
            $message = $response->body();

            $retUpload = ['status' => 0, 'message' => $message];
        } else {
            $retUpload = ['status' => 1, 'file' => $response->body()];
        }

        #return ['status' => 1, 'body' => $response->body()];
        return ($retUpload);
    }

    public function updateKhsFile(Request $request)
    {
        $isi = [
            'pathKhs' => $request->file_baru,
        ];

        $response = TubelHelper::patch('/laporan/files/' . $request->id, $isi);

        #delete file lama
        if (!empty($request->file_lama)) {
            TubelHelper::deleteFile('/files/' . $request->file_lama);
        }

        return response()->json($response);
    }

    public function uploadLps(Request $request)
    {
        #set validasi
        $file = $request->file('filelps'); #file
        $size = 2097152; #2MB
        $mime = ['application/pdf']; #pdf

        #request
        if (!$request->file('filelps')) {
            return ['status' => 0, 'message' => 'Tidak ada file yg diupload!'];
        } else {

            #upload data
            $response = TubelHelper::upload($file, $size, $mime);

            return response()->json($response);
        }
    }

    public function updateLpsFile(Request $request)
    {


        $isi = [
            'pathLps' => $request->file_baru,
        ];

        $response = TubelHelper::patch('/laporan/files/' . $request->id, $isi);

        #delete file lama
        TubelHelper::deleteFile('/files/' . $request->file_lama);

        return response()->json($response);
    }

    public function kirimLps(Request $request)
    {
        # cek lampiran
        $response = TubelHelper::get('/laporan/' . $request->id);

        if ($response['data']->pathKhs == null || $response['data']->pathLps == null) {
            return response()->json(['status' => 0, 'message' => 'Dokumen belum diupload']);
        }

        # next case = Laporan dikirim
        $outputid = '14620ceb-ec11-4f5e-8ac5-d453d7a213fc';

        $isi = [
            'keterangan' => null,
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        $response = TubelHelper::post('/laporan/' . $request->id . '/proses/' . $outputid, $body);

        return response()->json($response);
    }

    public function addCutiBelajar(Request $request)
    {
        $file_cuti = $this->upload($request->file_cuti);

        if ($file_cuti['status'] == 0) {
            return redirect()->back()->with('error', $file_cuti['message']);
        }

        $file_sponsor = $this->upload($request->file_sponsors);

        if ($file_sponsor['status'] == 0) {
            return redirect()->back()->with('error', $file_sponsor['message']);
        }

        $isi = [
            'alasanCuti' => $request->alasan_cuti,
            'tglMulaiCuti' => $request->tglMulaiSt,
            'tglSelesaiCuti' => $request->tglSelesaiSt,
            'nomorSuratKampus' => $request->surat_izin_cuti,
            'tglSuratKampus' => $request->tglSurat,
            'nomorSuratSponsor' => $request->surat_izin_cuti_sponsor,
            'tglSuratKepSponsor' => $request->tglSuratSponsor,
            'pathSuratIjinCutiKampus' => $file_cuti['file'],
            'pathSuratIjinCutiSponsor' => $file_sponsor['file'],
            'dataIndukTubelId' => $request->idTubel
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/cuti_belajar/pegawai', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data Berhasil ditambah!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function updateCutiBelajar(Request $request)
    {
        #upload file cuti
        if ($request->file_cuti != null) {  #jika update file
            $file_cuti = $this->upload($request->file_cuti);

            #delete file lama
            TubelHelper::deleteFile('/files/' . $request->file_cuti_lama);

            #validasi
            if ($file_cuti['status'] == 0) {
                return redirect()->back()->with('error', 'Gagal upload file kampus');
            }
        } else { #jika tidak
            $file_cuti['file'] = $request->file_cuti_lama;
        }

        #upload file sponsor
        if ($request->file_sponsors != null) {  #jika update file
            $file_sponsor = $this->upload($request->file_sponsors);

            #delete file lama
            TubelHelper::deleteFile('/files/' . $request->file_sponsors_lama);

            #validasi
            if ($file_sponsor['status'] == 0) {
                return redirect()->back()->with('error', 'Gagal upload file sponsor');
            }
        } else { #jika tidak
            $file_sponsor['file'] = $request->file_sponsors_lama;
        }

        $isi = [
            'alasanCuti' => $request->alasan_cuti,
            'tglMulaiCuti' => $request->tglMulaiSt,
            'tglSelesaiCuti' => $request->tglSelesaiSt,
            'nomorSuratKampus' => $request->surat_izin_cuti,
            'tglSuratKampus' => $request->tglSurat,
            'nomorSuratSponsor' => $request->surat_izin_cuti_sponsor,
            'tglSuratKepSponsor' => $request->tglSuratSponsor,
            'pathSuratIjinCutiKampus' => $file_cuti['file'],
            'pathSuratIjinCutiSponsor' => $file_sponsor['file'],
            'dataIndukTubelId' => $request->idTubel
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::patch('/cuti_belajar/' . $request->id_cuti, $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasil diupdate!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function deleteCutiBelajar(Request $request)
    {
        #get data
        $data = TubelHelper::get('/cuti_belajar/' . $request->id);

        #delete file
        if ($data['status'] == 1) {
            TubelHelper::deleteFile('/files/' . $data['data']->pathSuratIjinCutiKampus);
            TubelHelper::deleteFile('/files/' . $data['data']->pathSuratIjinCutiSponsor);
        } else {
            return ['status' => 0, 'message' => 'Gagal mendapatkan data'];
        }

        #delete data
        $body = TubelHelper::generateBody(session()->get('user'));
        $response = TubelHelper::delete('/cuti_belajar/' . $request->id, $body);

        return response()->json($response);
    }

    public function addCutiBelajarAdmin(Request $request)
    {

        $file_cuti = $this->upload($request->file_cuti_rekam);

        if ($file_cuti['status'] == 0) {
            return redirect()->back()->with('error', 'Gagal upload file kampus');
        }

        $file_sponsor = $this->upload($request->file_sponsors_rekam);

        if ($file_sponsor['status'] == 0) {
            return redirect()->back()->with('error', 'Gagal upload file sponsor');
        }


        $isi = [
            'alasanCuti' => $request->alasan_cuti_rekam,
            'tglMulaiCuti' => $request->tgl_mulai_cuti_rekam,
            'tglSelesaiCuti' => $request->tgl_selesai_cuti_rekam,
            'nomorSuratKampus' => $request->surat_izin_cuti_kampus_rekam,
            'tglSuratKampus' => $request->tgl_surat_kampus_rekam,
            'nomorSuratSponsor' => $request->surat_izin_cuti_sponsor_rekam,
            'tglSuratKepSponsor' => $request->tgl_surat_sponsor_rekam,
            'pathSuratIjinCutiKampus' => $file_cuti['file'],
            'pathSuratIjinCutiSponsor' => $file_sponsor['file'],
            'dataIndukTubelId' => $request->idTubel
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/cuti_belajar/admin', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data Berhasil ditambah!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function updateCutiBelajarAdmin(Request $request)
    {

        #upload file cuti kampus
        if ($request->file_cuti_edit != null) {  #jika update file
            $file_cuti = $this->upload($request->file_cuti_edit);

            #delete file lama
            TubelHelper::deleteFile('/files/' . $request->file_cuti_edit_lama);

            #validasi
            if ($file_cuti['status'] == 0) {
                return redirect()->back()->with('error', 'Gagal upload file cuti kampus');
            }
        } else { #jika tidak
            $file_cuti['file'] = $request->file_cuti_edit_lama;
        }

        #upload file cuti sponsor
        if ($request->file_sponsors_edit != null) {  #jika update file
            $file_sponsor = $this->upload($request->file_sponsors_edit);

            #delete file lama
            TubelHelper::deleteFile('/files/' . $request->file_sponsors_edit_lama);

            #validasi
            if ($file_sponsor['status'] == 0) {
                return redirect()->back()->with('error', 'Gagal upload file cuti sponsor');
            }
        } else { #jika tidak
            $file_sponsor['file'] = $request->file_sponsors_edit_lama;
        }

        $isi = [
            'alasanCuti' => $request->alasan_cuti_edit,
            'tglMulaiCuti' => $request->tgl_mulai_cuti_edit,
            'tglSelesaiCuti' => $request->tgl_selesai_cuti_edit,
            'nomorSuratKampus' => $request->surat_izin_cuti_kampus_edit,
            'tglSuratKampus' => $request->tgl_surat_kampus_edit,
            'nomorSuratSponsor' => $request->surat_izin_cuti_sponsor_edit,
            'tglSuratKepSponsor' => $request->tgl_surat_sponsor_edit,
            'pathSuratIjinCutiKampus' => $file_cuti['file'],
            'pathSuratIjinCutiSponsor' => $file_sponsor['file'],
            'dataIndukTubelId' => $request->id_tubel_edit
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::patch('/cuti_belajar/' . $request->id_cuti_edit, $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasil diupdate!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function updateStatusCutiBelajar(Request $request)
    {
        $caseOutputId = $request->caseOutputId;

        $id = $request->id;

        $isi = ['keterangan' => $request->keterangan];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        $response = TubelHelper::post('/cuti_belajar/' . $id . '/proses/' . $caseOutputId, $body);

        return response()->json($response);
    }

    public function addPerpanjangCutiPegawai(Request $request)
    {
        $file_cuti = $this->upload($request->file_cuti_perpanjang);

        if ($file_cuti['status'] == 0) {
            return redirect()->back()->with('error', 'Gagal upload file surat izin cuti kampus');
        }

        $file_sponsor = $this->upload($request->file_sponsors_perpanjang);

        if ($file_sponsor['status'] == 0) {
            return redirect()->back()->with('error', 'Gagal upload file surat izin cuti sponsor');
        }

        $isi = [
            'alasanCuti' => $request->alasan_cuti_perpanjang,
            'tglMulaiCuti' => $request->tgl_mulai_perpanjang,
            'tglSelesaiCuti' => $request->tgl_selesai_perpanjang,
            'nomorSuratKampus' => $request->surat_izin_cuti_perpanjang,
            'tglSuratKampus' => $request->tgl_surat_perpanjang,
            'nomorSuratSponsor' => $request->surat_izin_cuti_sponsor_perpanjang,
            'tglSuratKepSponsor' => $request->tgl_surat_sponsor_perpanjang,
            'pathSuratIjinCutiKampus' => $file_cuti['file'],
            'pathSuratIjinCutiSponsor' => $file_sponsor['file'],
            'dataIndukTubelId' => $request->idTubel
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/cuti_belajar_perpanjangan/pegawai/cuti_utama/' . $request->idCuti, $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data Berhasil ditambah!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function updateStatusCuti(Request $request)
    {

        $isi = ['keterangan' => $request->keterangan];
        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/cuti_belajar/' . $request->id . '/proses/' . $request->caseOutputId, $body);

        return response()->json($response);
    }

    public function addPengaktifanKembali(request $request)
    {
        $file_pengaktifan = $this->upload($request->file_pengaktifan);

        if ($file_pengaktifan['status'] == 0) {
            return redirect()->back()->with('error', $file_pengaktifan['message']);
        }

        $isi = [
            'tglMulaiAktif' => $request->tglMulaiAktif,
            'nomorSuratPengaktifanKembaliKampus' => $request->suratAktifKembali,
            'tglSuratPengaktifanKembaliKampus' => $request->tglSuratPengaktifan,
            'pathSuratPengaktifanKembaliKampus' => $file_pengaktifan['file'],
            'dataIndukTubelId' => $request->idTubel
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/pengaktifan_kembali/', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data Berhasil ditambah!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function updatePermohonanAktif(Request $request)
    {
        #update file permohonan aktif
        if ($request->file_pengaktifan_edit != null) {  #jika update file
            $file_pengaktifan = $this->upload($request->file_pengaktifan_edit);

            #delete file lama
            TubelHelper::deleteFile('/files/' . $request->file_pengaktifan_edit_lama);

            #validasi
            if ($file_pengaktifan['status'] == 0) {
                return redirect()->back()->with('error', 'Gagal upload file persetujuan kampus');
            }
        } else { #jika tidak
            $file_pengaktifan['file'] = $request->file_pengaktifan_edit_lama;
        }

        $isi = [
            'tglMulaiAktif' => $request->tglMulaiAktif,
            'nomorSuratPengaktifanKembaliKampus' => $request->suratAktifKembaliEdit,
            'tglSuratPengaktifanKembaliKampus' => $request->tglSuratPengaktifanEdit,
            'pathSuratPengaktifanKembaliKampus' => $file_pengaktifan['file'],
            'dataIndukTubelId' => $request->idTubel
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::patch('/pengaktifan_kembali/' . $request->id_permohonan_aktif, $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data Berhasil diupdate!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function deletePermohonanAktif(Request $request)
    {
        #get data
        $data = TubelHelper::get('/pengaktifan_kembali/' . $request->id);

        #delete file
        if ($data['status'] == 1) {
            TubelHelper::deleteFile('/files/' . $data['data']->pathSuratPengaktifanKembaliKampus);
        } else {
            return ['status' => 0, 'message' => 'Gagal mendapatkan data'];
        }

        $response = TubelHelper::delete('/pengaktifan_kembali/' . $request->id, [
            'pegawaiInputId' => session()->get('user')['pegawaiId']
        ]);

        return response()->json($response);
    }

    public function kirimPermohonanAktif(Request $request)
    {
        $isi = [
            'pegawaiInputId' => session()->get('user')['pegawaiId'],
            'keterangan' => null
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        $response = TubelHelper::post('/pengaktifan_kembali/' . $request->id . '/kirim', $body);

        return response()->json($response);
    }

    public function rekamPermohonanAktif(Request $request)
    {
        $file_permohonan_aktif = $this->upload($request->file_surat_tugas_rekam);

        if ($file_permohonan_aktif['status'] == 0) {
            return redirect()->back()->with('error', $file_permohonan_aktif['message']);
        }

        $isi = [
            'nomorSt' => $request->nomor_surat_tugas_rekam,
            'tanggalSt' => $request->tgl_surat_tugas_rekam,
            'tglMulaiSt' => $request->tgl_mulai_rekam,
            'tglSelesaiSt' => $request->tgl_selesai_rekam,
            'pathSt' => $file_permohonan_aktif['file'],
            'keterangan' => null
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/pengaktifan_kembali/' . $request->id . '/surat_tugas', $body);

        return response()->json($response);
    }

    public function addPerpanjanganStTubel(request $request)
    {

        $file_st_awal = $this->upload($request->file_st_awal_add);
        $file_surat_persetujuan_kampus = $this->upload($request->file_surat_persetujuan_kampus_add);
        $file_surat_persetujuan_sponsor = $this->upload($request->file_surat_persetujuan_sponsor_add);

        # file st awal
        if ($file_st_awal['status'] == 0) {
            return redirect()->back()->with('error', $file_st_awal['message']);
        }

        # file surat persetujuan kampus
        if ($file_surat_persetujuan_kampus['status'] == 0) {
            return redirect()->back()->with('error', $file_surat_persetujuan_kampus['message']);
        }

        # file surat persetujuan sponsor
        if ($file_surat_persetujuan_sponsor['status'] == 0) {
            return redirect()->back()->with('error', $file_surat_persetujuan_sponsor['message']);
        }

        $isi = [
            'alasanPerpanjangan' => $request->alasan_perpanjangan_st,
            'tahapanStudi' => $request->tahapan_studi_add,
            'tglSelesaiPerpanjangan' => $request->tgl_selesai_perpanjangan_add,
            'noStAwal' => $request->st_awal_add,
            'pathStAwal' => $file_st_awal['file'],
            'nomorPersetujuanKampus' => $request->surat_persetujuan_kampus_add,
            'tglPersetujuanKampus' => $request->tgl_surat_persetujuan_kampus_add,
            'pathPersetujuanKampus' => $file_surat_persetujuan_kampus['file'],
            'nomorPersetujuanSponsor' => $request->surat_persetujuan_sponsor_add,
            'tglPersetujuanSponsor' => $request->tgl_surat_persetujuan_sponsor_add,
            'pathPersetujuanSponsor' => $file_surat_persetujuan_sponsor['file']
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/perpanjangan_st/' . $request->idTubel, $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data Berhasil ditambah!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function updatePerpanjanganStTubel(request $request)
    {

        #featured backend delete file

        #file st awal
        $file_st_awal = $this->upload($request->file_st_awal_edit);

        #validasi
        if ($file_st_awal['status'] == 0) {
            return redirect()->back()->with('error', 'Gagal upload file ST Awal');
        }

        # file surat persetujuan kampus
        $file_surat_persetujuan_kampus = $this->upload($request->file_surat_persetujuan_kampus_edit);

        #validasi
        if ($file_surat_persetujuan_kampus['status'] == 0) {
            return redirect()->back()->with('error', 'Gagal upload file persetujuan kampus');
        }

        # file surat persetujuan sponsor
        $file_surat_persetujuan_sponsor = $this->upload($request->file_surat_persetujuan_sponsor_edit);

        #validasi
        if ($file_surat_persetujuan_sponsor['status'] == 0) {
            return redirect()->back()->with('error', 'Gagal upload file persetujuan sponsor');
        }

        #without featured backend delete file

        // if ($request->file_st_awal_edit != null) { #jika update file
        //     $file_st_awal = $this->upload($request->file_st_awal_edit);

        //     #validasi
        //     if ($file_st_awal['status'] == 0) {
        //         return redirect()->back()->with('error', 'Gagal upload file ST Awal');
        //     }
        // } else { #jika tidak
        //     $file_st_awal['file'] = $request->file_st_awal_lama;
        // }


        // if ($request->file_surat_persetujuan_kampus_edit != null) {  #jika update file
        //     $file_surat_persetujuan_kampus = $this->upload($request->file_surat_persetujuan_kampus_edit);

        //     #validasi
        //     if ($file_surat_persetujuan_kampus['status'] == 0) {
        //         return redirect()->back()->with('error', 'Gagal upload file persetujuan kampus');
        //     }
        // } else { #jika tidak
        //     $file_surat_persetujuan_kampus['file'] = $request->file_surat_persetujuan_kampus_lama;
        // }

        // if ($request->file_surat_persetujuan_sponsor_edit != null) {  #jika update file
        //     $file_surat_persetujuan_sponsor = $this->upload($request->file_surat_persetujuan_sponsor_edit);

        //     #validasi
        //     if ($file_surat_persetujuan_sponsor['status'] == 0) {
        //         return redirect()->back()->with('error', 'Gagal upload file persetujuan sponsor');
        //     }
        // } else { #jika tidak
        //     $file_surat_persetujuan_sponsor['file'] = $request->file_surat_persetujuan_sponsor_lama;
        // }

        $isi = [
            'alasanPerpanjangan' => $request->alasan_perpanjangan_st_edit,
            'tahapanStudi' => $request->tahapan_studi_edit,
            'tglSelesaiPerpanjangan' => $request->tgl_selesai_perpanjangan_edit,
            'noStAwal' => $request->st_awal_edit,
            'pathStAwal' => $file_st_awal['file'],
            'nomorPersetujuanKampus' => $request->surat_persetujuan_kampus_edit,
            'tglPersetujuanKampus' => $request->tgl_surat_persetujuan_kampus_edit,
            'pathPersetujuanKampus' => $file_surat_persetujuan_kampus['file'],
            'nomorPersetujuanSponsor' => $request->surat_persetujuan_sponsor_edit,
            'tglPersetujuanSponsor' => $request->tgl_surat_persetujuan_sponsor_edit,
            'pathPersetujuanSponsor' => $file_surat_persetujuan_sponsor['file']
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::patch('/perpanjangan_st/' . $request->id_perpanjangan_st, $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data Berhasil diupdate!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function deletePerpanjanganStTubel(Request $request)
    {
        $response = TubelHelper::delete('/perpanjangan_st/' . $request->id, [
            'pegawaiInputId' => session()->get('user')['pegawaiId']
        ]);

        return response()->json($response);
    }

    public function kirimPerpanjanganStTubel(request $request)
    {
        $isi = [
            'keterangan' => null,
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/perpanjangan_st/' . $request->id . '/kirim', $body);

        return response()->json($response);
    }

    public function addUsulanTopikRiset(Request $request)
    {

        #set validasi
        $file = $request->file('file_dokumen_proposal_add'); #file
        $size = 2097152; #2MB
        $mime = ['application/pdf']; #pdf

        #upload file
        $response_file = TubelHelper::upload($file, $size, $mime);

        if ($response_file['status'] == 1) {
            #set body
            $isi = [
                'topikId' => $request->topik_riset_add,
                'namaRiset' => $request->judul_riset_add,
                'pathProposal' => $response_file['file']
            ];

            #generate body
            $body = TubelHelper::generateBody(session()->get('user'), $isi);

            #send to api
            $response = TubelHelper::post('/usulan_topik_riset/tubel/' . $request->id_tubel, $body);

            if ($response['status'] == 1) {
                return redirect()->back()->with('success', 'Data berhasil disimpan');
            } else {
                return redirect()->back()->with('error', $response['message']);
            }
        } else {
            return redirect()->back()->with('error', $response_file['message']);
        }
    }

    public function updateUsulanTopikRiset(Request $request)
    {
        #upload file
        if ($request->file_dokumen_proposal_edit) {  #jika ada file

            #set validasi
            $file = $request->file('file_dokumen_proposal_edit'); #file
            $size = 2097152; #2MB
            $mime = ['application/pdf']; #pdf

            #upload file
            $response_file = TubelHelper::upload($file, $size, $mime);

            #validasi
            if ($response_file['status'] == 0) {
                return redirect()->back()->with('error', 'Gagal upload file dokumen proposal');
            }
        } else { #jika tidak
            $response_file['file'] = $request->file_dokumen_proposal_lama;
        }

        #set body
        $isi = [
            'topikId' => $request->pilihan_topik_edit,
            'namaRiset' => $request->judul_riset_edit,
            'pathProposal' => $response_file['file']
        ];

        #generate body
        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        #send to api
        $response = TubelHelper::patch('/usulan_topik_riset/' . $request->id_usulan_riset_edit, $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasil diupdate');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function kirimUsulanTopikRiset(request $request)
    {

        #get request data
        $caseIdSelanjutnya = $request->act;

        // $caseIdSelanjutnya = 'dea0e97e-308d-416f-942e-38874c108bcd'; #Usulan topik dikirim
        // $caseIdSelanjutnya = '72b47eed-7ad2-4dca-bf5d-f3d05727c719'; #Persetujuan PT dikirim

        $isi = [
            'keterangan' => null,
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        #send data
        if ($caseIdSelanjutnya == 'dea0e97e-308d-416f-942e-38874c108bcd') {
            $response = TubelHelper::post('/usulan_topik_riset/' . $request->id . '/kirim', $body);
        } elseif ($caseIdSelanjutnya == '72b47eed-7ad2-4dca-bf5d-f3d05727c719') {
            $response = TubelHelper::post('/usulan_topik_riset/' . $request->id . '/kirim_persetujuan_pt', $body);
        }

        #return
        return response()->json($response);
    }

    public function persetujuanPtUsulanTopikRiset(request $request)
    {
        if ($request->persetujuan == 1) { #disetujui
            #upload file
            if ($request->file_dokumen_persetujuan_usulan_riset) {  #jika ada file

                #set validasi
                $file = $request->file('file_dokumen_persetujuan_usulan_riset'); #file
                $size = 2097152; #2MB
                $mime = ['application/pdf']; #pdf

                #upload file
                $response_file = TubelHelper::upload($file, $size, $mime);

                #validasi
                if ($response_file['status'] == 1) {
                    #set message
                    $message = 'data usulan berhasil disetujui';

                    #set post body
                    $isi = [
                        'keputusan' => 1,
                        'tglPersetujuanPt' => $request->tgl_persetujuan_usulan_riset,
                        'pathDokumenPersetujuanPt' => $response_file['file'],
                    ];
                } else {
                    return redirect()->back()->with('error', 'Gagal upload file dokumen proposal');
                }
            } else {
                return redirect()->back()->with('error', 'Tidak ada file yg diupload');
            }
        } elseif ($request->persetujuan == 0) { #ditolak
            #set message
            $message = 'data usulan berhasil ditolak';

            #set post body
            // // DUMMY
            // $isi = [
            //     'keputusan' => 0,
            //     'tglPersetujuanPt' => '2021',
            //     'pathDokumenPersetujuanPt' => "123.pdf",
            //     'alasanPenolakan' => $request->alasan_penolakan_usulan_riset
            // ];

            $isi = [
                'keputusan' => 0,
                'alasanPenolakan' => $request->alasan_penolakan_usulan_riset
            ];
        }

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/usulan_topik_riset/' . $request->id_usulan_topik_riset_setuju . '/persetujuan_pt', $body);

        #return
        if ($response['status'] == 1) {
            return redirect()->back()->with('success', $message);
        } else {
            return redirect()->back()->with('error', 'Data usulan gagal disahkan');
        }
    }

    public function deleteUsulanTopikRiset(Request $request)
    {
        $response = TubelHelper::delete('/usulan_topik_riset/' . $request->id, [
            'pegawaiInputId' => session()->get('user')['pegawaiId']
        ]);

        return response()->json($response);
    }

    public function selesaiStudiLulus(Request $request)
    {
        # set validasi
        $file_penelitian = $request->file('file_penelitian'); #file
        $file_lps = $request->file('file_lps'); #file
        $size = 2097152; #2MB
        $mime = ['application/pdf']; #pdf

        /* ================== UPDATE PERGURUAN TINGGI ================== */

        foreach ($request->pt as $key => $item) {

            # upload skl
            if (!empty($item['file_skl'])) {
                $response_file_skl = TubelHelper::upload($item['file_skl'], $size, $mime);
            } else {
                $response_file_skl['file'] = $item['file_skl_lama'] ?? null;
            }

            # upload ijazah
            if (!empty($item['file_ijazah'])) {
                $response_file_ijazah = TubelHelper::upload($item['file_ijazah'], $size, $mime);
            } else {
                $response_file_ijazah['file'] = $item['file_ijazah_lama'] ?? null;
            }

            # upload transkrip
            if (!empty($item['file_transkrip'])) {
                $response_file_transkrip = TubelHelper::upload($item['file_transkrip'], $size, $mime);
            } else {
                $response_file_transkrip['file'] = $item['file_transkrip_lama'] ?? null;
            }

            $isi = [
                'nomorSkl' => $item['nomor_skl'],
                'tglSkl' => $item['tgl_skl'],
                'pathSkl' => $response_file_skl['file'],
                'nomorIjazah' => $item['nomor_ijazah'],
                'tglIjazah' => $item['tgl_ijazah'],
                'pathIjazah' => $response_file_ijazah['file'],
                'ipk' => $item['ipk'],
                'nomorTranskripNilai' => $item['nomor_transkrip'],
                'tglTranskripNilai' => $item['tgl_transkrip'],
                'pathTranskripNilai' => $response_file_transkrip['file']
            ];

            $body = TubelHelper::generateBody(session()->get('user'), $isi);
            if (!empty($item['id'])) {
                $response = TubelHelper::post('/perguruan_tinggi_peserta/' . $item['id'] . '/ijazah_skl', $body);
            }
        }

        /* ====================== UPDATE DATA KELULUSAN ==================== */

        # file penelitian
        if (!empty($file_penelitian)) {
            $response_file_penelitian = TubelHelper::upload($file_penelitian, $size, $mime);

            # validasi
            if ($response_file_penelitian['status'] == 0) {
                return redirect()->back()->with('error', $response_file_penelitian['message']);
            }
        } else {
            $response_file_penelitian['file'] = $request->file_penelitian_lama;
        }

        # file laporan selesai studi
        if (!empty($file_lps)) {
            $response_file_lps = TubelHelper::upload($file_lps, $size, $mime);

            # validasi
            if ($response_file_lps['status'] == 0) {
                return redirect()->back()->with('error', $response_file_lps['message']);
            }
        } else {
            $response_file_lps['file'] = $request->file_lps_lama;
        }

        #set body
        $isi = [
            'tglKelulusan' => $request->tgl_kelulusan,
            'tipeDokPenelitian' => $request->opsi_dokumen_penelitian,
            'pathDokumenPenelitian' => $response_file_penelitian['file'],
            'pathLaporanSelesaiStudi' => $response_file_lps['file']
        ];

        #generate body
        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        #send to api
        $response = TubelHelper::post('/lapor_selesai/tubel/' . $request->id_tubel . '/lulus', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasil disimpan');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function selesaiStudiBelumLulus(Request $request)
    {
        # set validasi
        $file_timeline_studi_add = $request->file('file_timeline_studi_add'); #file
        $file_draft_dokumen_penelitian_add = $request->file('file_draft_dokumen_penelitian_add'); #file
        $size = 2097152; #2MB
        $mime = ['application/pdf']; #pdf

        # upload file 1: timeline studi
        if (!empty($file_timeline_studi_add)) {
            $response_file_timeline_studi_add = TubelHelper::upload($file_timeline_studi_add, $size, $mime);

            # validasi file 1: timeline studi
            if ($response_file_timeline_studi_add['status'] == 0) {
                return redirect()->back()->with('error', $response_file_timeline_studi_add['message']);
            }
        } else {
            # jika tidak update file
            $response_file_timeline_studi_add['file'] = $request->file_timeline_studi_lama;
        }

        # upload file 2: dokumen penelitian
        if (!empty($file_draft_dokumen_penelitian_add)) {
            $response_file_draft_dokumen_penelitian_add = TubelHelper::upload($file_draft_dokumen_penelitian_add, $size, $mime);

            # validasi file 2: dokumen penelitian
            if ($response_file_draft_dokumen_penelitian_add['status'] == 0) {
                return redirect()->back()->with('error', $response_file_draft_dokumen_penelitian_add['message']);
            }
        } else {
            # jika tidak update file
            $response_file_draft_dokumen_penelitian_add['file'] = $request->file_draft_dokumen_penelitian_lama;
        }

        #set body
        $isi = [
            'tahapBelumLulus' => $request->opsi_tahapan_studi,
            'tipeDokPenelitian' => $request->opsi_tahapan_studi,
            'pathDokumenPenelitian' => $response_file_draft_dokumen_penelitian_add['file'],
            'pathTimelineRencanaStudi' => $response_file_timeline_studi_add['file']
        ];

        #generate body
        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        #send to api
        $response = TubelHelper::post('/lapor_selesai/tubel/' . $request->id_tubel . '/belum_lulus', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasil disimpan');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function selesaiStudiMengundurkanDiri(Request $request)
    {
        # set validasi
        $file_dokumen_permohonan = $request->file('file_dokumen_permohonan'); #file
        $file_dokumen_surat_persetujuan = $request->file('file_dokumen_surat_persetujuan'); #file
        $size = 2097152; #2MB
        $mime = ['application/pdf']; #pdf

        # file surat permohonan
        if (!empty($file_dokumen_permohonan)) {
            $response_dokumen_permohonan = TubelHelper::upload($file_dokumen_permohonan, $size, $mime);

            # validasi file dokumen permohonan
            if ($response_dokumen_permohonan['status'] == 0) {
                return redirect()->back()->with('error', $response_dokumen_permohonan['message']);
            }
        } else {
            # jika tidak update file
            $response_dokumen_permohonan['file'] = $request->file_dokumen_permohonan_lama;
        }

        # file surat persetujuan
        if (!empty($file_dokumen_surat_persetujuan)) {
            # upload file dokumen persetujuan
            $response_dokumen_surat_persetujuan = TubelHelper::upload($file_dokumen_surat_persetujuan, $size, $mime);

            # validasi file dokumen persetujuan
            if ($response_dokumen_surat_persetujuan['status'] == 0) {
                return redirect()->back()->with('error', $response_dokumen_surat_persetujuan['message']);
            }
        } else {
            # jika tidak update file
            $response_dokumen_surat_persetujuan['file'] = $request->file_dokumen_surat_persetujuan_lama;
        }

        #set body
        $isi = [
            'alasanMundur' => $request->alasan_pengunduran_diri,
            'semesterKe' => $request->mundur_semester,
            'tahunAkademik' => $request->tahun_akademik,
            'pathSuratPermohonanMundur' => $response_dokumen_permohonan['file'],
            'noSuratPersetujuanMundur' => $request->no_surat_persetujuan,
            'tglSuratPersetujuanMundur' => $request->tgl_surat_persetujuan,
            'pathSuratPersetujuanMundur' => $response_dokumen_surat_persetujuan['file']
        ];

        #generate body
        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        #send to api
        $response = TubelHelper::post('/lapor_selesai/tubel/' . $request->id_tubel . '/mengundurkan_diri', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasil disimpan');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function selesaiStudiDropOut(Request $request)
    {
        # set validasi
        $file_suket = $request->file('file_surat_keterangan'); #file
        $size = 2097152; #2MB
        $mime = ['application/pdf']; #pdf

        # file
        if (!empty($file_suket)) {
            # upload file
            $response_file_suket = TubelHelper::upload($file_suket, $size, $mime);

            # validasi file
            if ($response_file_suket['status'] == 0) {
                return redirect()->back()->with('error', $response_file_suket['message']);
            }
        } else {
            # jika tidak update file
            $response_file_suket['file'] = $request->file_surat_keterangan_do_lama;
        }

        #set body
        $isi = [
            'semesterKe' => $request->semester_do,
            'tahunAkademik' => $request->tahun,
            'nomorSuratDo' => $request->no_surat_keterangan,
            'tglSuratDo' => $request->tgl_surat_keterangan,
            'pathSuratDo' => $response_file_suket['file']
        ];

        #generate body
        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        #send to api
        $response = TubelHelper::post('/lapor_selesai/tubel/' . $request->id_tubel . '/dropout', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasil disimpan');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function kirimLaporanSelesaiStudi(Request $request)
    {
        $isi = [
            'keterangan' => null,
        ];

        # generate body
        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        # send to api
        $response = TubelHelper::post('/lapor_selesai/' . $request->id . '/kirim', $body);

        return response()->json($response);
    }

    public function hasilStudiLulus(Request $request)
    {
        # set validasi
        $file_penelitian = $request->file('file_penelitian'); #file
        $file_lps = $request->file('file_lps'); #file
        $size = 2097152; #2MB
        $mime = ['application/pdf']; #pdf


        /* ================== UPDATE PERGURUAN TINGGI ================== */

        foreach ($request->pt as $key => $item) {

            # upload skl
            if (!empty($item['file_skl'])) {
                $response_file_skl = TubelHelper::upload($item['file_skl'], $size, $mime);
            } else {
                $response_file_skl['file'] = $item['file_skl_lama'] ?? null;
            }

            # upload ijazah
            if (!empty($item['file_ijazah'])) {
                $response_file_ijazah = TubelHelper::upload($item['file_ijazah'], $size, $mime);
            } else {
                $response_file_ijazah['file'] = $item['file_ijazah_lama'] ?? null;
            }

            # upload transkrip
            if (!empty($item['file_transkrip'])) {
                $response_file_transkrip = TubelHelper::upload($item['file_transkrip'], $size, $mime);
            } else {
                $response_file_transkrip['file'] = $item['file_transkrip_lama'] ?? null;
            }

            $isi = [
                'nomorSkl' => $item['nomor_skl'] ?? null,
                'tglSkl' => $item['tgl_skl'] ?? null,
                'pathSkl' => $response_file_skl['file'] ?? null,
                'nomorIjazah' => $item['nomor_ijazah'],
                'tglIjazah' => $item['tgl_ijazah'],
                'pathIjazah' => $response_file_ijazah['file'],
                'ipk' => $item['ipk'] ?? null,
                'nomorTranskripNilai' => $item['nomor_transkrip'] ?? null,
                'tglTranskripNilai' => $item['tgl_transkrip'] ?? null,
                'pathTranskripNilai' => $response_file_transkrip['file'] ?? null
            ];

            $body = TubelHelper::generateBody(session()->get('user'), $isi);
            if (!empty($item['id'])) {
                $response = TubelHelper::post('/perguruan_tinggi_peserta/' . $item['id'] . '/ijazah_skl', $body);
            }
        }

        /* ====================== UPDATE DATA KELULUSAN ==================== */

        # file penelitian
        if (!empty($file_penelitian)) {
            $response_file_penelitian = TubelHelper::upload($file_penelitian, $size, $mime);

            # validasi
            if ($response_file_penelitian['status'] == 0) {
                return redirect()->back()->with('error', $response_file_penelitian['message']);
            }
        } else {
            $response_file_penelitian['file'] = $request->file_penelitian_lama;
        }

        # file laporan selesai studi
        if (!empty($file_lps)) {
            $response_file_lps = TubelHelper::upload($file_lps, $size, $mime);

            # validasi
            if ($response_file_lps['status'] == 0) {
                return redirect()->back()->with('error', $response_file_lps['message']);
            }
        } else {
            $response_file_lps['file'] = $request->file_lps_lama;
        }

        #set body
        $isi = [
            'tglKelulusan' => $request->tgl_kelulusan,
            'tipeDokPenelitian' => $request->opsi_dokumen_penelitian,
            'pathDokumenPenelitian' => $request->file_penelitian_ijazah ?? $response_file_penelitian['file'],
            'pathLaporanSelesaiStudi' => $request->file_lps_ijazah ?? $response_file_lps['file']
        ];

        #generate body
        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        #send to api
        $response = TubelHelper::post('/lapor_selesai/tubel/' . $request->id_tubel . '/lulus', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasil disimpan');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function hasilStudiMengundurkanDiri(Request $request)
    {
        # set validasi
        $file_dokumen_permohonan = $request->file('file_dokumen_permohonan'); #file
        $file_dokumen_surat_persetujuan = $request->file('file_dokumen_surat_persetujuan'); #file
        $size = 2097152; #2MB
        $mime = ['application/pdf']; #pdf

        # upload file dokumen permohonan
        if (!empty($file_dokumen_permohonan)) {
            $response_dokumen_permohonan = TubelHelper::upload($file_dokumen_permohonan, $size, $mime);

            # validasi file dokumen permohonan
            if ($response_dokumen_permohonan['status'] == 0) {
                return redirect()->back()->with('error', $response_dokumen_permohonan['message']);
            }
        } else {
            # jika tidak update file
            $response_dokumen_permohonan['file'] = $request->file_dokumen_permohonan_lama;
        }

        # upload file dokumen persetujuan
        if (!empty($file_dokumen_surat_persetujuan)) {
            $response_dokumen_surat_persetujuan = TubelHelper::upload($file_dokumen_surat_persetujuan, $size, $mime);

            # validasi file dokumen persetujuan
            if ($response_dokumen_surat_persetujuan['status'] == 0) {
                return redirect()->back()->with('error', $response_dokumen_surat_persetujuan['message']);
            }
        } else {
            # jika tidak update file
            $response_dokumen_surat_persetujuan['file'] = $request->file_dokumen_surat_persetujuan_lama;
        }

        #set body
        $isi = [
            'alasanMundur' => $request->alasan_pengunduran_diri,
            'semesterKe' => $request->mundur_semester,
            'tahunAkademik' => $request->tahun_akademik,
            'pathSuratPermohonanMundur' => $response_dokumen_permohonan['file'],
            'noSuratPersetujuanMundur' => $request->no_surat_persetujuan,
            'tglSuratPersetujuanMundur' => $request->tgl_surat_persetujuan,
            'pathSuratPersetujuanMundur' => $response_dokumen_surat_persetujuan['file']
        ];

        #generate body
        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        #send to api
        $response = TubelHelper::post('/hasil_studi/tubel/' . $request->id_tubel . '/mengundurkan_diri', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasil disimpan');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function hasilStudiDropOut(Request $request)
    {

        # set validasi
        $file_suket = $request->file('file_surat_keterangan'); #file
        $size = 2097152; #2MB
        $mime = ['application/pdf']; #pdf

        # upload file
        $response_file_suket = TubelHelper::upload($file_suket, $size, $mime);

        # validasi file
        if ($response_file_suket['status'] == 0) {
            return redirect()->back()->with('error', $response_file_suket['message']);
        }

        #set body
        $isi = [
            'semesterKe' => (int) $request->semester_do,
            'tahunAkademik' => (int) $request->tahun,
            'nomorSuratDo' => $request->no_surat_keterangan,
            'tglSuratDo' => $request->tgl_surat_keterangan,
            'pathSuratDo' => $response_file_suket['file']
        ];

        #generate body
        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        #send to api
        $response = TubelHelper::post('/hasil_studi/tubel/' . $request->id_tubel . '/dropout', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasil disimpan');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function hasilStudiRekamIjazah(Request $request)
    {

        # set validasi
        $size = 2097152; #2MB
        $mime = ['application/pdf']; #pdf


        /* ================== UPDATE PERGURUAN TINGGI ================== */

        foreach ($request->pt as $key => $item) {

            # upload skl
            if (!empty($item['file_skl'])) {
                $response_file_skl = TubelHelper::upload($item['file_skl'], $size, $mime);
            } else {
                $response_file_skl['file'] = $item['file_skl_lama'] ?? null;
            }

            # upload ijazah
            if (!empty($item['file_ijazah'])) {
                $response_file_ijazah = TubelHelper::upload($item['file_ijazah'], $size, $mime);
            } else {
                $response_file_ijazah['file'] = $item['file_ijazah_lama'] ?? null;
            }

            # upload transkrip
            if (!empty($item['file_transkrip'])) {
                $response_file_transkrip = TubelHelper::upload($item['file_transkrip'], $size, $mime);
            } else {
                $response_file_transkrip['file'] = $item['file_transkrip_lama'] ?? null;
            }

            $isi = [
                'nomorSkl' => $item['nomor_skl'] ?? null,
                'tglSkl' => $item['tgl_skl'] ?? null,
                'pathSkl' => $response_file_skl['file'] ?? null,
                'nomorIjazah' => $item['nomor_ijazah'],
                'tglIjazah' => $item['tgl_ijazah'],
                'pathIjazah' => $response_file_ijazah['file'],
                'ipk' => $item['ipk'] ?? null,
                'nomorTranskripNilai' => $item['nomor_transkrip'] ?? null,
                'tglTranskripNilai' => $item['tgl_transkrip'] ?? null,
                'pathTranskripNilai' => $response_file_transkrip['file'] ?? null
            ];

            $body = TubelHelper::generateBody(session()->get('user'), $isi);
            if (!empty($key)) {
                $response = TubelHelper::post('/perguruan_tinggi_peserta/' . $key . '/ijazah_skl', $body);
            }
        }

        /* ====================== UPDATE DATA IJAZAH ==================== */

        #set body
        $isi = [
            'keterangan' => null,
        ];

        #generate body
        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        #send to api
        $response = TubelHelper::post('/hasil_studi/' . $request->id_hasil_studi . '/ijazah/kirim', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasil disimpan');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function hasilStudiKirimIjazah(Request $request)
    {
        $isi = [
            'keterangan' => null,
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/hasil_studi/' . $request->id . '/ijazah/kirim', $body);

        return response()->json($response);
    }

    public function kirimLaporanHasilStudi(Request $request)
    {
        $isi = [
            'keterangan' => null,
        ];

        # generate body
        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        # send to api
        $response = TubelHelper::post('/hasil_studi/' . $request->id . '/studi/kirim', $body);

        return response()->json($response);
    }
}
