<?php

namespace App\Http\Controllers\Tubel;

use App\Helpers\TubelHelper;
use App\Models\MenuModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class SelesaiTubelController extends Controller
{
    public function index(Request $request)
    {
        #list route
        $route = ['rekam-kep', 'ditempatkan'];

        #validate route
        if ($request->route == '' || in_array($request->route, $route)) {

            #get menu, tab
            $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/kep-penempatan');
            $data['tab'] = $request->route ? $request->route : 'rekam-kep';

            #set parameters
            $size = 10;
            $page = $request->page;
            $nama = $request->nama;
            $nip = $request->nip;
            $jenjang = $request->jenjang;
            $lokasi = $request->lokasi;
            $topik = $request->topik;
            $tiket = $request->tiket;

            #set api
            switch ($data['tab']) {
                case 'rekam-kep':
                    $data['selesaitubel'] = TubelHelper::getRef('/lapor_selesai/monitoring/penempatan?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&jenjang=' . $jenjang
                        . '&lokasi=' . $lokasi
                        . '&topik=' . $topik
                        . '&tiket=' . $tiket);
                    break;

                case 'ditempatkan':
                    $data['selesaitubel'] = TubelHelper::getRef('/lapor_selesai/monitoring/ditempatkan?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&jenjang=' . $jenjang
                        . '&lokasi=' . $lokasi
                        . '&topik=' . $topik
                        . '&tiket=' . $tiket);
                    break;
            }

            #pagination
            $data['totalItems'] = $data['selesaitubel']->totalItems;
            $data['totalPages'] = $data['selesaitubel']->totalPages;
            $data['currentPage'] = $data['selesaitubel']->currentPage;
            $data['numberOfElements'] = $data['selesaitubel']->numberOfElements;
            $data['size'] = $data['selesaitubel']->size;

            #referensi
            $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
            $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

            return view('tubel.keppenempatanselesaitubel.selesai_tubel_index', $data);
        } else {
            return abort(404);
        }
    }

    public function uploadKepPenempatan(Request $request)
    {
        # set validasi file
        $file_kep = $request->file('file_kep_upload'); #file
        $size = 2097152; #2MB
        $mime = ['application/pdf']; #pdf

        # upload file
        $file_kep = TubelHelper::upload($file_kep, $size, $mime);

        # validasi file
        if ($file_kep['status'] == 0) {
            return redirect()->back()->with('error', $file_kep['message']);
        }

        # get data kantor
        $kantor = explode('|', $request->lokasi_penempatan_upload);

        $isi = [
            'nomorSuratKepPenempatan' => $request->nomor_kep_upload,
            'tglSuratKepPenempatan' => $request->tgl_kep_upload,
            'tglTmtSuratKepPenempatan' => $request->tmt_kep_upload,
            'kdKantorPenempatan' => $kantor[0],
            'namaKantorPenempatan' => $kantor[1],
            'pathSuratKepPenempatan' => $file_kep['file']
        ];

        #generate body
        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        #send to api
        $response = TubelHelper::post('/lapor_selesai/' . $request->id . '/kep_penempatan', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasil disimpan');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }
}
