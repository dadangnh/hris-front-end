<?php

namespace App\Http\Controllers\Tubel;

use App\Helpers\TubelHelper;
use App\Models\MenuModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;

class PengaktifanKembaliController extends Controller
{
    public function getPengaktifanKembali()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/pengaktifan-kembali');
        $data['route'] = 'tubel/pengaktifan-kembali';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $nost = isset($_GET['nost']) ? $_GET['nost'] : null;
        $kep = isset($_GET['kep']) ? $_GET['kep'] : null;
        $tgl_mulai = isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : null;
        $tgl_selesai = isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : null;

        $data['tab'] = 'menunggu_persetujuan';
        $data['pengaktifan_kembali'] = TubelHelper::getRef('/pengaktifan_kembali/proses/persetujuan?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&tiket=' . $tiket
            . '&surat_tugas=' . $nost
            . '&kep_pembebasan=' . $kep
            . '&tanggal_mulai_aktif_awal=' . $tgl_selesai
            . '&tanggal_selesai_aktif_akhir=' . $tgl_mulai);

        #pagination
        $data['totalItems'] = $data['pengaktifan_kembali']->totalItems;
        $data['totalPages'] = $data['pengaktifan_kembali']->totalPages;
        $data['currentPage'] = $data['pengaktifan_kembali']->currentPage;
        $data['numberOfElements'] = $data['pengaktifan_kembali']->numberOfElements;
        $data['size'] = $data['pengaktifan_kembali']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.pengaktifankembali.pengaktifankembali_index', $data);
    }

    public function getPengaktifanKembaliSetuju()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/pengaktifan-kembali');
        $data['route'] = 'tubel/pengaktifan-kembali';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $nost = isset($_GET['nost']) ? $_GET['nost'] : null;
        $kep = isset($_GET['kep']) ? $_GET['kep'] : null;
        $tgl_mulai = isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : null;
        $tgl_selesai = isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : null;

        $data['action'] = false;
        $data['tab'] = 'telah_disetujui';
        $data['pengaktifan_kembali'] = TubelHelper::getRef('/pengaktifan_kembali/proses/rekam_st?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&tiket=' . $tiket
            . '&surat_tugas=' . $nost
            . '&kep_pembebasan=' . $kep
            . '&tanggal_mulai_aktif_awal=' . $tgl_selesai
            . '&tanggal_selesai_aktif_akhir=' . $tgl_mulai);

        #pagination
        $data['totalItems'] = $data['pengaktifan_kembali']->totalItems;
        $data['totalPages'] = $data['pengaktifan_kembali']->totalPages;
        $data['currentPage'] = $data['pengaktifan_kembali']->currentPage;
        $data['numberOfElements'] = $data['pengaktifan_kembali']->numberOfElements;
        $data['size'] = $data['pengaktifan_kembali']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.pengaktifankembali.pengaktifankembali_index', $data);
    }

    public function getPengaktifanKembaliTolak()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/pengaktifan-kembali');
        $data['route'] = 'tubel/pengaktifan-kembali';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $nost = isset($_GET['nost']) ? $_GET['nost'] : null;
        $kep = isset($_GET['kep']) ? $_GET['kep'] : null;
        $tgl_mulai = isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : null;
        $tgl_selesai = isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : null;

        $data['tab'] = 'ditolak';
        $data['pengaktifan_kembali'] = TubelHelper::getRef('/pengaktifan_kembali/proses/tolak?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&tiket=' . $tiket
            . '&surat_tugas=' . $nost
            . '&kep_pembebasan=' . $kep
            . '&tanggal_mulai_aktif_awal=' . $tgl_selesai
            . '&tanggal_selesai_aktif_akhir=' . $tgl_mulai);

        #pagination
        $data['totalItems'] = $data['pengaktifan_kembali']->totalItems;
        $data['totalPages'] = $data['pengaktifan_kembali']->totalPages;
        $data['currentPage'] = $data['pengaktifan_kembali']->currentPage;
        $data['numberOfElements'] = $data['pengaktifan_kembali']->numberOfElements;
        $data['size'] = $data['pengaktifan_kembali']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.pengaktifankembali.pengaktifankembali_index', $data);
    }

    public function getPengaktifanKembaliTerbitst()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/pengaktifan-kembali');
        $data['route'] = 'tubel/pengaktifan-kembali';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $nost = isset($_GET['nost']) ? $_GET['nost'] : null;
        $kep = isset($_GET['kep']) ? $_GET['kep'] : null;
        $tgl_mulai = isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : null;
        $tgl_selesai = isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : null;

        $data['tab'] = 'terbitst';
        $data['pengaktifan_kembali'] = TubelHelper::getRef('/pengaktifan_kembali/proses/rekam_kep?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&tiket=' . $tiket
            . '&surat_tugas=' . $nost
            . '&kep_pembebasan=' . $kep
            . '&tanggal_mulai_aktif_awal=' . $tgl_selesai
            . '&tanggal_selesai_aktif_akhir=' . $tgl_mulai);

        #pagination
        $data['totalItems'] = $data['pengaktifan_kembali']->totalItems;
        $data['totalPages'] = $data['pengaktifan_kembali']->totalPages;
        $data['currentPage'] = $data['pengaktifan_kembali']->currentPage;
        $data['numberOfElements'] = $data['pengaktifan_kembali']->numberOfElements;
        $data['size'] = $data['pengaktifan_kembali']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.pengaktifankembali.pengaktifankembali_index', $data);
    }

    public function getPengaktifanKembaliTerbitkep()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/pengaktifan-kembali');
        $data['route'] = 'tubel/pengaktifan-kembali';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $nost = isset($_GET['nost']) ? $_GET['nost'] : null;
        $kep = isset($_GET['kep']) ? $_GET['kep'] : null;
        $tgl_mulai = isset($_GET['tgl_mulai']) ? $_GET['tgl_mulai'] : null;
        $tgl_selesai = isset($_GET['tgl_selesai']) ? $_GET['tgl_selesai'] : null;

        $data['tab'] = 'terbitkep';
        $data['pengaktifan_kembali'] = TubelHelper::getRef('/pengaktifan_kembali/proses/terbit_kep?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&tiket=' . $tiket
            . '&surat_tugas=' . $nost
            . '&kep_pembebasan=' . $kep
            . '&tanggal_mulai_aktif_awal=' . $tgl_selesai
            . '&tanggal_selesai_aktif_akhir=' . $tgl_mulai);

        #pagination
        $data['totalItems'] = $data['pengaktifan_kembali']->totalItems;
        $data['totalPages'] = $data['pengaktifan_kembali']->totalPages;
        $data['currentPage'] = $data['pengaktifan_kembali']->currentPage;
        $data['numberOfElements'] = $data['pengaktifan_kembali']->numberOfElements;
        $data['size'] = $data['pengaktifan_kembali']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.pengaktifankembali.pengaktifankembali_index', $data);
    }

    public function setujuPengaktifanKembali(Request $request)
    {

        $isi = ['keterangan' => null];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        $response = TubelHelper::post('/pengaktifan_kembali/' . $request->id . '/setuju', $body);

        return response()->json($response);
    }

    public function tolakPengaktifanKembali(Request $request)
    {

        $isi = ['keterangan' => null];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);

        $response = TubelHelper::post('/pengaktifan_kembali/' . $request->id_pengaktifan_kembali . '/tolak', $body);

        return response()->json($response);
    }

    public function rekamPengaktifanKembali(Request $request)
    {
        $file_permohonan_aktif = $this->upload($request->file_surat_tugas_rekam);

        if ($file_permohonan_aktif['status'] == 0) {
            return redirect()->back()->with('error', $file_permohonan_aktif['message']);
        }

        $isi = [
            'nomorSt' => $request->nomor_surat_tugas_rekam,
            'tanggalSt' => $request->tgl_surat_tugas_rekam,
            'tglMulaiSt' => $request->tgl_mulai_rekam,
            'tglSelesaiSt' => $request->tgl_selesai_rekam,
            'pathSt' => $file_permohonan_aktif['file'],
            'keterangan' => null
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/pengaktifan_kembali/' . $request->id_permohonan_aktif_kembali . '/surat_tugas', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data berhasli direkam!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    private function upload($file)
    {

        $filepath = $file->getRealPath();
        $nama = $file->getClientOriginalName();
        $mime = $file->getMimeType();
        $ukuran = $file->getSize();

        // validasi mime file
        if ($mime != 'application/pdf') {
            return ['status' => 0, 'message' => 'Format file bukan .pdf'];
        }

        // validasi ukuran 2mb
        if ($ukuran > 2097152) {
            return ['status' => 0, 'message' => 'Ukuran file lebih dari 2 MB'];
        }

        $response = Http::attach('file', file_get_contents($filepath), $nama)
            ->put(env('API_URL') . '/files');

        if (!$response->successful()) {
            $message = $response->body();

            $retUpload = ['status' => 0, 'message' => $message];
        } else {
            $retUpload = ['status' => 1, 'file' => $response->body()];
        }

        #return ['status' => 1, 'body' => $response->body()];
        return ($retUpload);
    }

    public function stAktifKembali()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/st-aktif-kembali');
        $data['route'] = 'tubel/st-aktif-kembali';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $nost = isset($_GET['nost']) ? $_GET['nost'] : null;
        $kep = isset($_GET['kep']) ? $_GET['kep'] : null;

        $data['tab'] = 'rekamst';
        $data['staktifkembali'] = TubelHelper::getRef('/pengaktifan_kembali/proses/rekam_st?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&tiket=' . $tiket
            . '&surat_tugas=' . $nost
            . '&kep_pembebasan=' . $kep);

        #pagination
        $data['totalItems'] = $data['staktifkembali']->totalItems;
        $data['totalPages'] = $data['staktifkembali']->totalPages;
        $data['currentPage'] = $data['staktifkembali']->currentPage;
        $data['numberOfElements'] = $data['staktifkembali']->numberOfElements;
        $data['size'] = $data['staktifkembali']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.staktifkembali.staktifkembali_index', $data);
    }

    public function stAktifKembaliRekam()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/st-aktif-kembali');
        $data['route'] = 'tubel/st-aktif-kembali';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $nost = isset($_GET['nost']) ? $_GET['nost'] : null;
        $kep = isset($_GET['kep']) ? $_GET['kep'] : null;

        $data['tab'] = 'telah_direkam';
        $data['staktifkembali'] = TubelHelper::getRef('/pengaktifan_kembali/proses/rekam_kep?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&tiket=' . $tiket
            . '&surat_tugas=' . $nost
            . '&kep_pembebasan=' . $kep);

        #pagination
        $data['totalItems'] = $data['staktifkembali']->totalItems;
        $data['totalPages'] = $data['staktifkembali']->totalPages;
        $data['currentPage'] = $data['staktifkembali']->currentPage;
        $data['numberOfElements'] = $data['staktifkembali']->numberOfElements;
        $data['size'] = $data['staktifkembali']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.staktifkembali.staktifkembali_index', $data);
    }

    public function rekamStAktifKembali(Request $request)
    {
        $file_st_aktif_kembali = $this->upload($request->file_surat_tugas_rekam);

        if ($file_st_aktif_kembali['status'] == 0) {
            return redirect()->back()->with('error', $file_st_aktif_kembali['message']);
        }

        $isi = [
            'nomorSt' => $request->nomor_surat_tugas_rekam,
            'tanggalSt' => $request->tgl_surat_tugas_rekam,
            'tglMulaiSt' => $request->tgl_mulai_rekam,
            'tglSelesaiSt' => $request->tgl_selesai_rekam,
            'pathSt' => $file_st_aktif_kembali['file'],
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/pengaktifan_kembali/' . $request->id_st_aktif_kembali . '/surat_tugas', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'KEP ST Aktif Kembali berhasil direkam!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function kepPembebasan()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/kep-pembebasan');
        $data['route'] = 'tubel/kep-pembebasan';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $nost = isset($_GET['nost']) ? $_GET['nost'] : null;
        $kep = isset($_GET['kep']) ? $_GET['kep'] : null;

        $data['tab'] = 'rekam_kep';
        $data['keppembebasan'] = TubelHelper::getRef('/pengaktifan_kembali/proses/rekam_kep?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&tiket=' . $tiket
            . '&surat_tugas=' . $nost
            . '&kep_pembebasan=' . $kep);

        #pagination
        $data['totalItems'] = $data['keppembebasan']->totalItems;
        $data['totalPages'] = $data['keppembebasan']->totalPages;
        $data['currentPage'] = $data['keppembebasan']->currentPage;
        $data['numberOfElements'] = $data['keppembebasan']->numberOfElements;
        $data['size'] = $data['keppembebasan']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.keppembebasan.keppembebasan_index', $data);
    }

    public function kepPembebasanDirekam()
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/kep-pembebasan');
        $data['route'] = 'tubel/kep-pembebasan';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;
        $nost = isset($_GET['nost']) ? $_GET['nost'] : null;
        $kep = isset($_GET['kep']) ? $_GET['kep'] : null;

        $data['tab'] = 'kep_telah_direkam';
        $data['keppembebasan'] = TubelHelper::getRef('/pengaktifan_kembali/proses/terbit_kep?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&jenjang=' . $jenjang
            . '&lokasi=' . $lokasi
            . '&tiket=' . $tiket
            . '&surat_tugas=' . $nost
            . '&kep_pembebasan=' . $kep);

        #pagination
        $data['totalItems'] = $data['keppembebasan']->totalItems;
        $data['totalPages'] = $data['keppembebasan']->totalPages;
        $data['currentPage'] = $data['keppembebasan']->currentPage;
        $data['numberOfElements'] = $data['keppembebasan']->numberOfElements;
        $data['size'] = $data['keppembebasan']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.keppembebasan.keppembebasan_index', $data);
    }

    public function rekamKepPembebasan(Request $request)
    {
        $file_surat_tugas_rekam = $this->upload($request->file_surat_tugas_rekam);

        if ($file_surat_tugas_rekam['status'] == 0) {
            return redirect()->back()->with('error', $file_surat_tugas_rekam['message']);
        }

        $isi = [
            'nomorKepPembebasan' => $request->nomor_surat_tugas_rekam,
            'tglKepPembebasan' => $request->tgl_surat_tugas_rekam,
            'tglTmtKepPembebasan' => $request->tmt_rekam,
            'pathKepPembebasan' => $file_surat_tugas_rekam['file'],
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/pengaktifan_kembali/' . $request->id_permohonan_aktif . '/kep_pembebasan', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'KEP Pembebasan berhasil direkam!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }
}
