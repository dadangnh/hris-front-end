<?php

namespace App\Http\Controllers\Tubel;

use App\Helpers\AppHelper;
use App\Helpers\TubelHelper;
use App\Models\MenuModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;

class UsulanTopikRisetController extends Controller
{

    public function usulanTopikRiset(Request $request)
    {
        #list route
        $route = ['persetujuan', 'ditolak', 'dikembalikan', 'pengesahan', 'unggah-persetujuan', 'riset-disetujui', 'dashboard'];

        #validate route
        if ($request->route == '' || in_array($request->route, $route)) {

            #get menu, tab
            $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/riset/usulan-topik-riset');
            $data['tab'] = $request->route ? $request->route : 'persetujuan';

            #set parameters
            $size = 10;
            $page = $request->page;
            $nama = $request->nama;
            $nip = $request->nip;
            $jenjang = $request->jenjang;
            $lokasi = $request->lokasi;
            $topik = $request->topik;
            $tiket = $request->tiket;

            #set api
            switch ($data['tab']) {
                case 'persetujuan':
                    $response = TubelHelper::get('/usulan_topik_riset/monitoring/persetujuan?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&jenjang=' . $jenjang
                        . '&lokasi=' . $lokasi
                        . '&topik=' . $topik
                        . '&tiket=' . $tiket);
                    break;

                case 'ditolak':
                    $response = TubelHelper::get('/usulan_topik_riset/monitoring/ditolak?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&jenjang=' . $jenjang
                        . '&lokasi=' . $lokasi
                        . '&topik=' . $topik
                        . '&tiket=' . $tiket);
                    break;

                case 'dikembalikan':
                    $response = TubelHelper::get('/usulan_topik_riset/monitoring/dikembalikan?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&jenjang=' . $jenjang
                        . '&lokasi=' . $lokasi
                        . '&topik=' . $topik
                        . '&tiket=' . $tiket);
                    break;

                case 'pengesahan':
                    $response = TubelHelper::get('/usulan_topik_riset/monitoring/menunggu_pengesahan?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&jenjang=' . $jenjang
                        . '&lokasi=' . $lokasi
                        . '&topik=' . $topik
                        . '&tiket=' . $tiket);
                    break;

                case 'unggah-persetujuan':
                    $response = TubelHelper::get('/usulan_topik_riset/monitoring/unggah_persetujuan?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&jenjang=' . $jenjang
                        . '&lokasi=' . $lokasi
                        . '&topik=' . $topik
                        . '&tiket=' . $tiket);
                    break;

                case 'riset-disetujui':
                    $response = TubelHelper::get('/usulan_topik_riset/monitoring/disahkan?page=' . $page . '&size=' . $size
                        . '&nama=' . $nama
                        . '&nip=' . $nip
                        . '&jenjang=' . $jenjang
                        . '&lokasi=' . $lokasi
                        . '&topik=' . $topik
                        . '&tiket=' . $tiket);
                    break;
            }

            #data
            $data['usulanriset'] = $response['data'];

            #pagination
            $data['totalItems'] = $data['usulanriset']->totalItems;
            $data['totalPages'] = $data['usulanriset']->totalPages;
            $data['currentPage'] = $data['usulanriset']->currentPage;
            $data['numberOfElements'] = $data['usulanriset']->numberOfElements;
            $data['size'] = $data['usulanriset']->size;

            #referensi
            $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
            $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

            return view('tubel.usulantopikriset.usulantopikriset_index', $data);
        } else {
            return abort(404);
        }
    }

    public function kelolaReferensiRiset(Request $request)
    {
        #get menu, tab
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/riset/kelola-referensi');
        $data['tab'] = 'menunggu_persetujuan';

        #param
        $size = 10;
        $page = $request->page;
        $topik = $request->topik_riset;
        $nomor = $request->nomor_pengumuman;

        #data
        $response = TubelHelper::get('/topik_riset/search?page=' . $page . '&size=' . $size
            . '&nama_topik=' . $topik
            . '&nomor_pengumuman=' . $nomor);
        $data['usulantopik'] = $response['data'];

        #pagination
        $data['totalItems'] = $data['usulantopik']->totalItems;
        $data['totalPages'] = $data['usulantopik']->totalPages;
        $data['currentPage'] = $data['usulantopik']->currentPage;
        $data['numberOfElements'] = $data['usulantopik']->numberOfElements;
        $data['size'] = $data['usulantopik']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.usulantopikriset.kelolatopikriset_index', $data);
    }

    public function usulanRisetAdd(request $request)
    {

        $file_dokumen_proposal = $this->upload($request->file_dokumen_proposal_add);

        # file dokumen riset
        if ($file_dokumen_proposal['status'] == 0) {
            return redirect()->back()->with('error', $file_dokumen_proposal['message']);
        }

        $isi = [
            'id_tubel' => $request->idTubel,
            'topikRiset' => $request->pilihan_topik_add,
            'judulRiset' => $request->judul_riset_add,
            'pathDokumenProposal' => $file_dokumen_proposal['file']
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/perpanjangan_st/' . $request->idTubel, $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data Berhasil ditambah!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function deleteTopikRiset(Request $request)
    {
        #set id
        $id = $request->id_usulan_topik;

        #set array
        $body = [];

        #loop
        foreach ($id as $key => $value) {
            $body[] = $value;
        }

        #response
        $response = TubelHelper::delete('/topik_riset/batch', $body);

        return response()->json($response);
    }

    public function detailTubelMon($id)
    {
        #menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/monitoring');

        #tab
        $tab = !empty($_GET['tab']) ? $_GET['tab'] : 'lps';
        $data['action'] = false;
        $data['tab'] = $tab;

        if ($tab == 'lps') {
            $data['lps'] = TubelHelper::getRef('/laporan/tubel/' . $id);
        } elseif ($tab == 'cuti') {
            $data['cuti'] = TubelHelper::getRef('/cuti_belajar/tubel/' . $id);
        } elseif ($tab == 'pengaktifan') {
            $data['pengaktifan_kembali'] = TubelHelper::getRef('/pengaktifan_kembali/tubel/' . $id);
        } elseif ($tab == 'perpanjangan') {
            $data['perpanjangan'] = TubelHelper::getRef('/perpanjangan_st/tubel/' . $id);
        }

        #get data tubel pegawai
        $data['tubel'] = TubelHelper::getRef('/tubel/' . $id);
        $data['perguruantinggi'] = TubelHelper::getRef('/perguruan_tinggi_peserta/tubel/' . $id);
        $data['countperguruantinggi'] = count(TubelHelper::getRef('/perguruan_tinggi_peserta/tubel/' . $id));

        #referensi
        $data['ref_lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');
        $data['ref_perguruantinggi'] = TubelHelper::getRef('/perguruan_tinggi');
        $data['ref_negara'] = TubelHelper::getRef('/negara_perguruan_tinggi/all');
        $data['ref_prodi'] = TubelHelper::getRef('/program_studi');

        $data['ptTubel'] = TubelHelper::getRef('/perguruan_tinggi_peserta/tubel/' . $id);

        return view('tubel.detail', $data);
    }

    public function previewCsv(Request $request)
    {
        #set mime
        $mime = ['text/plain', 'text/csv'];

        #set delimiter
        $delimiter = TubelHelper::detectDelimiter($request->file('file_csv'));

        # cek delimiter
        if ($delimiter == ',') {
            return response()->json(['status' => 0, 'message' => 'Delimiter harus titik koma ( ; )']);
        }

        #validasi file
        if ($request->file('file_csv') != null) {

            #get properti file
            $mimeFile = $request->file('file_csv')->getMimeType();

            #validasi properti file
            if (in_array($mimeFile, $mime)) {
                #get tmp file
                $tmp_file = $request->file('file_csv')->getRealPath();

                #use function csv to array
                $response = TubelHelper::csvToArray($tmp_file, $delimiter);

                #set array null
                $data = [];

                #loop data
                foreach ($response as $key => $value) {
                    $data[] = [
                        'topik' => $value['topik'],
                        'nomor_pengumuman' => $value['nomor_pengumuman'],
                        'tanggal_pengumuman' => AppHelper::instance()->indonesian_date($value['tanggal_pengumuman'], 'j F Y', '')
                    ];
                }

                return response()->json(['status' => 1, 'data' => mb_convert_encoding($data, 'UTF-8', 'UTF-8')]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Format file bukan csv!']);
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Tidak ada file yg diupload!']);
        }
    }

    private function csvToArray($tmp_file, $delimiter)
    {
        $header = null;
        $data = array();
        if (($handle = fopen($tmp_file, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }

    public function importCsv(Request $request)
    {
        #set data, validasi
        $file = $request->file('file_csv'); #file
        $size = 2097152; #2MB
        // $mime = ['text/plain']; #csv
        $mime = ['application/vnd.ms-excel']; #csv
        $pegawaiId = session()->get('user')['pegawaiId'];

        #get properti file
        $pathFile = $file->getRealPath();
        $namaFile = $file->getClientOriginalName();
        $mimeFile = $file->getClientMimeType();
        $ukuranFile = $file->getSize();

        #validasi properti file
        if (!in_array($mimeFile, $mime)) {
            return ['status' => 0, 'message' => 'Format file tidak sesuai'];
        } elseif ($ukuranFile > $size) {
            return ['status' => 0, 'message' => 'Ukuran file lebih dari 2 MB'];
        } else {

            $csv = url('topik.csv');

            $body = [
                'pegawai' => $pegawaiId,
                'file' => $csv
            ];

            #send file
            // $response = Http::attach('file', $file, $namaFile)
            // ->post(env('API_URL') . '/topik_riset/upload', $body);
            // ->post(env('API_URL') . '/topik_riset/upload', ['multipart' => ['contents' => fopen($csv, 'r'), 'name' => 'file']]$body);

            // ['multipart' => ['contents' => fopen($csv, 'r'), 'name' => 'file']]);

            // ->post(env('ESIGN_API_URL').'?nik='.$nik.'&passphrase='.$passphrase.'&jenis_dokumen='.$jenis.'&nomor='.$nomor.'&tujuan='.$tujuan.'&perihal='
            // .$perihal.'&info='.$info.'&tampilan=invisible', ['multipart' => ['contents' => fopen(\Storage::path($path), 'r'), 'name' => 'file']]);

            $response = Http::post(env('API_URL') . '/topik_riset/upload?pegawai=' . $pegawaiId, ['multipart' => ['contents' => fopen($csv, 'r'), 'name' => 'file']]);

            // $response = Http::post(env('API_URL') . '/topik_riset/upload', $body);

            // $response = TubelHelper::post('/topik_riset/upload?pegawai=' . $pegawaiId, $body);

            // $client = new Client();

            // $response = $client->request('POST', env('API_URL') . '/topik_riset/upload?pegawai=' . $pegawaiId, [
            //     'multipart' => [
            //         [
            //             'name'     => 'FileContents',
            //             'contents' => file_get_contents($pathFile),
            //             'filename' => $namaFile
            //         ],
            //         // 'header' => []
            //     ],
            // ]);

            // $fileinfo = array(
            //     'name'          =>  $namaFile,
            //     'clientNumber'  =>  "102425",
            //     'type'          =>  'Writeoff',
            // );

            // $client = new \GuzzleHttp\Client();

            // $response = $client->request('POST', env('API_URL') . '/topik_riset/upload?pegawai=' . $pegawaiId, [
            //     'multipart' => [
            //         [
            //             'name'  =>  $namaFile,
            //             'FileContents'  => fopen($pathFile, 'r'),
            //             'contents'      => fopen($pathFile, 'r'),
            //             'FileInfo'      => json_encode($fileinfo),
            //             'headers'       =>  [
            //                 'Content-Type' => 'text/plain',
            //                 'Content-Disposition'   => 'form-data; name="FileContents"; filename="'. $namaFile .'"',
            //             ],
            //             // 'contents' => $resource,
            //         ]
            //     ],
            // ]);

            dd($response->body());
            dd($response);

            // #validasi error
            // if (!$response->successful()) {
            //     $message = $response->body();

            //     $response_upload = ['status' => 0, 'message' => $message];
            // } else {
            //     $response_upload = ['status' => 1, 'file' => $response->body()];
            // }
        }
    }

    public function importCsvArray(Request $request)
    {
        #set data, validasi
        $file = $request->file('file_csv'); #file
        $size = 2097152; #2MB
        // $mime = ['text/plain']; #csv
        $mime = ['application/vnd.ms-excel']; #csv
        $pegawaiId = session()->get('user')['pegawaiId'];

        #get properti file
        $pathFile = $file->getRealPath();
        $mimeFile = $file->getClientMimeType();
        $ukuranFile = $file->getSize();

        #validasi properti file
        if (!in_array($mimeFile, $mime)) {
            return ['status' => 0, 'message' => 'Format file tidak sesuai'];
        } elseif ($ukuranFile > $size) {
            return ['status' => 0, 'message' => 'Ukuran file lebih dari 2 MB'];
        } else {

            #set delimiter
            $delimiter = TubelHelper::detectDelimiter($request->file('file_csv'));

            #use function csv to array
            $csv = $this->csvToArray($pathFile, $delimiter);

            #set array
            $data = [];

            #looping
            foreach ($csv as $key => $value) {
                $data[] = [
                    "namaTopik" => $value['topik'],
                    "nomorPengumuman" => $value['nomor_pengumuman'],
                    "tglPengumuman" => AppHelper::instance()->indonesian_date($value['tanggal_pengumuman'], 'Y-m-d', ''),
                    "pegawai" => $pegawaiId
                ];
            }

            #send request
            $response = TubelHelper::post('/topik_riset/loader', $data);

            return redirect()->back()->with('success', 'Data berhasil diimport');
        }
    }

    public function persetujuanUsulanTopikRiset(Request $request)
    {
        $param = [
            'keterangan' => $request->keterangan
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $param);

        #sqitch api
        $response = TubelHelper::post('/usulan_topik_riset/' . $request->id . '/' . $request->action, $body);

        return response()->json($response);
    }

    public function pengesahanUsulanTopikRiset(Request $request)
    {
        $param = [
            'keterangan' => $request->keterangan
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $param);

        #sqitch api
        $response = TubelHelper::post('/usulan_topik_riset/' . $request->id . '/sahkan', $body);

        return response()->json($response);
    }

    public function ndPengesahanUsulanTopikRiset(Request $request)
    {
        #set validasi
        $file = $request->file('dokumen_persetujuan_unggah_nd'); #file
        $size = 2097152; #2MB
        $mime = ['application/pdf']; #pdf

        #upload file
        $response_file = TubelHelper::upload($file, $size, $mime);

        if ($response_file['status'] == 1) {
            #set body
            $isi = [
                'nomorNdPersetujuanDjp' => $request->nomor_nd_persetujuan_unggah_nd,
                'tglPersetujuanDjp' => $request->tgl_nd_persetujuan_periksa_unggah_nd,
                'pathNdPersetujuanDjp' => $response_file['file']
            ];

            #generate body
            $body = TubelHelper::generateBody(session()->get('user'), $isi);

            #send data to api
            $response = TubelHelper::post('/usulan_topik_riset/' . $request->id_usulan_riset_unggah_nd . '/nd_pengesahan', $body);

            if ($response['status'] == 1) {
                return redirect()->back()->with('success', 'data berhasil disimpan');
            } else {
                return redirect()->back()->with('error', 'data gagal disimpan');
            }
        } else {
            return redirect()->back()->with('error', $response_file['message']);
        }
    }
}
