<?php

namespace App\Http\Controllers\Tubel;

use App\Helpers\AppHelper;
use App\Helpers\TubelHelper;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;

class TubelController extends Controller
{
    public static function downloadFile($namafile)
    {
        $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);

        $response = Http::get(env('API_URL') . '/files/' . $namafile);

        file_put_contents($tmp_direktori, $response->body());

        $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

        return response()->download($tmp_direktori, $namafile, $headers);
    }

    public function addCutiBelajarPerpanjanganAdmin(Request $request)
    {

        $file_cuti = $this->upload($request->file_cuti_perpanjangan_rekam);

        if ($file_cuti['status'] == 0) {
            return redirect()->back()->with('error', $file_cuti['message']);
        }

        $file_sponsor = $this->upload($request->file_sponsors_perpanjangan_rekam);

        if ($file_sponsor['status'] == 0) {
            return redirect()->back()->with('error', $file_sponsor['message']);
        }


        $isi = [
            'alasanCuti' => $request->alasan_cuti_perpanjangan_rekam,
            'tglMulaiCuti' => $request->tgl_mulai_cuti_perpanjangan_rekam,
            'tglSelesaiCuti' => $request->tgl_selesai_cuti_perpanjangan_rekam,
            'nomorSuratKampus' => $request->surat_izin_cuti_kampus_perpanjangan_rekam,
            'tglSuratKampus' => $request->tgl_surat_kampus_perpanjangan_rekam,
            'nomorSuratSponsor' => $request->surat_izin_cuti_sponsor_perpanjangan_rekam,
            'tglSuratKepSponsor' => $request->tgl_surat_sponsor_perpanjangan_rekam,
            'pathSuratIjinCutiKampus' => $file_cuti['file'],
            'pathSuratIjinCutiSponsor' => $file_sponsor['file'],
            'dataIndukTubelId' => $request->idTubel
        ];

        $body = TubelHelper::generateBody(session()->get('user'), $isi);
        $response = TubelHelper::post('/cuti_belajar/admin', $body);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Perpanjangan berhasil direkam!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function getTiket($tiket)
    {
        #get data
        $response = TubelHelper::get('/probis/log/' . $tiket);

        #fetch data
        foreach ($response['data'] as $key => $item) {
            $data[] = [
                'createdDate' => AppHelper::instance()->indonesian_date($item->createdDate, 'j F Y', ''),
                'output' => $item->output,
                'nomorTiket' => $item->nomorTiket,
                'keterangan' => $item->keterangan,
            ];
        }

        return $data;
    }

    public function getFile($namafile)
    {

        $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);

        $response = Http::get(env('API_URL') . '/files/' . $namafile);

        file_put_contents($tmp_direktori, $response->body());

        return response()->file($tmp_direktori, [
            'Content-Type' => 'application/pdf'
        ]);
    }
}
