<?php

namespace App\Http\Controllers\Tubel;

use App\Helpers\TubelHelper;
use App\Models\MenuModel;
use Illuminate\Routing\Controller;

class LpsController extends Controller
{

    public function getLps()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/lps');
        $data['route'] = 'tubel/lps';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $tahun = isset($_GET['tahun']) ? $_GET['tahun'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $semester = isset($_GET['semester']) ? $_GET['semester'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;

        $data['tab'] = 'menunggu_persetujuan';
        $data['lps'] = TubelHelper::getRef('/laporan/proses/persetujuan?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&lokasi=' . $lokasi
            . '&jenjang=' . $jenjang
            . '&flag_semester=' . $semester
            . '&tahun_akademik=' . $tahun
            . '&tiket=' . $tiket);

        #pagination
        $data['totalItems'] = $data['lps']->totalItems;
        $data['totalPages'] = $data['lps']->totalPages;
        $data['currentPage'] = $data['lps']->currentPage;
        $data['numberOfElements'] = $data['lps']->numberOfElements;
        $data['size'] = $data['lps']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.lps.lps_index', $data);
    }

    public function getLpsSetuju()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/lps');
        $data['route'] = 'tubel/lps';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['lokasi']) ? $_GET['nip'] : null;
        $lokasi = isset($_GET['nip']) ? $_GET['nip'] : null;
        $tahun = isset($_GET['tahun']) ? $_GET['tahun'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $semester = isset($_GET['semester']) ? $_GET['semester'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;

        $data['tab'] = 'telah_disetujui';
        $data['lps'] = TubelHelper::getRef('/laporan/proses/setuju?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&lokasi=' . $lokasi
            . '&jenjang=' . $jenjang
            . '&flag_semester=' . $semester
            . '&tahun_akademik=' . $tahun
            . '&tiket=' . $tiket);

        #pagination
        $data['totalItems'] = $data['lps']->totalItems;
        $data['totalPages'] = $data['lps']->totalPages;
        $data['currentPage'] = $data['lps']->currentPage;
        $data['numberOfElements'] = $data['lps']->numberOfElements;
        $data['size'] = $data['lps']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.lps.lps_index', $data);
    }

    public function getLpsDitolak()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/lps');
        $data['route'] = 'tubel/lps';

        #param
        $size = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $nama = isset($_GET['nama']) ? $_GET['nama'] : null;
        $nip = isset($_GET['nip']) ? $_GET['nip'] : null;
        $lokasi = isset($_GET['lokasi']) ? $_GET['lokasi'] : null;
        $tahun = isset($_GET['tahun']) ? $_GET['tahun'] : null;
        $jenjang = isset($_GET['jenjang']) ? $_GET['jenjang'] : null;
        $semester = isset($_GET['semester']) ? $_GET['semester'] : null;
        $tiket = isset($_GET['tiket']) ? $_GET['tiket'] : null;

        $data['tab'] = 'ditolak';
        $data['lps'] = TubelHelper::getRef('/laporan/proses/tolak?page=' . $page
            . '&size=' . $size
            . '&nama=' . $nama
            . '&nip=' . $nip
            . '&lokasi=' . $lokasi
            . '&jenjang=' . $jenjang
            . '&flag_semester=' . $semester
            . '&tahun_akademik=' . $tahun
            . '&tiket=' . $tiket);

        #pagination
        $data['totalItems'] = $data['lps']->totalItems;
        $data['totalPages'] = $data['lps']->totalPages;
        $data['currentPage'] = $data['lps']->currentPage;
        $data['numberOfElements'] = $data['lps']->numberOfElements;
        $data['size'] = $data['lps']->size;

        #referensi
        $data['jenjang'] = TubelHelper::getRef('/jenjang_pendidikan');
        $data['lokasi'] = TubelHelper::getRef('/lokasi_pendidikan');

        return view('tubel.lps.lps_index', $data);
    }

    public function getLpsDikirim()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/lps');

        $data['route'] = 'tubel/lps';

        $data['tab'] = 'kirim';

        return view('tubel.lps_index', $data);
    }

    public function getLpsDashboard()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/lps');

        $data['route'] = 'tubel/lps';

        $data['tab'] = 'dashboard';

        return view('tubel.lps.lps_index', $data);
    }
}
