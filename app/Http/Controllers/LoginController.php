<?php

namespace App\Http\Controllers;

use App\Helpers\LoginHelper;
use App\Helpers\MenuHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class LoginController extends Controller
{
    public function index()
    {
        return view('auth/login');
    }

    public function login(request $request)
    {
        #validate
        $request->validate([
            'username' => 'required',
            // 'password' => 'required'
        ]);

        if ($request == null) {
            return redirect()->back();
        }

        #proses login
        $username = $request->get('username');
        // $password = $request->get('password');
        $password = 'Pajak123';

        $data = Http::accept('application/json')
            ->withOptions([
                "verify" => false,
            ])
            ->post('https://iam.simulasikan.com/api/authentication', [
                'username' => $username,
                'password' => $password,
            ]);

        $body = $data->getBody();
        $authiam['data'] = json_decode($body);
        $token_decode = (json_decode(base64_decode(str_replace('_', '/', str_replace('-', '+', explode('.', $authiam['data']->token)[1])))));

        #data iam
        $whoamiiam = LoginHelper::iampostbaru('/api/whoami', [], $authiam['data']->token);
        $id_pegawai = $whoamiiam['data']->pegawai->pegawaiId;
        $nip9 = $whoamiiam['data']->pegawai->nip9;
        $role = $whoamiiam['data']->roles[0];
        $roleList = $whoamiiam['data']->roles;

        $dataIam = [
            "username" => $username,
            "_role" => $role,
            "_role_list" => $roleList,
            "_token" => $authiam['data']->token,
            "_refresh_token" => $authiam['data']->refresh_token,
            "_expired" => $token_decode->exp
        ];

        #data href
        $dataHref = LoginHelper::get('master_pegawais/' . $nip9 . '/data', $authiam['data']->token);

        #golongan dan pangkat
        $pangkatId = $dataHref['data']->data->pangkatId;
        $pangkat = LoginHelper::get('pangkats/' . $pangkatId, $authiam['data']->token);
        $golongan = LoginHelper::get($pangkat['data']->golongan, $authiam['data']->token);

        $jabatan['golongan'] = $golongan['data']->nama;

        #data jabatans iam
        $dataJabatan = LoginHelper::iamget('/api/jabatans/' . $dataHref['data']->data->jabatanId, $authiam['data']->token);
        $jabatan['kelasJabatan'] = $dataJabatan['data']->nama;
        $jabatan['levelJabatan'] = $dataJabatan['data']->level;
        $jabatan['kd_jabatan_id'] = $dataJabatan['data']->legacyKode;

        #data unit iam
        $dataUnit = LoginHelper::iamget('/api/units/' . $dataHref['data']->data->unitId, $authiam['data']->token);
        $dataParent = LoginHelper::iamget('/api/units?page=1&childs.id=' . $dataHref['data']->data->unitId, $authiam['data']->token);

        #data kantor iam
        $dataKantors = LoginHelper::iamget('/api/kantors?page=1&legacyKode=' . $dataHref['data']->data->kantorLegacyKode, $authiam['data']->token);
        $parent = LoginHelper::iamget('/api/kantors?page=1&childs.id=' . $dataHref['data']->data->kantorId, $authiam['data']->token);

        $jeniskantor = LoginHelper::iamget($dataKantors['data'][0]->jenisKantor, $authiam['data']->token);
        $kanwil = LoginHelper::iamget($parent['data'][0]->jenisKantor, $authiam['data']->token);
        $arrParent = explode('/', $dataKantors['data'][0]->jenisKantor);


        if ($jeniskantor['data']->tipe != 'UPT') {
            $kdkpp = $dataKantors['data'][0]->legacyKodeKpp;
            $kdkanwil = $dataKantors['data'][0]->legacyKodeKanwil;
            $kantorInduk = $arrParent[3];
            $nmkanwil = $kanwil['data']->nama;
            $tipeKantor = '';
        } else {
            $kdkpp = '';
            $kdkanwil = '';
            $kantorInduk = $arrParent[3];
            $nmkanwil = $kanwil['data']->nama;
            $tipeKantor = $jeniskantor['data']->tipe;
        }

        #data collection
        foreach ($dataHref['data']->data as $key) {
            $dataUser = [
                "id" => $dataHref['data']->data->id,
                "pegawaiId" => $dataHref['data']->data->pegawaiId,
                "namaPegawai" => $dataHref['data']->data->namaPegawai,
                "nip9" => $dataHref['data']->data->nip9,
                "nip18" => $dataHref['data']->data->nip18,
                "nik" => $dataHref['data']->data->nik,
                "pangkatId" => $dataHref['data']->data->pangkatId,
                "pangkat" => $dataHref['data']->data->pangkat,
                "jabatanId" => $dataHref['data']->data->jabatanId,
                "jabatan" => $dataHref['data']->data->jabatan,
                "unitId" => $dataHref['data']->data->unitId ?? null,
                "unit" => $dataHref['data']->data->unit,
                "unitLegacyKode" => $dataHref['data']->data->unitLegacyKode ?? null,
                "unitParent" => $dataParent['data'][0]->jenisKantor,
                "kantorId" => $dataHref['data']->data->kantorId,
                "kantor" => $dataHref['data']->data->kantor,
                "kantorLegacyKode" => $dataHref['data']->data->kantorLegacyKode,
                "golongan" => $golongan['data']->nama,
                "levelJabatan" => $dataJabatan['data']->level,
                "kd_jabatan_id" => $dataJabatan['data']->legacyKode,
                "kdkpp" => $kdkpp,
                "kdkanwil" => $kdkanwil,
                "nmkanwil" => $nmkanwil,
                "kantorInduk" => $kantorInduk,
                "tipekantor" => $tipeKantor,
                "agamaId" => $dataHref['data']->data->agamaId,
                "agama" => $dataHref['data']->data->agama,
                "jenisKelaminId" => $dataHref['data']->data->jenisKelaminId,
                "jenisKelamin" => $dataHref['data']->data->jenisKelamin,
                "alamatJalan" => $dataHref['data']->data->alamatJalan,
                "kelurahanId" => $dataHref['data']->data->kelurahanId,
                "kelurahan" => $dataHref['data']->data->kelurahan,
                "kecamatanId" => $dataHref['data']->data->kecamatanId,
                "kecamatan" => $dataHref['data']->data->kecamatan,
                "kabupatenId" => $dataHref['data']->data->kabupatenId,
                "kabupaten" => $dataHref['data']->data->kabupaten,
                "provinsiId" => $dataHref['data']->data->provinsiId,
                "provinsi" => $dataHref['data']->data->provinsi,
                "tanggalLahir" => $dataHref['data']->data->tanggalLahir,
                "tempatLahir" => $dataHref['data']->data->tempatLahir,
                "pensiun" => $dataHref['data']->data->pensiun,

            ];
        }

        #set session
        if (!$data->successful()) {
            return redirect()->back()->with('failed', 'Gagal login!');
        } else {
            # menu access
            $menu_access = MenuHelper::getMenuAccess($roleList);

            if ($menu_access == null) {
                return abort(500);
            }

            session()->put('menu_access', $menu_access);
            session()->put('login_info', $dataIam);
            session()->put('user', $dataUser);

            return redirect('beranda');
        }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();

        return redirect('login');
    }
}
