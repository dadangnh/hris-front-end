<?php

// use App\Http\Controllers\Ibel\IbelController;
use App\Http\Controllers\Ibel\PersetujuanHasilSeleksi;
use App\Http\Controllers\Ibel\KEpSeleksiIbelController;
use App\Http\Controllers\Ibel\AdministrasiIbelController;
use App\Http\Controllers\Ibel\HasilSeleksiIbelController;
use App\Http\Controllers\Ibel\PeriksaPermohonanIbelController;
use App\Http\Controllers\Ibel\PermintaanDokumenIbelController;
use App\Http\Controllers\Ibel\PenerbitanIzinIbelController;
use App\Http\Controllers\Ibel\TindaklanjutIbelController;
use App\Http\Controllers\Ibel\ReferensiController;
use App\Http\Controllers\Ibel\IbelController;
use Illuminate\Support\Facades\Route;

Route::middleware(['ibelRoute', 'CekAuth', 'MenuAccess'])->group(function () {

    // Tabel Administrasi Izin Belajar
    Route::prefix('/ibel/administrasi')->group(function () {
        Route::get('/', [AdministrasiIbelController::class, 'index']);
        Route::get('/tubel/{tiket}', [AdministrasiIbelController::class, 'getTiket']);
        Route::get('/lihat-draft/{id}', [AdministrasiIbelController::class, 'lihatPermohonanDraft']);
        Route::post('/kirim', [AdministrasiIbelController::class, 'kirimIbel']);
        Route::post('/hapus-ibel', [AdministrasiIbelController::class, 'deleteIbel']);
        Route::post('/get-data-pt', [AdministrasiIbelController::class, 'getDataPT']);
        Route::post('/get-data-prodi-akreditasi', [AdministrasiIbelController::class, 'getDataProdi']);
        Route::post('/get-data-lokasi-kampus', [AdministrasiIbelController::class, 'getDataLokasiIbel']);
        Route::post('/upload-dokumen-persyaratan/upload_file', [AdministrasiIbelController::class, 'uploadFileIzinbelajar']);
    });

    // Menu Periksa Permohonan
    Route::prefix('/ibel/periksa')->group(function () {
        Route::get('/', [PeriksaPermohonanIbelController::class, 'index']);
        Route::get('/{route}', [PeriksaPermohonanIbelController::class, 'index']);
        Route::get('/permohonan/{id}', [PeriksaPermohonanIbelController::class, 'periksaPermohonan']);
        Route::get('/lihat/{id}', [PeriksaPermohonanIbelController::class, 'lihatPermohonan']);
        Route::post('/permohonan/{id}/tolak', [PeriksaPermohonanIbelController::class, 'tolakPermohonan']);
        Route::get('/permohonan/{id}/setuju', [PeriksaPermohonanIbelController::class, 'setujuPermohonan']);
    });

    // Menu Permintaan Dokumen
    Route::prefix('/ibel/permintaan-dokumen')->group(function () {
        Route::get('/', [PermintaanDokumenIbelController::class, 'index']);
        Route::get('/{id}', [PermintaanDokumenIbelController::class, 'lihatPermohonan']);
        Route::get('/{id}/file_skorsing', [PermintaanDokumenIbelController::class, 'generateFileSkorsing']);
        Route::get('/{id}/file_hukdis', [PermintaanDokumenIbelController::class, 'generateFileHukdis']);
        Route::get('/{id}/file_jarak', [PermintaanDokumenIbelController::class, 'generateFileJarak']);
        Route::get('/{id}/file_waktu', [PermintaanDokumenIbelController::class, 'generateFileWaktu']);
    });

    // Menu Hasil Seleksi
    Route::prefix('/ibel/hasil-seleksi')->group(function () {
        Route::get('/', [HasilSeleksiIbelController::class, 'index']);
        Route::get('/{route}', [HasilSeleksiIbelController::class, 'index']);
        Route::get('/rekam/{id}', [HasilSeleksiIbelController::class, 'hasilSeleksi']);
        Route::post('/rekam/{id}', [HasilSeleksiIbelController::class, 'rekamHasilSeleksi']);
        Route::get('/lihat/{id}', [HasilSeleksiIbelController::class, 'lihatPermohonan']);
        Route::post('/get-tim-seleksi', [IbelController::class, 'getTimSeleksi']);
    });

    // Menu Kep Seleksi
    Route::prefix('/ibel/kep-seleksi')->group(function () {
        Route::get('/', [KepSeleksiIbelController::class, 'index']);
        Route::get('/{route}', [KepSeleksiIbelController::class, 'index']);
        Route::post('/rekam', [KepSeleksiIbelController::class, 'rekamPermohonan']);
        Route::get('/proses/{id}', [KepSeleksiIbelController::class, 'prosesPermohonan']);
        Route::get('/lihat/{id}', [KepSeleksiIbelController::class, 'detailPermohonan']);
        Route::get('/{id}/template_kep_seleksi', [KepSeleksiIbelController::class, 'generateKepKetuaTim']);
        Route::get('/{id}/kep_seleksi', [IbelController::class, 'kepSeleksiPermohonanIbel']);
    });

    // Menu Penerbitan KEP Seleksi
    Route::prefix('/ibel/penerbitan-izin')->group(function () {
        Route::get('/', [PenerbitanIzinIbelController::class, 'index']);
        Route::get('/{route}', [PenerbitanIzinIbelController::class, 'index']);
        Route::post('/ds', [PenerbitanIzinIbelController::class, 'dsPermohonan']);
        Route::get('/lihat/{id}', [PenerbitanIzinIbelController::class, 'lihatPermohonan']);
        Route::get('/preview/{id}', [PenerbitanIzinIbelController::class, 'generateFileKepKetuaTim']);
    });

    // Menu Tindak Lanjut
    Route::prefix('/ibel/tindak-lanjut')->group(function () {
        Route::get('/', [TindaklanjutIbelController::class, 'index']);
        Route::post('/rekam', [TindaklanjutIbelController::class, 'rekamTindakLanjut']);
        Route::get('/proses/{id}', [TindaklanjutIbelController::class, 'tetapkanIzinBelajar']);
        Route::get('/lihat/{id}', [TindaklanjutIbelController::class, 'detailPermohonan']);
        Route::get('/{id}/surat_kep_seleksi', [TindaklanjutIbelController::class, 'generateKepSeleksi']);
        Route::get('/{id}/surat_izin', [TindaklanjutIbelController::class, 'generateSuratIzin']);
        Route::get('/{id}/template_surat_penolakan', [TindaklanjutIbelController::class, 'generateTemplateSuratPenolakan']);
        Route::get('/{id}/template_nd_pemberitahuan_penolakan', [TindaklanjutIbelController::class, 'generateTemplateNdPemberitahuanPenolakan']);
        Route::get('/{id}/nd_penyampaian', [TindaklanjutIbelController::class, 'generateNdPenyampaian']);
        Route::get('/{id}/tindak_lanjut', [IbelController::class, 'tindaklanjutPermohonanIbel']);
    });


    // Tabel Referensi Perguruan Tinggi
    Route::prefix('/ibel/referensi/pt')->group(function () {
        Route::get('/', [ReferensiController::class, 'indexReferensiPT1']);
        Route::post('/tambah', [ReferensiController::class, 'addReferensiPT']);
        Route::post('/update_pt', [ReferensiController::class, 'updateReferensiPT']);
        Route::post('/delete', [ReferensiController::class, 'deleteReferensiPT']);
    });

    // Tabel Referensi Program Studi
    Route::prefix('/ibel/referensi/prodi')->group(function () {
        Route::get('/', [ReferensiController::class, 'indexReferensiProdi']);
        Route::post('/tambah', [ReferensiController::class, 'addReferensiProdi']);
        Route::post('/update_prodi', [ReferensiController::class, 'updateReferensiProdi']);
        Route::post('/delete', [ReferensiController::class, 'deleteReferensiProdi']);
    });

    // Tabel Referensi Lokasi Pendidikan
    Route::prefix('/ibel/referensi/lokasi')->group(function () {
        Route::get('/', [ReferensiController::class, 'indexReferensiLokasi']);
        Route::post('/tambah', [ReferensiController::class, 'addReferensiLokasi']);
        Route::post('/update_lokasi', [ReferensiController::class, 'updateReferensiLokasi']);
        Route::post('/delete', [ReferensiController::class, 'deleteReferensiLokasi']);
    });

    // Tabel Referensi Otorisasi Izin Belajar
    Route::prefix('/ibel/referensi/otorisasi')->group(function () {
        Route::get('/', [ReferensiController::class, 'indexReferensiOtorisasi']);
        Route::post('/tambah', [ReferensiController::class, 'addReferensiOtorisasi']);
        Route::post('/update_otorisasi', [ReferensiController::class, 'updateReferensiOtorisasi']);
        Route::post('/delete', [ReferensiController::class, 'deleteReferensiOtorisasi']);
        Route::post('/get-kantor-referensi', [ReferensiController::class, 'getDataKantorReferensi']);
        Route::post('/get-nama-referensi', [ReferensiController::class, 'getDataPegawaiReferensi']);
    });

    // Tabel Referensi PT Prodi Akreditasi
    Route::prefix('/ibel/referensi/prodi-akreditasi')->group(function () {
        Route::get('/', [ReferensiController::class, 'indexReferensiAkreditasi']);
        Route::post('/tambah', [ReferensiController::class, 'addReferensiAkreditasi']);
        Route::post('/update_akreditasi', [ReferensiController::class, 'updateReferensiAkreditasi']);
        Route::post('/delete', [ReferensiController::class, 'deleteReferensiAkreditasi']);
    });

    // Tabel Referensi Tim Otorisasi
    Route::prefix('/ibel/referensi/tim-otorisasi')->group(function () {
        Route::get('/', [ReferensiController::class, 'indexReferensiTimOtorisasi']);
        Route::get('/{id}/kelola-anggota', [ReferensiController::class, 'indexAnggotaTim']);
        Route::post('/tambah', [ReferensiController::class, 'addReferensiTimOtorisasi']);
        // Route::post('/update-tim-otorisasi', [ReferensiController::class, 'updateTimOtorisasi']);
        Route::post('/delete', [ReferensiController::class, 'deleteTimOtorisasi']);
        Route::post('/cari-anggota', [ReferensiController::class, 'cariAnggotaTim']);
        Route::post('/tambah-anggota', [ReferensiController::class, 'tambahAnggotaTim']);
        Route::post('/hapus-anggota', [ReferensiController::class, 'hapusAnggotaTim']);

    });

    // Menu Halaman Pegawai Perlangkah
    Route::prefix('/ibel/administrasi')->group(function () {
        //langkah paling awal
        Route::get('/isi-data-utama', [AdministrasiIbelController::class, 'indexIzinBelajarLangkahSatu']);
        Route::post('/isi-data-utama', [AdministrasiIbelController::class, 'addIzinBelajarLangkahSatu']);
        //langkah kedua awal
        Route::get('/isi-jadwal-perkuliahan/{id}', [AdministrasiIbelController::class, 'indexIzinBelajarLangkahDua']);
        Route::post('/isi-jadwal-perkuliahan/{id}', [AdministrasiIbelController::class, 'nextBuatIzinBelajar']);
        //langkah ketiga
        Route::get('/upload-dokumen-persyaratan/{id}', [AdministrasiIbelController::class, 'indexIzinBelajarLangkahTiga']);
        Route::get('/upload-dokumen-persyaratan/{id}/file-prodi-sesuai', [AdministrasiIbelController::class, 'generateFileProdiSesuai']);
        Route::get('/upload-dokumen-persyaratan/{id}/file-tidak-menuntut', [AdministrasiIbelController::class, 'generateFileTidakMenuntut']);
        Route::patch('/upload-dokumen-persyaratan/inputNameFile', [AdministrasiIbelController::class, 'postAfterUploadFile']);
        Route::post('/upload-dokumen-persyaratan/{id}', [AdministrasiIbelController::class, 'finishIzinBelajarTiga']);

        //update langkah 1
        Route::get('/isi-data-utama/edit/{id}', [AdministrasiIbelController::class, 'indexIzinBelajarStepSatuEdit']);
        Route::post('/isi-data-utama/edit/{id}', [AdministrasiIbelController::class, 'editIzinBelajarLangkahSatu']);
        //update langkah 2
        Route::get('/isi-jadwal-perkuliahan/edit/{id}', [AdministrasiIbelController::class, 'indexIzinBelajarLangkahDuaEdit']);
        Route::post('/isi-jadwal-perkuliahan/edit/{id}', [AdministrasiIbelController::class, 'editIzinBelajarLangkahDua']);

        //izin belajar karena mutasi
        Route::get('/konfirmasi-ibel/{id}', [AdministrasiIbelController::class, 'cekMutasiStatusIbel']);
        Route::get('/konfirmasi-tatap-muka/{id}', [AdministrasiIbelController::class, 'indexKonfirmasiIbel']);
        Route::post('/konfirmasi-tatap-muka/{id}', [AdministrasiIbelController::class, 'inputKonfirmasiIbel']);
        Route::get('/langkah-dua-mutasi/{id}', [AdministrasiIbelController::class, 'indexIzinBelajarLangkahDuaMutasi']);
        Route::get('/langkah-tiga-mutasi/{id}', [AdministrasiIbelController::class, 'indexIzinBelajarLangkahTigaMutasi']);
        Route::get('/langkah-empat-mutasi/{id}', [AdministrasiIbelController::class, 'indexIzinBelajarLangkahEmpatMutasi']);

    });


    // Menu Persetujuan Tim Seleksi
    Route::prefix('/ibel/persetujuan-hasil-seleksi')->group(function () {
        Route::get('/', [PersetujuanHasilSeleksi::class, 'index']);
        Route::get('/{route}', [PersetujuanHasilSeleksi::class, 'index']);
        Route::get('/persetujuan/{id}', [PersetujuanHasilSeleksi::class, 'periksaPersetujuan']);
        Route::post('/persetujuan/{id}', [PersetujuanHasilSeleksi::class, 'timSetuju']);
        Route::get('/lihat/{id}', [PersetujuanHasilSeleksi::class, 'lihatPermohonan']);
        // Route::get('/permohonan/{id}/tolak', [PeriksaPermohonanIbelController::class, 'tolakPermohonan']);
    });


});

Route::get('/files/{file}', [IbelController::class, 'getFile']);

Route::get('/ibel/tiket/{tiket}', [IbelController::class, 'getTiket']);
Route::get('/ibel/files/{file}', [IbelController::class, 'getFile']);


