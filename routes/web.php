<?php


use App\Http\Controllers\SDM01\Cuti\CutiBersamaController;
use App\Http\Controllers\SDM01\HariLibur\HariLiburController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\SDM01\Cuti\CutiController;
use App\Http\Controllers\SDM01\JamKerja\JamKerjaCutiController;
use App\Http\Controllers\SDM01\JamKerja\JamKerjaUmumController;
use App\Http\Controllers\TemplateController;
use App\Http\Controllers\TubelController;
use App\Http\Controllers\SDM01\Cuti\Uk3tspController;
use Illuminate\Support\Facades\Route;

// Route::middleware(['CekAuth', 'web', 'MenuAccess'])->group(function () {
// Route::group(['middleware' => ['web']], function () {
Route::middleware(['myauth'])->group(function () {
    Route::get('/', [HomeController::class, 'index']);
    Route::get('/beranda', [HomeController::class, 'index']);
    Route::get('/layananmandiri', [HomeController::class, 'layananMandiri']);

    Route::get('/template/form', [TemplateController::class, 'form']);
    Route::get('/template/notifikasi', [TemplateController::class, 'notifikasi']);
    Route::get('/template/tabel', [TemplateController::class, 'tabel']);
    Route::get('/template/cari', [TemplateController::class, 'cari']);

    Route::get('/harilibur/administrasi', [HariLiburController::class, 'index']);
    Route::post('/harilibur/administrasi/hapusUsulan',[HariLiburController::class, 'deletePermohonan']);
    Route::get('/harilibur/administrasi/showFormEditUsulan',[HariLiburController::class, 'showFormEditUsulan']);
    Route::post('/harilibur/administrasi/kirimUsulan',[HariLiburController::class, 'kirimUsulan']);
    Route::post('/harilibur/createPermohonan', [HariLiburController::class, 'createPermohonan'])->name('harilibur.createPermohonan');
    Route::get('/harilibur/testDelete',[HariLiburController::class, 'testDelete']);
    Route::get('/harilibur/getProvinsiList',[HariLiburController::class, 'getProvinsiList']);
    Route::get('/harilibur/getProvinsi',[HariLiburController::class, 'getProvinsi']);
    Route::get('/harilibur/getKotaList',[HariLiburController::class, 'getKotaList']);
    Route::get('/harilibur/getKantorList',[HariLiburController::class, 'getKantorList']);
    Route::get('/harilibur/getAgamaList',[HariLiburController::class, 'getAgamaList']);
    Route::get('/harilibur/getAgama',[HariLiburController::class, 'getAgama']);
    Route::get('/files/{file}', [TubelController::class, 'downloadFile']);
    Route::get('/harilibur/persetujuan', [HariLiburController::class, 'showPersetujuan']);
    Route::post('/harilibur/persetujuan/approveUsulan',[HariLiburController::class, 'approveUsulan']);
    Route::post('/harilibur/persetujuan/tolakUsulan',[HariLiburController::class,'tolakUsulan']);
    Route::get('/harilibur/monitoring',[HariLiburController::class,'monitoring']);

    Route::get('/jamkerjaumum/administrasi',[JamKerjaUmumController::class, 'index']);
    Route::post('/jamkerjaumum/createPermohonan', [JamKerjaUmumController::class, 'createPermohonan'])->name('jamkerjaumum.createPermohonan');
    Route::post('/jamkerjaumum/administrasi/hapusUsulan',[JamKerjaUmumController::class, 'deletePermohonan']);
    Route::get('/jamkerjaumum/persetujuan', [JamKerjaUmumController::class, 'showPersetujuan']);
    Route::post('/jamkerjaumum/persetujuan/approveUsulan',[JamKerjaUmumController::class, 'approveUsulan']);

    Route::get('/jamkerjacuti/administrasi',[JamKerjaCutiController::class, 'index']);
    Route::post('/jamkerjacuti/createPermohonan', [JamKerjaCutiController::class, 'createPermohonan'])->name('jamkerjacuti.createPermohonan');
    Route::post('/jamkerjacuti/administrasi/hapusUsulan',[JamKerjaCutiController::class, 'deletePermohonan']);
    Route::get('/jamkerjacuti/persetujuan', [JamKerjaCutiController::class, 'showPersetujuan']);
    Route::post('/jamkerjacuti/persetujuan/approveUsulan',[JamKerjaCutiController::class, 'approveUsulan']);

    Route::get('/cutibersama/administrasi',[CutiBersamaController::class, 'index']);
    Route::post('/cutibersama/createPermohonan', [CutiBersamaController::class, 'createPermohonan'])->name('cutibersama.createPermohonan');
    Route::post('/cutibersama/administrasi/hapusUsulan',[CutiBersamaController::class, 'deletePermohonan']);
    Route::get('/cutibersama/persetujuan', [CutiBersamaController::class, 'showPersetujuan']);
    Route::post('/cutibersama/persetujuan/approveUsulan',[CutiBersamaController::class, 'approveUsulan']);

    Route::get('/cuti/administrasi',[CutiController::class,'index']);
    Route::get('/cuti/showTest',[CutiController::class,'showTest']);
    Route::post('/cuti/getHariKerja',[CutiController::class,'getHariKerja']);
    Route::post('/cuti/createPermohonan', [CutiController::class, 'createPermohonan'])->name('cuti.createPermohonan');
    Route::post('/cuti/createPermohonanCutiTambahan', [CutiController::class, 'createPermohonanCutiTambahan'])->name('cuti.createPermohonanCutiTambahan');
    Route::post('/cuti/deletePermohonan', [CutiController::class, 'deletePermohonan']);
    Route::post('/cuti/deletePermohonanCutiTambahan', [CutiController::class, 'deletePermohonanCutiTambahan']);
    Route::post('/cuti/kirimPermohonan',[CutiController::class,'kirimPermohonan']);
    Route::post('/cuti/kirimPermohonanCutiTambahan',[CutiController::class,'kirimPermohonanCutiTambahan']);
    Route::get('cuti/tambahan/persetujuan',[CutiController::class, 'showPersetujuanCutiTambahan']);
    Route::get('cuti/persetujuan',[CutiController::class, 'showPersetujuan']);
    Route::post('cuti/tolakPermohonan',[CutiController::class, 'tolakPermohonan']);
    Route::post('cuti/approvePermohonan',[CutiController::class, 'approvePermohonan']);
    Route::post('cuti/tambahan/tolakPermohonan',[CutiController::class, 'tolakPermohonanTambahan']);
    Route::post('cuti/tambahan/approvePermohonan',[CutiController::class, 'approvePermohonanTambahan']);
    Route::get('cuti/persetujuan/upk',[CutiController::class, 'showPersetujuanUPK']);
    Route::get('cuti/tambahan/persetujuan/upk',[CutiController::class, 'showPersetujuanUPKTambahan']);
    Route::post('cuti/tolakPermohonan/upk',[CutiController::class, 'tolakPermohonanUPK']);
    Route::post('cuti/approvePermohonan/upk',[CutiController::class, 'approvePermohonanUPK']);
    Route::post('cuti/tambahan/tolakPermohonan/upk',[CutiController::class, 'tolakPermohonanUPKTambahan']);
    Route::post('cuti/tambahan/approvePermohonan/upk',[CutiController::class, 'approvePermohonanUPKTambahan']);
    Route::post('cuti/permohonanPembatalanCuti',[CutiController::class, 'permohonanPembatalanCuti']);
    Route::post('cuti/permohonanPembatalanCutiTambahan',[CutiController::class, 'permohonanPembatalanCutiTambahan']);
    Route::post('cuti/kembaliKePersetujuan',[CutiController::class, 'kembaliKePersetujuan']);
    Route::post('cuti/kembaliKePersetujuanTambahan',[CutiController::class, 'kembaliKePersetujuanTambahan']);
    Route::get('cuti/pengelolaan_cuti',[CutiController::class, 'pengelolaanCuti']);
    Route::get('/cuti/print',[CutiController::class,'cetak']);
    Route::post('cuti/sesuaikanPermohonan',[CutiController::class, 'sesuaikanPermohonan']);
    Route::post('cuti/sesuaikanPermohonanCutiTambahan',[CutiController::class, 'sesuaikanPermohonanTambahan']);

    Route::get('/uk3tsp/administrasi',[Uk3tspController::class,'index']);
    Route::post('/uk3tsp/createPermohonan', [Uk3tspController::class, 'createPermohonan'])->name('uk3tsp.createPermohonan');
    Route::post('/uk3tsp/editPermohonan', [Uk3tspController::class, 'editPermohonan'])->name('uk3tsp.editPermohonan');
    Route::post('/uk3tsp/administrasi/hapusUsulan',[Uk3tspController::class, 'deletePermohonan']);
    Route::get('/uk3tsp/getKantorList',[Uk3tspController::class, 'getKantorList']);
    Route::post('/uk3tsp/administrasi/kirimUsulan',[Uk3tspController::class, 'kirimUsulan']);
    Route::get('/uk3tsp/persetujuan', [Uk3tspController::class, 'showPersetujuan']);
    Route::post('/uk3tsp/persetujuan/approveUsulan',[Uk3tspController::class, 'approveUsulan']);
    Route::post('/uk3tsp/persetujuan/tolakUsulan',[Uk3tspController::class,'tolakUsulan']);
    Route::get('/uk3tsp/administrasi/showFormEditUsulan',[Uk3tspController::class, 'showFormEditUsulan']);

    // Logout
    Route::get('/logout', [LoginController::class, 'logout']);
});


// Login
Route::middleware(['guest', 'web'])->group(function () {
    Route::get('/login', [LoginController::class, 'index'])->name('login');
    Route::post('/login', [LoginController::class, 'login']);
});
