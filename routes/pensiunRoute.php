<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\Pensiun\PermohonanMasaPensiunController;
use App\Http\Controllers\Pensiun\PengusulanPermohonanPensiunController;
use App\Http\Controllers\Pensiun\AdministrasiPensiunController;
use App\Http\Controllers\Pensiun\MonitoringPPController;
use Illuminate\Support\Facades\Route;


Route::middleware(['CekAuth','web'])->group(function () {
    Route::prefix('/permohonan-masa-pensiun')->group(function () {

        #pegawai create mpp
        Route::get('/permohonan', [PermohonanMasaPensiunController::class, 'index']);
        Route::get('/permohonan/table', [PermohonanMasaPensiunController::class, 'getHomeAdd'])->name('permohonan.getHomeAdd');
        Route::get('/permohonan/log-status', [PermohonanMasaPensiunController::class, 'logstatusmpp']);
        Route::get('/form-permohonan', [PermohonanMasaPensiunController::class, 'addPermohonan']);
        Route::get('/konsep-sk/{id}', [PermohonanMasaPensiunController::class, 'generateSK']);
        Route::post('/form-permohonan/simpan', [PermohonanMasaPensiunController::class, 'simpanPermohonan']);
        Route::get('/edit/{id}', [PermohonanMasaPensiunController::class, 'editPermohonan']);
        Route::post('/update/simpan', [PermohonanMasaPensiunController::class, 'updatePermohonan']);
        Route::get('/file/{id}', [PermohonanMasaPensiunController::class, 'getFilePermohonan']);
        Route::patch('/ajukan/{id}',[PermohonanMasaPensiunController::class, 'ajukanPermohonan']);
        Route::delete('/delete/{id}', [PermohonanMasaPensiunController::class, 'deletePermohonan']);
        Route::get('/permohonan/download-sk/{namafile}', [PermohonanMasaPensiunController::class, 'getSKMpp']);
        # penelitian
        Route::post('/setujui/{id}',[PermohonanMasaPensiunController::class, 'setujuiPermohonan']);
        Route::post('/tolak',[PermohonanMasaPensiunController::class, 'tolakPermohonan']);
        Route::get('/review', [PermohonanMasaPensiunController::class, 'reviewPermohonan']);
        Route::get('/review/detil/{id}', [PermohonanMasaPensiunController::class, 'reviewPermohonanDetil']);
        Route::get('/disposisi', [PermohonanMasaPensiunController::class, 'getDisposisi']);
        Route::get('/disposisi/detil/{id}', [PermohonanMasaPensiunController::class, 'detilDisposisiPermohonan']);
        Route::post('/disposisi',[PermohonanMasaPensiunController::class, 'simpanDisposisiPermohonan']);
        Route::get('/review/disposisi/{id}',[PermohonanMasaPensiunController::class, 'detilReviewDisposisiPermohonan']);
        Route::post('/review/disposisi/penelitian',[PermohonanMasaPensiunController::class, 'penilitianDisposisiPermohonan']);
        Route::post('/review/disposisi/penelitian/simpan/{id}',[PermohonanMasaPensiunController::class, 'simpanFinalPenelitianPermohonan']);
        Route::get('/review/konsep-sk/{id}',[PermohonanMasaPensiunController::class, 'konsepSKPermohonan']);
        Route::get('/review/disposisi/penelitian/tangguhkan/{id}',[PermohonanMasaPensiunController::class, 'tangguhkanKonsepSKMpp']);
        Route::get('/review/disposisi/penelitian/tolak/{id}', [PermohonanMasaPensiunController::class, 'tolakKonsepSKMpp']);
        Route::get('/review/disposisi/penelitian/setujui/{id}',[PermohonanMasaPensiunController::class, 'setujuiKonsepSKMpp']);
        Route::post('/review/upload-sk-mpp',[PermohonanMasaPensiunController::class, 'uploadSKMpp']);
    });

    #pengusulan pensiun
    Route::prefix('/pengusulan-pensiun/')->group(function () {

        #create pengusulan
        Route::get('permohonan', [PengusulanPermohonanPensiunController::class, 'homePermohonan']);
        Route::get('permohonan/upk', [PengusulanPermohonanPensiunController::class, 'homePermohonanUpk']);
        Route::get('/permohonan/log-status', [PengusulanPermohonanPensiunController::class, 'logstatuspp']);
        Route::get('generate/{id}', [PengusulanPermohonanPensiunController::class, 'generateSurat']);
        Route::get('form-tambah-permohonan', [PengusulanPermohonanPensiunController::class, 'addPermohonan']);
        Route::post('form-tambah-permohonan', [PengusulanPermohonanPensiunController::class, 'simpanPermohonan']);
        Route::get('form-edit/{id}', [PengusulanPermohonanPensiunController::class, 'editPermohonan']);
        Route::post('form-edit/{id}', [PengusulanPermohonanPensiunController::class, 'setEditPermohonan']);
        Route::get('file/{id}', [PengusulanPermohonanPensiunController::class, 'getFilePermohonan']);
        Route::delete('delete/{id}', [PengusulanPermohonanPensiunController::class, 'deletePermohonan']);
        Route::patch('/ajukan/{id}',[PengusulanPermohonanPensiunController::class, 'ajukanPermohonan']);
        Route::patch('/tolak/{id}',[PengusulanPermohonanPensiunController::class, 'tolakPermohonan']);
        Route::post('/setujui/{id}',[PengusulanPermohonanPensiunController::class, 'setujuiPermohonan']);
        Route::get('form-tambah-permohonan-upk', [PengusulanPermohonanPensiunController::class, 'addPermohonanUpk']);
        Route::post('form-tambah-permohonan-upk', [PengusulanPermohonanPensiunController::class, 'simpanPermohonanUpk']);
        Route::get('getDataPegawai/{nip}', [PengusulanPermohonanPensiunController::class, 'getDataPegawai']);
        Route::get('getDokumen/{id}/{idRcase}', [PengusulanPermohonanPensiunController::class, 'getBerkasPensiun']);
        Route::get('form-edit-upk/{id}', [PengusulanPermohonanPensiunController::class, 'editPermohonanUpk']);
        Route::post('form-edit-upk/{id}', [PengusulanPermohonanPensiunController::class, 'setEditPermohonanUpk']);
        Route::post('form-upload-upk/draft', [PengusulanPermohonanPensiunController::class, 'setDraftPenelitian']);
        Route::patch('form-upload-upk/final/{id}', [PengusulanPermohonanPensiunController::class, 'setFinalPenelitian']);
        Route::get('/permohonan/download-sk/{namafile}', [PengusulanPermohonanPensiunController::class, 'getSKPp']);


        #penelitian
        Route::get('penelitian', [PengusulanPermohonanPensiunController::class, 'penelitianPermohonan']);
        Route::get('penelitian/detil/{id}', [PengusulanPermohonanPensiunController::class, 'detilPenelitianPermohonan']);
        Route::get('penelitian/detil/final/{id}', [PengusulanPermohonanPensiunController::class, 'detilPenelitianPermohonan']);
        Route::post('penelitian/draft', [PengusulanPermohonanPensiunController::class, 'setDraftPenelitian']);
        Route::patch('penelitian/final/{id}', [PengusulanPermohonanPensiunController::class, 'setFinalPenelitian']);
        Route::get('permohonan/disposisi', [PengusulanPermohonanPensiunController::class, 'getDisposisi']);
        Route::get('permohonan/disposisi/detil/{id}', [PengusulanPermohonanPensiunController::class, 'getDetilDisposisi']);
        Route::post('permohonan/disposisi', [PengusulanPermohonanPensiunController::class, 'setDisposisi']);
        Route::post('penelitian/upload-surat',[PengusulanPermohonanPensiunController::class, 'uploadSuratPersetujuan']);
        Route::post('penelitian/upload-sk-pp',[PengusulanPermohonanPensiunController::class, 'uploadSKPp']);

        Route::get('penelitian/tangguhkan/{id}',[PengusulanPermohonanPensiunController::class, 'generateDitangguhkan']);
        Route::get('penelitian/tolak/{id}', [PengusulanPermohonanPensiunController::class, 'generateDitolak']);
        Route::get('penelitian/setujui/{id}',[PengusulanPermohonanPensiunController::class, 'generateDisetujui']);
        Route::get('penelitian/generatekonsep/{id}',[PengusulanPermohonanPensiunController::class, 'generateKonsepSK']);

    });

    Route::prefix('/administrasi')->group(function(){
        Route::get('/',[AdministrasiPensiunController::class,'homeAdministrasi']);
        Route::get('/form-administrasi/{id}',[AdministrasiPensiunController::class,'detilAdministrasi']);
        Route::post('/setdraft',[AdministrasiPensiunController::class,'kirimSKDraft']);
        Route::patch('/setfinal/{id}',[AdministrasiPensiunController::class,'kirimSKFinal']);
    });

    Route::prefix('/monitoring')->group(function(){
        Route::get('/',[MonitoringPPController::class,'homeMonitoring']);
        Route::get('/detil/{id}',[MonitoringPPController::class,'detilMonitoring']);
    });
});


