<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\TemplateController;
use App\Http\Controllers\Tubel\AdministrasiController;
use App\Http\Controllers\Tubel\MonitoringController;
use App\Http\Controllers\Tubel\CutiBelajarController;
use App\Http\Controllers\Tubel\LpsController;
use App\Http\Controllers\Tubel\PengaktifanKembaliController;
use App\Http\Controllers\Tubel\PerpanjanganStController;
use App\Http\Controllers\Tubel\TubelController;
use App\Http\Controllers\Tubel\UsulanTopikRisetController;
use App\Http\Controllers\Tubel\KelolaReferensiController;
use App\Http\Controllers\Tubel\LaporDiriController;
use App\Http\Controllers\Tubel\SelesaiTubelController;
use Illuminate\Support\Facades\Route;

Route::middleware(['tubelRoute', 'CekAuth', 'MenuAccess'])->group(function () {
    Route::get('/beranda', [HomeController::class, 'index']);

    # template
    Route::get('/template/form', [TemplateController::class, 'form']);
    Route::get('/template/notifikasi', [TemplateController::class, 'notifikasi']);
    Route::get('/template/tabel', [TemplateController::class, 'tabel']);
    Route::get('/template/cari', [TemplateController::class, 'cari']);

    # administrasi
    Route::prefix('/tubel/administrasi')->group(function () {
        Route::get('/', [AdministrasiController::class, 'index']);
        Route::get('/get-tubel', [AdministrasiController::class, 'getDataTubel']);
        Route::post('/get-kantor', [AdministrasiController::class, 'getDataKantor']);
        Route::post('/get-jabatan', [AdministrasiController::class, 'getDataJabatan']);
        Route::post('/get-stan', [AdministrasiController::class, 'getDataStan']);
        Route::post('/get-pt', [AdministrasiController::class, 'getDataPerguruanTinggi']);
        Route::post('/get-prodi', [AdministrasiController::class, 'getDataProdi']);
        Route::post('/get-negara', [AdministrasiController::class, 'getDataNegara']);
        Route::post('/get-topik-riset', [AdministrasiController::class, 'getTopikRiset']);
        Route::get('/{id}', [AdministrasiController::class, 'detailTubel']);
        Route::get('/{id}/update', [AdministrasiController::class, 'updateTubel']);
        Route::patch('/{id}/update', [AdministrasiController::class, 'updateTubelAction']);
        Route::get('/generate-lps/{idlps}', [AdministrasiController::class, 'generateLps']);
    });

    # administrasi > perguruan tinggi
    Route::prefix('/tubel/administrasi')->group(function () {
        Route::get('/perguruan-tinggi/{id}', [AdministrasiController::class, 'getPerguruanTinggi']);
        Route::post('/perguruan-tinggi/{id}', [AdministrasiController::class, 'addPerguruanTinggi']);
        Route::patch('/perguruan-tinggi/{id}', [AdministrasiController::class, 'updatePerguruanTinggi']);
        Route::delete('/perguruan-tinggi/{id}', [AdministrasiController::class, 'deletePerguruanTinggi']);
    });

    # administrasi > tabel lps
    Route::prefix('/tubel/administrasi')->group(function () {
        Route::get('/{id}/lps', [AdministrasiController::class, 'detailLps']);
        Route::post('/{id}/lps', [AdministrasiController::class, 'addLps']);
        Route::patch('/{id}/lps', [AdministrasiController::class, 'updateLps']);
        Route::delete('/{id}/lps', [AdministrasiController::class, 'deleteLps']);
        Route::put('/uploadkhs', [AdministrasiController::class, 'uploadKhs']);
        Route::patch('/uploadkhs', [AdministrasiController::class, 'updateKhsFile']);
        Route::put('/uploadlps', [AdministrasiController::class, 'uploadLps']);
        Route::patch('/uploadlps', [AdministrasiController::class, 'updateLpsFile']);
        Route::post('/lps/kirim', [AdministrasiController::class, 'kirimLps']);
        Route::post('/updateStatusLps', [AdministrasiController::class, 'updateStatusLps']);
    });

    # administrasi > tabel cuti
    Route::prefix('/tubel/administrasi')->group(function () {
        Route::post('/cuti-belajar', [AdministrasiController::class, 'addCutiBelajar']);
        Route::patch('/cuti-belajar', [AdministrasiController::class, 'updateCutiBelajar']);
        Route::delete('/cuti-belajar', [AdministrasiController::class, 'deleteCutiBelajar']);
        Route::post('/cuti-belajar-admin', [AdministrasiController::class, 'addCutiBelajarAdmin']);
        Route::patch('/cuti-belajar-admin', [AdministrasiController::class, 'updateCutiBelajarAdmin']);
        Route::post('/cuti-belajar/update', [AdministrasiController::class, 'updateStatusCutiBelajar']);
        Route::post('/cuti-belajar-perpanjang', [AdministrasiController::class, 'addPerpanjangCutiPegawai']);
    });

    # administrasi > tabel pengaktifan kembali
    Route::prefix('/tubel/administrasi')->group(function () {
        Route::post('/pengaktifan-kembali', [AdministrasiController::class, 'addPengaktifanKembali']);
        Route::patch('/pengaktifan-kembali', [AdministrasiController::class, 'updatePermohonanAktif']);
        Route::delete('/pengaktifan-kembali', [AdministrasiController::class, 'deletePermohonanAktif']);
        Route::post('/pengaktifan-kembali/kirim', [AdministrasiController::class, 'kirimPermohonanAktif']);
        Route::post('/pengaktifan-kembali/rekam', [AdministrasiController::class, 'rekamPermohonanAktif']);
    });

    # administrasi > tabel perpanjangan st tubel
    Route::prefix('/tubel/administrasi')->group(function () {
        Route::post('/perpanjangan-st-tubel', [AdministrasiController::class, 'addPerpanjanganStTubel']);
        Route::patch('/perpanjangan-st-tubel', [AdministrasiController::class, 'updatePerpanjanganStTubel']);
        Route::delete('/perpanjangan-st-tubel', [AdministrasiController::class, 'deletePerpanjanganStTubel']);
        Route::post('/perpanjangan-st-tubel/kirim', [AdministrasiController::class, 'kirimPerpanjanganStTubel']);
    });

    # administrasi > tabel usulan topik riset
    Route::prefix('/tubel/administrasi')->group(function () {
        Route::post('/usulan-topik-riset', [AdministrasiController::class, 'addUsulanTopikRiset']);
        Route::patch('/usulan-topik-riset', [AdministrasiController::class, 'updateUsulanTopikRiset']);
        Route::delete('/usulan-topik-riset', [AdministrasiController::class, 'deleteUsulanTopikRiset']);
        Route::post('/usulan-topik-riset/kirim', [AdministrasiController::class, 'kirimUsulanTopikRiset']);
        Route::post('/usulan-topik-riset/persetujuan', [AdministrasiController::class, 'persetujuanPtUsulanTopikRiset']);
        Route::post('/usulan-topik-riset/kirim-persetujuan', [AdministrasiController::class, 'kirimPersetujuanPtUsulanTopikRiset']);

        Route::post('/selesai-studi/lulus', [AdministrasiController::class, 'selesaiStudiLulus']);
        Route::post('/selesai-studi/belum-lulus', [AdministrasiController::class, 'selesaiStudiBelumLulus']);
        Route::post('/selesai-studi/mengundurkan-diri', [AdministrasiController::class, 'selesaiStudiMengundurkanDiri']);
        Route::post('/selesai-studi/drop-out', [AdministrasiController::class, 'selesaiStudiDropOut']);
        Route::post('/selesai-studi/kirim', [AdministrasiController::class, 'kirimLaporanSelesaiStudi']);
        Route::get('/selesai-studi/generate/form-lps/{id}', [AdministrasiController::class, 'generateFormLps']);
        Route::post('/selesai-studi/generate/hasil-akhir', [AdministrasiController::class, 'getHasilAkhir']);

        Route::post('/hasil-studi/lulus', [AdministrasiController::class, 'hasilStudiLulus']);
        Route::post('/hasil-studi/mengundurkan-diri', [AdministrasiController::class, 'hasilStudiMengundurkanDiri']);
        Route::post('/hasil-studi/drop-out', [AdministrasiController::class, 'hasilStudiDropOut']);
        Route::post('/hasil-studi/kirim', [AdministrasiController::class, 'kirimLaporanHasilStudi']);
        Route::post('/hasil-studi/kirim-ijazah', [AdministrasiController::class, 'hasilStudiKirimIjazah']);
        Route::post('/hasil-studi/ijazah', [AdministrasiController::class, 'hasilStudiRekamIjazah']);
    });

    # monitoring
    Route::prefix('/tubel/monitoring')->group(function () {
        Route::get('/', [MonitoringController::class, 'monitoringTubel']);
        Route::get('/{id}', [MonitoringController::class, 'detailMonitoringTubel']);
    });

    # laporan perkembangan studi
    Route::prefix('/tubel/lps')->group(function () {
        Route::get('/', [LpsController::class, 'getLps']);
        Route::get('/setuju', [LpsController::class, 'getLpsSetuju']);
        Route::get('/tolak', [LpsController::class, 'getLpsDitolak']);
        Route::get('/kirim', [LpsController::class, 'getLpsDikirim']);
        Route::get('/dashboard', [LpsController::class, 'getLpsDashboard']);
        Route::post('/updateStatusLps', [AdministrasiController::class, 'updateStatusLps']);
    });

    # cuti belajar
    Route::prefix('/tubel/cuti')->group(function () {
        Route::get('/', [CutiBelajarController::class, 'getCuti']);
        Route::get('/setuju', [CutiBelajarController::class, 'getCutiSetuju']);
        Route::get('/tolak', [CutiBelajarController::class, 'getCutiTolak']);
        Route::get('/selesai', [CutiBelajarController::class, 'getCutiSelesai']);
        Route::get('/dashboard', [CutiBelajarController::class, 'getCutiDashboard']);
        Route::post('/updatestatus', [AdministrasiController::class, 'updateStatusCuti']);
        # Route::post('/updatecuti', [CutiBelajarController::class, 'updateCuti']);
        Route::get('/getinduktubel', [CutiBelajarController::class, 'GetIndukTubelCuti']);
    });

    # kep penempatan cuti
    Route::prefix('/tubel/kep-penempatan-cuti')->group(function () {
        Route::get('/', [CutiBelajarController::class, 'getKepPenempatanCuti']);
        Route::get('/persetujuan', [CutiBelajarController::class, 'getKepPenempatanCutiSetuju']);
        Route::get('/ditempatkan', [CutiBelajarController::class, 'getKepPenempatanCutiDitempatkan']);
        Route::post('/rekamkep', [CutiBelajarController::class, 'rekamKepPenempatanCuti']);
        Route::get('/get-lokasi-penempatan', [CutiBelajarController::class, 'getLokasiPenempatan']);
        Route::post('/get-kantor', [CutiBelajarController::class, 'getAdvKantor']);
        Route::post('/get-jabatan', [CutiBelajarController::class, 'getAdvJabatan']);
    });

    # perpanjang cuti
    Route::prefix('/tubel/perpanjangan-cuti')->group(function () {
        Route::get('/', [CutiBelajarController::class, 'getPerpanjanganCuti']);
        Route::get('/setuju', [CutiBelajarController::class, 'getPerpanjanganCutiSetuju']);
        Route::get('/tolak', [CutiBelajarController::class, 'getPerpanjanganCutiSetujuTolak']);
        Route::get('/getcuti', [CutiBelajarController::class, 'getDataPerpanjanganCuti']);
        # Route::post('/', [TubelController::class, 'updateCuti']);
        Route::post('/admin', [CutiBelajarController::class, 'addPerpanjanganCutiAdmin']);
        Route::post('/update', [CutiBelajarController::class, 'updatePerpanjanganCutiAdmin']);
    });

    # pengaktifan kembali
    Route::prefix('/tubel/pengaktifan-kembali')->group(function () {
        Route::get('/', [PengaktifanKembaliController::class, 'getPengaktifanKembali']);
        Route::get('/disetujui', [PengaktifanKembaliController::class, 'getPengaktifanKembaliSetuju']);
        Route::get('/ditolak', [PengaktifanKembaliController::class, 'getPengaktifanKembaliTolak']);
        Route::get('/terbit-st', [PengaktifanKembaliController::class, 'getPengaktifanKembaliTerbitst']);
        Route::get('/terbit-kep', [PengaktifanKembaliController::class, 'getPengaktifanKembaliTerbitkep']);
        # Route::get('/dashboard', [PengaktifanKembaliController::class, 'pengaktifankembalidashboard']);
        Route::post('/setuju', [PengaktifanKembaliController::class, 'setujuPengaktifanKembali']);
        Route::post('/tolak', [PengaktifanKembaliController::class, 'tolakPengaktifanKembali']);
        Route::post('/rekam', [PengaktifanKembaliController::class, 'pengaktifanKembaliRekam']);
    });

    # st aktif kembali
    Route::prefix('/tubel/st-aktif-kembali')->group(function () {
        Route::get('/', [PengaktifanKembaliController::class, 'stAktifKembali']);
        Route::get('/direkam', [PengaktifanKembaliController::class, 'stAktifKembaliRekam']);
        Route::post('/rekamst', [PengaktifanKembaliController::class, 'rekamStAktifKembali']);
    });

    # kep pembebasan
    Route::prefix('/tubel/kep-pembebasan')->group(function () {
        Route::get('/', [PengaktifanKembaliController::class, 'keppembebasan']);
        Route::get('/direkam', [PengaktifanKembaliController::class, 'keppembebasandirekam']);
        Route::post('/rekam', [PengaktifanKembaliController::class, 'rekamKepPembebasan']);
    });

    # perpanjangan st non stan
    Route::prefix('/tubel/perpanjangan-st-nonstan')->group(function () {
        Route::get('/', [PerpanjanganStController::class, 'perpanjanganStNonStan']);
        Route::get('/disetujui', [PerpanjanganStController::class, 'perpanjanganStNonStanSetuju']);
        Route::get('/dikembalikan', [PerpanjanganStController::class, 'perpanjanganStNonStankembalikan']);
        Route::get('/ditolak', [PerpanjanganStController::class, 'perpanjanganStNonStantolak']);
        Route::get('/terbitst', [PerpanjanganStController::class, 'perpanjanganStNonStanterbitst']);
        Route::get('/dashboard', [PerpanjanganStController::class, 'perpanjanganStNonStandashboard']);
        Route::post('/uploadst', [PerpanjanganStController::class, 'perpanjanganStNonStanActionUploadSt']);
        Route::post('/setuju', [PerpanjanganStController::class, 'perpanjanganStNonStanActionSetuju']);
        Route::post('/tolak', [PerpanjanganStController::class, 'perpanjanganStNonStanActionTolak']);
        Route::post('/kembalikan', [PerpanjanganStController::class, 'perpanjanganStNonStanActionKembalikan']);
    });

    # perpanjangan st stan
    Route::prefix('/tubel/perpanjangan-st-stan')->group(function () {
        Route::get('/', [PerpanjanganStController::class, 'perpanjanganStStan']);
        Route::get('uploadst', [PerpanjanganStController::class, 'perpanjanganStStanuploadst']);
        Route::get('uploadst/{id}', [PerpanjanganStController::class, 'perpanjanganStStanDetailLihat']);
        Route::get('/{id}/add', [PerpanjanganStController::class, 'perpanjanganStStanAdd']);
        Route::get('/{id}/lihat', [PerpanjanganStController::class, 'perpanjanganStStanDetail']);

        Route::post('/delete', [PerpanjanganStController::class, 'deletePerpanjanganStStan']);
        Route::post('/dasarst', [PerpanjanganStController::class, 'addDasarStPerpanjanganStStan']);
        Route::post('/updatedasarst', [PerpanjanganStController::class, 'updateDasarStPerpanjanganStStan']);
        Route::post('/uploadst', [PerpanjanganStController::class, 'uploadStPerpanjanganStStan']);
        Route::post('/nama_nip_find', [PerpanjanganStController::class, 'getNamaNip']);
        Route::post('/nip_nama_add', [PerpanjanganStController::class, 'addNamaNipPerpanjanganStStan']);
        Route::post('/tubel_add', [PerpanjanganStController::class, 'addPerpanjanganStStanTubel']);
        # Route::post('/lampiran/generate', [PerpanjanganStController::class, 'perpanjanganStStanLampiranGenerate']);
        Route::get('/{id}/generate', [PerpanjanganStController::class, 'generateLampiranStStan']);
        Route::post('/lampiran/delete', [PerpanjanganStController::class, 'deleteLampiranStStan']);
        Route::post('/lampiran/delete_all', [PerpanjanganStController::class, 'deleteAllLampiranStStan']);
    });

    # topik riset
    Route::prefix('/tubel/riset')->group(function () {
        Route::get('/usulan-topik-riset', [UsulanTopikRisetController::class, 'usulanTopikRiset']);
        Route::get('/usulan-topik-riset/{route}', [UsulanTopikRisetController::class, 'usulanTopikRiset']);
        Route::post('/usulan-topik-riset', [UsulanTopikRisetController::class, 'persetujuanUsulanTopikRiset']);
        Route::post('/usulan-topik-riset/sahkan', [UsulanTopikRisetController::class, 'pengesahanUsulanTopikRiset']);
        Route::post('/usulan-topik-riset/nd-pengesahan', [UsulanTopikRisetController::class, 'ndPengesahanUsulanTopikRiset']);
        Route::get('/kelola-referensi', [UsulanTopikRisetController::class, 'kelolaReferensiRiset']);
        Route::post('/kelola-referensi', [UsulanTopikRisetController::class, 'importCsvArray']);
        Route::delete('/kelola-referensi', [UsulanTopikRisetController::class, 'deleteTopikRiset']);
        Route::post('/kelola-referensi/preview', [UsulanTopikRisetController::class, 'previewCsv']);
        Route::post('/get-topik-riset', [AdministrasiController::class, 'getTopikRiset']);
    });

    # lapor diri > lapor diri
    Route::prefix('/tubel/pelaporan')->group(function () {
        Route::get('/lapor-diri', [LaporDiriController::class, 'laporDiri']);
        Route::get('/lapor-diri/{route}', [LaporDiriController::class, 'laporDiri']);
        Route::post('/lapor-diri/setuju', [LaporDiriController::class, 'setujuLaporDiri']);
        Route::post('/lapor-diri/tolak', [LaporDiriController::class, 'tolakLaporDiri']);
    });

    # lapor diri > verifikasi ijazah
    Route::prefix('/tubel/pelaporan')->group(function () {
        Route::get('/verifikasi-ijazah', [LaporDiriController::class, 'verifikasiIjazah']);
        Route::get('/verifikasi-ijazah/{route}', [LaporDiriController::class, 'verifikasiIjazah']);
        Route::post('/verifikasi-ijazah/setuju', [LaporDiriController::class, 'setujuverifikasiIjazah']);
        Route::post('/verifikasi-ijazah/tolak', [LaporDiriController::class, 'tolakverifikasiIjazah']);
    });

    # lapor diri > hasil akhir
    Route::prefix('/tubel/pelaporan')->group(function () {
        Route::get('/hasil-akhir', [LaporDiriController::class, 'hasilAkhir']);
        Route::get('/hasil-akhir/{route}', [LaporDiriController::class, 'hasilAkhir']);
        Route::post('/hasil-akhir/setuju', [LaporDiriController::class, 'setujuHasilAkhir']);
        Route::post('/hasil-akhir/tolak', [LaporDiriController::class, 'tolakHasilAkhir']);
    });

    # kep penempatan selesai tubel
    Route::prefix('/tubel/kep-penempatan')->group(function () {
        Route::get('/', [SelesaiTubelController::class, 'index']);
        Route::get('/{route}', [SelesaiTubelController::class, 'index']);
        Route::post('/upload-kep', [SelesaiTubelController::class, 'uploadKepPenempatan']);
    });

    # files
    Route::get('/files/{file}', [TubelController::class, 'getFile']);
    Route::get('/download/{file}', [TubelController::class, 'downloadFile']);

    # Tabel Referensi Negara
    Route::prefix('/tubel/referensi')->group(function () {
        Route::get('/', [KelolaReferensiController::class, 'indexReferensiNegara']);
        Route::post('/delete', [KelolaReferensiController::class, 'deleteReferensiNegara']);
        Route::post('/tambah', [KelolaReferensiController::class, 'addReferensiNegara']);
        Route::post('/update_negara', [KelolaReferensiController::class, 'updateReferensiNegara']);
    });

    # Tabel Referensi PT
    Route::prefix('/tubel/referensi/pt')->group(function () {
        Route::get('/', [KelolaReferensiController::class, 'indexReferensiPT']);
        Route::post('/delete', [KelolaReferensiController::class, 'deleteReferensiPT']);
        Route::post('/tambah', [KelolaReferensiController::class, 'addReferensiPT']);
        Route::post('/update_pt', [KelolaReferensiController::class, 'updateReferensiPT']);
    });

    # Tabel Referensi Prodi
    Route::prefix('/tubel/referensi/prodi')->group(function () {
        Route::get('/', [KelolaReferensiController::class, 'indexReferensiProdi']);
        Route::post('/delete', [KelolaReferensiController::class, 'deleteReferensiProdi']);
        Route::post('/tambah', [KelolaReferensiController::class, 'addReferensiProdi']);
        Route::post('/update_prodi', [KelolaReferensiController::class, 'updateReferensiProdi']);
    });

    # Tabel Referensi Jenjang Pendidikan
    Route::prefix('/tubel/referensi/jenjang')->group(function () {
        Route::get('/', [KelolaReferensiController::class, 'indexReferensiJenjang']);
        Route::post('/delete', [KelolaReferensiController::class, 'deleteReferensiJenjang']);
        Route::post('/tambah', [KelolaReferensiController::class, 'addReferensiJenjang']);
        Route::post('/update_jenjang', [KelolaReferensiController::class, 'updateReferensiJenjang']);
    });

    # Tabel Referensi Lokasi Pendidikan
    Route::prefix('/tubel/referensi/lokasi')->group(function () {
        Route::get('/', [KelolaReferensiController::class, 'indexReferensiLokasi']);
        Route::post('/delete', [KelolaReferensiController::class, 'deleteReferensiLokasi']);
        Route::post('/tambah', [KelolaReferensiController::class, 'addReferensiLokasi']);
        Route::post('/update_lokasi', [KelolaReferensiController::class, 'updateReferensiLokasi']);
    });

    # Tabel Referensi Tipe Dokumen Penelitian
    Route::prefix('/referensi/tipe_doc')->group(function () {
        Route::get('/', [KelolaReferensiController::class, 'indexReferensiDokumen']);
        Route::post('/delete', [KelolaReferensiController::class, 'deleteReferensiDokumen']);
        Route::post('/tambah', [KelolaReferensiController::class, 'addReferensiDokumen']);
        Route::post('/update_dokumen', [KelolaReferensiController::class, 'updateReferensiDokumen']);
    });
});

# Logout
Route::get('/logout', [LoginController::class, 'logout']);

# tiket
Route::get('/tubel/{tiket}', [TubelController::class, 'getTiket']);
