# HRIS Front-End

[![pipeline status](https://gitlab.com/dadangnh/hris-front-end/badges/master/pipeline.svg)](https://gitlab.com/dadangnh/hris-front-end/-/commits/master)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/111561d1a85d44c0870cbc97fbdc610c)](https://www.codacy.com/gl/dadangnh/hris-front-end/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=dadangnh/hris-front-end&amp;utm_campaign=Badge_Grade)
[![coverage report](https://gitlab.com/dadangnh/hris-front-end/badges/master/coverage.svg)](https://gitlab.com/dadangnh/hris-front-end/-/commits/master)

![Lines of code](https://img.shields.io/tokei/lines/gitlab.com/dadangnh/hris-front-end)
![GitLab tag (latest by SemVer)](https://img.shields.io/gitlab/v/tag/dadangnh/hris-front-end)

Source code for HRIS Front-End Application. Built with [Laravel](https://laravel.com/).

## Canonical source

The canonical source where all development takes place is [hosted on GitLab.com](https://gitlab.com/dadangnh/hris-front-end).

## Installation

Please check [INSTALLATION.md](INSTALLATION.md) for the installation instruction.

## Contributing

This is an open source project, and we are very happy to accept community contributions.

## License

This code is published under [GPLv3 License](LICENSE).
