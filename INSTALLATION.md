# Installation Method

## Requirement
This method require you to have [latest PHP](https://www.php.net), [composer](https://getcomposer.org/), and [Docker Engine](https://docker.com) installed on the host.

### Installation
First, clone this repository:

```bash
git clone git@gitlab.com:dadangnh/hris-front-end.git some_dir
cd some_dir
```

Then, create your environment by copying `.env.example` into `.env` and configure the backend URL, or you can use OS's environment variable.
Make sure to adjust the credentials on the environment for the Docker. You can find inside docker-compose.yaml file

Install all the dependencies:
```bash
composer install
```

Build the containers:
```bash
./vendor/bin/sail up -d
```

#### Migration
run the migration:
```bash
./vendor/bin/sail artisan migrate up
```

#### Add default data (optional on non production)
run the following to add dummy data:
```bash
./vendor/bin/sail artisan db:seed
```

Now your app are ready to use:

Landing page: [http://0.0.0.0/](http://0.0.0.0/)

#### Test

Unit testing also available with the following command:

```bash
./vendor/bin/sail artisan test
```
